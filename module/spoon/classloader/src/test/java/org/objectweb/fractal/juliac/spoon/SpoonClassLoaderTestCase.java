/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.spoon;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.SpoonSupportItf;
import org.objectweb.fractal.juliac.core.Juliac;
import org.objectweb.fractal.juliac.spoon.classloader.SpoonClassLoader;
import org.objectweb.fractal.juliac.spoon.classloader.SpoonSupportImpl;

import spoon.reflect.code.CtBlock;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtPackage;
import spoon.reflect.declaration.CtParameter;
import spoon.reflect.declaration.ModifierKind;
import spoon.reflect.factory.ClassFactory;
import spoon.reflect.factory.CoreFactory;
import spoon.reflect.factory.Factory;
import spoon.reflect.factory.MethodFactory;
import spoon.reflect.factory.PackageFactory;
import spoon.reflect.factory.TypeFactory;
import spoon.reflect.reference.CtTypeReference;

/**
 * Class for testing the functionalities of {@link SpoonClassLoader}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class SpoonClassLoaderTestCase {

	@Test
	public void testFindClass() throws IOException, ClassNotFoundException {

		// Initialize Juliac
		JuliacItf jc = new Juliac();

		// Initialize Spoon
		SpoonSupportItf spoon = new SpoonSupportImpl();
		spoon.init(jc);
		Factory factory = spoon.getSpoonFactory();

		// Retrieve Spoon factories
		PackageFactory packagef = factory.Package();
		ClassFactory classf = factory.Class();
		MethodFactory methodf = factory.Method();
		TypeFactory typef = factory.Type();
		CoreFactory coref = factory.Core();

		// Create a foo.Bar#meth() method
		CtPackage pack = packagef.getOrCreate("foo");
		CtClass<?> bar = classf.create(pack,"Bar");
		Set<ModifierKind> mods = new HashSet<>();
		mods.add(ModifierKind.PUBLIC);
		CtTypeReference<Void> tref = typef.createReference(void.class);
		List<CtParameter<?>> parameters = new ArrayList<>();
		Set<CtTypeReference<? extends Throwable>> thrownTypes = new HashSet<>();
		CtMethod<?> meth =
			methodf.create(bar,mods,tref,"meth",parameters,thrownTypes);
		CtBlock<?> body = coref.createBlock();
		meth.setBody(body);
		body.setParent(meth);

		ClassLoader parent = jc.getClassLoader();
		SpoonClassLoader scl = new SpoonClassLoader(jc,factory,parent);
		Class<?> cl = scl.loadClass("foo.Bar");
		Method[] methods = cl.getDeclaredMethods();
		assertEquals(1,methods.length);
		assertEquals("meth",methods[0].getName());
		assertEquals(0,methods[0].getParameterTypes().length);


		// Tear down Spoon and Juliac
		spoon.close(jc);
		jc.close();
	}
}
