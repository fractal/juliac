/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.spoon.classloader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.internal.compiler.ClassFile;
import org.eclipse.jdt.internal.compiler.batch.CompilationUnit;
import org.eclipse.jdt.internal.compiler.env.ICompilationUnit;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.SourceFileItf;
import org.objectweb.fractal.juliac.compile.jdt.JDTCompiler;
import org.objectweb.fractal.juliac.core.SourceFileString;

import spoon.reflect.declaration.CtPackage;
import spoon.reflect.declaration.CtType;
import spoon.reflect.factory.Factory;
import spoon.reflect.factory.TypeFactory;
import spoon.reflect.visitor.DefaultJavaPrettyPrinter;

/**
 * A class loader for loading the classes from a Spoon model.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class SpoonClassLoader extends ClassLoader {

	private JuliacItf jc;
	private Factory factory;

	public SpoonClassLoader( JuliacItf jc, Factory factory, ClassLoader parent ) {
		super(parent);
		this.jc = jc;
		this.factory = factory;
	}

	@Override
	protected Class<?> findClass( String name ) throws ClassNotFoundException {

		// Retrieve the Spoon type
		TypeFactory tf = factory.Type();
		CtType<?> type = tf.get(name);
		if( type == null ) {
			throw new ClassNotFoundException(name);
		}

		// Retrieve the source code of the type
		DefaultJavaPrettyPrinter printer =
			new DefaultJavaPrettyPrinter(factory.getEnvironment());
		CtPackage pack = type.getPackage();
		if( pack != null ) {
			printer.scan(pack);
		}
		printer.scan(type);
		final String code = printer.getResult();

		// Create the data structures for JDT
		SourceFileItf sf = new SourceFileString(name,code,"java");
	    char[] content = code.toCharArray();
	    String fileName = sf.toString();
		String encoding = System.getProperty("file.encoding");
	    ICompilationUnit unit = new CompilationUnit(content,fileName,encoding);
		Map<ICompilationUnit,SourceFileItf> units = new HashMap<>();
		units.put(unit,sf);

		// Compile the code with JDT
		List<SourceFileItf> sfs = List.of(sf);
		JDTCompiler jdtcompiler = new JDTCompiler(jc,sfs);
		jdtcompiler.compile(units);
		List<ClassFile> classFiles = jdtcompiler.getClassFiles();

		// Retrieve the bytecode
		ClassFile classFile = null;
		for (ClassFile cf : classFiles) {
			char[][] cfnames = cf.getCompoundName();
			StringBuilder sb = new StringBuilder();
			for (char[] cfname : cfnames) {
				sb.append(cfname);
				sb.append('.');
			}
			if( sb.substring(0,sb.length()-1).equals(name) ) {
				classFile = cf;
			}
		}
		if( classFile == null ) {
			final String str = "Cannot find ClassFile for "+name;
			throw new ClassNotFoundException(str);
		}

		// Define and return the class
		byte[] b = classFile.getBytes();
		Class<?> cl = defineClass(name,b,0,b.length);
		return cl;
	}
}
