/***
 * Juliac
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.compile.jdt;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.eclipse.jdt.internal.compiler.classfmt.ClassFileReader;
import org.eclipse.jdt.internal.compiler.classfmt.ClassFormatException;
import org.eclipse.jdt.internal.compiler.env.INameEnvironment;
import org.eclipse.jdt.internal.compiler.env.NameEnvironmentAnswer;

/**
 * Implementation of the {@link INameEnvironment} interface based on a specified
 * class loader. An {@link INameEnvironment} instance is used by the JDT
 * compiler for retrieving types, compilation units, and packages.
 *
 * @author Lionel <Lionel.Seinturier@univ-lille.fr>
 * @since 2.5
 */
public class NameEnvironmentImpl implements INameEnvironment {

	/** The class loader associated with this instance. */
	private ClassLoader classLoader;

	/** Compilation unit names. */
	private List<String> sources;

	public NameEnvironmentImpl( ClassLoader classLoader, List<String> sources ) {
		this.classLoader = classLoader;
		this.sources = sources;
	}

	@Override
	public void cleanup() {
		// Indeed nothing
	}

	@Override
	public NameEnvironmentAnswer findType( char[][] compoundTypeName ) {
		StringBuilder sb = getQName(compoundTypeName,null);
		return findType(sb);
	}

	@Override
	public NameEnvironmentAnswer findType( char[] typeName, char[][] packageName ) {
		StringBuilder sb = getQName(packageName,typeName);
		return findType(sb);
	}

	private NameEnvironmentAnswer findType( StringBuilder sb ) {
		try {
			return findTypeIOE(sb);
		}
		catch( IOException ioe ) {
			return null;
		}
	}

	private NameEnvironmentAnswer findTypeIOE( StringBuilder sb )
	throws IOException {

		String resourceName = sb.toString().replace('.','/')+".class";
		try( InputStream is = classLoader.getResourceAsStream(resourceName) ) {

			if( is == null ) {
				return null;
			}

			byte[] buf = new byte[8192];
			try( ByteArrayOutputStream baos = new ByteArrayOutputStream(buf.length) ) {

				int len;
				try {
					while( (len = is.read(buf,0,buf.length)) > 0 ) {
						baos.write(buf,0,len);
					}
					baos.flush();
					char[] fileName = sb.toString().toCharArray();
					ClassFileReader cfr =
						new ClassFileReader(baos.toByteArray(),fileName,true);
					String name = new String(cfr.getName())+".class";
					if( ! name.equals(resourceName) ) {
						/*
						 * I ran into a case insensitivity issue when launching Juliac
						 * under Windows/Eclipse (as the issue did not pop up under
						 * Windows/Maven, I do not know for sure where the cause comes
						 * from.) In the fractal-api artifact, the o/o/f/api/type.class
						 * can be loaded even though the real resource name is Type
						 * (type is the a name of the package.) This test is meant to
						 * deal with this issue.
						 */
						return null;
					}
					NameEnvironmentAnswer nea = new NameEnvironmentAnswer(cfr,null);
					return nea;
				}
				catch( ClassFormatException cfe ) {
					return null;
				}
			}
		}
	}

	@Override
	public boolean isPackage( char[][] parentPackageName, char[] packageName ) {

		/*
		 * I tried a different implementation for this method based on an
		 * extension of ClassLoader which exposes the getPackage(String) method
		 * (getPackage is protected in ClassLoader.)
		 *
		 * Yet, this does not work since, at some point, the JDT compiler calls
		 * isPackage with "java" and getPackage("java") returns null. Only
		 * packages with types seem to be reified, e.g. getPackage("java.lang")
		 * returns an instance of a Package.
		 */

		// Check whether this is a type
		StringBuilder sb = getQName(parentPackageName,packageName);
		NameEnvironmentAnswer nea = findType(sb);
		if( nea != null ) {
			return false;
		}

		/*
		 * Check whether this is a compilation unit name.
		 *
		 * JDT invokes this method with names which correspond to source types
		 * not yet compiled, and which thus cannot be loaded by the class
		 * loader. We need to know this in order to return that these names do
		 * not correspond to package names.
		 */
		String name = sb.toString();
		if( sources.contains(name) ) {
			return false;
		}

		return true;
	}


	// --------------------------------------------------------------------
	// Implementation specific
	// --------------------------------------------------------------------

	private static StringBuilder getQName( char[][] packageName, char[] typeName ) {
		StringBuilder sb = new StringBuilder();
		if( packageName != null ) {
			if( typeName != null ) {
				// packageName!=null , typeName!=null
				for (int i = 0; i < packageName.length; i++) {
					sb.append(packageName[i]);
					sb.append('.');
				}
				sb.append(typeName);
			}
			else {
				// packageName!=null , typeName==null
				for (int i = 0; i < packageName.length; i++) {
					if(i>0) sb.append('.');
					sb.append(packageName[i]);
				}
			}
		}
		else {
			if( typeName != null ) {
				// packageName==null , typeName!=null
				sb.append(typeName);
			}
		}
		return sb;
	}
}
