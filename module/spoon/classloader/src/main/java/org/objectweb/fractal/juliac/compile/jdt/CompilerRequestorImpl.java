/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Philippe Merle
 */

package org.objectweb.fractal.juliac.compile.jdt;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jdt.internal.compiler.ClassFile;
import org.eclipse.jdt.internal.compiler.CompilationResult;
import org.eclipse.jdt.internal.compiler.ICompilerRequestor;
import org.eclipse.jdt.internal.compiler.env.ICompilationUnit;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.SourceFileItf;

/**
 * A callback implementation for receiving compilation results.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2.1
 */
public class CompilerRequestorImpl implements ICompilerRequestor {

	private JuliacItf jc;
	private Map<ICompilationUnit,SourceFileItf> units;
	private List<ClassFile> classFiles;

	public CompilerRequestorImpl( JuliacItf jc ) {
		this.jc = jc;
	}

	public void init( Map<ICompilationUnit,SourceFileItf> units ) {
		this.units = units;
		classFiles = new ArrayList<>();
	}

	@Override
	public void acceptResult( CompilationResult result ) {

		/*
		 * Handle problems.
		 */
		if (result.hasProblems()) {
			Logger logger = jc.getLogger();
			String s = getFileName(result);
			IProblem[] pbs = result.getAllProblems();
			for (IProblem pb : pbs) {
				StringBuilder msg = new StringBuilder();
				msg.append(s);
				msg.append(":\n  line ");
				msg.append(pb.getSourceLineNumber());
				msg.append(": ");
				msg.append(pb.getMessage());
				if( pb.isError() ) {
					logger.severe(msg.toString());
				}
				else {
					// Problem is either error or warning (nothing else)
					logger.warning(msg.toString());
				}
			}
		}

		/*
		 * Retrieve class files.
		 */
		ClassFile[] cfs = result.getClassFiles();
		for (ClassFile cf : cfs) {
			classFiles.add(cf);
		}
	}

	public List<ClassFile> getClassFiles() {
		return classFiles;
	}

	/**
	 * Retrieve the name of the file where the problem occurred.
	 *
	 * @since 2.2.4
	 */
	private String getFileName( CompilationResult result ) {

		/*
		 * It happens that the source file may not be found. The cause of
		 * this is not yet clear to me, but a NPE was reported by Sebastien
		 * Jourdain:
		 * http://mail.ow2.org/wws/arc/rntl-sco-2006/2008-08/msg00064.html
		 */
		ICompilationUnit icu = result.compilationUnit;
		SourceFileItf sf = units.get(icu);

		if( sf == null ) {
			StringBuilder msg = new StringBuilder();
			char[][] packageNames = icu.getPackageName();
			if( packageNames != null ) {
				for (char[] packageName : packageNames) {
					msg.append(packageName);
					msg.append('.');
				}
			}
			char[] mainTypeName = icu.getMainTypeName();
			msg.append(mainTypeName);
			msg.append(" (");
			char[] fileName = icu.getFileName();
			msg.append(fileName);
			msg.append(')');
			return msg.toString();
		}
		else {
			/*
			 * sf provides the absolute path to the source file whereas
			 * icu.getFileName() only returns the short name.
			 */
			String s = sf.toString();
			return s;
		}
	}
}
