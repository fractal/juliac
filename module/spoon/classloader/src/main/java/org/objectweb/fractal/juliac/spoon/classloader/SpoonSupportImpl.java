/***
 * Juliac
 * Copyright (C) 2019-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.spoon.classloader;

import java.io.IOException;

import org.objectweb.fractal.juliac.api.JuliacItf;

/**
 * This class implements the services provided by Spoon to Juliac. This class
 * adds the ability to load a class from the Spoon code model.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public class SpoonSupportImpl
extends org.objectweb.fractal.juliac.spoon.SpoonSupportImpl {

	// ----------------------------------------------------------------------
	// Implementation of the JuliacModuleItf interface
	// ----------------------------------------------------------------------

	@Override
	public void init( JuliacItf jc ) throws IOException {

		super.init(jc);

		/*
		 * Let Juliac be able to load classes from the Spoon code model.
		 */
		ClassLoader cl = jc.getClassLoader();
		ClassLoader scl = new SpoonClassLoader(jc,factory,cl);
		jc.setClassLoader(scl);
	}

	@Override
	public void close( JuliacItf jc ) {

		super.close(jc);

		// Remove the Spoon class loader
		ClassLoader cl = jc.getClassLoader();
		ClassLoader parent = cl.getParent();
		jc.setClassLoader(parent);
	}
}
