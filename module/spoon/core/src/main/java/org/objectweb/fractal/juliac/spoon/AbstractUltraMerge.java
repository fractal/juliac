/***
 * Juliac
 * Copyright (C) 2010-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Frederic Loiret
 */

package org.objectweb.fractal.juliac.spoon;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.SpoonSupportItf;
import org.objectweb.fractal.juliac.core.desc.AttributeDesc;
import org.objectweb.fractal.juliac.core.desc.BindingDesc;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;
import org.objectweb.fractal.juliac.spoon.helper.CtConstructorHelper;
import org.objectweb.fractal.juliac.spoon.helper.CtExecutableHelper;
import org.objectweb.fractal.juliac.spoon.helper.CtFieldHelper;
import org.objectweb.fractal.juliac.spoon.helper.CtMethodHelper;
import org.objectweb.fractal.juliac.spoon.helper.CtTypeHelper;
import org.objectweb.fractal.juliac.spoon.helper.CtTypeReferenceHelper;

import spoon.Launcher;
import spoon.reflect.code.CtBlock;
import spoon.reflect.code.CtComment;
import spoon.reflect.code.CtComment.CommentType;
import spoon.reflect.code.CtExpression;
import spoon.reflect.code.CtFieldAccess;
import spoon.reflect.code.CtInvocation;
import spoon.reflect.code.CtJavaDoc;
import spoon.reflect.code.CtJavaDocTag;
import spoon.reflect.code.CtJavaDocTag.TagType;
import spoon.reflect.code.CtLiteral;
import spoon.reflect.code.CtStatement;
import spoon.reflect.code.CtStatementList;
import spoon.reflect.code.CtThisAccess;
import spoon.reflect.code.CtTypeAccess;
import spoon.reflect.cu.SourcePosition;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtConstructor;
import spoon.reflect.declaration.CtElement;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtType;
import spoon.reflect.declaration.ModifierKind;
import spoon.reflect.factory.Factory;
import spoon.reflect.reference.CtArrayTypeReference;
import spoon.reflect.reference.CtExecutableReference;
import spoon.reflect.reference.CtFieldReference;
import spoon.reflect.reference.CtTypeReference;
import spoon.reflect.visitor.CtScanner;
import spoon.reflect.visitor.Query;
import spoon.reflect.visitor.filter.AbstractFilter;

/**
 * Source code generator which merges all component business code in a single
 * class. This class is abstract in the sense that it is independent of the
 * annotation framework used for implementing component business code.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Frederic Loiret <Frederic.Loiret@inria.fr>
 * @since 2.2.4
 */
public abstract class AbstractUltraMerge {

	protected JuliacItf jc;

	public AbstractUltraMerge( JuliacItf jc ) {
		this.jc = jc;
	}

	/**
	 * Generate the source code of a factory class for the specified component
	 * descriptor.
	 *
	 * @param adl         the fully-qualified name of the ADL descriptor
	 * @param targetname  the fully-qualified name of the factory class
	 */
	public void generate( ComponentDesc<?> cdesc, String targetname )
	throws IOException {
		try {
			innergenerate(cdesc,targetname);
		}
		catch( IllegalComponentCodeException icce ) {
			throw new IOException(icce);
		}
	}

	private void innergenerate( ComponentDesc<?> cdesc, String targetname )
	throws IOException, IllegalComponentCodeException {

		// Mergeability checks
		mergeabilityCheck(cdesc);

		// Merge the business code
		CtType<?> type = merge(cdesc,targetname);

		// Dump the generated types
		SpoonSupportItf spoon = jc.lookupFirst(SpoonSupportItf.class);
		spoon.generateSourceCode(type);
	}

	/**
	 * Merge the business code of the specified component descriptor into a
	 * single class. This method returns a list of types containing the merged
	 * {@link CtClass} and all package protected types that are referenced by
	 * the business code and that have been copied in the package of the merged
	 * class in order to be reachable.
	 *
	 * @param root        the component descriptor
	 * @param targetname  the fully-qualified name of the merged class
	 * @return            the created type
	 */
	private CtType<?> merge( ComponentDesc<?> root, String targetname )
	throws IllegalComponentCodeException {

		SpoonSupportItf spoon = jc.lookupFirst(SpoonSupportItf.class);
		Factory factory = spoon.getSpoonFactory();

		/*
		 * Retrieve CtClasses corresponding to component implementations.
		 */
		List<ComponentDesc<?>> primitives = new ArrayList<>();
		List<CtClass<?>> ctclasses = new ArrayList<>();
		parse(root,primitives,ctclasses);

		/*
		 * Create the target class.
		 */
		CtClass<?> target = factory.createClass(targetname);
		target.setVisibility(ModifierKind.PUBLIC);
		CtJavaDoc targetcomment = null;
		for (CtClass<?> ctclass : ctclasses) {
			CtJavaDocTag tag = factory.createJavaDocTag(ctclass.getQualifiedName(),TagType.SEE);
			if( targetcomment == null ) {
				targetcomment = (CtJavaDoc) factory.createComment(
					"@see "+ctclass.getQualifiedName(),CommentType.JAVADOC);
				target.addComment(targetcomment);
			}
			else {
				targetcomment.addTag(tag);
			}
		}

		/*
		 * Rename duplicated fields.
		 */
		renameDuplicateFields(ctclasses);

		/*
		 * Compute duplicated methods.
		 */
		Collection<CtMethod<?>> duplicateMethods = computeDuplicateMethods(ctclasses);

		/*
		 * Rename 'this.' invocations to duplicated methods.
		 */
		for (CtClass<?> ctclass : ctclasses) {
			renameInvocationsToThis(ctclass,duplicateMethods,target);
		}

		/*
		 * Merge CtClasses into target.
		 */
		for (int i = 0; i < ctclasses.size(); i++) {

			ComponentDesc<?> cdesc = primitives.get(i);
			CtClass<?> ctclass = ctclasses.get(i);

			/*
			 * Copy fields.
			 */
			for (CtFieldReference<?> fieldRef : ctclass.getAllFields()) {

				CtField<?> field = fieldRef.getDeclaration();
				CtField<?> dst = factory.createField(target,field);

				// Set the default expression of required fields
				if( isRequiredAnnotatedField(field) ) {
					if( isRequiredCollectionAnnotatedField(field) ) {
						// Collection client interface: initiliaze with a Map
						initRequiredCollectionField(field,cdesc,targetname,duplicateMethods,dst);
						fixExecutableRefStatic(dst,targetname);
					}
					else {
						// Singleton client interface: initialize to this
						CtThisAccess<?> thisaccess = factory.createThisAccess(target.getReference());
						dst.setDefaultExpression((CtExpression)thisaccess);
						renameInvocationsToSingletonFields(field,cdesc,ctclass,duplicateMethods,target);
					}
				}

				// Set attribute values
				if( isAttributeAnnotatedField(field) ) {
					String attributeName = getAttributeName(field);
					AttributeDesc adesc = cdesc.getAttribute(attributeName);
					String value = adesc.getValue();
					CtLiteral<?> lit = CtTypeReferenceHelper.toCtLiteral(field.getType(),value);
					dst.setDefaultExpression((CtLiteral)lit);
				}
			}

			/*
			 * Copy methods.
			 */
			for (CtMethod<?> method : ctclass.getAllMethods()) {

				// Skip methods from j.l.Object and abstract methods
				String declTypeName = method.getDeclaringType().getQualifiedName();
				if( ! declTypeName.equals("java.lang.Object") && ! method.isAbstract() ) {

					String methodName = method.getSimpleName();
					CtMethod<?> dst = null;
					if( duplicateMethods.contains(method) ) {
						// Duplicate method, rename
		    			Set<ModifierKind> mods = method.getModifiers();
						String name = methodName+'_'+declTypeName.replace('.','_');
						dst = factory.createMethod(target,mods,method.getType(),name,method.getParameters(),method.getThrownTypes(),method.getBody());
						logInfo(method.getPosition(),"method "+methodName+" renamed to "+name);
					}
					else {
						dst = factory.createMethod(target,method,true);
					}

			        // Add Javadoc @see comment
					String see = CtMethodHelper.toSeeLink(method);
					dst.addComment(factory.createComment(see,CommentType.JAVADOC));
				}
			}

			/*
			 * Copy constructors.
			 */
			Set<CtConstructor<?>> ctors = ((CtClass)ctclass).getConstructors();
			for (CtConstructor<?> ctor : ctors) {
				CtTypeReference<?>[] parameterTypes =
					CtExecutableHelper.getParameterTypes(ctor);
				CtConstructor<?> dst = target.getConstructor(parameterTypes);
				if( dst == null ) {
					dst = factory.createConstructor(target,ctor);
				}
				else {
					// Already existing constructor, append statements
					CtBlock<?> body = dst.getBody();
					CtStatementList ctstats = factory.createStatementList(ctor.getBody());
					for (CtStatement ctstat : ctstats) {
						body.addStatement(ctstat.clone());
					}
				}

				// Add Javadoc @see tag
				String see = CtConstructorHelper.toSeeLink(ctor);
				Iterator<CtComment> ctcomments =
					dst.getComments().stream().filter(c -> c instanceof CtJavaDoc).iterator();
				if( ctcomments.hasNext() ) {
					while( ctcomments.hasNext() ) {
						CtComment ctcomment = ctcomments.next();
						CtJavaDocTag tag = factory.createJavaDocTag(see.substring(5),TagType.SEE);
						((CtJavaDoc)ctcomment).addTag(tag);
					}
				}
				else {
					dst.addComment(factory.createComment(see,CommentType.JAVADOC));
				}
			}

			/*
			 * Add implemented interfaces.
			 */
			CtTypeReference<?> current = ctclass.getReference();
			do {
				for (CtTypeReference<?> interf : current.getSuperInterfaces()) {
					target.addSuperInterface(interf);
				}
				current = current.getSuperclass();
			}
			while( current != null );
		}

		/*
		 * Add main method.
		 */
		addMainMethod(root,target);

		return target;
	}

	/**
	 * Traverse the specified component descriptor by following bindings and add
	 * the encountered primitive component descriptors and classes to the lists.
	 */
	private void parse( ComponentDesc<?> cdesc, List<ComponentDesc<?>> primitives, List<CtClass<?>> ctclasses )
			throws IllegalComponentCodeException {

		String contentClassName = cdesc.getContentClassName();
		if( contentClassName != null ) {
			SpoonSupportItf spoon = jc.lookupFirst(SpoonSupportItf.class);
			Factory factory = spoon.getSpoonFactory();
			CtClass<?> ctclass = factory.Class().get(contentClassName);
			if( ctclass == null ) {
				String msg = "CtClass not found exception: "+contentClassName;
				throw new IllegalComponentCodeException(msg);
			}
			primitives.add(cdesc);
			ctclasses.add(ctclass);
		}

		for (BindingDesc bdesc : cdesc.getBindingDescs()) {
			ComponentDesc<?> srv = bdesc.getSrv();
			if( ! primitives.contains(srv) ) {
				parse(srv,primitives,ctclasses);
			}
		}
	}


	// ----------------------------------------------------------------------
	// Abstract methods
	// ----------------------------------------------------------------------

	protected abstract String getAttributeName( CtField<?> ctfield );
	protected abstract CtTypeReference<?> getCollectionFieldType( CtField<?> ctfield );
	protected abstract String getRequiresName( CtField<?> ctfield );

	protected abstract void initRequiredCollectionField(
		CtField<?> field, ComponentDesc<?> cdesc, String targetname,
		Collection<CtMethod<?>> duplicateMethods, CtField<?> dst );

	protected abstract boolean isAttributeAnnotatedField( CtField<?> ctfield );
	protected abstract boolean isRequiredAnnotatedField( CtField<?> ctfield );
	protected abstract boolean isRequiredCollectionAnnotatedField( CtField<?> ctfield );


	// ----------------------------------------------------------------------
	// Mergeability check
	// ----------------------------------------------------------------------

	/**
	 * Check that the specified component descriptor can be merged.
	 *
	 * @param cdesc  the component descriptor
	 * @throws IllegalComponentCodeException
	 *      if the the component code cannot be merged
	 */
	protected void mergeabilityCheck( ComponentDesc<?> cdesc )
	throws IllegalComponentCodeException {

		/*
		 * Check whether the Fractal API is used.
		 */
		List<CtClass<?>> ctclasses = new ArrayList<>();
		parse(cdesc,new ArrayList<>(),ctclasses);
		final StringBuilder sb = new StringBuilder();
		for (CtType<?> type : ctclasses) {
			new CtScanner() {
				@Override
				public void scan( CtElement element ) {
					if( element == null ) return;
					if( ! (element instanceof CtTypeReference<?>) ) return;
					String qname = ((CtTypeReference<?>)element).getQualifiedName();
					if( qname.startsWith("org.objectweb.fractal.api.") ||
						qname.startsWith("org.objectweb.fractal.util.") ) {
						sb.append(type.getQualifiedName()+" uses "+qname+". ");
					}
				}
			}.scan(type);
		}
		if( sb.length() != 0 ) {
			sb.append("Illegal usage of the Fractal API in "+cdesc+". ");
			throw new IllegalComponentCodeException(sb.toString());
		}
	}


	// ----------------------------------------------------------------------
	// Steps
	// ----------------------------------------------------------------------

	private Set<CtField<?>> renameDuplicateFields( Collection<CtClass<?>> ctclasses ) {

		Set<CtField<?>> duplicateFields = new HashSet<>();
		Set<String> duplicateFieldNames = new HashSet<>();

		// Compute duplicate fields
		for (CtClass<?> ctclass : ctclasses) {
			for (CtFieldReference<?> fieldRef : ctclass.getAllFields()) {
				CtField<?> field = fieldRef.getDeclaration();
				String fieldName = field.getSimpleName();
				if( duplicateFieldNames.contains(fieldName) ) {
					duplicateFields.add(field);
				}
				else {
					duplicateFieldNames.add(fieldName);
				}
				// Add Javadoc @see comment
				String see = CtFieldHelper.toSeeLink(field);
				field.addComment(ctclass.getFactory().createComment(see,CommentType.JAVADOC));
			}
		}

		// Rename duplicate accesses and fields
		for (CtField<?> field : duplicateFields) {
			String newName =
				field.getSimpleName()+'_'+
				field.getDeclaringType().getQualifiedName().replace('.','_');
			for (CtClass<?> scope : ctclasses) {
				do {
					Query.getElements(
						scope,
						new AbstractFilter<CtFieldAccess<?>>(CtFieldAccess.class) {
							@Override
							public boolean matches( CtFieldAccess<?> fa ) {
								CtFieldReference<?> fref = fa.getVariable();
								CtField<?> f = fref.getFieldDeclaration();
								if( field == f ) {
									logInfo(fa.getPosition(),"field access "+fa+" renamed to "+newName);
									fref.setSimpleName(newName);
								}
								return false;
							}
						}
					);
					scope = scope.getSuperclass() == null ? null :
						(CtClass<?>) scope.getSuperclass().getDeclaration();
				}
				while( scope != null );
			}
			logInfo(field.getPosition(),"field "+field.getSimpleName()+" renamed to "+newName);
			field.setSimpleName(newName);
		}

		return duplicateFields;
	}

	private Collection<CtMethod<?>> computeDuplicateMethods( Collection<CtClass<?>> ctclasses ) {

		/*
		 * The implementation of contains in HashSet compares the hash code of
		 * the objects. Spoon hashes the definition of the method. Two different
		 * methods with the same definition share the same hash. We need here to
		 * compare two different methods and not their definition.
		 */
		Collection<CtMethod<?>> duplicateMethods = new ArrayList<>() {
			private static final long serialVersionUID = -6741976565035663982L;
			@Override
			public boolean contains( Object o ) {
				for( int i=0 ; i < size() ; i++ ) {
					if( get(i) == o ) {
						return true;
					}
				}
				return false;
			}
		};
		Set<String> duplicateMethodSignatures = new HashSet<>();

		// Compute duplicated methods
		for (CtClass<?> ctclass : ctclasses) {
			for (CtMethod<?> method : ctclass.getAllMethods()) {

				// Skip methods from j.l.Object and abstract methods
				String declTypeName = method.getDeclaringType().getQualifiedName();
				if( ! declTypeName.equals("java.lang.Object") && ! method.isAbstract() ) {

			        String sig = method.getSignature();
			        if( duplicateMethodSignatures.contains(sig) ) {
			        	duplicateMethods.add(method);
			        }
			        else {
			        	duplicateMethodSignatures.add(sig);
			        }
				}
			}
		}

		return duplicateMethods;
	}

	private void renameInvocationsToSingletonFields(
		CtField<?> field, ComponentDesc<?> cdesc, CtClass<?> ctclass,
		Collection<CtMethod<?>> duplicateMethods, CtClass<?> merged ) {

		Query.getElements(
			ctclass,
			new AbstractFilter<CtInvocation<?>>(CtInvocation.class) {
				@Override
				public boolean matches( CtInvocation<?> inv ) {
					CtExpression<?> target = inv.getTarget();
					if( target instanceof CtFieldAccess<?> ) {
						CtField<?> f = ((CtFieldAccess<?>)target).getVariable().getDeclaration();
						if( field == f ) {
							// Retrieve the server component bound to the current client interface
							String cltItfName = getRequiresName(field);
							ComponentDesc<?> srv = cdesc.getBoundPrimitiveComponent(cltItfName);
							if( srv != null ) {
								String srvContentClassName = srv.getContentClassName();
								CtClass<?> srvclass = ctclass.getFactory().Class().get(srvContentClassName);
								renameInvocation(inv,srvclass,ctclass,duplicateMethods,merged);
							}
						}
					}
					return false;
				}
			}
		);
	}

	private void renameInvocationsToThis(
		CtClass<?> ctclass, Collection<CtMethod<?>> duplicateMethods, CtClass<?> merged ) {

		do {
			final CtClass<?> scope = ctclass;
			Query.getElements(
					scope,
				new AbstractFilter<CtInvocation<?>>(CtInvocation.class) {
					@Override
					public boolean matches( CtInvocation<?> inv ) {
						CtExpression<?> target = inv.getTarget();
						if( target instanceof CtThisAccess<?> ) {
							renameInvocation(inv,scope,scope,duplicateMethods,merged);
						}
						return false;
					}
				}
			);
			ctclass = ctclass.getSuperclass() == null ? null :
				(CtClass<?>) ctclass.getSuperclass().getDeclaration();
		}
		while( ctclass != null );
	}

	private void renameInvocation(
		CtInvocation<?> inv, CtClass<?> srvclass, CtClass<?> ctclass,
		Collection<CtMethod<?>> duplicateMethods, CtClass<?> merged ) {

		CtExecutableReference<?> execRef = inv.getExecutable();
		String methodName = execRef.getSimpleName();
		CtMethod<?> srvmethod =
			CtTypeHelper.getDeclaredOrInheritedMethod(
				srvclass,methodName,execRef.getParameters().toArray(new CtTypeReference[0]));
		if( srvmethod != null ) {

			// Replace the field access with type.this.
			logInfo(inv.getPosition(),"specifying "+merged.getQualifiedName()+".this. for "+inv.getTarget()+'.'+execRef.getSimpleName());
			CtTypeAccess<?> ctta = ctclass.getFactory().createTypeAccess();
			ctta.setAccessedType((CtTypeReference)merged.getReference());
			CtThisAccess<?> ctthisa = ctclass.getFactory().createThisAccess();
			ctthisa.setTarget(ctta);
			inv.setTarget(ctthisa);

			// Rename the invoked method if needed
			if( duplicateMethods.contains(srvmethod) ) {
				String newName =
					execRef.getSimpleName()+'_'+
					srvclass.getQualifiedName().replace('.','_');
				logInfo(inv.getPosition(),"invocation "+inv.getTarget()+'.'+execRef.getSimpleName()+" renamed to "+newName);
				execRef.setSimpleName(newName);
			}
		}
	}

	/**
	 * This method implements a workaround for the implementation of Spoon
	 * starting on 14 June 2019 with commit
	 * https://github.com/INRIA/spoon/commit/341c2428b0735a5cb2912513b79485982f9bfc81
	 * in file
	 * https://github.com/INRIA/spoon/commit/341c2428b0735a5cb2912513b79485982f9bfc81#diff-615506b427fda5a1b15c17caf7786ae2
	 * on line 457 with ref.setStatic(true);
	 *
	 * This workaround sets static to false for targeted this accesses that are
	 * encountered in default expressions for collection fields such as
	 *
	 * <code>
	 * public java.util.List<example.hw.PrinterItf> ms = new java.util.ArrayList() {
	 * {
	 *   add(new example.hw.PrinterItf() {
	 *     public void print(java.lang.String msg) {
	 *       HelloWorld.this.print(msg);
	 *     }
	 *   });
	 * }
	 * </code>
	 *
	 * Spoon assumes that HelloWorld.this.print is a call to a static method
	 * since HelloWorld#print is not defined here. Prior to the aforementioned
	 * commit that was not the case.
	 *
	 * @since 2.8
	 */
	private void fixExecutableRefStatic( CtField<?> dst, String targetname ) {
		Query.getElements(
			dst,
			new AbstractFilter<CtThisAccess<?>>(CtThisAccess.class) {
				@Override
				public boolean matches( CtThisAccess<?> ta ) {
					if( ta.getTarget().toString().equals(targetname) ) {
						ta.getParent(CtInvocation.class).getExecutable().setStatic(false);
					}
					return false;
				}
			}
		);
	}


	// ----------------------------------------------------------------------
	// Add main method
	// ----------------------------------------------------------------------

	/**
	 * If the specified component descriptor provides a
	 * <code>java.lang.Runnable/r</code> server interface and if there is not
	 * already a <code>main(String[])</code> method in the merged class, add one
	 * to launch the application.
	 *
	 * @param cdesc    the component descriptor
	 * @param ctclass  the merged class
	 */
	private void addMainMethod( ComponentDesc<?> cdesc, CtClass<?> ctclass ) {

		/*
		 * Check whether the component descriptor provides a
		 * java.lang.Runnable/r server interface.
		 */
		ComponentType ct = cdesc.getCT();
		try {
			InterfaceType it = ct.getFcInterfaceType("r");
			if( it.isFcClientItf() ) {
				// Not a server interface. Do nothing.
				return;
			}
			String signature = it.getFcItfSignature();
			if( ! signature.equals(Runnable.class.getName()) ) {
				// Not a java.lang.Runnable interface. Do nothing.
				return;
			}
		}
		catch (NoSuchInterfaceException e) {
			// No r interface. Do nothing.
			return;
		}

		/*
		 * Check whether there is already a main(String[]) method.
		 * If not, add one.
		 */
		CtArrayTypeReference<?> trefstringarray =
			ctclass.getFactory().createArrayReference(String.class.getName());
		CtMethod<?> method = ctclass.getMethod("main",trefstringarray);
		if( method == null ) {
			// Create a main(String[]) method
			StringBuilder sb = new StringBuilder();
			sb.append("class C {");
			sb.append("public static void main(String[] args) {");
			sb.append("Runnable r = new ");
			sb.append(ctclass.getQualifiedName());
			sb.append("(); r.run(); }}");
			CtClass<?> c = Launcher.parseClass(sb.toString());
			CtMethod<?> main = c.getMethod("main",trefstringarray);
			ctclass.addMethod(main);
		}
	}


	// ----------------------------------------------------------------------
	// Utility methods
	// ----------------------------------------------------------------------

	/** @since 2.8 */
	private void logInfo( SourcePosition sp, String message ) {
		if( sp.getFile() == null ) {
			// sp.getFile() is null when using the Spoon noclasspath mode
			jc.getLogger().info("(null:"+sp.getLine()+") "+message);
		}
		else {
			jc.getLogger().info("("+sp.getFile()+':'+sp.getLine()+") "+message);
		}
	}
}
