/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.spoon.helper;

import spoon.reflect.cu.SourcePosition;
import spoon.reflect.declaration.CtNamedElement;

/**
 * This class provides some helper methods for dealing with
 * {@link CtNamedElement}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.2
 */
public class CtNamedElementHelper {

	/**
	 * Return a string of the form <code>typename.name(filename:line)</code> for
	 * the specified {@link CtNamedElement}. When displayed, such a string can
	 * be interpreted by Eclipse as a link to a source code position.
	 */
	public static String toEclipseClickableString( CtNamedElement ne ) {
		StringBuilder msg = new StringBuilder();
		SourcePosition sp = ne.getPosition();
		msg.append(sp.getCompilationUnit().getMainType().getQualifiedName());
		msg.append('.');
		msg.append(ne.getSimpleName());
		msg.append('(');
		msg.append(sp.getFile().getName());
		msg.append(':');
		msg.append( sp.getLine());
		msg.append(')');
		return msg.toString();
	}
}
