/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.spoon.helper;

import java.util.ArrayList;
import java.util.List;

import spoon.reflect.declaration.CtClass;
import spoon.reflect.factory.ClassFactory;
import spoon.reflect.factory.Factory;

/**
 * This class provides some helper methods for dealing with {@link CtClass}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.2
 */
public class CtClassHelper {

	/**
	 * Return the list of {@link CtClass}es corresponding to the given list of
	 * class names.
	 *
	 * @throws IllegalArgumentException
	 *      if one of the {@link CtClass}es cannot be retrieved
	 */
	public static List<CtClass<?>> toCtClass( Factory f, List<String> srcs ) {

		List<CtClass<?>> results = new ArrayList<>();
		ClassFactory cf = f.Class();

		for (String src : srcs) {
			CtClass<?> ctclass = cf.get(src);
			if( ctclass == null ) {
				final String msg = "No such CtClass: "+src;
				throw new IllegalArgumentException(msg);
			}
			results.add(ctclass);
		}

		return results;
	}
}
