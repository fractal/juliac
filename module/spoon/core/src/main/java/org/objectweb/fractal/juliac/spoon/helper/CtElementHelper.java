/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.spoon.helper;

import spoon.reflect.declaration.CtElement;
import spoon.reflect.reference.CtPackageReference;
import spoon.reflect.reference.CtTypeReference;
import spoon.reflect.visitor.Query;
import spoon.reflect.visitor.filter.TypeFilter;

/**
 * This class provides some helper methods for dealing with {@link CtElement}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.2
 */
public class CtElementHelper {

	/**
	 * Update all source type references in the specified element to the
	 * specified target type reference.
	 *
	 * @param element  the element
	 * @param src      the source type reference
	 * @param dst      the target type reference
	 */
	public static void updateTypeReferences(
		CtElement element, CtTypeReference<?> src, CtTypeReference<?> dst ) {

		final String srcQName = src.getQualifiedName();
		final String dstSName = dst.getSimpleName();
		final CtPackageReference dstPRef = dst.getPackage();

		Query.getElements(
			element,
			new TypeFilter<CtTypeReference<?>>(CtTypeReference.class) {
				@Override
				public boolean matches( CtTypeReference<?> tref ) {
					String qname = tref.getQualifiedName();
					if( qname.equals(srcQName) ) {
						tref.setPackage(dstPRef);
						tref.setSimpleName(dstSName);
					}
					return false;
				}
			}
		);
	}
}
