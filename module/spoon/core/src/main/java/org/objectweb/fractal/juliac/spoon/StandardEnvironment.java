/***
 * Juliac
 * Copyright (C) 2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.spoon;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import spoon.SpoonException;

/**
 * This class provides a custom Spoon Environment for Spoon.
 * 
 * This class was added after commit
 * https://github.com/INRIA/spoon/commit/7dc7b2c2fb41149a921906c8581cc55d44a74ca6
 * that broke Juliac https://ci.inria.fr/sos/job/juliac/332/consoleText
 * 
 * This class re-introduces the implementation that was used prior to the
 * commit. A smarter fix should be implemented to figure out Spoon classloader
 * should be used by Juliac.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class StandardEnvironment extends spoon.support.StandardEnvironment {

	private static final long serialVersionUID = 1L;

	private transient ClassLoader classloader;
	private transient ClassLoader inputClassloader;

	@Override
	public void setInputClassLoader(ClassLoader aClassLoader) {
		if (aClassLoader instanceof URLClassLoader) {
			final URL[] urls = ((URLClassLoader) aClassLoader).getURLs();
			if (urls != null && urls.length > 0) {
				// Check that the URLs are only file URLs
				boolean onlyFileURLs = true;
				for (URL url : urls) {
					if (!"file".equals(url.getProtocol())) {
						onlyFileURLs = false;
					}
				}
				if (onlyFileURLs) {
					List<String> classpath = new ArrayList<>();
					for (URL url : urls) {
						classpath.add(url.getPath());
					}
					setSourceClasspath(classpath.toArray(new String[0]));
				} else {
					throw new SpoonException("Spoon does not support a URLClassLoader containing other resources than local file.");
				}
			}
			return;
		}
		this.classloader = aClassLoader;
	}

	@Override
	public ClassLoader getInputClassLoader() {
		if (classloader != null) {
			return classloader;
		}
		if (inputClassloader == null) {
			inputClassloader = new URLClassLoader(urlClasspath(), Thread.currentThread().getContextClassLoader());
		}
		return inputClassloader;
	}
}
