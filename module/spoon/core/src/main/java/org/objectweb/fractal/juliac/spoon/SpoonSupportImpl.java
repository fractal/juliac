/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Frederic Loiret
 */

package org.objectweb.fractal.juliac.spoon;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.SpoonSupportItf;
import org.objectweb.fractal.juliac.core.SourceFileDir;
import org.objectweb.fractal.juliac.spoon.helper.CtClassHelper;

import spoon.SpoonModelBuilder;
import spoon.compiler.Environment;
import spoon.compiler.SpoonResourceHelper;
import spoon.processing.FileGenerator;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtNamedElement;
import spoon.reflect.declaration.CtPackage;
import spoon.reflect.declaration.CtType;
import spoon.reflect.factory.CoreFactory;
import spoon.reflect.factory.Factory;
import spoon.reflect.factory.FactoryImpl;
import spoon.reflect.visitor.DefaultJavaPrettyPrinter;
import spoon.reflect.visitor.PrettyPrinter;
import spoon.support.DefaultCoreFactory;
import spoon.support.JavaOutputProcessor;
import spoon.support.compiler.jdt.JDTBasedSpoonCompiler;

/**
 * This class implements the services provided by Spoon to Juliac.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Frederic Loiret <Frederic.Loiret@inria.fr>
 * @since 1.3
 */
public class SpoonSupportImpl implements SpoonSupportItf {

    // ----------------------------------------------------------------------
    // Implementation of the JuliacModuleItf interface
    // ----------------------------------------------------------------------
    
	public static final Class<SpoonSupportItf> SERVICE_TYPE =
        SpoonSupportItf.class;
    
    private JuliacItf jc;
    protected Factory factory;

    /*
     * (non-Javadoc)
     * @see org.objectweb.fractal.juliac.api.JuliacModuleItf#init(org.objectweb.fractal.juliac.Juliac)
     */
    @Override
    public void init( JuliacItf jc ) throws IOException {
        this.jc = jc;
        jc.register(SERVICE_TYPE,this);
        
        /*
         * Create the Spoon factory.
         */
        Environment env = new StandardEnvironment();
        env.setNoClasspath(true);
        env.setCommentEnabled(true);
        env.setLevel("ERROR");
        CoreFactory coref = new DefaultCoreFactory();
        factory = new FactoryImpl(coref,env);
        setSpoonInputClassLoader(jc.getClassLoader());

        /*
         * Feed Spoon with the source and library files.
         */
		SpoonModelBuilder smb = new JDTBasedSpoonCompiler(factory);
        File baseDir = jc.getBaseDir();
        
        for (String src : jc.getSrcs()) {
        	File f = new File(baseDir,src);
        	if(!f.exists()) {
        		// Handle the case where src is an absolute path
        		f = new File(src);
        	}
    		smb.addInputSource(SpoonResourceHelper.createResource(f));
        }        
        for (String srclib : jc.getSrclibs()) {
        	File f = new File(baseDir,srclib);
        	if(!f.exists()) {
        		f = new File(srclib);
        	}
    		smb.addInputSource(SpoonResourceHelper.createResource(f));
        }
        
        /*
         * Build the Spoon code model.
         */
		smb.build();
    }

    /*
     * (non-Javadoc)
     * @see org.objectweb.fractal.juliac.api.JuliacModuleItf#close(org.objectweb.fractal.juliac.Juliac)
     */
    @Override
    public void close( JuliacItf jc ) {
        
        // Unregister the Spoon service
        jc.unregister(SERVICE_TYPE,this);
    }
    

    // ----------------------------------------------------------------------
    // Implementation of the SpoonSupportItf interface
    // ----------------------------------------------------------------------

    /*
     * (non-Javadoc)
     * @see org.objectweb.fractal.juliac.api.SpoonSupportItf#mixAndGenerate(java.lang.String, java.util.List)
     */
    @Override
    public void mixAndGenerate( String targetClassName, List<String> mixins )
    throws IOException {
                
        List<CtClass<?>> layers = CtClassHelper.toCtClass(factory,mixins);
        MixinClassGenerator mcg = new MixinClassGenerator(factory);
        CtClass<?> generated = mcg.generate(targetClassName,layers);                

        // Dump the generated class
        generateSourceCode(generated);
    }
    
    /*
     * (non-Javadoc)
     * @see org.objectweb.fractal.juliac.api.SpoonSupportItf#mix(java.lang.String, java.util.List, java.io.Writer)
     */
    @Override
    public void mix(
        String targetClassName, List<String> mixins, Writer writer )
    throws IOException {
        
        List<CtClass<?>> layers = CtClassHelper.toCtClass(factory,mixins);
        MixinClassGenerator mcg = new MixinClassGenerator(factory);
        CtClass<?> element = mcg.generate(targetClassName,layers);                

        Environment env = factory.getEnvironment();
        DefaultJavaPrettyPrinter printer = new DefaultJavaPrettyPrinter(env);

        CtPackage pack = element.getPackage();
        if( pack != null ) {
            printer.scan(pack);
        }
        printer.scan(element);
        String content = printer.getResult();

        writer.write(content);
    }
    
    /*
     * (non-Javadoc)
     * @see org.objectweb.fractal.juliac.api.SpoonSupportItf#getSpoonFactory()
     */
    @SuppressWarnings("unchecked")
    @Override
    public Factory getSpoonFactory() {
        return factory;
    }
    
    /*
     * (non-Javadoc)
     * @see org.objectweb.fractal.juliac.api.SpoonSupportItf#setSpoonInputClassLoader(java.lang.ClassLoader)
     */
    @Override
    public void setSpoonInputClassLoader( ClassLoader classLoader ) {
    	
    	/*
    	 * Spoon checks that URLClassLoaders do not contain any non existing
    	 * directory and throws InvalidClassPathException otherwise. See
    	 * StandardEnvironment#verifySourceClasspath.
    	 * 
    	 * When invoked from Maven, target/classes appears in the classpath,
    	 * although the directory does not exists. I encountered the case among
    	 * others in extension/adlet/module/opt-mergectrl/mixed.
    	 * 
    	 * To prevent Spoon from throwing InvalidClassPathException, I build an
    	 * URLClassLoader with no non existing directory.
    	 */
    	URLClassLoader urlcl = getURLClassLoaderWithOnlyExistingFiles(classLoader);
    	if( urlcl != null ) {
    		classLoader = urlcl;
    	}
        factory.getEnvironment().setInputClassLoader(classLoader);
    }

    /*
     * (non-Javadoc)
     * @see org.objectweb.fractal.juliac.api.SpoonSupportItf#generateSourceCode(java.lang.Object)
     */
    @Override
    public void generateSourceCode( Object o ) throws IOException {
        
        if( !( o instanceof CtType) ) {
            final String msg = o+" should implement "+CtType.class.getName();
            throw new IllegalArgumentException(msg);
        }
        CtType<?> element = (CtType<?>) o;
        
        File genDir = jc.getGenDir();
        
        Environment env = factory.getEnvironment();
        env.setSourceOutputDirectory(genDir);
        PrettyPrinter pp = new DefaultJavaPrettyPrinter(env);
        FileGenerator<CtNamedElement> jop = new JavaOutputProcessor(pp);
        jop.setFactory(factory);
        
        jop.process(element);

        String name = element.getQualifiedName();
        List<File> files = jop.getCreatedFiles();
        if( files.size() != 1 ) {
            final String msg =
                "Only one source was expected. Got "+files.size()+" for "+name+
                ".";
            throw new IOException(msg);
        }
        
        URI uri = files.get(0).toURI();
        jc.addGenerated(new SourceFileDir(uri,name,"java"),null);
    }

    
    // ----------------------------------------------------------------------
    // Implementation specific
    // ----------------------------------------------------------------------

    /**
     * If the specified ClassLoader is an URLCLassLoader and if the
     * URLClassLoader contains file URLs that do not exist, returns an
     * URLClassLoader with only URLs that exist and the same parent ClassLoader
     * as the one of the specified ClassLoader. In all other cases, returns
     * <code>null</code>.
     * 
     * @since 2.8
     */
    private URLClassLoader getURLClassLoaderWithOnlyExistingFiles( ClassLoader classLoader ) {

    	if( classLoader instanceof URLClassLoader ) {
    		URL[] urls = ((URLClassLoader) classLoader).getURLs();
			List<URL> lurls = new ArrayList<>();
    		boolean onlyFileURLs=true, nonExistingFiles=false;
    		for (URL url : urls) {
				if( url.getProtocol().equals("file") ) {
					String path = url.getPath();
					if( new File(path).exists() ) {
						lurls.add(url);
					}
					else {
						nonExistingFiles = true;
					}
				}
				else {
					onlyFileURLs = false;
				}
			}
    		if( onlyFileURLs && nonExistingFiles ) {
    			return
					new URLClassLoader(
						lurls.toArray(new URL[lurls.size()]),
						classLoader.getParent() );
    		}
    	}
    	
    	return null;
    }
}
