/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.spoon.helper;

import java.util.ArrayList;
import java.util.List;

import spoon.reflect.declaration.CtExecutable;
import spoon.reflect.declaration.CtParameter;
import spoon.reflect.reference.CtExecutableReference;
import spoon.reflect.reference.CtTypeReference;

/**
 * This class provides some helper methods for dealing with
 * {@link CtExecutableReference}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class CtExecutableReferenceHelper {

	/**
	 * Return the type references of the parameters associated with the
	 * specified executable reference. Return <code>null</code> if the
	 * executable has not been parsed by Spoon.
	 *
	 * @param cer  the executable reference
	 * @return     the parameter types
	 */
	public static List<CtTypeReference<?>>
	getParameterTypes( CtExecutableReference<?> cer ) {

		CtExecutable<?> exec = cer.getDeclaration();
		if( exec == null ) {
			/*
			 * Declaration is null when it has not been parsed by Spoon, e.g.
			 * when the executable is in a classpath library. Returning
			 * cer.getParameters() is not correct in all cases since the types
			 * of the parameters used when invoking the executable may be
			 * subtypes of the ones that have been declared with the source code
			 * of the executable.
			 */
			return null;
		}

		List<CtParameter<?>> params = exec.getParameters();
		List<CtTypeReference<?>> trefs = new ArrayList<>();
		for (CtParameter<?> param : params) {
			CtTypeReference<?> tref = param.getType();
			trefs.add(tref);
		}
		return trefs;
	}

	/**
	 * Return a Javadoc like string representing the specified executable
	 * reference.
	 *
	 * This method has been added following a change in SignaturePrinter that
	 * used to return such a String, and that no longer does so since Spoon 6.0
	 * and commit
	 * https://github.com/INRIA/spoon/commit/a4e9c9ca39d25a018f651a948139525409c41369
	 * on 2 October 2017
	 *
	 * @since 2.7
	 */
	public static String toJavadocString( CtExecutableReference<?> cer ) {

		StringBuilder sb = new StringBuilder();

		sb.append(cer.getDeclaringType().toString());
		sb.append(CtExecutable.EXECUTABLE_SEPARATOR);
		if (cer.isConstructor()) {
			sb.append(cer.getDeclaringType().getSimpleName());
		} else {
			sb.append(cer.getSimpleName());
		}
		sb.append("(");
		if (cer.getParameters().size() > 0) {
			boolean first = true;
			for (CtTypeReference<?> param : cer.getParameters()) {
				if(first) {
					first = false;
				}
				else {
					sb.append(", ");
				}
				if (param != null && !"null".equals(param.getSimpleName())) {
					sb.append(param);
				} else {
					sb.append(CtExecutableReference.UNKNOWN_TYPE);
				}
			}
		}
		sb.append(")");

		return sb.toString();
	}
}
