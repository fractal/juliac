/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.spoon.helper;

import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtTypeReference;

/**
 * This class provides some helper methods for dealing with {@link CtType}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2.4
 */
public class CtTypeHelper {

	/**
	 * Return the method corresponding to the specified name and parameter
	 * types, either declared or inherited by the specified type. Return
	 * <code>null</null> if the method does not exist.
	 *
	 * @since 2.8
	 */
	public static CtMethod<?> getDeclaredOrInheritedMethod(
		CtType<?> type, String name, CtTypeReference<?>... parameterTypes ) {

		CtMethod<?> method = type.getMethod(name,parameterTypes);
		if( method == null ) {
			type = type.getSuperclass() == null ? null : type.getSuperclass().getDeclaration();
			if( type != null ) {
				method = getDeclaredOrInheritedMethod(type,name,parameterTypes);
			}
		}

		return method;
	}

	/**
	 * Return a string containing a @see link to reference the specified simple
	 * type.
	 */
	public static String toSeeLink( CtType<?> stype ) {
		String qname = stype.getQualifiedName();
		StringBuilder comment = new StringBuilder("@see ");
		comment.append(qname);
		return comment.toString();
	}
}
