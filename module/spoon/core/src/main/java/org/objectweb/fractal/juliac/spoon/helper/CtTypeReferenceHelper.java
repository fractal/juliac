/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.spoon.helper;

import spoon.reflect.code.CtLiteral;
import spoon.reflect.factory.CodeFactory;
import spoon.reflect.reference.CtPackageReference;
import spoon.reflect.reference.CtTypeReference;

/**
 * This class provides some helper methods for dealing with
 * {@link CtTypeReference}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.2
 */
public class CtTypeReferenceHelper {

	/**
	 * Return the 0-value literal for the given type reference.
	 */
	public static CtLiteral<?> nil( CtTypeReference<?> ctr ) {

		CodeFactory cf = ctr.getFactory().Code();
		String qname = ctr.getQualifiedName();

		if( qname.equals("boolean") )
			return cf.createLiteral(false);
		if( qname.equals("byte") )
			return cf.createLiteral((byte)0);
		if( qname.equals("char") )
			return cf.createLiteral(' ');
		if( qname.equals("short") )
			return cf.createLiteral((short)0);
		if( qname.equals("int") )
			return cf.createLiteral((int)0);
		if( qname.equals("long") )
			return cf.createLiteral((long)0);
		if( qname.equals("float") )
			return cf.createLiteral((float)0.0);
		if( qname.equals("double") )
			return cf.createLiteral((double)0.0);

		return cf.createLiteral(null);
	}

	/**
	 * Return the literal for the given type reference.
	 */
	public static CtLiteral<?> toCtLiteral( CtTypeReference<?> ctr, String value ) {

		CodeFactory cf = ctr.getFactory().Code();
		String qname = ctr.getQualifiedName();

		if( qname.equals("boolean") )
			return cf.createLiteral(Boolean.valueOf(value));
		if( qname.equals("byte") )
			return cf.createLiteral(Byte.valueOf(value));
		if( qname.equals("char") ) {
			if( value.length() != 1 ) {
				final String msg =
					"Only 1-length value String supported for the char type";
				throw new IllegalArgumentException(msg);
			}
			return cf.createLiteral(value.charAt(0));
		}
		if( qname.equals("short") )
			return cf.createLiteral(Short.valueOf(value));
		if( qname.equals("int") )
			return cf.createLiteral(Integer.valueOf(value));
		if( qname.equals("long") )
			return cf.createLiteral(Long.valueOf(value));
		if( qname.equals("float") )
			return cf.createLiteral(Float.valueOf(value));
		if( qname.equals("double") )
			return cf.createLiteral(Double.valueOf(value));
		if( qname.equals(String.class.getName()) )
			return cf.createLiteral(value);

		final String msg = "Unsupported type: "+ctr.getQualifiedName();
		throw new IllegalArgumentException(msg);
	}

	/**
	 * Return the package reference associated to the specified type. This
	 * method complements {@link CtTypeReference#getPackage()} which returns
	 * <code>null</code> for inner types. For the inner types, this method
	 * returns the package reference of the top level type declaring the
	 * specified type.
	 *
	 * This method returns <code>null</code> if the specified type is inner and
	 * does not have a declaring type with a package reference.
	 *
	 * @param tref  the type reference
	 * @return      the reference of the package declaring the type
	 * @since 2.2.4
	 */
	public static CtPackageReference getPackage( CtTypeReference<?> tref ) {
		CtPackageReference pref = tref.getPackage();
		while( pref == null ) {
			tref = tref.getDeclaringType();
			if( tref == null ) {
				// No declaring type. Then no package neither.
				return null;
			}
			pref = tref.getPackage();
		}
		return pref;
	}
}
