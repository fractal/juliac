/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.spoon.helper;

import java.util.List;

import spoon.reflect.declaration.CtExecutable;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtParameter;
import spoon.reflect.reference.CtTypeReference;

/**
 * This class provides some helper methods for dealing with
 * {@link CtExecutable}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.2
 */
public class CtExecutableHelper {

	/**
	 * Return the types of the parameters for the specified {@link CtExecutable}
	 * (e.g. such as a {@link CtMethod}).
	 */
	public static CtTypeReference<?>[] getParameterTypes( CtExecutable<?> exec ) {
		List<CtParameter<?>> params = exec.getParameters();
		CtTypeReference<?>[] parameterTypes =
			new CtTypeReference<?>[params.size()];
		int i=0;
		for (CtParameter<?> param : params) {
			CtTypeReference<?> tref = param.getType();
			parameterTypes[i] = tref;
			i++;
		}
		return parameterTypes;
	}
}
