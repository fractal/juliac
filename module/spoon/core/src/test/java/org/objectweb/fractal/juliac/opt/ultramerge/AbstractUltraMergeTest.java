/***
 * Juliac
 * Copyright (C) 2019-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Frederic Loiret
 */

package org.objectweb.fractal.juliac.opt.ultramerge;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.util.Collection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.type.BasicComponentType;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.SpoonSupportItf;
import org.objectweb.fractal.juliac.core.Juliac;
import org.objectweb.fractal.juliac.core.desc.BindingDesc;
import org.objectweb.fractal.juliac.core.desc.BindingType;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;
import org.objectweb.fractal.juliac.spoon.AbstractUltraMerge;
import org.objectweb.fractal.juliac.spoon.SpoonSupportImpl;

import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.factory.Factory;
import spoon.reflect.reference.CtTypeReference;

/**
 * Class for testing the functionalities of {@link AbstractUltraMerge}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public class AbstractUltraMergeTest {

	class MockUltraMerge extends AbstractUltraMerge {
		MockUltraMerge( JuliacItf jc ) { super(jc); }
		@Override protected String getAttributeName(CtField<?> ctfield) { throw new UnsupportedOperationException(); }
		@Override protected CtTypeReference<?> getCollectionFieldType(CtField<?> ctfield) { throw new UnsupportedOperationException(); }
		@Override protected String getRequiresName(CtField<?> ctfield) { return ctfield.getSimpleName(); }
		@Override protected void initRequiredCollectionField(CtField<?> field, ComponentDesc<?> cdesc, String targetname, Collection<CtMethod<?>> duplicateMethods, CtField<?> dst) { throw new UnsupportedOperationException(); }
		@Override protected boolean isAttributeAnnotatedField(CtField<?> ctfield) { return false; }
		@Override protected boolean isRequiredAnnotatedField(CtField<?> ctfield) { return true; }
		@Override protected boolean isRequiredCollectionAnnotatedField(CtField<?> ctfield) { return false; }
	}

	private ComponentType ct0, ct;
	private ComponentDesc<CtClass<?>> cd0;

	@BeforeEach
	public void setUp() throws InstantiationException {
		ct0 = new BasicComponentType(new InterfaceType[]{
			new BasicInterfaceType("r","java.util.Runnable",false,false,false) });
		ct = new BasicComponentType(new InterfaceType[]{
			new BasicInterfaceType("r","java.util.Runnable",false,false,false),
			new BasicInterfaceType("c","java.util.Runnable",true,false,false) });
		cd0 = new ComponentDesc<>("C0","C0","C0",ct0,"composite",null,null);
	}

	@Test
	public void testDuplicateMethods() throws IOException {

		JuliacItf jc = new Juliac();
		jc.addSrc("src/test/resources");

		SpoonSupportItf spoon = new SpoonSupportImpl();
		spoon.init(jc);
		Factory f = spoon.getSpoonFactory();

		ComponentDesc<CtClass<?>> cd1 = getCDesc("C1",ct,f);
		ComponentDesc<CtClass<?>> cd2 = getCDesc("C2",ct0,f);

		cd0.addSubComponent(cd1);
		cd0.addSubComponent(cd2);
		cd0.putBinding("r",new BindingDesc(BindingType.EXPORT,cd0,"r",cd1,"r"));
		cd1.putBinding("c",new BindingDesc(BindingType.NORMAL,cd1,"c",cd2,"r"));

		AbstractUltraMerge aum = new MockUltraMerge(jc);
		aum.generate(cd0,"DuplicateMethodsUM");

		CtClass<?> um = f.Class().get("DuplicateMethodsUM");
		assertEquals(3,um.getDeclaredExecutables().size());
		assertNotNull(um.getMethod("run_C2"));

		spoon.close(jc);
		jc.close();
	}

	private ComponentDesc<CtClass<?>> getCDesc( String id, ComponentType ct, Factory f ) {
		return new ComponentDesc<>(id,id,id,ct,"primitive",id,f.Class().get(id));
	}
}
