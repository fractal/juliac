/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.runtime;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.TypeFactory;

/**
 * The Fractal provider class for Juliac.
 *
 * This Fractal provider returns a bootstrap component with a type-factory and
 * a generic-factory interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class Juliac implements Factory, GenericFactory {

	/**
	 * The package prefix used by Juliac for code which special needs. This
	 * includes:
	 *
	 * <ul>
	 * <li>initalizer classes for component without a content class (such as
	 *     composites),</li>
	 * <li>Fractal interface and interceptor classes for types with a reserved
	 *     package name (starting with or <code>java.</code> or
	 *     <code>javax.</code>),</li>
	 * <li>controller implementation classes.</li>
	 * </ul>
	 *
	 * The value value of this constant must be the same as the one of {@link
	 * org.objectweb.fractal.juliac.core.Constants#JULIAC_RUNTIME_GENERATED}
	 * defined in the juliac-compiler module.
	 */
	public static final String JULIAC_GENERATED = "juliac";

	public Juliac() {}

	/** Return the Fractal type of this component. */
	public Type getFcInstanceType() {
		Component boot = newFcInstance();
		Type type = boot.getFcType();
		return type;
	}

	/** Return the Fractal controller description of this component. */
	public Object getFcControllerDesc() {
		return "bootstrap";
	}

	/** Return the Fractal content description of this component. */
	public Object getFcContentDesc() {
		Component boot = newFcInstance();
		String name = boot.getClass().getName();
		return name;
	}

	/**
	 * Return the reference of the bootstrap component.
	 * This method is called by {@link
	 * org.objectweb.fractal.api.Fractal#getBootstrapComponent()}.
	 *
	 * The bootstrap component provides the following interfaces:
	 * <ul>
	 * <li> {@link TypeFactory} </li>
	 * <li> {@link GenericFactory} </li>
	 * <li> {@link ClassLoaderItf} </li>
	 * </ul>
	 */
	public Component newFcInstance() {
		return newFcInstance(null,null,null);
	}

	/**
	 * Return the reference of the bootstrap component.
	 */
	public Component newFcInstance(
		Type type, Object controllerDesc, Object contentDesc ) {

		if( bootstrap == null ) {
			bootstrap = new JuliacBootstrapComponentImpl();
		}

		return bootstrap;
	}

	/** The singleton instance of the bootstrap component. */
	private static Component bootstrap;
}
