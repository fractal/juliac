/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Philippe Merle
 */

package org.objectweb.fractal.juliac.runtime;

import java.util.ArrayList;
import java.util.HashMap;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;

/**
 * Root class for membrane initializer classes. Membrane initializer classes
 * are factories for membranes. Initializer classes are factories for
 * components. Initializer classes use membrane initializer classes to
 * instantiate the membrane associated with the component they instantiate.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2.2
 */
public abstract class MembraneInitializer implements Factory {

	protected InitializationContext initFcInitializationContext() {
		InitializationContext ic = new InitializationContext();
		ic.controllers = new ArrayList<>();
		ic.interfaces = new HashMap<>();
		return ic;
	}

	protected void initFcController( InitializationContext ic )
	throws InstantiationException {
		for( int i=0 ; i < ic.controllers.size() ; i++ ) {
			Object ctrl = ic.controllers.get(i);
			if( ctrl instanceof Controller ) {
			   ((Controller)ctrl).initFcController(ic);
			}
		}
	}

	/**
	 * Return the controller implementing the specified control interface either
	 * among the specified initialization context or among the specified content
	 * instance.
	 *
	 * @param ic       the initialization context containing the controllers
	 * @param content  the content instance
	 * @param ctrlcl   the control interface type
	 * @return         the controller
	 * @throws InstantiationException  if there is no such controller
	 * @since 2.5
	 */
	protected Object getFcController(
		InitializationContext ic, Object content, Class<?> ctrlcl )
	throws InstantiationException {

		for( int i=0 ; i < ic.controllers.size() ; i++ ) {
			Object ctrl = ic.controllers.get(i);
			Class<?> cl = ctrl.getClass();
			if( ctrlcl.isAssignableFrom(cl) ) {
				return ctrl;
			}
		}

		Class<?> cl = content.getClass();
		if( ctrlcl.isAssignableFrom(cl) ) {
			return content;
		}

		final String msg = "No such controller for "+ctrlcl.getName();
		throw new InstantiationException(msg);
	}
}
