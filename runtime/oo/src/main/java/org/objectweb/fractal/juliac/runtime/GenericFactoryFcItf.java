/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.runtime;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.BasicComponentInterface;
import org.objectweb.fractal.julia.type.BasicInterfaceType;

/**
 * Implementation of the Fractal interface for {@link GenericFactory}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class GenericFactoryFcItf
extends BasicComponentInterface implements GenericFactory {

	/** <code>NAME</code> of the generic factory Fractal interface. */
	public static final String NAME = "generic-factory";

	/** <code>TYPE</code> of the generic factory Fractal interface. */
	public static final InterfaceType TYPE =
		new BasicInterfaceType(
			NAME,
			GenericFactory.class.getName(),
			false, false, false );

	public GenericFactoryFcItf( Component component, String s, Type type, boolean flag, Object obj ) {
		super(component,s,type,flag,obj);
	}

	public Object getFcItfImpl() { return impl; }

	public void setFcItfImpl(Object obj) {
		impl = (GenericFactory)obj;
	}

	private GenericFactory impl;

	public Component newFcInstance( Type arg0, Object arg1, Object arg2 )
	throws InstantiationException {
		if( impl == null )
			throw new NullPointerException("Trying to invoke a method on a client interface, or on a server interface whose complementary interface is not bound.");
		return impl.newFcInstance(arg0, arg1, arg2);
	}
}
