/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.runtime;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * A runtime exception which wraps a {@link Throwable} instance. This class is
 * meant to be used with JDK 1.3.x since an equivalent feature has been
 * introduced in JDK 1.4.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.5
 */
public class RuntimeException extends java.lang.RuntimeException {

	private static final long serialVersionUID = 4855418822937171277L;

	private Throwable cause;

	/** @since 2.5 */
	public RuntimeException( String message ) {
		super(message);
	}

	public RuntimeException( Throwable cause ) {
		this.cause = cause;
	}

	@Override
	public String getMessage() {
		return cause.getMessage();
	}

	@Override
	public void printStackTrace() {
		cause.printStackTrace();
	}

	@Override
	public void printStackTrace( PrintStream ps ) {
		cause.printStackTrace(ps);
	}

	@Override
	public void printStackTrace( PrintWriter pw ) {
		cause.printStackTrace(pw);
	}
}
