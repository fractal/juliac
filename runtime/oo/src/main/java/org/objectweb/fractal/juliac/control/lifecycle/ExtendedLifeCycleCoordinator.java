/***
 * Juliac
 * Copyright (C) 2012-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.control.lifecycle;

import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;

/**
 * This interface extends the Julia {@link LifeCycleCoordinator} interface with
 * methods to manage the number of received invocations. These methods exist in
 * the implementation and are public but are not exposed in an interface. We are
 * introducing this interface in order to be able to invoke these methods from
 * an interceptor component.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.5
 */
public interface ExtendedLifeCycleCoordinator
extends LifeCycleCoordinator, InvocationCounterItf {}
