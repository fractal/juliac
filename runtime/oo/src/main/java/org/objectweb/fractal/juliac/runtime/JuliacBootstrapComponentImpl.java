/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.runtime;

import java.lang.reflect.InvocationTargetException;
import java.util.Set;
import java.util.TreeSet;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;
import org.objectweb.fractal.julia.type.BasicComponentType;
import org.objectweb.fractal.julia.type.BasicInterfaceType;

/**
 * The Juliac bootstrap component.
 *
 * This bootstrap component provides a type-factory, a generic-factory and a
 * classloader interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class JuliacBootstrapComponentImpl
implements TypeFactory, GenericFactory, ClassLoaderItf, Component {

	// -----------------------------------------------------------------
	// Implementation of the TypeFactory interface
	// -----------------------------------------------------------------

	public InterfaceType createFcItfType(
		String name, String signature,
		boolean isClient, boolean isOptional, boolean isCollection ) {

		return new BasicInterfaceType(
				name,signature,isClient,isOptional,isCollection);
	}

	public ComponentType createFcType(InterfaceType[] interfaceTypes)
	throws InstantiationException {

		return new BasicComponentType(interfaceTypes);
	}


	// -----------------------------------------------------------------
	// Implementation of the GenericFactory interface
	// -----------------------------------------------------------------

	/**
	 * Create a new component instance.
	 *
	 * @param type            the component type
	 * @param controllerDesc  the component controller descriptor
	 * @param contentDesc     the component content descriptor
	 */
	public Component newFcInstance(
		Type type, Object controllerDesc, Object contentDesc )
	throws InstantiationException {

		ComponentType ct = checkType(type);
		String ctrlDesc = checkControllerDesc(controllerDesc);
		Object contDesc = checkContentDesc(ctrlDesc,contentDesc);
		String factoryClassName = getFactoryClassName(ct,ctrlDesc,contDesc);

		Class<?> cl = loadClassIE(factoryClassName);
		Object f = instanciateClass(cl);

		if( ! (f instanceof Factory) ) {
			final String msg =
				"Component initializer class "+factoryClassName+
				" should implement "+Factory.class.getName();
			throw new InstantiationException(msg);
		}

		Factory factory = (Factory) f;
		Component c =
			(contDesc==null || contDesc instanceof String) ?
			factory.newFcInstance() :
			factory.newFcInstance(contDesc);
		return c;
	}

	/**
	 * <p>
	 * Analyze the type specified as the first argument of
	 * {@link #newFcInstance(Type, Object, Object)}.
	 * </p>
	 *
	 * <p>
	 * Check that the specified argument implements {@link ComponentType}.
	 * </p>
	 *
	 * @param type  the type to analyze
	 * @return      the type casted to {@link ComponentType}
	 * @throws InstantiationException
	 *      if the specified type does not implement {@link ComponentType}
	 */
	protected ComponentType checkType( Type type )
	throws InstantiationException {

		if( !(type instanceof ComponentType) ) {
			final String msg = "type should implement ComponentType";
			throw new InstantiationException(msg);
		}

		return (ComponentType) type;
	}

	/**
	 * <p>
	 * Analyze the controller descriptor specified as the second argument of
	 * {@link #newFcInstance(Type, Object, Object)}.
	 * </p>
	 *
	 * <p>
	 * When the specified controller descriptor is an array, extract and save in
	 * the {@link #tl} thread-local variable the classloader which is specified
	 * as a first element.
	 * </p>
	 *
	 * @param controllerDesc  the controller descroptor to analyze
	 * @return                the controller descriptor as a string
	 */
	protected String checkControllerDesc( Object controllerDesc )
	throws InstantiationException {

		/*
		 * When called from Fractal ADL, the component creation process may be
		 * accompagnied with some hints. One of the them is the class loader to
		 * be used. In this case, the controllerDesc parameter is an array where
		 * the 1st element contains the class loader.
		 */
		if( controllerDesc.getClass().isArray() ) {
			Object[] array = (Object[]) controllerDesc;
			ClassLoader classLoader = (ClassLoader) array[0];
			tl.set(classLoader);
			controllerDesc = array[1];
		}

		if( !(controllerDesc instanceof String) ) {
			final String msg = "controllerDesc should be a String";
			throw new InstantiationException(msg);
		}

		return (String) controllerDesc;
	}

	/**
	 * <p>
	 * Analyze the content descriptor specified as the third argument of
	 * {@link #newFcInstance(Type, Object, Object)}.
	 * </p>
	 *
	 * <p>
	 * For template controller descriptors, extract and return the content
	 * descriptor of components created from the template.
	 * </p>
	 *
	 * @param ctrlDesc     the controller descriptor
	 * @param contentDesc  the content descriptor to analyze
	 * @return             the extracted content descriptor
	 */
	protected Object checkContentDesc( String ctrlDesc, Object contentDesc )
	/*
	 * thrown by sublcasses, e.g.
	 * org.objectweb.fractal.juliac.felix.JuliacOSGiFelixBootstrapComponentImpl
	 */
	throws InstantiationException {

		/*
		 * Adaptation for templates components.
		 * Templates are created with something like:
		 * Component cTmpl = cf.newFcInstance(
		 *      cType, "flatPrimitiveTemplate",
		 *      new Object[] { "flatPrimitive", "ClientImpl" });
		 */
		if( ctrlDesc.endsWith("Template") ) {
			Object[] cont = (Object[]) contentDesc;
			contentDesc = cont[1];
		}

		return contentDesc;
	}

	/**
	 * Return the name of the factory class associated with the specified {@link
	 * ComponentType}, controller descriptor and content descriptor.
	 */
	protected String getFactoryClassName(
		ComponentType ct, String ctrlDesc, Object contDesc ) {

		/*
		 * Compute the hash code used as the suffix by the initializer class.
		 * The idea is that there is a class for each different record of
		 * content class, controller descriptor and component type. The two
		 * first elements can act, by nature, as identity. A hash is computed to
		 * uniquely identify component types. Morevover interface types are
		 * sorted. See the comment in {@link
		 * org.objectweb.fractal.juliac.Utils#hexhash(ComponentType)} for the
		 * rationale for this choice.
		 */
		InterfaceType[] its = ct.getFcInterfaceTypes();
		Set<String> names = new TreeSet<>();
		for (int i = 0; i < its.length; i++) {
			InterfaceType it = its[i];
			String name = it.getFcItfName();
			names.add(name);
		}

		StringBuilder sct = new StringBuilder("[");
		boolean first = true;
		for (String name : names) {
			InterfaceType it = null;
			try {
				it = ct.getFcInterfaceType(name);
			}
			catch (NoSuchInterfaceException nsie) {
				// Shouldn't occur since name if one the interface names from ct
				throw new RuntimeException(nsie);
			}
			if(first) { first = false; }
			else { sct.append(','); }
			sct.append(it.toString());
		}
		sct.append(']');

		/*
		 * toString() mandatory to compute the hash on the String, not the
		 * StringBuilder. 2 Strings with the same content have the same hash
		 * code, whereas 2 StringBuilders with the same content are considered as
		 * different objects, and do not share the same hash code.
		 */
		int hashct = sct.toString().hashCode();
		String hexhashct = Integer.toHexString(hashct);

		/*
		 * If contDesc is a string, consider that it is the fully qualified name
		 * of the class which needs to be instantiated. Else, consider that this
		 * is the instance which must be associated with the component.
		 */
		String membraneClassName =
			contDesc == null ?
			Juliac.JULIAC_GENERATED+"."+ctrlDesc :
			(contDesc instanceof String ? contDesc : contDesc.getClass().getName())
			+"FC"+ctrlDesc;
		String factoryClassName = membraneClassName+"FC"+hexhashct;

		return factoryClassName;
	}


	// -----------------------------------------------------------------
	// Implementation of the ClassLoaderItf interface
	// -----------------------------------------------------------------

	/**
	 * A thread local object which may contain the class loader specified when
	 * calling {@link #newFcInstance(Type, Object, Object)}.
	 */
	private ThreadLocal<ClassLoader> tl = new ThreadLocal<>();

	/**
	 * Load the class whose name is specified.
	 *
	 * @param name  the name of the class
	 * @return      the corresponding class
	 * @throws ClassNotFoundException  if the class can not be found
	 * @since 2.1.2
	 */
	public Class<?> loadClass( String name ) throws ClassNotFoundException {
		ClassLoader cl = getClassLoader();
		Class<?> c = cl.loadClass(name);
		return c;
	}

	/**
	 * Return the class loader to be used for loading classes and resources.
	 *
	 * @since 2.1.2
	 */
	protected ClassLoader getClassLoader() {
		ClassLoader cl = tl.get();
		if( cl == null ) {
			cl = getClass().getClassLoader();
		}
		return cl;
	}


	// -----------------------------------------------------------------
	// Utility method
	// -----------------------------------------------------------------

	protected Class<?> loadClassIE( String name ) throws InstantiationException {
		try {
			Class<?> cl = loadClass(name);
			return cl;
		}
		catch( ClassNotFoundException cnfe ) {
			throw new ChainedInstantiationException(cnfe,null,"");
		}
	}

	protected Object instanciateClass( Class<?> cl )
	throws InstantiationException {
		try {
			Object o = cl.getConstructor().newInstance();
			return o;
		}
		catch(
			java.lang.InstantiationException | IllegalAccessException |
			IllegalArgumentException | InvocationTargetException |
			NoSuchMethodException e ) {
			throw new ChainedInstantiationException(e,null,"");
		}
	}


	// -----------------------------------------------------------------
	// Implementation of the Component interface
	// -----------------------------------------------------------------

	public Object getFcInterface( String arg0 )
	throws NoSuchInterfaceException {

		Object[] interfaces = getFcInterfaces();
		for (int i = 0; i < interfaces.length; i++) {
			String name = ((Interface)interfaces[i]).getFcItfName();
			if( name.equals(arg0) ) {
				return interfaces[i];
			}
		}

		throw new NoSuchInterfaceException(arg0);
	}

	public Object[] getFcInterfaces() {
		if( interfaces == null ) {
			interfaces =
				new Object[]{
					new TypeFactoryFcItf( this, TypeFactoryFcItf.NAME, TypeFactoryFcItf.TYPE, false, this ),
					new GenericFactoryFcItf( this, GenericFactoryFcItf.NAME, GenericFactoryFcItf.TYPE, false, this ),
					new ClassLoaderFcItf( this, ClassLoaderFcItf.NAME, ClassLoaderFcItf.TYPE, false, this ),
				};
		}
		return interfaces;
	}

	public Type getFcType() {
		if( type == null ) {
			try {
				type =
					new BasicComponentType(
						new InterfaceType[] {
							TypeFactoryFcItf.TYPE,
							GenericFactoryFcItf.TYPE,
							ClassLoaderFcItf.TYPE
						});
			}
			catch( InstantiationException e ) {
				throw new RuntimeException(e);
			}
		}
		return type;
	}

	private Type type;
	private Object[] interfaces;
}
