/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.runtime;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.BasicComponentInterface;
import org.objectweb.fractal.julia.type.BasicInterfaceType;

/**
 * Implementation of the Fractal interface for {@link ClassLoaderItf}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.2
 */
public class ClassLoaderFcItf
extends BasicComponentInterface implements ClassLoaderItf {

	/** <code>NAME</code> of the class loader Fractal interface. */
	public static final String NAME = "classloader";

	/** <code>TYPE</code> of the class loader Fractal interface. */
	public static final InterfaceType TYPE =
		new BasicInterfaceType(
			NAME,
			ClassLoaderItf.class.getName(),
			false, false, false );

	public ClassLoaderFcItf( Component component, String s, Type type, boolean flag, Object obj ) {
		super(component, s, type, flag, obj);
	}

	public Object getFcItfImpl() {
		return impl;
	}

	public void setFcItfImpl(Object obj) {
		impl = (ClassLoaderItf) obj;
	}

	private ClassLoaderItf impl;

	public Class<?> loadClass( String name ) throws ClassNotFoundException {
		if (impl == null)
			throw new NullPointerException(
				"Trying to invoke a method on a client interface, or on a server interface whose complementary interface is not bound.");
		return impl.loadClass(name);
	}
}
