/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.runtime;

import java.lang.reflect.InvocationTargetException;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.util.Fractal;

/**
 * <p>
 * The class provides a {@link #main(String[])} method for launching a Fractal
 * component compiled with Juliac.
 * </p>
 *
 * <p>
 * The first command line argument must be the fully-qualified name of the
 * component to instantiate. If present, the second command line argument
 * designates the name of a {@link Runnable} server interface provided by the
 * component which is invoked by this program.
 * </p>
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class Launcher {

	/**
	 * Entry point for this program.
	 */
	public static void main(String[] args)
	throws
		ClassNotFoundException, InstantiationException, IllegalAccessException,
		java.lang.InstantiationException,
		NoSuchInterfaceException, IllegalLifeCycleException,
		InvocationTargetException, NoSuchMethodException {

		if( args.length==0 || args.length>2 ) {
			usage();
			return;
		}

		/*
		 * Retrieve the factory.
		 */
		String adl = args[0];
		Class<?> cl = Class.forName(adl);
		Object o = cl.getConstructor().newInstance();

		if( o instanceof Factory ) {

			/*
			 * Fractal factory.
			 */
			Factory factory = (Factory) o;
			Component root = factory.newFcInstance();
			Fractal.getLifeCycleController(root).startFc();

			/*
			 * If an interface name has been specified in the command line
			 * arguments, invoke it.
			 */
			if( args.length == 2 ) {
				String itfname = args[1];
				Object itf = root.getFcInterface(itfname);
				if( !(itf instanceof Runnable) ) {
					final String msg =
						"The "+itfname+
						" interface should implement java.lang.Runnable";
					throw new IllegalArgumentException(msg);
				}
				((Runnable)itf).run();
			}
		}
		else {
			if( o instanceof Runnable ) {
				/*
				 * Not a Fractal factory. Assume the class implements Runnable.
				 */
				((Runnable)o).run();
			}
			else {
				final String msg = "Illegal factory class: "+adl;
				throw new IllegalArgumentException(msg);
			}
		}
	}

	/**
	 * Display the usage information for this program.
	 */
	public static void usage() {
		System.out.println("Usage: java "+Launcher.class.getName()+" factory [itfname]");
		System.out.println("where:");
		System.out.println("- factory is the fully-qualified name of a Fractal Factory");
		System.out.println("- itfname is the name of a Runnable Fractal interface");
	}
}
