/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.runtime;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.InstantiationException;

/**
 * A factory for instantiating components.
 *
 * This factory instantiates components of a particular type, with a particular
 * control membrane and with a particular content implementation.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public interface Factory extends org.objectweb.fractal.api.factory.Factory {

	/**
	 * Return a new component instance.
	 *
	 * @param content  the content instance
	 * @return         a new component instance
	 */
	public Component newFcInstance( Object content )
	throws InstantiationException;
}
