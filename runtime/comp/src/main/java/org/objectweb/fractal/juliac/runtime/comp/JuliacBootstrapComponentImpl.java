/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.runtime.comp;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.type.BasicComponentType;
import org.objectweb.fractal.juliac.runtime.ClassLoaderFcItf;
import org.objectweb.fractal.juliac.runtime.Factory;
import org.objectweb.fractal.juliac.runtime.GenericFactoryFcItf;
import org.objectweb.fractal.juliac.runtime.TypeFactoryFcItf;
import org.objectweb.fractal.juliac.runtime.RuntimeException;
import org.objectweb.fractal.koch.factory.MembraneFactory;

/**
 * The Juliac bootstrap component for the COMP optimization level.
 *
 * This bootstrap component provides a type-factory, a generic-factory, a
 * membrane-factory and a classloader interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.0
 */
public class JuliacBootstrapComponentImpl
extends org.objectweb.fractal.juliac.runtime.JuliacBootstrapComponentImpl
implements MembraneFactory {

	// -----------------------------------------------------------------
	// Implementation of the GenericFactory interface
	// -----------------------------------------------------------------

	/**
	 * Create a new component instance.
	 *
	 * @param type            the component type
	 * @param controllerDesc  the component controller descriptor
	 * @param contentDesc     the component content descriptor
	 */
	@Override
	public Component newFcInstance(
		Type type, Object controllerDesc, Object contentDesc )
	throws InstantiationException {

		/*
		 * If the specified controllerDesc is a Component, consider that this is
		 * the control membrane which is to be associated with the component.
		 */
		if( controllerDesc instanceof Component ) {
			// TODO Koch-like factory
			throw new UnsupportedOperationException();
		}

		Component comp = super.newFcInstance(type,controllerDesc,contentDesc);
		return comp;
	}


	// -----------------------------------------------------------------
	// Implementation of the MembraneFactory interface
	// -----------------------------------------------------------------

	/** The map <Object,String> of membrane adls. */
	private Map<Object,String> membranes = new HashMap<>();

	@Override
	public String getFcMembraneADL( Object controllerDesc ) {
		String adl = (String) membranes.get(controllerDesc);
		return adl;
	}

	@Override
	public Component newFcMembrane( Object controllerDesc )
	throws InstantiationException {

		Class<?> cl = null;
		try {
			String adl = Juliac.JULIAC_GENERATED+'.'+controllerDesc;
			cl = loadClassIE(adl);
		}
		catch( InstantiationException ie ) {
			/*
			 * The factory class hasn't been found in juliac.generated.
			 * Check whether a custom ADL has been registered for this membrane.
			 */
			if( ! membranes.containsKey(controllerDesc) ) {
				final String msg = "Unknown controller descriptor: "+controllerDesc;
				throw new InstantiationException(msg);
			}
			String adl = (String) membranes.get(controllerDesc);
			cl = loadClassIE(adl);
		}

		org.objectweb.fractal.api.factory.Factory factory =
			(org.objectweb.fractal.api.factory.Factory) instanciateClass(cl);
		Component c = factory.newFcInstance();
		return c;
	}

	@Override
	public void registerFcNewMembraneDef( String controllerDesc, String adl )
	throws InstantiationException {

		Class<?> cl = loadClassIE(adl);
		if( ! Factory.class.isAssignableFrom(cl) ) {
			final String msg = adl+" should implement "+Factory.class.getName();
			throw new InstantiationException(msg);
		}
		membranes.put(controllerDesc, adl);
	}


	// -----------------------------------------------------------------
	// Implementation of the Component interface
	// -----------------------------------------------------------------

	@Override
	public Object[] getFcInterfaces() {
		if( interfaces == null ) {
			interfaces =
				new Object[]{
					new TypeFactoryFcItf( this, TypeFactoryFcItf.NAME, TypeFactoryFcItf.TYPE, false, this ),
					new GenericFactoryFcItf( this, GenericFactoryFcItf.NAME, GenericFactoryFcItf.TYPE, false, this ),
					new MembraneFactoryFcItf( this, MembraneFactoryFcItf.NAME, MembraneFactoryFcItf.TYPE, false, this ),
					new ClassLoaderFcItf( this, ClassLoaderFcItf.NAME, ClassLoaderFcItf.TYPE, false, this ),
				};
		}
		return interfaces;
	}

	@Override
	public Type getFcType() {
		if( type == null ) {
			try {
				type =
					new BasicComponentType(
						new InterfaceType[] {
							TypeFactoryFcItf.TYPE,
							GenericFactoryFcItf.TYPE,
							MembraneFactoryFcItf.TYPE,
							ClassLoaderFcItf.TYPE
						});
			}
			catch( InstantiationException e ) {
				throw new RuntimeException(e);
			}
		}
		return type;
	}

	private Type type;
	private Object[] interfaces;
}
