/***
 * Juliac
 * Copyright (C) 2019-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */
package org.objectweb.fractal.juliac.runtime.comp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.Interceptor;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;
import org.objectweb.fractal.koch.factory.ControlComponentBoundMap;
import org.objectweb.fractal.util.ContentControllerHelper;

/**
 * Root class for initializer class for Fractal components with a
 * component-based control membrane.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public class MembraneInitializer {

	/**
	 * Instantiate and start a membrane with the specified membrane factory.
	 */
	protected Component instantiateAndStartMembrane( Factory membraneFactory )
	throws InstantiationException {

	    Component membrane = membraneFactory.newFcInstance();
	    try {
	    	LifeCycleController membranelc = (LifeCycleController)
	    		membrane.getFcInterface("lifecycle-controller");
	    	membranelc.startFc();
	    	return membrane;
	    }
	    catch( Exception e ) {
	    	throw new ChainedInstantiationException(e,null,e.getMessage());
	    }
	}

	/**
	 * Retrieve all controllers contained in the specified membrane.
	 */
	protected void retrieveAllControllers(
		Component membrane, InitializationContext ic, Map<Object,Component> ctrls )
	throws InstantiationException {

	    List<Component> l = ContentControllerHelper.getAllSubComponents(membrane);
	    for( Component comp : l ) {
	    	Object ctrl = getFcInterface(comp,"/content");
		    if( ctrl != null ) {
		        ic.controllers.add(ctrl);
		        ctrls.put(ctrl,comp);
		    }
	    }
	}

	/**
	 * Initialize controllers.
	 *
	 * Iterate on ic.controllers which is a List, to initialize the
	 * component controller first, and the ContentController second.
	 * When initialized, controllers (e.g. the interceptor controller) may
	 * assume that the component controller has already been initialized to
	 * retrieve for example, the component type or the array of internal
	 * fcinterfaces.
	 */
	protected void initializeControllers(
		InitializationContext ic, Map<Controller,Component> ctrls )
	throws InstantiationException {

		// First the Component control-component
		for (Object o : ic.controllers) {
			if( o instanceof Component ) {
			  Controller ctrl = (Controller) o;
			  ctrl.initFcController(ic);
			}
		}

		// Second the Content control-component
		for (Object o : ic.controllers) {
			if( o instanceof ContentController && !(o instanceof Interceptor) ) {
			  Controller ctrl = (Controller) o;
			  Component ctrlcomp = ctrls.get(ctrl);
			  InitializationContext localic = new InitializationContext();
			  localic.content = ic.content;
			  localic.type = ic.type;
			  localic.interfaces = new ControlComponentBoundMap(ctrlcomp);
			  localic.internalInterfaces = ic.internalInterfaces;
			  ctrl.initFcController(localic);
			}
		}

		// Interceptors
		InitializationContext localic = new InitializationContext();
		localic.content = ic.content;
		localic.type = ic.type;
		localic.interfaces = new HashMap<String,Object>();
		for( Map.Entry<String,Object> entry : (Set<Map.Entry<String,Object>>) ic.interfaces.entrySet() ) {
			String key = entry.getKey();
			if( key.equals("component") || key.endsWith("-controller") ) {
			  Object value = entry.getValue();
			  while( value instanceof ComponentInterface ) {
				ComponentInterface ci = (ComponentInterface) value;
				value = ci.getFcItfImpl();
			  }
			  while( value instanceof Interceptor ) {
				Interceptor ci = (Interceptor) value;
				value = ci.getFcItfDelegate();
			  }
			  localic.interfaces.put(key,value);
			}
		}
		localic.internalInterfaces = null;
		for (Object o : ic.controllers) {
			if( o instanceof Interceptor ) {
				Controller ctrl = (Controller) o;
				ctrl.initFcController(localic);
			  }
		}

		// Remaining control-components
		for (Object o : ic.controllers) {
			if( o instanceof Component || o instanceof ContentController ||
				o instanceof Interceptor ) {
			  continue;
			}
			if( !(o instanceof Controller) ) {
			  continue;
			}
			Controller ctrl = (Controller) o;
			Component ctrlcomp = ctrls.get(ctrl);
			localic = new InitializationContext();
			localic.content = ic.content;
			localic.type = ic.type;
			localic.controllers = null;
			localic.interfaces = new ControlComponentBoundMap(ctrlcomp);
			localic.internalInterfaces = null;
			ctrl.initFcController(localic);
		}
	}

	protected Object getFcInterface( Component c, String name )
	throws InstantiationException  {
		try {
			return c.getFcInterface(name);
		}
		catch( NoSuchInterfaceException nsie ) {
			throw new ChainedInstantiationException(
					nsie,null,"No such interface: "+nsie.getMessage());
		}
	}
}
