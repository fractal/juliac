/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.runtime.comp;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.BasicComponentInterface;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.koch.factory.MembraneFactory;

/**
 * Implementation of the Fractal interface for {@link MembraneFactory}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class MembraneFactoryFcItf
extends BasicComponentInterface implements MembraneFactory {

	/** <code>NAME</code> of the type factory Fractal interface. */
	public static final String NAME = "membrane-factory";

	/** <code>TYPE</code> of the type factory Fractal interface. */
	public static final InterfaceType TYPE =
		new BasicInterfaceType(
			NAME,
			MembraneFactory.class.getName(),
			false, false, false );

  public MembraneFactoryFcItf( Component component, String s, Type type, boolean flag, Object obj ) {
	super(component,s,type,flag,obj);
  }

  public Object getFcItfImpl() { return impl; }

  public void setFcItfImpl(Object obj) {
	impl = (MembraneFactory)obj;
  }

  private MembraneFactory impl;

	public String getFcMembraneADL(Object controllerDesc) {
		if( impl == null )
			throw new NullPointerException("Trying to invoke a method on a client interface, or on a server interface whose complementary interface is not bound.");
		return impl.getFcMembraneADL(controllerDesc);
	}

	public Component newFcMembrane(Object controllerDesc) throws InstantiationException {
		if( impl == null )
			throw new NullPointerException("Trying to invoke a method on a client interface, or on a server interface whose complementary interface is not bound.");
		return impl.newFcMembrane(controllerDesc);
	}

	public void registerFcNewMembraneDef(String controllerDesc, String adl)
	throws InstantiationException {
		if( impl == null )
			throw new NullPointerException("Trying to invoke a method on a client interface, or on a server interface whose complementary interface is not bound.");
		impl.registerFcNewMembraneDef(controllerDesc, adl);
	}
}
