/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.runtime.comp;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;

/**
 * The Fractal provider class for Juliac in the COMP optimization level.
 *
 * This Fractal provider returns a bootstrap component with a type-factory,
 * a generic-factory and a membrane-factory interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.0
 */
public class Juliac extends org.objectweb.fractal.juliac.runtime.Juliac {

	public Juliac() {
		super();
	}

	/**
	 * Return the reference of the bootstrap component.
	 */
	@Override
	public Component newFcInstance(
		Type type, Object controllerDesc, Object contentDesc ) {

		if( bootstrap == null ) {
			bootstrap = new JuliacBootstrapComponentImpl();
		}

		return bootstrap;
	}


	/** The singleton instance of the bootstrap component. */
	private static Component bootstrap;
}
