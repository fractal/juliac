# Juliac

Juliac is a Java code generation framework for the [Fractal](http://fractal.ow2.org) software component model. 

Juliac generates at compile-time the infrastructure code, the so-called membrane, of Fractal components programmed in Java. The generated infrastructe code covers interfaces, controllers, and optimization strategies. [Julia](https://fractal.ow2.io/julia/index.html) performs this generation at run-time. Compared to Julia, Juliac removes the usage of dynamic code generation and class loading. Juliac is to Java what [Think](http://think.ow2.org) is to C.

The name Juliac stands for "Julia Compiler" and was proposed by Philippe Merle.

## Source Code

The source code of Juliac is composed of:

- api: the core API of Juliac,
- commons: common utility classes,
- core: the code generation framework,
- extension: extensions to Juliac,
- module: modules for generating code with [Spoon](http://spoon.gforge.inria.fr) and compiling,
- runtime: the library needed to run an application generated with Juliac.

Juliac provides extensions that implement different membranes, configuration mechanisms, programming language supports, architecture description language supports. These extensions are:

- adlet: the support for annotated architecture descriptors (read this [report](https://hal.inria.fr/hal-01964792) for more information),
- dream: the support for the [Dream](http://dream.ow2.org) and [COSMOS](http://picoforge.int-evry.fr/projects/svn/cosmos) frameworks,
- fraclet: the support for [Fraclet](https://fractal.ow2.io/fraclet/index.html) annotated Fractal components,
- julia: the support for the .cfg membrane configuration mechanism of Julia,
- osa: the support for the [OSA](http://osa.gforge.inria.fr) simulation framework,
- osgi: the support for the [OSGi](https://www.osgi.org) framework with the [Equinox](https://www.eclipse.org/equinox), [Felix](https://felix.apache.org), [JBoss](https://docs.jboss.org/author/display/JBOSGI/JBoss+OSGi+Home), and [Knopflerfish](https://www.knopflerfish.org), implementations,
- scala: the support for components implemented with the [Scala](https://www.scala-lang.org) programming language,
- tinfi: the [SCA](http://www.oasis-opencsa.org/sca) component personality that is used in [FraSCAti](http://frascati.ow2.org).

## Optimization Levels

The notion of an optimization level refers to the way the membrane code is generated.

Four optimization levels are implemented in the Julia extension of Juliac:

- OO: the membrane is implemented as a set of objects, each object corresponding to a dedicated controller,
- COMP: the membrane is implemented as an assembly of control components that can be reconfigured with the Fractal API,
- MERGE_ALL: the membrane and the implementation of the component are merged in a single object,
- ULTRA_MERGE: the assembly of components corresponding to the application is merged in a single object.

The Adlet extension of Juliac provides the additional MERGE_CTRL optimization level where the membrane is merged in a single object. The Adlet extension supports the previously mentioned COMP and ULTRA_MERGE optimization levels.

Juliac can be extended with other optimization levels.

The following table summarizes the optimization levels that are implemented in Julia, [AOKell](https://fractal.ow2.io/tutorials/aokell/index.html), the Julia extension of Juliac, and the Adlet extension of Juliac.

| | COMP | OO | MERGE_CTRL | MERGE_ALL | ULTRA_MERGE |
|:--|:--:|:--:|:--:|:--:|:--:|
| Julia | | X | X | X | |
| AOKell | X | | | | |
| Juliac Julia | X | X | | X | X |
| Juliac Adlet | X | | X | | X |


## Contact Information

For any question, please contact: fractal@ow2.org.

Author: Lionel Seinturier.

Contributors (by alphabetical order of last names): Loris Bouzonnet, Olivier Dalle, Damien Fournier, Jonathan Labéjof, Frédéric Loiret, Philippe Merle, Christophe Munilla, Victor Noël, Judicael Ribault, Romain Rouvoy, Valerio Schiavoni.
