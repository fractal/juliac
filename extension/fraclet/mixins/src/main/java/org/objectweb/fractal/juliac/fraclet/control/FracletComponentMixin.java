/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */
package org.objectweb.fractal.juliac.fraclet.control;

import java.lang.reflect.InvocationTargetException;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.juliac.commons.ipf.DuplicationInjectionPointException;
import org.objectweb.fractal.juliac.commons.ipf.InjectionPoint;
import org.objectweb.fractal.juliac.commons.ipf.InjectionPointHashMap;

/**
 * Mixin layer for implementing the injection of Fraclet defined controller
 * references.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public abstract class FracletComponentMixin
implements org.objectweb.fractal.julia.Controller {

	// -------------------------------------------------------------------------
	// Private constructor
	// -------------------------------------------------------------------------

	private FracletComponentMixin() {}

	// -------------------------------------------------------------------------
	// Implementation of the Controller interface
	// -------------------------------------------------------------------------

	public void initFcController( InitializationContext ic )
	throws InstantiationException {

		_super_initFcController(ic);

		Object fcContent = ic.content;
		if( fcContent == null ) {
			// The content class is null for composite components
			return;
		}

		/*
		 * Retrieve all the fields and setter methods annotated with
		 * @Controller. Only fields should be retrieved since @Controller is a
		 * field annotation.
		 */
		Class<?> cl = fcContent.getClass();
		InjectionPointHashMap<Controller> ipm =
			new InjectionPointHashMap<>(cl,Controller.class);

		try {
			ipm.putAll();
		}
		catch( DuplicationInjectionPointException dipe ) {
			InstantiationException ie =
				new InstantiationException(dipe.getMessage());
			ie.initCause(dipe);
			throw ie;
		}

		for (InjectionPoint<Controller> ip : ipm.values()) {
			Controller controller = ip.getAnnotation();
			String name = controller.name();
			try {
				Object itf = _this_getFcInterface(name);
				ip.set( fcContent, itf );
			}
			catch( NoSuchInterfaceException nsie ) {
				final String msg = "No such control interface: "+name;
				InstantiationException ie = new InstantiationException(msg);
				ie.initCause(nsie);
				throw ie;
			}
			catch( IllegalAccessException ioe ) {
				final String msg =
					"Error while injecting reference to control interface: "+
					name;
				InstantiationException ie = new InstantiationException(msg);
				ie.initCause(ioe);
				throw ie;
			}
			catch( InvocationTargetException ite ) {
				final String msg =
					"Error while injecting reference to control interface: "+
					name;
				InstantiationException ie = new InstantiationException(msg);
				ie.initCause(ite);
				throw ie;
			}
		}
	}


	// -------------------------------------------------------------------------
	// Fields and methods required by the mixin class in the base class
	// -------------------------------------------------------------------------

	protected abstract void _super_initFcController( InitializationContext ic )
	throws InstantiationException;

	protected abstract Object _this_getFcInterface( String interfaceName )
	throws NoSuchInterfaceException;
}
