/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.fraclet.control.lifecycle;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.fraclet.annotations.Lifecycle;
import org.objectweb.fractal.fraclet.types.Policy;
import org.objectweb.fractal.fraclet.types.Step;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.juliac.commons.lang.ClassHelper;
import org.objectweb.fractal.juliac.commons.lang.reflect.MethodHelper;

/**
 * Mixin layer for implementing the Fraclet lifecycle policy.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public abstract class FracletLifeCycleControllerMixin implements Controller {

	private Object fcContent;

	/**
	 * The methods to be invoked for every lifecycle step.
	 */
	private Map<Step,List<Method>> notifiables = new HashMap<>();

	// -------------------------------------------------------------------------
	// Private constructor
	// -------------------------------------------------------------------------

	private FracletLifeCycleControllerMixin() {}

	// -------------------------------------------------------------------------
	// Implementation of the Controller interface
	// -------------------------------------------------------------------------

	public void initFcController( InitializationContext ic )
	throws InstantiationException {

		_super_initFcController(ic);

		fcContent = ic.content;
		Class<?> cl = fcContent.getClass();

		// Initialize notifiables
		for (Step step : Step.values()) {
			notifiables.put(step,new ArrayList<Method>());
		}

		// Retrieve all methods annotated with @Lifecycle except overridden ones
		Method[] methods = ClassHelper.getAllMethods(cl);
		methods = MethodHelper.removeOverridden(methods);
		for (Method method : methods) {
			Lifecycle lifecycle = method.getAnnotation(Lifecycle.class);
			if( lifecycle != null ) {
				Class<?>[] types = method.getParameterTypes();
				if( types.length != 0 ) {
					final String msg =
						"Lifecycle callback method "+method+
						" cannot declare parameters";
					throw new InstantiationException(msg);
				}
				Step step = lifecycle.step();
				List<Method> ms = notifiables.get(step);
				ms.add(method);

				// Enable access to private methods
				int mod = method.getModifiers();
				if( Modifier.isPrivate(mod) ) {
					method.setAccessible(true);
				}
			}
		}
	}


	// -------------------------------------------------------------------------
	// Fields and methods added and overriden by the mixin class
	// -------------------------------------------------------------------------

	public void setFcContentState( final boolean started )
	throws IllegalLifeCycleException {

		Step step = started ? Step.START : Step.STOP;
		List<Method> methods = notifiables.get(step);
		for (final Method method : methods) {

			Lifecycle lifecycle = method.getAnnotation(Lifecycle.class);
			Policy policy = lifecycle.policy();

			if( policy.equals(Policy.SYNCHRONOUS) ) {
				invoke(method);
			}
			else {
				new Thread(){
					@Override
					public void run() {
						try {
							invoke(method);
						}
						catch( IllegalLifeCycleException ilce ) {
							ilce.printStackTrace();
						}
					}
				}.start();
			}
		}

		_super_setFcContentState(started);
	}

	private void invoke( final Method method )
	throws IllegalLifeCycleException {
		try {
			method.invoke(fcContent);
		}
		catch (IllegalAccessException|InvocationTargetException e) {
			IllegalLifeCycleException ilce = new IllegalLifeCycleException("");
			ilce.initCause(e);
			throw ilce;
		}
	}


	// -------------------------------------------------------------------------
	// Fields and methods required by the mixin class in the base class
	// -------------------------------------------------------------------------

	protected abstract void _super_initFcController( InitializationContext ic )
	throws InstantiationException;

	protected abstract void _super_setFcContentState( boolean started )
	throws IllegalLifeCycleException;
}
