/***
 * Juliac
 * Copyright (C) 2015-2021 Linagora
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Victor Noel
 */

package org.objectweb.fractal.juliac.fraclet.control.binding;

/**
 * Mixin layer for extending the Fraclet binding policy with simple collection
 * names.
 *
 * For a collection interface named itf1, to add a binding one will use the name
 * itf1binding1 and this binding will be available from the Map under the name
 * binding1.
 *
 * It only changes things from the point of view of the implementation but the
 * Fractal specifications about interface and binding naming are not changes.
 *
 * @author Victor Noel <victor.noel@linagora.com>
 * @since 2.6
 */

public abstract class FracletBindingControllerMixinSimpleCollectionKeys {

	// -------------------------------------------------------------------------
	// Private constructor
	// -------------------------------------------------------------------------

	private FracletBindingControllerMixinSimpleCollectionKeys() {
	}

	// -------------------------------------------------------------------------
	// Fields and methods added and overriden by the mixin class
	// -------------------------------------------------------------------------

	private String computeCollectionKeyBindingName(final String clientItfName, final String itname) {
		return clientItfName.substring(itname.length());
	}
}
