/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Victor Noel
 */

package org.objectweb.fractal.juliac.fraclet.control.binding;

import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.control.binding.ContentBindingController;
import org.objectweb.fractal.juliac.commons.ipf.DuplicationInjectionPointException;
import org.objectweb.fractal.juliac.commons.ipf.InjectionPoint;
import org.objectweb.fractal.juliac.commons.ipf.InjectionPointHashMap;
import org.objectweb.fractal.juliac.commons.ipf.InjectionPointHelper;

/**
 * Mixin layer for implementing the Fraclet binding policy.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Victor Noel <victor.noel@linagora.com>
 * @since 2.6
 */
public abstract class FracletBindingControllerMixin
implements Controller, BindingController {

	/**
	 * <code>true</code> if the content class implements either
	 * {@link BindingController} or {@link ContentBindingController}. In this
	 * case, skips this mixin layer for the next one that is supposed to
	 * implement the traditional mechanism for notifying binding notifications
	 * based on the implementation of the {@link BindingController} or
	 * {@link ContentBindingController} interface.
	 */
	private boolean bc = false;

	private InjectionPointHashMap<Requires> ipm;

	// -------------------------------------------------------------------------
	// Implementation of the Controller interface
	// -------------------------------------------------------------------------

	public void initFcController( InitializationContext ic )
	throws InstantiationException {

		_super_initFcController(ic);

		_this_fcContent = ic.content;

		if( _this_fcContent instanceof BindingController ||
			_this_fcContent instanceof ContentBindingController ) {
			bc = true;
			return;
		}

		/*
		 * Retrieve all the fields and setter methods annotated with @Requires.
		 * Only fields should be retrieved since @Requires is a field
		 * annotation.
		 */
		Class<?> cl = _this_fcContent.getClass();
		ipm = new InjectionPointHashMap<>(cl,Requires.class);

		try {
			ipm.putAll();
		}
		catch( DuplicationInjectionPointException dipe ) {
			InstantiationException ie =
				new InstantiationException(dipe.getMessage());
			ie.initCause(dipe);
			throw ie;
		}
	}

	// -------------------------------------------------------------------------
	// Fields and methods added and overriden by the mixin class
	// -------------------------------------------------------------------------

	public String[] listFc() {

		if(bc) return _super_listFc();

		Object[] itfs = _this_weaveableC.getFcInterfaces();
		int size = 0;
		for (Object o : itfs) {
			Interface itf = (Interface)o;
			InterfaceType it = (InterfaceType) itf.getFcItfType();
			if( it.isFcClientItf() ) {
				size++;
			}
		}

		String[] names = new String[size];
		int i = 0;
		for (Object o : itfs) {
			Interface itf = (Interface)o;
			InterfaceType it = (InterfaceType) itf.getFcItfType();
			if( it.isFcClientItf() ) {
				names[i] = itf.getFcItfName();
				i++;
			}
		}

		return names;
	}

	public Object lookupFc( final String clientItfName )
	throws NoSuchInterfaceException {

		if(bc) return _super_lookupFc(clientItfName);

		// Retrieve the interface type
		InterfaceType it = getFcInterfaceType(_this_weaveableC,clientItfName);
		String itname = it.getFcItfName();

		// Retrieve the injection point
		InjectionPoint<Requires> ip = ipm.get(itname);
		if( ip == null ) {
			throw new NoSuchInterfaceException(clientItfName);
		}

		// Retrieve the value stored in the injection point
		Object srv = InjectionPointHelper.get(ip,_this_fcContent);
		boolean isCollection = it.isFcCollectionItf();
		if( isCollection ) {
			// Collection interface
			Map<String,Object> map = (Map<String,Object>) srv;
			String key = computeCollectionKeyBindingName(clientItfName,itname);
			srv = map.get(key);
		}
		return srv;
	}

	public void bindFc( final String clientItfName, final Object serverItf )
	throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException {

		if(bc) { _super_bindFc(clientItfName,serverItf); return; }

		// Retrieve the interface type
		InterfaceType it = getFcInterfaceType(_this_weaveableC,clientItfName);
		String itname = it.getFcItfName();

		// Retrieve the injection point
		InjectionPoint<Requires> ip = ipm.get(itname);
		if( ip == null ) {
			throw new NoSuchInterfaceException(clientItfName);
		}

		// Inject the value in the injection point
		boolean isCollection = it.isFcCollectionItf();
		if( isCollection ) {
			// Collection interface
			Map<String,Object> map = (Map<String,Object>)
				InjectionPointHelper.get(ip,_this_fcContent);
			String key = computeCollectionKeyBindingName(clientItfName,itname);
			map.put(key,serverItf);
		}
		else {
			InjectionPointHelper.set(ip,_this_fcContent,serverItf);
		}
	}

	public void unbindFc( final String clientItfName )
	throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException {

		if(bc) { _super_unbindFc(clientItfName); return; }

		// Retrieve the interface type
		InterfaceType it = getFcInterfaceType(_this_weaveableC,clientItfName);
		String itname = it.getFcItfName();

		// Retrieve the injection point
		InjectionPoint<Requires> ip = ipm.get(itname);
		if( ip == null ) {
			throw new NoSuchInterfaceException(clientItfName);
		}

		// Inject the value in the injection point
		boolean isCollection = it.isFcCollectionItf();
		if( isCollection ) {
			// Collection interface
			Map<String,Object> map = (Map<String,Object>)
				InjectionPointHelper.get(ip,_this_fcContent);
			String key = computeCollectionKeyBindingName(clientItfName,itname);
			map.remove(key);
		}
		else {
			InjectionPointHelper.set(ip,_this_fcContent,null);
		}
	}


	// -------------------------------------------------------------------------
	// Implementation specific
	// -------------------------------------------------------------------------

	/**
	 * Return the {@link InterfaceType} corresponding to the specific interface
	 * name. Handle the case of collection interfaces where the interface type
	 * name is a prefix for the specified interface name.
	 *
	 * @param component  the component
	 * @param itfName  the interface name
	 * @return               the corresponding interface type
	 * @throws NoSuchInterfaceException   if no such interface type exists
	 */
	private static InterfaceType getFcInterfaceType(
		Component component, final String itfName )
	throws NoSuchInterfaceException {

		Type type = component.getFcType();
		if( ! (type instanceof ComponentType) ) {
			throw new NoSuchInterfaceException(itfName);
		}

		ComponentType ct = (ComponentType) type;
		try {
			InterfaceType it = ct.getFcInterfaceType(itfName);
			return it;
		}
		catch( NoSuchInterfaceException nsie ) {
			// Check collection interfaces
			InterfaceType[] its = ct.getFcInterfaceTypes();
			for (InterfaceType cit : its) {
				String itname = cit.getFcItfName();
				if( itfName.startsWith(itname) ) {
					if( cit.isFcCollectionItf() ) {
						return cit;
					}
				}
			}
		}

		throw new NoSuchInterfaceException(itfName);
	}

	/**
	 * Default implementation to compute the key of a collection interface
	 * binding.
	 *
	 * This can be overriden by a Mixin (such as
	 * {@link FracletBindingControllerMixinSimpleCollectionKeys})
	 * to change the name of the key used in collection interfaces annotated
	 * with @{@link Requires}.
	 *
	 * @author Victor Noel <victor.noel@linagora.com>
	 * @since 2.6
	 */
	private String computeCollectionKeyBindingName(
		final String clientItfName, final String itname ) {

		return clientItfName;
	}


	// -------------------------------------------------------------------------
	// Fields and methods required by the mixin class in the base class
	// -------------------------------------------------------------------------

	private Component _this_weaveableC;
	private Object _this_fcContent;

	protected abstract void _super_initFcController( InitializationContext ic )
	throws InstantiationException;

	protected abstract String[] _super_listFc();
	protected abstract Object _super_lookupFc( String clientItfName )
	throws NoSuchInterfaceException;
	protected abstract void _super_bindFc( String clientItfName, Object serverItf )
	throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException;
	protected abstract void _super_unbindFc( String clientItfName )
	throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException;
}
