/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.fraclet.adl;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.arguments.ArgumentComponentLoader;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.juliac.adl.FractalADLSupportImpl;
import org.objectweb.fractal.juliac.api.RuntimeClassNotFoundException;
import org.objectweb.fractal.juliac.core.Juliac;
import org.objectweb.fractal.juliac.fraclet.core.FracletGenerator;

/**
 * A component loader for Fractal Fraclet annotated components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class FracletComponentLoader extends ArgumentComponentLoader {

	@Override
	protected Definition load(
		final List<String> loaded, final String name,
		final Map<Object,Object> context )
	throws ADLException {

		Juliac jc = (Juliac)
			context.get(FractalADLSupportImpl.CONTEXT_ID_JULIAC);

		/*
		 * Check whether the definition corresponds to a @Component annotated
		 * class.
		 */
		try {
			Class<?> uc = jc.loadClass(name);
			Component component = uc.getAnnotation(Component.class);
			if( component != null ) {
				try {
					FracletGenerator fg = new FracletGenerator();
					fg.generate(jc,uc,name);
				}
				catch( IOException ioe ) {
					throw new ADLException(ioe.getMessage(),ioe);
				}
			}
		}
		catch( RuntimeClassNotFoundException cnfe ) {}

		/*
		 * Check whether the definition corresponds to a Fractal ADL file.
		 */
		return super.load(loaded,name,context);
	}
}
