/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.fraclet.core;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;

import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.opt.ClassGenerator;

/**
 * This class generates the implementations of the finalize method for Fraclet
 * annotated component implementations with destroy lifecycle event
 * notifications.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class FinalizeClassGenerator extends ClassGenerator {

	private String contentClassName;
	private List<Method> destroyMethods;

	public FinalizeClassGenerator(
		String contentClassName, List<Method> destroyMethods ) {

		this.contentClassName = contentClassName;
		this.destroyMethods = destroyMethods;
	}

	@Override
	public String getTargetTypeName() {
		return contentClassName+"Finalize";
	}

	@Override
	public String getSuperClassName() { return contentClassName; }

	@Override
	public void generateMethods( ClassSourceCodeVisitor cv ) {
		generateMethodFinalize(cv);
	}

	protected void generateMethodFinalize( ClassSourceCodeVisitor cv ) {
		BlockSourceCodeVisitor bv =
			cv.visitMethod(
				Modifier.PROTECTED, null, "void", "finalize", null,
				new String[]{Throwable.class.getName()} );

		boolean decl = false;
		for (Method method : destroyMethods) {
			// Use reflection to call private methods
			int mods = method.getModifiers();
			if( Modifier.isPrivate(mods) ) {
				if(!decl) {
					bv.visitVar(Method.class.getName(),"m","null");
					decl = true;
				}
				bv.visitSet(
					"m",
					method.getDeclaringClass().getName()+
					".class.getDeclaredMethod(\""+method.getName()+"\")");
				bv.visitIns("m.setAccessible(true)");
				bv.visitIns("m.invoke(this)");
			}
			else {
				bv.visitIns(method.getName()+"()");
			}
		}

		bv.visitIns("super.finalize()");
		bv.visitEnd();
	}
}
