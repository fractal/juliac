/***
 * Juliac
 * Copyright (C) 2019-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.fraclet.core;

import java.lang.reflect.Field;
import java.util.List;

import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.types.Access;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.opt.ClassGenerator;

/**
 * Root class for generating setter and getter methods for attribute-based
 * generators.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public abstract class AttributeControlSetterGetterGenerator
extends ClassGenerator {

	private List<Field> attributeFields;

	public AttributeControlSetterGetterGenerator( List<Field> attributeFields ) {
		this.attributeFields = attributeFields;
	}

	@Override
	public void generateMethods( ClassSourceCodeVisitor cv ) {

		for (Field attributeField : attributeFields) {

			Attribute attribute = attributeField.getAnnotation(Attribute.class);
			Access mode = attribute.mode();

			/*
			 * Generate the setter method.
			 */
			if( mode.equals(Access.READ_WRITE) ||
				mode.equals(Access.WRITE_ONLY) ) {

				String setterName = attribute.set();
				if( setterName.length() == 0 ) {
					String name = attribute.name();
					if( name.length() == 0 ) {
						name = attributeField.getName();
					}
					setterName =
						"set"+name.substring(0,1).toUpperCase()+
						name.substring(1);
				}

				generateSetterMethod(cv,attributeField,setterName);
			}

			/*
			 * Generate the getter method.
			 */
			if( mode.equals(Access.READ_WRITE) ||
				mode.equals(Access.READ_ONLY) ) {

				String getterName = attribute.get();
				if( getterName.length() == 0 ) {
					String name = attribute.name();
					if( name.length() == 0 ) {
						name = attributeField.getName();
					}
					getterName =
						"get"+name.substring(0,1).toUpperCase()+
						name.substring(1);
				}

				generateGetterMethod(cv,attributeField,getterName);
			}
		}
	}

	protected abstract void generateSetterMethod(
		ClassSourceCodeVisitor cv, Field attributeField, String setterName );

	protected abstract void generateGetterMethod(
		ClassSourceCodeVisitor cv, Field attributeField, String getterName );
}
