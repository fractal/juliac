/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.fraclet.core;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;

import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.CatchSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.runtime.RuntimeException;

/**
 * This class generate attribute controller implementations for Fraclet
 * annotated components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class AttributeControlImplementationGenerator
extends AttributeControlSetterGetterGenerator {

	private String adl;
	private String contentClassName;
	private String attributeControlInterfaceName;

	public AttributeControlImplementationGenerator(
		String adl, List<Field> attributeFields, String contentClassName,
		String attributeControlInterfaceName ) {

		super(attributeFields);

		this.adl = adl;
		this.contentClassName = contentClassName;
		this.attributeControlInterfaceName = attributeControlInterfaceName;
	}

	@Override
	public String getTargetTypeName() {
		return adl + "AttributesImpl";
	}

	@Override
	public String getSuperClassName() { return contentClassName; }

	@Override
	public String[] getImplementedInterfaceNames() {
		return new String[]{attributeControlInterfaceName};
	}

	@Override
	protected void generateSetterMethod(
		ClassSourceCodeVisitor cv, Field attributeField, String setterName ) {

		Class<?> type = attributeField.getType();
		String typeName = type.getName();
		int modifiers = attributeField.getModifiers();
		String attributeFieldName = attributeField.getName();

		BlockSourceCodeVisitor bv =
			cv.visitMethod(
				Modifier.PUBLIC, null, "void", setterName,
				new String[]{typeName+" value"},null);
		if( Modifier.isPrivate(modifiers) ) {
			// Use reflection to set private fields
			CatchSourceCodeVisitor catchv = bv.visitTry();
			catchv.visitVar(
				Field.class.getName(),"f",
				attributeField.getDeclaringClass().getName().replace('$','.')+
				".class.getDeclaredField(\""+attributeFieldName+"\")" );
			catchv.visitIns("f.setAccessible(true)");
			catchv.visitIns("f.set(this,value)");
			catchv = catchv.visitCatch(Exception.class,"e");
			catchv.visitIns(
				"throw new "+RuntimeException.class.getName()+"(e)" );
			catchv.visitEnd();
		}
		else {
			bv.visitSet("this."+attributeFieldName,"value");
		}
		bv.visitEnd();
	}

	@Override
	protected void generateGetterMethod(
		ClassSourceCodeVisitor cv, Field attributeField, String getterName ) {

		Class<?> type = attributeField.getType();
		String typeName = type.getName();
		int modifiers = attributeField.getModifiers();
		String attributeFieldName = attributeField.getName();

		BlockSourceCodeVisitor bv =
			cv.visitMethod(
				Modifier.PUBLIC, null, typeName, getterName, null, null );
		if( Modifier.isPrivate(modifiers) ) {
			// Use reflection to set private fields
			CatchSourceCodeVisitor catchv = bv.visitTry();
			catchv.visitVar(
				Field.class.getName(),"f",
				attributeField.getDeclaringClass().getName().replace('$','.')+
				".class.getDeclaredField(\""+attributeFieldName+"\")" );
			catchv.visitIns("f.setAccessible(true)");
			catchv.visitIns("return ("+typeName+") f.get(this)");
			catchv = catchv.visitCatch(Exception.class,"e");
			catchv.visitIns(
				"throw new "+RuntimeException.class.getName()+"(e)" );
			catchv.visitEnd();
		}
		else {
			bv.visitIns("return","this."+attributeFieldName);
		}
		bv.visitEnd();
	}
}
