/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.fraclet.core;

import java.io.IOException;
import java.util.Map;

import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.RuntimeClassNotFoundException;
import org.objectweb.fractal.juliac.core.desc.ADLParserSupportItf;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;

/**
 * This class provides an ADL parser that handles Fractal Fraclet annotated
 * components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class FracletADLSupportImpl implements ADLParserSupportItf {

	// -------------------------------------------------------------------
	// Implementation of the JuliacModuletItf
	// -------------------------------------------------------------------

	private JuliacItf jc;

	@Override
	public void init( JuliacItf jc ) {
		this.jc = jc;
		jc.register(ADLParserSupportItf.class,this);
	}

	@Override
	public void close( JuliacItf jc ) {
		jc.unregister(ADLParserSupportItf.class,this);
	}


	// -------------------------------------------------------------------
	// Implementation of the ADLParserSupportItf
	// -------------------------------------------------------------------

	@Override
	public boolean test( String adl ) {
		try {
			// Check that adl corresponds to a @Component annotated Java class
			Class<?> uc = jc.loadClass(adl);
			Component component = uc.getAnnotation(Component.class);
			return component != null;
		}
		catch( RuntimeClassNotFoundException cnfe ) {
			return false;
		}
	}

	@Override
	public ComponentDesc<?> parse( String adl, Map<Object,Object> context )
	throws IOException {
		// Not relevant for this ADL parser
		throw new UnsupportedOperationException();
	}

	@Override
	public void generate( String adl, String targetname )
	throws IOException {
		FracletGenerator fg = new FracletGenerator();
		fg.generate(jc,adl);
	}

	@Override
	public void generate( ComponentDesc<?> cdesc, String targetname )
	throws IOException {
		// Not relevant for this ADL parser
		throw new UnsupportedOperationException();
	}
}
