/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.fraclet.core;

import java.lang.reflect.Field;
import java.util.List;

import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.juliac.api.visit.InterfaceSourceCodeVisitor;
import org.objectweb.fractal.juliac.commons.lang.annotation.AnnotationHelper;
import org.objectweb.fractal.juliac.core.opt.InterfaceGenerator;

/**
 * This class generate attribute control interfaces for Fraclet annotated
 * components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class AttributeControlInterfaceGenerator extends InterfaceGenerator {

	private String adl;
	private List<Field> attributeFields;

	public AttributeControlInterfaceGenerator(
		String adl, List<Field> attributeFields ) {

		this.adl = adl;
		this.attributeFields = attributeFields;
	}

	@Override
	public String getTargetTypeName() {
		return adl + "AttributesItf";
	}

	@Override
	protected String[] getExtendedInterfaceNames() {
		return new String[]{AttributeController.class.getName()};
	}

	@Override
	protected void generateMethods( InterfaceSourceCodeVisitor iv ) {

		for (Field attributeField : attributeFields) {

			Attribute attribute = attributeField.getAnnotation(Attribute.class);
			String name = AnnotationHelper.getAnnotationParamValue(attribute,"name");
			if( name.length() == 0 ) {
				name = attributeField.getName();
			}
			name = name.substring(0,1).toUpperCase() + name.substring(1);
			Class<?> type = attributeField.getType();
			String typeName = type.getName();

			// Generate setter and getter
			iv.visitMethod(null,"void","set"+name,new String[]{typeName+" value"},null);
			iv.visitMethod(null,typeName,"get"+name,null,null);
		}
	}
}
