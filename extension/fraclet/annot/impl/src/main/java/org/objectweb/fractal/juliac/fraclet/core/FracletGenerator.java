/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.fraclet.core;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Definition;
import org.objectweb.fractal.fraclet.annotations.Interface;
import org.objectweb.fractal.fraclet.annotations.Lifecycle;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.objectweb.fractal.fraclet.types.Constants;
import org.objectweb.fractal.fraclet.types.Step;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.commons.lang.ClassHelper;
import org.objectweb.fractal.juliac.commons.lang.reflect.MethodHelper;
import org.objectweb.fractal.juliac.core.helper.InterfaceTypeHelper;
import org.objectweb.fractal.juliac.core.helper.MembraneHelper;

/**
 * A class for generating the code associated with a Fractal Fraclet annotated
 * class.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class FracletGenerator {

	/**
	 * Generate the .fractal file corresponding to the specified class name.
	 * If any @{@link Attribute} annotated field is defined, generate the
	 * attribute control interface and implementation.
	 */
	public void generate( JuliacItf jc, String adl )
	throws IOException {

		Class<?> cl = jc.loadClass(adl);
		generate(jc,cl,adl);
	}

	/**
	 * Generate the .fractal file corresponding to the specified class.
	 * If any @{@link Attribute} annotated field is defined, generate the
	 * attribute control interface and implementation.
	 */
	public void generate( JuliacItf jc, Class<?> cl, String adl )
	throws IOException {

		List<InterfaceType> lits = new ArrayList<>();

		/*
		 * Generate the content of the Fractal file.
		 */
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n");
		sb.append("<!DOCTYPE definition PUBLIC \"-//objectweb.org//DTD Fractal ADL 2.0//EN\" \"classpath://org/objectweb/fractal/adl/xml/basic.dtd\">\n");

		// Generate the definition name
		sb.append("<definition name=\"");
		sb.append(adl);
		sb.append('"');

		// Generate extended definitions
		Component component = cl.getAnnotation(Component.class);
		if( component != null ) {
			Definition[] definitions = component.uses();
			if( definitions.length > 0 ) {
				sb.append(" extends=\"");
				for (int i = 0; i < definitions.length; i++) {
					if(i!=0) sb.append(", ");
					Definition definition = definitions[i];
					sb.append(definition.name());
					String[] arguments = definition.arguments();
					if( arguments.length > 0 ) {
						sb.append('(');
						for (int j = 0; j < arguments.length; j++) {
							if(i!=0) sb.append(',');
							sb.append(arguments[j]);
						}
						sb.append(')');
					}
				}
			}
		}

		sb.append(">\n");

		// Generate the definition of server interfaces
		// retrieved from the @Component(provides=...)
		if( component != null ) {
			Interface[] interfaces = component.provides();
			for (Interface itf : interfaces) {
				InterfaceType it = MembraneHelper.toInterfaceType(itf);
				lits.add(it);
				StringBuilder str = InterfaceTypeHelper.fractalADLify(it);
				sb.append(str);
			}
		}

		// Generate the definition of server interfaces
		// retrieved from @Interface annotated implemented interfaces
		Class<?>[] impls = ClassHelper.getAllImplementedInterfaces(cl);
		Map<String,Class<?>> map = new HashMap<>();
		for (int i = 0; i < impls.length; i++) {
			Class<?> impl = impls[i];
			Interface interf = impl.getAnnotation(Interface.class);
			if( interf != null ) {
				String name = interf.name();
				if( ! map.containsKey(name) ) {
					// Skip @Interface annotations already encountered with the
					// same name. This means that the annotation has been
					// redefined with a most specialized type.
					Class<?> interfcl = interf.signature();
					Class<?> itcl =
						interfcl.equals(Constants.class) ? impl : interfcl;
					map.put(name,itcl);
				}
			}
		}
		for (Map.Entry<String,Class<?>> entry : map.entrySet()) {
			String name = entry.getKey();
			Class<?> itcl = entry.getValue();
			InterfaceType it = InterfaceTypeHelper.toInterfaceType(name,itcl);
			lits.add(it);
			StringBuilder str = InterfaceTypeHelper.fractalADLify(it);
			sb.append(str);
		}

		// Generate the definition of client interfaces
		Field[] fields = ClassHelper.getAllFields(cl);
		for (Field field : fields) {
			Requires requires = field.getAnnotation(Requires.class);
			if( requires != null ) {

				InterfaceType it = null;
				try {
					it = MembraneHelper.toInterfaceType(field,requires);
				}
				catch( IllegalArgumentException iae ) {
					IOException ioe = new IOException();
					ioe.initCause(iae);
					throw ioe;
				}
				lits.add(it);

				sb.append("  <interface ");
				sb.append("name=\"");
				sb.append(it.getFcItfName());
				sb.append("\" ");
				sb.append("role=\"client\" ");
				sb.append("signature=\"");
				sb.append(it.getFcItfSignature());
				sb.append("\" ");
				if( it.isFcOptionalItf() ) {
					sb.append("contingency=\"optional\" ");
				}
				if( it.isFcCollectionItf() ) {
					sb.append("cardinality=\"collection\" ");
				}
				sb.append("/>\n");
			}
		}

		// Check whether some attributes need to be configured since this
		// changes the implementation class that is then a generated subclass of
		// the content class with the implementation of the attribute controller
		String contentClassName = cl.getName();
		List<Field> attributeFields = new ArrayList<>();
		List<Field> attributeFieldsWithValue = new ArrayList<>();
		for (Field field : fields) {
			Attribute attribute = field.getAnnotation(Attribute.class);
			if( attribute != null ) {
				attributeFields.add(field);
				String value = attribute.value();
				if( value.length() > 0 ) {
					attributeFieldsWithValue.add(field);
				}
			}
		}

		// If needed, generate the attribute control interface and the attribute
		// controller implementation
		String attributeControlInterfaceName = null;
		if( attributeFields.size() > 0 ) {

			// Check whether the content class can be extended
			int mod = cl.getModifiers();
			if( Modifier.isFinal(mod) ) {
				final String msg =
					"Class "+adl+" must not be final to enable extending it "+
					"to support attribute control interface implementation";
				throw new IOException(msg);
			}

			// Generate the attribute control interface
			SourceCodeGeneratorItf acig =
				new AttributeControlInterfaceGenerator(adl,attributeFields);
			jc.generateSourceCode(acig);
			attributeControlInterfaceName = acig.getTargetTypeName();

			// Generate the attribute controller implementation
			SourceCodeGeneratorItf acg =
				new AttributeControlImplementationGenerator(
					adl, attributeFields, contentClassName,
					attributeControlInterfaceName );
			jc.generateSourceCode(acg);
			contentClassName = acg.getTargetTypeName();

			// Add the corresponding interface type
			InterfaceType it =
				new BasicInterfaceType(
					"attribute-controller", attributeControlInterfaceName,
					false, false, false );
			lits.add(it);
		}

		// Check whether the class needs to be extended to support lifecycle
		// destroy events by extending the Object#finalize method. Yet this
		// support is non deterministic since there are no guarantee that the
		// JVM calls this method.
		Method[] methods = ClassHelper.getAllMethods(cl);
		methods = MethodHelper.removeOverridden(methods);
		List<Method> destroyMethods = new ArrayList<>();
		for (Method method : methods) {
			Lifecycle lifecycle = method.getAnnotation(Lifecycle.class);
			if( lifecycle != null ) {
				Class<?>[] types = method.getParameterTypes();
				if( types.length != 0 ) {
					final String msg =
						"Lifecycle callback method "+method+
						" cannot declare parameters";
					throw new IOException(msg);
				}
				Step step = lifecycle.step();
				if( step.equals(Step.DESTROY) ) {
					destroyMethods.add(method);
				}
			}
		}
		if( destroyMethods.size() > 0 ) {
			SourceCodeGeneratorItf fg =
				new FinalizeClassGenerator(contentClassName,destroyMethods);
			jc.generateSourceCode(fg);
			contentClassName = fg.getTargetTypeName();
		}

		// Generate the definition of the content class
		sb.append("  <content class=\"");
		sb.append(contentClassName);
		sb.append("\" />\n");

		// If needed, generate the ADL definition of attribute values
		if( attributeFieldsWithValue.size() > 0 ) {

			// Generate the fragment of the ADL that initializes the attributes
			sb.append("  <attributes signature=\"");
			sb.append(attributeControlInterfaceName);
			sb.append("\">\n");
			for (Field field : fields) {
				Attribute attribute = field.getAnnotation(Attribute.class);
				if( attribute != null ) {
					String value = attribute.value();
					if( value.length() > 0 ) {
						sb.append("    <attribute ");
						sb.append("name=\"");
						String name =
							attribute.name().length() == 0 ?
							field.getName() :
							attribute.name();
						sb.append(name);
						sb.append("\" ");
						sb.append("value=\"");
						sb.append(value);
						sb.append("\" ");
						sb.append("/>\n");
					}
				}
			}
			sb.append("  </attributes>\n");
		}

		// Generate the definition of the controller descriptor
		String controller = "primitive";
		Membrane membrane = cl.getAnnotation(Membrane.class);
		if( membrane != null ) {
			sb.append("  <controller desc=\"");
			controller = membrane.controller();
			sb.append(controller);
			sb.append("\" />\n");
		}

		// Generate the end of the Fractal file
		sb.append("</definition>\n");

		/*
		 * Create the resource file, add it to the compilation round, and copy
		 * it to the class directory. Build the generated files: the .java ones
		 * because the code generation that follows below needs it, and the
		 * .fractal one because the Fractal ADL toolchain needs it to keep on
		 * parsing the architecture description.
		 *
		 * TODO do not compile if no file has been generated
		 */
		String filename = adl.replace('.','/')+".fractal";
		jc.addResource(filename,sb);
		jc.build();

		/*
		 * Generate the code for the component.
		 */
		InterfaceType[] its = lits.toArray(new InterfaceType[lits.size()]);
		ComponentType ct = InterfaceTypeHelper.newBasicComponentType(its);
		jc.generate(ct,controller,contentClassName,cl);
	}
}
