/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.fraclet.opt.oo;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;

/**
 * This class generates the source code associated to Fractal Fraclet annotated
 * components. The membrane implementation and the initializer implementation
 * are generated.
 *
 * The content, the interceptors and the controllers are kept in separate
 * classes and the controllers are implemented with objects.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class FCOOCtrlSourceCodeGenerator
extends org.objectweb.fractal.juliac.opt.oo.FCOOCtrlSourceCodeGenerator {

	// -----------------------------------------------------------------------
	// Implementation of the FCSourceCodeGeneratorItf interface
	// -----------------------------------------------------------------------

	/**
	 * Return the class generator for component initializers. An initializer is
	 * a factory and provides a <code>newFcInstance</code> method for
	 * instantiating components
	 */
	@Override
	protected InitializerOOCtrlClassGenerator getInitializerClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		return new InitializerOOCtrlClassGenerator(
			jc,fcscg,membraneDesc,ct,contentDesc,source);
	}
}
