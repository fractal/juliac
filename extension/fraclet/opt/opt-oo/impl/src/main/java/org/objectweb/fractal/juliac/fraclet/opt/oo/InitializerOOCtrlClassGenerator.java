/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.fraclet.opt.oo;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.fraclet.annotations.Lifecycle;
import org.objectweb.fractal.fraclet.types.Step;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.JuliacRuntimeException;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.CatchSourceCodeVisitor;
import org.objectweb.fractal.juliac.commons.lang.ClassHelper;
import org.objectweb.fractal.juliac.commons.lang.reflect.MethodHelper;
import org.objectweb.fractal.juliac.core.helper.JuliacHelper;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;

/**
 * Initializer generator for Fractal Fraclet annotated components with an
 * object-oriented control membrane and where the content, the interceptors and
 * the controllers are kept in separate classes.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class InitializerOOCtrlClassGenerator
extends org.objectweb.fractal.juliac.opt.oo.InitializerOOCtrlClassGenerator {

	public InitializerOOCtrlClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		super(jc,fcscg,membraneDesc,ct,contentDesc,source);
	}


	@Override
	protected void generateContentCheckForBC( BlockSourceCodeVisitor mv ) {
		// Indeed nothing
	}

	@Override
	protected void generateNewFcInstanceMethod( BlockSourceCodeVisitor mv ) {

		mv.visitVar("Object","content","newFcContent()");
		mv.visitVar(Component.class.getName(),"instance","newFcInstance(content)");

		/*
		 * Invoke @LifeCycle(step=Step.CREATE) annotated methods only when the
		 * component membrane has been initialized.
		 */
		generateLifeCycleCreate(mv);

		mv.visitIns("return","instance");
	}

	protected void generateLifeCycleCreate( BlockSourceCodeVisitor mv ) {

		String ctrlDesc = membraneDesc.getDescriptor();
		String contentClassName =
			JuliacHelper.getContentClassName(ctrlDesc,contentDesc);

		if( contentClassName != null ) {
			CatchSourceCodeVisitor cv = mv.visitTry();
			generateLifeCycleCreate(mv,contentClassName);
			cv = cv.visitCatch(Throwable.class,"t");
			cv.visitIns(
				"throw","new",ChainedInstantiationException.class.getName(),
				"(t,null,\"\")" );
			cv.visitEnd();
		}
	}

	protected void generateLifeCycleCreate(
		BlockSourceCodeVisitor mv, String contentClassName ) {

		Class<?> cl = jc.loadClass(contentClassName);

		Method[] methods = ClassHelper.getAllMethods(cl);
		methods = MethodHelper.removeOverridden(methods);
		boolean decl = false;
		for (Method method : methods) {
			Lifecycle lifecycle = method.getAnnotation(Lifecycle.class);
			if( lifecycle != null ) {
				Class<?>[] types = method.getParameterTypes();
				if( types.length != 0 ) {
					final String msg =
						"Lifecycle callback method "+method+
						" cannot declare parameters";
					throw new JuliacRuntimeException(msg);
				}
				Step step = lifecycle.step();
				if( step.equals(Step.CREATE) ) {
					int mods = method.getModifiers();
					if( Modifier.isPublic(mods) ) {
						mv.visitIns(
							"(("+contentClassName+")content)."+method.getName()+"()");
					}
					else {
						// Use reflection to invoke non public methods
						if(!decl) {
							mv.visitVar(Method.class.getName(),"m","null");
							decl = true;
						}
						mv.visitSet(
							"m",
							method.getDeclaringClass().getName()+
							".class.getDeclaredMethod(\""+method.getName()+"\")");
						mv.visitIns("m.setAccessible(true)");
						mv.visitIns("m.invoke(content)");
					}
				}
			}
		}
	}
}
