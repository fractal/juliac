###############################################################################
# STANDARD JULIA CONFIGURATION FILE - DO NOT EDIT
#
# PUT NEW OR OVERRIDEN DEFINITIONS AT THE END OF THE FILE, OR IN OTHER FILES
###############################################################################


###############################################################################
# CUSTOM CONFIGURATION INFORMATION
###############################################################################

# -----------------------------------------------------------------------------
# Definitions for a "flat" component model, without composite components
# ContentController, SuperController and all Content related mixins are not used
# -----------------------------------------------------------------------------

(flat-primitive-binding-controller-impl
  ((org.objectweb.fractal.julia.asm.MixinClassGenerator
    FlatPrimitiveBindingControllerImpl
    org.objectweb.fractal.julia.BasicControllerMixin
    org.objectweb.fractal.julia.control.binding.BasicBindingControllerMixin
    # to initialize the BasicBindingControllerMixin from the component's type:
    org.objectweb.fractal.julia.control.binding.TypeBasicBindingMixin
    # to check some basic pre conditions (interface not already bound, ...)
    org.objectweb.fractal.julia.UseComponentMixin
    org.objectweb.fractal.julia.control.binding.CheckBindingMixin
    # to check type related constraints for bindings:
    org.objectweb.fractal.julia.control.binding.TypeBindingMixin
    # to check content related constraints for bindings:
    # org.objectweb.fractal.julia.control.content.UseSuperControllerMixin
    # org.objectweb.fractal.julia.control.binding.ContentBindingMixin
    # to check lifecycle related constraints for bindings:
    org.objectweb.fractal.julia.control.lifecycle.UseLifeCycleControllerMixin
    org.objectweb.fractal.julia.control.binding.LifeCycleBindingMixin
  ))
)

(flat-container-binding-controller-impl
  ((org.objectweb.fractal.julia.asm.MixinClassGenerator
    FlatContainerBindingControllerImpl
    org.objectweb.fractal.julia.BasicControllerMixin
    org.objectweb.fractal.julia.control.binding.ContainerBindingControllerMixin
    org.objectweb.fractal.juliac.fraclet.control.binding.FracletBindingControllerMixin
    # to skip Interface objects before delegating to the encapsulated component:
    # org.objectweb.fractal.julia.control.binding.OptimizedContainerBindingMixin
    # to manage output interceptors:
    org.objectweb.fractal.julia.UseComponentMixin
    org.objectweb.fractal.julia.control.binding.InterceptorBindingMixin
    # to check some basic pre conditions (interface not already bound, ...)
    org.objectweb.fractal.julia.control.binding.CheckBindingMixin
    # to check type related constraints for bindings:
    org.objectweb.fractal.julia.control.binding.TypeBindingMixin
    # to check content related constraints for bindings:
    # org.objectweb.fractal.julia.control.content.UseSuperControllerMixin
    # org.objectweb.fractal.julia.control.binding.ContentBindingMixin
    # to check lifecycle related constraints for bindings:
    org.objectweb.fractal.julia.control.lifecycle.UseLifeCycleControllerMixin
    org.objectweb.fractal.julia.control.binding.LifeCycleBindingMixin
  ))
)

(flatPrimitive
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'name-controller-itf
    )
    (
      'component-impl
      'flat-container-binding-controller-impl
      'lifecycle-controller-impl
      'name-controller-impl
    )
    (
      (org.objectweb.fractal.juliac.core.proxy.InterceptorClassGenerator
        org.objectweb.fractal.juliac.core.proxy.LifeCycleSourceCodeGenerator
      )
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(flatParametricPrimitive
  'flatPrimitive
)

(flatPrimitiveTemplate
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'name-controller-itf
      'factory-itf
      # only if factory-itf does not designate the Julia interface:
      # 'julia-factory-itf
    )
    (
      'component-impl
      'flat-primitive-binding-controller-impl
      'name-controller-impl
      'factory-impl
    )
    (
      # no interceptors
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(flatParametricPrimitiveTemplate
  (
    'interface-class-generator
    (
      'component-itf
      'julia-attribute-controller-itf
      'binding-controller-itf
      'name-controller-itf
      'factory-itf
      # only if factory-itf does not designate the Julia interface:
      # 'julia-factory-itf
    )
    (
      'component-impl
      ((org.objectweb.fractal.julia.asm.AttributeControllerClassGenerator
        ('attributeControllerInterface)
      ))
      'flat-primitive-binding-controller-impl
      'name-controller-impl
      'factory-impl
    )
    (
      # no interceptors
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

# -----------------------------------------------------------------------------
# Definitions for the interceptor tests
# -----------------------------------------------------------------------------

(stat-controller-itf
  (stat-controller org.objectweb.fractal.julia.conform.controllers.StatController)
)

(statPrimitive
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'name-controller-itf
      'stat-controller-itf
    )
    (
      'component-impl
      'container-binding-controller-impl
      'super-controller-impl
      'lifecycle-controller-impl
      'name-controller-impl
      org.objectweb.fractal.julia.conform.controllers.BasicStatController
    )
    (
      (org.objectweb.fractal.juliac.core.proxy.InterceptorClassGenerator
        org.objectweb.fractal.juliac.core.proxy.LifeCycleSourceCodeGenerator
        org.objectweb.fractal.juliac.conform.controllers.StatSourceCodeGenerator
      )
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(statComposite
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'content-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'name-controller-itf
      'stat-controller-itf
    )
    (
      'component-impl
      'composite-binding-controller-impl
      'content-controller-impl
      'super-controller-impl
      'composite-lifecycle-controller-impl
      'name-controller-impl
      org.objectweb.fractal.julia.conform.controllers.BasicStatController
    )
    (
      (org.objectweb.fractal.juliac.core.proxy.InterceptorClassGenerator
        org.objectweb.fractal.juliac.conform.controllers.StatSourceCodeGenerator
      )
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

# -----------------------------------------------------------------------------
# Bad definitions to test errors detection
# -----------------------------------------------------------------------------

(badPrimitive
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'name-controller-itf
    )
    (
      'component-impl
      'container-binding-controller-impl
      'super-controller-impl
      # implementation missing for the LifeCycleController interface
      # 'lifecycle-controller-impl
      'name-controller-impl
    )
    (
      (org.objectweb.fractal.juliac.core.proxy.InterceptorClassGenerator
        org.objectweb.fractal.juliac.core.proxy.LifeCycleSourceCodeGenerator
      )
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(badParametricPrimitive
  'badPrimitive
)
