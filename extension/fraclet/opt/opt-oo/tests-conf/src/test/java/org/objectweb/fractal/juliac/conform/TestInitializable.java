/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.conform;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.conform.AbstractTest;
import org.objectweb.fractal.julia.conform.components.C;
import org.objectweb.fractal.juliac.conform.controllers.InitializableController;
import org.objectweb.fractal.util.Fractal;

/**
 * A class for testing the initialization of controllers from a configuration
 * {@link org.objectweb.fractal.julia.loader.Tree}.
 *
 * This test is not relevant in the COMP optimization level since configuring a
 * controller in this level is rather performed with attributes defined in the
 * architecture definition of the membrane.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.1
 */
public class TestInitializable extends AbstractTest {

	protected Component boot;
	protected TypeFactory tf;
	protected GenericFactory gf;

	protected ComponentType t;
	protected Component d;

	@BeforeEach
	public void setUp() throws InstantiationException, NoSuchInterfaceException {
		boot = Fractal.getBootstrapComponent();
		tf = Fractal.getTypeFactory(boot);
		gf = Fractal.getGenericFactory(boot);
		t = tf.createFcType(new InterfaceType[0]);
		d = gf.newFcInstance(t, "initializable", C.class.getName());
	}

	// -------------------------------------------------------------------------
	// Test controller initialization
	// -------------------------------------------------------------------------

	@Test
	public void testInitializable() throws NoSuchInterfaceException {
		InitializableController ic = (InitializableController)
			d.getFcInterface(InitializableController.NAME);
		Object[] params = (Object[]) ic.getInitTreeValues();
		assertEquals(2,params.length);
		assertEquals("p0",params[0]);
		assertEquals("p1",params[1]);
	}
}
