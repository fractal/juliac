/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.fraclet.opt.comp;

import org.objectweb.fractal.juliac.api.JuliacRuntimeException;
import org.objectweb.fractal.juliac.fraclet.runtime.comp.MPrimitiveImpl;
import org.objectweb.fractal.koch.factory.MCompositeImpl;

/**
 * This class generates the source code associated to Fractal components.
 * The membrane implementation and the initializer implementation are generated.
 *
 * This generator handles mPrimitive and mComposite components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class FCmCompCtrlSourceCodeGenerator
extends org.objectweb.fractal.juliac.opt.comp.FCmCompCtrlSourceCodeGenerator {

	/**
	 * Return the class implementing the membrane of the specified control
	 * component descriptor mPrimitive or mComposite).
	 */
	@Override
	protected Class<?> getMembraneClass( String ctrldesc ) {

		if( ctrldesc.equals("mPrimitive") ) {
			return MPrimitiveImpl.class;
		}

		if( ctrldesc.equals("mComposite") ) {
			return MCompositeImpl.class;
		}

		final String msg = "No such controller desc: "+ctrldesc;
		throw new JuliacRuntimeException(msg);
	}
}
