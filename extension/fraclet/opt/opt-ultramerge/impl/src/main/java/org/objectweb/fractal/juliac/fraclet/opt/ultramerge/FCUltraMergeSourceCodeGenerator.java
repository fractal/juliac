/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.fraclet.opt.ultramerge;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.juliac.adl.FractalADLSupportImpl;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;
import org.objectweb.fractal.juliac.spoon.AbstractUltraMerge;

/**
 * Optimization level source code generator which merges all component business
 * code in a single class. This source code generator handles component business
 * code annotated with the Fraclet framework.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.3
 */
public class FCUltraMergeSourceCodeGenerator extends FractalADLSupportImpl {

	// -----------------------------------------------------------------------
	// Implementation of the FractalADLSupportItf interface
	// -----------------------------------------------------------------------

	/**
	 * Generate the source code of a factory class for the specified ADL.
	 *
	 * @param adl         the fully-qualified name of the ADL descriptor
	 * @param targetname  the fully-qualified name of the factory class
	 * @since 2.2.4
	 */
	@Override
	public void generate( String adl, String targetname )
	throws IOException {

		Map<Object,Object> context = new HashMap<>();
		ComponentDesc<?> cdesc = parse(adl,context);
		AbstractUltraMerge aum = new FracletUltraMerge(jc);
		aum.generate(cdesc,targetname);
	}
}
