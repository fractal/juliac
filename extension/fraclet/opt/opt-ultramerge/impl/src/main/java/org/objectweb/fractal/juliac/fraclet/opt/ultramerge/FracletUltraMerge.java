/***
 * Juliac
 * Copyright (C) 2010-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.fraclet.opt.ultramerge;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.fraclet.types.Cardinality;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.JuliacRuntimeException;
import org.objectweb.fractal.juliac.api.SpoonSupportItf;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;
import org.objectweb.fractal.juliac.spoon.AbstractUltraMerge;
import org.objectweb.fractal.juliac.spoon.IllegalComponentCodeException;
import org.objectweb.fractal.juliac.spoon.helper.CtNamedElementHelper;

import spoon.Launcher;
import spoon.reflect.code.CtExpression;
import spoon.reflect.cu.SourcePosition;
import spoon.reflect.declaration.CtAnnotation;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtExecutable;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtNamedElement;
import spoon.reflect.declaration.CtParameter;
import spoon.reflect.factory.Factory;
import spoon.reflect.reference.CtExecutableReference;
import spoon.reflect.reference.CtTypeReference;
import spoon.reflect.visitor.Query;
import spoon.reflect.visitor.filter.AbstractFilter;

/**
 * Source code generator which merges all component business code in a single
 * class. This class handles component business code annotated with the Fraclet
 * framework.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2.4
 */
public class FracletUltraMerge extends AbstractUltraMerge {

	public FracletUltraMerge( JuliacItf jc ) {
		super(jc);
	}

	// ----------------------------------------------------------------------
	// Abstract methods
	// ----------------------------------------------------------------------

	@Override
	protected String getAttributeName( CtField<?> ctfield ) {
		Attribute r = ctfield.getAnnotation(Attribute.class);
		String name = r.name();
		if( name.length() == 0 ) {
			name = ctfield.getSimpleName();
		}
		return name;
	}

	@Override
	protected CtTypeReference<?> getCollectionFieldType( CtField<?> ctfield ) {
		CtTypeReference<?> fieldType = ctfield.getType();
		List<CtTypeReference<?>> genericTypes =
			fieldType.getActualTypeArguments();
		if( genericTypes==null || genericTypes.size()!=2 ) {
			String msg =
				CtNamedElementHelper.toEclipseClickableString(ctfield)+
				" expected to be of type Map<K,V>";
			throw new JuliacRuntimeException(msg);
		}
		CtTypeReference<?> refType = genericTypes.get(1);
		return refType;
	}

	@Override
	protected String getRequiresName( CtField<?> ctfield ) {
		Requires r = ctfield.getAnnotation(Requires.class);
		String name = r.name();
		if( name.length() == 0 ) {
			name = ctfield.getSimpleName();
		}
		return name;
	}

	@Override
	protected boolean isAttributeAnnotatedField( CtField<?> ctfield ) {
		boolean b = ctfield.hasAnnotation(Attribute.class);
		return b;
	}

	@Override
	protected boolean isRequiredAnnotatedField( CtField<?> ctfield ) {
		boolean b = ctfield.hasAnnotation(Requires.class);
		return b;
	}

	@Override
	protected boolean isRequiredCollectionAnnotatedField( CtField<?> ctfield ) {
		boolean b = ctfield.hasAnnotation(Requires.class);
		if(!b) return false;
		Requires r = ctfield.getAnnotation(Requires.class);
		b = r.cardinality() == Cardinality.COLLECTION;
		return b;
	}

	@Override
	protected void initRequiredCollectionField(
		CtField<?> field, ComponentDesc<?> cdesc, String targetname,
		Collection<CtMethod<?>> duplicateMethods, CtField<?> dst ) {

		// Collection client interface: initiliaze with a Map
		String cltItfName = getRequiresName(field);
		Iterator<String> cltItfNames =
			cdesc.getBindingCltItfNames().stream().
			filter(s->s.startsWith(cltItfName)).iterator();
		StringBuilder sb = new StringBuilder();
		sb.append("class C {");
		sb.append("java.util.TreeMap<String,");
		CtTypeReference<?> tref = getCollectionFieldType(field);
		sb.append(tref.toString());
		sb.append("> f = new java.util.TreeMap<>(){{");
		while( cltItfNames.hasNext() ) {
			String name = cltItfNames.next();
			sb.append("put(\""+name+"\",");
			sb.append("new "+tref.toString()+"(){");
			ComponentDesc<?> srv = cdesc.getBoundPrimitiveComponent(name);
			String srvContentClassName = srv.getContentClassName();
			CtClass<?> srvclass = field.getFactory().Class().get(srvContentClassName);
			for (CtExecutableReference<?> execRef : tref.getAllExecutables()) {
				CtExecutable<?> method = execRef.getDeclaration();
				sb.append("public ");
				sb.append(method.getType());
				sb.append(' ');
				sb.append(method.getSimpleName());
				sb.append('(');
				boolean first=true;
				for (CtParameter<?> param : method.getParameters()) {
					if(first) first=false; else sb.append(',');
					sb.append(param.getType());
					sb.append(' ');
					sb.append(param.getSimpleName());
				}
				sb.append(')');
				if( method.getThrownTypes().size() != 0 ) {
					sb.append("throws ");
					first=true;
					for (CtTypeReference<? extends Throwable> thrown : method.getThrownTypes()) {
						if(first) first=false; else sb.append(',');
						sb.append(thrown);
					}
				}
				sb.append('{');
				if( ! method.getType().toString().equals("void") ) {
					sb.append("return ");
				}
				String methodName = execRef.getSimpleName();
				sb.append(targetname);
				sb.append(".this.");
				sb.append(methodName);
				CtMethod<?> srvmethod =
					srvclass.getMethod(methodName,execRef.getParameters().toArray(new CtTypeReference[0]));
				if( duplicateMethods.contains(srvmethod) ) {
					sb.append('_');
					sb.append(srvmethod.getDeclaringType().getQualifiedName().toString().replace('.','_'));
				}
				sb.append('(');
				first=true;
				for (CtParameter<?> param : method.getParameters()) {
					if(first) first=false; else sb.append(',');
					sb.append(param.getSimpleName());
				}
				sb.append(");}");
			}
			sb.append("});");
		}
		sb.append("}};}");
		CtClass<?> c = Launcher.parseClass(sb.toString());
		CtField<?> f = c.getField("f");
		dst.setDefaultExpression((CtExpression)f.getDefaultExpression());
	}


	// ----------------------------------------------------------------------
	// Mergeability check
	// ----------------------------------------------------------------------

	@Override
	protected void mergeabilityCheck( ComponentDesc<?> cdesc )
	throws IllegalComponentCodeException {
		super.mergeabilityCheck(cdesc);
		mergeabilityCheckNoControllerAnnotation(cdesc);
	}


	/**
	 * Check that the classes of the architecture do not contain
	 * @{@link Controller} annotated fields.
	 */
	private void mergeabilityCheckNoControllerAnnotation( ComponentDesc<?> cdesc )
	throws IllegalComponentCodeException {

		String contentClassName = cdesc.getContentClassName();
		if( contentClassName != null ) {
			SpoonSupportItf spoon = jc.lookupFirst(SpoonSupportItf.class);
			Factory factory = spoon.getSpoonFactory();
			CtClass<?> ctclass = factory.Class().get(contentClassName);
			mergeabilityCheckNoControllerAnnotation(ctclass);
		}

		for (ComponentDesc<?> sub : cdesc.getSubComponents()) {
			mergeabilityCheck(sub);
		}
	}

	/**
	 * Check that the specified {@link CtClass} does not contain @{@link
	 * Controller} annotated fields.
	 *
	 * @param src  the {@link CtClass} to be checked against mergeability
	 * @throws IllegalComponentCodeException  if this is not the case
	 */
	private void mergeabilityCheckNoControllerAnnotation( CtClass<?> src )
	throws IllegalComponentCodeException {

		List<CtAnnotation<?>> annots =
			Query.getElements(
				src,
				new AbstractFilter<>(CtAnnotation.class) {
					@Override
					public boolean matches( CtAnnotation<?> ctannot ) {
						Annotation annot = ctannot.getActualAnnotation();
						return ( annot instanceof Controller );
					}
				}
			);

		if( annots.size() != 0 ) {
			StringBuilder msg = new StringBuilder();
			msg.append("@Controller annotations at the following source ");
			msg.append("position prevent the code from being merged");
			for (CtAnnotation<?> annot : annots) {
				msg.append("\n");
				String pos = toPositionString(annot);
				msg.append(pos);
			}
			throw new IllegalComponentCodeException(msg.toString());
		}
	}

	/**
	 * Return a stringified indication of the source position for the specified
	 * {@link CtAnnotation}.
	 */
	private String toPositionString( CtAnnotation<?> annot ) {
		CtNamedElement ne = annot.getParent(CtNamedElement.class);
		if( ne == null ) {
			SourcePosition sp = annot.getPosition();
			return sp.toString();
		}
		else {
			String s = CtNamedElementHelper.toEclipseClickableString(ne);
			return s;
		}
	}
}
