/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 */

package example.hw;

import java.util.Map;
import java.util.TreeMap;

import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Interface;
import org.objectweb.fractal.fraclet.annotations.Lifecycle;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.fraclet.types.Cardinality;
import org.objectweb.fractal.fraclet.types.Step;
import org.objectweb.fractal.juliac.commons.io.Console;

@Component(provides=@Interface(name="r",signature=Runnable.class))
public class ClientImpl implements Runnable {

	private Console console;

	public ClientImpl() {
		console = Console.getConsole("hw-fraclet-");
	}

	@Lifecycle(step=Step.CREATE)
	public void onCreate() {
		console.println("CLIENT created");
	}

	public void run() {
		s.print(nc.getFcName()+": Hello world");
		for (PrinterItf s : col.values()) {
			s.print("Hello world");
		}
	}

	@Requires
	private PrinterItf s;

	@Requires(cardinality=Cardinality.COLLECTION)
	private Map<String,PrinterItf> col = new TreeMap<String,PrinterItf>();

	@Controller(name="name-controller")
	private NameController nc;

	@Lifecycle(step=Step.START)
	public void onStart() {
		console.println("CLIENT starting");
	}

	@Lifecycle(step=Step.STOP)
	private void onStop() {
		console.println("CLIENT stopping");
	}
}
