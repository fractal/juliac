/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 */

package example.hw;

import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Interface;
import org.objectweb.fractal.fraclet.annotations.Lifecycle;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.objectweb.fractal.fraclet.types.Step;
import org.objectweb.fractal.juliac.commons.io.Console;

@Membrane(controller="parametricPrimitive")
@Component(provides=@Interface(name="s",signature=PrinterItf.class))
public class ServerImpl implements PrinterItf {

	@Attribute(value="-> ")
	private String header;

	@Attribute()
	protected int count = 1;

	private Console console;

	public ServerImpl() {
		console = Console.getConsole("hw-fraclet-");
	}

	@Lifecycle(step=Step.CREATE)
	private void onCreate() {
		console.println("SERVER created");
	}

	public void print(final String msg) {
		console.println("Server "+nc.getFcName()+": begin printing...");
		for (int i = 0 ; i < (count) ; ++i) {
			console.println(header + msg);
		}
		console.println("Server: print done.");
	}

	@Controller(name="name-controller")
	private NameController nc;
}
