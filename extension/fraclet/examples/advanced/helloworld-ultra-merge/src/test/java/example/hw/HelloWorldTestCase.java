/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package example.hw;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.jupiter.api.Test;
import org.objectweb.fractal.juliac.commons.io.Console;

/**
 * Class for testing the Hello World example.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class HelloWorldTestCase {

	@Test
	public void testHelloWorld()
	throws
		ClassNotFoundException, NoSuchMethodException, IllegalAccessException,
		InvocationTargetException {

		// Direct the output of the example to a temporary file
		Console console = Console.getConsole("hw-ultra-merge-");

		// Invoke the main() method
		ClassLoader loader = getClass().getClassLoader();
		Class<?> cl = loader.loadClass("example.hw.HelloWorld");
		Object args = new String[0];
		Method m = cl.getMethod("main",args.getClass());
		m.invoke(null,args);

		// Compare the result with the expected result
		console.close();
		console.dump(System.err);
		console.assertEquals(expecteds);
	}

	final static private String[] expecteds =
		new String[]{
			"CLIENT",
			"SERVER",
			"Server: begin printing...",
			"-> Hello world",
			"Server: print done.",
		};
}
