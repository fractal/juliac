<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE definition PUBLIC "-//objectweb.org//DTD Fractal ADL 2.0//EN" "classpath://org/objectweb/fractal/adl/xml/basic.dtd">

<!--
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 -->

<definition name="example.hw.HelloWorld">

	<interface name="r" role="server" signature="java.lang.Runnable" />

	<component name="client">
		<interface name="r" role="server" signature="java.lang.Runnable" />
		<interface name="s" role="client" signature="example.hw.Service" />
		<content class="example.hw.ClientImpl" />
	</component>

	<component name="server">
		<interface name="s" role="server" signature="example.hw.Service" />
		<content class="example.hw.ServerImpl" />
		<attributes signature="example.hw.ServiceAttributes">
			<attribute name="header" value="-> " />
			<attribute name="count" value="1" />
		</attributes>
		<controller desc="parametricPrimitive" />
	</component>

	<binding client="this.r" server="client.r" />
	<binding client="client.s" server="server.s" />

</definition>
