package example.hw;

import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Interface;
import org.objectweb.fractal.juliac.commons.io.Console;

@Component(provides=@Interface(name="s",signature=ServiceAttributes.class))
public class ServerImpl implements Service, ServiceAttributes {

	@Attribute private String header;
	@Attribute private int count;

	private Console console;

	public ServerImpl() {
		console = Console.getConsole("hw-ultra-merge-");
	}

	public void print(final String msg) {
		console.println("SERVER");
		String header = getHeader();
		console.println("Server: begin printing...");
		for (int i = 0 ; i < (count) ; ++i) {
			console.println(header + msg);
		}
		console.println("Server: print done.");
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(final String header) {
		this.header = header;
	}

	public int getCount() {
		return count;
	}

	public void setCount(final int count) {
		this.count = count;
	}
}
