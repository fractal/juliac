package example.hw;

import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Interface;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.juliac.commons.io.Console;

@Component(provides=@Interface(name="r",signature=Runnable.class))
public class ClientImpl implements Runnable {

	private Console console;

	public ClientImpl() {
		console = Console.getConsole("hw-ultra-merge-");
	}

	public void run() {
		console.println("CLIENT");
		s.print("Hello world");
	}

	@Requires
	private Service s;
}
