package example.comanche;


public class ErrorRequestHandler implements RequestHandler {
  public void handleRequest (Request r) {
	r.out.print("HTTP/1.0 404 Not Found\n\n");
	r.out.print("<html>Document not found.</html>");
  }
}
