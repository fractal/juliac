package example.comanche;

import java.io.LineNumberReader;
import java.io.PrintStream;
import java.net.Socket;

public class Request {
  public Socket s;
  public LineNumberReader in;
  public PrintStream out;
  public String rq;
  public String url;
  public Request (Socket s) { this.s = s; }
}
