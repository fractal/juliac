package example.comanche;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.types.Cardinality;

public class RequestAnalyzer implements RequestHandler {

	@Requires(name="cols",cardinality=Cardinality.COLLECTION)
	public Map<String, RequestHandler> handlers = new TreeMap<String, RequestHandler>();

	public void handleRequest(Request r) throws IOException {
		r.in = new LineNumberReader(new InputStreamReader(r.s.getInputStream()));
		r.out = new PrintStream(r.s.getOutputStream());
		r.rq = r.in.readLine();
		System.out.println(r.rq);

		Iterator<RequestHandler> i = handlers.values().iterator();
		while (i.hasNext()) {
			try {
				((RequestHandler)i.next()).handleRequest(r);
				r.out.close();
				r.s.close();
				return;
			} catch (IOException e) { }
		}
	}
}
