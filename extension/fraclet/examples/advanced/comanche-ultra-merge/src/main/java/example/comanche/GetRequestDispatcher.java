package example.comanche;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.types.Cardinality;

public class GetRequestDispatcher implements RequestHandler {

	@Requires(name="cols",cardinality=Cardinality.COLLECTION)
	public Map<String, RequestHandler> handlers = new TreeMap<String, RequestHandler>();

	public void handleRequest (Request r) throws IOException {
		if (! r.rq.startsWith("GET ")) {
			throw new IOException("Not a GET request");
		}

		Iterator<RequestHandler> i = handlers.values().iterator();
		while (i.hasNext()) {
			try {
				((RequestHandler)i.next()).handleRequest(r);
				return;
			} catch (IOException e) { }
		}
	}
}
