package example.comanche;

import java.io.Closeable;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.objectweb.fractal.fraclet.annotations.Requires;

public class RequestReceiver implements Runnable, Closeable {

	@Requires(name="rh")
	public RequestHandler rh;

	private ServerSocket ss;

	public void run() {
		try {
			ss = new ServerSocket(9042);
			System.out.println("Comanche HTTP Server ready on port 9042.");
			System.out.println("Load http://localhost:9042/src/main/resources/example/comanche/gnu.jpg");
			while (true) {
				final Socket socket = ss.accept();
				Thread t = new Thread() {
					public void run() {
						try {
							rh.handleRequest(new Request(socket));
						} catch (IOException ioe) {}
					}
				};
				t.start();
			}
		} catch ( IOException e ) {}
	}

	public void close() throws IOException {
		ss.close();
	}
}
