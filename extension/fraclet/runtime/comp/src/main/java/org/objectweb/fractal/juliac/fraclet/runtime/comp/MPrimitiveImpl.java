/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.fraclet.runtime.comp;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.juliac.commons.ipf.DuplicationInjectionPointException;
import org.objectweb.fractal.juliac.commons.ipf.InjectionPoint;
import org.objectweb.fractal.juliac.commons.ipf.InjectionPointHashMap;
import org.objectweb.fractal.juliac.commons.ipf.InjectionPointHelper;

/**
 * Implementation of the control membrane for mPrimitive Fraclet annotated
 * components. mPrimitive components are primitive control components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 *
 */
public class MPrimitiveImpl
extends org.objectweb.fractal.koch.factory.MPrimitiveImpl {

	public MPrimitiveImpl( Type type, Object content ) {
		super(type,content);
	}

	// -----------------------------------------------------------------
	// Implementation specific
	// -----------------------------------------------------------------

	private InjectionPointHashMap<Requires> ipm;

	@Override
	protected void setFcIsBCNotifiable() {

		/*
		 * Retrieve all the fields and setter methods annotated with @Requires.
		 * Only fields should be retrieved since @Requires is a field
		 * annotation.
		 */
		Class<?> cl = content.getClass();
		ipm = new InjectionPointHashMap<>(cl,Requires.class);

		try {
			ipm.putAll();
		}
		catch( DuplicationInjectionPointException dipe ) {
			throw new RuntimeException(dipe);
		}

		/*
		 * Notifications are needed if there is at least one injection point.
		 */
		isBCNotifiable = (ipm.size() != 0);
	}

	@Override
	protected void notifyBindFcComponent() {
		if( ipm.containsKey("component") ) {
			InjectionPoint<Requires> ip = ipm.get("component");
			InjectionPointHelper.set(ip,content,this);
		}
	}

	@Override
	protected void notifyListFc() {
		// Indeed nothing
	}

	@Override
	protected void notifyLookupFc( String s ) throws NoSuchInterfaceException {
		// Indeed nothing
	}

	@Override
	protected void notifyBindFc( String s, Object ref )
	throws
		NoSuchInterfaceException, IllegalBindingException,
		IllegalLifeCycleException {

		InjectionPoint<Requires> ip = ipm.get("//"+s);
		InjectionPointHelper.set(ip,content,ref);
	}

	@Override
	protected void notifyUnBindFc( String s )
	throws
		NoSuchInterfaceException, IllegalBindingException,
		IllegalLifeCycleException {

		InjectionPoint<Requires> ip = ipm.get("//"+s);
		InjectionPointHelper.set(ip,content,null);
	}
}
