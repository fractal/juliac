/***
 * Juliac
 * Copyright (C) 2015-2021 Linagora
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Victor Noel
 */

package org.objectweb.fractal.juliac.core.helper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.types.Cardinality;
import org.objectweb.fractal.fraclet.types.Contingency;
import org.objectweb.fractal.juliac.core.helper.MembraneHelper;

/**
 * Class for testing the functionalities of {@link MembraneHelper}.
 *
 * @author Victor Noel <victor.noel@linagora.com>
 * @since 2.6
 */
public class MembraneHelperTestCase {

	@Requires()
	public Runnable testRequire;

	@Requires(name = "test")
	public Runnable testRequire1;

	@Requires(name = "test", contingency = Contingency.OPTIONAL)
	public Runnable testRequire1o;

	@Requires(cardinality = Cardinality.COLLECTION)
	public Runnable testRequire2;

	@Requires(cardinality = Cardinality.COLLECTION)
	public Map<String, String> testRequire3;

	@Requires(cardinality = Cardinality.COLLECTION, signature = String.class)
	public Map<String, String> testRequire3e;

	@Requires(cardinality = Cardinality.COLLECTION, signature = String.class)
	public Map<String, Integer> testRequire3ew;

	@Requires(cardinality = Cardinality.COLLECTION)
	public Map<String,Collection<String>> testRequire4;

	@Requires(cardinality = Cardinality.COLLECTION, signature = Collection.class)
	public Map<String,Collection<String>> testRequire4e;

	@Requires()
	public String testRequire5;

	@Requires(signature = String.class)
	public String testRequire5e;

	@Requires(signature = String.class)
	public Integer testRequire5ew;

	private void checkInterface(
		InterfaceType itf, boolean isClient, boolean isCollection,
		boolean isOptional, String name, Class<?> signature ) {

		assertEquals(isOptional, itf.isFcOptionalItf());
		assertEquals(isCollection, itf.isFcCollectionItf());
		assertEquals(isClient, itf.isFcClientItf());
		assertEquals(name, itf.getFcItfName());
		assertEquals(signature.getName(), itf.getFcItfSignature());
	}

	@Test
	public void testSimpleRequireOptional()
	throws NoSuchFieldException, SecurityException {
		final Field field = this.getClass().getField("testRequire1o");
		assertNotNull(field);
		final Requires annot = field.getAnnotation(Requires.class);
		assertNotNull(annot);
		final InterfaceType interfaceType =
			MembraneHelper.toInterfaceType(field, annot);
		checkInterface(interfaceType, true, false, true, "test", Runnable.class);
	}

	@Test
	public void testSimpleRequire()
	throws NoSuchFieldException, SecurityException {
		final Field field = this.getClass().getField("testRequire1");
		assertNotNull(field);
		final Requires annot = field.getAnnotation(Requires.class);
		assertNotNull(annot);
		final InterfaceType interfaceType =
			MembraneHelper.toInterfaceType(field, annot);
		checkInterface(interfaceType, true, false, false, "test", Runnable.class);
	}

	@Test
	public void testSimplestRequire()
	throws NoSuchFieldException, SecurityException {
		final Field field = this.getClass().getField("testRequire");
		assertNotNull(field);
		final Requires annot = field.getAnnotation(Requires.class);
		assertNotNull(annot);
		final InterfaceType interfaceType =
			MembraneHelper.toInterfaceType(field, annot);
		checkInterface(interfaceType, true, false, false, "testRequire", Runnable.class);
	}

	@Test
	public void testFailCollection()
	throws NoSuchFieldException, SecurityException {
		final Field field = this.getClass().getField("testRequire2");
		assertNotNull(field);
		final Requires annot = field.getAnnotation(Requires.class);
		assertNotNull(annot);
		assertThrows(
			IllegalArgumentException.class,
			() -> MembraneHelper.toInterfaceType(field, annot));
	}

	@Test
	public void testOKCollection()
	throws NoSuchFieldException, SecurityException {
		final Field field = this.getClass().getField("testRequire3");
		assertNotNull(field);
		final Requires annot = field.getAnnotation(Requires.class);
		assertNotNull(annot);
		checkInterface(
			MembraneHelper.toInterfaceType(field, annot),
			true, true, false, "testRequire3", String.class);
	}

	@Test
	public void testOKCollectionExplicit()
	throws NoSuchFieldException, SecurityException {
		final Field field = this.getClass().getField("testRequire3e");
		assertNotNull(field);
		final Requires annot = field.getAnnotation(Requires.class);
		assertNotNull(annot);
		checkInterface(
			MembraneHelper.toInterfaceType(field, annot),
			true, true, false, "testRequire3e", String.class);
	}

	@Test
	public void testNOKCollectionExplicit() throws NoSuchFieldException, SecurityException {
		final Field field = this.getClass().getField("testRequire3ew");
		assertNotNull(field);
		final Requires annot = field.getAnnotation(Requires.class);
		assertNotNull(annot);
		assertThrows(
			IllegalArgumentException.class,
			() -> MembraneHelper.toInterfaceType(field, annot));
	}

	@Test
	public void testOKCollectionParam()
	throws NoSuchFieldException, SecurityException {
		final Field field = this.getClass().getField("testRequire4");
		assertNotNull(field);
		final Requires annot = field.getAnnotation(Requires.class);
		assertNotNull(annot);
		checkInterface(
			MembraneHelper.toInterfaceType(field, annot),
			true, true, false, "testRequire4", Collection.class);
	}

	@Test
	public void testOKCollectionParamExplicit()
	throws NoSuchFieldException, SecurityException {
		final Field field = this.getClass().getField("testRequire4e");
		assertNotNull(field);
		final Requires annot = field.getAnnotation(Requires.class);
		assertNotNull(annot);
		checkInterface(
			MembraneHelper.toInterfaceType(field, annot),
			true, true, false, "testRequire4e", Collection.class);
	}

	@Test
	public void testOK() throws NoSuchFieldException, SecurityException {
		final Field field = this.getClass().getField("testRequire5");
		assertNotNull(field);
		final Requires annot = field.getAnnotation(Requires.class);
		assertNotNull(annot);
		checkInterface(MembraneHelper.toInterfaceType(field, annot), true, false, false, "testRequire5", String.class);
	}

	@Test
	public void testOKExplicit()
	throws NoSuchFieldException, SecurityException {
		final Field field = this.getClass().getField("testRequire5e");
		assertNotNull(field);
		final Requires annot = field.getAnnotation(Requires.class);
		assertNotNull(annot);
		checkInterface(
			MembraneHelper.toInterfaceType(field, annot),
			true, false, false, "testRequire5e", String.class);
	}

	@Test
	public void testNOKExplicit()
	throws NoSuchFieldException, SecurityException {
		final Field field = this.getClass().getField("testRequire5ew");
		assertNotNull(field);
		final Requires annot = field.getAnnotation(Requires.class);
		assertNotNull(annot);
		assertThrows(
			IllegalArgumentException.class,
			() -> MembraneHelper.toInterfaceType(field, annot));
	}
}
