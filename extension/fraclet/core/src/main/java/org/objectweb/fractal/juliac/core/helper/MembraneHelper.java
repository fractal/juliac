/***
 * Juliac
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.helper;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.fraclet.annotations.Interface;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.objectweb.fractal.fraclet.types.Constants;
import org.objectweb.fractal.julia.type.BasicInterfaceType;

/**
 * Helper class for dealing with @{@link Membrane}-annotated classes.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.3.1
 */
public class MembraneHelper {

	private MembraneHelper() {}

	/**
	 * Check that the class associated with the specified
	 *         @{@link Membrane}<code>(controllerDesc=...)</code>
	 * annotation references a membrane definition, i.e. a class that is
	 * annotated with
	 *         @{@link Membrane}<code>(desc="...")</code>.
	 *
	 * @param jm    the {@link Membrane} annotation
	 * @param name  the name of the class annotated with {@link Membrane}
	 * @return      the membrane definition
	 * @throws IllegalArgumentException
	 *         if the referenced class is not a membrane definition
	 */
	public static Class<?> getMembraneDef( Membrane jm, String name ) {
		Class<?> cl = jm.controllerDesc();
		Membrane mdef = cl.getAnnotation(Membrane.class);
		if( mdef == null ) {
			String msg =
				"The "+cl.getName()+" membrane class specified in the "+
				"@Membrane(controllerDesc=...) annotation on the "+name+
				" class should be annotated with @Membrane(desc=\"...\").";
			throw new IllegalArgumentException(msg);
		}
		return cl;
	}

	/**
	 * Check that the specified membrane definition is consistent.
	 *
	 * @throws IllegalArgumentException  if the membrane definition is not consistent
	 */
	public static void check( Class<?> cl ) {

		// Check that the class is annotated with @Membrane
		Membrane mdef = cl.getAnnotation(Membrane.class);
		if( mdef == null ) {
			String msg = "No @Membrane annotation on class "+cl.getName();
			throw new IllegalArgumentException(msg);
		}

		/*
		 * Check that either the controller, template, controllerDesc,
		 * templateDesc parameters are set or the desc, generator, interceptors
		 * parameters are set.
		 */
		boolean isControllerSet = ! mdef.controller().equals(Constants.EMPTY);
		boolean isTemplateSet = ! mdef.template().equals(Constants.EMPTY);
		boolean isControllerDescSet = ! mdef.controllerDesc().equals(Constants.class);
		boolean isTemplateDescSet = ! mdef.templateDesc().equals(Constants.class);
		boolean isDescSet = ! mdef.desc().equals(Constants.EMPTY);
		boolean isGeneratorSet = ! mdef.generator().equals(Constants.class);
		boolean isInterceptorsSet = mdef.interceptors().length != 0;

		boolean first =
			isControllerSet || isTemplateSet || isControllerDescSet ||
			isTemplateDescSet;
		boolean second = isDescSet || isGeneratorSet || isInterceptorsSet;
		if( first && second ) {
			String msg =
				"{controller,template,controllerDesc,templateDesc} on one side"+
				" and {desc,generator,interceptors} on the other side "+
				"cannot be set jointly in "+mdef+" for class "+cl.getName();
			throw new IllegalArgumentException(msg);
		}
		if( !first && !second ) {
			String msg =
				"Either {controller,template,controllerDesc,templateDesc} "+
				"or {desc,generator,interceptors} should be set in "+
				mdef+" for class "+cl.getName();
			throw new IllegalArgumentException(msg);
		}

		/*
		 * Check that the inheritance hierarchy contains at least one @Membrane
		 * annotated class.
		 */
		List<Class<?>> classes = new ArrayList<>();
		addSuperClasses(cl,classes);
		boolean membraneAnnotated = false;
		for (Class<?> c : classes) {
			Membrane m = c.getAnnotation(Membrane.class);
			if( m != null ) {
				membraneAnnotated = true;
				break;
			}
		}
		if( ! membraneAnnotated ) {
			String msg =
				"No @Membrane annotated class in the inheritance hierarchy of "+
				cl.getName();
			throw new IllegalArgumentException(msg);
		}
	}

	/**
	 * Return the generator class associated with the specified membrane
	 * definition. Return <code>null</code> if no generator class is defined.
	 */
	public static Class<?> getGenerator( Class<?> cl ) {

		List<Class<?>> classes = new ArrayList<>();
		addSuperClasses(cl,classes);

		for (Class<?> c : classes) {
			Membrane mdef = c.getAnnotation(Membrane.class);
			if( mdef != null ) {
				Class<?> generator = mdef.generator();
				if( ! generator.equals(Constants.class) ) {
					return generator;
				}
			}
		}

		// No generator specified
		return null;
	}

	/**
	 * Return the interceptor classes associated with the specified membrane
	 * definition. Return <code>null</code> if no interceptor class is defined.
	 */
	public static Class<?>[] getInterceptors( Class<?> cl ) {

		List<Class<?>> classes = new ArrayList<>();
		addSuperClasses(cl,classes);

		for (Class<?> c : classes) {
			Membrane mdef = c.getAnnotation(Membrane.class);
			if( mdef != null ) {
				Class<?>[] interceptors = mdef.interceptors();
				if( interceptors.length != 0 ) {
					return interceptors;
				}
			}
		}

		// No interceptor class specified
		return null;
	}

	/**
	 * Add the super membrane definitions of the specified membrane definition
	 * to the specified list. Recurse into the inheritance hierarchy.
	 */
	public static void addSuperClasses( Class<?> cl, List<Class<?>> classes ) {

		// Stop the recursion when java.lang.Object is reached
		if( cl.equals(Object.class) ) {
			return;
		}
		classes.add(cl);

		// Super class
		Class<?> sup = cl.getSuperclass();
		addSuperClasses(sup,classes);
	}

	/**
	 * Return the list of {@link Controller}-annotated fields declared in the
	 * inheritance hierarchy of the specified class.
	 */
	public static List<Field> getControllerAnnotatedFields( Class<?> cl ) {

		List<Class<?>> classes = new ArrayList<>();
		MembraneHelper.addSuperClasses(cl,classes);

		List<Field> fields = new ArrayList<>();
		Set<String> names = new HashSet<>();

		for (Class<?> c : classes) {
			Field[] fs = c.getDeclaredFields();
			for (Field f : fs) {

				// Skip fields with no @Controller annotation
				if( ! f.isAnnotationPresent(Controller.class) ) {
					continue;
				}

				String name = f.getName();
				/*
				 * Do not add the field if another one with the same name has
				 * already been declared previously in the inheritance
				 * hierarchy.
				 */
				if( ! names.contains(name) ) {
					fields.add(f);
					names.add(name);
				}
			}
		}

		return fields;
	}

	/**
	 * Return the interface type associated with the specified field and
	 * @{@link Requires} annotation.
	 *
	 * @param field     the field
	 * @param requires  the annotation
	 * @return          the interface type
	 * @throws IllegalArgumentException
	 *                  if, for collection interfaces, the type of the field does
	 *                  not match <code>Map&lt;String,V&gt;</code>
	 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
	 * @author Victor Noel <victor.noel@linagora.com>
	 * @since 2.6
	 */
	public static InterfaceType toInterfaceType( Field field, Requires requires ) {

	    final String name =
	        requires.name().length() == 0 ? field.getName() : requires.name();
	    final boolean isOptional = requires.contingency().isOptional();
	    final boolean isCollection = requires.cardinality().isCollection();
	    final boolean isExplicit = Constants.isExplicit(requires.signature());

	    final String signature;
	    if( isCollection ) {
	        final Type type = field.getGenericType();
	        final Class<?> clazz = field.getType();
	        final Type[] typeParameters;
	        if (type instanceof ParameterizedType) {
	            typeParameters =
	                ((ParameterizedType) type).getActualTypeArguments();
	        } else {
	            typeParameters = new Type[]{};
	        }
	        final Class<?> receivingSignatureType;
	        if (typeParameters.length == 2) {
	            if (typeParameters[1] instanceof ParameterizedType) {
	                receivingSignatureType = (Class<?>)
	                    ((ParameterizedType) typeParameters[1]).getRawType();
	            } else {
	                receivingSignatureType = (Class<?>) typeParameters[1];
	            }
	        } else {
	            receivingSignatureType = null;
	        }
	        if( !Map.class.equals(clazz)
	                || typeParameters.length != 2
	                || !String.class.equals(typeParameters[0])
	                || (isExplicit &&
	                    !requires.signature().equals(receivingSignatureType))) {
	            final String msg =
	                "For field "+field.getDeclaringClass().getName()+"#" +
	                field.getName()+" expected type is Map<String, " +
	                (isExplicit ? requires.signature().getName() : "?" ) +
	                ">. Got instead "+type+".";
	            throw new IllegalArgumentException(msg);
	        }
	        if( !isExplicit ) {
	            signature = receivingSignatureType.getName();
	        }
	        else {
	            signature = requires.signature().getName();
	        }
	    }
	    else {
	        if( !isExplicit ) {
	            signature = field.getType().getName();
	        }
	        else {
	            if (!requires.signature().equals(field.getType())) {
	                final String msg =
	                    "For field "+field.getDeclaringClass().getName()+"#" +
	                    field.getName()+" expected type is " +
	                    requires.signature().getName() +
	                    ">. Got instead "+field.getGenericType()+".";
	                throw new IllegalArgumentException(msg);
	            }
	            signature = requires.signature().getName();
	        }
	    }

		return new BasicInterfaceType(name,signature,true,isOptional,isCollection);
	}

	/**
	 * Return the server interface type associated with the specified
	 * @{@link Interface} annotation instance.
	 *
	 * @param itf  the annotation instance
	 * @return     the corresponding server interface type
	 * @since 2.6
	 */
	public static InterfaceType toInterfaceType( Interface itf ) {
	    final String name = itf.name();
	    final String signature = itf.signature().getName();
	    return new BasicInterfaceType(name,signature,false,false,false);
	}
}
