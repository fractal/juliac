/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.helper;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.fraclet.annotations.Interface;
import org.objectweb.fractal.julia.type.BasicInterfaceType;

/**
 * This class provides helper methods for the @{@link Interface} annotation
 * type.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public class InterfaceHelper {

	/**
	 * Return the {@link InterfaceType} corresponding to the specified
	 * @{@link Interface} instance.
	 */
	public static InterfaceType toInterfaceType( Interface itf ) {

		String name = itf.name();
		String signature = itf.signature().getName();

		InterfaceType it =
			new BasicInterfaceType(name,signature,false,false,false);

		return it;
	}
}
