/***
 * Juliac
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.conf;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.NoSuchControllerDescriptorException;
import org.objectweb.fractal.juliac.core.desc.ControllerDesc;
import org.objectweb.fractal.juliac.core.helper.MembraneHelper;

/**
 * A loader for membrane configuration data. Data is retrieved from {@link
 * org.objectweb.fractal.fraclet.extensions.Membrane} annotated classes.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.3.1
 */
public class JulietLoader extends HashMap<String,Class<?>>
implements MembraneLoaderItf {

	private static final long serialVersionUID = 8731060429069406161L;

	// -----------------------------------------------------------------------
	// Implementation of the JuliacModuleItf interface
	// -----------------------------------------------------------------------

	@Override
	public void init( JuliacItf jc ) throws IOException {
		jc.register(MembraneLoaderItf.class,this);
	}

	@Override
	public void close( JuliacItf jc ) throws IOException {
		jc.unregister(MembraneLoaderItf.class,this);
	}


	// -----------------------------------------------------------------------
	// Implementation of the MembraneLoaderItf interface
	// -----------------------------------------------------------------------

	@Override
	public Class<?> put( String ctrlDesc, Object o ) {
		/*
		 * Check that cl is a valid membrane definition.
		 * Else throws IllegalArgumentException.
		 */
		Class<?> cl = (Class<?>) o;
		MembraneHelper.check(cl);
		return super.put(ctrlDesc,cl);
	}

	@Override
	public Object get( String ctrlDesc ) {
		return super.get(ctrlDesc);
	}

	@Override
	public boolean containsKey( String key ) {
		return super.containsKey(key);
	}

	@Override
	public InterfaceType[] getMembraneType( ComponentType ct, String ctrlDesc ) {

		if( ! containsKey(ctrlDesc) ) {
			throw new NoSuchControllerDescriptorException(ctrlDesc);
		}

		Class<?> cl = super.get(ctrlDesc);
		List<Field> fields = MembraneHelper.getControllerAnnotatedFields(cl);

		InterfaceType[] its = new InterfaceType[fields.size()];
		for (int i = 0; i < fields.size(); i++) {
			Field field = fields.get(i);
			Controller controller = field.getAnnotation(Controller.class);
			String name = controller.name();
			String signature = field.getType().getName();
			its[i] = new BasicInterfaceType(name,signature,false,false,false);
		}

		return its;
	}

	@Override
	public List<ControllerDesc> getCtrlImplLayers(
		ComponentType ct, String ctrlDesc ) {

		if( ! containsKey(ctrlDesc) ) {
			throw new NoSuchControllerDescriptorException(ctrlDesc);
		}

		Class<?> cl = super.get(ctrlDesc);
		List<Field> fields = MembraneHelper.getControllerAnnotatedFields(cl);
		List<ControllerDesc> cdescs = new ArrayList<>();

		for (Field field : fields) {

			Controller controller = field.getAnnotation(Controller.class);
			String impl = controller.impl();
			Class<?>[] mixins = controller.mixins();
			List<String> names = new ArrayList<>();
			for (Class<?> mixin : mixins) {
				String name = mixin.getName();
				names.add(name);
			}

			ControllerDesc cdesc = new ControllerDesc(impl,names,null);
			cdescs.add(cdesc);
		}

		return cdescs;
	}

	@Override
	public String getInterceptorClassGeneratorName( String ctrlDesc ) {

		if( ! containsKey(ctrlDesc) ) {
			throw new NoSuchControllerDescriptorException(ctrlDesc);
		}

		Class<?> cl = super.get(ctrlDesc);
		Class<?> generator = MembraneHelper.getGenerator(cl);
		return generator.getName();
	}

	@Override
	public String[] getInterceptorSourceCodeGeneratorNames( String ctrlDesc ) {

		if( ! containsKey(ctrlDesc) ) {
			throw new NoSuchControllerDescriptorException(ctrlDesc);
		}

		Class<?> cl = super.get(ctrlDesc);
		Class<?>[] interceptors = MembraneHelper.getInterceptors(cl);
		if( interceptors == null ) {
			return null;
		}

		String[] strs = new String[interceptors.length];
		for (int i = 0; i < interceptors.length; i++) {
			strs[i] = interceptors[i].getName();
		}

		return strs;
	}
}
