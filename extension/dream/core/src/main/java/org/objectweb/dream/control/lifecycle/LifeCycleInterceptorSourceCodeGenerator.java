/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.dream.control.lifecycle;

import java.lang.reflect.Method;

import org.objectweb.fractal.juliac.core.proxy.SimpleSourceCodeGenerator;
import org.objectweb.fractal.juliac.core.proxy.SimpleSourceCodeGeneratorInterceptionType;
import org.objectweb.fractal.juliac.core.proxy.SimpleSourceCodeGeneratorMode;

/**
 * Lifecycle source code generator for Dream.
 *
 * @see org.objectweb.dream.control.lifecycle.LifeCycleInterceptorGenerator
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class LifeCycleInterceptorSourceCodeGenerator
extends SimpleSourceCodeGenerator {

	public LifeCycleInterceptorSourceCodeGenerator() {
		super();
	}

	protected SimpleSourceCodeGeneratorMode getMode() {
		return SimpleSourceCodeGeneratorMode.IN;
	}

	protected String getControllerInterfaceName() {
		return "lifecycle-controller";
	}

	protected String getPreMethodName() {
		return "addCurrentThread";
	}

	protected String getPostMethodName() {
		return "removeCurrentThread";
	}

	protected Class<?> getContextType() {
		return Integer.TYPE;
	}

	protected String getMethodName(Method m) {
		return "";
	}

	protected SimpleSourceCodeGeneratorInterceptionType getInterceptionType(Method m) {
		return SimpleSourceCodeGeneratorInterceptionType.FINALLY;
	}

	public String getClassNameSuffix() {

		String suffix = "DreamLCInterceptor";

		if( ! mergeable ) {
			/*
			 * Append a hash code which corresponds to the name of the lifecycle
			 * controller class. The reason is that the code of each lifecycle
			 * interceptor relies on the name of the lifecycle controller class.
			 * Hence, a different interceptor class must be generated each time
			 * a new lifecycle controller class is encountered in a membrane.
			 */
			Class<?> ctrlImpl = getCtrlImpl();
			String name = ctrlImpl.getName();
			int hash = name.hashCode();
			String hexhash = Integer.toHexString(hash);
			suffix += hexhash;
		}

		return suffix;
	}

	@Override
	public boolean match() {
		// Skip control interface /logger-controller-register
		String itname = it.getFcItfName();
		if( itname.equals("/logger-controller-register") ) {
			return false;
		}
		return super.match();
	}
}
