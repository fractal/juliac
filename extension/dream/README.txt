This extension provides a support for Dream in Juliac.

To run an example, from this directory, type:
	cd examples/original/xxx
	mvn compile spoon:recompile juliac:compile exec:java

where xxx is one of:
- cosmos-helloworld: Cosmos (uses Dream) HelloWorld example
- dream-core-helloworld: Dream Core HelloWorld example
- dream-core-activity: Dream Core Activity example


References
----------
- Cosmos	: http://picoforge.int-evry.fr/projects/svn/cosmos
- Dream		: http://dream.ow2.org
