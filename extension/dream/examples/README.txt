The original/ and spooned/ directories contain the three same examples
illustrating the use of Dream and Cosmos with Juliac.

The original/ directory contains the original source code of the examples. To
run the examples, type:
	mvn clean compile spoon:recompile juliac:compile exec:java

The spooned/ directory contains the code of the examples transformed by Spoon.
This corresponds to the transformation performed by spoon:recompile. The purpose
is to have a version of the examples which can be better integrated with the
global Maven build and test process. The examples contained in the original/
directory can certainly be integrated similarily. However, since I did not dig
into the intricacies of the Maven lifecycle customizing procedure, I do not know
how to have both spoon:recompile and juliac:compile cohabit seamlessly in the
same module with the guarantee that the former will be always executed first.
The examples contained in the spooned/ directory are then a poor man's solution
for having only one Maven plugin per module and a safer Maven build and test
process. To run the examples, type:
	mvn test
