/**
 * Dream
 * Copyright (C) 2003-2021 Inria Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s):
 */

package helloworld;

import org.objectweb.dream.Push;
import org.objectweb.dream.control.activity.manager.TaskManager;
import org.objectweb.dream.control.activity.task.AbstractTask;
import org.objectweb.dream.dreamannotation.DreamComponent;
import org.objectweb.dream.message.ChunkFactoryReference;
import org.objectweb.dream.message.MessageManagerType;
import org.objectweb.dream.message.Message;
import org.objectweb.fractal.fraclet.annotation.annotations.Requires;

/** Client component abstract implementation. */
@DreamComponent()
public abstract class AbstractClientImpl
{
  @Requires(name = "out-push")
  Push           outPushItf;
  @Requires(name = "message-manager")
  MessageManagerType messageManager;
  @Requires(name = "task-manager")
  protected TaskManager    taskManagerItf;

  protected class ClientTask extends AbstractTask
  {

	private String message;

	/**
	 * Constructor
	 */
	public ClientTask(String message)
	{
	  super("client-task-" + message);
	  this.message = message;
	}

	/**
	 * @see org.objectweb.dream.control.activity.task.Task#execute(Object)
	 */
	public Object execute(Object hints) throws InterruptedException
	{
	  try
	  {
		Message msg = messageManager.createMessage();
		ChunkFactoryReference<HelloWorldChunk> chunkFactory = messageManager
			.getChunkFactory(HelloWorldChunk.class);
		HelloWorldChunk chunk = messageManager.createChunk(chunkFactory);
		chunk.setMessage(message);
		messageManager.addChunk(msg, HelloWorldChunk.DEFAULT_NAME, chunk);
		outPushItf.push(msg);
	  }
	  catch (Exception e)
	  {
		e.printStackTrace();
		return STOP_EXECUTING;
	  }
	  Thread.sleep(1000);
	  return EXECUTE_AGAIN;
	}
  }
}
