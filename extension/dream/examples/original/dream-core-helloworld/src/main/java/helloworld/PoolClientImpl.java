/**
 * Dream Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s):
 */

package helloworld;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.dream.control.activity.Util;
import org.objectweb.dream.control.activity.manager.ThreadPoolManager;
import org.objectweb.dream.control.activity.task.AbstractTask;
import org.objectweb.dream.dreamannotation.DreamComponent;
import org.objectweb.dream.dreamannotation.DreamMonolog;
import org.objectweb.dream.dreamannotation.util.DreamLifeCycleType;
import org.objectweb.dream.dreamannotation.DreamLifeCycle;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.fraclet.annotation.annotations.LifeCycle;
import org.objectweb.fractal.fraclet.annotation.annotations.Service;
import org.objectweb.fractal.fraclet.annotation.annotations.type.LifeCycleType;
import org.objectweb.fractal.julia.control.lifecycle.ChainedIllegalLifeCycleException;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Client implementation, using thread pool tasks.
 */
@DreamComponent(controllerDesc = "activeDreamPrimitive")
public class PoolClientImpl extends AbstractClientImpl
{
  @Service
  Component ref;

  @DreamMonolog
  Logger logger;

  AbstractTask task = new ClientTask("Hello World task");

  // -------------------------------------------------------------------------
  // Overridden methods
  // -------------------------------------------------------------------------

  /**
   * @see AbstractComponent#beforeFirstStart(Component)
   */
  @DreamLifeCycle(on=DreamLifeCycleType.FIRST_START)
  protected void beforeFirstStart(Component componentItf)
	  throws IllegalLifeCycleException
  {
	try
	{
	  Map<String, Object> m = new HashMap<String, Object>();
	  m.put("thread", "pool");
	  m.put("threadPool.capacity", new Integer(2));
	  Util.addTask(componentItf, task, m);
	  logger.log(BasicLevel.DEBUG, "tasks added");
	}
	catch (Exception e)
	{
	  throw new IllegalLifeCycleException("Can't add task");
	}
  }

  /**
   * @see org.objectweb.dream.AbstractComponent#startFc()
   */
  @SuppressWarnings("unused")
  @LifeCycle(on=LifeCycleType.START)
  protected void startFcHandler() throws IllegalLifeCycleException
  {
	// add two threads in the thread pool.
	try
	{
	  ThreadPoolManager threadPoolManager = (ThreadPoolManager) task
		  .getControlItf();
	  threadPoolManager.addThread(task);
	  threadPoolManager.addThread(task);
	}
	catch (Exception e)
	{
	  throw new ChainedIllegalLifeCycleException(e, null,
		  "An error occurs while retrieving task control interface");
	}
  }
}
