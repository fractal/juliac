/**
 * Dream
 * Copyright (C) 2003-2021 Inria Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s):
 */

package helloworld;

import org.objectweb.dream.PushException;
import org.objectweb.dream.control.activity.Util;
import org.objectweb.dream.dreamannotation.DreamComponent;
import org.objectweb.dream.dreamannotation.DreamLifeCycle;
import org.objectweb.dream.dreamannotation.util.DreamLifeCycleType;
import org.objectweb.dream.dreamannotation.DreamMonolog;
import org.objectweb.dream.message.ChunkFactoryReference;
import org.objectweb.dream.message.Message;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.fraclet.annotation.annotations.LifeCycle;
import org.objectweb.fractal.fraclet.annotation.annotations.Service;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Basic Client implementation, using two mono-threaded tasks.
 */
@DreamComponent(controllerDesc = "activeDreamPrimitive", needAsyncStart = true)
public class ClientImpl extends AbstractClientImpl
{
  @Service
  Component ref;

  @DreamMonolog
  Logger logger;

  //-------------------------------------------------------------------------
  // Overridden methods
  // -------------------------------------------------------------------------


  /**
   * @see org.objectweb.dream.AbstractComponent#startFcHandler()
   */
  @LifeCycle
  protected void startFcHandler() throws IllegalLifeCycleException
  {
	Message msg = messageManager.createMessage();
	ChunkFactoryReference<HelloWorldChunk> chunkFactory = messageManager.getChunkFactory(HelloWorldChunk.class);
	HelloWorldChunk chunk = messageManager.createChunk(chunkFactory);
	chunk.setMessage("in start");
	messageManager.addChunk(msg, HelloWorldChunk.DEFAULT_NAME, chunk);
	try
	{
	  outPushItf.push(msg);
	}
	catch (PushException e)
	{
	  e.printStackTrace();
	}
  }

  /**
   * @see org.objectweb.dream.AbstractComponent#beforeFirstStart(Component)
   */
  @DreamLifeCycle(on=DreamLifeCycleType.FIRST_START)
  protected void beforeFirstStart(Component componentItf)
	  throws IllegalLifeCycleException
  {
	try
	{
	  Util.addTask(componentItf, new ClientTask("Hello World task1"), null);
	  Util.addTask(componentItf, new ClientTask("Hello World task2"), null);
	  logger.log(BasicLevel.DEBUG, "tasks added");
	}
	catch (Exception e)
	{
	  throw new IllegalLifeCycleException("Can't add task");
	}
  }
}
