/**
 * Dream
 * Copyright (C) 2003-2021 Inria Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s):
 */

package helloworld;

import org.objectweb.dream.Push;
import org.objectweb.dream.PushException;
import org.objectweb.dream.dreamannotation.DreamComponent;
import org.objectweb.dream.message.MessageManagerType;
import org.objectweb.dream.message.Message;
import org.objectweb.fractal.fraclet.annotation.annotations.Interface;
import org.objectweb.fractal.fraclet.annotation.annotations.Provides;
import org.objectweb.fractal.fraclet.annotation.annotations.Requires;

/** Server component implementation. */
@DreamComponent(controllerDesc = "dreamPrimitive")
@Provides(interfaces=@Interface(name="in-push", signature = org.objectweb.dream.Push.class))
public class ServerImpl implements Push
{
  @Requires(name = "message-manager")
  MessageManagerType messageManager;

  /**
   * @see Push#push(Message)
   */
  public void push(Message message) throws PushException
  {
	HelloWorldChunk chunk = messageManager.getChunk(message,
		HelloWorldChunk.DEFAULT_NAME);
	if (chunk == null)
	{
	  throw new PushException("Unable to find HelloWorld chunk");
	}
	System.out.println("Thread=" + Thread.currentThread().getName() + " : "
		+ chunk.getMessage());
	messageManager.deleteMessage(message);
  }
}
