/**
 * Dream
 * Copyright (C) 2003-2021 Inria Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact : dream@objectweb.org
 *
 * Initial developer(s): Vivien Quema
 * Contributor(s): Romain Lenglet
 */

package activity;

import java.util.HashMap;

import org.objectweb.dream.InterruptedPullException;
import org.objectweb.dream.Pull;
import org.objectweb.dream.PullException;
import org.objectweb.dream.control.activity.Util;
import org.objectweb.dream.control.activity.manager.TaskManager;
import org.objectweb.dream.control.activity.manager.ThreadPoolManager;
import org.objectweb.dream.control.activity.task.AbstractTask;
import org.objectweb.dream.dreamannotation.DreamComponent;
import org.objectweb.dream.dreamannotation.DreamLifeCycle;
import org.objectweb.dream.dreamannotation.DreamMonolog;
import org.objectweb.dream.dreamannotation.util.DreamLifeCycleType;
import org.objectweb.dream.message.MessageManagerType;
import org.objectweb.dream.message.Message;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.fraclet.annotation.annotations.Requires;
import org.objectweb.fractal.fraclet.annotation.annotations.Service;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Client component implementation
 */
@DreamComponent(controllerDesc = "activeDreamPrimitive")
public class Client
{
  @Service
  Component ref;

  @DreamMonolog
  Logger logger;

  @Requires(name = "in-pull")
  protected Pull           inPullItf;
  @Requires(name = "message-manager")
  protected MessageManagerType messageManagerItf;
  @Requires(name = "task-manager")
  protected TaskManager    taskManagerItf;

  // ---------------------------------------------------------------------------
  // Overridden methods
  // ---------------------------------------------------------------------------

  /**
   * @see AbstractComponent#beforeFirstStart(Component)
   */
  @DreamLifeCycle(on = DreamLifeCycleType.FIRST_START)
  protected void beforeFirstStart(Component componentItf)
	  throws IllegalLifeCycleException
  {
	try
	{
	  HashMap<String, Object> context = new HashMap<String, Object>();
	  context.put("thread", "pool");
	  Util.addTask(componentItf, new PullTask(), context);
	  logger.log(BasicLevel.INFO, "task added");
	}
	catch (Exception e)
	{
	  throw new IllegalLifeCycleException("Can't add task");
	}
  }

  protected class PullTask extends AbstractTask
  {
	/**
	 * Constructor
	 *
	 */
	PullTask()
	{
	  super("PullTask");
	}

	// -------------------------------------------------------------------------
	// Implementation of the Task interface
	// -------------------------------------------------------------------------

	/**
	 * @see org.objectweb.dream.control.activity.task.Task#execute(Object)
	 */
	public Object execute(Object hints) throws InterruptedException
	{
	  Message message = null;
	  try
	  {
		message = inPullItf.pull();
		System.out.println("PullComponent has received a message : " + message);
		messageManagerItf.deleteMessage(message);
	  }
	  catch (PullException e)
	  {
		System.out.println("An exception occurred while pulling a message : "
			+ e);
		if (e instanceof InterruptedPullException)
		{
		  System.out.println("The pull method has been interrupted");
		}
		return STOP_EXECUTING;
	  }
	  return EXECUTE_AGAIN;
	}

	// -------------------------------------------------------------------------
	// Overridden method
	// -------------------------------------------------------------------------

	/**
	 * @see org.objectweb.dream.control.activity.task.Task#registered(Object)
	 */
	public void registered(Object controlItf)
	{
	  if (controlItf instanceof ThreadPoolManager)
	  {
		ThreadPoolManager tpm = (ThreadPoolManager) controlItf;
		try
		{
		  tpm.addThread(this);
		}
		catch (Exception e)
		{
		  e.printStackTrace();
		}
	  }
	}
  }
}
