/**
 * Dream
 * Copyright (C) 2003-2021 Inria Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s): Romain Lenglet
 */

package activity;

import org.objectweb.dream.InterruptedPullException;
import org.objectweb.dream.Pull;
import org.objectweb.dream.PullException;
import org.objectweb.dream.dreamannotation.DreamComponent;
import org.objectweb.dream.dreamannotation.DreamMonolog;
import org.objectweb.dream.message.Message;
import org.objectweb.dream.util.EmptyStringArray;
import org.objectweb.dream.util.Error;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.fraclet.annotation.annotations.Interface;
import org.objectweb.fractal.fraclet.annotation.annotations.Provides;
import org.objectweb.fractal.fraclet.annotation.annotations.Service;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Server component providing a pull interface. The pull method waits on
 * <code>this</code>.
 */
@DreamComponent
@Provides(interfaces=@Interface(name="out-pull", signature = org.objectweb.dream.Pull.class))
public class Server implements Pull
{
  @Service
  Component weaveableC;

  @DreamMonolog
  Logger logger;

  boolean reentrant = false;

  /**
   * @see org.objectweb.dream.Pull#pull()
   */
  public Message pull() throws PullException
  {
	if (!reentrant)
	{
	  reentrant = true;
	  Message m = null;
	  try
	  {
		logger.log(BasicLevel.INFO, "reentrant call");
		m = ((Pull) weaveableC.getFcInterface(OUT_PULL_ITF_NAME)).pull();
	  }
	  catch (InterruptedPullException e)
	  {
		logger.log(BasicLevel.INFO, "Wait interrupted 1");
		throw e;
	  }
	  catch (NoSuchInterfaceException e)
	  {
		Error.bug(logger, e);
	  }
	  reentrant = false;
	  return m;
	}
	synchronized (this)
	{
	  try
	  {
		logger.log(BasicLevel.INFO, "Waiting");
		this.wait();
		logger.log(BasicLevel.INFO, "End of wait");
	  }
	  catch (InterruptedException e)
	  {
		logger.log(BasicLevel.INFO, "Wait interrupted 2");
		throw new InterruptedPullException(e);
	  }
	  return null;
	}
  }

  /**
   * @see org.objectweb.fractal.api.control.BindingController#listFc()
   */
  public String[] listFc()
  {
	return EmptyStringArray.EMPTY_STRING_ARRAY;
  }

}
