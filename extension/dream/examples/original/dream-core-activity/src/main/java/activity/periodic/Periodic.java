/**
 * Dream
 * Copyright (C) 2003-2021 Inria Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s):
 */

package activity.periodic;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.dream.control.activity.Util;
import org.objectweb.dream.control.activity.manager.TaskManager;
import org.objectweb.dream.control.activity.task.AbstractTask;
import org.objectweb.dream.control.activity.task.Task;
import org.objectweb.dream.dreamannotation.DreamComponent;
import org.objectweb.dream.dreamannotation.DreamLifeCycle;
import org.objectweb.dream.dreamannotation.DreamMonolog;
import org.objectweb.dream.dreamannotation.util.DreamLifeCycleType;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.fraclet.annotation.annotations.Requires;
import org.objectweb.fractal.fraclet.annotation.annotations.Service;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * This class shows an example of component with a periodic task.
 */
@DreamComponent(controllerDesc = "activeDreamPrimitive")
public class Periodic
{
  @Service
  Component weaveableC;

  @DreamMonolog
  Logger logger;


  @Requires(name = "task-manager")
  protected TaskManager    taskManagerItf;

  class PeriodicTask extends AbstractTask
  {
	/**
	 * Contructor
	 * @param name name of the task
	 */
	PeriodicTask(String name)
	{
	  super(name);
	}

	/**
	 * @see Task#execute(Object)
	 */
	public Object execute(Object hints) throws InterruptedException
	{
	  logger.log(BasicLevel.INFO, getFcName());
	  return EXECUTE_AGAIN;
	}
  }

  /**
   * @see AbstractComponent#beforeFirstStart(Component)
   */
  @DreamLifeCycle(on = DreamLifeCycleType.FIRST_START)
  protected void beforeFirstStart(Component componentItf)
	  throws IllegalLifeCycleException
  {
	try
	{
	  Task t1 = new PeriodicTask("task 1");
	  Map<String, Object> hints1 = new HashMap<String, Object>();
	  hints1.put("period", new Long(1000));
	  Util.addTask(componentItf, t1, hints1);

	  Task t2 = new PeriodicTask("task 2");
	  Map<String, Object> hints2 = new HashMap<String, Object>();
	  hints2.put("period", new Long(1500));
	  Util.addTask(componentItf, t2, hints2);
	}
	catch (Exception e)
	{
	  logger.log(BasicLevel.ERROR, "Unable to register tasks", e);
	}
  }
}
