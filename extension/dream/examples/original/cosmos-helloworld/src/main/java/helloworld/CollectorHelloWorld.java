/**
 COSMOS: COntext entitieS coMpositiOn and Sharing
 Copyright: Copyright (C) 2008
 Contact: cosmos@picoforge.int-evry.fr

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA

 Initial developer(s): Denis Conan.
 Contributor(s):
 */
package helloworld;

import org.objectweb.dream.message.Message;
import org.objectweb.dream.message.MessageManagerType;
import org.objectweb.fractal.fraclet.annotations.Component;

import cosmos.core.ComputingException;
import cosmos.core.InitChunkException;
import cosmos.core.InitMessageException;
import cosmos.core.LegacyCollectorWrapper;
import cosmos.core.util.Util;

/**
 * Fractal primitive component, which is a stub legacy collector wrapper for
 * unitary tests. It provides an integer value in a dummy chunk of type
 * <code>HelloWorldChunk</code>. The interface
 * <code>NeedAsyncStartController</code> is for the Dream membrane
 * <code>dreamPrimitive</code>: It is not introduced by
 * <code>dream-annotation</code> since we prefer using the Fraclet-java
 * annotation <code>Component</code> to the Dream-annotation annotation
 * <code>DreamComponent</code>.
 *
 * @author Denis Conan
 */
@Component
public class CollectorHelloWorld extends LegacyCollectorWrapper {

	/**
	 * Initializes <code>output</code> with an initial state. The initial
	 * <code>output</code> updated by this method is used by the method
	 * <code>doComputeNewOutputMessageContent</code>.
	 *
	 * Assumption: The <code>output</code> is created before entering this
	 * method.
	 *
	 * @param legacyData
	 *            Context data to be pushed from the legacy wrapper collector.
	 * @param msgMngr
	 *            reference to the reference manager of the current context
	 *            node.
	 * @param output
	 *            message corresponding to the output of the current context
	 *            collector.
	 * @throws InitMessageException
	 *             Cosmos exception.
	 */
	protected final void doInitialiseOutputMessage(final Object[] legacyData,
			final MessageManagerType msgMngr, final Message output)
			throws InitMessageException {
		try {
			HelloWorldChunk chunk = Util.doCreateChunk(HelloWorldChunk.class,
					msgMngr, output);
			chunk.recycle();
		} catch (InitChunkException e) {
			e.printStackTrace();
			throw new InitMessageException("Problem in creating a "
					+ HelloWorldChunk.class);
		}
	}

	/**
	 * Computes the new content of the output message.
	 *
	 * @param legacyData
	 *            Context data to be pushed from the legacy wrapper collector.
	 * @param msgMngr
	 *            reference to the reference manager of the current context
	 *            node.
	 * @param output
	 *            message corresponding to the output of the current context
	 *            collector.
	 * @param isInPull
	 *            boolean stating whether the call is performed in a pull call
	 *            (value <tt>true</tt>) or in a push call (value <tt>false</tt>
	 *            ).
	 * @throws ComputingException
	 *             the COSMOS exception to be thrown
	 */
	@Override
	protected final void doComputeNewOutputMessageContent(
			final Object[] legacyData, final MessageManagerType msgMngr,
			final Message output, final boolean isInPull)
			throws ComputingException {
		LOGGER.info(this.getClass().getName()
				+ " method doComputeNewMessageContent");
		HelloWorldChunk hwc = msgMngr.getChunk(output,
				HelloWorldChunk.DEFAULT_NAME);
		hwc.recycle();
	}
}
