package helloworld;

import java.util.HashMap;
import java.util.logging.LogManager;

import org.objectweb.dream.Pull;
import org.objectweb.dream.PullException;
import org.objectweb.dream.message.Message;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.util.Fractal;
import org.objectweb.util.monolog.Monolog;
import org.objectweb.util.monolog.api.LoggerFactory;

/**
 * This class launches the helloworld example.
 *
 * @author Denis Conan
 */
public final class Main {

	/**
	 * Attribute to configure <code>Dream</code> logging using
	 * <code>Monolog</code>.
	 */
	public static final LoggerFactory LOGGER_FACTORY = Monolog
			.getMonologFactory("defaultMonologForDream.properties");

	/**
	 * Empty constructor of this utilitary class.
	 */
	private Main() {

	}

	/**
	 * Entry point method of the example.
	 *
	 * @param args
	 *            Command line input arguments
	 * @throws Exception
	 *             Returned exception
	 */
	public static void main(final String[] args) throws Exception {
		LogManager.getLogManager().readConfiguration(
				new java.io.FileInputStream(
						System.getProperty("java.util.logging.config.file")));
		Component cn = null;
		Message msg = null;
		Factory factory = FactoryFactory
				.getFactory(FactoryFactory.FRACTAL_BACKEND);
		cn = (Component) factory.newComponent("helloworld.HelloWorld",
				new HashMap<String, Object>());
		System.out.println("This is the end!");
		Fractal.getLifeCycleController(cn).startFc();
		try {
			msg = ((Pull) cn.getFcInterface("pull-obs-out")).pull();
		} catch (PullException e) {
			System.out.println("Context policy not initialised, yet");
		}
		if (msg != null) {
			HelloWorldChunk hwc = msg.getOwner().getChunk(msg,
					HelloWorldChunk.DEFAULT_NAME);
			System.out.println(hwc.getValue());
			msg.getOwner().deleteMessage(msg);
		}
		Fractal.getLifeCycleController(cn).stopFc();
	}
}
