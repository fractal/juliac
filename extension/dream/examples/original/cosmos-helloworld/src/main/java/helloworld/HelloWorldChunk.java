/**
 COSMOS: COntext entitieS coMpositiOn and Sharing
 Copyright: Copyright (C) 2008
 Contact: cosmos@picoforge.int-evry.fr

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA

 Initial developer(s): Denis Conan.
 Contributor(s):
 */

package helloworld;

import cosmos.core.chunk.CosmosAbstractChunk;

/**
 * A chunk containing a <tt>String</tt>.
 *
 * @author Denis Conan
 */
public class HelloWorldChunk extends CosmosAbstractChunk<HelloWorldChunk,String> {

	/**
	 * Serial version Unique Identifier for this chunk.
	 */
	private static final long serialVersionUID = -272443505733063799L;

	/**
	 * Default name of the chunk.
	 */
	public static final String DEFAULT_NAME = "helloworld-chunk";

	/**
	 * @see org.objectweb.dream.pool.Recyclable#recycle()
	 */
	public final void recycle() {
		setValue(new String("Hello World!"));
	}
}
