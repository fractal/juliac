package helloworld;

import java.util.logging.LogManager;

import org.objectweb.dream.Pull;
import org.objectweb.dream.PullException;
import org.objectweb.dream.message.Message;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.util.Fractal;

public final class JuliacMain {

	public static void main(String[] args) throws Exception {

		LogManager.getLogManager().reset(); // disable java logging
		Component cn = null;
		Message msg = null;

		@SuppressWarnings("unchecked")
		Class<Factory> cl = (Class<Factory>) Class.forName("helloworld.HelloWorld");
		Factory factory = cl.newInstance();
		cn = factory.newFcInstance();

		System.out.println("This is the end!");
		Fractal.getLifeCycleController(cn).startFc();
		try {
			msg = ((Pull) cn.getFcInterface("pull-obs-out")).pull();
		} catch (PullException e) {
			System.out.println("Context policy not initialised, yet");
		}
		if (msg != null) {
			HelloWorldChunk hwc = msg.getOwner().getChunk(msg,
					HelloWorldChunk.DEFAULT_NAME);
			System.out.println(hwc.getValue());
			msg.getOwner().deleteMessage(msg);
		}
		Fractal.getLifeCycleController(cn).stopFc();
	}
}

