/**
 * Dream
 * Copyright (C) 2003-2021 Inria Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s): Vivien Quema
 */

package helloworld;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import org.objectweb.dream.message.AbstractChunk;

/**
 * A chunk containing a string message
 */
public class HelloWorldChunk extends AbstractChunk<HelloWorldChunk>
{

  private static final long  serialVersionUID = 3257570624300986424L;

  /** The default name of chunk of this type */
  public static final String DEFAULT_NAME     = "helloworld-chunk";

  private String             message;

  // ---------------------------------------------------------------------------
  // Implementation of the HelloWorldChunk interface
  // ---------------------------------------------------------------------------

  /**
   * Returns the message.
   *
   * @return the message.
   */
  public final String getMessage()
  {
	return message;
  }

  /**
   * Sets the message.
   *
   * @param message the message.
   */
  public final void setMessage(String message)
  {
	this.message = message;
  }

  // ---------------------------------------------------------------------------
  // Implementation of the Chunk abstract methods
  // ---------------------------------------------------------------------------

  /**
   * Create a new chunk
   */
  @Override
  protected HelloWorldChunk newChunk()
  {
	return new HelloWorldChunk();
  }

  /**
   * transfert the state of the chunk
   */
  @Override
  protected void transfertStateTo(HelloWorldChunk newInstance)
  {
	newInstance.message = message;
  }

  // ---------------------------------------------------------------------------
  // Implementation of the Recyclable interface
  // ---------------------------------------------------------------------------

  /**
   * @see org.objectweb.dream.pool.Recyclable#recycle()
   */
  public void recycle()
  {
	message = null;
  }

  // ---------------------------------------------------------------------------
  // Implementation of the Externalizable interface
  // ---------------------------------------------------------------------------

  /**
   * @see java.io.Externalizable#readExternal(java.io.ObjectInput)
   */
  public void readExternal(ObjectInput in) throws IOException,
	  ClassNotFoundException
  {
	message = in.readUTF();
  }

  /**
   * @see java.io.Externalizable#writeExternal(java.io.ObjectOutput)
   */
  public void writeExternal(ObjectOutput out) throws IOException
  {
	out.writeUTF(message);
  }

}
