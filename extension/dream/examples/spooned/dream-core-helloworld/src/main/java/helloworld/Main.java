/**
 * Dream
 * Copyright (C) 2003-2021 Inria Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s): Vivien Quema
 */

package helloworld;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.dream.adl.FactoryFactory;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.util.Fractal;

/** Launcher class. */
public final class Main
{

  /**
   * Private constructor
   *
   */
  private Main()
  {
  }

  /**
   * Entry point
   *
   * @param args if no argument or one equals to <code>-mono</code> use the
   *          {@link Client}. If equals to <code>-pool</code> the the
   *          {@link PoolClientImpl}.
   * @throws Exception if an exception occurs
   */
  public static void main(String[] args) throws Exception
  {
	if (args.length > 1)
	{
	  usage();
	}
	String clientClass = null;
	if (args.length == 0)
	{
	  clientClass = ClientImpl.class.getName();
	}
	else
	{
	  String arg = args[0];
	  if (arg.equalsIgnoreCase("-mono"))
	  {
		clientClass = "helloworld.ClientImpl";
	  }
	  else if (arg.equalsIgnoreCase("-pool"))
	  {
		clientClass = "helloworld.PoolClientImpl";
	  }
	  else
	  {
		usage();
	  }
	}

	Factory factory = FactoryFactory.getFactory();
	Map<String, Object> context = new HashMap<String, Object>();
	context.put("clientImpl", clientClass);
	Component composite = (Component) factory.newComponent(
		"helloworld.Helloworld", context);
	Fractal.getLifeCycleController(composite).startFc();
	Thread.sleep(3000);
	System.out.println("call to stop...");
	Fractal.getLifeCycleController(composite).stopFc();
	System.out.println("stop returned");
  }

  /**
   * Usage of the example
   *
   */
  private static void usage()
  {
	System.out.println("Usage : HelloWorld [-mono|-pool]");
	System.out.println("Run the HelloWorld example of Dream core framework");
	System.out.println("Arguments : ");
	System.out
		.println("  -mono : use the mono-threaded client component (default) ");
	System.out.println("  -pool : use the client component using thread pool");
	System.exit(1);
  }
}
