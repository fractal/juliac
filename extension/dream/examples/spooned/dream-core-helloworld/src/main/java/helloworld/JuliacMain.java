package helloworld;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.Factory;

public final class JuliacMain {

	public static void main(String[] args) throws Exception {

		@SuppressWarnings("unchecked")
		Class<Factory> cl = (Class<Factory>) Class.forName("helloworld.Helloworld");
		Factory factory = cl.newInstance();
		Component composite = factory.newFcInstance();

		org.objectweb.fractal.util.Fractal.getLifeCycleController(composite).startFc();
		java.lang.Thread.sleep(3000);
		System.out.println("call to stop...");
		org.objectweb.fractal.util.Fractal.getLifeCycleController(composite).stopFc();
		System.out.println("stop returned");
	}
}

