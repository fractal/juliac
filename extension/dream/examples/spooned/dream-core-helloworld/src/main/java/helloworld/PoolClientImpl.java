/**
 * Dream Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s):
 */

package helloworld;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.dream.InitializationException;
import org.objectweb.dream.Push;
import org.objectweb.dream.control.activity.manager.TaskManager;
import org.objectweb.dream.control.activity.manager.ThreadPoolManager;
import org.objectweb.dream.control.activity.task.AbstractTask;
import org.objectweb.dream.control.lifecycle.BeforeFirstStartLifeCycle;
import org.objectweb.dream.control.lifecycle.NeedAsyncStartController;
import org.objectweb.dream.control.logger.Loggable;
import org.objectweb.dream.control.logger.LoggerControllerRegister;
import org.objectweb.dream.dreamannotation.DreamComponent;
import org.objectweb.dream.dreamannotation.DreamLifeCycle;
import org.objectweb.dream.dreamannotation.util.DreamLifeCycleType;
import org.objectweb.dream.message.MessageManagerType;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.fraclet.annotation.annotations.LifeCycle;
import org.objectweb.fractal.fraclet.annotation.annotations.Service;
import org.objectweb.fractal.fraclet.annotation.annotations.type.LifeCycleType;
import org.objectweb.fractal.julia.control.lifecycle.ChainedIllegalLifeCycleException;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * Client implementation, using thread pool tasks.
 */
@DreamComponent(controllerDesc = "activeDreamPrimitive")
public class PoolClientImpl extends AbstractClientImpl
implements
	BeforeFirstStartLifeCycle, NeedAsyncStartController, Loggable,
	BindingController, LifeCycleController {

	/**
	 * Use this constructor for singeton bindings
	 */
	public PoolClientImpl() {}

	@Service
	Component ref;

	Logger logger;

	AbstractTask task = new AbstractClientImpl.ClientTask("Hello World task");

	/**
	 * @see AbstractComponent#beforeFirstStart(Component)
	 */
	@DreamLifeCycle(on = DreamLifeCycleType.FIRST_START)
	protected void beforeFirstStart(Component componentItf)
	throws IllegalLifeCycleException {
		try {
			Map<java.lang.String, java.lang.Object>  m = new HashMap<java.lang.String, java.lang.Object> ();
			m.put("thread" ,"pool");
			m.put("threadPool.capacity" ,new Integer(2));
			org.objectweb.dream.control.activity.Util.addTask(componentItf ,task ,m);
			logger.log(BasicLevel.DEBUG ,"tasks added");
		}
		catch (Exception e) {
			throw new IllegalLifeCycleException("Can\'t add task");
		}
	}

	/**
	 * @see Loggable#setLogger(String, Logger)
	 */
	public void setLogger(String name, Logger logger) {
		if (name.equals("impl")) {
			this.logger = logger;
		}
	}

	/**
	 * @see org.objectweb.dream.control.lifecycle.NeedAsyncStartController#getFcNeedAsyncStart()
	 */
	public boolean getFcNeedAsyncStart() {
		return false;
	}

	/**
	 * Initializes the logger of this component. This method is called during initialization of
	 * the component controllers. <br>
	 *
	 * @throws InitializationException if an error occurs.
	 *
	 */
	protected void _initLogger() {
		try {
			LoggerControllerRegister lcr = ((LoggerControllerRegister)(ref.getFcInterface("/logger-controller-register")));
			lcr.register("impl" ,this);
			logger.log(BasicLevel.DEBUG ,"Component initialized.");
		}
		catch (NoSuchInterfaceException e) {}
	}

	/**
	 * @see org.objectweb.dream.AbstractComponent#startFc()
	 */
	@LifeCycle(on = LifeCycleType.START)
	protected void startFcHandler() throws IllegalLifeCycleException {
		try {
			ThreadPoolManager threadPoolManager = ((ThreadPoolManager)(task.getControlItf()));
			threadPoolManager.addThread(task);
			threadPoolManager.addThread(task);
		} catch (Exception e) {
			throw new ChainedIllegalLifeCycleException(e , null , "An error occurs while retrieving task control interface");
		}
	}

	/**
	 * This attribute give the state of Dream Component.
	 * The fcState value is {@link LifeCycleController#STARTED} or
	 * {@link LifeCycleController#STOPPED}.
	 */
	private String _fcState = LifeCycleController.STOPPED;

	private boolean _firstStart = true;

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 */
	public String getFcState() {
		return _fcState;
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 */
	public void startFc() throws IllegalLifeCycleException {
		try {
			_fcState = LifeCycleController.STARTED;
			if (_firstStart) {
				_beforeFirstStart(ref);
				_firstStart = false;
			}
			startFcHandler();
		} catch (Exception e) {
			throw new IllegalLifeCycleException(e.getMessage());
		}
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 */
	public void stopFc() throws IllegalLifeCycleException {
		_fcState = LifeCycleController.STOPPED;
	}

	/**
	 * This method is called the first time the component is started.
	 *
	 * @param componentItf the {@link Component }interface of this component.
	 */
	public void _beforeFirstStart( Component componentItf )
	throws IllegalLifeCycleException {
		try {
			beforeFirstStart(this.ref);
		}
		catch (Exception e) {
			throw new IllegalLifeCycleException(e.getMessage());
		}
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 */
	@Override
	public void bindFc( String clientItfName, Object serverItf )
	throws
		NoSuchInterfaceException, IllegalBindingException,
		IllegalLifeCycleException {

		if (clientItfName.equals("out-push")) {
			if (!(Push.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (Push.class.getName())));
			}
			outPushItf = ((Push)(serverItf));
			return ;
		}
		if (clientItfName.equals("message-manager")) {
			if (!(MessageManagerType.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (MessageManagerType.class.getName())));
			}
			messageManager = ((MessageManagerType)(serverItf));
			return ;
		}
		if (clientItfName.equals("task-manager")) {
			if (!(TaskManager.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (TaskManager.class.getName())));
			}
			taskManagerItf = ((TaskManager)(serverItf));
			return ;
		}
		if (clientItfName.equals("component")) {
			ref = ((Component)(serverItf));
			_initLogger();
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 */
	@Override
	public String[] listFc() {
		List<java.lang.String>  __interfaces__ = new ArrayList<java.lang.String> ();
		__interfaces__.add("out-push");
		__interfaces__.add("message-manager");
		__interfaces__.add("task-manager");
		return __interfaces__.toArray(new String[__interfaces__.size()]);
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 */
	@Override
	public Object lookupFc(String clientItfName) throws NoSuchInterfaceException {
		if (clientItfName.equals("out-push")) {
			return outPushItf;
		}
		if (clientItfName.equals("message-manager")) {
			return messageManager;
		}
		if (clientItfName.equals("task-manager")) {
			return taskManagerItf;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 */
	@Override
	public void unbindFc(String clientItfName)
	throws
		NoSuchInterfaceException, IllegalBindingException,
		IllegalLifeCycleException {

		if (clientItfName.equals("out-push")) {
			outPushItf = null;
			return ;
		}
		if (clientItfName.equals("message-manager")) {
			messageManager = null;
			return ;
		}
		if (clientItfName.equals("task-manager")) {
			taskManagerItf = null;
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}
}
