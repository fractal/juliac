/**
 * Dream
 * Copyright (C) 2003-2021 Inria Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s):
 */

package helloworld;

import java.util.ArrayList;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.dream.dreamannotation.DreamComponent;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.fraclet.annotation.annotations.Interface;
import java.util.List;
import org.objectweb.dream.message.Message;
import org.objectweb.dream.message.MessageManagerType;
import org.objectweb.dream.control.lifecycle.NeedAsyncStartController;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.fraclet.annotation.annotations.Provides;
import org.objectweb.dream.Push;
import org.objectweb.dream.PushException;
import org.objectweb.fractal.fraclet.annotation.annotations.Requires;

@DreamComponent(controllerDesc = "dreamPrimitive")
@Provides(interfaces = @Interface(name = "in-push", signature = Push.class))
public class ServerImpl implements Push , NeedAsyncStartController , BindingController {

	@Requires(name = "message-manager")
	MessageManagerType messageManager;

	public void push(Message message) throws PushException {
		HelloWorldChunk chunk =
			messageManager.getChunk(message ,HelloWorldChunk.DEFAULT_NAME);
		if (chunk == null) {
			throw new PushException("Unable to find HelloWorld chunk");
		}
		System.out.println(((("Thread=" + (Thread.currentThread().getName())) + " : ") + (chunk.getMessage())));
		messageManager.deleteMessage(message);
	}

	public boolean getFcNeedAsyncStart() {
		return false;
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 */
	public void bindFc(String clientItfName, Object serverItf)
	throws
		NoSuchInterfaceException, IllegalBindingException,
		IllegalLifeCycleException {

		if (clientItfName.equals("message-manager")) {
			if (!(MessageManagerType.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (MessageManagerType.class.getName())));
			}
			messageManager = ((MessageManagerType)(serverItf));
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 */
	public String[] listFc() {
		List<java.lang.String>  __interfaces__ =
			new ArrayList<java.lang.String> ();
		__interfaces__.add("message-manager");
		return __interfaces__.toArray(new String[__interfaces__.size()]);
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 */
	public Object lookupFc(String clientItfName)
	throws NoSuchInterfaceException {

		if (clientItfName.equals("message-manager")) {
			return messageManager;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 */
	public void unbindFc(String clientItfName)
	throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException {

		if (clientItfName.equals("message-manager")) {
			messageManager = null;
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}
}
