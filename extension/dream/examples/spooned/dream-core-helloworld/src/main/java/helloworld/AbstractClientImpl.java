/**
 * Dream
 * Copyright (C) 2003-2021 Inria Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s):
 */

package helloworld;

import org.objectweb.dream.control.activity.task.AbstractTask;
import java.util.ArrayList;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.dream.message.ChunkFactoryReference;
import org.objectweb.dream.dreamannotation.DreamComponent;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import java.util.List;
import org.objectweb.dream.message.Message;
import org.objectweb.dream.message.MessageManagerType;
import org.objectweb.dream.control.lifecycle.NeedAsyncStartController;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.dream.Push;
import org.objectweb.fractal.fraclet.annotation.annotations.Requires;
import org.objectweb.dream.control.activity.task.Task;
import org.objectweb.dream.control.activity.manager.TaskManager;

@DreamComponent
public abstract class AbstractClientImpl
implements NeedAsyncStartController , BindingController {

	@Requires(name = "out-push")
	Push outPushItf;

	@Requires(name = "message-manager")
	MessageManagerType messageManager;

	@Requires(name = "task-manager")
	protected TaskManager taskManagerItf;

	protected class ClientTask extends AbstractTask {

		private String message;

		public ClientTask(String message) {
			super(("client-task-" + message));
			this.message = message;
		}

		public Object execute(Object hints) throws InterruptedException {
			try {
				Message msg = messageManager.createMessage();
				ChunkFactoryReference<helloworld.HelloWorldChunk>  chunkFactory =
					messageManager.getChunkFactory(HelloWorldChunk.class);
				HelloWorldChunk chunk = messageManager.createChunk(chunkFactory);
				chunk.setMessage(message);
				messageManager.addChunk(msg ,HelloWorldChunk.DEFAULT_NAME ,chunk);
				outPushItf.push(msg);
			}
			catch (Exception e) {
				e.printStackTrace();
				return Task.STOP_EXECUTING;
			}
			Thread.sleep(1000);
			return Task.EXECUTE_AGAIN;
		}

	}

	public boolean getFcNeedAsyncStart() {
		return false;
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 */
	public void bindFc( String clientItfName, Object serverItf )
	throws
		NoSuchInterfaceException, IllegalBindingException,
		IllegalLifeCycleException {

		if (clientItfName.equals("out-push")) {
			if (!(Push.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (Push.class.getName())));
			}
			outPushItf = ((Push)(serverItf));
			return ;
		}
		if (clientItfName.equals("message-manager")) {
			if (!(MessageManagerType.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (MessageManagerType.class.getName())));
			}
			messageManager = ((MessageManagerType)(serverItf));
			return ;
		}
		if (clientItfName.equals("task-manager")) {
			if (!(TaskManager.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (TaskManager.class.getName())));
			}
			taskManagerItf = ((TaskManager)(serverItf));
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 */
	public String[] listFc() {
		List<java.lang.String>  __interfaces__ =
			new ArrayList<java.lang.String> ();
		__interfaces__.add("out-push");
		__interfaces__.add("message-manager");
		__interfaces__.add("task-manager");
		return __interfaces__.toArray(new String[__interfaces__.size()]);
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 */
	public Object lookupFc(String clientItfName)
	throws NoSuchInterfaceException {

		if (clientItfName.equals("out-push")) {
			return outPushItf;
		}
		if (clientItfName.equals("message-manager")) {
			return messageManager;
		}
		if (clientItfName.equals("task-manager")) {
			return taskManagerItf;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 */
	public void unbindFc( String clientItfName )
	throws
		NoSuchInterfaceException, IllegalBindingException,
		IllegalLifeCycleException {

		if (clientItfName.equals("out-push")) {
			outPushItf = null;
			return ;
		}
		if (clientItfName.equals("message-manager")) {
			messageManager = null;
			return ;
		}
		if (clientItfName.equals("task-manager")) {
			taskManagerItf = null;
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}
}
