###############################################################################
# EXTENSION OF JULIA CONFIGURATION FILE 
#
# DESCRIBE DREAM SPECIFIC CONTROLLERS
###############################################################################


# -----------------------------------------------------------------------------
# CONTROLLER INTERFACES
# -----------------------------------------------------------------------------

# ContextualBindingController interface
(contextual-binding-controller-itf
  (binding-controller
    org.objectweb.dream.control.binding.ContextualBindingController)
)


# TaskController interface
(task-controller-itf
  (task-controller
    org.objectweb.dream.control.activity.task.TaskController)
)

(task-activation-controller-itf
  (task-activation-controller
    org.objectweb.dream.control.activity.task.TaskActivationController)
)


# LocationController interface
(location-controller-itf
  (location-controller
    org.objectweb.dream.control.location.LocationController)
)

# NeedAsyncStartController interface 
(need-async-start-controller-itf
  (need-async-start-controller
    org.objectweb.dream.control.lifecycle.NeedAsyncStartController)
)

# LoggerController interface
(logger-controller-itf
  (logger-controller
    org.objectweb.dream.control.logger.LoggerController)
)

(logger-controller-register-itf
  (/logger-controller-register
    org.objectweb.dream.control.logger.LoggerControllerRegister)
)

# -----------------------------------------------------------------------------
# CONTROLLER CLASSES
#
# define new controllers implementation, and override definition of modified
# controllers
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Overriden definitions of julia-bundled.cfg
# -----------------------------------------------------------------------------

# LifeCycleController implementation (for primitive or composite components)
# Introduction of monolog logger for pure fractal component
# see javadoc of org.objectweb.fractal.julia.logger.LoggerLifeCycleMixin class
# http://fractal.objectweb.org/current/doc/javadoc/julia/org/objectweb/fractal/julia/logger/LoggerLifeCycleMixin.html

(lifecycle-controller-impl
  ((org.objectweb.fractal.julia.asm.MixinClassGenerator
    LifeCycleControllerImpl
    org.objectweb.fractal.julia.BasicControllerMixin
    org.objectweb.fractal.julia.UseComponentMixin
    org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleCoordinatorMixin
    org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleControllerMixin
    # to check that mandatory client interfaces are bound in startFc:
    org.objectweb.fractal.julia.control.lifecycle.TypeLifeCycleMixin
    # MODIFICATION to automatically assign the logger and logger factory:
    org.objectweb.fractal.julia.BasicInitializableMixin
    org.objectweb.fractal.julia.logger.LoggerLifeCycleMixin
    # to notify the encapsulated component (if present) when its state changes:
    org.objectweb.fractal.julia.control.lifecycle.ContainerLifeCycleMixin
  )
    # optional initialization parameter (monolog configuration file name):
    # MODIFICATION of the original julia-bundled.cfg
    (monolog-conf-file monolog-dreamcore-helloworld.properties)
  )
)

# LifeCycleController implementation (for composite components only) 
# use BasicLifeCycleControllerMixin instead of OptimizedLifeCycleControllerMixin

(composite-lifecycle-controller-impl
  ((org.objectweb.fractal.julia.asm.MixinClassGenerator
    CompositeLifeCycleControllerImpl
    org.objectweb.fractal.julia.BasicControllerMixin
    org.objectweb.fractal.julia.UseComponentMixin
    org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleCoordinatorMixin
    org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleControllerMixin
    # to check that mandatory client interfaces are bound in startFc:
    org.objectweb.fractal.julia.control.lifecycle.TypeLifeCycleMixin
  ))
)

# GenericFactory implementation

(generic-factory-impl
  ((org.objectweb.fractal.julia.asm.MixinClassGenerator
    GenericFactoryImpl
    org.objectweb.fractal.julia.BasicControllerMixin
    org.objectweb.fractal.julia.BasicInitializableMixin
    org.objectweb.fractal.julia.loader.UseLoaderMixin
    org.objectweb.fractal.julia.type.UseTypeFactoryMixin
    org.objectweb.fractal.julia.factory.BasicGenericFactoryMixin
    # to check the component content descriptor with the Java Reflection API:
    org.objectweb.fractal.julia.factory.CheckGenericFactoryMixin
    org.objectweb.fractal.julia.UseComponentMixin
    org.objectweb.fractal.julia.control.content.UseContentControllerMixin
    org.objectweb.dream.control.factory.ContentFactoryMixin
    org.objectweb.dream.control.factory.LocationFactoryMixin
  ))
)

# -----------------------------------------------------------------------------
# New definitions
# -----------------------------------------------------------------------------

# NameController implementation (for bootstrap component)
(bootstrap-name-controller-impl
  ((org.objectweb.fractal.julia.asm.MixinClassGenerator
    BootstrapNameControllerImpl
    org.objectweb.fractal.julia.BasicControllerMixin
    org.objectweb.fractal.julia.control.name.BasicNameControllerMixin
    org.objectweb.dream.control.factory.NameFactoryMixin
  ))
)



# ContentController implementation (for bootstrap component)

(bootstrap-content-controller-impl
  ((org.objectweb.fractal.julia.asm.MixinClassGenerator
    BootstrapContentControllerImpl
    org.objectweb.fractal.julia.BasicControllerMixin
    org.objectweb.fractal.julia.UseComponentMixin
    org.objectweb.fractal.julia.control.content.BasicContentControllerMixin
    # to check some basic pre conditions, and to prevent hierarchy cycles:
    org.objectweb.fractal.julia.control.content.CheckContentMixin
    # to check type related constraints in getFcInternalInterface:
    org.objectweb.fractal.julia.control.content.TypeContentMixin
    # to check binding locality related constraints in removeFcSubComponent:
    org.objectweb.fractal.julia.control.content.BindingContentMixin
    # to check lifecycle related constraints:
    org.objectweb.fractal.julia.control.lifecycle.UseLifeCycleControllerMixin
    org.objectweb.fractal.julia.control.content.LifeCycleContentMixin
  ))
)

# TaskController implementation

(task-controller-impl
  ((org.objectweb.fractal.julia.asm.MixinClassGenerator
    TaskControllerImpl
    org.objectweb.fractal.julia.BasicControllerMixin
    org.objectweb.fractal.julia.control.binding.UseBindingControllerMixin
    org.objectweb.dream.control.logger.UseLoggerControllerMixin
    org.objectweb.dream.control.activity.task.LoggableTaskMixin
    org.objectweb.dream.control.activity.task.BasicTaskControllerMixin
    # TaskActivationController implementation
    org.objectweb.dream.control.activity.task.BasicTaskActivationMixin
    # Auto Activate new task
    org.objectweb.dream.control.activity.task.UseTaskActivationControllerMixin
    org.objectweb.fractal.julia.control.lifecycle.UseLifeCycleControllerMixin
    org.objectweb.dream.control.activity.task.LifeCycleActivationTaskMixin
  ))
)

# LocationController implementation
(location-controller-impl
  ((org.objectweb.fractal.julia.asm.MixinClassGenerator
    LocationControllerImpl
    org.objectweb.fractal.julia.BasicControllerMixin
    org.objectweb.dream.control.content.UseOptContentControllerMixin
    org.objectweb.dream.control.location.BasicLocationControllerMixin
  ))
)

# LifeCycleController implementation (for unstoppable components)

(dream-simple-lifecycle-controller-impl
  ((org.objectweb.fractal.julia.asm.MixinClassGenerator
    DreamSimpleLifeCycleControllerImpl
    org.objectweb.fractal.julia.BasicControllerMixin
    org.objectweb.fractal.julia.UseComponentMixin
    org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleCoordinatorMixin
    org.objectweb.dream.control.lifecycle.AsyncStartLifeCycleCoordinatorMixin
    org.objectweb.dream.control.logger.UseLoggerControllerMixin
    org.objectweb.dream.control.lifecycle.LoggableLifeCycleMixin
    # Dream lifecycle controller
    org.objectweb.dream.control.lifecycle.SimpleLifeCycleControllerMixin
    # to check that mandatory client interfaces are bound in startFc:
    org.objectweb.fractal.julia.control.lifecycle.TypeLifeCycleMixin
    # to give loggers to controller and content objects
    org.objectweb.dream.control.logger.LoggerLifeCycleCoordinatorMixin
    # to notify the encapsulated component (if present) when its state changes:
    org.objectweb.dream.control.lifecycle.ContainerPrepareStopLifeCycleControllerMixin
    org.objectweb.fractal.julia.control.lifecycle.ContainerLifeCycleMixin
  ))
)

# LifeCycleController implementation (for stoppable components with interceptor)

(dream-full-lifecycle-controller-impl
  ((org.objectweb.fractal.julia.asm.MixinClassGenerator
    DreamFullLifeCycleControllerImpl
    org.objectweb.fractal.julia.BasicControllerMixin
    org.objectweb.fractal.julia.UseComponentMixin
    org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleCoordinatorMixin
    org.objectweb.dream.control.lifecycle.AsyncStartLifeCycleCoordinatorMixin
    # to give loggers to controller and content objects
    org.objectweb.dream.control.logger.UseLoggerControllerMixin
    org.objectweb.dream.control.lifecycle.LoggableLifeCycleMixin
    # Dream lifecycle controller
    org.objectweb.dream.control.lifecycle.SimpleLifeCycleControllerMixin
    org.objectweb.dream.control.lifecycle.ThreadCounterMixin
    org.objectweb.dream.control.lifecycle.FullLifeCycleMixin
    # to check that mandatory client interfaces are bound in startFc:
    org.objectweb.fractal.julia.control.lifecycle.TypeLifeCycleMixin
    # to give loggers to controller and content objects
    org.objectweb.dream.control.logger.LoggerLifeCycleCoordinatorMixin
    # to notify the encapsulated component (if present) when its state changes:
    org.objectweb.dream.control.lifecycle.ContainerPrepareStopLifeCycleControllerMixin
    org.objectweb.fractal.julia.control.lifecycle.ContainerLifeCycleMixin
  ))
)

# LifeCycleController implementation (for active unstoppable components)

(active-simple-lifecycle-controller-impl
  ((org.objectweb.fractal.julia.asm.MixinClassGenerator
    ActiveSimpleLifeCycleControllerImpl
    org.objectweb.fractal.julia.BasicControllerMixin
    org.objectweb.fractal.julia.UseComponentMixin
    org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleCoordinatorMixin
    org.objectweb.dream.control.lifecycle.AsyncStartLifeCycleCoordinatorMixin
    # to give loggers to controller and content objects
    org.objectweb.dream.control.logger.UseLoggerControllerMixin
    org.objectweb.dream.control.lifecycle.LoggableLifeCycleMixin
    # Dream lifecycle controller
    org.objectweb.dream.control.lifecycle.SimpleLifeCycleControllerMixin
    org.objectweb.dream.control.activity.task.UseTaskControllerMixin
    org.objectweb.dream.control.activity.task.UseTaskActivationControllerMixin
    org.objectweb.dream.control.lifecycle.ActivityLifeCycleMixin
    # to check that mandatory client interfaces are bound in startFc:
    org.objectweb.fractal.julia.control.lifecycle.TypeLifeCycleMixin
    # to give loggers to controller and content objects
    org.objectweb.dream.control.logger.LoggerLifeCycleCoordinatorMixin
    # to notify the encapsulated component (if present) when its state changes:
    org.objectweb.dream.control.lifecycle.ContainerPrepareStopLifeCycleControllerMixin
    org.objectweb.fractal.julia.control.lifecycle.ContainerLifeCycleMixin
  ))
)

# LifeCycleController implementation (for active stoppable components with interceptor)

(active-full-lifecycle-controller-impl
  ((org.objectweb.fractal.julia.asm.MixinClassGenerator
    ActiveFullLifeCycleControllerImpl
    org.objectweb.fractal.julia.BasicControllerMixin
    org.objectweb.fractal.julia.UseComponentMixin
    org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleCoordinatorMixin
    org.objectweb.dream.control.lifecycle.AsyncStartLifeCycleCoordinatorMixin
    # to give loggers to controller and content objects
    org.objectweb.dream.control.logger.UseLoggerControllerMixin
    org.objectweb.dream.control.lifecycle.LoggableLifeCycleMixin
    # Dream lifecycle controller
    org.objectweb.dream.control.lifecycle.SimpleLifeCycleControllerMixin
    org.objectweb.dream.control.lifecycle.ThreadCounterMixin
    org.objectweb.dream.control.lifecycle.FullLifeCycleMixin
    org.objectweb.dream.control.activity.task.UseTaskControllerMixin
    org.objectweb.dream.control.activity.task.UseTaskActivationControllerMixin
    org.objectweb.dream.control.lifecycle.ActivityLifeCycleMixin
    # to check that mandatory client interfaces are bound in startFc:
    org.objectweb.fractal.julia.control.lifecycle.TypeLifeCycleMixin
    # to give loggers to controller and content objects
    org.objectweb.dream.control.logger.LoggerLifeCycleCoordinatorMixin
    # to notify the encapsulated component (if present) when its state changes:
    org.objectweb.dream.control.lifecycle.ContainerPrepareStopLifeCycleControllerMixin
    org.objectweb.fractal.julia.control.lifecycle.ContainerLifeCycleMixin
  ))
)

# BindingController implementation (for primitive components with content)

(container-auto-binding-controller-impl
  ((org.objectweb.fractal.julia.asm.MixinClassGenerator
    ContainerAutoBindingControllerImpl
    org.objectweb.fractal.julia.BasicControllerMixin
    org.objectweb.dream.control.binding.ContainerContextualBindingControllerMixin
    # to skip Interface objects before delegating to the encapsulated component:
    # org.objectweb.fractal.julia.control.binding.OptimizedContainerBindingMixin
    # to manage output interceptors:
    org.objectweb.fractal.julia.UseComponentMixin
    org.objectweb.fractal.julia.control.binding.InterceptorBindingMixin
    # to check some basic pre conditions (interface not already bound, ...)
    org.objectweb.fractal.julia.control.binding.CheckBindingMixin
    # to check type related constraints for bindings:
    org.objectweb.fractal.julia.control.binding.TypeBindingMixin
    # to check content related constraints for bindings:
    org.objectweb.fractal.julia.control.content.UseSuperControllerMixin
    org.objectweb.fractal.julia.control.binding.ContentBindingMixin
    # to check lifecycle related constraints for bindings:
    org.objectweb.fractal.julia.control.lifecycle.UseLifeCycleControllerMixin
    org.objectweb.fractal.julia.control.binding.LifeCycleBindingMixin
    
    # added mixin for auto binding
    org.objectweb.fractal.julia.control.binding.AutoBindingMixin
  ))
)

# BindingController implementation (for composite components)

(composite-auto-binding-controller-impl
  ((org.objectweb.fractal.julia.asm.MixinClassGenerator
    CompositeAutoBindingControllerImpl
    org.objectweb.fractal.julia.BasicControllerMixin
    org.objectweb.fractal.julia.control.binding.BasicBindingControllerMixin
    # to initialize the BasicBindingControllerMixin from the component's type:
    org.objectweb.fractal.julia.control.binding.TypeBasicBindingMixin
    # to check some basic pre conditions (interface not already bound, ...)
    org.objectweb.fractal.julia.UseComponentMixin
    org.objectweb.fractal.julia.control.binding.CheckBindingMixin
    # to check type related constraints for bindings:
    org.objectweb.fractal.julia.control.binding.TypeBindingMixin
    # to check content related constraints for bindings:
    org.objectweb.fractal.julia.control.content.UseSuperControllerMixin
    org.objectweb.fractal.julia.control.binding.ContentBindingMixin
    # to check lifecycle related constraints for bindings:
    org.objectweb.fractal.julia.control.lifecycle.UseLifeCycleControllerMixin
    org.objectweb.fractal.julia.control.binding.LifeCycleBindingMixin
    # to manage the getFcItfImpl links of the Interface objects:
    # choose one of ComponentBindingMixin and OptimizedCompositeBindingMixin
    # (the last one creates and updates shortcuts links when possible)
    org.objectweb.fractal.julia.control.content.UseContentControllerMixin
    # org.objectweb.fractal.julia.control.binding.CompositeBindingMixin
    org.objectweb.fractal.julia.control.binding.OptimizedCompositeBindingMixin
    
    # added mixin for auto binding
    org.objectweb.fractal.julia.control.binding.AutoBindingMixin
  ))
)

# LoggerController implementation

(logger-controller-impl
  ((org.objectweb.fractal.julia.asm.MixinClassGenerator
    LoggerControllerImpl
    org.objectweb.fractal.julia.BasicControllerMixin
    org.objectweb.fractal.julia.control.name.UseNameControllerMixin
    org.objectweb.fractal.julia.BasicInitializableMixin
    org.objectweb.dream.control.logger.BasicLoggerControllerMixin
  )
    # optional initialization parameter (monolog configuration file name):
    (monolog-conf-file monolog-dreamcore-helloworld.properties)
  )
)

# -----------------------------------------------------------------------------
# CONTROLLER DESCRIPTORS
# -----------------------------------------------------------------------------

(optimizationLevel
  # choose one of the following optimization options:
  none
  # mergeControllers
  # mergeControllersAndInterceptors
  # mergeControllersAndContent
  # mergeControllersInterceptorsAndContent
)

(bootstrap
  (
    'interface-class-generator
    (
      'component-itf
      'type-factory-itf
      'generic-factory-itf
      'content-controller-itf
      (loader org.objectweb.fractal.julia.loader.Loader)
      'name-controller-itf
    )
    (
      'component-impl
      'type-factory-impl
      'generic-factory-impl
      'bootstrap-content-controller-impl
      # choose one of the following classes:
      # the first one loads all classes from the classpath
      # the second one can generate missing classes on the fly, dynamically
      # org.objectweb.fractal.julia.loader.BasicLoader
      org.objectweb.fractal.julia.loader.DynamicLoader
      'bootstrap-name-controller-impl
    )
    (
      # no interceptors
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    none
  )
)

(dreamPrimitive
  (
    'interface-class-generator
    (
      'component-itf
      'contextual-binding-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'need-async-start-controller-itf
      'name-controller-itf
      'logger-controller-itf
      'logger-controller-register-itf
      'location-controller-itf
    )
    (
      # set first to be initialized first
      'logger-controller-impl
      'component-impl
      'super-controller-impl
      'dream-full-lifecycle-controller-impl
      'name-controller-impl
      # binding controller implementation must be the last one, inorder to 
      # ensure correct initialization
      'container-auto-binding-controller-impl
      'location-controller-impl
    )
    (
      (org.objectweb.fractal.juliac.core.proxy.InterceptorClassGenerator
        org.objectweb.dream.control.lifecycle.LifeCycleInterceptorSourceCodeGenerator
      )
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(dreamUnstoppablePrimitive
  (
    'interface-class-generator
    (
      'component-itf
      'contextual-binding-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'need-async-start-controller-itf
      'name-controller-itf
      'logger-controller-itf
      'logger-controller-register-itf
      'location-controller-itf
    )
    (
      # set first to be initialized first
      'logger-controller-impl
      'component-impl
      'super-controller-impl
      'dream-simple-lifecycle-controller-impl
      'name-controller-impl
      # binding controller implementation must be the last one, inorder to 
      # ensure correct initialization
      'container-auto-binding-controller-impl
      'location-controller-impl
    )
    (
      # no interceptor
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(activeDreamPrimitive
  (
    'interface-class-generator
    (
      'component-itf
      'contextual-binding-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'need-async-start-controller-itf
      'name-controller-itf
      'task-controller-itf
      'task-activation-controller-itf
      'logger-controller-itf
      'logger-controller-register-itf
      'location-controller-itf
    )
    (
      # set first to be initialized first
      'logger-controller-impl
      'component-impl
      'super-controller-impl
      # lifecycle and activity controller
      'task-controller-impl
      'active-full-lifecycle-controller-impl
      'name-controller-impl
      # binding controller implementation must be the last one, inorder to 
      # ensure correct initialization
      'container-auto-binding-controller-impl
      'location-controller-impl
    )
    (
      (org.objectweb.fractal.juliac.core.proxy.InterceptorClassGenerator
        org.objectweb.dream.control.lifecycle.LifeCycleInterceptorSourceCodeGenerator
      )
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(activeDreamUnstoppablePrimitive
  (
    'interface-class-generator
    (
      'component-itf
      'contextual-binding-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'need-async-start-controller-itf
      'name-controller-itf
      'task-controller-itf
      'task-activation-controller-itf
      'logger-controller-itf
      'logger-controller-register-itf
      'location-controller-itf
    )
    (
      # set first to be initialized first
      'logger-controller-impl
      'component-impl
      'super-controller-impl
      # lifecycle and activity controller
      'task-controller-impl
      'active-simple-lifecycle-controller-impl
      'name-controller-impl
      # binding controller implementation must be the last one, inorder to 
      # ensure correct initialization
      'container-auto-binding-controller-impl
      'location-controller-impl
    )
    (
      # no interceptor
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

# composite

(dreamComposite
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'content-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'name-controller-itf
      'logger-controller-itf
      'logger-controller-register-itf
      'location-controller-itf
    )
    (
      # set first to be initialized first
      'logger-controller-impl
      'component-impl
      'composite-auto-binding-controller-impl
      'content-controller-impl
      'super-controller-impl
      # use non optimized lifecycle controller implementation
      'dream-full-lifecycle-controller-impl
      'name-controller-impl
      'location-controller-impl
    )
    (
      (org.objectweb.fractal.juliac.core.proxy.InterceptorClassGenerator
        org.objectweb.dream.control.lifecycle.LifeCycleInterceptorSourceCodeGenerator
      )
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(dreamParametricComposite
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'content-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'name-controller-itf
      'logger-controller-itf
      'logger-controller-register-itf
      'location-controller-itf
    )
    (
      # set first to be initialized first
      'logger-controller-impl
      'component-impl
      ((org.objectweb.fractal.julia.asm.AttributeControllerClassGenerator
        ('attributeControllerInterface)
      ))
      'composite-auto-binding-controller-impl
      'content-controller-impl
      'super-controller-impl
      # use non optimized lifecycle controller implementation
      'dream-full-lifecycle-controller-impl
      'name-controller-impl
      'location-controller-impl
    )
    (
      (org.objectweb.fractal.juliac.core.proxy.InterceptorClassGenerator
        org.objectweb.dream.control.lifecycle.LifeCycleInterceptorSourceCodeGenerator
      )
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)


(dreamUnstoppableComposite
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'content-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'name-controller-itf
      'logger-controller-itf
      'logger-controller-register-itf
      'location-controller-itf
    )
    (
      # set first to be initialized first
      'logger-controller-impl
      'component-impl
      'composite-auto-binding-controller-impl
      'content-controller-impl
      'super-controller-impl
      # use non optimized lifecycle controller implementation
      'dream-simple-lifecycle-controller-impl
      'name-controller-impl
      'location-controller-impl
    )
    (
      # no interceptors
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(dreamUnstoppableParametricComposite
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'content-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'name-controller-itf
      'logger-controller-itf
      'logger-controller-register-itf
      'location-controller-itf
    )
    (
      # set first to be initialized first
      'logger-controller-impl
      'component-impl
      ((org.objectweb.fractal.julia.asm.AttributeControllerClassGenerator
        ('attributeControllerInterface)
      ))
      'composite-auto-binding-controller-impl
      'content-controller-impl
      'super-controller-impl
      # use non optimized lifecycle controller implementation
      'dream-simple-lifecycle-controller-impl
      'name-controller-impl
      'location-controller-impl
    )
    (
      # no interceptors
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

