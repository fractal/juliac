/**
 * Dream
 * Copyright (C) 2003-2021 Inria Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact : dream@objectweb.org
 *
 * Initial developer(s): Vivien Quema
 * Contributor(s): Romain Lenglet
 */

package activity;

import org.objectweb.dream.control.activity.task.AbstractTask;
import java.util.ArrayList;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.dream.control.lifecycle.BeforeFirstStartLifeCycle;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.Component;
import org.objectweb.dream.dreamannotation.DreamComponent;
import org.objectweb.dream.dreamannotation.DreamLifeCycle;
import org.objectweb.dream.dreamannotation.util.DreamLifeCycleType;
import java.util.HashMap;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.dream.InitializationException;
import org.objectweb.dream.InterruptedPullException;
import org.objectweb.fractal.api.control.LifeCycleController;
import java.util.List;
import org.objectweb.dream.control.logger.Loggable;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.dream.control.logger.LoggerControllerRegister;
import java.util.Map;
import org.objectweb.dream.message.Message;
import org.objectweb.dream.message.MessageManagerType;
import org.objectweb.dream.control.lifecycle.NeedAsyncStartController;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.dream.Pull;
import org.objectweb.dream.PullException;
import org.objectweb.fractal.fraclet.annotation.annotations.Requires;
import org.objectweb.fractal.fraclet.annotation.annotations.Service;
import org.objectweb.dream.control.activity.task.Task;
import org.objectweb.dream.control.activity.manager.TaskManager;
import org.objectweb.dream.control.activity.manager.ThreadPoolManager;

/**
 * Client component implementation
 */
@DreamComponent(controllerDesc = "activeDreamPrimitive")
public class Client implements BeforeFirstStartLifeCycle , NeedAsyncStartController , Loggable , BindingController , LifeCycleController {
	/**
	 * Use this constructor for singeton bindings
	 *
	 * @param serviceName
	 *            The name of the java field
	 */
	public Client() {
	}

	@Service
	Component ref;

	Logger logger;

	@Requires(name = "in-pull")
	protected Pull inPullItf;

	@Requires(name = "message-manager")
	protected MessageManagerType messageManagerItf;

	@Requires(name = "task-manager")
	protected TaskManager taskManagerItf;

	/**
	 * @see Loggable#setLogger(String, Logger)
	 */
	public void setLogger(String name, Logger logger) {
		if (name.equals("impl")) {
			this.logger = logger;
		}
	}

	/**
	 * @see org.objectweb.dream.control.lifecycle.NeedAsyncStartController#getFcNeedAsyncStart()
	 */
	public boolean getFcNeedAsyncStart() {
		return false;
	}

	/**
	 * @see AbstractComponent#beforeFirstStart(Component)
	 */
	@DreamLifeCycle(on = DreamLifeCycleType.FIRST_START)
	protected void beforeFirstStart(Component componentItf) throws IllegalLifeCycleException {
		try {
			HashMap<java.lang.String, java.lang.Object>  context = new HashMap<java.lang.String, java.lang.Object> ();
			context.put("thread" ,"pool");
			org.objectweb.dream.control.activity.Util.addTask(componentItf ,new PullTask() ,context);
			logger.log(BasicLevel.INFO ,"task added");
		} catch (Exception e) {
			throw new IllegalLifeCycleException("Can\'t add task");
		}
	}

	/**
	 * Initializes the logger of this component. This method is called during initialization of
	 * the component controllers. <br>
	 *
	 * @throws InitializationException if an error occurs.
	 *
	 */
	protected void _initLogger() throws InitializationException {
		try {
			LoggerControllerRegister lcr = ((LoggerControllerRegister)(ref.getFcInterface("/logger-controller-register")));
			lcr.register("impl" ,this);
			logger.log(BasicLevel.DEBUG ,"Component initialized.");
		} catch (NoSuchInterfaceException e) {
		}
	}

	protected class PullTask extends AbstractTask {
		/**
		 * Constructor
		 */
		PullTask() {
			super("PullTask");
		}

		/**
		 * @see org.objectweb.dream.control.activity.task.Task#execute(Object)
		 */
		public Object execute(Object hints) throws InterruptedException {
			Message message = null;
			try {
				message = inPullItf.pull();
				System.out.println(("PullComponent has received a message : " + message));
				messageManagerItf.deleteMessage(message);
			} catch (PullException e) {
				System.out.println(("An exception occurred while pulling a message : " + e));
				if (e instanceof InterruptedPullException) {
					System.out.println("The pull method has been interrupted");
				}
				return Task.STOP_EXECUTING;
			}
			return Task.EXECUTE_AGAIN;
		}

		/**
		 * @see org.objectweb.dream.control.activity.task.Task#registered(Object)
		 */
		public void registered(Object controlItf) {
			if (controlItf instanceof ThreadPoolManager) {
				ThreadPoolManager tpm = ((ThreadPoolManager)(controlItf));
				try {
					tpm.addThread(this);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * This attribute give the state of Dream Component. <br>
	 * the fcSstate value is {@link LifeCycleController#STARTED}
	 * {@link LifeCycleController#STOPPED}
	 */
	private String _fcState = LifeCycleController.STOPPED;

	/**
	 * True if it is the first start of the component
	 */
	private boolean _firstStart = true;

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 *
	 * @see org.objectweb.fractal.api.control.LifeCycleController#getFcState()
	 */
	public String getFcState() {
		return _fcState;
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 *
	 * @see org.objectweb.fractal.api.control.LifeCycleController#startFc()
	 */
	public void startFc() throws IllegalLifeCycleException {
		try {
			_fcState = LifeCycleController.STARTED;
			if (_firstStart) {
				_beforeFirstStart(ref);
				_firstStart = false;
			}
		} catch (Exception e) {
			throw new IllegalLifeCycleException(e.getMessage());
		}
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 *
	 * @see org.objectweb.fractal.api.control.LifeCycleController#stopFc()
	 */
	public void stopFc() throws IllegalLifeCycleException {
		_fcState = LifeCycleController.STOPPED;
	}

	/**
	 * This method is called the first time the component is started.
	 *
	 * @param componentItf
	 *            the {@link Component }interface of this component.
	 */
	public void _beforeFirstStart(Component componentItf) throws IllegalLifeCycleException {
		try {
			beforeFirstStart(this.ref);
		} catch (Exception e) {
			throw new IllegalLifeCycleException(e.getMessage());
		}
	}

	/**
	 * (non-Javadoc)
	 *
	 * @see org.objectweb.fractal.api.control.BindingController#bindFc(java.lang.String,
	 *      java.lang.Object) Method automatically generated with Spoon
	 *      <http://spoon.gforge.inria.fr>
	 */
	public void bindFc(String clientItfName, Object serverItf) throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException {
		if (clientItfName.equals("component")) {
			ref = ((Component)(serverItf));
			try {
				_initLogger();
			} catch (InitializationException e) {
				throw new RuntimeException("An error occurred while initializing logger");
			}
			return ;
		}
		if (clientItfName.equals("in-pull")) {
			if (!(Pull.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (Pull.class.getName())));
			}
			inPullItf = ((Pull)(serverItf));
			return ;
		}
		if (clientItfName.equals("message-manager")) {
			if (!(MessageManagerType.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (MessageManagerType.class.getName())));
			}
			messageManagerItf = ((MessageManagerType)(serverItf));
			return ;
		}
		if (clientItfName.equals("task-manager")) {
			if (!(TaskManager.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (TaskManager.class.getName())));
			}
			taskManagerItf = ((TaskManager)(serverItf));
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	/**
	 * (non-Javadoc)
	 *
	 * @see org.objectweb.fractal.api.control.BindingController#listFc() Method
	 *      automatically generated with Spoon <http://spoon.gforge.inria.fr>
	 */
	public String[] listFc() {
		List<java.lang.String>  __interfaces__ = new ArrayList<java.lang.String> ();
		__interfaces__.add("in-pull");
		__interfaces__.add("message-manager");
		__interfaces__.add("task-manager");
		return __interfaces__.toArray(new String[__interfaces__.size()]);
	}

	/**
	 * (non-Javadoc)
	 *
	 * @see org.objectweb.fractal.api.control.BindingController#lookupFc(java.lang.String)
	 *      Method automatically generated with Spoon
	 *      <http://spoon.gforge.inria.fr>
	 */
	public Object lookupFc(String clientItfName) throws NoSuchInterfaceException {
		if (clientItfName.equals("in-pull")) {
			return inPullItf;
		}
		if (clientItfName.equals("message-manager")) {
			return messageManagerItf;
		}
		if (clientItfName.equals("task-manager")) {
			return taskManagerItf;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	/**
	 * (non-Javadoc)
	 *
	 * @see org.objectweb.fractal.api.control.BindingController#unbindFc(java.lang.String)
	 *      Method automatically generated with Spoon
	 *      <http://spoon.gforge.inria.fr>
	 */
	public void unbindFc(String clientItfName) throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException {
		if (clientItfName.equals("in-pull")) {
			inPullItf = null;
			return ;
		}
		if (clientItfName.equals("message-manager")) {
			messageManagerItf = null;
			return ;
		}
		if (clientItfName.equals("task-manager")) {
			taskManagerItf = null;
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

}
