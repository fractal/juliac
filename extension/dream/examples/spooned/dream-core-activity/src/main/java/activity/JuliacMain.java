package activity;

import org.objectweb.dream.util.Util;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.util.Fractal;

public final class JuliacMain {

  public static void main(String[] args) throws Exception {

	if (args.length == 0){
	  System.err.println("A component should be given as argument");
	  System.exit(1);
	}

	@SuppressWarnings("unchecked")
	Class<Factory> cl = (Class<Factory>) Class.forName(args[0]);
	Factory factory = cl.newInstance();
	Component composite = factory.newFcInstance();

	Fractal.getLifeCycleController(composite).startFc();
	Component server = Util.getComponentByName(Fractal
		.getContentController(composite), "Server");
	Thread.sleep(3000);
	System.out.println("call to stop on server component ...");
	Fractal.getLifeCycleController(server).stopFc();
	System.out.println("stop returned");
  }
}
