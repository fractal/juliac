/**
 * Dream
 * Copyright (C) 2003-2021 Inria Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s):
 */

package activity;

import java.util.HashMap;

import org.objectweb.dream.adl.FactoryFactory;
import org.objectweb.dream.util.Util;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.util.Fractal;

/**
 * Main class
 */
public final class Main
{

  /**
   * Constructor
   *
   */
  private Main()
  {
  }

  /**
   * Entry point.
   *
   * @param args ignored.
   * @throws Exception if an error occurs.
   */
  public static void main(String[] args) throws Exception
  {
	// Component bootstrap = org.objectweb.fractal.api.Fractal
	// .getBootstrapComponent();
	// GenericFactory genericFactory = Fractal.getGenericFactory(bootstrap);
	// TypeFactory typeFactory = Fractal.getTypeFactory(bootstrap);
	//
	// // create interface types
	// InterfaceType outPullType = typeFactory.createFcItfType(
	// Pull.OUT_PULL_ITF_NAME, Pull.class.getName(), false, false, false);
	// InterfaceType inPullType = typeFactory.createFcItfType(
	// Pull.IN_PULL_ITF_NAME, Pull.class.getName(), true, false, false);
	// InterfaceType messageManagerClientType = typeFactory.createFcItfType(
	// MessageManagerType.ITF_NAME, MessageManagerType.class.getName(), true, false,
	// false);
	// InterfaceType messageManagerServerType = typeFactory.createFcItfType(
	// MessageManagerType.ITF_NAME, MessageManagerType.class.getName(), false, false,
	// false);
	// InterfaceType messageManagerAttributeControllerType = typeFactory
	// .createFcItfType("attribute-controller",
	// MessageManagerAttributeController.class.getName(), false, false,
	// false);
	// InterfaceType taskRegistrationClientType = typeFactory.createFcItfType(
	// "task-manager", TaskManagerController.class.getName(), true, false,
	// false);
	//
	// // create component types
	// ComponentType clientType = typeFactory.createFcType(new InterfaceType[]{
	// inPullType, messageManagerClientType, taskRegistrationClientType});
	// ComponentType serverType = typeFactory.createFcType(new InterfaceType[]{
	// outPullType, messageManagerClientType});
	// ComponentType messageManagerType = typeFactory
	// .createFcType(new InterfaceType[]{messageManagerServerType,
	// messageManagerAttributeControllerType});
	// ComponentType compositeType = typeFactory
	// .createFcType(new InterfaceType[0]);
	//
	// // instanciate components
	// Component client = genericFactory.newFcInstance(clientType,
	// "activeDreamPrimitive", Client.class.getName());
	// Component server = genericFactory.newFcInstance(serverType,
	// "dreamPrimitive", Server.class.getName());
	// Component messageManager =
	// genericFactory.newFcInstance(messageManagerType,
	// "dreamPrimitive", MessageManager.class.getName());
	// Component composite = genericFactory.newFcInstance(compositeType,
	// "dreamComposite", null);
	// Component activityManager = genericFactory.newFcInstance(compositeType,
	// "threadPerTaskActivityComposite", null);
	//
	// // set component names
	// Fractal.getNameController(composite).setFcName("Activity");
	// Fractal.getNameController(client).setFcName("Client");
	// Fractal.getNameController(server).setFcName("Server");
	// Fractal.getNameController(messageManager).setFcName("MessageManagerType");
	// Fractal.getNameController(activityManager).setFcName("ActivityManager");
	//
	// // set component attributes
	// MessageManagerAttributeController mmac =
	// (MessageManagerAttributeController) messageManager
	// .getFcInterface("attribute-controller");
	// mmac.setId((short) 0);
	//
	// // add sub components in composite.
	// ContentController compositeCC = Fractal.getContentController(composite);
	// compositeCC.addFcSubComponent(client);
	// compositeCC.addFcSubComponent(server);
	// compositeCC.addFcSubComponent(messageManager);
	// compositeCC.addFcSubComponent(activityManager);
	//
	// // bind them
	// BindingController clientBC = Fractal.getBindingController(client);
	// clientBC.bindFc(Pull.IN_PULL_ITF_NAME, server
	// .getFcInterface(Pull.OUT_PULL_ITF_NAME));
	// clientBC.bindFc(MessageManagerType.ITF_NAME, messageManager
	// .getFcInterface(MessageManagerType.ITF_NAME));
	// clientBC.bindFc("task-manager", activityManager
	// .getFcInterface("task-manager-controller"));
	// BindingController serverBC = Fractal.getBindingController(server);
	// serverBC.bindFc(MessageManagerType.ITF_NAME, messageManager
	// .getFcInterface(MessageManagerType.ITF_NAME));
	//
	// // and start
	// // The activity manager is simply started before to activate its loggers
	// Fractal.getLifeCycleController(activityManager).startFc();
	// Fractal.getLifeCycleController(composite).startFc();

	if (args.length == 0){
	  System.err.println("A component should be given as argument");
	  System.exit(1);
	}

	Component composite = (Component) FactoryFactory.getFactory().newComponent(
		args[0], new HashMap());
	Fractal.getLifeCycleController(composite).startFc();
	Component server = Util.getComponentByName(Fractal
		.getContentController(composite), "Server");
	Thread.sleep(3000);
	System.out.println("call to stop on server component ...");
	Fractal.getLifeCycleController(server).stopFc();
	System.out.println("stop returned");
  }
}
