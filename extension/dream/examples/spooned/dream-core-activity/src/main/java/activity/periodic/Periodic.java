/**
 * Dream
 * Copyright (C) 2003-2021 Inria Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s):
 */

package activity.periodic;

import org.objectweb.dream.control.activity.task.AbstractTask;
import java.util.ArrayList;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.dream.control.lifecycle.BeforeFirstStartLifeCycle;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.Component;
import org.objectweb.dream.dreamannotation.DreamComponent;
import org.objectweb.dream.dreamannotation.DreamLifeCycle;
import org.objectweb.dream.dreamannotation.util.DreamLifeCycleType;
import java.util.HashMap;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.dream.InitializationException;
import org.objectweb.fractal.api.control.LifeCycleController;
import java.util.List;
import org.objectweb.dream.control.logger.Loggable;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.dream.control.logger.LoggerControllerRegister;
import java.util.Map;
import org.objectweb.dream.control.lifecycle.NeedAsyncStartController;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.fraclet.annotation.annotations.Requires;
import org.objectweb.fractal.fraclet.annotation.annotations.Service;
import org.objectweb.dream.control.activity.task.Task;
import org.objectweb.dream.control.activity.manager.TaskManager;

/**
 * This class shows an example of component with a periodic task.
 */
@DreamComponent(controllerDesc = "activeDreamPrimitive")
public class Periodic implements BeforeFirstStartLifeCycle , NeedAsyncStartController , Loggable , BindingController , LifeCycleController {
	/**
	 * Use this constructor for singeton bindings
	 *
	 * @param serviceName
	 *            The name of the java field
	 */
	public Periodic() {
	}

	@Service
	Component weaveableC;

	Logger logger;

	@Requires(name = "task-manager")
	protected TaskManager taskManagerItf;

	class PeriodicTask extends AbstractTask {
		/**
		 * Contructor
		 * @param name name of the task
		 */
		PeriodicTask(String name) {
			super(name);
		}

		/**
		 * @see Task#execute(Object)
		 */
		public Object execute(Object hints) throws InterruptedException {
			logger.log(BasicLevel.INFO ,getFcName());
			return Task.EXECUTE_AGAIN;
		}

	}

	/**
	 * @see Loggable#setLogger(String, Logger)
	 */
	public void setLogger(String name, Logger logger) {
		if (name.equals("impl")) {
			this.logger = logger;
		}
	}

	/**
	 * @see org.objectweb.dream.control.lifecycle.NeedAsyncStartController#getFcNeedAsyncStart()
	 */
	public boolean getFcNeedAsyncStart() {
		return false;
	}

	/**
	 * Initializes the logger of this component. This method is called during initialization of
	 * the component controllers. <br>
	 *
	 * @throws InitializationException if an error occurs.
	 *
	 */
	protected void _initLogger() throws InitializationException {
		try {
			LoggerControllerRegister lcr = ((LoggerControllerRegister)(weaveableC.getFcInterface("/logger-controller-register")));
			lcr.register("impl" ,this);
			logger.log(BasicLevel.DEBUG ,"Component initialized.");
		} catch (NoSuchInterfaceException e) {
		}
	}

	/**
	 * @see AbstractComponent#beforeFirstStart(Component)
	 */
	@DreamLifeCycle(on = DreamLifeCycleType.FIRST_START)
	protected void beforeFirstStart(Component componentItf) throws IllegalLifeCycleException {
		try {
			Task t1 = new PeriodicTask("task 1");
			Map<java.lang.String, java.lang.Object>  hints1 = new HashMap<java.lang.String, java.lang.Object> ();
			hints1.put("period" ,new Long(1000));
			org.objectweb.dream.control.activity.Util.addTask(componentItf ,t1 ,hints1);
			Task t2 = new PeriodicTask("task 2");
			Map<java.lang.String, java.lang.Object>  hints2 = new HashMap<java.lang.String, java.lang.Object> ();
			hints2.put("period" ,new Long(1500));
			org.objectweb.dream.control.activity.Util.addTask(componentItf ,t2 ,hints2);
		} catch (Exception e) {
			logger.log(BasicLevel.ERROR ,"Unable to register tasks" ,e);
		}
	}

	/**
	 * This attribute give the state of Dream Component. <br>
	 * the fcSstate value is {@link LifeCycleController#STARTED}
	 * {@link LifeCycleController#STOPPED}
	 */
	private String _fcState = LifeCycleController.STOPPED;

	/**
	 * True if it is the first start of the component
	 */
	private boolean _firstStart = true;

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 *
	 * @see org.objectweb.fractal.api.control.LifeCycleController#getFcState()
	 */
	public String getFcState() {
		return _fcState;
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 *
	 * @see org.objectweb.fractal.api.control.LifeCycleController#startFc()
	 */
	public void startFc() throws IllegalLifeCycleException {
		try {
			_fcState = LifeCycleController.STARTED;
			if (_firstStart) {
				_beforeFirstStart(weaveableC);
				_firstStart = false;
			}
		} catch (Exception e) {
			throw new IllegalLifeCycleException(e.getMessage());
		}
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 *
	 * @see org.objectweb.fractal.api.control.LifeCycleController#stopFc()
	 */
	public void stopFc() throws IllegalLifeCycleException {
		_fcState = LifeCycleController.STOPPED;
	}

	/**
	 * This method is called the first time the component is started.
	 *
	 * @param componentItf
	 *            the {@link Component }interface of this component.
	 */
	public void _beforeFirstStart(Component componentItf) throws IllegalLifeCycleException {
		try {
			beforeFirstStart(this.weaveableC);
		} catch (Exception e) {
			throw new IllegalLifeCycleException(e.getMessage());
		}
	}

	/**
	 * (non-Javadoc)
	 *
	 * @see org.objectweb.fractal.api.control.BindingController#bindFc(java.lang.String,
	 *      java.lang.Object) Method automatically generated with Spoon
	 *      <http://spoon.gforge.inria.fr>
	 */
	public void bindFc(String clientItfName, Object serverItf) throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException {
		if (clientItfName.equals("component")) {
			weaveableC = ((Component)(serverItf));
			try {
				_initLogger();
			} catch (InitializationException e) {
				throw new RuntimeException("An error occurred while initializing logger");
			}
			return ;
		}
		if (clientItfName.equals("task-manager")) {
			if (!(TaskManager.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (TaskManager.class.getName())));
			}
			taskManagerItf = ((TaskManager)(serverItf));
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	/**
	 * (non-Javadoc)
	 *
	 * @see org.objectweb.fractal.api.control.BindingController#listFc() Method
	 *      automatically generated with Spoon <http://spoon.gforge.inria.fr>
	 */
	public String[] listFc() {
		List<java.lang.String>  __interfaces__ = new ArrayList<java.lang.String> ();
		__interfaces__.add("task-manager");
		return __interfaces__.toArray(new String[__interfaces__.size()]);
	}

	/**
	 * (non-Javadoc)
	 *
	 * @see org.objectweb.fractal.api.control.BindingController#lookupFc(java.lang.String)
	 *      Method automatically generated with Spoon
	 *      <http://spoon.gforge.inria.fr>
	 */
	public Object lookupFc(String clientItfName) throws NoSuchInterfaceException {
		if (clientItfName.equals("task-manager")) {
			return taskManagerItf;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	/**
	 * (non-Javadoc)
	 *
	 * @see org.objectweb.fractal.api.control.BindingController#unbindFc(java.lang.String)
	 *      Method automatically generated with Spoon
	 *      <http://spoon.gforge.inria.fr>
	 */
	public void unbindFc(String clientItfName) throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException {
		if (clientItfName.equals("task-manager")) {
			taskManagerItf = null;
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

}
