/**
 * Dream
 * Copyright (C) 2003-2021 Inria Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s): Romain Lenglet
 */

package activity;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.Component;
import org.objectweb.dream.dreamannotation.DreamComponent;
import org.objectweb.dream.util.EmptyStringArray;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.dream.InitializationException;
import org.objectweb.fractal.fraclet.annotation.annotations.Interface;
import org.objectweb.dream.InterruptedPullException;
import org.objectweb.dream.control.logger.Loggable;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.dream.control.logger.LoggerControllerRegister;
import org.objectweb.dream.message.Message;
import org.objectweb.dream.control.lifecycle.NeedAsyncStartController;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.fraclet.annotation.annotations.Provides;
import org.objectweb.dream.Pull;
import org.objectweb.dream.PullException;
import org.objectweb.fractal.fraclet.annotation.annotations.Service;

/**
 * Server component providing a pull interface. The pull method waits on
 * <code>this</code>.
 */
@DreamComponent
@Provides(interfaces = @Interface(name = "out-pull", signature = Pull.class)
)
public class Server implements Pull , NeedAsyncStartController , Loggable , BindingController {
	/**
	 * Use this constructor for singeton bindings
	 *
	 * @param serviceName
	 *            The name of the java field
	 */
	public Server() {
	}

	@Service
	Component weaveableC;

	Logger logger;

	boolean reentrant = false;

	/**
	 * @see org.objectweb.dream.Pull#pull()
	 */
	public Message pull() throws PullException {
		if (!(reentrant)) {
			reentrant = true;
			Message m = null;
			try {
				logger.log(BasicLevel.INFO ,"reentrant call");
				m = ((Pull)(weaveableC.getFcInterface(Pull.OUT_PULL_ITF_NAME))).pull();
			} catch (InterruptedPullException e) {
				logger.log(BasicLevel.INFO ,"Wait interrupted 1");
				throw e;
			} catch (NoSuchInterfaceException e) {
				org.objectweb.dream.util.Error.bug(logger ,e);
			}
			reentrant = false;
			return m;
		}
		synchronized(this) {
			try {
				logger.log(BasicLevel.INFO ,"Waiting");
				wait();
				logger.log(BasicLevel.INFO ,"End of wait");
			} catch (InterruptedException e) {
				logger.log(BasicLevel.INFO ,"Wait interrupted 2");
				throw new InterruptedPullException(e);
			}
			return null;
		}
	}

	/**
	 * @see Loggable#setLogger(String, Logger)
	 */
	public void setLogger(String name, Logger logger) {
		if (name.equals("impl")) {
			this.logger = logger;
		}
	}

	/**
	 * @see org.objectweb.dream.control.lifecycle.NeedAsyncStartController#getFcNeedAsyncStart()
	 */
	public boolean getFcNeedAsyncStart() {
		return false;
	}

	/**
	 * Initializes the logger of this component. This method is called during initialization of
	 * the component controllers. <br>
	 *
	 * @throws InitializationException if an error occurs.
	 *
	 */
	protected void _initLogger() throws InitializationException {
		try {
			LoggerControllerRegister lcr = ((LoggerControllerRegister)(weaveableC.getFcInterface("/logger-controller-register")));
			lcr.register("impl" ,this);
			logger.log(BasicLevel.DEBUG ,"Component initialized.");
		} catch (NoSuchInterfaceException e) {
		}
	}

	/**
	 * @see org.objectweb.fractal.api.control.BindingController#listFc()
	 */
	public String[] listFc() {
		return EmptyStringArray.EMPTY_STRING_ARRAY;
	}

	/**
	 * (non-Javadoc)
	 *
	 * @see org.objectweb.fractal.api.control.BindingController#bindFc(java.lang.String,
	 *      java.lang.Object) Method automatically generated with Spoon
	 *      <http://spoon.gforge.inria.fr>
	 */
	public void bindFc(String clientItfName, Object serverItf) throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException {
		if (clientItfName.equals("component")) {
			weaveableC = ((Component)(serverItf));
			try {
				_initLogger();
			} catch (InitializationException e) {
				throw new RuntimeException("An error occurred while initializing logger");
			}
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	/**
	 * (non-Javadoc)
	 *
	 * @see org.objectweb.fractal.api.control.BindingController#lookupFc(java.lang.String)
	 *      Method automatically generated with Spoon
	 *      <http://spoon.gforge.inria.fr>
	 */
	public Object lookupFc(String clientItfName) throws NoSuchInterfaceException {
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	/**
	 * (non-Javadoc)
	 *
	 * @see org.objectweb.fractal.api.control.BindingController#unbindFc(java.lang.String)
	 *      Method automatically generated with Spoon
	 *      <http://spoon.gforge.inria.fr>
	 */
	public void unbindFc(String clientItfName) throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException {
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

}
