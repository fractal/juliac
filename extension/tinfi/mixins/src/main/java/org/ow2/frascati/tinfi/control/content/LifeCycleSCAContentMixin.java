/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.content;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;
import org.ow2.frascati.tinfi.api.control.SCAContentController;

/**
 * Provides lifecycle related checks to a {@link SCAContentController}. Check
 * that the component is stopped and then call the overriden method.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.5
 */
public abstract class LifeCycleSCAContentMixin
implements SCAContentController {

	// -------------------------------------------------------------------------
	// Implementation of the SCAContentController interface
	// -------------------------------------------------------------------------

	public void setFcContentClass( Class<?> c )
	throws IllegalLifeCycleException, ContentInstantiationException {

		/*
		 * Lifecycle check.
		 */
		String state = _this_weaveableOptLC.getFcState();
		if( ! state.equals(LifeCycleController.STOPPED) ) {
			String msg = "Component should be stopped";
			throw new IllegalLifeCycleException(msg);
		}

		_super_setFcContentClass(c);
	}

	public void setFcContent( Object content )
	throws IllegalLifeCycleException, ContentInstantiationException {

		/*
		 * Lifecycle check.
		 */
		String state = _this_weaveableOptLC.getFcState();
		if( ! state.equals(LifeCycleController.STOPPED) ) {
			String msg = "Component should be stopped";
			throw new IllegalLifeCycleException(msg);
		}

		_super_setFcContent(content);
	}


	// -------------------------------------------------------------------------
	// Fields and methods required by the mixin class in the base class
	// -------------------------------------------------------------------------

	protected abstract void _super_setFcContentClass( Class<?> c )
	throws IllegalLifeCycleException, ContentInstantiationException;

	protected abstract void _super_setFcContent( Object content )
	throws IllegalLifeCycleException, ContentInstantiationException;

	private LifeCycleController _this_weaveableOptLC;
}
