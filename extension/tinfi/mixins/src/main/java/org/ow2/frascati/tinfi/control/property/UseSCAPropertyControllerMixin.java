/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.property;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * Mixin layer for controllers which use the services provided by the {@link
 * SCAPropertyController} controller.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public abstract class UseSCAPropertyControllerMixin implements Controller {

	// -------------------------------------------------------------------------
	// Private constructor
	// -------------------------------------------------------------------------

	private UseSCAPropertyControllerMixin () {}

	// -------------------------------------------------------------------------
	// Implementation of the Controller interface
	// -------------------------------------------------------------------------

	/**
	 * Initialize the fields of this mixin and then calls the overriden method.
	 *
	 * @param ic information about the component to which this controller object
	 *      belongs.
	 * @throws InstantiationException if the initialization fails.
	 */
	public void initFcController(final InitializationContext ic)
	throws InstantiationException {

		weaveableSCAPC = (SCAPropertyController)
			ic.interfaces.get(SCAPropertyController.NAME);

		/*
		 * Propagate the invocation to other mixin layers.
		 */
		_super_initFcController(ic);
	}

	/**
	 * Reference to the property controller.
	 *
	 * The field is declared as protected since when mixed for the
	 * SCAContentController, it is used by generated subclasses. See the code
	 * generated for annotated properties in SCAContentControllerClassGenerator,
	 * method generateInitializeFcInstance().
	 */
	protected SCAPropertyController weaveableSCAPC;


	// -------------------------------------------------------------------------
	// Fields and methods required by the mixin class in the base class
	// -------------------------------------------------------------------------

	/**
	 * The {@link Controller#initFcController initFcController} method overriden
	 * by this mixin.
	 *
	 * @param ic information about the component to which this controller object
	 *      belongs.
	 * @throws InstantiationException if the initialization fails.
	 */
	public abstract void _super_initFcController( InitializationContext ic )
	throws InstantiationException;
}
