/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Philippe Merle
 */

package org.ow2.frascati.tinfi.control.intent;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.julia.ComponentInterface;
import org.ow2.frascati.tinfi.TinfiComponentInterceptor;
import org.ow2.frascati.tinfi.TinfiRuntimeException;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.InterfaceFilter;
import org.ow2.frascati.tinfi.api.InterfaceMethodFilter;
import org.ow2.frascati.tinfi.api.control.SCABasicIntentController;

/**
 * Mixin layer for implementing the {@link SCABasicIntentController} interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Philippe Merle <Philippe.Merle@inria.fr>
 * @since 0.4
 */
public abstract class SCABasicIntentControllerMixin
implements SCABasicIntentController {

	// -------------------------------------------------------------------------
	// Private constructor
	// -------------------------------------------------------------------------

	private SCABasicIntentControllerMixin () {}

	// -------------------------------------------------------------------------
	// Implementation of the SCABasicIntentController interface
	// -------------------------------------------------------------------------

	public void addFcIntentHandler( IntentHandler handler ) {
		Map<String,TinfiComponentInterceptor<?>> interceptors =
			getFcInterceptors();

		for (String name : interceptors.keySet()) {

			if( name.endsWith("-controller") || name.equals("component") ) {
				// Skip control interface
				continue;
			}

			TinfiComponentInterceptor<?> tci = interceptors.get(name);
			tci.addIntentHandler(handler);
		}
	}

	public void addFcIntentHandler(
		IntentHandler handler, InterfaceFilter filter ) {

		Object[] itfs = _this_weaveableOptC.getFcInterfaces();
		for (Object o : itfs) {
			Interface itf = (Interface) o;

			boolean accept = filter.accept(itf);
			if( ! accept ) {
				// Skip interfaces which are not accepted
				continue;
			}

			Object i = ((ComponentInterface)itf).getFcItfImpl();
			if( ! (i instanceof TinfiComponentInterceptor<?>) ) {
				/*
				 * Shouldn't occur. All interceptors extend
				 * TinfiComponentInterceptor.
				 */
				String name = itf.getFcItfName();
				String msg =
					"Interface "+name+
					" was expected to delegate to an object implementing"+
					" TinfiComponentInterceptor";
				throw new TinfiRuntimeException(msg);
			}

			TinfiComponentInterceptor<?> tci = (TinfiComponentInterceptor<?>) i;
			tci.addIntentHandler(handler);
		}
	}

	public void addFcIntentHandler(
		IntentHandler handler, InterfaceMethodFilter filter ) {

		Object[] itfs = _this_weaveableOptC.getFcInterfaces();
		for (Object o : itfs) {
			Interface itf = (Interface) o;

			Object i = ((ComponentInterface)itf).getFcItfImpl();
			if( ! (i instanceof TinfiComponentInterceptor<?>) ) {
				/*
				 * Shouldn't occur. All interceptors extend
				 * TinfiComponentInterceptor.
				 */
				String name = itf.getFcItfName();
				String msg =
					"Interface "+name+
					" was expected to delegate to an object implementing"+
					" TinfiComponentInterceptor";
				throw new TinfiRuntimeException(msg);
			}

			TinfiComponentInterceptor<?> tci = (TinfiComponentInterceptor<?>) i;
			Method[] methods = tci.getMethods();

			for (Method method : methods) {
				boolean accept = filter.accept(itf,method);
				if( accept ) {
					try {
						tci.addIntentHandler(handler,method);
					}
					catch (NoSuchMethodException nsme) {
						/*
						 * Shouldn't occur since method is retrieved from tci.
						 */
						throw new TinfiRuntimeException(nsme);
					}
				}
			}
		}
	}

	public void addFcIntentHandler( IntentHandler handler, String name )
	throws NoSuchInterfaceException {

		Map<String,TinfiComponentInterceptor<?>> interceptors =
			getFcInterceptors();

		if( ! interceptors.containsKey(name) ) {
			throw new NoSuchInterfaceException(name);
		}

		TinfiComponentInterceptor<?> tci = interceptors.get(name);
		tci.addIntentHandler(handler);
	}

	public void addFcIntentHandler(
		IntentHandler handler, String name, Method method )
	throws NoSuchInterfaceException, NoSuchMethodException {

		Map<String,TinfiComponentInterceptor<?>> interceptors =
			getFcInterceptors();

		if( ! interceptors.containsKey(name) ) {
			throw new NoSuchInterfaceException(name);
		}

		TinfiComponentInterceptor<?> tci = interceptors.get(name);
		tci.addIntentHandler(handler,method);
	}

	public List<IntentHandler> listFcIntentHandler( String name )
	throws NoSuchInterfaceException {

		Map<String,TinfiComponentInterceptor<?>> interceptors =
			getFcInterceptors();

		if( ! interceptors.containsKey(name) ) {
			throw new NoSuchInterfaceException(name);
		}

		TinfiComponentInterceptor<?> tci = interceptors.get(name);
		List<IntentHandler> handlers = tci.listIntentHandler();  // Already a copy
		return handlers;
	}

	public List<IntentHandler> listFcIntentHandler( String name, Method method )
	throws NoSuchInterfaceException, NoSuchMethodException {

		Map<String,TinfiComponentInterceptor<?>> interceptors =
			getFcInterceptors();

		if( ! interceptors.containsKey(name) ) {
			throw new NoSuchInterfaceException(name);
		}

		TinfiComponentInterceptor<?> tci = interceptors.get(name);
		List<IntentHandler> handlers = tci.listIntentHandler(method);  // Already a copy
		return handlers;
	}

	public void removeFcIntentHandler( IntentHandler handler ) {
		Map<String,TinfiComponentInterceptor<?>> interceptors =
			getFcInterceptors();
		Collection<TinfiComponentInterceptor<?>> tcis = interceptors.values();
		for (TinfiComponentInterceptor<?> tci : tcis) {
			tci.removeIntentHandler(handler);
		}
	}

	public void removeFcIntentHandler( IntentHandler handler, String name )
	throws NoSuchInterfaceException {

		Map<String,TinfiComponentInterceptor<?>> interceptors =
			getFcInterceptors();

		if( ! interceptors.containsKey(name) ) {
			throw new NoSuchInterfaceException(name);
		}

		TinfiComponentInterceptor<?> tci = interceptors.get(name);
		tci.removeIntentHandler(handler);
	}

	public void removeFcIntentHandler(
		IntentHandler handler, String name, Method method )
	throws NoSuchInterfaceException, NoSuchMethodException {

		Map<String,TinfiComponentInterceptor<?>> interceptors =
			getFcInterceptors();

		if( ! interceptors.containsKey(name) ) {
			throw new NoSuchInterfaceException(name);
		}

		TinfiComponentInterceptor<?> tci = interceptors.get(name);
		tci.removeIntentHandler(handler,method);
	}


	// -------------------------------------------------------------------------
	// Implementation specific
	// -------------------------------------------------------------------------

	/**
	 * Return the interceptors associated with the business interfaces of the
	 * current component.
	 */
	private Map<String,TinfiComponentInterceptor<?>> getFcInterceptors() {

		/*
		 * I used to cache the interceptors map in a field. Philippe noticed
		 * that this may be problematic for multiple references since additional
		 * references may be bound after that the map has been computed.
		 *
		 * The solution is not to cache interceptors. The loss in performance
		 * should not be that important.
		 */

		Map<String,TinfiComponentInterceptor<?>> interceptors = new HashMap<>();
		Object[] itfs = _this_weaveableOptC.getFcInterfaces();
		for (Object o : itfs) {

			Interface itf = (Interface) o;
			String name = itf.getFcItfName();
			Object i = ((ComponentInterface)itf).getFcItfImpl();

			if( ! (i instanceof TinfiComponentInterceptor<?>) ) {
				/*
				 * Shouldn't occur. All interceptors extend
				 * TinfiComponentInterceptor.
				 */
				String msg =
					"Interface "+name+
					" was expected to delegate to an object implementing"+
					" TinfiComponentInterceptor";
				throw new TinfiRuntimeException(msg);
			}

			TinfiComponentInterceptor<?> tci = (TinfiComponentInterceptor<?>) i;
			interceptors.put(name,tci);
		}

		return interceptors;
	}


	// -------------------------------------------------------------------------
	// Fields and methods required by the mixin class in the base class
	// -------------------------------------------------------------------------

	/**
	 * The <tt>weaveableOptC</tt> field required by this mixin. This field is
	 * supposed to reference the {@link Component} interface of the component to
	 * which this controller object belongs.
	 */
	public Component _this_weaveableOptC;
}
