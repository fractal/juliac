/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2013-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.binding;

import org.oasisopen.sca.ServiceReference;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.Interceptor;
import org.ow2.frascati.tinfi.TinfiComponentOutInterface;
import org.ow2.frascati.tinfi.control.content.SCAExtendedContentController;

/**
 * This mixin manages the references hold by client interfaces in relation with
 * the methods of the {@link BindingController} interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.6
 */
public abstract class InterfaceBindingControllerMixin implements BindingController {

	// -------------------------------------------------------------------------
	// Implementation of the BindingController interface
	// -------------------------------------------------------------------------

	public void bindFc( final String clientItfName, final Object serverItf )
	throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException {

		// Propagate to super layers
		_super_bindFc( clientItfName, serverItf );

		ComponentInterface ci = (ComponentInterface)
			_this_weaveableC.getFcInterface(clientItfName);

		// Skip client interceptor
		Interceptor interceptor = (Interceptor) ci.getFcItfImpl();
		interceptor.setFcItfDelegate(serverItf);

		// Inject reference into content instances
		TinfiComponentOutInterface<?> tci =
			(TinfiComponentOutInterface<?>) ci;
		ServiceReference<?> sr = tci.getServiceReference();
		_this_weaveableSCACC.setReferenceValue(clientItfName,sr);
	}

	public void unbindFc( final String clientItfName )
	throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException {

		// Propagate to super layers
		_super_unbindFc( clientItfName );

		ComponentInterface ci = (ComponentInterface)
			_this_weaveableC.getFcInterface(clientItfName);

		// Skip client interceptor
		Interceptor interceptor = (Interceptor) ci.getFcItfImpl();
		interceptor.setFcItfDelegate(null);

		// Inject null into content instances
		_this_weaveableSCACC.unsetReferenceValue(clientItfName);
	}


	// -------------------------------------------------------------------------
	// Fields and methods required by the mixin class in the base class
	// -------------------------------------------------------------------------

	private Component _this_weaveableC;
	private SCAExtendedContentController _this_weaveableSCACC;

	protected abstract void _super_bindFc( String clientItfName, Object serverItf )
	throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException;

	protected abstract void _super_unbindFc( String clientItfName )
	throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException;
}
