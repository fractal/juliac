/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.content;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.ow2.frascati.tinfi.api.control.SCAContentController;

/**
 * Mixin layer for controllers which use the services provided by the {@link
 * SCAContentController} controller.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public abstract class UseSCAContentControllerMixin implements Controller {

	// -------------------------------------------------------------------------
	// Implementation of the Controller interface
	// -------------------------------------------------------------------------

	public void initFcController(final InitializationContext ic)
	throws InstantiationException {

		weaveableSCACC = (SCAExtendedContentController)
			ic.interfaces.get(SCAContentController.NAME);

		/*
		 * Propagate the invocation to other mixin layers.
		 */
		_super_initFcController(ic);
	}

	/** Reference to the SCA content controller. */
	@SuppressWarnings("unused")
	private SCAExtendedContentController weaveableSCACC;


	// -------------------------------------------------------------------------
	// Fields and methods required by the mixin class in the base class
	// -------------------------------------------------------------------------

	public abstract void _super_initFcController( InitializationContext ic )
	throws InstantiationException;
}
