/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.component;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;

import org.oasisopen.sca.ComponentContext;
import org.oasisopen.sca.RequestContext;
import org.oasisopen.sca.ServiceReference;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.runtime.Juliac;
import org.ow2.frascati.tinfi.TinfiComponentOutInterface;
import org.ow2.frascati.tinfi.TinfiRuntimeException;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;
import org.ow2.frascati.tinfi.control.content.SCAExtendedContentController;
import org.ow2.frascati.tinfi.oasis.ServiceReferenceImpl;

/**
 * Mixin layer for implementing the {@link ComponentContext} controller.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class ComponentContextMixin implements ComponentContext {

	public String getURI() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @throws IllegalArgumentException
	 * 		if the specified argument is not a service reference
	 */
	public <B> ServiceReference<B> cast( B target ) {

		if( !(target instanceof ServiceReference<?>) ) {
			String msg = "Not a reference: "+target;
			throw new IllegalArgumentException(msg);
		}

		ServiceReference<?> ci = (ServiceReference<?>) target;
		Class<?> businessInterface = ci.getBusinessInterface();
		Object service = ci.getService();

		// Create a ServiceReference
		String signature = businessInterface.getName();
		String clname = Juliac.JULIAC_GENERATED+'.'+signature+"FcSR";

		try {
			Class<?> cl = Class.forName(clname);
			Constructor<?>[] ctrs = cl.getConstructors();
			Constructor<?> ctr = ctrs[0];
			Object sr = ctr.newInstance(businessInterface,service);

			@SuppressWarnings("unchecked")
			ServiceReference<B> ret = (ServiceReference<B>) sr;
			return ret;
		}
		catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	public <B> B getService( Class<B> businessInterface, String referenceName ) {

		B fcCltItf = getFcCltItf(businessInterface,referenceName);

		/*
		 * Do not return "fcCltItf". The idea is that the object returned by
		 * this method must implement the business interface while yet being
		 * able to execute the control logic (such as pushing conversations on
		 * the conversation stack) when invoked. This control logic is
		 * implemented by the generated subclasses of ServiceReferenceImpl.
		 */

		@SuppressWarnings("unchecked")
		TinfiComponentOutInterface<B> tco =
			(TinfiComponentOutInterface<B>) fcCltItf;
		ServiceReference<B> sr = tco.getServiceReference();

		@SuppressWarnings("unchecked")
		B service = (B) sr;
		return service;
	}

	public <B> ServiceReference<B> getServiceReference(
		Class<B> businessInterface, String referenceName ) {

		B service = getFcCltItf(businessInterface, referenceName);
		ServiceReference<B> sr =
			new ServiceReferenceImpl<>(businessInterface,service);
		return sr;
	}

	/**
	 * @since 1.4.1
	 */
	public <B> Collection<ServiceReference<B>> getServiceReferences(
		Class<B> businessInterface, String referenceName ) {

		Collection<B> services =
			getFcCltColItf(businessInterface,referenceName);
		Collection<ServiceReference<B>> srs = new ArrayList<>();
		for (B service : services) {
			ServiceReference<B> sr =
				new ServiceReferenceImpl<>(businessInterface,service);
			srs.add(sr);
		}
		return srs;
	}

	/**
	 * @since 1.4.1
	 */
	public <B> Collection<B> getServices(
		Class<B> businessInterface, String referenceName ) {

		Collection<B> fcCltItfs =
			getFcCltColItf(businessInterface,referenceName);

		// Same comment as in getService()

		Collection<B> services = new ArrayList<>();
		for (B fcCltItf : fcCltItfs) {
			@SuppressWarnings("unchecked")
			TinfiComponentOutInterface<B> tco =
				(TinfiComponentOutInterface<B>) fcCltItf;
			ServiceReference<B> sr = tco.getServiceReference();

			@SuppressWarnings("unchecked")
			B service = (B) sr;
			services.add(service);
		}

		return services;
	}

	public <B> B getProperty(Class<B> type, String propertyName) {

		if( ! _this_weaveableSCAPC.containsPropertyName(propertyName) ) {
			String msg = "No such property: "+propertyName;
			throw new IllegalArgumentException(msg);
		}

		Object value = _this_weaveableSCAPC.getValue(propertyName);

		try {
			@SuppressWarnings("unchecked")
			B ret = (B) value;
			return ret;
		}
		catch( ClassCastException cce ) {
			String msg =
				"Property "+propertyName+" is not of type "+type.getName();
			throw new IllegalArgumentException(msg);
		}
	}

	public <B> ServiceReference<B> createSelfReference(
		Class<B> businessInterface ) {

		B service = getFcSrvItf(businessInterface);
		ServiceReference<B> sr =
			new ServiceReferenceImpl<>(businessInterface,service);
		return sr;
	}

	public <B> ServiceReference<B> createSelfReference(
		Class<B> businessInterface, String serviceName ) {

		B service = getFcSrvItf(businessInterface, serviceName);
		ServiceReference<B> sr =
			new ServiceReferenceImpl<>(businessInterface,service);
		return sr;
	}

	public RequestContext getRequestContext() {
		RequestContext rc = _this_weaveableSCACC.getRequestContext();
		return rc;
	}


	// -------------------------------------------------------------------------
	// Implementation specific
	// -------------------------------------------------------------------------

	/**
	 * Return the Fractal server interface provided by this component which
	 * implements the specified business interface type.
	 *
	 * @param cl  the business interface class
	 * @return    the corresponding Fractal server interface
	 * @throws TinfiRuntimeException  if no such Fractal server interface exists
	 */
	private <B> B getFcSrvItf( Class<B> cl ) {

		Object[] itfs = _this_weaveableC.getFcInterfaces();
		for (Object o : itfs) {
			Interface itf = (Interface) o;
			InterfaceType it = (InterfaceType) itf.getFcItfType();
			if( ! it.isFcClientItf() ) {
				Class <?> itfClass = itf.getClass();
				if( cl.isAssignableFrom(itfClass) ) {
					@SuppressWarnings("unchecked")
					B ret = (B) itf;
					return ret;
				}
			}
		}

		String msg = "No such default service";
		throw new TinfiRuntimeException(msg);
	}

	/**
	 * Return the Fractal server interface provided by this component whose name
	 * is specified and which implements the specified business interface type.
	 *
	 * @param cl    the business interface class
	 * @param name  the interface name
	 * @return      the corresponding Fractal server interface
	 * @throws TinfiRuntimeException  if no such Fractal server interface exists
	 */
	private <B> B getFcSrvItf( Class<B> cl, String name ) {

		Interface itf = null;
		try {
			itf = (Interface) _this_weaveableC.getFcInterface(name);
		}
		catch (NoSuchInterfaceException e) {
			throw new TinfiRuntimeException(e);
		}

		InterfaceType it = (InterfaceType) itf.getFcItfType();
		if( it.isFcClientItf() ) {
			String msg = "No such service: "+name;
			throw new TinfiRuntimeException(msg);
		}
		Class<?> itfClass = itf.getClass();
		if( ! cl.isAssignableFrom(itfClass) ) {
			String msg = "Service can not be casted to: "+cl.getName();
			throw new TinfiRuntimeException(msg);
		}

		@SuppressWarnings("unchecked")
		B ret = (B) itf;
		return ret;
	}

	/**
	 * Return the Fractal client interface required by this component whose name
	 * is specified and which implements the specified business interface type.
	 *
	 * @param cl    the business interface class
	 * @param name  the interface name
	 * @return      the corresponding Fractal client interface
	 * @throws TinfiRuntimeException  if no such Fractal client interface exists
	 */
	private <B> B getFcCltItf( Class<B> cl, String name ) {

		Interface itf = null;
		try {
			itf = (Interface) _this_weaveableC.getFcInterface(name);
		}
		catch (NoSuchInterfaceException e) {
			throw new TinfiRuntimeException(e);
		}

		InterfaceType it = (InterfaceType) itf.getFcItfType();
		if( ! it.isFcClientItf() ) {
			String msg = "No such reference: "+name;
			throw new TinfiRuntimeException(msg);
		}
		Class<?> itfClass = itf.getClass();
		if( ! cl.isAssignableFrom(itfClass) ) {
			String msg = "Reference can not be casted to: "+cl.getName();
			throw new TinfiRuntimeException(msg);
		}

		@SuppressWarnings("unchecked")
		B ret = (B) itf;
		return ret;
	}

	/**
	 * Return the Fractal client interfaces required by this component whose
	 * name is specified and which implements the specified business interface
	 * type.
	 *
	 * @param cl    the business interface class
	 * @param name  the interface name
	 * @return      the corresponding Fractal client collection interfaces
	 * @throws TinfiRuntimeException  if no such Fractal client interface exists
	 * @since 1.4.1
	 */
	private <B> Collection<B> getFcCltColItf( Class<B> cl, String name ) {

		Collection<B> ret = new ArrayList<>();
		Object[] itfs = _this_weaveableC.getFcInterfaces();

		for (Object o : itfs) {
			Interface itf = (Interface) o;
			String itfname = itf.getFcItfName();
			if( itfname.startsWith(name) ) {
				InterfaceType it = (InterfaceType) itf.getFcItfType();
				if( ! it.isFcClientItf() ) {
					String msg = "No such reference: "+name;
					throw new TinfiRuntimeException(msg);
				}
				Class<?> itfClass = itf.getClass();
				if( ! cl.isAssignableFrom(itfClass) ) {
					String msg = "Reference can not be casted to: "+cl.getName();
					throw new TinfiRuntimeException(msg);
				}

				@SuppressWarnings("unchecked")
				B elt = (B) itf;
				ret.add(elt);
			}
		}

		return ret;
	}


	// -------------------------------------------------------------------------
	// Fields and methods required by the mixin class in the base class
	// -------------------------------------------------------------------------

	private Component _this_weaveableC;
	private SCAPropertyController _this_weaveableSCAPC;
	private SCAExtendedContentController _this_weaveableSCACC;
}
