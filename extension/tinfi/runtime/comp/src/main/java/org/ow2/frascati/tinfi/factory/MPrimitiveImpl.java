/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.factory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.api.Type;
import org.ow2.frascati.tinfi.api.control.IllegalPromoterException;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * Implementation of the control membrane for mPrimitive components.
 * mPrimitive components are primitive control components.
 *
 * This implementation extends the Juliac one with the support of the SCA
 * property control interface that is needed when component-based membranes
 * are defined with the SCA Assembly Language and when properties are specified
 * in such descriptors. This is the case for example for the description of the
 * Interceptor control component.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.5
 */
public class MPrimitiveImpl
extends org.objectweb.fractal.koch.factory.MPrimitiveImpl
implements SCAPropertyController {

	public MPrimitiveImpl( Type type, Object content ) {
		super(type,content);
	}


	// -----------------------------------------------------------------
	// Implementation of the SCAPropertyController interface
	// -----------------------------------------------------------------

	/** The property values. */
	private Map<String,Object> values = new HashMap<>();

	/** The property types. */
	private Map<String,Class<?>> types = new HashMap<>();

	public void setType( String name, Class<?> type ) {
		types.put(name,type);
	}

	public void setValue( String name, Object value ) {
		values.put(name,value);
	}

	public Class<?> getType( String name ) {
		return types.get(name);
	}

	public Object getValue( String name ) {
		return values.get(name);
	}

	public boolean containsPropertyName( String name ) {
		return values.containsKey(name);
	}

	public String[] getPropertyNames() {
		Set<String> propnames = values.keySet();
		String[] names = propnames.toArray( new String[propnames.size()] );
		return names;
	}

	public boolean isDeclared( String name ) {
		throw new UnsupportedOperationException();
	}

	public boolean hasBeenSet( String name ) {
		throw new UnsupportedOperationException();
	}

	public void setPromoter( String name, SCAPropertyController promoter )
	throws IllegalPromoterException {
		throw new UnsupportedOperationException();
	}

	public SCAPropertyController getPromoter( String name ) {
		throw new UnsupportedOperationException();
	}

	public void setPromoter(
		String name, SCAPropertyController promoter,
		String promoterPropertyName )
	throws IllegalPromoterException {
		throw new UnsupportedOperationException();
	}

	public String getPromoterPropertyName( String name ) {
		throw new UnsupportedOperationException();
	}

	public void removePromoter( String name ) {
		throw new UnsupportedOperationException();
	}
}
