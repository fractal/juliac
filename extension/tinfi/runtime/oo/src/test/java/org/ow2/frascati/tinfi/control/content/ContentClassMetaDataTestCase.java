/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.content;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;
import org.oasisopen.sca.annotation.Destroy;
import org.oasisopen.sca.annotation.Init;

/**
 * Unit tests for the {@link ContentClassMetaData} class.
 *
 * @author Lionel <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.3
 */
public class ContentClassMetaDataTestCase {

	@Test
	public void testBadInitDestroy() {
		try {
			ContentClassMetaData.get(BadInitDestroy.class);
			fail("BadInitDestroy should not be accepted by ContentClassMetaData");
		}
		catch (IllegalContentClassMetaData e) {}
	}

	@Test
	public void testDuplicateInitDestroy() {
		try {
			ContentClassMetaData.get(DuplicateInitDestroy.class);
			fail("DuplicateInitDestroy should not be accepted by ContentClassMetaData");
		}
		catch (IllegalContentClassMetaData e) {}
	}
}

class BadInitDestroy {

	@Init
	public void init( Object arg ) {}

	@Destroy
	public void destroy( Object arg ) {}
}

class DuplicateInitDestroy {

	@Init
	public void init1() {}

	@Init
	public void init2() {}

	@Destroy
	public void destroy1() {}

	@Destroy
	public void destroy2() {}
}

