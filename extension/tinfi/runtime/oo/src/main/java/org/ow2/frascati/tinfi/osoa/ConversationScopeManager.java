/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Authors: Lionel Seinturier, Philippe Merle
 */

package org.ow2.frascati.tinfi.osoa;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.osoa.sca.Conversation;
import org.osoa.sca.ConversationEndedException;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;
import org.ow2.frascati.tinfi.control.content.ContentClassMetaData;
import org.ow2.frascati.tinfi.control.content.scope.AbstractScopeManager;

/**
 * Manager for conversation-scoped components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Philippe Merle <Philippe.Merle@inria.fr>
 */
public class ConversationScopeManager extends AbstractScopeManager {

	public ConversationScopeManager(Component comp,ContentClassMetaData ccmd) {
		super(comp,ccmd);
	}

	public Object getFcContent() throws ContentInstantiationException {

		ConversationManager cm = ConversationManager.get();
		ConversationImpl conv = null;
		try {
			conv = cm.peek();
		}
		catch( EmptyStackException ese ) {
			/*
			 * Conversation stack empty: this means that the conversation-scoped
			 * component is invoked by a non-SCA client program.
			 */
		}

		Object content = null;
		if( contentMapConversationalScope.containsKey(conv) ) {
			content = contentMapConversationalScope.get(conv);
		}
		else {
			content = createAndInitializeFcInstance();
			if( ccmd.conversationIDAnnotatedElement != null ) {
				Object conversationID = conv==null ? null : conv.getConversationID();
				try {
					ccmd.conversationIDAnnotatedElement.set(content,conversationID);
				}
				catch (IllegalAccessException e) {
					throw new ContentInstantiationException(e);
				}
				catch (InvocationTargetException e) {
					throw new ContentInstantiationException(e);
				}
			}
			contentMapConversationalScope.put(conv,content);
		}

		/*
		 * Manage conversation expiration.
		 */
		if( conv!=null && ccmd.convMaxAge != 0 ) {
			long current = System.currentTimeMillis();
			long age = current - conv.getCreation();
			if( age > ccmd.convMaxAge ) {
				conv.end();
				contentMapConversationalScope.remove(conv);
				String msg =
					"Conversation has expired (maxAge: "+ccmd.convMaxAge+")";
				throw new ConversationEndedException(msg);
			}
		}
		if( conv!=null && ccmd.convMaxIdleTime != 0 ) {
			long current = System.currentTimeMillis();
			long idleTime = current - conv.getLastAccess();
			if( idleTime > ccmd.convMaxIdleTime ) {
				conv.end();
				contentMapConversationalScope.remove(conv);
				String msg =
					"Conversation has expired (maxIdleTime: "+
					ccmd.convMaxIdleTime+")";
				throw new ConversationEndedException(msg);
			}
		}

		return content;
	}

	public void releaseFcContent( Object content, boolean isEndMethod ) {
		/*
		 * TODO Destroy-annotated methods should be executed only when the conversation ends
		 */
		destroyFcInstance(content);
	}

	/**
	 * Return the current content instances.
	 *
	 * @return  the current content instances
	 * @since 1.2.1
	 */
	public Object[] getFcCurrentContents() {
		Collection<Object> contents = contentMapConversationalScope.values();
		return contents.toArray(new Object[contents.size()]);
	}

	/**
	 * The map of content instances for conversation scoped components.
	 * The key is the conversation identifier.
	 * The value is the associated content instance.
	 */
	private Map<Conversation,Object> contentMapConversationalScope = new HashMap<>();
}
