/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Authors: Lionel Seinturier, Philippe Merle
 */

package org.ow2.frascati.tinfi.control.content.scope;

import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;

/**
 * Interface for scope managers.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Philippe Merle <Philippe.Merle@inria.fr>
 */
public interface ScopeManager {

	/**
	 * Return a content instance according to the current scope policy.
	 *
	 * @throws ContentInstantiationException
	 *      if the content can not be instantiated
	 */
	public Object getFcContent() throws ContentInstantiationException;

	/**
	 * Release the specified content instance.
	 *
	 * @param content  the content instance which is released
	 * @param isEndMethod
	 *      <code>true</code> if the method which releases the content instance
	 *      is annotated with {@link org.osoa.sca.annotations.EndsConversation}
	 */
	public void releaseFcContent( Object content, boolean isEndMethod );

	/**
	 * Return the current content instances.
	 *
	 * @return  the current content instances
	 * @since 1.2.1
	 */
	public Object[] getFcCurrentContents();

	/**
	 * For composite-scoped components, specify the content instance to be used.
	 *
	 * @param content  the content instance
	 * @throws ContentInstantiationException
	 *         if the content can not be initialized or
	 *         if the component is not composite-scoped
	 * @since 1.4.3
	 */
	public void setFcContent( Object content )
	throws ContentInstantiationException;
}
