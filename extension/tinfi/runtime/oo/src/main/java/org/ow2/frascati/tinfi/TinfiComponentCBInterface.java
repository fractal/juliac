/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import org.oasisopen.sca.RequestContext;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;
import org.ow2.frascati.tinfi.api.control.SCAContentController;
import org.ow2.frascati.tinfi.control.content.SCAExtendedContentController;
import org.ow2.frascati.tinfi.oasis.RequestContextImpl;

/**
 * The root class for Tinfi component callback interfaces.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class TinfiComponentCBInterface<T> extends TinfiComponentInterface<T> {

	public TinfiComponentCBInterface() { super(); }
	public TinfiComponentCBInterface(
		Component owner, String s, Type type, boolean flag, Object obj ) {
		super(owner,s,type,flag,obj);
	}

	// -----------------------------------------------------------------------
	// Request context and content instance management.
	// These methods are used by generated subclasses.
	// -----------------------------------------------------------------------

	/**
	 * Register the current request context and return the content instance
	 * corresponding to the current scope policy.
	 *
	 * @since 1.1.1
	 */
	protected <B> B setRequestContextAndGet(
		String serviceName, Class<B> businessInterface, B service ) {

		// Register the current request context
		SCAExtendedContentController cc = getFcSCAContentCtrlItf();
		RequestContext rc =
			new RequestContextImpl<B>(serviceName,businessInterface,service);
		cc.setRequestContext(rc);

		// Get a new content instance
		try {
			Object content = cc.getFcContent();
			return (B) content;
		}
		catch( ContentInstantiationException e ) {
			throw new TinfiRuntimeException(e);
		}
	}

	/**
	 * Release the specified content instance previously retrieved with method
	 * <code>getFcContent()</code>.
	 *
	 * @param content  the content instance which is released
	 * @param isEndMethod
	 *      <code>true</code> if the method which releases the content instance
	 *      is annotated with @EndsConversation
	 * @since 1.1.1
	 */
	protected void releaseContent( Object content, boolean isEndMethod ) {
		SCAExtendedContentController cc = getFcSCAContentCtrlItf();
		cc.releaseFcContent(content,isEndMethod);
	}

	/**
	 * Return the reference of the SCA content controller associated to the
	 * current component.
	 */
	private SCAExtendedContentController getFcSCAContentCtrlItf() {
		try {
			if( cc == null ) {
				cc = (SCAExtendedContentController)
					owner.getFcInterface(SCAContentController.NAME);
			}
			return cc;
		  }
		  catch( NoSuchInterfaceException e ) {
			  throw new TinfiRuntimeException(e);
		  }
	}
	private SCAExtendedContentController cc;
}
