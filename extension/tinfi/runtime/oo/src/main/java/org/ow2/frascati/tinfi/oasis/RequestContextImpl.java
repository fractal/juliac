/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.oasis;

import javax.security.auth.Subject;

import org.oasisopen.sca.RequestContext;
import org.oasisopen.sca.ServiceReference;
import org.ow2.frascati.tinfi.CallbackManager;
import org.ow2.frascati.tinfi.SecuritySubjectManager;

/**
 * Implementation of a request context.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class RequestContextImpl<B> implements RequestContext {

	public RequestContextImpl(
		String serviceName, Class<B> businessInterface, B service ) {

		this.serviceName = serviceName;
		this.businessInterface = businessInterface;
		this.service = service;
	}

	// ----------------------------------------------------------------------
	// Implementation of the RequestContext interface
	// ----------------------------------------------------------------------

	private String serviceName;
	private Class<B> businessInterface;
	private B service;

	public Subject getSecuritySubject() {
		SecuritySubjectManager ssm = SecuritySubjectManager.get();
		Subject subject = ssm.getSecuritySubject();
		return subject;
	}

	public String getServiceName() {
		return serviceName;
	}

	public <CB> ServiceReference<CB> getServiceReference() {
		return new ServiceReferenceImpl(businessInterface,service);
	}

	public <CB> CB getCallback() {
		CallbackManager cm = CallbackManager.get();
		ServiceReference<CB> cr = (ServiceReference<CB>) cm.peek();
		CB service = cr.getService();
		return service;
	}

	public <CB> ServiceReference<CB> getCallbackReference() {
		CallbackManager cm = CallbackManager.get();
		ServiceReference<CB> cr = (ServiceReference<CB>) cm.peek();
		return cr;
	}
}
