/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Philippe Merle
 */

package org.ow2.frascati.tinfi;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.oasisopen.sca.RequestContext;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.Interceptor;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;
import org.ow2.frascati.tinfi.api.control.SCAContentController;
import org.ow2.frascati.tinfi.control.content.SCAExtendedContentController;
import org.ow2.frascati.tinfi.oasis.RequestContextImpl;

/**
 * The root class for Tinfi component interceptors.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Philippe Merle <Philippe.Merle@inria.fr>
 */
public abstract class TinfiComponentInterceptor<T> implements Interceptor {

	// -----------------------------------------------------------------------
	// Implementation of the Controller interface
	// -----------------------------------------------------------------------

	public void initFcController( InitializationContext ic )
	throws InstantiationException {

		fcComp = (Component) ic.getInterface("component");

		// scacc may be null, e.g. for scaComposite
		scacc = (SCAExtendedContentController)
				ic.getOptionalInterface(SCAContentController.NAME);
	}

	protected Component fcComp;
	private SCAExtendedContentController scacc;


	// -----------------------------------------------------------------------
	// Implementation of the Interceptor interface
	// -----------------------------------------------------------------------

	public void setFcItfDelegate( Object impl ) {
		/*
		 * The impl parameter is not declared to be of type T since the compiler
		 * in this case complains that the method does not override
		 * setFcItfDelegate from the super class.
		 */
		this.impl = (T) impl;
	}
	public T getFcItfDelegate() { return impl; }
	protected T impl;


	// -----------------------------------------------------------------------
	// Implementation of the Controller interface
	// -----------------------------------------------------------------------

	@Override
	public abstract Object clone();

	/**
	 * Initialize a clone instance. This method is called by generated
	 * subclasses.
	 */
	protected void initFcClone( TinfiComponentInterceptor<T> clone ) {
		clone.fcComp = fcComp;
		clone.scacc = scacc;
		clone.itf = itf;
	}


	// -----------------------------------------------------------------------
	// Methods for managing the reference between the interceptor and its
	// associated interface
	// -----------------------------------------------------------------------

	public void setFcItf( Interface itf ) {
		this.itf = itf;
	}

	public Interface getFcItf() {
		return itf;
	}

	private Interface itf;


	// -----------------------------------------------------------------------
	// Management of intent handlers
	// -----------------------------------------------------------------------

	/*
	 * The Fc string in method names has been removed to avoid name clashes when
	 * adding intent handlers on the SCAIntentController interface.
	 */

	/**
	 * The map of intent handlers associated with each component method managed
	 * by this interceptor. This map is queried by generated subclasses.
	 *
	 * @since 0.4
	 */
	protected Map<Method,List<IntentHandler>> intentHandlersMap;

	/**
	 * The array of component methods managed by this interceptor.
	 *
	 * @since 1.0
	 */
	protected Method[] methods;

	/**
	 * Initialize the map of intent handlers by adding an empty entry for each
	 * specified method.
	 */
	protected void initIntentHandlersMap( Method[] methods ) {
		this.methods = methods;
		intentHandlersMap = new HashMap<>();
		for (Method method : methods) {
			List<IntentHandler> handlers = new ArrayList<>();
			intentHandlersMap.put(method,handlers);
		}
	}

	/**
	 * Add the specified intent handler for all component methods managed by
	 * this interceptor.
	 */
	public void addIntentHandler( IntentHandler handler ) {
		for (Map.Entry<Method,List<IntentHandler>> entry : intentHandlersMap.entrySet()) {
			List<IntentHandler> handlers = entry.getValue();
			handlers.add(handler);
		}
	}

	/**
	 * Add the specified intent handler for the specified method.
	 *
	 * @throws NoSuchMethodException
	 *      if the specified method is not a component method managed by this
	 *      interceptor
	 */
	public void addIntentHandler( IntentHandler handler, Method method )
	throws NoSuchMethodException {

		if( ! intentHandlersMap.containsKey(method) ) {
			throw new NoSuchMethodException();
		}

		List<IntentHandler> handlers = intentHandlersMap.get(method);
		handlers.add(handler);
	}

	/**
	 * Return the array of component methods managed by this interceptor.
	 *
	 * @since 1.0
	 */
	public Method[] getMethods() {
		return methods;
	}

	/**
	 * Return the list of all intent handlers managed by this interceptor.
	 *
	 * @since 0.4.4
	 */
	public List<IntentHandler> listIntentHandler() {
		List<IntentHandler> all = new ArrayList<>();
		for (Map.Entry<Method,List<IntentHandler>> entry : intentHandlersMap.entrySet()) {
			List<IntentHandler> handlers = entry.getValue();
			all.addAll(handlers);
		}
		return all;
	}

	/**
	 * Return the list of all intent handlers managed by this interceptor for
	 * the specified method.
	 *
	 * @throws NoSuchMethodException
	 *      if the specified method is not a component method managed by this
	 *      interceptor
	 * @since 0.4.4
	 */
	public List<IntentHandler> listIntentHandler( Method method )
	throws NoSuchMethodException {

		if( ! intentHandlersMap.containsKey(method) ) {
			throw new NoSuchMethodException();
		}

		List<IntentHandler> handlers = intentHandlersMap.get(method);
		List<IntentHandler> all = new ArrayList<>();
		all.addAll(handlers);
		return all;
	}

	/**
	 * Remove the specified intent handler for all component methods managed by
	 * this interceptor.
	 */
	public void removeIntentHandler( IntentHandler handler ) {
		for (Map.Entry<Method,List<IntentHandler>> entry : intentHandlersMap.entrySet()) {
			List<IntentHandler> handlers = entry.getValue();
			handlers.remove(handler);
		}
	}

	/**
	 * Remove the specified intent handler for the specified method.
	 *
	 * @throws NoSuchMethodException
	 *      if the specified method is not a component method managed by this
	 *      interceptor
	 */
	public void removeIntentHandler( IntentHandler handler, Method method )
	throws NoSuchMethodException {

		if( ! intentHandlersMap.containsKey(method) ) {
			throw new NoSuchMethodException();
		}

		List<IntentHandler> handlers = intentHandlersMap.get(method);
		handlers.remove(handler);
	}


	// -----------------------------------------------------------------------
	// Request context stack and content instance management.
	// These methods are used by generated subclasses.
	// -----------------------------------------------------------------------

	/**
	 * Register the current request context and return the content instance
	 * corresponding to the current scope policy.
	 *
	 * @since 1.1.1
	 */
	protected T setRequestContextAndGet(
		String serviceName, Class<?> businessInterface, T service ) {

		/*
		 * The change from Class<T> to Class<?> in the 2nd parameter and the
		 * cast to (Class<T>) below have been introduced in r2328 following a
		 * bug report by Philippe. The purpose of the change is to obtain
		 * generated interceptors which are compilable with respect to generics.
		 */

		// Register the current request context
		RequestContext rc =
			new RequestContextImpl<T>(
				serviceName, (Class<T>)businessInterface, service );
		scacc.setRequestContext(rc);

		// Get a new content instance
		try {
			Object content = scacc.getFcContent();
			return (T) content;
		}
		catch( ContentInstantiationException e ) {
			throw new TinfiRuntimeException(e);
		}
	}

	/**
	 * Release the specified content instance previously retrieved with method
	 * <code>getFcContent()</code>.
	 *
	 * @param content  the content instance which is released
	 * @param isEndMethod
	 *      <code>true</code> if the method which releases the content instance
	 *      is annotated with @EndsConversation
	 * @since 1.1.1
	 */
	protected void releaseContent( Object content, boolean isEndMethod ) {
		scacc.releaseFcContent(content,isEndMethod);
	}
}
