/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.content;

/**
 * Exception thrown whenever the content class of a Tinfi component contains
 * illegal data (such as badly placed annotations).
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class IllegalContentClassMetaData extends Exception {

	private static final long serialVersionUID = 6675124056784592648L;

	public IllegalContentClassMetaData( String s ) {
		super(s);
	}

	public IllegalContentClassMetaData( Throwable t ) {
		super(t);
	}

	public IllegalContentClassMetaData( String s, Throwable t ) {
		super(s,t);
	}
}
