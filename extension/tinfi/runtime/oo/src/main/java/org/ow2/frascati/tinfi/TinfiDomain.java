/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.util.Fractal;

/**
 * This class provides helper methods for Tinfi conformance tests.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class TinfiDomain {

	/**
	 * Return an instance of the component described by the specified ADL.
	 */
	public static Component getComponent( String adl )
	throws
		ClassNotFoundException, InstantiationException, IllegalAccessException,
		IllegalLifeCycleException, NoSuchInterfaceException,
		java.lang.InstantiationException {

		Class<?> cl = Class.forName(adl);
		Object o = cl.newInstance();
		if( !(o instanceof Factory) ) {
			String msg =
				"The specified descriptor is not a component factory: "+adl;
			throw new IllegalArgumentException(msg);
		}
		Factory factory = (Factory) o;
		Component c = factory.newFcInstance();
		Fractal.getLifeCycleController(c).startFc();
		return c;
	}

	/**
	 * Return the <code>itfName</code> server interface
	 * for the component described by the specified ADL.
	 */
	public static <T> T getService( String adl, String itfName )
	throws
		ClassNotFoundException, InstantiationException, IllegalAccessException,
		IllegalLifeCycleException, NoSuchInterfaceException,
		java.lang.InstantiationException {

		Component c = getComponent(adl);
		T s = getService(c,itfName);
		return s;
	}

	/**
	 * Return the <code>itfName</code> server interface of type <code>cl</code>
	 * for the specified component.
	 */
	public static <T> T getService( Component c, String itfName )
	throws NoSuchInterfaceException {
		T s = (T) c.getFcInterface(itfName);
		return s;
	}

	/**
	 * Stop the specified component. Defined to mimic SCADomain.close().
	 */
	public static void close( Component c )
	throws IllegalLifeCycleException, NoSuchInterfaceException {
		Fractal.getLifeCycleController(c).stopFc();
	}
}
