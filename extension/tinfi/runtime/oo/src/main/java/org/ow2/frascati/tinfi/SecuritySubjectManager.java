/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2010-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Remi Melisson
 */

package org.ow2.frascati.tinfi;

import javax.security.auth.Subject;

/**
 * This class manages the thread-local security subject.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Remi Melisson <Remi.Melisson@inria.fr>
 * @since 1.4.1
 */
public class SecuritySubjectManager {

	/**
	 * Return the singleton instance of this class.
	 */
	public static SecuritySubjectManager get() {
		return instance;
	}
	private static SecuritySubjectManager instance =
		new SecuritySubjectManager();

	private static ThreadLocal<Subject> tl = new ThreadLocal<>();

	public void setSecuritySubject( Subject subject ) {
		tl.set(subject);
	}

	public Subject getSecuritySubject() {
		Subject subject = tl.get();
		return subject;
	}
}
