/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.osoa;

import java.util.EmptyStackException;

import org.ow2.frascati.tinfi.oasis.ServiceReferenceImpl;

/**
 * <p>
 * This class manages a thread-local stack of conversations.
 * </p>
 *
 * <p>
 * Prior to 1.4.5 I was using a Stack but Romain suggested a smarter
 * implementation based on a chain stored in {@link ServiceReferenceImpl}.
 * </p>
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@lifl.fr>
 */
public class ConversationManager {

	/**
	 * Return the singleton instance of this class.
	 *
	 * @since 1.1.1
	 */
	public static ConversationManager get() { return instance; }
	private static ConversationManager instance = new ConversationManager();

	/**
	 * The current conversation.
	 *
	 * The {@link ConversationImpl} class is used instead of the {@link
	 * org.osoa.sca.Conversation} interface since some public methods from
	 * {@link ConversationImpl} that are not defined in the interface are needed
	 * by clients that retrieve conversation from this manager.
	 *
	 * @since 1.1.1
	 */
	private static ThreadLocal<ConversationImpl> tl = new ThreadLocal<>();

	/** @since 1.1.1 */
	public void push( ConversationImpl conv ) {
		ConversationImpl current = tl.get();
		conv.setPrevious(current);
		tl.set(conv);
	}

	/** @since 1.1.1 */
	public ConversationImpl peek() {
		ConversationImpl current = tl.get();
		if( current == null ) {
			throw new EmptyStackException();
		}
		return current;
	}

	/** @since 1.1.1 */
	public ConversationImpl pop() {

		ConversationImpl current = tl.get();
		if( current == null ) {
			throw new EmptyStackException();
		}

		ConversationImpl previous = current.getPrevious();
		current.setPrevious(null);
		tl.set(previous);
		return current;
	}
}
