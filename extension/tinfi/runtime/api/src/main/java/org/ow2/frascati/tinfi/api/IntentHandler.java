/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.api;

import org.aopalliance.intercept.Interceptor;

/**
 * <p>
 * Interface implemented by intent handlers.
 * </p>
 *
 * <p>
 * An intent handler is a regular SCA component (primitive or composite) which
 * implements an intent policy. Intent policies are usually non functional
 * features (e.g. transaction, security, logging) which must be applied on
 * SCA business components.
 * </p>
 *
 * <p>
 * Tinfi provides an
 * <a href="http://en.wikipedia.org/wiki/Aspect-oriented_programming">AOP</a>
 * (Aspect-Oriented Programming) like mechanism for dealing with intents. Intent
 * handlers are interceptors which are installed on some selected service and
 * reference methods. Whenever the method is invoked, the invocation is trapped
 * and the intent handler is notified by calling the {@link
 * #invoke(IntentJoinPoint)} method. The argument of type {@link
 * IntentJoinPoint} given to this method allows:
 * </p>
 *
 * <ul>
 * <li>introspecting the intercepted method (so called join point in AOP terms)
 * by retrieving the component reference, the method and the arguments,</li>
 * <li>invoking the intercepted method (method {@link
 * IntentJoinPoint#proceed()}).</li>
 * </ul>
 *
 * <p>
 * Several intent handlers may be added on the same method. In this case, they
 * are executed in the order in which they were added.
 * </p>
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 0.3
 */
public interface IntentHandler extends Interceptor {

	/**
	 * This method defines the actions performed by this intent handler.
	 *
	 * @param ijp  the join point where the interception occured
	 * @return     the value returned by the intercepted method
	 */
	public Object invoke( IntentJoinPoint ijp ) throws Throwable;
}
