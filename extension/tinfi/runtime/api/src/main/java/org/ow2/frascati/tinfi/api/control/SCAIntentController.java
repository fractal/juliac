/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.api.control;

import java.lang.annotation.Annotation;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.ow2.frascati.tinfi.api.IntentHandler;

/**
 * Intent control interface for SCA primitive components.
 * This control interface extends {@link SCABasicIntentController} by adding
 * some control methods specific to SCA primitive components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 0.3
 */
public interface SCAIntentController extends SCABasicIntentController {

	/**
	 * Add the specified intent handler on all service methods of the current
	 * component which satisfy the following conditions:
	 *
	 * <ul>
	 * <li>the implementation method in the component class is associated with
	 *     an annotation of the specified class,</li>
	 * <li>the annotation provides a <code>String[] value()</code> method,</li>
	 * <li>one of the returned strings when calling <code>value()</code>is equal
	 *     to the specified value.</li>
	 * </ul>
	 *
	 * @param handler  the intent handler to add
	 * @param annotcl  the searched for annotation class
	 * @param value    the searched for parameter value
	 * @throws IllegalLifeCycleException  if the component is not stopped
	 */
	public <T extends Annotation>
		void addFcIntentHandler(
			IntentHandler handler, Class<T> annotcl, String value )
		throws IllegalLifeCycleException;
}
