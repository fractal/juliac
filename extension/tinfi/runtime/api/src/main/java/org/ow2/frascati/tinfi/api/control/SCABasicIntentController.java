/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.api.control;

import java.lang.reflect.Method;
import java.util.List;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.InterfaceFilter;
import org.ow2.frascati.tinfi.api.InterfaceMethodFilter;

/**
 * Intent control interface for SCA components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 0.4
 */
public interface SCABasicIntentController {

	/** <code>NAME</code> of the intent controller. */
	public static final String NAME = "sca-intent-controller";

	/**
	 * Add the specified intent handler on all business interfaces.
	 *
	 * @param handler  the intent handler
	 * @throws IllegalLifeCycleException  if the component is not stopped
	 */
	public void addFcIntentHandler( IntentHandler handler )
	throws IllegalLifeCycleException;

	/**
	 * Add the specified intent handler on all interfaces, business or control,
	 * that match the specified interface filter.
	 *
	 * @param handler  the intent handler
	 * @param filter   the interface filter
	 * @throws IllegalLifeCycleException  if the component is not stopped
	 * @since 1.0
	 */
	public void addFcIntentHandler(
		IntentHandler handler, InterfaceFilter filter )
	throws IllegalLifeCycleException;

	/**
	 * Add the specified intent handler on all interface methods, business or
	 * control, that match the specified filter.
	 *
	 * @param handler  the intent handler
	 * @param filter   the filter for interface methods
	 * @throws IllegalLifeCycleException  if the component is not stopped
	 * @since 1.0
	 */
	public void addFcIntentHandler(
		IntentHandler handler, InterfaceMethodFilter filter )
	throws IllegalLifeCycleException;

	/**
	 * Add the specified intent handler on the specified interface.
	 *
	 * @param handler  the intent handler
	 * @param name     the interface name
	 * @throws NoSuchInterfaceException   if the interface does not exist
	 * @throws IllegalLifeCycleException  if the component is not stopped
	 * @since 0.4.4
	 */
	public void addFcIntentHandler( IntentHandler handler, String name )
	throws NoSuchInterfaceException, IllegalLifeCycleException;

	/**
	 * Add the specified intent handler on the specified method of the specified
	 * interface.
	 *
	 * @param handler  the intent handler
	 * @param name     the interface name
	 * @param method   the method
	 * @throws NoSuchInterfaceException   if the interface does not exist
	 * @throws NoSuchMethodException      if the method does not exist
	 * @throws IllegalLifeCycleException  if the component is not stopped
	 * @since 1.0
	 */
	public void addFcIntentHandler(
		IntentHandler handler, String name, Method method )
	throws
		NoSuchInterfaceException, NoSuchMethodException,
		IllegalLifeCycleException;

	/**
	 * Return the list of intent handlers associated with the specified
	 * interface.
	 *
	 * @param name  the interface name
	 * @throws NoSuchInterfaceException  if the interface does not exist
	 * @since 0.4.4
	 */
	public List<IntentHandler> listFcIntentHandler( String name )
	throws NoSuchInterfaceException;

	/**
	 * Return the list of intent handlers associated with the specified method
	 * of the specified interface.
	 *
	 * @param name    the interface name
	 * @param method  the method
	 * @throws NoSuchInterfaceException  if the interface does not exist
	 * @throws NoSuchMethodException     if the method does not exist
	 * @since 1.0
	 */
	public List<IntentHandler> listFcIntentHandler( String name, Method method )
	throws NoSuchInterfaceException, NoSuchMethodException;

	/**
	 * Remove the specified intent handler on all interfaces, business and
	 * control, of the current component.
	 *
	 * @param handler  the intent handler to remove
	 * @throws IllegalLifeCycleException  if the component is not stopped
	 * @since 0.4.4
	 */
	public void removeFcIntentHandler( IntentHandler handler )
	throws IllegalLifeCycleException;

	/**
	 * Remove the specified intent handler on the interface (service or
	 * reference) whose name is specified.
	 *
	 * @param handler  the intent handler to remove
	 * @param name     the interface name
	 * @throws NoSuchInterfaceException   if the interface does not exist
	 * @throws IllegalLifeCycleException  if the component is not stopped
	 * @since 0.4.4
	 */
	public void removeFcIntentHandler( IntentHandler handler, String name )
	throws NoSuchInterfaceException, IllegalLifeCycleException;

	/**
	 * Remove the specified intent handler on the method of the interface
	 * (service or reference) whose name is specified.
	 *
	 * @param handler  the intent handler to remove
	 * @param name     the interface name
	 * @param method   the method
	 * @throws NoSuchInterfaceException   if the interface does not exist
	 * @throws NoSuchMethodException      if the method does not exist
	 * @throws IllegalLifeCycleException  if the component is not stopped
	 * @since 1.0
	 */
	public void removeFcIntentHandler(
		IntentHandler handler, String name, Method method )
	throws
		NoSuchInterfaceException, NoSuchMethodException,
		IllegalLifeCycleException;
}
