To compile and run the example:
	mvn -Ptinfi:run

In addition, this module illustrates the usage of the Retrotranslator tool to
generate JDK 1.3 compliant bytecode. The steps for putting into practise this
usage scenario follow.

1/ Download Retrotranslator from http://retrotranslator.sourceforge.net
2/ Install it
3/ Edit retro13.bat and set the value of the RETRO_HOME property to the
   directory where Retrotranslator has been installed

To generate a JDK 1.3 compliant version of the example:
	mvn assembly:assembly
	retro13.bat

To run the example (with a JRE 1.3):
	run13.bat
