/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package example.hw;

import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.ow2.frascati.tinfi.TinfiDomain;

/**
 * Automate the launching of the HelloWorld example and check that the example
 * runs as expected.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 0.2
 */
public class HelloWorldTestCase {

	@Test
	public void testHelloWorld()
	throws
		IOException, IllegalArgumentException, ClassNotFoundException,
		InstantiationException, IllegalAccessException,
		java.lang.InstantiationException, InstantiationException,
		IllegalLifeCycleException, NoSuchInterfaceException {

		/*
		 * Use the Console class to direct the output of the example to a
		 * temporary file.
		 */
		File tmp = File.createTempFile("helloworld-bin-",".txt");
		PrintStream ps = new PrintStream(tmp);
		Console.ps = ps;
		Runnable r = TinfiDomain.getService("example.hw.HelloWorld","r");
		r.run();
		ps.close();

		/*
		 * Dump the content of the temporay file to the console for visual
		 * check.
		 */
		FileReader fr = new FileReader(tmp);
		int b;
		while( (b=fr.read()) != -1 ) {
			char c = (char) b;
			System.err.print(c);
		}
		fr.close();

		File tmpExpected = File.createTempFile("helloworld-bin-",".txt");
		ps = new PrintStream(tmpExpected);
		for (String expected : expecteds) {
			ps.println(expected);
		}
		ps.close();

		/*
		 * Compare the output with the expected result.
		 */
		fr = new FileReader(tmp);
		FileReader frExpected = new FileReader(tmpExpected);
		int line=1,col=1;
		while( (b=fr.read()) != -1 ) {
			char c = (char) b;
			char e = (char) frExpected.read();
			if( c != e ) {
				String msg = "Unexpected character at line "+line+", column "+col;
				System.err.println();
				System.err.println("Expected output is: ");
				for (String expected : expecteds) {
					System.err.println(expected);
				}
				fail(msg);
			}
			if( c == '\n' ) {
				line++;
				col=1;
			}
			else {
				col++;
			}
		}
		frExpected.close();
		fr.close();

		/*
		 * Delete temporary files.
		 */
		tmp.delete();
		tmpExpected.delete();
	}

	final static private String[] expecteds =
		new String[]{
			"CLIENT created",
			"CLIENT initialized",
			"SERVER created",
			"Server: begin printing...",
			"->hello world",
			"Server: print done.",
		};
}
