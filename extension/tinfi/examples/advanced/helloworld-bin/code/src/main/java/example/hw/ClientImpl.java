package example.hw;

import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;

public class ClientImpl implements Runnable {

	public ClientImpl() {
		Console.println("CLIENT created");
	}

	@Init
	public void init() {
		Console.println("CLIENT initialized");
	}

	public void run() {
		s.print("hello world");
	}

	@Reference
	public Service s;
}
