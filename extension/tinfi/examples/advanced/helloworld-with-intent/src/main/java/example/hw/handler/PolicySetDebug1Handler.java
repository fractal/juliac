/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package example.hw.handler;

import org.osoa.sca.annotations.PolicySets;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.IntentJoinPoint;

import example.hw.Console;

/**
 * A handler for the policy set debug1.
 * This handler applies on component classes annotated with @{@link
 * PolicySets}("debug1").
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 0.3
 */
@Scope("STATELESS")
public class PolicySetDebug1Handler implements IntentHandler {

	public Object invoke( IntentJoinPoint ijp ) {

		String methodName = ijp.getMethod().getName();
		Console.get().println("<< debug1: before "+methodName);

		try {
			Object ret = ijp.proceed();
			Console.get().println("<< debug1: after");
			return ret;
		}
		catch( Throwable e ) {
			Console.get().print("<< MyException thrown with message: ");
			Console.get().println(e.getMessage());
			return null;
		}
	}
}
