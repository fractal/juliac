package example.hw.srv;

import org.oasisopen.sca.annotation.Property;
import org.oasisopen.sca.annotation.Scope;

import example.hw.PrinterItf;

@Scope("COMPOSITE")
public class ServerImpl2 implements PrinterItf {

	@Property(name="header") private String header;
	@Property(name="count") private int count;

	public ServerImpl2() {}

	public void print(final String msg) {
		Printer printer = new Printer();
		String header = getHeader();
		printer.print("Server2: begin printing...");
		for (int i = 0 ; i < (count) ; ++i) {
			printer.print(header + msg);
		}
		printer.print("Server2: print done.");
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(final String header) {
		this.header = header;
	}

	public int getCount() {
		return count;
	}

	public void setCount(final String count) {
		this.count = Integer.valueOf(count);
	}
}
