package example.hw;

import java.util.ArrayList;
import java.util.List;

import org.oasisopen.sca.annotation.Reference;
import org.oasisopen.sca.annotation.Scope;

@Scope("COMPOSITE")
public class ClientImpl implements Runnable {

	public ClientImpl() {}

	@Override
	public void run() {
		for (PrinterItf m : ms) {
			m.print("hello world");
		}
	}

	@Reference(name="servers")
	public List<PrinterItf> ms = new ArrayList<PrinterItf>();
}
