/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package example.hw;

import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;

import org.junit.jupiter.api.Test;

/**
 * Automate the launching of the HelloWorld example for the ultra-merge
 * optimization level of Juliac and check that the example runs as expected.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.3
 */
public class HelloWorldTestCase {

	@Test
	public void testHelloWorld()
	throws
		ClassNotFoundException, IOException, java.lang.InstantiationException,
		IllegalAccessException, InvocationTargetException,
		NoSuchMethodException {

		@SuppressWarnings("unchecked")
		Class<Runnable> hw = (Class<Runnable>) Class.forName("HelloWorld");

		/*
		 * Use the Console class to direct the output of the example to a
		 * temporary file.
		 */
		File tmp = File.createTempFile("ultra-merge-",".txt");
		PrintStream ps = new PrintStream(tmp);
		Console.ps = ps;
		Runnable r = hw.getConstructor().newInstance();
		r.run();
		ps.close();

		/*
		 * Dump the content of the temporay file to the console for visual
		 * check.
		 */
		FileReader fr = new FileReader(tmp);
		int b;
		while( (b=fr.read()) != -1 ) {
			char c = (char) b;
			System.err.print(c);
		}
		fr.close();

		File tmpExpected = File.createTempFile("ultra-merge-",".txt");
		ps = new PrintStream(tmpExpected);
		for (String expected : expecteds) {
			ps.println(expected);
		}
		ps.close();

		/*
		 * Compare the output with the expected result.
		 */
		fr = new FileReader(tmp);
		FileReader frExpected = new FileReader(tmpExpected);
		int line=1,col=1;
		while( (b=fr.read()) != -1 ) {
			char c = (char) b;
			char e = (char) frExpected.read();
			if( c != e ) {
				String msg = "Unexpected character at line "+line+", column "+col;
				System.err.println();
				System.err.println("Expected output is: ");
				for (String expected : expecteds) {
					System.err.println(expected);
				}
				fail(msg);
			}
			if( c == '\n' ) {
				line++;
				col=1;
			}
			else {
				col++;
			}
		}
		frExpected.close();
		fr.close();

		/*
		 * Delete temporary files.
		 */
		tmp.delete();
		tmpExpected.delete();
	}

	final static private String[] expecteds =
		new String[]{
			"Server: begin printing...",
			"-> hello world",
			"Server: print done.",
			"Server2: begin printing...",
			"=> hello world",
			"=> hello world",
			"Server2: print done.",
			"Server3: begin printing...",
			"== hello world",
			"== hello world",
			"== hello world",
			"Server3: print done."
		};
}
