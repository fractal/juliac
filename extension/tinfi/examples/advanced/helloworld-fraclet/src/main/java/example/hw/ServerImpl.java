/***
 * OW2 FraSCAti Tinfi Examples: HelloWorld-Fraclet
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 */

package example.hw;

import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Interface;

@Component(provides=@Interface(name="s",signature=PrinterItf.class))
public class ServerImpl implements PrinterItf {

	@Attribute
	private String header;

	@Attribute
	private String count;

	public ServerImpl() {
		Console.println("SERVER created");
	}

	public void print(final String msg) {
		Console.println("Server: begin printing...");
		for (int i = 0 ; i < getCount() ; ++i) {
			Console.println(((header) + msg));
		}
		Console.println("Server: print done.");
	}

	public int getCount() {
		return Integer.valueOf(count);
	}
}
