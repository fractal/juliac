package example.hw;

import java.io.IOException;

import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.JuliacModuleItf;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;

public class Conf implements JuliacModuleItf {

	private FCSourceCodeGeneratorItf sca;
	private FCSourceCodeGeneratorItf osgi;

	public void init( JuliacItf jc ) throws IOException {
		sca = new org.ow2.frascati.tinfi.opt.oo.FCOOCtrlSourceCodeGenerator();
		osgi = new org.ow2.frascati.tinfi.osgi.FCOOCtrlSourceCodeGenerator();
		sca.setCtrlDescPrefix("/sca/");
		osgi.setCtrlDescPrefix("/osgi/");
		sca.init(jc);
		osgi.init(jc);
	}

	public void close( JuliacItf jc ) throws IOException {
		osgi.close(jc);
		sca.close(jc);
	}
}
