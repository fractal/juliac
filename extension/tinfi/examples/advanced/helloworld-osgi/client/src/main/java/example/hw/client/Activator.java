package example.hw.client;

import java.util.Dictionary;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import example.hw.itf.Service;

public class Activator implements BundleActivator {

	public void start( BundleContext context ) throws InvalidSyntaxException {

		ServiceReference[] refs =
			context.getServiceReferences( Service.class.getName(), null );
		Service service = (Service) context.getService(refs[0]);

		ClientImpl client = new ClientImpl();
		client.setService(service);

		context.registerService( Runnable.class.getName(), client, null );

		Bundle bundle = context.getBundle();
		Dictionary dict = bundle.getHeaders();
		String name = (String) dict.get("Bundle-Name");
		System.err.println("Bundle <"+name+"> started");
	}

	public void stop( BundleContext context ) {

		Bundle bundle = context.getBundle();
		Dictionary dict = bundle.getHeaders();
		String name = (String) dict.get("Bundle-Name");
		System.err.println("Bundle <"+name+"> stopped");
	}
}
