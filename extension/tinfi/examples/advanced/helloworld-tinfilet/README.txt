This example illustrates the generation of code from SCA annotated component
classes with no SCA Assembly Language descriptors.

To compile and run the example:
	mvn test
