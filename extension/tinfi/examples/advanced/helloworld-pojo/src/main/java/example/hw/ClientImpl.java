package example.hw;

public class ClientImpl implements Runnable {

	public ClientImpl() {
		Console.println("CLIENT created");
	}

	public void run() {
		s.print("hello world");
	}

	public void setS( PrinterItf s ) {
		this.s = s;
	}

	private PrinterItf s;
}
