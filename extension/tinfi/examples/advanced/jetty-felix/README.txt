This example illustrates the use of the OSGi implementation type in a .composite
file. Two components are included in the composite: a Java component which
steers a Jetty HTTP server OSGi component.

To compile and run the example:
	mvn test
