/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package example.hw;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.ow2.frascati.tinfi.TinfiDomain;

/**
 * Automate the launching of the HelloWorld example and check that the example
 * runs as expected.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 0.4.4
 */
public class HelloWorldTestCase {

	@Test
	public void helloWorld()
	throws
		InstantiationException, NoSuchInterfaceException,
		ClassNotFoundException, java.lang.InstantiationException,
		IllegalAccessException, IllegalLifeCycleException {

		Component root = TinfiDomain.getComponent("Property");
		ClassItf s = (ClassItf) root.getFcInterface("s");
		Class<?> cl = s.getClassProperty();

		assertEquals(Runnable.class,cl);
	}
}
