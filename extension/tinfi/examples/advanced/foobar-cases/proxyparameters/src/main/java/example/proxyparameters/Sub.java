package example.proxyparameters;

import java.util.List;

public interface Sub<T> {

	public List<?> sub1();
	public List<T> sub2();
	public T sub3();
}
