package example.proxyparameters;

/**
 * @author Philippe Merle <philippe.merle@inria.fr>
 * @since 1.3
 */
public interface Implementation {}
