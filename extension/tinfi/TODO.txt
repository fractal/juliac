mixins/
* provide a scaOsgiPrimitive membrane which mixes the controllers (property,
  intent) from the scaPrimitive membrane (suggested by Philippe)

module/opt-oo/
* check the way callback references should be working with SCA 1.1 since
  @Conversational is no longer part of the API
* when managing callback references, build the delegation chain
  CallableReference > OutFcIf > InFcItf instead of CallableReference > InFcItf
* fix service reference class and SCA interceptor class generation for generic
  interfaces with components from the BF which provide interfaces which are
  generic, e.g. o.o.f.bf.BindingFactoryPlugin. The mismatch occurs since the
  generated proxy classes define also generic parameters, e.g. when extending
  ServiceReference<B>. Check whether this bug has been fixed or not by r2312.
* generate per content class SCA content controller implementations

runtime/oo/
* support polymorphic setter methods in POJOs (see p12 of "SCA Service Component
  Architecture Java Component Implementation Specification")

runtime/oo-dyn/
* provide a runtime-generic module which would be able to instantiate components
  without relying on a generated initializer class. The idea would be that this
  module would also rely on ASM to generate FcItf, FcOutIf, FcSR,
  InterceptorLCSCAIntent (for scaPrimitive) and InterceptorIntent (for
  scaComposite) classes. By this way, Tinfi could be used in a mode where it
  would be independent of juliac-compiler and juliac-jdt. The assumption is that
  it should not be so difficult to generate Fc* and Interceptor* classes with
  ASM (not so sure for Interceptor*, but anyway...) whereas generating
  initializer classes with ASM or julia-asm is significantly more difficult.
  Having a generic initializer should bypass this difficulty.

tests/oasis/
* add a test for membrane scaCompositeWithContent
 