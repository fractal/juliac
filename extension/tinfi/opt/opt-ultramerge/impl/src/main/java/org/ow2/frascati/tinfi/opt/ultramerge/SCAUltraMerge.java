/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.ultramerge;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.oasisopen.sca.annotation.Property;
import org.oasisopen.sca.annotation.Reference;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.JuliacRuntimeException;
import org.objectweb.fractal.juliac.core.desc.BindingDesc;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;
import org.objectweb.fractal.juliac.spoon.AbstractUltraMerge;
import org.objectweb.fractal.juliac.spoon.helper.CtNamedElementHelper;

import spoon.Launcher;
import spoon.reflect.code.CtExpression;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtExecutable;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtParameter;
import spoon.reflect.reference.CtExecutableReference;
import spoon.reflect.reference.CtTypeReference;

/**
 * Source code generator which merges all component business code in a single
 * class. This source code generator handles component business code annotated
 * with the OSOA SCA API.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.3.1
 */
public class SCAUltraMerge extends AbstractUltraMerge {

	public SCAUltraMerge( JuliacItf jc ) {
		super(jc);
	}

	// ----------------------------------------------------------------------
	// Abstract methods
	// ----------------------------------------------------------------------

	@Override
	protected boolean isAttributeAnnotatedField( CtField<?> ctfield ) {
		Property p = ctfield.getAnnotation(Property.class);
		return p != null;
	}

	@Override
	protected String getAttributeName( CtField<?> ctfield ) {
		Property p = ctfield.getAnnotation(Property.class);
		return p.name();
	}

	@Override
	protected CtTypeReference<?> getCollectionFieldType( CtField<?> ctfield ) {

		CtTypeReference<?> fieldType = ctfield.getType();
		List<CtTypeReference<?>> genericTypes =
			fieldType.getActualTypeArguments();
		if( genericTypes==null || genericTypes.size()!=1 ) {
			final String msg =
				CtNamedElementHelper.toEclipseClickableString(ctfield)+
				" expected to be of type List<K>";
			throw new JuliacRuntimeException(msg);
		}

		return genericTypes.get(0);
	}

	@Override
	protected String getRequiresName( CtField<?> ctfield ) {
		Reference r = ctfield.getAnnotation(Reference.class);
		String name = r.name();
		if( name.length() == 0 ) {
			name = ctfield.getSimpleName();
		}
		return name;
	}

	@Override
	protected boolean isRequiredAnnotatedField( CtField<?> ctfield ) {
		return ctfield.hasAnnotation(Reference.class);
	}

	@Override
	protected boolean isRequiredCollectionAnnotatedField( CtField<?> ctfield ) {

		boolean b = ctfield.hasAnnotation(Reference.class);
		if(!b) return false;

		CtTypeReference<?> tref = ctfield.getType();
		String qname = tref.getQualifiedName();
		Class<?> cl = jc.loadClass(qname);

		/*
		 * Fields of type List hold references for collection references.
		 */
		b = List.class.isAssignableFrom(cl);
		return b;
	}

	@Override
	protected void initRequiredCollectionField(
		CtField<?> field, ComponentDesc<?> cdesc, String targetname,
		Collection<CtMethod<?>> duplicateMethods, CtField<?> dst ) {

		// Collection client interface: initiliaze with a Map
		String cltItfName = getRequiresName(field);
		Iterator<BindingDesc> bdescs =
			cdesc.getBindingDescs().stream().
			filter(bdesc->bdesc.getCltItfName().startsWith(cltItfName)).iterator();
		StringBuilder sb = new StringBuilder();
		sb.append("class C {");
		sb.append("java.util.List<");
		CtTypeReference<?> tref = getCollectionFieldType(field);
		sb.append(tref.toString());
		sb.append("> f = new java.util.ArrayList<>(){{");
		while( bdescs.hasNext() ) {
			String name = bdescs.next().getCltItfName();
			sb.append("add(new "+tref.toString()+"(){");
			ComponentDesc<?> srv = cdesc.getBoundPrimitiveComponent(name);
			String srvContentClassName = srv.getContentClassName();
			CtClass<?> srvclass = field.getFactory().Class().get(srvContentClassName);
			for (CtExecutableReference<?> execRef : tref.getAllExecutables()) {
				CtExecutable<?> method = execRef.getDeclaration();
				sb.append("public ");
				sb.append(method.getType());
				sb.append(' ');
				sb.append(method.getSimpleName());
				sb.append('(');
				boolean first=true;
				for (CtParameter<?> param : method.getParameters()) {
					if(first) first=false; else sb.append(',');
					sb.append(param.getType());
					sb.append(' ');
					sb.append(param.getSimpleName());
				}
				sb.append(')');
				if( method.getThrownTypes().size() != 0 ) {
					sb.append("throws ");
					first=true;
					for (CtTypeReference<? extends Throwable> thrown : method.getThrownTypes()) {
						if(first) first=false; else sb.append(',');
						sb.append(thrown);
					}
				}
				sb.append('{');
				if( ! method.getType().toString().equals("void") ) {
					sb.append("return ");
				}
				String methodName = execRef.getSimpleName();
				sb.append(targetname);
				sb.append(".this.");
				sb.append(methodName);
				CtMethod<?> srvmethod =
					srvclass.getMethod(methodName,execRef.getParameters().toArray(new CtTypeReference[0]));
				if( duplicateMethods.contains(srvmethod) ) {
					sb.append('_');
					sb.append(srvmethod.getDeclaringType().getQualifiedName().replace('.','_'));
				}
				sb.append('(');
				first=true;
				for (CtParameter<?> param : method.getParameters()) {
					if(first) first=false; else sb.append(',');
					sb.append(param.getSimpleName());
				}
				sb.append(");}");
			}
			sb.append("});");
		}
		sb.append("}};}");
		CtClass<?> c = Launcher.parseClass(sb.toString());
		CtField<?> f = c.getField("f");
		dst.setDefaultExpression((CtExpression)f.getDefaultExpression());
	}
}
