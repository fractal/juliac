<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>org.ow2.fractal.juliac.tinfi</groupId>
		<artifactId>frascati-tinfi-oo-parent</artifactId>
		<version>2.9-SNAPSHOT</version>
		<relativePath>../pom.xml</relativePath>
	</parent>

	<artifactId>frascati-tinfi-tests-fraclet</artifactId>
	<packaging>jar</packaging>
	<name>Tinfi Opt Level OO Conf Tests Fraclet</name>

	<dependencies>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-engine</artifactId>
		</dependency>

		<dependency>
			<groupId>org.ow2.fractal.juliac.tinfi</groupId>
			<artifactId>frascati-tinfi-runtime</artifactId>
			<version>${project.version}</version>
		</dependency>

		<!-- Needed to run the org.ow2.frascati.tinfi.mix.Mix TestCase -->
		<dependency>
			<groupId>org.objectweb.fractal.julia</groupId>
			<artifactId>julia-asm</artifactId>
			<version>${julia.version}</version>
		</dependency>
		<dependency>
			<groupId>org.objectweb.fractal.julia</groupId>
			<artifactId>julia-mixins</artifactId>
		</dependency>
		<dependency>
			<groupId>org.objectweb.fractal</groupId>
			<artifactId>fractal-util</artifactId>
		</dependency>
	</dependencies>

	<build>
		<plugins>

			<!-- ======================================================== -->
			<!-- Copy julia-mixins sources to target/dependency           -->
			<!-- Copy frascati-tinfi-mixins sources to target/dependency  -->
			<!-- ======================================================== -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-dependency-plugin</artifactId>
				<executions>
					<execution>
						<id>copy-julia-tinfi-mixins</id>
						<phase>generate-sources</phase>
						<goals><goal>copy</goal></goals>
						<configuration>
							<artifactItems>
								<artifactItem>
									<groupId>org.objectweb.fractal.julia</groupId>
									<artifactId>julia-mixins</artifactId>
									<version>${julia.version}</version>
									<classifier>sources</classifier>
								</artifactItem>
								<artifactItem>
									<groupId>org.ow2.fractal.juliac.tinfi</groupId>
									<artifactId>frascati-tinfi-mixins</artifactId>
									<version>${project.version}</version>
									<classifier>sources</classifier>
								</artifactItem>
							</artifactItems>
							<stripVersion>true</stripVersion>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<!-- ========================= -->
			<!-- Generate code with Juliac -->
			<!-- ========================= -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>exec-maven-plugin</artifactId>
				<executions>
					<execution>
						<id>juliac-compile</id>
						<phase>process-classes</phase>
						<goals><goal>java</goal></goals>
					</execution>
				</executions>
				<configuration>
					<mainClass>org.objectweb.fractal.juliac.core.JuliacCmdLine</mainClass>
					<arguments>
						<argument>--basedir</argument>
						<argument>${project.basedir}</argument>
						<argument>--srcs</argument>
						<argument>src/main/java</argument>
						<argument>--mixins</argument>
						<argument>target/dependency/julia-mixins-sources.jar:target/dependency/frascati-tinfi-mixins-sources.jar</argument>
						<argument>org.ow2.frascati.tinfi.ComponentName</argument>
						<argument>org.ow2.frascati.tinfi.Controller</argument>
						<argument>org.ow2.frascati.tinfi.IntentBase</argument>
						<argument>org.ow2.frascati.tinfi.IntentHandlerCounter</argument>
						<argument>org.ow2.frascati.tinfi.OneWay</argument>
						<argument>org.ow2.frascati.tinfi.Property</argument>
						<argument>org.ow2.frascati.tinfi.PropertyReInject</argument>
						<argument>org.ow2.frascati.tinfi.ReferenceMultiple</argument>
						<argument>org.ow2.frascati.tinfi.ReferenceMandatoryNotSet</argument>
						<argument>org.ow2.frascati.tinfi.ReferenceOptional</argument>
						<argument>org.ow2.frascati.tinfi.SCAContainerMembrane</argument>
						<argument>org.ow2.frascati.tinfi.SCAContentController</argument>
						<argument>org.ow2.frascati.tinfi.ScopeComposite</argument>
						<argument>org.ow2.frascati.tinfi.ScopeConversation</argument>
						<argument>org.ow2.frascati.tinfi.ScopeRequest</argument>
						<argument>org.ow2.frascati.tinfi.mix.Mix</argument>
						<argument>org.ow2.frascati.tinfi.oabdoun.optionalreference.OptionalReference</argument>
						<argument>org.ow2.frascati.tinfi.vaudaux.callback.Forge</argument>
					</arguments>
					<includePluginDependencies>true</includePluginDependencies>
					<sourceRoot>target/generated-sources/juliac</sourceRoot>
				</configuration>
				<dependencies>
					<dependency>
						<groupId>org.ow2.fractal.juliac</groupId>
						<artifactId>juliac-fractaladl-service</artifactId>
						<version>${project.version}</version>
					</dependency>
					<dependency>
						<groupId>org.ow2.fractal.juliac</groupId>
						<artifactId>juliac-spoon-classloader-service</artifactId>
						<version>${project.version}</version>
					</dependency>
					<dependency>
						<groupId>org.ow2.fractal.juliac.tinfi</groupId>
						<artifactId>frascati-tinfi-oo-service</artifactId>
						<version>${project.version}</version>
					</dependency>
					<dependency>
						<groupId>org.ow2.fractal.juliac</groupId>
						<artifactId>juliac-oo-service</artifactId>
						<version>${project.version}</version>
					</dependency>
				</dependencies>
			</plugin>

			<!-- ====================== -->
			<!-- Compile generated code -->
			<!-- ====================== -->
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<executions>
					<execution>
						<id>juliac-compile</id>
						<phase>process-classes</phase>
						<goals><goal>compile</goal></goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<configuration>
					<systemPropertyVariables>
						<fractal.provider>org.objectweb.fractal.util.ChainedProvider</fractal.provider>
						<fractal.providers>org.objectweb.fractal.juliac.runtime.Juliac,org.objectweb.fractal.julia.Julia</fractal.providers>
					</systemPropertyVariables>
				</configuration>
			</plugin>

		</plugins>
	</build>

</project>
