/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oasisopen.sca.annotation.Property;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.juliac.commons.ipf.DuplicationInjectionPointException;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;
import org.ow2.frascati.tinfi.control.content.ContentClassMetaData;
import org.ow2.frascati.tinfi.control.content.IllegalContentClassMetaData;

/**
 * Class for testing component properties.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class PropertyTestCase {

	protected PropertyItf itf;
	protected SCAPropertyController scapc;

	protected void initCCMD()
	throws
		IllegalContentClassMetaData, NoSuchFieldException,
		DuplicationInjectionPointException, NoSuchMethodException {

		/*
		 * Dynamically declare that PropertyImpl#unannotatedProp corresponds to
		 * an injection point for a property.
		 */
		ContentClassMetaData ccmd =
			ContentClassMetaData.get(PropertyImpl.class);
		Field field =
			ccmd.fcContentClass.getDeclaredField("unannotatedFieldProp");
		Property annot = new PropertyAnnotationImpl("unannotatedFieldProp");
		ccmd.props.put(field,annot);

		/*
		 * Dynamically declare that PropertyImpl#setUnannotatedProp(String)
		 * corresponds to an injection point for a property.
		 */
		Method method =
			ccmd.fcContentClass.getDeclaredMethod(
				"setUnannotatedProp",String.class);
		annot = new PropertyAnnotationImpl("unannotatedProp");
		ccmd.props.put(method,annot);
	}

	@AfterEach
	public void closeCCMD() throws IllegalContentClassMetaData {

		/*
		 * Unregister PropertyImpl#unannotatedFieldProp and
		 * PropertyImpl#setUnannotatedProp(String).
		 */
		ContentClassMetaData ccmd =
			ContentClassMetaData.get(PropertyImpl.class);
		ccmd.props.remove("unannotatedFieldProp");
		ccmd.props.remove("unannotatedProp");
	}

	@BeforeEach
	public void setUp() throws Exception {

		initCCMD();

		String adl = getClass().getPackage().getName()+".Property";
		String service = "r";
		Component comp = TinfiDomain.getComponent(adl);

		ContentController cc = Fractal.getContentController(comp);
		Component[] subs = cc.getFcSubComponents();
		Component inner = subs[0];

		itf = TinfiDomain.getService(inner,service);
		scapc = (SCAPropertyController)
			inner.getFcInterface(SCAPropertyController.NAME);
	}

	public static class PropertyAnnotationImpl implements Property {
		private String name;
		public PropertyAnnotationImpl( String name ) {
			this.name = name;
		}
		public String name() {
			return name;
		}
		public boolean required() {
			return false;
		}
		public Class<? extends Annotation> annotationType() {
			return Property.class;
		}
	}

	@Test
	public void testNewValueForMandatoryProp() {
		/*
		 * Not strictly related to this test, but a mandatory property must be
		 * set.
		 */
		scapc.setValue("mandatoryProp","bar");

		String expected = "bar";
		String value = itf.mandatoryPropValue();
		assertEquals(expected,value);
	}

	@Test
	public void testNewValuePropWithDefault() {
		scapc.setValue("mandatoryProp","bar");

		int expected = 12;
		scapc.setValue("propWithDefault",expected);
		int value = itf.propWithDefaultValue();
		assertEquals(expected,value);
	}

	/**
	 * @since 1.4.1
	 */
	@Test
	public void testPropIntegerForInt() {
		scapc.setValue("mandatoryProp","bar");

		int expected = 42;
		scapc.setValue("propWithDefault",Integer.valueOf(expected));
		int value = itf.propWithDefaultValue();
		assertEquals(expected,value);
	}

	/**
	 * @since 1.4.1
	 */
	@Test
	public void testPropIntForInteger() {
		scapc.setValue("mandatoryProp","bar");

		Integer expected = Integer.valueOf(42);
		scapc.setValue("fieldPropInteger",expected.intValue());
		Integer value = itf.fieldPropIntegerValue();
		assertEquals(expected,value);
	}

	/**
	 * @since 1.1.2
	 */
	@Test
	public void testGetValuePropWithDefault() {
		scapc.setValue("mandatoryProp","bar");

		/*
		 * Invoked to trigger the instantiation of the content instance. Else
		 * the default value for the property can not be retrieved.
		 */
		itf.propWithDefaultValue();

		String expected = "default";
		Object value = scapc.getValue("fieldPropWithDefault");
		assertEquals(expected,value);
	}

	/**
	 * @since 1.1.2
	 */
	@Test
	public void testGetTwiceValuePropWithDefault() {
		scapc.setValue("mandatoryProp","bar");

		/*
		 * Invoked to trigger the instantiation of the content instance. Else
		 * the default value for the property can not be retrieved.
		 */
		itf.propWithDefaultValue();

		String expected = "default2";
		scapc.setValue("fieldPropWithDefault",expected);
		Object value = scapc.getValue("fieldPropWithDefault");
		assertEquals(expected,value);
	}

	/**
	 * @since 1.2.1
	 */
	@Test
	public void testReInjection()
	throws
		ClassNotFoundException, InstantiationException, IllegalAccessException,
		IllegalLifeCycleException, NoSuchInterfaceException,
		java.lang.InstantiationException, InterruptedException {

		String adl = getClass().getPackage().getName()+".PropertyReInject";
		String service = "r";
		Component comp = TinfiDomain.getComponent(adl);
		final PropertyReInjectItf itf = TinfiDomain.getService(comp,service);

		/*
		 * Retrieve the property control interface of the server component which
		 * is the 2nd subcomponent of the PropertyReInject composite.
		 */
		ContentController cc = Fractal.getContentController(comp);
		Component[] subs = cc.getFcSubComponents();
		Component server = subs[1];
		final SCAPropertyController scapc = (SCAPropertyController)
			server.getFcInterface(SCAPropertyController.NAME);

		final String expected1 = "foo";
		final String expected2 = "bar";

		scapc.setValue("prop",expected1);

		String v1 = itf.getValue();
		assertEquals(expected1,v1);

		// Start a second conversation
		Thread t =
			new Thread() {
				@Override
				public void run() {
					/*
					 * Check that the new conversation instance has been
					 * instantiated with the previously set value.
					 */
					String v2 = itf.getValue();
					assertEquals(expected1,v2);

					// Change the value of the property
					scapc.setValue("prop",expected2);

					// Check that the value has been re-injected in the instance
					v2 = itf.getValue();
					assertEquals(expected2,v2);

					// End the second conversation
					itf.end();
				}
			};
		t.start();
		t.join();

		/*
		 * Check that the value has been re-injected also in the instance
		 * associated with the first conversation.
		 */
		v1 = itf.getValue();
		assertEquals(expected2,v1);

		// End the first conversation
		itf.end();
	}
}
