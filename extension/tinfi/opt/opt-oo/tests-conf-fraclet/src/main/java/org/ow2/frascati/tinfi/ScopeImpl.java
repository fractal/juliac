/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import org.oasisopen.sca.annotation.Destroy;
import org.oasisopen.sca.annotation.Init;
import org.objectweb.fractal.fraclet.annotations.Lifecycle;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.types.Step;

/**
 * Root class for component implementations used for testing the {@link
 * org.osoa.sca.annotations.Scope} annotation.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class ScopeImpl implements ScopeItf {

	public static int initcounter = 0;
	public static int destroycounter = 0;
	public static int startcounter = 0;
	public static int stopcounter = 0;

	@Lifecycle(step=Step.CREATE)
	public void init() {
		initcounter++;
	}

	@Lifecycle(step=Step.DESTROY)
	public void destroy() {
		destroycounter++;
	}

	@Lifecycle(step=Step.START)
	public void start() {
		startcounter++;
	}

	@Lifecycle(step=Step.STOP)
	public void stop() {
		stopcounter++;
	}

	@Requires
	protected ScopeItf c;

	public Object getContent( long millis ) {
		try {
			Thread.sleep(millis);
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new TinfiRuntimeException(e);
		}

		return this;
	}

	public Object[] loop() {
		Object loop = c.getContent(0);
		Object[] result = new Object[]{this,loop};
		return result;
	}

	/**
	 * A simple do nothing service method used to test the number of times the
	 * @{@link {@link Init} and @{@link Destroy} annotated methods are invoked.
	 *
	 * @see org.ow2.frascati.tinfi.ScopeTestCase
	 * @since 1.1.1
	 */
	public void run() {}
}
