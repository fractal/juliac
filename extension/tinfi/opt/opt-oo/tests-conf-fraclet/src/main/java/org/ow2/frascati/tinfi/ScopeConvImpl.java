/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import org.oasisopen.sca.annotation.Callback;
import org.oasisopen.sca.annotation.Scope;
import org.osoa.sca.annotations.ConversationAttributes;

/**
 * Component implementation for testing the conversation scope policy.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
@Scope("CONVERSATION")
@ConversationAttributes(maxAge="2 seconds", maxIdleTime="1 seconds")
public class ScopeConvImpl implements ScopeConvItf {

	@Callback
	public ScopeConvCallbackItf cb;

	public Object first( long millis ) {
		try {
			Thread.sleep(millis);
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new TinfiRuntimeException(e);
		}

		return this;
	}

	public Object second() {
		return this;
	}

	public Object third() {
		return this;
	}

	public void callback() {
		cb.onMessage("");
	}

	public void callbackEndsConversation() {
		cb.onMessageEndsConversation("");
	}
}
