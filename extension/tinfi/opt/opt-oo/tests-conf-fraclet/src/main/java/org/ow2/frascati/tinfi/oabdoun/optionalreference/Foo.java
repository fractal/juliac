package org.ow2.frascati.tinfi.oabdoun.optionalreference;

import org.oasisopen.sca.annotation.Service;

/**
 * @author Olivier Abdoun <Olivier.Abdoun@inria.fr>
 */
@Service(Foo.class)
public interface Foo {
	public String getFoo();
}
