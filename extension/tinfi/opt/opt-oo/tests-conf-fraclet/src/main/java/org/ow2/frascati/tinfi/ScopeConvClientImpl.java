/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import org.oasisopen.sca.annotation.Scope;
import org.objectweb.fractal.fraclet.annotations.Requires;

/**
 * Component implementation for testing the conversation scope policy.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
@Scope("COMPOSITE")
public class ScopeConvClientImpl implements ScopeConvItf, ScopeConvCallbackItf {

	@Requires
	public ScopeConvItf s;

	public Object first( long millis ) {
		return s.first(millis);
	}

	public Object second() {
		return s.second();
	}

	public Object third() {
		return s.third();
	}

	public void callback() {
		s.callback();
	}

	public void callbackEndsConversation() {
		s.callbackEndsConversation();
	}

	public void onMessage( String msg ) {}

	public void onMessageEndsConversation( String msg ) {}
}
