/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import org.osoa.sca.annotations.Scope;

/**
 * Component implementation used for testing {@link
 * org.osoa.sca.annotations.OneWay} methods.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
@Scope("COMPOSITE")
public class OneWayImpl implements OneWayItf {

	private boolean running = false;

	/**
	 * Sleep for a given amount of time before returning to simulate concurrent
	 * calls.
	 *
	 * @param millis  the length of time in milliseconds before returning the call
	 * @see org.ow2.frascati.tinfi.OneWayTestCase
	 */
	public void run( long millis ) {
		try {
			running = true;
			Thread.sleep(millis);
			running = false;
		}
		catch (InterruptedException e) {
			running = false;
			Thread.currentThread().interrupt();
			throw new TinfiRuntimeException(e);
		}
	}

	/**
	 * Return true if the run method is running.
	 *
	 * @see org.ow2.frascati.tinfi.OneWayTestCase
	 */
	public boolean running() {
		return running;
	}
}
