/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import org.osoa.sca.annotations.Property;

/**
 * Component implementation used for testing properties.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
@SuppressWarnings("unused")
public class PropertyImpl implements PropertyItf {

	private String mandatoryProp;
	private int propWithDefault = 12;

	@Property
	private String fieldPropWithDefault = "default";

	private String unannotatedFieldProp;

	@Property
	private String propWithType;

	@Property
	private Integer fieldPropInteger;

	@Property(required=true)
	public void setMandatoryProp( String value ) {
		mandatoryProp = value;
	}

	@Property
	public void setPropWithDefault( int value ) {
		propWithDefault = value;
	}

	public String mandatoryPropValue() {
		return mandatoryProp;
	}

	public int propWithDefaultValue() {
		return propWithDefault;
	}

	public Integer fieldPropIntegerValue() {
		return fieldPropInteger;
	}

	public void setUnannotatedProp( String value ) {}
}
