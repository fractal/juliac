package org.ow2.frascati.tinfi.oabdoun.optionalreference;

import org.osoa.sca.annotations.Service;

/**
 * @author Olivier Abdoun <Olivier.Abdoun@inria.fr>
 */
@Service
public interface Bar {

	public String getBar();

}
