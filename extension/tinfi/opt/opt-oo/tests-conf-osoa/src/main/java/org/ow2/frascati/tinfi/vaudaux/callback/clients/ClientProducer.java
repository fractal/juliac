package org.ow2.frascati.tinfi.vaudaux.callback.clients;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.ow2.frascati.tinfi.vaudaux.callback.NotifierProducer;

/**
 * @author Guillaume Vaudaux-Ruth <guillaume.vaudaux-ruth@inria.fr>
 */
@Service(Runnable.class)
@Scope("COMPOSITE")
public class ClientProducer implements Runnable {

	private NotifierProducer notifierProducer;

	@Reference(name="notifier")
	public void setNotifierProducer(NotifierProducer notifierProducer){
		this.notifierProducer = notifierProducer;
	}

	public void run() {
		notifierProducer.sendMessage("topic", "msg");
	}
}
