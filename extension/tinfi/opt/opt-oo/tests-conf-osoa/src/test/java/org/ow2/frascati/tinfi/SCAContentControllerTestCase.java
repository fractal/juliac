/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;
import org.ow2.frascati.tinfi.api.control.SCAContentController;

/**
 * Class for testing the SCA Content Controller.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 0.3
 */
public class SCAContentControllerTestCase {

	private Component client;

	@BeforeEach
	public void setUp()
	throws
		ClassNotFoundException, InstantiationException, IllegalAccessException,
		IllegalLifeCycleException, NoSuchInterfaceException,
		java.lang.InstantiationException {

		String adl = getClass().getPackage().getName()+".SCAContentController";
		Component root = TinfiDomain.getComponent(adl);
		Component[] subs =
			Fractal.getContentController(root).getFcSubComponents();
		client = subs[0];
	}

	/**
	 * @since 1.0
	 */
	@Test
	public void testGetContentClass()
	throws IllegalLifeCycleException, NoSuchInterfaceException {

		SCAContentControllerItf r =
			(SCAContentControllerItf) client.getFcInterface("r");
		Fractal.getLifeCycleController(client).startFc();

		Class<?> expected = SCAContentControllerImpl1.class;
		Class<?> c = r.getContentClass();
		assertEquals(expected,c);
	}

	/**
	 * Test that the content class of <code>scaPrimitive</code> components can
	 * be changed by invoking {@link
	 * SCAContentController#setFcContentClassName(String)}.
	 *
	 * @since 1.0
	 */
	@Test
	public void testSetContentClass()
	throws
		IllegalLifeCycleException, NoSuchInterfaceException,
		ContentInstantiationException {

		SCAContentControllerItf r =
			(SCAContentControllerItf) client.getFcInterface("r");

		// Start and invoke the component once
		Fractal.getLifeCycleController(client).startFc();
		r.getContentClass();

		// Reconfigure the content class
		Class<?> expected = SCAContentControllerImpl2.class;
		Fractal.getLifeCycleController(client).stopFc();
		SCAContentController scacc = (SCAContentController)
			client.getFcInterface(SCAContentController.NAME);
		scacc.setFcContentClass(expected);
		Fractal.getLifeCycleController(client).startFc();

		// Re-invoke the component and check that the content class has changed
		Class<?> c = r.getContentClass();
		assertEquals(expected,c);
	}

	/**
	 * Test that the content class of <code>scaPrimitive</code> components can
	 * not be changed if the component is not stopped.
	 *
	 * @since 1.0
	 */
	@Test
	public void testSetContentClassNotStopped()
	throws
		NoSuchInterfaceException, IllegalLifeCycleException,
		ContentInstantiationException {

		SCAContentControllerItf r =
			(SCAContentControllerItf) client.getFcInterface("r");

		// Start and invoke the component once
		Fractal.getLifeCycleController(client).startFc();
		r.getContentClass();

		// Try to reconfigure the content class
		SCAContentController scacc = (SCAContentController)
			client.getFcInterface(SCAContentController.NAME);
		Class<?> expected = SCAContentControllerImpl2.class;

		try {
			scacc.setFcContentClass(expected);
			final String msg =
				"The content class can not be changed without "+
				"stopping the component. IllegalLifeCycleException should "+
				"have been thrown.";
			fail(msg);
		}
		catch( IllegalLifeCycleException ilce ) {}
	}
}
