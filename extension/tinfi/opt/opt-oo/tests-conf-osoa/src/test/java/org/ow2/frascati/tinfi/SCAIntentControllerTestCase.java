/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Method;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.util.Fractal;
import org.osoa.sca.annotations.PolicySets;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.IntentJoinPoint;
import org.ow2.frascati.tinfi.api.InterfaceFilter;
import org.ow2.frascati.tinfi.api.InterfaceMethodFilter;
import org.ow2.frascati.tinfi.api.control.SCAIntentController;

/**
 * Class for testing the SCA Intent Controller.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.0
 */
public class SCAIntentControllerTestCase {

	private Component root, comp;
	private SCAIntentController ic;
	private IntentHandler h;
	private IntentBaseClientItf itf;
	private IntentHandlerCounterItf countItf;

	@BeforeEach
	public void setUp()
	throws
		ClassNotFoundException, InstantiationException, IllegalAccessException,
		IllegalLifeCycleException, NoSuchInterfaceException,
		java.lang.InstantiationException {

		String adl = getClass().getPackage().getName()+".IntentBase";
		String service = "r";
		root = TinfiDomain.getComponent(adl);
		Component[] subs =
			Fractal.getContentController(root).getFcSubComponents();
		comp = subs[0];
		itf = TinfiDomain.getService(root,service);
		ic = (SCAIntentController)
			comp.getFcInterface(SCAIntentController.NAME);

		/*
		 * Instantiate the intent handler component.
		 */
		adl = getClass().getPackage().getName()+".IntentHandlerCounter";
		Component handler = TinfiDomain.getComponent(adl);
		h = TinfiDomain.getService(handler,"h");
		countItf = TinfiDomain.getService(handler,"count");
	}

	/**
	 * Add the intent handler on all annotated service methods.
	 */
	@Test
	public void testAnnotatedServiceMethods()
	throws IllegalLifeCycleException, NoSuchInterfaceException {

		Fractal.getLifeCycleController(comp).stopFc();
		ic.addFcIntentHandler(h,PolicySets.class,IntentBaseClientItf.COUNT);
		Fractal.getLifeCycleController(comp).startFc();

		itf.runAnnotatedMethod();
		int count = countItf.getCount();
		assertEquals(2,count);

		itf.runUnAnnotatedMethod();
		count = countItf.getCount();
		assertEquals(2,count);
	}

	/**
	 * Add the intent handler on all services and references of a component.
	 */
	@Test
	public void testAllServicesAndReferences()
	throws IllegalLifeCycleException, NoSuchInterfaceException {

		Fractal.getLifeCycleController(comp).stopFc();
		ic.addFcIntentHandler(h);
		Fractal.getLifeCycleController(comp).startFc();

		itf.runAnnotatedMethod();
		int count = countItf.getCount();
		assertEquals(2,count);

		itf.runUnAnnotatedMethod();
		count = countItf.getCount();
		assertEquals(4,count);
	}

	/**
	 * Add the intent handler on an interface of a component.
	 */
	@Test
	public void testOneInterface()
	throws IllegalLifeCycleException, NoSuchInterfaceException {

		Fractal.getLifeCycleController(comp).stopFc();
		ic.addFcIntentHandler(h,"r");
		Fractal.getLifeCycleController(comp).startFc();

		itf.runAnnotatedMethod();
		int count = countItf.getCount();
		assertEquals(2,count);

		itf.runUnAnnotatedMethod();
		count = countItf.getCount();
		assertEquals(4,count);
	}

	/**
	 * Add the intent handler on a method of a component interface.
	 */
	@Test
	public void testOneMethod()
	throws
		IllegalLifeCycleException, NoSuchInterfaceException,
		NoSuchMethodException {

		Class<?> cl = IntentBaseClientItf.class;
		Method m = cl.getMethod("runUnAnnotatedMethod");

		Fractal.getLifeCycleController(comp).stopFc();
		ic.addFcIntentHandler(h,"r",m);
		Fractal.getLifeCycleController(comp).startFc();

		itf.runAnnotatedMethod();
		int count = countItf.getCount();
		assertEquals(0,count);

		itf.runUnAnnotatedMethod();
		count = countItf.getCount();
		assertEquals(2,count);
	}

	/**
	 * Add the intent handler on all interfaces (business and control.)
	 */
	@Test
	public void testAllInterfaces()
	throws IllegalLifeCycleException, NoSuchInterfaceException {

		Fractal.getLifeCycleController(comp).stopFc();
		ic.addFcIntentHandler(
			h,
			new InterfaceFilter(){
				public boolean accept( Interface itf ) {
					return true;
				}
			}
		);
		Fractal.getLifeCycleController(comp).startFc();

		/*
		 * 36. That's the count. counter is incremented before and after
		 * invocations on interface methods.
		 */
		int count = countItf.getCount();
		assertEquals(30,count);
	}

	/**
	 * Add the intent handler on the name controller.
	 */
	@Test
	public void testNameController()
	throws IllegalLifeCycleException, NoSuchInterfaceException {

		Fractal.getLifeCycleController(comp).stopFc();
		ic.addFcIntentHandler(
			h,
			new InterfaceFilter(){
				public boolean accept( Interface itf ) {
					String name = itf.getFcItfName();
					return name.equals("name-controller");
				}
			}
		);
		Fractal.getLifeCycleController(comp).startFc();

		Fractal.getNameController(comp).getFcName();
		int count = countItf.getCount();
		assertEquals(2,count);
	}

	/**
	 * Add the intent handler on the setFcName method of the name controller.
	 */
	@Test
	public void testNameControllerSetFcName()
	throws IllegalLifeCycleException, NoSuchInterfaceException {

		Fractal.getLifeCycleController(comp).stopFc();
		ic.addFcIntentHandler(
			h,
			new InterfaceMethodFilter(){
				public boolean accept( Interface itf, Method method ) {
					String iname = itf.getFcItfName();
					if( iname.equals("name-controller") ) {
						String mname = method.getName();
						return mname.equals("setFcName");
					}
					return false;
				}
			}
		);
		Fractal.getLifeCycleController(comp).startFc();

		Fractal.getNameController(comp).setFcName("hello");
		Fractal.getNameController(comp).getFcName();
		int count = countItf.getCount();
		assertEquals(2,count);
	}

	@Test
	public void testIntentJoinPointSingletonInterfaceName()
	throws IllegalLifeCycleException, NoSuchInterfaceException {

		Fractal.getLifeCycleController(comp).stopFc();
		ic.addFcIntentHandler(h);
		Fractal.getLifeCycleController(comp).startFc();

		itf.runAnnotatedMethod();
		IntentJoinPoint ijp = countItf.getLastIntentJoinPoint();
		Interface itf = ijp.getInterface();
		InterfaceType it = (InterfaceType) itf.getFcItfType();
		String itName = it.getFcItfName();
		assertEquals("r",itName);
	}

	/**
	 * @since 1.4
	 */
	@Test
	public void testIntentJoinPointCollectionInterfaceName()
	throws IllegalLifeCycleException, NoSuchInterfaceException {

		Fractal.getLifeCycleController(comp).stopFc();
		ic.addFcIntentHandler(h);
		Fractal.getLifeCycleController(comp).startFc();

		itf.run();
		IntentJoinPoint ijp = countItf.getLastIntentJoinPoint();
		Interface itf = ijp.getInterface();
		String itfName = itf.getFcItfName();
		assertEquals("clients0",itfName);
	}

	@Test
	public void testIntentJoinPointMethodName()
	throws IllegalLifeCycleException, NoSuchInterfaceException {

		Fractal.getLifeCycleController(comp).stopFc();
		ic.addFcIntentHandler(h);
		Fractal.getLifeCycleController(comp).startFc();

		itf.runAnnotatedMethod();
		IntentJoinPoint ijp = countItf.getLastIntentJoinPoint();
		Method method = ijp.getMethod();
		String methodName = method.getName();
		assertEquals("runAnnotatedMethod",methodName);
	}

	/**
	 * Add the intent handler on the clients1 client interface.
	 *
	 * @since 1.4
	 */
	@Test
	public void testMultipleReferences()
	throws IllegalLifeCycleException, NoSuchInterfaceException {

		Fractal.getLifeCycleController(comp).stopFc();
		ic.addFcIntentHandler(
			h,
			new InterfaceFilter() {
				public boolean accept( Interface itf ) {
					String name = itf.getFcItfName();
					return name.equals("clients1");
				}
			}
		);
		Fractal.getLifeCycleController(root).startFc();

		itf.run();
		int count = countItf.getCount();
		assertEquals(2,count);
	}
}
