/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.util.Fractal;
import org.osoa.sca.ConversationEndedException;
import org.osoa.sca.annotations.Scope;

/**
 * Class for testing the use of the {@link Scope} annotation.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class ScopeTestCase {

	/** The delay between two calls to getContent() to simulate concurrency. */
	final private static long DELAY = 100;

	@Test
	public void testScopeComposite() throws Exception {

		String adl = getClass().getPackage().getName()+".ScopeComposite";
		ScopeItf itf = TinfiDomain.getService(adl,"s");

		TestScopeRequestThread t = new TestScopeRequestThread(itf);
		t.start();
		Object content = itf.getContent(DELAY);
		t.join();
		assertSame(
			content, t.content,
			"Content instance should persist for composite-scoped components");
	}

	/**
	 * @since 1.2.1
	 */
	@Test
	public void testScopeCompositeEager() throws Exception {

		ScopeCompositeImpl.eagerinit = false;
		String adl = getClass().getPackage().getName()+".ScopeComposite";
		Component c = TinfiDomain.getComponent(adl);

		Fractal.getLifeCycleController(c).startFc();

		assertTrue(
			ScopeCompositeImpl.eagerinit,
			"Content instance should have been eagerly initialized");
	}

	@Test
	public void testScopeCompositeEagerInitInitAnnotated() throws Exception {

		ScopeImpl.initcounter = 0;
		String adl = getClass().getPackage().getName()+".ScopeComposite";
		TinfiDomain.getService(adl,"s");

		assertEquals(
			1, ScopeImpl.initcounter,
			"@Init annotated methods should be executed when the component is eagerly instantiated");
	}

	/**
	 * @since 1.1.1
	 */
	@Test
	public void testScopeCompositeInit() throws Exception {

		ScopeImpl.initcounter = 0;
		String adl = getClass().getPackage().getName()+".ScopeComposite";
		ScopeItf itf = TinfiDomain.getService(adl,"s");

		itf.run();
		itf.run();

		assertEquals(
			1, ScopeImpl.initcounter,
			"@Init annotated methods should be executed only once for scoped-annotated components");
	}

	/**
	 * @since 1.1.1
	 */
	@Test
	public void testScopeCompositeDestroy() throws Exception {

		ScopeImpl.destroycounter = 0;
		String adl = getClass().getPackage().getName()+".ScopeComposite";
		ScopeItf itf = TinfiDomain.getService(adl,"s");

		itf.run();
		itf.run();

		assertEquals(
			0, ScopeImpl.destroycounter,
			"@Destroy annotated methods should not be executed for scoped-annotated components");
	}

	/**
	 * @since 1.3
	 */
	@Test
	public void testScopeCompositeStartStop() throws Exception {

		ScopeImpl.startcounter = 0;
		ScopeImpl.stopcounter = 0;
		String adl = getClass().getPackage().getName()+".ScopeComposite";

		// getComponent(String) starts the component
		Component c = TinfiDomain.getComponent(adl);
		assertEquals(1,ScopeImpl.startcounter);
		assertEquals(0,ScopeImpl.stopcounter);

		Fractal.getLifeCycleController(c).stopFc();
		assertEquals(1,ScopeImpl.startcounter);
		assertEquals(1,ScopeImpl.stopcounter);

		Fractal.getLifeCycleController(c).startFc();
		assertEquals(2,ScopeImpl.startcounter);
		assertEquals(1,ScopeImpl.stopcounter);

		Fractal.getLifeCycleController(c).stopFc();
		assertEquals(2,ScopeImpl.startcounter);
		assertEquals(2,ScopeImpl.stopcounter);
	}

	@Test
	public void testScopeRequest() throws Exception {

		String adl = getClass().getPackage().getName()+".ScopeRequest";
		String service = "s";
		ScopeItf itf = TinfiDomain.getService(adl,service);

		TestScopeRequestThread t = new TestScopeRequestThread(itf);
		t.start();
		Object content = itf.getContent(DELAY);
		t.join();
		assertNotSame(
			content, t.content,
			"Different content instances should be created for request-scoped components");
	}

	class TestScopeRequestThread extends Thread {
		private Object content;
		private ScopeItf itf;
		public TestScopeRequestThread( ScopeItf itf ) {
			this.itf = itf;
		}
		@Override
		public void run() {
			content = itf.getContent(DELAY);
		}
	}

	@Test
	public void testScopeRequestReentrant() throws Exception {

		String adl = getClass().getPackage().getName()+".ScopeRequest";
		String service = "s";
		ScopeItf itf = TinfiDomain.getService(adl,service);

		ScopeImpl.initcounter = 0;
		ScopeImpl.destroycounter = 0;

		Object[] contents = itf.loop();

		assertEquals(
			2, contents.length,
			"loop() should return an array of length 2");
		assertSame(
			contents[0], contents[1],
			"Reentrant calls on a request-scoped component should be handled by the same instance");
		assertEquals(
			1, ScopeImpl.initcounter,
			"@Init annotated method should have been called only once");
		assertEquals(
			1, ScopeImpl.destroycounter,
			"@Destroy annotated method should have been called only once");
	}

	@Test
	public void testScopeConversation() throws Exception {

		String adl = getClass().getPackage().getName()+".ScopeConversation";
		String service = "r";
		ScopeConvItf itf = TinfiDomain.getService(adl,service);

		Object content1 = itf.first(0);
		Object content2 = itf.second();
		assertEquals(
			content1, content2,
			"The same content instance should be used for conversation-scoped component");
		itf.third();
		try {
			itf.first(0);
			fail("Conversation should have ended");
		}
		catch( ConversationEndedException cee ) {}
	}

	@Test
	public void testScopeConversationConcurrent() throws Exception {

		String adl = getClass().getPackage().getName()+".ScopeConversation";
		String service = "r";
		ScopeConvItf itf = TinfiDomain.getService(adl,service);

		TestScopeConversationThread t = new TestScopeConversationThread(itf);
		t.start();
		Object content = itf.first(DELAY);
		t.join();
		assertNotSame(
			content, t.content,
			"Different content instances should be created for conversation-scoped components");
	}

	class TestScopeConversationThread extends Thread {
		private Object content;
		private ScopeConvItf itf;
		public TestScopeConversationThread( ScopeConvItf itf ) {
			this.itf = itf;
		}
		@Override
		public void run() {
			content = itf.first(DELAY);
		}
	}

	@Test
	public void testScopeConversationMaxIdleTime() throws Exception {

		String adl = getClass().getPackage().getName()+".ScopeConversation";
		String service = "r";
		ScopeConvItf itf = TinfiDomain.getService(adl,service);

		itf.first(0);
		Thread.sleep(1200);
		try {
			itf.second();
			fail("Conversation should have expired");
		}
		catch( ConversationEndedException cee ) {}
	}

	@Test
	public void testScopeConversationMaxAge() throws Exception {

		String adl = getClass().getPackage().getName()+".ScopeConversation";
		String service = "r";
		ScopeConvItf itf = TinfiDomain.getService(adl,service);

		itf.first(0);
		Thread.sleep(700);  // To prevent expiration by maxIdleTime (1s)
		itf.first(0);
		Thread.sleep(700);  // To prevent expiration by maxIdleTime (1s)
		itf.first(0);
		Thread.sleep(700);  // To prevent expiration by maxIdleTime (1s)
		try {
			itf.second();
			fail("Conversation should have expired");
		}
		catch( ConversationEndedException cee ) {}
	}

	/**
	 * @since 1.4
	 */
	@Test
	public void testScopeConversationCallback()
	throws
		ClassNotFoundException, InstantiationException, IllegalAccessException,
		IllegalLifeCycleException, NoSuchInterfaceException,
		java.lang.InstantiationException {

		String adl = getClass().getPackage().getName()+".ScopeConversation";
		String service = "r";
		ScopeConvItf itf = TinfiDomain.getService(adl,service);

		itf.first(0);
		itf.callback();
		itf.callbackEndsConversation();
		try {
			itf.first(0);
			fail("Conversation should have ended");
		}
		catch( ConversationEndedException cee ) {}
	}
}
