/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.vaudaux.callback;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.ow2.frascati.tinfi.TinfiDomain;
import org.ow2.frascati.tinfi.vaudaux.callback.clients.ClientConsumer;

/**
 * Class for testing the use of assemblies which mix scaPrimitive and
 * primitive components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 0.4.3
 */
public class CallbackTestCase {

	final private String pkg = getClass().getPackage().getName();
	final private String adl = pkg+".Forge";

	@Test
	public void testCallback()
	throws
		ClassNotFoundException, InstantiationException, IllegalAccessException,
		IllegalLifeCycleException, NoSuchInterfaceException,
		java.lang.InstantiationException {

		Component c = TinfiDomain.getComponent(adl);
		Runnable consumer = TinfiDomain.getService(c,"callbackConsumer");
		Runnable producer = TinfiDomain.getService(c,"callbackProducer");
		consumer.run();
		producer.run();

		assertEquals(
			true, ClientConsumer.onMessageCalled,
			"Callback method ClientConsumer.onMessage should have been called");
	}
}
