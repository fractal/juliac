/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import org.oasisopen.sca.annotation.Scope;
import org.ow2.frascati.tinfi.api.control.SCAContentController;

/**
 * Component implementation class used for testing the features provided by the
 * {@link SCAContentController} interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.0
 */
@Scope("COMPOSITE")
public class SCAContentControllerImpl1 implements SCAContentControllerItf {

	public Class<?> getContentClass() {
		Class<?> cl = getClass();
		return cl;
	}

	/**
	 * @since 1.4.3
	 */
	public void setProp( String value ) {
		prop = value;
	}

	/**
	 * @since 1.4.3
	 */
	public String getProp() {
		return prop;
	}

	/**
	 * @since 1.4.3
	 */
	private String prop = DEFAULT;
	public static final String DEFAULT = "default";
}
