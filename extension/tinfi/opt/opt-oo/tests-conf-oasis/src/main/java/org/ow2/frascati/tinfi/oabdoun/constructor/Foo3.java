package org.ow2.frascati.tinfi.oabdoun.constructor;

import org.oasisopen.sca.annotation.Constructor;
import org.oasisopen.sca.annotation.Property;
import org.oasisopen.sca.annotation.Service;

/**
 * @author Olivier Abdoun <Olivier.Abdoun@inria.fr>
 */
@Service(Bar.class)
public class Foo3 implements Bar, Foo3Attributes {

	protected String bar;

	@Constructor
	public Foo3() {
		this.bar = "bar";
	}

	public String getBar() {
		return this.bar;
	}

	@Property(name="bar")
	public void setBar(String b) {
		this.bar = b;
	}

}
