/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.oasisopen.sca.annotation.Scope;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.util.Fractal;

/**
 * Class for testing the use of the {@link Scope} annotation.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class ScopeTestCase {

	/** The delay between two calls to getContent() to simulate concurrency. */
	final private static long DELAY = 100;

	@Test
	public void testScopeComposite() throws Exception {

		String adl = getClass().getPackage().getName()+".ScopeComposite";
		ScopeItf itf = TinfiDomain.getService(adl,"s");

		TestScopeCompositeThread t = new TestScopeCompositeThread(itf);
		t.start();
		Object content = itf.getContent(DELAY);
		t.join();
		assertSame(
			content, t.content,
			"Content instance should persist for composite-scoped components");
	}

	/**
	 * @since 1.2.1
	 */
	@Test
	public void testScopeCompositeEager() throws Exception {

		ScopeCompositeImpl.eagerinit = false;
		String adl = getClass().getPackage().getName()+".ScopeComposite";
		Component c = TinfiDomain.getComponent(adl);

		Fractal.getLifeCycleController(c).startFc();

		assertTrue(
			ScopeCompositeImpl.eagerinit,
			"Content instance should have been eagerly initialized");
	}

	@Test
	public void testScopeCompositeEagerInitInitAnnotated() throws Exception {

		ScopeImpl.initcounter = 0;
		String adl = getClass().getPackage().getName()+".ScopeComposite";
		TinfiDomain.getService(adl,"s");

		assertEquals(
			1, ScopeImpl.initcounter,
			"@Init annotated methods should be executed when the component is eagerly instantiated");
	}

	/**
	 * @since 1.1.1
	 */
	@Test
	public void testScopeCompositeInit() throws Exception {

		ScopeImpl.initcounter = 0;
		String adl = getClass().getPackage().getName()+".ScopeComposite";
		ScopeItf itf = TinfiDomain.getService(adl,"s");

		itf.run();
		itf.run();

		assertEquals(
			1, ScopeImpl.initcounter,
			"@Init annotated methods should be executed only once for scoped-annotated components");
	}

	/**
	 * @since 1.1.1
	 */
	@Test
	public void testScopeCompositeDestroy() throws Exception {

		ScopeImpl.destroycounter = 0;
		String adl = getClass().getPackage().getName()+".ScopeComposite";
		ScopeItf itf = TinfiDomain.getService(adl,"s");

		itf.run();
		itf.run();

		assertEquals(
			0, ScopeImpl.destroycounter,
			"@Destroy annotated methods should not be executed for scoped-annotated components");
	}

	/**
	 * @since 1.3
	 */
	@Test
	public void testScopeCompositeStartStop() throws Exception {

		ScopeImpl.startcounter = 0;
		ScopeImpl.stopcounter = 0;
		String adl = getClass().getPackage().getName()+".ScopeComposite";

		// getComponent(String) starts the component
		Component c = TinfiDomain.getComponent(adl);
		assertEquals(1,ScopeImpl.startcounter);
		assertEquals(0,ScopeImpl.stopcounter);

		Fractal.getLifeCycleController(c).stopFc();
		assertEquals(1,ScopeImpl.startcounter);
		assertEquals(1,ScopeImpl.stopcounter);

		Fractal.getLifeCycleController(c).startFc();
		assertEquals(2,ScopeImpl.startcounter);
		assertEquals(1,ScopeImpl.stopcounter);

		Fractal.getLifeCycleController(c).stopFc();
		assertEquals(2,ScopeImpl.startcounter);
		assertEquals(2,ScopeImpl.stopcounter);
	}

	private static class TestScopeCompositeThread extends Thread {
		private Object content;
		private ScopeItf itf;
		public TestScopeCompositeThread( ScopeItf itf ) {
			this.itf = itf;
		}
		@Override
		public void run() {
			content = itf.getContent(DELAY);
		}
	}
}
