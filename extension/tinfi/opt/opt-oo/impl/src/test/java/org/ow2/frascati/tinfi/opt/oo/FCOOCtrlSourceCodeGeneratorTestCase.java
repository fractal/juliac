/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2012-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.oo;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.JuliacModuleItf;
import org.objectweb.fractal.juliac.core.Juliac;

/**
 * Class for testing the functionalities of the {@link
 * FCOOCtrlSourceCodeGeneratorTestCase} class.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.5
 */
public class FCOOCtrlSourceCodeGeneratorTestCase {

	/**
	 * Test whether the OO and Tinfi OO mode can cohabit. This is related to the
	 * cohabitation of ModuleLoader and JulietLoader.
	 */
	@Test
	public void testTwoGenerators() throws IOException {

		JuliacItf jc = new Juliac();

		JuliacModuleItf oo =
			new org.objectweb.fractal.juliac.opt.oo.FCOOCtrlSourceCodeGenerator();
		JuliacModuleItf tinfioo =
			new org.ow2.frascati.tinfi.opt.oo.FCOOCtrlSourceCodeGenerator();

		oo.init(jc);
		tinfioo.init(jc);

		tinfioo.close(jc);
		oo.close(jc);
		jc.close();
	}
}
