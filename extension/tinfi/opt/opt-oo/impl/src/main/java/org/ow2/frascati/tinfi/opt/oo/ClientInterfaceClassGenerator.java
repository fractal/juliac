/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Philippe Merle
 */

package org.ow2.frascati.tinfi.opt.oo;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.oasisopen.sca.ServiceReference;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.commons.lang.ClassHelper;
import org.ow2.frascati.tinfi.TinfiComponentOutInterface;

/**
 * This class generates the source code of component client (output) interface
 * implementations.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Philippe Merle <Philippe.Merle@inria.fr>
 */
public class ClientInterfaceClassGenerator
extends ServerInterfaceClassGenerator {

	public ClientInterfaceClassGenerator(
		InterfaceType it, Class<?> cl, String pkgRoot, boolean mergeable ) {

		super(it,cl,pkgRoot,mergeable);
	}

	/**
	 * Return the name of the suffix which is appended to generated class names.
	 */
	@Override
	public String getClassNameSuffix() {
		return "FcOutItf";
	}

	@Override
	public String getSuperClassName() {

		/*
		 * The generated client interface extends the class implementing the
		 * server interface of the same type.
		 */
		SourceCodeGeneratorItf cg =
			new ServerInterfaceClassGenerator(it,proxycl,pkgRoot,mergeable);
		String name = cg.getTargetTypeName();

		/*
		 * Add type parameters if any.
		 */
		String[] tpnames = ClassHelper.getTypeParameterNames(proxycl);
		if( tpnames.length != 0 ) {
			String s = ClassHelper.getTypeParameterNamesSignature(tpnames);
			name += s;
		}

		return name;
	}

	@Override
	public String[] getImplementedInterfaceNames() {

		String[] old = super.getImplementedInterfaceNames();
		String[] result = new String[old.length+1];
		System.arraycopy(old,0,result,0,old.length);

		StringBuilder sb = new StringBuilder();
		sb.append(TinfiComponentOutInterface.class.getName());
		sb.append('<');
		sb.append(it.getFcItfSignature());
		sb.append('>');
		result[old.length] = sb.toString();

		return result;
	}

	@Override
	public void generateMethods( ClassSourceCodeVisitor cv ) {

		super.generateMethods(cv);

		BlockSourceCodeVisitor mv =
			cv.visitMethod(
				Modifier.PUBLIC, null,
				ServiceReference.class.getName()+'<'+it.getFcItfSignature()+'>',
				"getServiceReference", null, null );
		SourceCodeGeneratorItf cg =
			new ServiceReferenceClassGenerator(
				it,this.proxycl,pkgRoot,membraneDesc,mergeable);
		String srclname = cg.getTargetTypeName();
		mv.visit  ("    return new ");
		mv.visit  (srclname);
		mv.visit  ("(");
		mv.visit  (it.getFcItfSignature());
		mv.visitln(".class,this);");
		mv.visitEnd();
	}

	@Override
	public void generateProxyMethod(
		ClassSourceCodeVisitor cv, Method proxym ) {
		/*
		 * Indeed nothing.
		 * Proxy methods are inherited from the component server interface
		 * class.
		 */
	}
}
