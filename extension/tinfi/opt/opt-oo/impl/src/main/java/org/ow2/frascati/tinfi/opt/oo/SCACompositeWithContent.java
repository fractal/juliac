/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.oo;

import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.objectweb.fractal.julia.BasicControllerMixin;
import org.objectweb.fractal.julia.UseComponentMixin;
import org.objectweb.fractal.julia.control.content.BasicContentControllerMixin;
import org.objectweb.fractal.julia.control.content.BindingContentMixin;
import org.objectweb.fractal.julia.control.content.CheckContentMixin;
import org.objectweb.fractal.julia.control.content.LifeCycleContentMixin;
import org.objectweb.fractal.julia.control.content.SuperContentMixin;
import org.objectweb.fractal.julia.control.content.TypeContentMixin;
import org.objectweb.fractal.julia.control.lifecycle.UseLifeCycleControllerMixin;

/**
 * Definition of the SCA composite with content membrane.
 *
 * This is a membrane for SCA composites with content instances in addition to
 * subcomponents. This membrane has been introduced for EasyViper which uses
 * composites with content instances.
 *
 * This membrane provides the same control interfaces as scaPrimitive plus the
 * content-controller interface which is present in composite membranes for
 * managing subcomponents.
 *
 * This membrane was introduced in Tinfi 1.3.1 and defined with Tinfilet in
 * version 1.4.3.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.3
 */
@Membrane(desc=SCACompositeWithContent.NAME)  // generator and interceptors are inherited
public class SCACompositeWithContent extends SCAPrimitive {

	public static final String NAME = "scaCompositeWithContent";

	@Controller(
		name="content-controller",
		impl="ContentControllerImpl",
		mixins={
			BasicControllerMixin.class,
			UseComponentMixin.class,
			BasicContentControllerMixin.class,
			CheckContentMixin.class,
			TypeContentMixin.class,
			BindingContentMixin.class,
			UseLifeCycleControllerMixin.class,
			LifeCycleContentMixin.class,
			SuperContentMixin.class})
	protected ContentController cc;
}
