/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2010-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.osgi;

import java.io.IOException;

import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.generator.ProxyClassGeneratorItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.osgi.FCOOCtrlGenerator;

/**
 * Source code generator for <code>scaOsgiPrimitive</code> components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.3
 */
public class FCOOCtrlSourceCodeGenerator
extends org.ow2.frascati.tinfi.opt.oo.FCOOCtrlSourceCodeGenerator {

	// -----------------------------------------------------------------------
	// Implementation of the JuliacModuleItf interface
	// -----------------------------------------------------------------------

	private FCOOCtrlGenerator osgig;

	@Override
	public void init( JuliacItf jc ) throws IOException {
		super.init(jc);
		osgig = new FCOOCtrlGenerator(jc);
	}

	@Override
	protected void postInit() throws IOException {
		super.postInit();
		mloader.put(SCAOSGiPrimitive.NAME,SCAOSGiPrimitive.class);
	}


	// -----------------------------------------------------------------------
	// Implementation of the FCSourceCodeGeneratorItf interface
	// -----------------------------------------------------------------------

	@Override
	public SourceCodeGeneratorItf generate(
		Type type, Object controllerDesc, Object contentDesc, Object source )
	throws IOException {

		/*
		 * Check parameter consistence.
		 */
		osgig.check(type,controllerDesc,contentDesc);

		SourceCodeGeneratorItf scg =
			super.generate(type,controllerDesc,contentDesc,source);

		return scg;
	}

	@Override
	public ProxyClassGeneratorItf getInterfaceClassGenerator(InterfaceType it) {

		ProxyClassGeneratorItf pcg =
			osgig.getInterfaceClassGenerator(it);
		return pcg;
	}


	// ------------------------------------------------------------------
	// Implementation of methods defined in FCSourceCodeGenerator
	// ------------------------------------------------------------------

	@Override
	protected void generateInterfaceImpl( InterfaceType[] its, String ctrlDesc )
	throws IOException {

		for (InterfaceType it : its) {

			generateInterfaceImpl(it,ctrlDesc);

			/*
			 * Super method generate internal interfaces. We don't need them for
			 * scaOsgiPrimitive.
			 */
		}
	}

	/**
	 * Generate the source code of component interfaces associated with the
	 * specified interface type.
	 */
	@Override
	protected void generateInterfaceImpl( InterfaceType it, String ctrlDesc )
	throws IOException {
		super.generateInterfaceImpl(it,ctrlDesc);
		osgig.generateInterfaceImpl(it);
	}
}
