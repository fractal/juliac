/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.osgi;

import org.oasisopen.sca.ComponentContext;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.objectweb.fractal.julia.BasicComponentMixin;
import org.objectweb.fractal.julia.BasicControllerMixin;
import org.objectweb.fractal.julia.TypeComponentMixin;
import org.objectweb.fractal.julia.UseComponentMixin;
import org.objectweb.fractal.julia.control.binding.BasicBindingControllerMixin;
import org.objectweb.fractal.julia.control.binding.CheckBindingMixin;
import org.objectweb.fractal.julia.control.binding.ContentBindingMixin;
import org.objectweb.fractal.julia.control.binding.LifeCycleBindingMixin;
import org.objectweb.fractal.julia.control.binding.TypeBasicBindingMixin;
import org.objectweb.fractal.julia.control.binding.TypeBindingMixin;
import org.objectweb.fractal.julia.control.content.BasicSuperControllerMixin;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;
import org.objectweb.fractal.julia.control.content.UseSuperControllerMixin;
import org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleControllerMixin;
import org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleCoordinatorMixin;
import org.objectweb.fractal.julia.control.lifecycle.ContainerLifeCycleMixin;
import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;
import org.objectweb.fractal.julia.control.lifecycle.TypeLifeCycleMixin;
import org.objectweb.fractal.julia.control.lifecycle.UseLifeCycleControllerMixin;
import org.objectweb.fractal.julia.control.name.BasicNameControllerMixin;
import org.objectweb.fractal.julia.control.name.UseNameControllerMixin;
import org.objectweb.fractal.juliac.core.proxy.LifeCycleSourceCodeGenerator;
import org.objectweb.fractal.juliac.osgi.control.lifecycle.OSGiLifeCycleControllerMixin;
import org.ow2.frascati.tinfi.api.control.SCAIntentController;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;
import org.ow2.frascati.tinfi.control.binding.CollectionInterfaceBindingControllerMixin;
import org.ow2.frascati.tinfi.control.binding.InterfaceBindingControllerMixin;
import org.ow2.frascati.tinfi.control.component.ComponentContextMixin;
import org.ow2.frascati.tinfi.control.content.UseSCAContentControllerMixin;
import org.ow2.frascati.tinfi.control.intent.LifeCycleIntentMixin;
import org.ow2.frascati.tinfi.control.intent.SCABasicIntentControllerMixin;
import org.ow2.frascati.tinfi.control.intent.SCAIntentControllerMixin;
import org.ow2.frascati.tinfi.control.property.SCAPrimitivePropertyControllerMixin;
import org.ow2.frascati.tinfi.control.property.SCAPropertyControllerMixin;
import org.ow2.frascati.tinfi.control.property.SCAPropertyPromoterMixin;
import org.ow2.frascati.tinfi.control.property.UseSCAPropertyControllerMixin;
import org.ow2.frascati.tinfi.opt.oo.SCAContentInterceptorSourceCodeGenerator;
import org.ow2.frascati.tinfi.opt.oo.SCATinfiInterceptorSourceCodeGenerator;
import org.ow2.frascati.tinfi.opt.oo.light.InterceptorNoIntentClassGenerator;

/**
 * Definition of the SCA OSGi primitive membrane.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.3
 */
@Membrane(
	desc=SCAOSGiPrimitive.NAME,
	generator=InterceptorNoIntentClassGenerator.class,
	interceptors={
		SCATinfiInterceptorSourceCodeGenerator.class,
		LifeCycleSourceCodeGenerator.class,
		SCAContentInterceptorSourceCodeGenerator.class})
public class SCAOSGiPrimitive {

	public static final String NAME = "scaOsgiPrimitive";

	// ---------------------------------------------------------
	// Julia controllers
	// ---------------------------------------------------------

	@Controller(
		name="component",
		impl="ComponentImpl",
		mixins={
			BasicControllerMixin.class,
			BasicComponentMixin.class,
			TypeComponentMixin.class})
	protected Component comp;

	@Controller(
		name="super-controller",
		impl="SuperControllerImpl",
		mixins={
			BasicControllerMixin.class,
			BasicSuperControllerMixin.class})
	protected SuperControllerNotifier sc;

	@Controller(
		name="name-controller",
		impl="NameControllerImpl",
		mixins={
			BasicControllerMixin.class,
			BasicNameControllerMixin.class})
	protected NameController nc;

	// ---------------------------------------------------------
	// Redefined Julia controllers
	// ---------------------------------------------------------

	@Controller(
		name="binding-controller",
		impl="SCABindingControllerImpl",
		mixins={
			BasicControllerMixin.class,
			BasicBindingControllerMixin.class,
			TypeBasicBindingMixin.class,
			CollectionInterfaceBindingControllerMixin.class,
			InterfaceBindingControllerMixin.class,
			UseSCAContentControllerMixin.class,
			UseComponentMixin.class,
			CheckBindingMixin.class,
			TypeBindingMixin.class,
			UseSuperControllerMixin.class,
			ContentBindingMixin.class,
			UseLifeCycleControllerMixin.class,
			LifeCycleBindingMixin.class})
	protected BindingController bc;

	@Controller(
		name="lifecycle-controller",
		impl="OSGiLifeCycleControllerImpl",
		mixins={
			BasicControllerMixin.class,
			UseComponentMixin.class,
			BasicLifeCycleCoordinatorMixin.class,
			BasicLifeCycleControllerMixin.class,
			TypeLifeCycleMixin.class,
			ContainerLifeCycleMixin.class,
			OSGiLifeCycleControllerMixin.class})
	protected LifeCycleCoordinator lc;

	// ---------------------------------------------------------
	// Tinfi controllers
	// ---------------------------------------------------------

	@Controller(
		name="sca-component-controller",
		impl="SCAComponentControllerImpl",
		mixins={
			BasicControllerMixin.class,
			UseComponentMixin.class,
			UseSCAContentControllerMixin.class,
			UseSCAPropertyControllerMixin.class,
			ComponentContextMixin.class})
//        mixins=ComponentContextImpl.class)
	protected ComponentContext compctx;

	@Controller(
		name="sca-property-controller",
		impl="SCAPrimitivePropertyControllerImpl",
		mixins={
			BasicControllerMixin.class,
			UseSCAContentControllerMixin.class,
			UseNameControllerMixin.class,
			SCAPrimitivePropertyControllerMixin.class,
			SCAPropertyControllerMixin.class,
			SCAPropertyPromoterMixin.class})
//        mixins=SCAPrimitivePropertyControllerImpl.class)
	protected SCAPropertyController scapc;

	@Controller(
		name="sca-intent-controller",
		impl="SCAPrimitiveIntentControllerImpl",
		mixins={
			BasicControllerMixin.class,
			SCAIntentControllerMixin.class,
			UseComponentMixin.class,
			SCABasicIntentControllerMixin.class,
			UseNameControllerMixin.class,
			UseLifeCycleControllerMixin.class,
			LifeCycleIntentMixin.class})
	protected SCAIntentController scaic;
}
