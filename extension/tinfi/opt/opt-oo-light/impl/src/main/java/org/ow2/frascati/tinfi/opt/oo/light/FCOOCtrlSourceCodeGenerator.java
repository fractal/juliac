/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.oo.light;

import java.io.IOException;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.desc.SimpleMembraneDesc;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;
import org.ow2.frascati.tinfi.control.content.IllegalContentClassMetaData;

/**
 * This class generates the source code associated to Fractal components.
 * The membrane implementation and the initializer implementation are generated.
 *
 * The content, the interceptors and the controllers are kept in separate
 * classes and the controllers are implemented with objects.
 *
 * This source code generator uses versions of the Tinfi control membranes that
 * do not provide intent controllers and interceptors.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.3
 */
public class FCOOCtrlSourceCodeGenerator
extends org.ow2.frascati.tinfi.opt.oo.FCOOCtrlSourceCodeGenerator {

	@Override
	protected void postInit() throws IOException {
		super.postInit();
		mloader.put(SCALightPrimitive.NAME,SCALightPrimitive.class);
		mloader.put(SCALightComposite.NAME,SCALightComposite.class);
	}

	@Override
	public SimpleMembraneDesc generateMembraneImpl(
		ComponentType ct, String ctrldesc, String contentClassName,
		Object source )
	throws IOException {

		/*
		 * Generate the implementation of the SCA Content Controller for the
		 * specified content class.
		 */
		generateSCAContentControllerImpl(contentClassName);

		return super.generateMembraneImpl(ct,ctrldesc,contentClassName,source);
	}

	@Override
	protected InitializerOOCtrlClassGenerator getInitializerClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		return new InitializerOOCtrlClassGenerator(
			jc,fcscg,membraneDesc,ct,contentDesc,source);
	}


	// -----------------------------------------------------------------------
	// Implementation specific
	// -----------------------------------------------------------------------

	private void generateSCAContentControllerImpl( String contentClassName )
	throws IOException {

		if( contentClassName == null ) {
			return;
		}

		try {
			SourceCodeGeneratorItf cg =
				SCAContentControllerClassGenerator.get(jc,contentClassName);
			jc.generateSourceCode(cg);
		}
		catch( IllegalContentClassMetaData iccmd ) {
			throw new IOException(iccmd);
		}
	}
}
