/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.oo.light;

import org.oasisopen.sca.ComponentContext;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.objectweb.fractal.julia.BasicComponentMixin;
import org.objectweb.fractal.julia.BasicControllerMixin;
import org.objectweb.fractal.julia.TypeComponentMixin;
import org.objectweb.fractal.julia.UseComponentMixin;
import org.objectweb.fractal.julia.control.binding.BasicBindingControllerMixin;
import org.objectweb.fractal.julia.control.binding.CheckBindingMixin;
import org.objectweb.fractal.julia.control.binding.ContentBindingMixin;
import org.objectweb.fractal.julia.control.binding.LifeCycleBindingMixin;
import org.objectweb.fractal.julia.control.binding.OptimizedCompositeBindingMixin;
import org.objectweb.fractal.julia.control.binding.TypeBasicBindingMixin;
import org.objectweb.fractal.julia.control.binding.TypeBindingMixin;
import org.objectweb.fractal.julia.control.content.BasicContentControllerMixin;
import org.objectweb.fractal.julia.control.content.BasicSuperControllerMixin;
import org.objectweb.fractal.julia.control.content.BindingContentMixin;
import org.objectweb.fractal.julia.control.content.CheckContentMixin;
import org.objectweb.fractal.julia.control.content.LifeCycleContentMixin;
import org.objectweb.fractal.julia.control.content.SuperContentMixin;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;
import org.objectweb.fractal.julia.control.content.TypeContentMixin;
import org.objectweb.fractal.julia.control.content.UseContentControllerMixin;
import org.objectweb.fractal.julia.control.content.UseSuperControllerMixin;
import org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleCoordinatorMixin;
import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;
import org.objectweb.fractal.julia.control.lifecycle.OptimizedLifeCycleControllerMixin;
import org.objectweb.fractal.julia.control.lifecycle.TypeLifeCycleMixin;
import org.objectweb.fractal.julia.control.lifecycle.UseLifeCycleControllerMixin;
import org.objectweb.fractal.julia.control.name.BasicNameControllerMixin;
import org.objectweb.fractal.julia.control.name.UseNameControllerMixin;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;
import org.ow2.frascati.tinfi.control.component.ComponentContextMixin;
import org.ow2.frascati.tinfi.control.content.UseSCAContentControllerMixin;
import org.ow2.frascati.tinfi.control.property.SCACompositePropertyControllerMixin;
import org.ow2.frascati.tinfi.control.property.SCAPropertyControllerMixin;
import org.ow2.frascati.tinfi.control.property.SCAPropertyPromoterMixin;
import org.ow2.frascati.tinfi.control.property.UseSCAPropertyControllerMixin;
import org.ow2.frascati.tinfi.opt.oo.SCATinfiInterceptorSourceCodeGenerator;

/**
 * Definition of the SCA composite membrane with no intent controller.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.3
 */
@Membrane(
	desc=SCALightComposite.NAME,
	generator=InterceptorNoIntentClassGenerator.class,
	interceptors=SCATinfiInterceptorSourceCodeGenerator.class)
public class SCALightComposite {

	public static final String NAME = "scaComposite";

	// ---------------------------------------------------------
	// Julia controllers
	// ---------------------------------------------------------

	@Controller(
		name="component",
		impl="ComponentImpl",
		mixins={
			BasicControllerMixin.class,
			BasicComponentMixin.class,
			TypeComponentMixin.class})
	protected Component comp;

	@Controller(
		name="binding-controller",
		impl="CompositeBindingControllerImpl",
		mixins={
			BasicControllerMixin.class,
			BasicBindingControllerMixin.class,
			TypeBasicBindingMixin.class,
			UseComponentMixin.class,
			CheckBindingMixin.class,
			TypeBindingMixin.class,
			UseSuperControllerMixin.class,
			ContentBindingMixin.class,
			UseLifeCycleControllerMixin.class,
			LifeCycleBindingMixin.class,
			UseContentControllerMixin.class,
			OptimizedCompositeBindingMixin.class})
	protected BindingController bc;

	@Controller(
		name="content-controller",
		impl="ContentControllerImpl",
		mixins={
			BasicControllerMixin.class,
			UseComponentMixin.class,
			BasicContentControllerMixin.class,
			CheckContentMixin.class,
			TypeContentMixin.class,
			BindingContentMixin.class,
			UseLifeCycleControllerMixin.class,
			LifeCycleContentMixin.class,
			SuperContentMixin.class})
	protected ContentController cc;

	@Controller(
		name="super-controller",
		impl="SuperControllerImpl",
		mixins={
			BasicControllerMixin.class,
			BasicSuperControllerMixin.class})
	protected SuperControllerNotifier sc;

	@Controller(
		name="lifecycle-controller",
		impl="CompositeLifeCycleControllerImpl",
		mixins={
			BasicControllerMixin.class,
			UseComponentMixin.class,
			BasicLifeCycleCoordinatorMixin.class,
			OptimizedLifeCycleControllerMixin.class,
			TypeLifeCycleMixin.class})
	protected LifeCycleCoordinator lc;

	@Controller(
		name="name-controller",
		impl="NameControllerImpl",
		mixins={
			BasicControllerMixin.class,
			BasicNameControllerMixin.class})
	protected NameController nc;

	// ---------------------------------------------------------
	// Tinfi controllers
	// ---------------------------------------------------------

	@Controller(
		name="sca-component-controller",
		impl="SCAComponentControllerImpl",
		mixins={
			BasicControllerMixin.class,
			UseComponentMixin.class,
			UseSCAContentControllerMixin.class,
			UseSCAPropertyControllerMixin.class,
			ComponentContextMixin.class})
	protected ComponentContext compctx;

	@Controller(
		name="sca-property-controller",
		impl="SCACompositePropertyControllerImpl",
		mixins={
			BasicControllerMixin.class,
			UseNameControllerMixin.class,
			SCACompositePropertyControllerMixin.class,
			SCAPropertyControllerMixin.class,
			SCAPropertyPromoterMixin.class})
	protected SCAPropertyController scapc;
}
