/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.oo.light;

import java.lang.reflect.Modifier;
import java.util.Vector;

import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.ow2.frascati.tinfi.control.content.ContentClassMetaData;

/**
 * The class generates implementations of the SCA content controller specific
 * for a given stateless-scoped content class.
 *
 * @author Lionel <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.5
 */
public class SCAContentControllerStatelessScopeClassGenerator
extends SCAContentControllerClassGenerator {

	protected SCAContentControllerStatelessScopeClassGenerator(
		JuliacItf jc, String contentClassName, ContentClassMetaData ccmd) {

		super(jc,contentClassName,ccmd);
	}

	protected void generateFieldContent( ClassSourceCodeVisitor cv ) {
		/*
		 * Use Vector in generated code to remain compatible for future (to be
		 * implemented) J2ME support.
		 */
		String type = Vector.class.getName();
		String expr = "new "+Vector.class.getName()+"()";
		cv.visitField(Modifier.PRIVATE,type,"contents",expr);
	}

	protected void generateMethodGetFcContent( BlockSourceCodeVisitor mv ) {
		mv.visitSet(contentClassName+" content","null");
		generateCreateFcInstance(mv);
		generateInitializeFcInstance(mv);
		mv.visitIns("contents.addElement(content)");
		mv.visitIns("return content");
	}

	protected void generateMethodReleaseFcContent( ClassSourceCodeVisitor cv ) {
		BlockSourceCodeVisitor mv =
			cv.visitMethod(
				Modifier.PUBLIC,null,"void","releaseFcContent",
				new String[]{"Object content","boolean isEndMethod"},null);
		if( ccmd.destroyMethod != null ) {
			mv.visitIns("content.",ccmd.destroyMethod.getName(),"()");
		}
		mv.visitIns("contents.removeElement(content)");
		mv.visitEnd();
	}

	protected void generateMethodGetFcCurrentContents( BlockSourceCodeVisitor mv ) {
		mv.visitSet(contentClassName+"[] contents","new ",contentClassName,"[this.contents.size()]");
		BlockSourceCodeVisitor forv = mv.visitFor("int i=0; i<this.contents.size(); i++");
		forv.visitSet("contents[i]","(",contentClassName,")this.contents.elementAt(i)");
		forv.visitEnd();
		mv.visitIns("return contents");
	}
}
