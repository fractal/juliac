/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.oo.light;

import java.lang.reflect.Method;

import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;

/**
 * This class generates interceptor classes. This generator differs from {@link
 * org.ow2.frascati.tinfi.opt.oo.InterceptorClassGenerator} by generating
 * interceptors which do not manage intents.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.1.1
 */
public class InterceptorNoIntentClassGenerator
extends org.objectweb.fractal.juliac.core.proxy.InterceptorClassGenerator {

	public InterceptorNoIntentClassGenerator() {}

	@Override
	public void generateFieldImpl( ClassSourceCodeVisitor cv ) {
		/*
		 * The implementation of the impl field is inherited from
		 * TinfiComponentInterceptor.
		 */
	}

	@Override
	public void generateMethodGetFcItfDelegate( ClassSourceCodeVisitor cv ) {
		/*
		 * The implementation of the getFcItfDelegate method is inherited from
		 * TinfiComponentInterceptor.
		 */
	}

	@Override
	public void generateMethodSetFcItfDelegate( ClassSourceCodeVisitor cv ) {
		/*
		 * The implementation of the setFcItfDelegate method is inherited from
		 * TinfiComponentInterceptor.
		 */
	}

	@Override
	public String getDelegatingInstance( Method proxym ) {
		return "impl";
	}
}
