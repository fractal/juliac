package org.ow2.frascati.tinfi.oabdoun.constructor;

import org.objectweb.fractal.api.control.AttributeController;

/**
 * @author Olivier Abdoun <Olivier.Abdoun@inria.fr>
 */
public interface Foo3Attributes extends AttributeController {
	public void setBar(String b);
	public String getBar();
}
