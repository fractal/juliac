/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * Class for testing the use of the {@link org.osoa.sca.annotations.Constructor}
 * annotation.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class ConstructorTestCase {

	@Test
	public void testReference() throws Exception {

		String adl = getClass().getPackage().getName()+".Constructor";
		String service = "r";

		/*
		 * Temporary solution to declare the 'name' property.
		 */
		// TODO To be removed when the SCA Assembly Language frontend is ready.
		Component composite = TinfiDomain.getComponent(adl);
		Component client = Fractal.getContentController(composite).getFcSubComponents()[0];
		SCAPropertyController scapc = (SCAPropertyController)
			client.getFcInterface(SCAPropertyController.NAME);
		scapc.setValue("name", "Bob");

		Runnable itf = TinfiDomain.getService(composite,service);

		itf.run();
		assertEquals("Bob", ConstructorClientImpl.name);
		assertEquals(1, ConstructorImpl.count);
	}
}
