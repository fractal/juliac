/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;
import org.ow2.frascati.tinfi.api.control.SCAContentController;

/**
 * Class for testing the SCA Content Controller.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 0.3
 */
public class SCAContentControllerTestCase {

	private Component client;
	private SCAContentControllerItf r;

	@BeforeEach
	public void setUp()
	throws
		ClassNotFoundException, InstantiationException, IllegalAccessException,
		IllegalLifeCycleException, NoSuchInterfaceException,
		java.lang.InstantiationException {

		String adl = getClass().getPackage().getName()+".SCAContentController";
		Component root = TinfiDomain.getComponent(adl);
		Component[] subs =
			Fractal.getContentController(root).getFcSubComponents();
		client = subs[0];
		r = (SCAContentControllerItf) client.getFcInterface("r");
	}

	/**
	 * @since 1.0
	 */
	@Test
	public void testGetContentClass()
	throws IllegalLifeCycleException, NoSuchInterfaceException {

		Fractal.getLifeCycleController(client).startFc();

		Class<?> expected = SCAContentControllerImpl1.class;
		Class<?> c = r.getContentClass();
		assertEquals(expected,c);
	}

	/**
	 * Test that the content class of <code>scaPrimitive</code> components can
	 * not be changed if the component is not stopped.
	 *
	 * @since 1.0
	 */
	@Test
	public void testSetContentClassNotStopped()
	throws
		NoSuchInterfaceException, IllegalLifeCycleException,
		ContentInstantiationException {

		// Start and invoke the component once
		Fractal.getLifeCycleController(client).startFc();
		r.getContentClass();

		// Try to reconfigure the content class
		SCAContentController scacc = (SCAContentController)
			client.getFcInterface(SCAContentController.NAME);
		Class<?> expected = SCAContentControllerImpl2.class;

		try {
			scacc.setFcContentClass(expected);
			final String msg =
				"The content class can not be changed without "+
				"stopping the component. IllegalLifeCycleException should "+
				"have been thrown.";
			fail(msg);
		}
		catch( IllegalLifeCycleException ilce ) {}
	}

	/**
	 * @since 1.4.3
	 */
	@Test
	public void testSetContent()
	throws
		IllegalLifeCycleException, NoSuchInterfaceException,
		ContentInstantiationException {

		// Check that the component is instantiated with the regular content instance
		String value = r.getProp();
		assertEquals(SCAContentControllerImpl1.DEFAULT,value);
		Fractal.getLifeCycleController(client).stopFc();

		// Create a new ad hoc content instance
		SCAContentControllerImpl1 content = new SCAContentControllerImpl1();
		final String expected = "foo";
		content.setProp(expected);

		// Associate the ad hoc content instance with the component
		SCAContentController scacc = (SCAContentController)
			client.getFcInterface(SCAContentController.NAME);
		scacc.setFcContent(content);

		// Check that the association has been correctly performed
		Fractal.getLifeCycleController(client).startFc();
		value = r.getProp();
		assertEquals(expected,value);
	}

	/**
	 * @since 1.4.3
	 */
	@Test
	public void testSetContentNotStopped()
	throws NoSuchInterfaceException, ContentInstantiationException {

		SCAContentControllerImpl1 content = new SCAContentControllerImpl1();
		SCAContentController scacc = (SCAContentController)
			client.getFcInterface(SCAContentController.NAME);

		// Try to invoke setContent while the component is started
		try {
			scacc.setFcContent(content);
			final String msg =
				"setFcContent(Object) cannot be invoked without "+
				"stopping the component. IllegalLifeCycleException should "+
				"have been thrown.";
			fail(msg);
		}
		catch( IllegalLifeCycleException ilce ) {}
	}

	/**
	 * @since 1.4.3
	 */
	@Test
	public void testSetContentNotCompositeScoped()
	throws
		NoSuchInterfaceException, IllegalLifeCycleException,
		ClassNotFoundException, InstantiationException, IllegalAccessException,
		java.lang.InstantiationException {

		String adl = SCAContentControllerImpl2.class.getName()+"Factory";
		Component client = TinfiDomain.getComponent(adl);
		Fractal.getLifeCycleController(client).stopFc();
		SCAContentController scacc = (SCAContentController)
			client.getFcInterface(SCAContentController.NAME);

		SCAContentControllerImpl2 content = new SCAContentControllerImpl2();

		// Try to invoke setContent with a non composite-scoped instance
		try {
			scacc.setFcContent(content);
			final String msg =
				"setFcContent(Object) cannot be invoked with a non "+
				"composite-scoped component. ContentInstantiationException "+
				"should have been thrown.";
			fail(msg);
		}
		catch( ContentInstantiationException cie ) {}
	}
}
