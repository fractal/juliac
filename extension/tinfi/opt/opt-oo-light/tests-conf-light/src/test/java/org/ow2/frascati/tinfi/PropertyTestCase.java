/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * Class for testing component properties.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class PropertyTestCase {

	protected PropertyItf itf;
	protected SCAPropertyController scapc;

	@BeforeEach
	public void setUp() throws Exception {
		String adl = getClass().getPackage().getName()+".Property";
		String service = "r";
		Component comp = TinfiDomain.getComponent(adl);

		ContentController cc = Fractal.getContentController(comp);
		Component[] subs = cc.getFcSubComponents();
		Component inner = subs[0];

		itf = TinfiDomain.getService(inner,service);
		scapc = (SCAPropertyController)
			inner.getFcInterface(SCAPropertyController.NAME);
	}

	@Test
	public void testMandatory() {
		try {
			itf.mandatoryPropValue();
			fail("Mandatory property shouldn't be allowed to be null");
		}
		catch( TinfiRuntimeException tre ) {}
	}

	@Test
	public void testNewValueForMandatoryProp() {
		/*
		 * Not strictly related to this test, but a mandatory property must be
		 * set.
		 */
		scapc.setValue("mandatoryProp","bar");

		String expected = "bar";
		String value = itf.mandatoryPropValue();
		assertEquals(expected,value);
	}

	@Test
	public void testNewValuePropWithDefault() {
		scapc.setValue("mandatoryProp","bar");

		int expected = 12;
		scapc.setValue("propWithDefault",expected);
		int value = itf.propWithDefaultValue();
		assertEquals(expected,value);
	}

	/**
	 * @since 1.4.1
	 */
	@Test
	public void testPropIntegerForInt() {
		scapc.setValue("mandatoryProp","bar");

		int expected = 42;
		scapc.setValue("propWithDefault",Integer.valueOf(expected));
		int value = itf.propWithDefaultValue();
		assertEquals(expected,value);
	}

	/**
	 * @since 1.4.1
	 */
	@Test
	public void testPropIntForInteger() {
		scapc.setValue("mandatoryProp","bar");

		Integer expected = Integer.valueOf(42);
		scapc.setValue("fieldPropInteger",expected.intValue());
		Integer value = itf.fieldPropIntegerValue();
		assertEquals(expected,value);
	}

	/**
	 * @since 1.1.2
	 */
	@Test
	public void testGetValuePropWithDefault() {
		scapc.setValue("mandatoryProp","bar");

		/*
		 * Invoked to trigger the instantiation of the content instance. Else
		 * the default value for the property can not be retrieved.
		 */
		itf.propWithDefaultValue();

		String expected = "default";
		Object value = scapc.getValue("fieldPropWithDefault");
		assertEquals(expected,value);
	}

	/**
	 * @since 1.1.2
	 */
	@Test
	public void testGetTwiceValuePropWithDefault() {
		scapc.setValue("mandatoryProp","bar");

		/*
		 * Invoked to trigger the instantiation of the content instance. Else
		 * the default value for the property can not be retrieved.
		 */
		itf.propWithDefaultValue();

		String expected = "default2";
		scapc.setValue("fieldPropWithDefault",expected);
		Object value = scapc.getValue("fieldPropWithDefault");
		assertEquals(expected,value);
	}

	/**
	 * @since 1.4.2
	 */
	public void testIsDeclared() {

		assertTrue(scapc.isDeclared("propWithType"));
		assertTrue(scapc.isDeclared("unannotatedProp"));
		assertTrue(scapc.isDeclared("fieldPropWithDefault"));
		assertTrue(scapc.isDeclared("propWithDefault"));
		assertTrue(scapc.isDeclared("unannotatedFieldProp"));
		assertTrue(scapc.isDeclared("mandatoryProp"));
		assertTrue(scapc.isDeclared("fieldPropInteger"));

		scapc.setValue("dyn",42);
		assertFalse(scapc.isDeclared("dyn"));
		assertFalse(scapc.isDeclared("unknown"));
	}

	/**
	 * @since 1.1.2
	 */
	@Test
	public void testPropertyTypes() {
		Class<?> type = scapc.getType("mandatoryProp");
		assertEquals(String.class,type);
		type = scapc.getType("fieldPropWithDefault");
		assertEquals(String.class,type);
		type = scapc.getType("propWithDefault");
		assertEquals(int.class,type);
	}

	/**
	 * @since 1.2.1
	 */
	@Test
	public void testReInjection()
	throws
		ClassNotFoundException, InstantiationException, IllegalAccessException,
		IllegalLifeCycleException, NoSuchInterfaceException,
		java.lang.InstantiationException, InterruptedException {

		String adl = getClass().getPackage().getName()+".PropertyReInject";
		String service = "r";
		Component comp = TinfiDomain.getComponent(adl);
		final PropertyReInjectItf itf = TinfiDomain.getService(comp,service);

		/*
		 * Retrieve the property control interface of the server component which
		 * is the 2nd subcomponent of the PropertyReInject composite.
		 */
		ContentController cc = Fractal.getContentController(comp);
		Component[] subs = cc.getFcSubComponents();
		Component server = subs[1];
		final SCAPropertyController scapc = (SCAPropertyController)
			server.getFcInterface(SCAPropertyController.NAME);

		final String expected1 = "foo";
		final String expected2 = "bar";

		scapc.setValue("prop",expected1);

		String v1 = itf.getValue();
		assertEquals(expected1,v1);

		// Start a second conversation
		Thread t =
			new Thread() {
				@Override
				public void run() {
					/*
					 * Check that the new conversation instance has been
					 * instantiated with the previously set value.
					 */
					String v2 = itf.getValue();
					assertEquals(expected1,v2);

					// Change the value of the property
					scapc.setValue("prop",expected2);

					// Check that the value has been re-injected in the instance
					v2 = itf.getValue();
					assertEquals(expected2,v2);

					// End the second conversation
					itf.end();
				}
			};
		t.start();
		t.join();

		/*
		 * Check that the value has been re-injected also in the instance
		 * associated with the first conversation.
		 */
		v1 = itf.getValue();
		assertEquals(expected2,v1);

		// End the first conversation
		itf.end();
	}
}
