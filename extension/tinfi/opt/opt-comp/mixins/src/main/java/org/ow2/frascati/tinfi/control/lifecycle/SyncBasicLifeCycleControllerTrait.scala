/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.lifecycle

import org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleControllerTrait

/**
 * <p>
 * Trait implementing a synchronized version of the
 * <code>incrementFcInvocationCounter</code> and the
 * <code>decrementFcInvocationCounter</code> methods.
 * </p>
 *
 * <p>
 * With component-based membranes, these methods are invoked as business methods
 * of the lifecycle control component. They need synchronized accesses since
 * they perform <code>wait</code> and <code>notify</code> operations. In the OO
 * version, this synchronized accessed is provided by the generated lifecycle
 * interceptors.
 * </p>
 *
 * @author Lionel <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.5
 */
trait SyncBasicLifeCycleControllerTrait extends BasicLifeCycleControllerTrait {

	override def incrementFcInvocationCounter = synchronized {
		super.incrementFcInvocationCounter
	}

	override def decrementFcInvocationCounter = synchronized {
		super.decrementFcInvocationCounter
	}
}
