/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2013-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.binding

import org.objectweb.fractal.api.`type`.InterfaceType
import org.objectweb.fractal.api.Interface
import org.objectweb.fractal.julia.control.binding.AbstractBindingControllerTrait
import org.objectweb.fractal.julia.control.binding.BasicBindingControllerTrait
import org.objectweb.fractal.julia.BasicControllerTrait
import org.objectweb.fractal.julia.UseComponentTrait

/**
 * This trait removes collection interface instances from the map of bindings.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.6
 */
trait CollectionInterfaceBindingControllerTrait extends BasicControllerTrait
with UseComponentTrait with BasicBindingControllerTrait
with AbstractBindingControllerTrait {

	// -------------------------------------------------------------------------
	// Implementation of the BindingController interface
	// -------------------------------------------------------------------------

	override def unbindFc( clientItfName: String ) = {

		val itf = weaveableC.getFcInterface(clientItfName).asInstanceOf[Interface]
		val it = itf.getFcItfType().asInstanceOf[InterfaceType]

		if( it.isFcCollectionItf ) {
			fcBindings.remove(clientItfName)
		}
		else {
			super.unbindFc( clientItfName )
		}
	}
}
