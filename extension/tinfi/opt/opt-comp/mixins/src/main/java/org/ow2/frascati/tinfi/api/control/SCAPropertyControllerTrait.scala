/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.api.control

/**
 * Abstract implementation of the {@link SCAPropertyController} interface to
 * enable calling super.aMethod (e.g. setType) in traits (e.g.
 * SCAPropertyPromoterTrait) which use this trait.
 *
 * @author Lionel <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.5
 */
trait SCAPropertyControllerTrait extends SCAPropertyController {
	def setType( name: String, typ: Class[_] ) = {}
	def setValue( name: String, value: Object ) = {}
	def getType( name: String ) : Class[_] = return null
	def getValue( name: String ) : Object = return null
	def containsPropertyName( name: String ) : Boolean = return false
	def getPropertyNames : Array[String] = return null
	def isDeclared( name: String ) : Boolean = return false
	def hasBeenSet( name: String ) : Boolean = return false
}
