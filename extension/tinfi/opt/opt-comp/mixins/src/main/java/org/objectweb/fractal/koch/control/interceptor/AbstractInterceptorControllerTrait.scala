/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.control.interceptor

import org.objectweb.fractal.julia.ComponentInterface
import org.objectweb.fractal.julia.Interceptor

/**
 * Abstract implementation of the {@link InterceptorController} interface to
 * enable calling super.aMethod in types which use this trait.
 *
 * @author Lionel <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.5
 */
trait AbstractInterceptorControllerTrait extends InterceptorController {
	override def addFcInterceptor( itf: ComponentInterface, interceptor: Interceptor ) = {}
	override def getFcInterceptors( itf: ComponentInterface ) : Array[Interceptor] = return null
	override def removeFcInterceptor( itf: ComponentInterface, interceptor: Interceptor ) : Boolean = return false
}
