/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.property

import org.objectweb.fractal.julia.BasicControllerTrait
import org.objectweb.fractal.julia.control.name.UseNameControllerTrait
import org.ow2.frascati.tinfi.api.control.IllegalPromoterException;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController

import scala.collection.mutable.HashMap

/**
 * Trait implementing the {@link SCAPropertyController} control interface
 * functionalities shared by scaPrimitive and scaComposite components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.5
 */
trait SCAPropertyControllerTrait extends BasicControllerTrait
with org.ow2.frascati.tinfi.api.control.SCAPropertyControllerTrait {

	private var values = new HashMap[String,Object]
	private var types = new HashMap[String,Class[_]]

	// -------------------------------------------------------------------------
	// Implementation of the SCAPropertyController interface
	// -------------------------------------------------------------------------

	override def setType( name: String, typ: Class[_] ) = {
	  types += name -> typ
	}

	override def setValue( name: String, value: Object ) = {

		// Record the new value
		values += name -> value

		// Re-inject the value on current content instances
		super.setValue(name,value)
	}

	override def getType( name: String ) : Class[_] = {
		if( types.contains(name) ) {
			val typ = types(name)
			return typ
		}
		else {
			val typ = super.getType(name)
			return typ
		}
	}

	override def getValue( name: String ) : Object = {
		if( values.contains(name) ) {
			val value = values(name)
			return value
		}
		else {
			return null
		}
	}

	override def containsPropertyName( name: String ) : Boolean = {
		var b = values.contains(name)
		if(!b) {
			b = super.containsPropertyName(name)
		}
		return b
	}

	override def getPropertyNames() : Array[String] = {
		val keys = values.keys
		val declared = super.getPropertyNames()
		val names = new Array[String](keys.size + declared.size)
		keys.copyToArray(names)
		declared.copyToArray(names,keys.size)
		return names
	}

	override def hasBeenSet( name: String ) : Boolean = {
		val b = values.contains(name)
		return b
	}
}
