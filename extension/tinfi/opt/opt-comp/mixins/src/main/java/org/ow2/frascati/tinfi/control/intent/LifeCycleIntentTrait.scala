/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.intent

import java.lang.annotation.Annotation
import java.lang.reflect.Method

import org.objectweb.fractal.api.NoSuchInterfaceException
import org.objectweb.fractal.api.control.IllegalLifeCycleException
import org.objectweb.fractal.api.control.LifeCycleController
import org.objectweb.fractal.api.control.NameController
import org.objectweb.fractal.julia.control.lifecycle.UseLifeCycleControllerTrait
import org.objectweb.fractal.julia.control.name.UseNameControllerTrait
import org.objectweb.fractal.julia.BasicControllerTrait
import org.ow2.frascati.tinfi.api.IntentHandler
import org.ow2.frascati.tinfi.api.InterfaceFilter
import org.ow2.frascati.tinfi.api.InterfaceMethodFilter
import org.ow2.frascati.tinfi.api.control.SCAIntentController

/**
 * Provides lifecycle related checks to a {@link SCAIntentController}. Check
 * that the component is stopped and then call the overriden method.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.5
 */
trait LifeCycleIntentTrait extends BasicControllerTrait
with UseLifeCycleControllerTrait with UseNameControllerTrait
with org.ow2.frascati.tinfi.api.control.SCAIntentControllerTrait {

	// -----------------------------------------------------------------------
	// SCAIntentController
	// -----------------------------------------------------------------------

	override def addFcIntentHandler[T <: Annotation](
		handler: IntentHandler , annotcl: Class[T], value: String ) = {

		checkFcStopped
		super.addFcIntentHandler(handler,annotcl,value)
	}

	// -----------------------------------------------------------------------
	// SCABasicIntentController
	// -----------------------------------------------------------------------

	override def addFcIntentHandler( handler: IntentHandler ) = {
		checkFcStopped
		super.addFcIntentHandler(handler)
	}

	override def addFcIntentHandler( handler: IntentHandler, filter: InterfaceFilter ) = {
		checkFcStopped
		super.addFcIntentHandler(handler,filter)
	}

	override def addFcIntentHandler( handler: IntentHandler, filter: InterfaceMethodFilter )  = {
		checkFcStopped
		super.addFcIntentHandler(handler,filter)
	}

	override def addFcIntentHandler( handler: IntentHandler, name: String ) = {
		checkFcStopped
		super.addFcIntentHandler(handler,name)
	}

	override def addFcIntentHandler( handler: IntentHandler, name: String, method: Method ) = {
		checkFcStopped
		super.addFcIntentHandler(handler,name,method)
	}

	override def removeFcIntentHandler( handler: IntentHandler ) = {
		checkFcStopped
		super.removeFcIntentHandler(handler)
	}

	override def removeFcIntentHandler( handler: IntentHandler, name: String ) = {
		checkFcStopped
		super.removeFcIntentHandler(handler,name)
	}

	override def removeFcIntentHandler( handler: IntentHandler, name: String, method: Method ) = {
		checkFcStopped
		super.removeFcIntentHandler(handler,name,method)
	}

	// -----------------------------------------------------------------------
	// Implementation specific
	// -----------------------------------------------------------------------

	private def checkFcStopped = {
		val state = weaveableLC.getFcState
		if( ! LifeCycleController.STOPPED.equals(state) ) {
			val msg = "Component "+weaveableNC.getFcName+" is not stopped"
			throw new IllegalLifeCycleException(msg)
		}
	}
}
