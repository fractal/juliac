/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.comp;

import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;
import org.ow2.frascati.tinfi.TinfiComponentInterceptor;

/**
 * This class generates the source code for initializing Fractal components.
 *
 * This initializer class generator assumes that the content, the interceptors
 * and the controllers are kept in separate classes and that the controllers are
 * implemented with components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class InitializerCompCtrlClassGenerator
extends org.objectweb.fractal.juliac.opt.comp.InitializerCompCtrlClassGenerator {

	public InitializerCompCtrlClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		super(jc,fcscg,membraneDesc,ct,contentDesc,source);
	}

	@Override
	protected void generateNewFcContentMethod( BlockSourceCodeVisitor mv ) {
		mv.visitln("    return null;");
	}

	@Override
	protected void generateContentChecks( BlockSourceCodeVisitor mv ) {
		// Indeed nothing
	}

	@Override
	public void generateInitializationContextForContent( BlockSourceCodeVisitor mv ) {
		mv.visit("    ic.content = ");
		if( contentDesc == null ) {
			mv.visit("null");
		}
		else {
			mv.visit(contentDesc.toString());
			mv.visit(".class");
		}
		mv.visitln(";");
	}

	@Override
	protected void generateInterceptorPostInit(
		BlockSourceCodeVisitor mv, InterfaceType it, String delegate ) {

		super.generateInterceptorPostInit(mv,it,delegate);

		mv.visit  ("    ((");
		mv.visit  (TinfiComponentInterceptor.class.getName());
		mv.visit  ("<?>)intercept).setFcItf((");
		mv.visit  (Interface.class.getName());
		mv.visit  (")");
		mv.visit  (delegate);
		mv.visitln(");");
	}

	@Override
	protected void generateNFICMExternalInterface(
		BlockSourceCodeVisitor mv, InterfaceType it ) {

		super.generateNFICMExternalInterface(mv,it);

		String itname = it.getFcItfName();
		if( ! itname.equals("attribute-controller") ) {
			mv.visit  ("    ((");
			mv.visit  (TinfiComponentInterceptor.class.getName());
			mv.visitln(")intercept).setFcItf(proxy);");
		}
	}
}
