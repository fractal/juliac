/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.comp;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.desc.NoSuchControllerDescriptorException;
import org.objectweb.fractal.juliac.api.generator.ProxyClassGeneratorItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.commons.lang.ClassHelper;
import org.objectweb.fractal.juliac.commons.lang.annotation.AnnotationHelper;
import org.objectweb.fractal.juliac.core.conf.JulietLoader;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.opt.InitializerClassGenerator;
import org.objectweb.fractal.juliac.core.proxy.InterfaceImplementationClassGenerator;
import org.ow2.frascati.tinfi.opt.oo.CallBackInterfaceClassGenerator;
import org.ow2.frascati.tinfi.opt.oo.ClientInterfaceClassGenerator;
import org.ow2.frascati.tinfi.opt.oo.SCAComposite;
import org.ow2.frascati.tinfi.opt.oo.SCACompositeWithContent;
import org.ow2.frascati.tinfi.opt.oo.SCAContainer;
import org.ow2.frascati.tinfi.opt.oo.SCAPrimitive;
import org.ow2.frascati.tinfi.opt.oo.ServerInterfaceClassGenerator;
import org.ow2.frascati.tinfi.opt.oo.ServiceReferenceClassGenerator;

/**
 * This class generates the source code associated to Fractal components.
 * The membrane implementation and the initializer implementation are generated.
 *
 * The content, the interceptors and the controllers are kept in separate
 * classes and the controllers are implemented with components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class FCCompCtrlSourceCodeGenerator
extends org.objectweb.fractal.juliac.opt.comp.FCCompCtrlSourceCodeGenerator {

	@Override
	protected void postInit() throws IOException {

		/*
		 * Don't call super.postInit(). This generator does not use the .cfg
		 * based configuration mechanism for defining membranes.
		 */

		mloader = new JulietLoader();
		mloader.init(jc);

		mloader.put(KOCK_PREFIX+SCAPrimitive.NAME,SCAPrimitive.class);
		mloader.put(KOCK_PREFIX+SCAComposite.NAME,SCAComposite.class);
		mloader.put(KOCK_PREFIX+SCAContainer.NAME,SCAContainer.class);
		mloader.put(
			KOCK_PREFIX+SCACompositeWithContent.NAME,
			SCACompositeWithContent.class );

		adlDescs.put(
			SCAPrimitive.NAME,"org.ow2.frascati.tinfi.membrane.SCAPrimitive");
		adlDescs.put(
			SCAComposite.NAME,"org.ow2.frascati.tinfi.membrane.SCAComposite");
		adlDescs.put(
			SCAContainer.NAME,"org.ow2.frascati.tinfi.membrane.SCAContainer");
		adlDescs.put(
			SCACompositeWithContent.NAME,
			"org.ow2.frascati.tinfi.membrane.SCACompositeWithContent");
	}

	private Map<String,String> adlDescs = new HashMap<>();


	// -----------------------------------------------------------------------
	// Implementation of the FCSourceCodeGeneratorItf interface
	// -----------------------------------------------------------------------

	/**
	 * Return the source code generator for the initializer class associated
	 * with this component source code generator.
	 */
	@Override
	protected InitializerClassGenerator
	getInitializerClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		return new InitializerCompCtrlClassGenerator(
			jc,fcscg,membraneDesc,ct,contentDesc,source);
	}

	/**
	 * Return the source code generator for component interfaces.
	 */
	@Override
	public ProxyClassGeneratorItf getInterfaceClassGenerator(InterfaceType it) {

		ProxyClassGeneratorItf pcg = null;
		String itname = it.getFcItfName();

		/*
		 * See the comments in method generateInterfaceImpl for the rationale
		 * for choosing interface source code generator.
		 */

		final String signature = it.getFcItfSignature();
		final Class<?> cl = jc.loadClass(signature);
		final String pkgRoot = jc.getPkgRoot();

		if( itname.endsWith("-controller") || itname.equals("component") ) {
			pcg =
				new InterfaceImplementationClassGenerator(it,cl,pkgRoot,false);
		}
		else {
			if( it.isFcClientItf() ) {
				/*
				 * In theory we should be returning here two interface source
				 * code generators since the client interface class extends the
				 * server one corresponding to the same type. However, the
				 * current method is mainly used for retrieving the name of the
				 * generated interface class (such as in initializer source code
				 * generators.) It is then sufficient to return only the class
				 * which corresponds to the real implementation.
				 */
				pcg =
					new ClientInterfaceClassGenerator(it,cl,pkgRoot,false);
			}
			else {
				pcg =
					new ServerInterfaceClassGenerator(it,cl,pkgRoot,false);
			}
		}

		return pcg;
	}

	/**
	 * Generate the source code of component interfaces associated with the
	 * specified interface type.
	 */
	@Override
	protected void generateInterfaceImpl( InterfaceType it, String ctrlDesc )
	throws IOException {

		final String signature = it.getFcItfSignature();
		final Class<?> cl = jc.loadClass(signature);
		final String pkgRoot = jc.getPkgRoot();

		String itname = it.getFcItfName();
		if( itname.endsWith("-controller") || itname.equals("component") ) {

			/*
			 * Control interfaces.
			 *
			 * It seems to be wiser to keep
			 * InterfaceImplementationClassGenerator as a source code generator
			 * for control interfaces even though ServerInterfaceClassGenerator
			 * could be used. This allows reusing pre-generated implementations
			 * for Julia control interfaces.
			 */
			SourceCodeGeneratorItf cg =
				new InterfaceImplementationClassGenerator(it,cl,pkgRoot,false);
			jc.generateSourceCode(cg);
		}
		else {

			/*
			 * Business interfaces.
			 */

			// Server (input) interface implementation
			SourceCodeGeneratorItf cg =
				new ServerInterfaceClassGenerator(it,cl,pkgRoot,false);
			jc.generateSourceCode(cg);

			if( it.isFcClientItf() ) {
				/*
				 * Client (output) interface implementations. Server (input)
				 * interface implementations are generated also for client ones
				 * since client interface implementations extend the server
				 * interface implementation corresponding to the same type.
				 */
				cg = new ClientInterfaceClassGenerator(it,cl,pkgRoot,false);
				jc.generateSourceCode(cg);
			}

			// ServiceReference implementation
			cg = new ServiceReferenceClassGenerator(it,cl,pkgRoot,null,false);
			jc.generateSourceCode(cg);

			// ServiceReference and input interface implementations for callbacks
			Annotation annot =
				ClassHelper.getAnnotation(
					cl,
					"org.oasisopen.sca.annotation.Callback",
					"org.osoa.sca.annotations.Callback");
			if( annot != null ) {
				Class<?> cbcl =
					AnnotationHelper.getAnnotationParamValue(annot,"value");
				InterfaceType cbit =
					new BasicInterfaceType(
						"callback", cbcl.getName(), false, false, false );
				cg = new ServiceReferenceClassGenerator(
						cbit,cbcl,pkgRoot,null,false);
				jc.generateSourceCode(cg);

				cg = new CallBackInterfaceClassGenerator(
						cbit,cbcl,pkgRoot,null,false);
				jc.generateSourceCode(cg);
			}
		}
	}

	/**
	 * Return the fully-qualified name of the ADL descriptor associated to the
	 * specified controller descriptor.
	 *
	 * @param ctrlDesc  the controller descriptor
	 * @throws NoSuchControllerDescriptorException  if there is no such controller
	 */
	@Override
	protected String getADLDesc( String ctrlDesc ) {
		if( adlDescs.containsKey(ctrlDesc) ) {
			String adlDesc = adlDescs.get(ctrlDesc);
			return adlDesc;
		}
		throw new NoSuchControllerDescriptorException(ctrlDesc);
	}
}
