/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.comp;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;
import org.ow2.frascati.tinfi.factory.MPrimitiveImpl;

/**
 * This class generates the source code associated to Fractal components.
 * The membrane implementation and the initializer implementation are generated.
 *
 * This generator handles mPrimitive and mComposite components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.5
 */
public class FCmCompCtrlSourceCodeGenerator
extends org.objectweb.fractal.juliac.opt.comp.FCmCompCtrlSourceCodeGenerator {

	/**
	 * Return the class implementing the membrane of the specified control
	 * component descriptor (mPrimitive or mComposite).
	 */
	@Override
	protected Class<?> getMembraneClass( String ctrldesc ) {

		if( ctrldesc.equals("mPrimitive") ) {
			return MPrimitiveImpl.class;
		}

		return super.getMembraneClass(ctrldesc);
	}

	/**
	 * Return the control interface types associated to the specified controller
	 * descritptor.
	 *
	 * @param ctrldesc  the controller descriptor
	 * @return          the associated array of control interface types
	 */
	@Override
	protected InterfaceType[] getMembraneType( String ctrldesc ) {

		InterfaceType[] its = super.getMembraneType(ctrldesc);

		if( ctrldesc.equals("mPrimitive") ) {
			InterfaceType[] newits = new InterfaceType[its.length+1];
			System.arraycopy(its,0,newits,0,its.length);
			newits[its.length] =
				new BasicInterfaceType(
					SCAPropertyController.NAME,
					SCAPropertyController.class.getName(),
					false, false, false );
			its = newits;
		}

		return its;
	}
}
