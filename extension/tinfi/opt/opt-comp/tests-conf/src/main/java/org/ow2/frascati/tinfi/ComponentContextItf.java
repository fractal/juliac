/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import java.util.Collection;

import org.oasisopen.sca.ComponentContext;
import org.oasisopen.sca.ServiceReference;

/**
 * Interface provided by the component implementation used for testing the
 * features provided by the {@link ComponentContext} interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public interface ComponentContextItf {
	public ServiceReference<ComponentContextItf> cast();
	public <C> C getService( Class<C> cl, String refname );
	public <C> ServiceReference<C> getServiceReference( Class<C> cl, String refname );
	public <C> C getProperty( Class<C> type, String propertyName );
	public String getRequestContextServiceName();
	public Class<?> getRequestContextServiceBusinessInterface();

	/**
	 * @since 1.4.1
	 */
	public <B> Collection<B> getServices( Class<B> cl, String refname );

	/**
	 * @since 1.4.1
	 */
	public <B> Collection<ServiceReference<B>> getServiceReferences(
		Class<B> cl, String refname );
}
