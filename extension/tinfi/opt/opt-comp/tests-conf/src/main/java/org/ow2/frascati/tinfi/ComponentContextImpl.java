/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import java.util.Collection;
import java.util.List;

import org.oasisopen.sca.ComponentContext;
import org.oasisopen.sca.RequestContext;
import org.oasisopen.sca.ServiceReference;
import org.oasisopen.sca.annotation.Context;
import org.oasisopen.sca.annotation.Property;
import org.oasisopen.sca.annotation.Reference;

/**
 * Component implementation for testing the features provided by the {@link
 * ComponentContext} interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class ComponentContextImpl implements ComponentContextItf {

	@Context
	public ComponentContext ctx;

	@Reference(required=false)
	public ComponentContextItf s;

	@Reference(required=false)
	public List<ComponentContextItf> cols;

	@Property
	public String name;

	public ServiceReference<ComponentContextItf> cast() {
		ServiceReference<ComponentContextItf> sr = ctx.cast(s);
		return sr;
	}

	public <B> B getService( Class<B> cl, String refname ) {
		B r = ctx.getService(cl,refname);
		return r;
	}

	public <B> ServiceReference<B> getServiceReference(
		Class<B> cl, String refname ) {

		ServiceReference<B> sr = ctx.getServiceReference(cl, refname);
		return sr;
	}

	public <B> B getProperty( Class<B> type, String propertyName ) {
		B value = ctx.getProperty(type, propertyName);
		return value;
	}

	public String getRequestContextServiceName() {
		RequestContext rq = ctx.getRequestContext();
		String sn = rq.getServiceName();
		return sn;
	}

	public Class<?> getRequestContextServiceBusinessInterface() {
		RequestContext rq = ctx.getRequestContext();
		ServiceReference<?> sr = rq.getServiceReference();
		Class<?> cl = sr.getBusinessInterface();
		return cl;
	}

	/**
	 * @since 1.4.1
	 */
	public <B> Collection<B> getServices( Class<B> cl, String refname ) {
		Collection<B> r = ctx.getServices(cl,refname);
		return r;
	}

	/**
	 * @since 1.4.1
	 */
	public <B> Collection<ServiceReference<B>> getServiceReferences(
		Class<B> cl, String refname ) {

		Collection<ServiceReference<B>> sr =
			ctx.getServiceReferences(cl, refname);
		return sr;
	}
}
