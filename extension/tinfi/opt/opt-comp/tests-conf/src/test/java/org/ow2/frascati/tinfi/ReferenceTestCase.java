/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.util.Fractal;

/**
 * Class for testing the use of references.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class ReferenceTestCase {

	@Test
	public void testMandatory()
	throws
		ClassNotFoundException, InstantiationException, IllegalAccessException,
		IllegalLifeCycleException, NoSuchInterfaceException,
		java.lang.InstantiationException {

		String adl = getClass().getPackage().getName()+".ReferenceMandatoryNotSet";
		String service = "r";
		Runnable itf = TinfiDomain.getService(adl,service);

		try {
			itf.run();
			fail("Mandatory reference shouldn't be allowed to be null");
		}
		catch(TinfiRuntimeException re) {}
	}

	@Test
	public void testOptional()
	throws
		ClassNotFoundException, InstantiationException, IllegalAccessException,
		IllegalLifeCycleException, NoSuchInterfaceException,
		java.lang.InstantiationException {

		String adl = getClass().getPackage().getName()+".ReferenceOptional";
		String service = "r";
		ReferenceOptionalItf itf = TinfiDomain.getService(adl,service);

		ReferenceOptionalItf opt = itf.opt();
		assertNull(opt,"Unbound optional reference should be null");

		List<ReferenceOptionalItf> opts = itf.opts();
		assertNotNull(
			opts,"Unbound optional multiple reference should not be null");
		int size = opts.size();
		assertEquals(
			0,size,"Unbound optional multiple reference should be an empty array");
	}

	@Test
	public void testMultiple()
	throws
		ClassNotFoundException, InstantiationException, IllegalAccessException,
		IllegalLifeCycleException, NoSuchInterfaceException,
		java.lang.InstantiationException {

		String adl = getClass().getPackage().getName()+".ReferenceMultiple";
		String service = "r";
		Runnable itf = TinfiDomain.getService(adl,service);

		itf.run();
		assertSame(2,ReferenceMultipleImpl.count);
	}

	/**
	 * @since 1.3.1
	 */
	@Test
	public void testReInjection()
	throws
		ClassNotFoundException, InstantiationException, IllegalAccessException,
		IllegalLifeCycleException, NoSuchInterfaceException,
		java.lang.InstantiationException, IllegalBindingException {

		String adl = getClass().getPackage().getName()+".ReferenceOptional";
		String service = "r";

		Component root = TinfiDomain.getComponent(adl);
		ContentController cc = Fractal.getContentController(root);
		Component c = cc.getFcSubComponents()[0];
		BindingController bc = Fractal.getBindingController(c);
		LifeCycleController lc = Fractal.getLifeCycleController(c);
		ReferenceOptionalItf itf = (ReferenceOptionalItf)
			c.getFcInterface(service);

		// The client collection interface opts is empty
		List<ReferenceOptionalItf> opts = itf.opts();
		int size = opts.size();
		assertEquals(0,size);

		// Add a new binding for opts and check that the dependency is injected
		bc.bindFc("opts0",itf);
		size = opts.size();
		assertEquals(1,size);

		// Add a new binding for opts and check that the dependency is injected
		bc.bindFc("opts1",itf);
		size = opts.size();
		assertEquals(2,size);

		// Remove the first binding
		lc.stopFc();
		bc.unbindFc("opts0");
		size = opts.size();
		assertEquals(1,size);

		// Remove the second binding
		bc.unbindFc("opts1");
		size = opts.size();
		assertEquals(0,size);
	}
}
