/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.oabdoun.constructor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.tinfi.TinfiDomain;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * Class for testing the instantiation @{@link
 * org.osoa.sca.annotations.Constructor} annotated components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class ConstructorPropertyInjectionTestCase {

	final private String pkg = getClass().getPackage().getName();
	final private String adl = pkg+".ConstructorPropertyInjection";

	@Test
	public void testFoo2() throws Exception {
		Component composite = TinfiDomain.getComponent(adl);
		Component client =
			Fractal.getContentController(composite).getFcSubComponents()[0];
		SCAPropertyController scapc = (SCAPropertyController)
			client.getFcInterface(SCAPropertyController.NAME);
		scapc.setValue("bar","fubar");

		Bar foo = (Bar) composite.getFcInterface("Foo2Component");
		assertEquals("fubar", foo.getBar());
		Class<?> type = scapc.getType("bar");
		assertEquals(String.class,type);
	}

	@Test
	public void testFoo3() throws Exception {
		Component composite = TinfiDomain.getComponent(adl);
		Component client =
			Fractal.getContentController(composite).getFcSubComponents()[1];
		SCAPropertyController scapc = (SCAPropertyController)
			client.getFcInterface(SCAPropertyController.NAME);
		scapc.setValue("bar", "fubar");

		Bar foo = (Bar) composite.getFcInterface("Foo3Component");
		assertEquals("fubar", foo.getBar());
		Class<?> type = scapc.getType("bar");
		assertEquals(String.class,type);
	}
}
