/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.oabdoun.optionalreference;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.ow2.frascati.tinfi.TinfiDomain;

/**
 * Class for testing mandatory references.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class OptionalReferenceTestCase {

	final private String pkg = getClass().getPackage().getName();
	final private String adl = pkg+".OptionalReference";

	@Test
	public void testOptionalReference() throws Exception {
		Component composite = TinfiDomain.getComponent(adl);
		Bar bar = (Bar) composite.getFcInterface("BazComponent");
		assertEquals("baz", bar.getBar());
	}

	@Test
	public void testOptionalReference2() throws Exception {
		Component composite = TinfiDomain.getComponent(adl);
		Bar bar = (Bar) composite.getFcInterface("BazComponent2");
		assertEquals("foo", bar.getBar());
	}
}
