/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.tinfi.api.control.IllegalPromoterException;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * Class for testing properties on scaComposite components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.1.2
 */
public class PropertyCompositeTestCase extends PropertyTestCase {

	private SCAPropertyController innerscapc;

	@BeforeEach
	@Override
	public void setUp() throws Exception {

		initCCMD();

		String adl = getClass().getPackage().getName()+".Property";
		String service = "r";
		Component comp = TinfiDomain.getComponent(adl);

		ContentController cc = Fractal.getContentController(comp);
		Component[] subs = cc.getFcSubComponents();
		Component inner = subs[0];
		innerscapc = (SCAPropertyController)
			inner.getFcInterface(SCAPropertyController.NAME);

		itf = TinfiDomain.getService(comp,service);
		scapc = (SCAPropertyController)
			comp.getFcInterface(SCAPropertyController.NAME);

		/*
		 * Promote the properties from the composite to the subcomponent.
		 */
		scapc.setPromoter("mandatoryProp",innerscapc);
		scapc.setPromoter("propWithDefault",innerscapc);
		scapc.setPromoter("fieldPropWithDefault",innerscapc);
		scapc.setPromoter("unannotatedFieldProp",innerscapc);
		scapc.setPromoter("unannotatedProp",innerscapc);
		scapc.setPromoter("fieldPropInteger",innerscapc);
	}

	@Test
	public void testSetPromoterWithDifferentName()
	throws IllegalPromoterException {

		scapc.setPromoter("top",innerscapc,"mandatoryProp");

		String expected = "foo";
		scapc.setValue("top",expected);
		Object actual = scapc.getValue("top");
		assertEquals(expected,actual);

		actual = innerscapc.getValue("mandatoryProp");
		assertEquals(expected,actual);
	}

	/** @since 1.4.5 */
	@Test
	public void testPromoterCycle() {
		try {
			innerscapc.setPromoter("mandatoryProp",scapc);
		}
		catch( IllegalPromoterException ipe ) {}
	}

	/** @since 1.4.5 */
	@Test
	public void testPromoterInnerToTop() throws IllegalPromoterException {

		scapc.removePromoter("mandatoryProp");
		innerscapc.setPromoter("mandatoryProp",scapc);

		String expected = "foo";
		scapc.setValue("mandatoryProp",expected);
		String actual = itf.mandatoryPropValue();
		assertEquals(expected,actual);
	}
}
