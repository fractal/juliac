# Tinfi

Tinfi implements a component personality for the [SCA](http://www.oasis-opencsa.org/sca) standard.

The term component personality refers to the structural and run-time characteristics associated to a component. This includes elements, such as how a component should be instantiated, started, wired with peers, activated, reconfigured, how requests should be processed, how properties should be managed, etc.

The Tinfi component personality is described in Section 3.2 of the article: Lionel Seinturier, Philippe Merle, Romain Rouvoy, Daniel Romero, Valerio Schiavoni, Jean-Bernard Stefani, "[A Component-Based Middleware Platform for Reconfigurable Service-Oriented Architectures](https://hal.inria.fr/inria-00567442)", Software: Practice and Experience, 42(5):559-583, May 2012, Wiley, Doi: [10.1002/spe.1077](https://dx.doi.org/10.1002/spe.1077).

If you use Tinfi for academic purposes, this article can be cited as:

```
@article{seinturier:inria-00567442,
  TITLE = {{A Component-Based Middleware Platform for Reconfigurable Service-Oriented Architectures}},
  AUTHOR = {Seinturier, Lionel and Merle, Philippe and Rouvoy, Romain and Romero, Daniel and Schiavoni, Valerio and Stefani, Jean-Bernard},
  URL = {https://hal.inria.fr/inria-00567442},
  JOURNAL = {{Software: Practice and Experience}},
  PUBLISHER = {{Wiley}},
  VOLUME = {42},
  NUMBER = {5},
  PAGES = {559-583},
  YEAR = {2012},
  MONTH = May,
  DOI = {10.1002/spe.1077},
  KEYWORDS = {adaptation ; component ; middleware ; soa ; service ; reconfiguration},
  PDF = {https://hal.inria.fr/inria-00567442/file/frascati.pdf},
  HAL_ID = {inria-00567442},
  HAL_VERSION = {v1},
}
```
