/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.frascati.model.osgi.impl;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.ow2.frascati.model.osgi.DocumentRoot;
import org.ow2.frascati.model.osgi.OsgiImplementation;
import org.ow2.frascati.model.osgi.OsgiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.ow2.frascati.model.osgi.impl.DocumentRootImpl#getImplementationOsgi <em>Implementation Osgi</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DocumentRootImpl extends org.eclipse.stp.sca.impl.DocumentRootImpl implements DocumentRoot {
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DocumentRootImpl() {
	super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
	return OsgiPackage.Literals.DOCUMENT_ROOT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OsgiImplementation getImplementationOsgi() {
	return (OsgiImplementation)getMixed().get(OsgiPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_OSGI, true);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetImplementationOsgi(OsgiImplementation newImplementationOsgi, NotificationChain msgs) {
	return ((FeatureMap.Internal)getMixed()).basicAdd(OsgiPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_OSGI, newImplementationOsgi, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setImplementationOsgi(OsgiImplementation newImplementationOsgi) {
	((FeatureMap.Internal)getMixed()).set(OsgiPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_OSGI, newImplementationOsgi);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
	switch (featureID) {
	  case OsgiPackage.DOCUMENT_ROOT__IMPLEMENTATION_OSGI:
		return basicSetImplementationOsgi(null, msgs);
	}
	return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
	switch (featureID) {
	  case OsgiPackage.DOCUMENT_ROOT__IMPLEMENTATION_OSGI:
		return getImplementationOsgi();
	}
	return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
	switch (featureID) {
	  case OsgiPackage.DOCUMENT_ROOT__IMPLEMENTATION_OSGI:
		setImplementationOsgi((OsgiImplementation)newValue);
		return;
	}
	super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
	switch (featureID) {
	  case OsgiPackage.DOCUMENT_ROOT__IMPLEMENTATION_OSGI:
		setImplementationOsgi((OsgiImplementation)null);
		return;
	}
	super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
	switch (featureID) {
	  case OsgiPackage.DOCUMENT_ROOT__IMPLEMENTATION_OSGI:
		return getImplementationOsgi() != null;
	}
	return super.eIsSet(featureID);
  }

} //DocumentRootImpl
