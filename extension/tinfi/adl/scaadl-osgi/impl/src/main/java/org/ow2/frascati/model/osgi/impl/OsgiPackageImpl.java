/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.frascati.model.osgi.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import org.eclipse.stp.sca.ScaPackage;

import org.eclipse.stp.sca.instance.InstancePackage;

import org.eclipse.stp.sca.policy.PolicyPackage;

import org.ow2.frascati.model.osgi.DocumentRoot;
import org.ow2.frascati.model.osgi.OsgiFactory;
import org.ow2.frascati.model.osgi.OsgiImplementation;
import org.ow2.frascati.model.osgi.OsgiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OsgiPackageImpl extends EPackageImpl implements OsgiPackage {
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass documentRootEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass osgiImplementationEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.ow2.frascati.model.osgi.OsgiPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private OsgiPackageImpl() {
	super(eNS_URI, OsgiFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this
   * model, and for any others upon which it depends.  Simple
   * dependencies are satisfied by calling this method on all
   * dependent packages before doing anything else.  This method drives
   * initialization for interdependent packages directly, in parallel
   * with this package, itself.
   * <p>Of this package and its interdependencies, all packages which
   * have not yet been registered by their URI values are first created
   * and registered.  The packages are then initialized in two steps:
   * meta-model objects for all of the packages are created before any
   * are initialized, since one package's meta-model objects may refer to
   * those of another.
   * <p>Invocation of this method will not affect any packages that have
   * already been initialized.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static OsgiPackage init() {
	if (isInited) { return (OsgiPackage)EPackage.Registry.INSTANCE.getEPackage(OsgiPackage.eNS_URI); }

	// Obtain or create and register package
	OsgiPackageImpl theOsgiPackage = (OsgiPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(eNS_URI) instanceof OsgiPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(eNS_URI) : new OsgiPackageImpl());

	isInited = true;

	// Initialize simple dependencies
	ScaPackage.eINSTANCE.eClass();
	PolicyPackage.eINSTANCE.eClass();
	InstancePackage.eINSTANCE.eClass();
	XMLTypePackage.eINSTANCE.eClass();

	// Create package meta-data objects
	theOsgiPackage.createPackageContents();

	// Initialize created meta-data
	theOsgiPackage.initializePackageContents();

	// Mark meta-data to indicate it can't be changed
	theOsgiPackage.freeze();

	return theOsgiPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDocumentRoot() {
	return documentRootEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_ImplementationOsgi() {
	return (EReference)documentRootEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOsgiImplementation() {
	return osgiImplementationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOsgiImplementation_Bundle() {
	return (EAttribute)osgiImplementationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOsgiImplementation_AnyAttribute() {
	return (EAttribute)osgiImplementationEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOsgiImplementation_Group() {
	return (EAttribute)osgiImplementationEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OsgiFactory getOsgiFactory() {
	return (OsgiFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents() {
	if (isCreated) { return; }
	isCreated = true;

	// Create classes and their features
	documentRootEClass = createEClass(DOCUMENT_ROOT);
	createEReference(documentRootEClass, DOCUMENT_ROOT__IMPLEMENTATION_OSGI);

	osgiImplementationEClass = createEClass(OSGI_IMPLEMENTATION);
	createEAttribute(osgiImplementationEClass, OSGI_IMPLEMENTATION__BUNDLE);
	createEAttribute(osgiImplementationEClass, OSGI_IMPLEMENTATION__ANY_ATTRIBUTE);
	createEAttribute(osgiImplementationEClass, OSGI_IMPLEMENTATION__GROUP);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents() {
	if (isInitialized) { return; }
	isInitialized = true;

	// Initialize package
	setName(eNAME);
	setNsPrefix(eNS_PREFIX);
	setNsURI(eNS_URI);

	// Obtain other dependent packages
	ScaPackage theScaPackage = (ScaPackage)EPackage.Registry.INSTANCE.getEPackage(ScaPackage.eNS_URI);
	XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

	// Create type parameters

	// Set bounds for type parameters

	// Add supertypes to classes
	documentRootEClass.getESuperTypes().add(theScaPackage.getDocumentRoot());
	osgiImplementationEClass.getESuperTypes().add(theScaPackage.getImplementation());

	// Initialize classes and features; add operations and parameters
	initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	initEReference(getDocumentRoot_ImplementationOsgi(), this.getOsgiImplementation(), null, "implementationOsgi", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

	initEClass(osgiImplementationEClass, OsgiImplementation.class, "OsgiImplementation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	initEAttribute(getOsgiImplementation_Bundle(), theXMLTypePackage.getString(), "bundle", null, 1, 1, OsgiImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	initEAttribute(getOsgiImplementation_AnyAttribute(), ecorePackage.getEFeatureMapEntry(), "anyAttribute", null, 0, -1, OsgiImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	initEAttribute(getOsgiImplementation_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, OsgiImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

	// Create resource
	createResource(eNS_URI);

	// Create annotations
	// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
	createExtendedMetaDataAnnotations();
  }

  /**
   * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createExtendedMetaDataAnnotations() {
	String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";
	addAnnotation
	  (documentRootEClass,
	   source,
	   new String[] {
	   "name", "",
	   "kind", "mixed"
	   });
	addAnnotation
	  (getDocumentRoot_ImplementationOsgi(),
	   source,
	   new String[] {
	   "kind", "element",
	   "name", "implementation.osgi",
	   "namespace", "##targetNamespace",
	   "affiliation", "http://www.osoa.org/xmlns/sca/1.0#implementation"
	   });
	addAnnotation
	  (osgiImplementationEClass,
	   source,
	   new String[] {
	   "name", "OsgiImplementation",
	   "kind", "elementOnly"
	   });
	addAnnotation
	  (getOsgiImplementation_AnyAttribute(),
	   source,
	   new String[] {
	   "kind", "attributeWildcard",
	   "wildcards", "##any",
	   "name", ":3",
	   "processing", "lax"
	   });
	addAnnotation
	  (getOsgiImplementation_Group(),
	   source,
	   new String[] {
	   "kind", "group",
	   "name", "group:sca:osgiimplementation"
	   });
  }

} //OsgiPackageImpl
