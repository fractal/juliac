/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.frascati.model.osgi.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

import org.ow2.frascati.model.osgi.OsgiPackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class OsgiXMLProcessor extends XMLProcessor {

  /**
   * Public constructor to instantiate the helper.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OsgiXMLProcessor() {
	super((EPackage.Registry.INSTANCE));
	OsgiPackage.eINSTANCE.eClass();
  }

  /**
   * Register for "*" and "xml" file extensions the OsgiResourceFactoryImpl factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected Map<String, Resource.Factory> getRegistrations() {
	if (registrations == null) {
	  super.getRegistrations();
	  registrations.put(XML_EXTENSION, new OsgiResourceFactoryImpl());
	  registrations.put(STAR_EXTENSION, new OsgiResourceFactoryImpl());
	}
	return registrations;
  }

} //OsgiXMLProcessor
