/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2010-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.tinfilet;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.oasisopen.sca.annotation.PolicySets;
import org.oasisopen.sca.annotation.Reference;
import org.oasisopen.sca.annotation.Service;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.objectweb.fractal.fraclet.types.Constants;
import org.objectweb.fractal.julia.type.BasicComponentType;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.RuntimeClassNotFoundException;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.commons.lang.ClassHelper;
import org.objectweb.fractal.juliac.commons.lang.GenericClass;
import org.objectweb.fractal.juliac.commons.lang.reflect.FieldHelper;
import org.objectweb.fractal.juliac.core.conf.JulietLoader;
import org.objectweb.fractal.juliac.core.conf.MembraneLoaderItf;
import org.objectweb.fractal.juliac.core.desc.ADLParserSupportItf;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;
import org.objectweb.fractal.juliac.core.helper.MembraneHelper;

/**
 * <p>
 * Factory source code generator for SCA annotated component implementations.
 * The name of the generated factory class is the concatenation of the name of
 * the component implementation class and the Factory suffix.
 * </p>
 *
 * <p>
 * The rationale for this generator is that we want to generate factories with
 * simple class names (hence the Factory suffix) and not with the usual Juliac
 * naming convention <code>impl FC ctrlDesc FC component type hash</code> in
 * order to be able to invoke the factory without having to specify neither the
 * controller descriptor nor the component type (of course the consequence is
 * that several factories for the same component implementation class but for
 * different controller descriptors and/or component types cannot be
 * generated.)
 * </p>
 *
 * <p>
 * This generator is not an usual ADL parser in the sense that it does not take
 * as input an ADL descriptor but an SCA annotated component implementation. The
 * idea is to facilitate the writing of components for developers who do not
 * need to write ADL descriptors but still want to develop for example component
 * libraries.
 * </p>
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.3.1
 */
public class TinfiParserSupportImpl implements ADLParserSupportItf {

	// -------------------------------------------------------------------
	// Implementation of the JuliacModuletItf
	// -------------------------------------------------------------------

	private JuliacItf jc;

	public void init( JuliacItf jc ) {
		this.jc = jc;
		jc.register(ADLParserSupportItf.class,this);
	}

	public void close( JuliacItf jc ) {
		jc.unregister(ADLParserSupportItf.class,this);
	}


	// -------------------------------------------------------------------
	// Implementation of the ADLParserSupportItf
	// -------------------------------------------------------------------

	public static final String FACTORY_SUFFIX = "Factory";

	/**
	 * Test whether this ADL parser accepts the specified ADL descriptor.
	 *
	 * @param adl  the fully-qualified name of the ADL descriptor
	 * @return     <code>true</code> if this ADL parser accepts the specified
	 *             ADL descriptor
	 */
	public boolean test( String adl ) {
		try {
			// Check that the ADL corresponds to a class that can be loaded
			jc.loadClass(adl);
			return true;
		}
		catch( RuntimeClassNotFoundException cnfe ) {
			return false;
		}
	}

	/**
	 * Parse the specified ADL descriptor and return a model of it.
	 *
	 * @param adl      the fully-qualified name of the ADL descriptor
	 * @param context  contextual information
	 * @return         the model of the specified ADL
	 */
	public ComponentDesc<?> parse( String adl, Map<Object,Object> context )
	throws IOException {
		// Not relevant for this ADL parser
		throw new UnsupportedOperationException();
	}

	/**
	 * Generate the source code of a factory class for the specified ADL
	 * descriptor.
	 *
	 * @param adl         the fully-qualified name of the ADL descriptor
	 * @param targetname  the fully-qualified name of the factory class
	 */
	public void generate( String adl, String targetname )
	throws IOException {

		/*
		 * adl contains the name of the SCA annotated component implementation.
		 */
		Class<?> cl = jc.loadClass(adl);

		/*
		 * Retrieve provided services declared by the @Service annotation.
		 * Naming convention for service names: if the names parameter is not
		 * not specified, the service name is the simple name of the service
		 * type.
		 *
		 * TODO iterate on inherited classes to retrieve @Service annotations
		 * TODO iterate on implemented interfaces to retrieve @Service annotations
		 *
		 * TODO support @Component from the Fractal Fraclet API
		 */
		Map<String,Class<?>> servicemap = new HashMap<>();
		Service serviceannot = ClassHelper.getAnnotation(cl,Service.class);
		if( serviceannot != null ) {

			// Service types and names
			Class<?>[] services = serviceannot.value();
			String[] servicenames = serviceannot.names();

			// Fill the service map
			for (int i = 0; i < services.length; i++) {
				String name =
					i < servicenames.length ?
					servicenames[i] :
					services[i].getSimpleName();
				if( servicemap.containsKey(name) ) {
					final String msg =
						"Duplicate service name "+name+" in "+adl;
					throw new IOException(msg);
				}
				servicemap.put(name,services[i]);
			}
		}

		/*
		 * Retrieve references.
		 *
		 * TODO iterate on setter methods to retrieve @Reference annotations
		 * TODO iterate on inherited classes to retrieve @Reference annotations
		 */
		Map<String,Class<?>> refmap = new HashMap<>();
		Map<String,Boolean> refcontingencymap = new HashMap<>();
		Map<String,Boolean> refcardinalitymap = new HashMap<>();
		Field[] fields = ClassHelper.getAllFields(cl);
		fields = FieldHelper.removeOverridden(fields);
		for (Field field : fields) {

			// TODO support @Requires from the Fractal Fraclet API
			Reference annot = field.getAnnotation(Reference.class);
			if( annot == null ) {
				continue;
			}

			String name = annot.name();
			if( name.length() == 0 ) {
				name = field.getName();
			}
			if( servicemap.containsKey(name) || refmap.containsKey(name) ) {
				final String msg =
					"Duplicate service/reference name "+name+" in "+adl;
				throw new IOException(msg);
			}

			Class<?> fieldtype = field.getType();
			boolean isMultiple = List.class.isAssignableFrom(fieldtype);
			refcardinalitymap.put(name,isMultiple);
			Class<?> reftype = null;
			if(isMultiple) {
				/*
				 * gt is expected to be of the form "type<generic>".
				 * Extract generic to retrieve the type of the reference.
				 */
				Class<?> rtype = field.getType();
				GenericClass uc = new GenericClass(rtype);
				String gt = uc.toString(rtype);
				String[] strs = gt.split("<");
				if( strs.length != 2 ) {
					final String msg =
						"Illegal field type "+gt+
						" for multiple references. Expected type is List<T>.";
					throw new IOException(msg);
				}
				String reftypename =
					strs[1].substring(0,strs[1].length()-1);  // remove trailing >
				reftype = jc.loadClass(reftypename);
			}
			else {
				reftype = fieldtype;
			}
			refmap.put(name,reftype);

			boolean required = annot.required();
			refcontingencymap.put(name,required);
		}

		/*
		 * Compute the component type.
		 */
		int size = servicemap.size() + refmap.size();
		InterfaceType[] its = new InterfaceType[size];
		int i = 0;
		for (String name : servicemap.keySet()) {
			Class<?> service = servicemap.get(name);
			String signature = service.getName();
			its[i] = new BasicInterfaceType(name,signature,false,false,false);
			i++;
		}
		for (String name : refmap.keySet()) {
			Class<?> service = refmap.get(name);
			String signature = service.getName();
			boolean isOptional = ! refcontingencymap.get(name);
			boolean isCollection = refcardinalitymap.get(name);
			its[i] =
				new BasicInterfaceType(
					name,signature,true,isOptional,isCollection);
			i++;
		}
		ComponentType ct = null;
		try {
			ct = new BasicComponentType(its);
		}
		catch (InstantiationException ie) {
			IOException ioe = new IOException();
			ioe.initCause(ie);
			throw ioe;
		}

		/*
		 * Retrieve the controller descriptor.
		 *
		 * The following three forms are supported and are queried in this
		 * order:
		 *
		 *         @Membrane(controller="...")        // from the Fractal Fraclet API
		 *         @PolicySets("frascati:...")        // from the OASIS SCA API
		 *        @Membrane(controllerDesc=...)    // from the Fractal Fraclet API
		 *
		 * If none of them apply, the scaPrimitive value is taken by default.
		 */
		String controller = "scaPrimitive";
		Membrane mAnnot = ClassHelper.getAnnotation(cl,Membrane.class);
		if( mAnnot != null ) {

			controller = mAnnot.controller();
			Class<?> controllerDesc = mAnnot.controllerDesc();

			// Consistency check
			if( controller.equals(Constants.EMPTY) &&
				controllerDesc.equals(Constants.class) ) {
				GenericClass uc = new GenericClass(cl);
				final String msg =
					"Both controller and controllerDesc cannot be null in "+
					mAnnot+" for class "+uc.toString(cl);
				throw new IOException(msg);
			}

			// Custom membrane specified with @Membrane(controllerDesc=...)
			if( ! controllerDesc.equals(Constants.class) ) {
				GenericClass uc = new GenericClass(cl);
				Class<?> mdefcl =
					MembraneHelper.getMembraneDef(mAnnot,uc.toString(cl));
				Membrane mdef = mdefcl.getAnnotation(Membrane.class);
				controller = mdef.desc();
				Iterable<MembraneLoaderItf> mloaders =
					jc.lookup(MembraneLoaderItf.class);
				for( MembraneLoaderItf mloader : mloaders ) {
					if( mloader instanceof JulietLoader ) {
						mloader.put(controller,mdefcl);
					}
				}
			}
			// Otherwise controller is not empty and we just need to proceed
		}
		else {
			/*
			 * Legacy (Tinfi 1.4) support of the
			 * @PolicySets("frascati:...") syntax for specifying membrane
			 * descripors. Used by Easy* from Petals Link.
			 */
			PolicySets psAnnot =
				ClassHelper.getAnnotation(cl,PolicySets.class);
			if( psAnnot != null ) {
				final String prefix = "frascati:";
				String[] strs = psAnnot.value();
				for (String str : strs) {
					if( str.startsWith(prefix) ) {
						controller = str.substring(prefix.length());
					}
				}
			}
		}

		/*
		 * Generate the Juliac factory class for the component.
		 */
		SourceCodeGeneratorItf icg = jc.generate(ct,controller,adl,cl);

		/*
		 * Generate a second factory that extends the previous one and that
		 * owns a simpler name. The purpose is to be able to invoke the factory
		 * without having to be aware of the controller descriptor nor the
		 * component type. Only the component implementation class name and the
		 * naming convention based on the Factory suffix is needed.
		 */
		String compFactClassName = adl + FACTORY_SUFFIX;
		String superClassName = icg.getTargetTypeName();
		SourceCodeGeneratorItf cg =
			new EmptySubClassGenerator(compFactClassName,superClassName);
		jc.generateSourceCode(cg);
	}

	/**
	 * Generate the source code of a factory class for the specified model of
	 * the ADL.
	 *
	 * @param cdesc       the model of the ADL descriptor
	 * @param targetname  the fully-qualified name of the factory class
	 * @since 2.5
	 */
	public void generate( ComponentDesc<?> cdesc, String targetname )
	throws IOException {
		// Not relevant for this ADL parser
		throw new UnsupportedOperationException();
	}
}
