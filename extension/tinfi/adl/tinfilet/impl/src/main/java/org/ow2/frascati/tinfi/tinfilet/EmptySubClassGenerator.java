/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2010-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.tinfilet;

import org.objectweb.fractal.juliac.core.opt.ClassGenerator;

/**
 * Generate an empty class that extends the one specified in the constructor.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.3.1
 */
public class EmptySubClassGenerator extends ClassGenerator {

	/**
	 * @param targetname   the fully-qualified name of the class to be generated
	 * @param superclname  the fully-qualified name of the super class
	 */
	public EmptySubClassGenerator( String targetname, String superclname ) {
		this.targetname = targetname;
		this.superclname = superclname;
	}

	private String targetname;
	private String superclname;

	public String getTargetTypeName() {
		return targetname;
	}

	@Override
	public String getSuperClassName() {
		return superclname;
	}
}
