/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.emf;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.core.Juliac;
import org.objectweb.fractal.juliac.core.desc.ADLParserSupportItf;
import org.objectweb.fractal.juliac.core.desc.AttributeDesc;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;

/**
 * Class for testing the functionalities of the {@link EMFParserSupportImpl}
 * class.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.4.5
 */
public class EMFParserSupportImplTestCase {

	private JuliacItf jc;
	private ADLParserSupportItf emfps;

	@BeforeEach
	public void setUp() throws IOException {
		jc = new Juliac();
		emfps = new EMFParserSupportImpl();
		emfps.init(jc);
	}

	@AfterEach
	public void tearDown() throws IOException {
		emfps.close(jc);
		jc.close();
	}

	@Test
	public void testPropertyClass() throws IOException {

		ComponentDesc<Object> cdesc = (ComponentDesc<Object>) emfps.parse("Property",null);
		List<ComponentDesc<Object>> subs = cdesc.getSubComponents();
		ComponentDesc<?> sub = subs.get(0);
		AttributeDesc adesc = sub.getAttribute("classprop");
		String type = adesc.getType();
		String value = adesc.getValue();

		assertEquals(Class.class.getName(),type);
		assertEquals(Runnable.class.getName(),value);
	}
}
