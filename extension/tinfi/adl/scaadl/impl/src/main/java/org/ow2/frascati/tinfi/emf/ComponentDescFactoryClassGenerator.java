/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.emf;

import java.io.PrintWriter;
import java.util.Set;

import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.core.desc.AttributeDesc;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * This class extends the {@link
 * org.objectweb.fractal.juliac.core.desc.ComponentDescFactoryClassGenerator} class
 * from Juliac to redefine the way attribute values are set.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.1
 */
public class ComponentDescFactoryClassGenerator<T>
extends org.objectweb.fractal.juliac.core.desc.ComponentDescFactoryClassGenerator<T> {

	public ComponentDescFactoryClassGenerator(
		JuliacItf jc, ComponentDesc<T> cdesc, String targetname ) {

		super(jc,cdesc,targetname);
	}

	@Override
	protected void generateAttributes(
		ComponentDesc<T> cdesc, PrintWriter pw ) {

		String id = cdesc.getID();
		Set<String> names = cdesc.getAttributeNames();

		for (String name : names) {
			AttributeDesc adesc = cdesc.getAttribute(name);
			Object value = adesc.getValue();
			String type = adesc.getType();

			if( type != null ) {
				pw.print  ("((");
				pw.print  (SCAPropertyController.class.getName());
				pw.print  (")");
				pw.print  (id);
				pw.print  (".getFcInterface(\"");
				pw.print  (SCAPropertyController.NAME);
				pw.print  ("\")).setType(\"");
				pw.print  (name);
				pw.print  ("\",");
				pw.print  (type);
				pw.println(".class);");
			}

			pw.print  ("((");
			pw.print  (SCAPropertyController.class.getName());
			pw.print  (")");
			pw.print  (id);
			pw.print  (".getFcInterface(\"");
			pw.print  (SCAPropertyController.NAME);
			pw.print  ("\")).setValue(\"");
			pw.print  (name);
			pw.print  ("\",");
			if( type == null ) {
				// String value assumed
				pw.print  ("\"");
				pw.print  (value);
				pw.print  ("\"");
			}
			else {
				// Only Class values supported so far
				pw.print  (value);
				pw.print  (".class");
			}
			pw.println(");");
		}
	}
}
