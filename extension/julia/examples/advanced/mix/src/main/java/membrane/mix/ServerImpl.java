package membrane.mix;

public class ServerImpl implements Service, ServiceAttributes {

  private String header;

  private int count;

  public ServerImpl () {
	System.err.println("SERVER created");
  }

  public void print (final String msg) {
	new Exception() {
		private static final long serialVersionUID = 2182742162070453637L;
		@Override
		public String toString () {
			return "Server: print method called";
		}
	}.printStackTrace();
	System.err.println("Server: begin printing...");
	for (int i = 0; i < count; ++i) {
	  System.err.println(header + msg);
	}
	System.err.println("Server: print done.");
  }

  public String getHeader () {
	return header;
  }

  public void setHeader (final String header) {
	this.header = header;
  }

  public int getCount () {
	return count;
  }

  public void setCount (final int count) {
	this.count = count;
  }
}
