package membrane.mix;

import java.io.IOException;

import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.JuliacModuleItf;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.opt.comp.FCCompCtrlSourceCodeGenerator;
import org.objectweb.fractal.juliac.opt.oo.FCOOCtrlSourceCodeGenerator;

public class Conf implements JuliacModuleItf {

	private FCSourceCodeGeneratorItf oo;
	private FCSourceCodeGeneratorItf comp;

	public void init( JuliacItf jc ) throws IOException {
		oo = new FCOOCtrlSourceCodeGenerator();
		comp = new FCCompCtrlSourceCodeGenerator();
		oo.setCtrlDescPrefix("/julia/");
		comp.setCtrlDescPrefix("/koch/");
		oo.init(jc);
		comp.init(jc);
	}

	public void close( JuliacItf jc ) throws IOException {
		comp.close(jc);
		oo.close(jc);
	}

}
