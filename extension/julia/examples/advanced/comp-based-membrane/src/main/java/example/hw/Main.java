/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package example.hw;

import java.lang.reflect.InvocationTargetException;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.koch.control.membrane.MembraneController;
import org.objectweb.fractal.util.ContentControllerHelper;
import org.objectweb.fractal.util.Fractal;

import example.hw.membrane.StatController;

/**
 * Main class for this example.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.5
 */
public class Main {

	public static void main( String[] args ) throws Exception {

		/*
		 * Instanciate the example.hw.HelloWorld composite.
		 */
		Component hw = newFcInstance("example.hw.HelloWorld");

		/*
		 * Retrieve the membrane of the client component inserted into the
		 * example.hw.HelloWorld composite.
		 */
		Component client = ContentControllerHelper.getSubComponentByName(hw,"client");
		MembraneController mc = (MembraneController) client.getFcInterface(MembraneController.NAME);
		Component membrane = mc.getFcMembrane();

		/*
		 * Instanciate the example.hw.membrane.ExternalStatController and bind
		 * it to the previously retrieved membrane.
		 */
		Component esc = newFcInstance("example.hw.membrane.ExternalStatController");
		StatController serverItf = (StatController) esc.getFcInterface("//stat-controller");
		Fractal.getBindingController(membrane).bindFc( "external", serverItf );
		Fractal.getLifeCycleController(esc).startFc();

		/*
		 * Start the example.hw.HelloWorld composite and invoke the r interface.
		 */
		Fractal.getLifeCycleController(hw).startFc();
		Runnable r = (Runnable) hw.getFcInterface("r");
		r.run();
	}

	/**
	 * Utility method for instanciating a component.
	 *
	 * @param adl  the ADL descriptor
	 * @return     the corresponding component instance
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws
	 */
	private static Component newFcInstance( String adl )
	throws
		ClassNotFoundException, java.lang.InstantiationException,
		IllegalAccessException, InstantiationException,
		InvocationTargetException, NoSuchMethodException {

		@SuppressWarnings("unchecked")
		Class<Factory> cl = (Class<Factory>) Class.forName(adl);
		Factory f = cl.getConstructor().newInstance();
		Component c = f.newFcInstance();
		return c;
	}
}
