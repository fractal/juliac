/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package example.hw.membrane;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;

/**
 * Implementation of the stat controller as a control component located inside
 * the myStatPrimitive primitive. This component delegates the invocation to the
 * stat controller bound to the <code>external</code> interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.5
 */
public class BasicStatController implements StatController, BindingController {

	public void incrementFcCounter( String method ) {
	  System.err.println("<< delegating");
	  sc.incrementFcCounter(method);
	}

	public int getNumberOfMethodCalled() {
	  return sc.getNumberOfMethodCalled();
	}

	private StatController sc;

	public void bindFc(String clientItfName, Object serverItf) {
		if( clientItfName.equals("external") ) {
			sc = (StatController) serverItf;
		}
	}

	public String[] listFc() {
		return new String[]{"external"};
	}

	public Object lookupFc(String clientItfName) throws NoSuchInterfaceException {
		if( clientItfName.equals("external") ) {
			return sc;
		}
		throw new NoSuchInterfaceException(clientItfName);
	}

	public void unbindFc(String clientItfName) {
		if( clientItfName.equals("external") ) {
			sc = null;
		}
	}
}
