/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package example.large;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;

import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.util.Fractal;

/**
 * Class for testing the processing by Juliac of large Fractal assembling.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.4
 */
public class LargeTestCase {

	@Test
	public void testRun()
	throws
		ClassNotFoundException, InstantiationException, IllegalAccessException,
		java.lang.InstantiationException, NoSuchInterfaceException,
		IllegalLifeCycleException, IOException,
		InvocationTargetException, NoSuchMethodException {

		Class<?> cl = Class.forName("example.large.Large");
		Factory f = (Factory) cl.getConstructor().newInstance();

		/*
		 * Use the Console class to direct the output of the example to a
		 * temporary file.
		 */
		File tmp = File.createTempFile("ultra-merge-",".txt");
		PrintStream ps = new PrintStream(tmp);
		Console.ps = ps;
		Component comp = f.newFcInstance();
		Fractal.getLifeCycleController(comp).startFc();
		Runnable r = (Runnable) comp.getFcInterface("r");
		r.run();
		ps.close();

		/*
		 * Compare the output with the expected result.
		 */
		FileReader fr = new FileReader(tmp);
		BufferedReader br = new BufferedReader(fr);

		String first = br.readLine();
		assertEquals( "CLIENT created", first );

		String second = br.readLine();
		assertEquals( "SERVER created", second );

		for (int i = 0; i < Generator.COUNT; i++) {
			String middle = br.readLine();
			assertEquals( "print in MIDDLE"+i, middle );
		}

		String last = br.readLine();
		assertEquals( "Server: begin printing...", last );
		last = br.readLine();
		assertEquals( "-> hello world", last );
		last = br.readLine();
		assertEquals( "Server: print done.", last );

		br.close();
		fr.close();
		tmp.delete();
	}
}
