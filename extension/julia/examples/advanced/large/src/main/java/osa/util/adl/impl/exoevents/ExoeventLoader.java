/**
 * Copyright INRIA Authors : Olivier Dalle (Olivier.Dalle-AT-sophia.inria.fr)
 * Judicael Ribault (Judicael.Ribault-AT-sophia.inria.fr) Cyrine Mrabet
 * (Cyrine.Mrabet-AT-sophia.inria.fr) Fabrice Peix
 * (Fabrice.Peix-AT-sophia.inria.fr) This software is part of the Open
 * Simulation Architecture (OSA). OSA is primarily intended to be a federating
 * platform for the simulation community: it is designed to favor the
 * integration of new or existing contributions at every level of its
 * architecture. This software is governed by the CeCILL-C license under French
 * law and abiding by the rules of distribution of free software. You can use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". As a counterpart to the access to the source code
 * and rights to copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the holder
 * of the economic rights, and the successive licensors have only limited
 * liability. In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or reproducing
 * the software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals having
 * in-depth computer knowledge. Users are therefore encouraged to load and test
 * the software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured and, more
 * generally, to use and operate it in the same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */
package osa.util.adl.impl.exoevents;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.components.ComponentDefinition;

import osa.util.adl.interfaces.exoevents.Exoevent;
import osa.util.adl.interfaces.exoevents.Exoevents;
import osa.util.adl.interfaces.exoevents.ExoeventsContainer;

/**
 * ADL extension module to support exogenous event insertion. This is Loader
 * part of the module, which is responsible for the semantic verifications.
 * FIXME : Add missing JavaDoc
 *
 * @author odalle
 */
public class ExoeventLoader extends AbstractLoader {

  /**
   * FIXME : Missing JavaDoc
   *
   * @param container
   * @param extend
   * @param context
   * @throws ADLException
   */
  private void checkExoeventsContainer(final ExoeventsContainer container,
									   final boolean extend, final Map context)
																			   throws ADLException {

	final Exoevents evts = container.getExoevents();
	if (evts == null) {

	  return;
	}

	final String signature = evts.getSignature();

	if (signature == null) {
	  if (!extend) {
		throw new ADLException("Signature missing", (Node) evts);
	  }
	  return;
	}

	/*
	 * Class c; try { c = getClassLoader(context).loadClass(signature); } catch
	 * (ClassNotFoundException e) { throw new ADLException(
	 * "Invalid signature '" + signature + "'", (Node)evts, e); }
	 */

	final Exoevent[] events = evts.getExoevents();
	for (int i = 0; i < events.length; ++i) {
	  final String evtName = events[i].getName();
	  final String evtType = events[i].getType();
	  final String evtTime = events[i].getTime();
	  final String evtMeth = events[i].getMethod();

	  if (evtName == null) {
		throw new ADLException("Event 'name' attribute missing",
							   (Node) events[i]);
	  }
	  if (evtType == null) {
		throw new ADLException("Event 'type' attribute missing",
							   (Node) events[i]);
	  }

	  if (!evtType.equals("StartOfCall")) {
		throw new ADLException("Event type '" + evtType
											  + "' unsupported (yet).", (Node) events[i]);
	  }

	  if (evtTime == null) {
		throw new ADLException("Event '_time' attribute missing",
							   (Node) events[i]);
	  }

	  if (Long.valueOf(evtTime) < 0) {
		throw new ADLException("Event '_time' attribute negative",
							   (Node) events[i]);
	  }

	  if (evtMeth == null) {
		throw new ADLException("Event 'method' attribute missing",
							   (Node) events[i]);
	  }

	}
  }


  /**
   * FIXME : Missing JavaDoc
   *
   * @param node
   * @param extend
   * @param context
   * @throws ADLException
   */
  private void checkNode(final Object node, final boolean extend,
						 final Map context) throws ADLException {
	if (node instanceof ExoeventsContainer) {
	  checkExoeventsContainer((ExoeventsContainer) node, extend, context);
	}
	if (node instanceof ComponentContainer) {
	  final Component[] comps = ((ComponentContainer) node).getComponents();
	  for (final Component element : comps) {
		checkNode(element, extend, context);
	  }
	}
  }


  public Definition load(String name, Map context) throws ADLException {
	final Definition d = clientLoader.load(name, context);
	boolean extend = false;
	if (d instanceof ComponentDefinition) {
	  extend = ((ComponentDefinition) d).getExtends() != null;
	}
	checkNode(d, extend, context);
	return d;
  }

}
