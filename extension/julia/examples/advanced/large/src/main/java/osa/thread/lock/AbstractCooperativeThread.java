package osa.thread.lock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * This abstract class implement basic cooperative threads scheduling
 * .
 * @author fabrice.peix@sophia.inria.fr
 */
public abstract class AbstractCooperativeThread extends Thread {

  /** Say of this thread is running. */
  private boolean                running_ = false;

  /** The lock used by this thread for managing scheduling. */
  private Lock                   lock_;

  /** The condition associated with this thread. */
  private Condition              condition_;

  /** The group of this thread or <code>null</code> if not yet define. */
  private CooperativeThreadGroup group_;


  /**
   * Initialize this thread.
   */
  public AbstractCooperativeThread() {
	// Nothing to do
  }


  /**
   * Set the group of this thread.
   *
   * @param group
   *        The new group of this thread.
   */
  public final void setGroup(final CooperativeThreadGroup group) {
	if (group_ != null) {
	  group_.removeThread(this);
	}
	group_ = group;
	group_.addThread(this);
	lock_ = group_.getGroupLock();
	condition_ = lock_.newCondition();
  }


  /**
   *
   */
  @Override
  public final void run() {
	lock_.lock();
	try {
	  while (!running_) {
		try {
		  condition_.await();
		} catch (InterruptedException e) {
		  throw new RuntimeException("Receive InterruptedException :"
									 + e.getMessage());
		}
	  }
	  threadCode();
	  AbstractCooperativeThread next = chooseNextThreadWhenDie();
	  if (next != null) {
		next.wakeUp();
	  }
	} finally {
	  lock_.unlock();
	}
  }


  /**
   * Resume a given thread.
   *
   * @param toBeActive
   *        The thread that must be resumed.
   */
  public final void changeActiveThread(
	final AbstractCooperativeThread toBeActive) {
	if (toBeActive.getState() == State.NEW) {
	  toBeActive.start();
	}
	if (!toBeActive.isAlive()) {
	  throw new IllegalThreadStateException("The next thread is not alive");
	}
	toBeActive.wakeUp();
	running_ = false;
	while (!running_) {
	  try {
		condition_.await();
	  } catch (InterruptedException e) {
		throw new RuntimeException("Receive InterruptedException :"
								   + e.getMessage());
	  }
	}
  }


  /**
   * Give the current running cooperative thread.
   *
   * @return The running cooperative thread.
   */
  public static AbstractCooperativeThread getCurrentThread() {
	return (AbstractCooperativeThread) Thread.currentThread();
  }


  /**
   * The code executed by this thread.
   */
  protected abstract void threadCode();


  /**
   * This method give the next thread to execute when this thread die.
   *
   * @return Give thread to wake up when this thread die.
   */
  protected abstract AbstractCooperativeThread chooseNextThreadWhenDie();


  /**
   * Wake up this thread.
   */
  final void wakeUp() {
	running_ = true;
	condition_.signal();
  }

}
