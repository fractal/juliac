package osa.control.api;

import osa.api.SimulationServices;
import osa.thread.lock.AbstractCooperativeThread;

/**
 *
 * @author Judicael Ribault
 */
public interface SimulationController extends SimulationServices {

  /**
   * Give the thread managing this controller.
   *
   * @return The thread managing this controller.
   */
  AbstractCooperativeThread getThread();


  /**
   * Stop this simulation controller.
   */
  void quit();
}
