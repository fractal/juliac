package example.large;

public class LastImpl implements Service , ServiceAttributes {

	public LastImpl() {
		Console.println("SERVER created");
	}

	public void print(final String msg) {
		Console.println("Server: begin printing...");
		for (int i = 0 ; i < (count) ; ++i) {
			Console.println(((header) + msg));
		}
		Console.println("Server: print done.");
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(final String header) {
		this.header = header;
	}

	public int getCount() {
		return count;
	}

	public void setCount(final int count) {
		this.count = count;
	}

	private String header;
	private int count;
}
