package example.large;

import java.io.PrintStream;

public class Console {

	public static PrintStream ps = System.err;

	public static void println( String msg ) {
		ps.println(msg);
	}
}
