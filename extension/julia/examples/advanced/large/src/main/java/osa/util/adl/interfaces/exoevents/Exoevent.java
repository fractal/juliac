/**
 * Copyright INRIA Authors : Olivier Dalle (Olivier.Dalle-AT-sophia.inria.fr)
 * Judicael Ribault (Judicael.Ribault-AT-sophia.inria.fr) Cyrine Mrabet
 * (Cyrine.Mrabet-AT-sophia.inria.fr) Fabrice Peix
 * (Fabrice.Peix-AT-sophia.inria.fr) This software is part of the Open
 * Simulation Architecture (OSA). OSA is primarily intended to be a federating
 * platform for the simulation community: it is designed to favor the
 * integration of new or existing contributions at every level of its
 * architecture. This software is governed by the CeCILL-C license under French
 * law and abiding by the rules of distribution of free software. You can use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". As a counterpart to the access to the source code
 * and rights to copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the holder
 * of the economic rights, and the successive licensors have only limited
 * liability. In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or reproducing
 * the software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals having
 * in-depth computer knowledge. Users are therefore encouraged to load and test
 * the software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured and, more
 * generally, to use and operate it in the same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */
package osa.util.adl.interfaces.exoevents;

/**
 * ADL extension module to support exogenous event insertion. This is the AST
 * interface definition for the new ExoEvent node with six attributes : a unique
 * name (identifier) for the event (to avoid duplicates in case the event is
 * inserted in a shared component), the type of the event (SOC, EOR, ...), the
 * currentTime_ of the event, the method name and the interface to which the method
 * belong and an optional parameter.
 *
 * @author odalle FIXME : All methods must be documented
 */
public interface Exoevent {
  /**
   * FIXME : Give the name of the method associated with this event.
   *
   * @return The name of the method associated with this event.
   */
  public String getMethod();


  /**
   * Give the name of this event.
   *
   * @return The name of this event.
   */
  public String getName();


  /**
   * Give the parameter of the method associated with this event.
   *
   * @return The parameter of the method associated with this event.
   */
  public String getParam();


  /**
   * Return the currentTime_ of this event.
   *
   * @return The currentTime_ of this event.
   */
  public String getTime();


  /**
   * Give the type of this event.
   *
   * @return The type of this event.
   */
  public String getType();


  /**
   * FIXME : Set the method name associated with this event ?
   *
   * @param method
   *        The method name. FIXME : <code>null</code> is a valid value ?
   */
  public void setMethod(String method);


  /**
   * Set the name of this event.
   *
   * @param name
   *        The new name of this event.
   */
  public void setName(String name);


  /**
   * FIXME : Set the parameter of the method associated with this event.
   *
   * @param parameter
   *        The parameter of the method associated with this event. FIXME :
   *        <code>null</code> is a valid value ?
   */
  public void setParam(String parameter);


  /**
   * Set the currentTime_ of this event.
   *
   * @param currentTime_
   *        The currentTime_ of this event.
   */
  public void setTime(String time);


  /**
   * Set the type of this event.
   *
   * @param type
   *        The type of this event.
   */
  public void setType(String type);
}
