/**
 * Copyright INRIA Authors : Olivier Dalle (Olivier.Dalle-AT-sophia.inria.fr)
 * Judicael Ribault (Judicael.Ribault-AT-sophia.inria.fr) Cyrine Mrabet
 * (Cyrine.Mrabet-AT-sophia.inria.fr) Fabrice Peix
 * (Fabrice.Peix-AT-sophia.inria.fr) This software is part of the Open
 * Simulation Architecture (OSA). OSA is primarily intended to be a federating
 * platform for the simulation community: it is designed to favor the
 * integration of new or existing contributions at every level of its
 * architecture. This software is governed by the CeCILL-C license under French
 * law and abiding by the rules of distribution of free software. You can use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". As a counterpart to the access to the source code
 * and rights to copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the holder
 * of the economic rights, and the successive licensors have only limited
 * liability. In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or reproducing
 * the software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals having
 * in-depth computer knowledge. Users are therefore encouraged to load and test
 * the software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured and, more
 * generally, to use and operate it in the same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */
package osa.util.adl.impl.exoevents;

import java.io.PrintStream;

import org.objectweb.fractal.api.Component;

import osa.control.api.SimulationController;
import osa.util.adl.interfaces.exoevents.ExoeventAttributes;
import osa.util.adl.interfaces.exoevents.ExoeventBuilder;

/**
 * A Java based implementation of the {@link ExoeventBuilder} interface.
 *
 * @author Judicael Ribault
 */
public class FractalExoeventBuilder implements ExoeventBuilder,
								   ExoeventAttributes {

  /**
   * Print a given message on a given stream.
   *
   * @param message
   *        The message to print.
   * @param printStream
   *        The stream.
   * @return Always <code>true</code>
   */
  public static boolean debugMsg(String message, PrintStream printStream) {
	printStream.println(message + "\n");
	return true;
  }

  /**
   * FIXME : Missing JavaDoc
   */
  private String exoeventType_;


  /*
   * (non-Javadoc)
   * @see osa.util.adl.exoevents.ExoeventAttributes#getExoeventType()
   */
  public String getExoeventType() {
	return exoeventType_;
  }


  /*
   * (non-Javadoc)
   * @see osa.util.adl.exoevents.ExoeventBuilder#setExoevent(java.lang.Object,
   * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
   * java.lang.String, java.lang.String, java.lang.Object)
   */
  public void setExoevent(final Object component, final String itf,
						  final String name, final String type,
						  final String time, final String method,
						  final String param,
						  @SuppressWarnings("unused") final Object context)
																		   throws Exception {

	assert FractalExoeventBuilder.debugMsg("ADL compiler: setExoevent('"
										   + name + "'," + itf + "." + method
										   + "(" + param + ")," + type + ","
										   + time + ")", System.err);

//    final Interface fcItf = (Interface) ((Component) component).getFcInterface(itf);
	final SimulationController sc = (SimulationController) ((Component) component).getFcInterface("simulation-controller");
	String[] result;
	if (param == null){
	  result = null;
	}
	else {
	  result = param.split(",");
	}
	sc.scheduleMyself(method, result, Long.parseLong(time));


  }


  /*
   * (non-Javadoc)
   * @see
   * osa.util.adl.exoevents.ExoeventAttributes#setExoeventType(java.lang.String
   * )
   */
  public void setExoeventType(String exoeventType) {
	exoeventType_ = exoeventType;
  }

}
