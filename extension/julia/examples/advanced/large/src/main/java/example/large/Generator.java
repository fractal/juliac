/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package example.large;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * <p>
 * This class generates a Fractal ADL .fractal file containing a pipe of
 * primitive components inserted into a composite component. The pipe composes
 * a primitive component (named first), a given number of intermediate primitive
 * components (named middle999), and a last primitive component (named last).
 * These components are bound together to form a pipe.
 * </p>
 *
 * <p>
 * The purpose of this class is to generate large assemblies of component to
 * assess the scalability of Juliac and its Fractal ADL backend.
 * </p>
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.4
 */
public class Generator {

	final static int COUNT = 1000;

	public static void main( String[] args ) throws IOException {

		FileWriter fw = new FileWriter("Large.fractal");
		PrintWriter pw = new PrintWriter(fw);
		generate(pw);
		pw.close();
		fw.close();

		fw = new FileWriter("target/classes/example/large/Large.fractal");
		pw = new PrintWriter(fw);
		generate(pw);
		pw.close();
		fw.close();
	}

	private static void generate( PrintWriter pw ) {

		pw.println("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>");
		pw.println("<!DOCTYPE definition PUBLIC \"-//objectweb.org//DTD Fractal ADL 2.0//EN\" \"classpath://osa/util/adl/stdsim.dtd\">");

		pw.println("<definition name=\"example.large.Large\">");

		pw.println("  <interface name=\"r\" role=\"server\" signature=\"java.lang.Runnable\" />");

		pw.println("  <component name=\"first\">");
		pw.println("    <interface name=\"r\" role=\"server\" signature=\"java.lang.Runnable\" />");
		pw.println("    <interface name=\"s\" role=\"client\" signature=\"example.large.Service\" />");
		pw.println("    <content class=\"example.large.FirstImpl\" />");
		pw.println("  </component>");

		for (int i = 0; i < COUNT; i++) {
			pw.println("  <component name=\"middle"+i+"\">");
			pw.println("    <interface name=\"r\" role=\"server\" signature=\"example.large.Service\" />");
			pw.println("    <interface name=\"s\" role=\"client\" signature=\"example.large.Service\" />");
			pw.println("    <content class=\"example.large.MiddleImpl\" />");
			pw.println("    <attributes signature=\"example.large.ServiceAttributes\">");
			pw.println("      <attribute name=\"count\" value=\""+i+"\" />");
			pw.println("    </attributes>");
//            pw.println("    <exoevents signature=\"user\">");
//            pw.println("      <exoevent name=\"user\" type=\"StartOfCall\" time=\"10\" method=\"start\" />");
//            pw.println("    </exoevents>");
			pw.println("  </component>");
		}

		pw.println("  <component name=\"last\">");
		pw.println("    <interface name=\"r\" role=\"server\" signature=\"example.large.Service\" />");
		pw.println("    <content class=\"example.large.LastImpl\" />");
		pw.println("    <attributes signature=\"example.large.ServiceAttributes\">");
		pw.println("      <attribute name=\"header\" value=\"-> \" />");
		pw.println("      <attribute name=\"count\" value=\"1\" />");
		pw.println("    </attributes>");
		pw.println("  </component>");

		pw.println("  <binding client=\"this.r\" server=\"first.r\" />");
		pw.println("  <binding client=\"first.s\" server=\"middle0.r\" />");
		for (int i = 0; i < COUNT-1; i++) {
			pw.println("  <binding client=\"middle"+i+".s\" server=\"middle"+(i+1)+".r\" />");
		}
		pw.println("  <binding client=\"middle"+(COUNT-1)+".s\" server=\"last.r\" />");

		pw.println("</definition>");
	}
}
