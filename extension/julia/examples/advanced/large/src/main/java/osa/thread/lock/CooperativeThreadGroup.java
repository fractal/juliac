package osa.thread.lock;

import java.lang.Thread.State;
import java.util.HashSet;
import java.util.concurrent.locks.Lock;

/**
 *
 * @author fabrice.peix@sophia.inria.fr
 */
public class CooperativeThreadGroup {
  /** The lock associated with this thread group. */
  private Lock                               lock_;

  /**
   * The threads belonging to this group.
   */
  private HashSet<AbstractCooperativeThread> threadSet_;


  /**
   * Initialize this thread group with the given lock.
   *
   * @param lock
   *        The lock associated with this thread group.
   */
  public CooperativeThreadGroup(final Lock lock) {
	lock_ = lock;
	threadSet_ = new HashSet<AbstractCooperativeThread>();
  }


  /**
   * Begin thread scheduling.
   *
   * @param firstThread
   *        The first thread that must be start.
   */
  public final void startThreads(final AbstractCooperativeThread firstThread) {
	for (AbstractCooperativeThread currentThread : threadSet_) {
	  if (currentThread.getState() != State.NEW) {
		throw new IllegalThreadStateException(
			  "The thread must not be started manually");
	  }
	}
	firstThread.start();
	lock_.lock();
	try {
	  firstThread.wakeUp();
	} finally {
	  lock_.unlock();
	}
  }


  /**
   * Return the lock associated with this thread group.
   *
   * @return The lock associated with this thread group.
   */
  final Lock getGroupLock() {
	return lock_;
  }


  /**
   * Remove a given thread from this group.
   *
   * @param thread
   *        The thread that must be removed from this group.
   */
  final void removeThread(final AbstractCooperativeThread thread) {
	if (!threadSet_.remove(thread)) {
	  throw new IllegalArgumentException(
				"The given thread not belong to this thread group");
	}
  }


  /**
   * Add a given thread to this group.
   *
   * @param thread
   *        The thread that must be add to this group.
   */
  final void addThread(final AbstractCooperativeThread thread) {
	if (!threadSet_.add(thread)) {
	  throw new IllegalArgumentException(
				"The given thread already belong to this thread group");
	}
  }

}
