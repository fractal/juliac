/**
 * Copyright INRIA Authors : Olivier Dalle (Olivier.Dalle-AT-sophia.inria.fr)
 * Judicael Ribault (Judicael.Ribault-AT-sophia.inria.fr) Cyrine Mrabet
 * (Cyrine.Mrabet-AT-sophia.inria.fr) Fabrice Peix
 * (Fabrice.Peix-AT-sophia.inria.fr) This software is part of the Open
 * Simulation Architecture (OSA). OSA is primarily intended to be a federating
 * platform for the simulation community: it is designed to favor the
 * integration of new or existing contributions at every level of its
 * architecture. This software is governed by the CeCILL-C license under French
 * law and abiding by the rules of distribution of free software. You can use,
 * modify and/ or redistribute the software under the terms of the CeCILL-C
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". As a counterpart to the access to the source code
 * and rights to copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the holder
 * of the economic rights, and the successive licensors have only limited
 * liability. In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or reproducing
 * the software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals having
 * in-depth computer knowledge. Users are therefore encouraged to load and test
 * the software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured and, more
 * generally, to use and operate it in the same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */
package osa.util.adl.impl.exoevents;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.components.PrimitiveCompiler;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.task.core.Task;
import org.objectweb.fractal.task.core.TaskMap;
import org.objectweb.fractal.task.deployment.lib.AbstractConfigurationTask;

import osa.util.adl.interfaces.exoevents.Exoevent;
import osa.util.adl.interfaces.exoevents.ExoeventBuilder;
import osa.util.adl.interfaces.exoevents.Exoevents;
import osa.util.adl.interfaces.exoevents.ExoeventsContainer;

/**
 * FIXME : Missing JavaDoc
 *
 * @author Judicael Ribault
 */
public class ExoeventCompiler implements PrimitiveCompiler, BindingController {
  static class ExoeventTask extends AbstractConfigurationTask {

	private final ExoeventBuilder _builder;

	private final String          itf_;

	private final String          name_;
	private final String          type_;
	private final String          time_;
	private final String          method_;
	private final String          param_;



	/**
	 * FIXME : documentation.
	 *
	 * @param builder
	 * @param signature
	 * @param name
	 * @param type
	 * @param time
	 * @param method
	 * @param param
	 */
	public ExoeventTask(final ExoeventBuilder builder, final String signature,
						final String name, final String type,
						final String time, final String method,
						final String param) {
	  super();
	  // TODO Auto-generated constructor stub
	  _builder = builder;
	  name_ = name;
	  type_ = type;
	  time_ = time;
	  itf_ = signature;
	  method_ = method;
	  param_ = param;
	}


	public void execute(Map<Object,Object> context) throws Exception {
	  final Object component = getInstanceProviderTask().getInstance();
	  _builder.setExoevent(component, itf_, name_, type_, time_, method_,
						   param_, context);
	}


	public Object getResult() {
	  return null;
	}


	public void setResult(Object result) {
	  // FIXME : Method non implementee
	}


	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
	  return "T" + System.identityHashCode(this) + "[ExoeventTask(" + name_
	  + ")]";
	}
  }

  /**
   * Name of the mandatory interface bound to the {@link ExoeventBuilder} used
   * by this compiler.
   */
  public final static String BUILDER_BINDING = "builder";

  private ExoeventBuilder    builder_;


  /*
   * (non-Javadoc)
   * @see org.objectweb.fractal.api.control.BindingController#bindFc(java.lang.
   * String, java.lang.Object)
   */
  public void bindFc(String clientItfName, Object serverItf)
  throws NoSuchInterfaceException,
  IllegalBindingException,
  IllegalLifeCycleException {
	if (ExoeventCompiler.BUILDER_BINDING.equals(clientItfName)) {
	  builder_ = (ExoeventBuilder) serverItf;
	}
  }


  /*
   * (non-Javadoc)
   * @see org.objectweb.fractal.api.control.BindingController#listFc()
   */
  public String[] listFc() {
	return new String[] {ExoeventCompiler.BUILDER_BINDING};
  }


  // --------------------------------------------------------------------------
  // Inner classes
  // --------------------------------------------------------------------------

  /*
   * (non-Javadoc)
   * @see org.objectweb.fractal.api.control.BindingController#lookupFc(java.lang
   * .String)
   */
  public Object lookupFc(String clientItfName) throws NoSuchInterfaceException {

	if (ExoeventCompiler.BUILDER_BINDING.equals(clientItfName)) {
	  return builder_;
	}
	return null;
  }


  /*
   * (non-Javadoc)
   * @see org.objectweb.fractal.api.control.BindingController#unbindFc(java.lang
   * .String)
   */
  public void unbindFc(String clientItfName) throws NoSuchInterfaceException,
  IllegalBindingException,
  IllegalLifeCycleException {
	if (ExoeventCompiler.BUILDER_BINDING.equals(clientItfName)) {
	  builder_ = null;
	}
  }

  // --------------------------------------------------------------------------
  // Implementation of the Compiler interface
  // --------------------------------------------------------------------------
  public void compile(List<ComponentContainer> arg0, ComponentContainer arg1,
					  TaskMap arg2, Map<Object, Object> arg3)
  throws ADLException {


	if (arg1 instanceof ExoeventsContainer) {
	  final Exoevents events = ((ExoeventsContainer) arg1).getExoevents();
	  if (events != null) {
		//        final InstanceProviderTask createTask = (InstanceProviderTask) tasks
		//                                                                            .getTask(
		//                                                                                     "create",
		//                                                                                     container);
		TaskMap.TaskHole createTaskHole = arg2.getTaskHole("create",
															arg1);

		//        final Task startTask = tasks.getTask("start", container);

		// TaskMap.TaskHole startTaskHole = tasks.getTaskHole("start",
		// container);

		final Exoevent[] evts = events.getExoevents();
		for (int i = 0; i < evts.length; ++i) {
		  try {
			// the task may already exist, in case of a shared
			// component
			arg2.getTask("event" + evts[i].getName(), arg1);
		  } catch (final NoSuchElementException e) {
			// System.out.println("catch1");
			final ExoeventTask t = new ExoeventTask(builder_,
													events.getSignature(),
													evts[i].getName(),
													evts[i].getType(),
													evts[i].getTime(),
													evts[i].getMethod(),
													evts[i].getParam());
			//            t.setInstanceProviderTask(createTask);
			//
			//            startTask.addPreviousTask(t);
			//
			//            tasks.addTask("event" + evts[i].getName(), container, t);

			//             TaskMap.TaskHole attributeTaskHole =
			arg2.addTask("event" + evts[i].getName(), arg1,
						  t);

			t.setInstanceProviderTask(createTaskHole);
			t.addDependency(createTaskHole,Task.PREVIOUS_TASK_ROLE,
							arg1);
		  }
		}
	  }
	}




  }

}
