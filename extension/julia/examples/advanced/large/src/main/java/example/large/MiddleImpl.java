package example.large;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;

public class MiddleImpl implements Service , BindingController, ServiceAttributes {

	public MiddleImpl() {
	}

	private Service s;

	public void bindFc(String clientItfName, Object serverItf) throws NoSuchInterfaceException, IllegalBindingException {
		if (clientItfName.equals("s")) {
			if (!(Service.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (Service.class.getName())));
			}
			s = ((Service)(serverItf));
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	public String[] listFc() {
		List<String> interfaces = new ArrayList<String>();
		interfaces.add("s");
		return interfaces.toArray(new String[interfaces.size()]);
	}

	public Object lookupFc(String clientItfName) throws NoSuchInterfaceException {
		if (clientItfName.equals("s")) {
			return s;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	public void unbindFc(String clientItfName) throws NoSuchInterfaceException {
		if (clientItfName.equals("s")) {
			s = null;
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	public void print(String msg) {
		Console.println("print in MIDDLE"+getCount());
		s.print(msg);
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(final String header) {
		this.header = header;
	}

	public int getCount() {
		return count;
	}

	public void setCount(final int count) {
		this.count = count;
	}

	private String header;
	private int count;
}
