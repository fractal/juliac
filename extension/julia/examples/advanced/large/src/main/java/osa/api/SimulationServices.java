package osa.api;

/**
 * The simulation interface. Define all services that simulation
 * component provides.
 *
 * @author Judicael Ribault
 */
public interface SimulationServices {

  /**
   * Give the current simulation currentTime_.
   *
   * @return The current simulation currentTime_.
   */
  long getSimulationTime();


  /**
   * Schedule a new method call at a given currentTime_.
   *
   * @param methodName
   *        The method that must be called.
   * @param parameters
   *        The method's parameters.
   * @param time
   *        The simulation currentTime_ at which the thread must be
   *        created.
   */
  void scheduleMyself(String methodName, Object[] parameters, long time);
}
