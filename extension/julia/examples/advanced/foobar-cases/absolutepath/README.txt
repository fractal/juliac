Test whether Juliac works correctly when invoked with absolute paths. This
feature has been introduced in Juliac 2.1.

Before running the example, open and edit the assembly/pom.xml file and set the
right value for property target.dir on line 17 depending on whether you are
running under Unix or Windows.

To compile and run the example:
	mvn install

Note: mvn install is really needed to run the example (i.e. mvn test is not
sufficient.) The reason is that the dependency plugin in the assembly module
unpacks the source artifact of the code module. If this artifact is not
available in the local repository, the dependency plugin fails.