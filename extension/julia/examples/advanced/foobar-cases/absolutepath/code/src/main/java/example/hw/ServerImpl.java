package example.hw;


public class ServerImpl implements Service , ServiceAttributes {
	private String header;

	private int count;

	public ServerImpl() {
		Console.println("SERVER created");
	}

	public void print(final String msg) {
		Console.println("Server: begin printing...");
		for (int i = 0 ; i < (count) ; ++i) {
			Console.println(((header) + msg));
		}
		Console.println("Server: print done.");
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(final String header) {
		this.header = header;
	}

	public int getCount() {
		return count;
	}

	public void setCount(final int count) {
		this.count = count;
	}
}
