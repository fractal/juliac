Test various issues related with handling parameters when generating proxy
classes.

* inner types: check that inner types are generated as outter.inner (which is
  the source code syntax) and not as outter$inner (which the binary form) which
  was the case prior to Juliac 2.0.1.

* generic types: check that generic types are taken into account when generating
  proxy classes which was not the case prior to Juliac 2.1.
