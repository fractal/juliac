/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package example.proxyparameters.membrane;

import java.lang.reflect.Method;

import org.objectweb.fractal.juliac.core.proxy.SimpleSourceCodeGenerator;
import org.objectweb.fractal.juliac.core.proxy.SimpleSourceCodeGeneratorInterceptionType;
import org.objectweb.fractal.juliac.core.proxy.SimpleSourceCodeGeneratorMode;

/**
 * This class generates the source code of component interceptors for the stat
 * controller. This controller counts incoming and outgoing operation
 * invocations. This class is adapted from {@link
 * org.objectweb.fractal.julia.conform.controllers.StatCodeGenerator}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2.4
 */
public class StatSourceCodeGenerator extends SimpleSourceCodeGenerator {

	@Override
	public boolean match() {

		// Skip the stat-controller control interfaces
		String itname = it.getFcItfName();
		if( itname.equals("stat-controller") ) {
			return false;
		}

		// Match all other interfaces (including control ones)
		return true;
	}

	public SimpleSourceCodeGeneratorMode getMode() {
		return SimpleSourceCodeGeneratorMode.IN_OUT;
	}

	protected String getControllerInterfaceName() {
		return "stat-controller";
	}

	protected SimpleSourceCodeGeneratorInterceptionType getInterceptionType(Method m) {
		return SimpleSourceCodeGeneratorInterceptionType.EMPTY;
	}

	protected String getPreMethodName() {
		return "incrementFcCounter";
	}

	protected String getPostMethodName() {
		return null;
	}

	protected Class<?> getContextType() {
		return Void.TYPE;
	}

	protected String getMethodName(final Method m) {
		return m.getName();
	}

	public String getClassNameSuffix() {
		return "Stat2";
	}
}
