package example.proxyparameters;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface I< W extends Runnable & Comparable<? super W>,
					K extends Serializable,
					Z >
extends Sub<String> {

	public void foo1( Param.Inner pi );
	public byte[] foo2( Object o ) throws Exception, NullPointerException;
	public void foo3() throws Throwable;
	public float foo4();

	public <B,
			R extends J<B>,
			E extends Throwable,
			V extends Serializable & Comparable<V>,
			T extends Comparable<? super T>
		   >
		R cast( B target, V[] param ) throws E, Throwable;
	public <B> B[] cast( Collection<B> c, List<Integer> l, Set<?> s );
	public <B, R extends J<B>> R cast( B target ) throws IllegalArgumentException;

	public void foo();
	public Object[] bar( J.Inner ji );

	public ArrayList<?> sub1();
}
