package example.comanche;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.NoSuchInterfaceException;

public class BasicLogger implements Logger {

	public void log(String msg) {
		System.out.println(msg);
	}

	public void bindFc(String clientItfName, Object serverItf) throws NoSuchInterfaceException {
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	public String[] listFc() {
		List<String> interfaces = new ArrayList<String>();
		return interfaces.toArray(new String[interfaces.size()]);
	}

	public Object lookupFc(String clientItfName) throws NoSuchInterfaceException {
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	public void unbindFc(String clientItfName) throws NoSuchInterfaceException {
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

}
