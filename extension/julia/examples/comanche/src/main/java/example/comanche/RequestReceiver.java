package example.comanche;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;

public class RequestReceiver implements Runnable , BindingController {

	private Scheduler s;

	private RequestHandler rh;

	public void run() {
		try {
			ServerSocket ss = new ServerSocket(8042);
			System.out.println("Comanche HTTP Server ready on port 8042.");
			System.out.println("Load http://localhost:8042/src/main/resources/example/comanche/gnu.jpg");
			while (true) {
				final Socket socket = ss.accept();
				s.schedule(new Runnable() {
					public void run() {
						try {
							rh.handleRequest(new Request(socket));
						} catch (IOException e) {
						}
					}

				});
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void bindFc(String clientItfName, Object serverItf) throws NoSuchInterfaceException, IllegalBindingException {
		if (clientItfName.equals("s")) {
			if (!(Scheduler.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (Scheduler.class.getName())));
			}
			s = ((Scheduler)(serverItf));
			return ;
		}
		if (clientItfName.equals("rh")) {
			if (!(RequestHandler.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (RequestHandler.class.getName())));
			}
			rh = ((RequestHandler)(serverItf));
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	public String[] listFc() {
		List<String> interfaces = new ArrayList<String>();
		interfaces.add("s");
		interfaces.add("rh");
		return interfaces.toArray(new String[interfaces.size()]);
	}

	public Object lookupFc(String clientItfName) throws NoSuchInterfaceException {
		if (clientItfName.equals("s")) {
			return s;
		}
		if (clientItfName.equals("rh")) {
			return rh;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	public void unbindFc(String clientItfName) throws NoSuchInterfaceException {
		if (clientItfName.equals("s")) {
			s = null;
			return ;
		}
		if (clientItfName.equals("rh")) {
			rh = null;
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

}
