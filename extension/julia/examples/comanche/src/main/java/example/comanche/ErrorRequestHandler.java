package example.comanche;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.NoSuchInterfaceException;

public class ErrorRequestHandler implements RequestHandler {
	public void handleRequest(Request r) {
		r.out.print("HTTP/1.0 404 Not Found\n\n");
		r.out.print("<html>Document not found.</html>");
	}

	public void bindFc(String clientItfName, Object serverItf) throws NoSuchInterfaceException {
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	public String[] listFc() {
		List<String> interfaces = new ArrayList<String>();
		return ((String[])(interfaces.toArray(new String[]{  })));
	}

	public Object lookupFc(String clientItfName) throws NoSuchInterfaceException {
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	public void unbindFc(String clientItfName) throws NoSuchInterfaceException {
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

}
