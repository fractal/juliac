package example.comanche;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;

public class RequestDispatcher implements RequestHandler , BindingController {

	private Map<String,RequestHandler> handlers = new TreeMap<String,RequestHandler>();

	public void handleRequest(Request r) {
		for (RequestHandler h : handlers.values()) {
			try {
				h.handleRequest(r);
				return ;
			} catch (IOException e) {
			}
		}
	}

	public void bindFc(String clientItfName, Object serverItf) throws NoSuchInterfaceException, IllegalBindingException {
		if (clientItfName.startsWith("h")) {
			if (!(RequestHandler.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (RequestHandler.class.getName())));
			}
			handlers.put(clientItfName, (RequestHandler) serverItf);
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	public String[] listFc() {
		List<String> interfaces = new ArrayList<String>();
		interfaces.addAll(handlers.keySet());
		return interfaces.toArray(new String[interfaces.size()]);
	}

	public Object lookupFc(String clientItfName) throws NoSuchInterfaceException {
		if (clientItfName.startsWith(clientItfName)) {
			return handlers.get(clientItfName);
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	public void unbindFc(String clientItfName) throws NoSuchInterfaceException {
		if (handlers.containsKey(clientItfName)) {
			handlers.remove(clientItfName);
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

}
