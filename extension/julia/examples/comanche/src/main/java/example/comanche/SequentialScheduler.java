package example.comanche;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.NoSuchInterfaceException;

public class SequentialScheduler implements Scheduler {
	public void schedule(Runnable task) {
		task.run();
	}

	public void bindFc(String clientItfName, Object serverItf) throws NoSuchInterfaceException {
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	public String[] listFc() {
		List<String> __interfaces__ = new ArrayList<String>();
		return ((String[])(__interfaces__.toArray(new String[]{  })));
	}

	public Object lookupFc(String clientItfName) throws NoSuchInterfaceException {
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	public void unbindFc(String clientItfName) throws NoSuchInterfaceException {
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

}
