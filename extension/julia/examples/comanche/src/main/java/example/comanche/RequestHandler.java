package example.comanche;

import java.io.IOException;

public interface RequestHandler {
	void handleRequest(Request r) throws IOException;
}
