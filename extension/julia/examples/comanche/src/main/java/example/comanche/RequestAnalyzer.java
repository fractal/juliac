package example.comanche;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;

public class RequestAnalyzer implements RequestHandler , BindingController {

	private RequestHandler rh;

	private Logger l;

	public void handleRequest(Request r) throws IOException {
		r.in = new InputStreamReader(r.s.getInputStream());
		r.out = new PrintStream(r.s.getOutputStream());
		String rq = new LineNumberReader(r.in).readLine();
		l.log(rq);
		if (rq.startsWith("GET ")) {
			r.url = rq.substring(5 ,rq.indexOf(' ' ,4));
			rh.handleRequest(r);
		}
		r.out.close();
		r.s.close();
	}

	public void bindFc(String clientItfName, Object serverItf) throws NoSuchInterfaceException, IllegalBindingException {
		if (clientItfName.equals("a")) {
			if (!(RequestHandler.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (RequestHandler.class.getName())));
			}
			rh = ((RequestHandler)(serverItf));
			return ;
		}
		if (clientItfName.equals("l")) {
			if (!(Logger.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (Logger.class.getName())));
			}
			l = ((Logger)(serverItf));
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	public String[] listFc() {
		List<String> interfaces = new ArrayList<String>();
		interfaces.add("a");
		interfaces.add("l");
		return interfaces.toArray(new String[interfaces.size()]);
	}

	public Object lookupFc(String clientItfName) throws NoSuchInterfaceException {
		if (clientItfName.equals("a")) {
			return rh;
		}
		if (clientItfName.equals("l")) {
			return l;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	public void unbindFc(String clientItfName) throws NoSuchInterfaceException {
		if (clientItfName.equals("a")) {
			rh = null;
			return ;
		}
		if (clientItfName.equals("l")) {
			l = null;
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

}
