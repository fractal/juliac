package example.hw;

import org.objectweb.fractal.juliac.commons.io.Console;

public class ServerImpl implements Service , ServiceAttributes {

	private String header;
	private int count;

	private Console console;

	public ServerImpl() {
		console = Console.getConsole("hw-");
		console.println("SERVER created");
	}

	public void print(final String msg) {
		console.println("Server: begin printing...");
		for (int i = 0 ; i < (count) ; ++i) {
			console.println(((header) + msg));
		}
		console.println("Server: print done.");
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(final String header) {
		this.header = header;
	}

	public int getCount() {
		return count;
	}

	public void setCount(final int count) {
		this.count = count;
	}
}
