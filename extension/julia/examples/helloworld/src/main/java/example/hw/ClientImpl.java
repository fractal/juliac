package example.hw;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.juliac.commons.io.Console;

public class ClientImpl implements Runnable , BindingController {

	private Console console;

	public ClientImpl() {
		console = Console.getConsole("hw-");
		console.println("CLIENT created");
	}

	public void run() {
		s.print("hello world");
	}

	private Service s;

	public void bindFc(String clientItfName, Object serverItf) throws NoSuchInterfaceException, IllegalBindingException {
		if (clientItfName.equals("s")) {
			if (!(Service.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(((("server interfaces connected to " + clientItfName) + " must be instances of ") + (Service.class.getName())));
			}
			s = ((Service)(serverItf));
			return ;
		}
	}

	public String[] listFc() {
		List<String> interfaces = new ArrayList<String>();
		interfaces.add("s");
		return interfaces.toArray(new String[interfaces.size()]);
	}

	public Object lookupFc(String clientItfName) throws NoSuchInterfaceException {
		if (clientItfName.equals("s")) {
			return s;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	public void unbindFc(String clientItfName) throws NoSuchInterfaceException {
		if (clientItfName.equals("s")) {
			s = null;
			return ;
		}
		throw new NoSuchInterfaceException((("Client interface \'" + clientItfName) + "\' is undefined."));
	}

}
