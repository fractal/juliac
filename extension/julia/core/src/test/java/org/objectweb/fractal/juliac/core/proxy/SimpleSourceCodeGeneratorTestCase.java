/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.proxy;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;

import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.generator.InterceptorSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.visit.FileSourceCodeVisitor;
import org.objectweb.fractal.juliac.commons.io.Console;
import org.objectweb.fractal.juliac.core.Constants;
import org.objectweb.fractal.juliac.core.Juliac;
import org.objectweb.fractal.juliac.core.proxy.SimpleSourceCodeGenerator;
import org.objectweb.fractal.juliac.core.proxy.SimpleSourceCodeGeneratorInterceptionType;
import org.objectweb.fractal.juliac.core.proxy.SimpleSourceCodeGeneratorMode;
import org.objectweb.fractal.juliac.core.visit.FileSourceCodeWriter;

/**
 * Class for testing the functionalities of the {@link
 * SimpleSourceCodeGenerator} class.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.6
 */
public class SimpleSourceCodeGeneratorTestCase {

	@Test
	public void testNormalMode() throws IOException {

		JuliacItf jc = new Juliac();
		final String pkgRoot = jc.getPkgRoot();
		InterfaceType it =
			new BasicInterfaceType(
				"r",Runnable.class.getName(),false,false,false);

		SimpleSourceCodeGenerator sscg =
			new NormalModeSourceCodeInterceptor();
		sscg.setInterfaceType(it,Runnable.class,pkgRoot);
		sscg.setMergeable(true);  // To skip checking

		InterceptorSourceCodeGeneratorItf[] iscgs =
			new InterceptorSourceCodeGeneratorItf[]{sscg};

		CharArrayWriter caw = new CharArrayWriter();
		PrintWriter pw = new PrintWriter(caw);
		FileSourceCodeVisitor fv = new FileSourceCodeWriter(pw);
		SourceCodeGeneratorItf cg =
			new NoHeaderInterceptorClassGenerator(
				iscgs,it,Runnable.class,pkgRoot,null,true);
		cg.generate(fv);
		pw.close();
		caw.close();

		Console console = Console.getConsole("simple-source-code-generator-");
		char[] cars = caw.toCharArray();
		for (char c : cars) {
			console.print(c);
		}
		console.close();
		console.assertEquals(expecteds);

		jc.close();
	}

	final private static String[] expecteds =
		new String[]{
			"",
			"package "+Constants.JULIAC_RUNTIME_GENERATED+".java.lang;",
			"",
			"abstract class RunnableInterceptorNormal",
			"implements java.lang.Runnable {",
			"",
			"  public abstract void _super_run()  ;",
			"  public void run()  {",
			"    _this_pre(\"m\");",
			"    this._super_run();",
			"    _this_post(\"m\");",
			"  }",
			"",
			"  public abstract void _this_pre(java.lang.String arg0)  ;",
			"  public abstract void _this_post(java.lang.String arg0)  ;",
			"}",
		};

	private static class NormalModeSourceCodeInterceptor
	extends SimpleSourceCodeGenerator {

		public NormalModeSourceCodeInterceptor() {
			super();
		}

		@Override
		protected Class<?> getContextType() {
			return void.class;
		}

		@Override
		protected String getControllerInterfaceName() {
			return "controller";
		}

		@Override
		protected SimpleSourceCodeGeneratorInterceptionType
			getInterceptionType( Method m ) {
			return SimpleSourceCodeGeneratorInterceptionType.NORMAL;
		}

		@Override
		protected String getMethodName( Method m ) {
			return "m";
		}

		@Override
		protected SimpleSourceCodeGeneratorMode getMode() {
			return SimpleSourceCodeGeneratorMode.IN_OUT;
		}

		@Override
	   protected String getPostMethodName() {
			return "post";
		}

		@Override
		protected String getPreMethodName() {
			return "pre";
		}

		@Override
		public String getClassNameSuffix() {
			return "Normal";
		}
	}
}
