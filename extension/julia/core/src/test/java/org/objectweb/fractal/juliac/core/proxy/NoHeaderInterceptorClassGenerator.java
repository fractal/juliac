/***
 * Juliac
 * Copyright (C) 2010-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.proxy;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.InterceptorSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.visit.FileSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.proxy.InterceptorClassGenerator;

/**
 * Class for facilitating tests with {@link InterceptorClassGenerator}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.6
 */
public class NoHeaderInterceptorClassGenerator
extends InterceptorClassGenerator {

	public NoHeaderInterceptorClassGenerator(
		InterceptorSourceCodeGeneratorItf[] iscgs,
		InterfaceType it, Class<?> proxycl, String pkgRoot,
		MembraneDesc<?> membraneDesc, boolean mergeable ) {

		super(iscgs,it,proxycl,pkgRoot,membraneDesc,mergeable);
	}

	@Override
	public void generateFileHeaders( FileSourceCodeVisitor fv ) {
		// Skip the generation of the date
	}
}
