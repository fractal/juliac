/***
 * Juliac
 * Copyright (C) 2010-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.proxy;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.generator.InterceptorSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.visit.FileSourceCodeVisitor;
import org.objectweb.fractal.juliac.commons.io.Console;
import org.objectweb.fractal.juliac.core.Constants;
import org.objectweb.fractal.juliac.core.Juliac;
import org.objectweb.fractal.juliac.core.proxy.InterceptorClassGenerator;
import org.objectweb.fractal.juliac.core.visit.FileSourceCodeWriter;

/**
 * Class for testing the functionalities of the {@link
 * InterceptorClassGenerator} class.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2.5
 */
public class InterceptorClassGeneratorTestCase {

	private JuliacItf jc;

	@BeforeEach
	public void setUp() {
		jc = new Juliac();
	}

	@AfterEach
	public void tearDown() {
		jc.close();
	}

	/**
	 * Test that only interface types (not class types) are accepted for proxy
	 * generation.
	 */
	@Test
	public void testOnlyInterfaceTypes() {

		final String pkgRoot = jc.getPkgRoot();
		InterfaceType it =
			new BasicInterfaceType("r",Void.class.getName(),false,false,false);

		InterceptorSourceCodeGeneratorItf[] iscgs =
			new InterceptorSourceCodeGeneratorItf[]{};
		assertThrows(
			IllegalArgumentException.class,
			() -> new NoHeaderInterceptorClassGenerator(iscgs,it,Void.class,pkgRoot,null,false));
	}

	/**
	 * Test the interceptor class generators generate some correct code even if
	 * no source code generator is provided.
	 */
	@Test
	public void testNoSourceCodeGenerator() throws IOException {

		InterfaceType it =
			new BasicInterfaceType(
				"r",Runnable.class.getName(),false,false,false);

		InterceptorSourceCodeGeneratorItf[] iscgs =
			new InterceptorSourceCodeGeneratorItf[]{};

		CharArrayWriter caw = new CharArrayWriter();
		PrintWriter pw = new PrintWriter(caw);
		FileSourceCodeVisitor fv = new FileSourceCodeWriter(pw);
		String pkgRoot = jc.getPkgRoot();
		SourceCodeGeneratorItf cg =
			new NoHeaderInterceptorClassGenerator(
				iscgs,it,Runnable.class,pkgRoot,null,false);
		cg.generate(fv);
		pw.close();
		caw.close();

		Console console = Console.getConsole("interceptor-class-generator-");
		char[] cars = caw.toCharArray();
		for (char c : cars) {
			console.print(c);
		}
		console.close();
		console.assertEquals(expecteds);
	}

	final private static String[] expecteds =
		new String[]{
			"",
			"package "+Constants.JULIAC_RUNTIME_GENERATED+".java.lang;",
			"",
			" class RunnableInterceptor",
			"implements org.objectweb.fractal.julia.Interceptor {",
			"",
			"  private java.lang.Runnable _impl;",
			"  public RunnableInterceptor()  {",
			"  }",
			"",
			"  private RunnableInterceptor(Object obj)  {",
			"    setFcItfDelegate(obj);",
			"  }",
			"",
			"  public void run()  {",
			"    _impl.run();",
			"  }",
			"",
			"  public void initFcController(org.objectweb.fractal.julia.InitializationContext ic) throws org.objectweb.fractal.api.factory.InstantiationException  {",
			"  }",
			"",
			"  public Object clone()  {",
			"    RunnableInterceptor clone = new RunnableInterceptor(getFcItfDelegate());",
			"    return clone;",
			"  }",
			"",
			"  public Object getFcItfDelegate()  {",
			"    return _impl;",
			"  }",
			"",
			"  public void setFcItfDelegate(Object obj)  {",
			"    _impl = (java.lang.Runnable)obj;",
			"  }",
			"",
			"}",
		};
}
