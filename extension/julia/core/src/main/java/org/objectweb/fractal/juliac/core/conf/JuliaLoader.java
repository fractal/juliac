/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.conf;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.loader.Tree;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.NoSuchControllerDescriptorException;
import org.objectweb.fractal.juliac.core.desc.ControllerDesc;

/**
 * A loader for membrane configuration data. This loader uses {@link
 * CfgLoader} to retrieve data from a <code>.cfg</code> file.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class JuliaLoader implements MembraneLoaderItf {

	private Iterable<CfgLoaderItf> cfgloaders;

	// -----------------------------------------------------------------------
	// Implementation of the JuliacModuleItf interface
	// -----------------------------------------------------------------------

	@Override
	public void init( JuliacItf jc ) throws IOException {
		cfgloaders = jc.lookup(CfgLoaderItf.class);
		jc.register(MembraneLoaderItf.class,this);
	}

	@Override
	public void close( JuliacItf jc ) throws IOException {
		jc.unregister(MembraneLoaderItf.class,this);
	}


	// -----------------------------------------------------------------------
	// Implementation of the MembraneLoaderItf interface
	// -----------------------------------------------------------------------

	@Override
	public Object put( String ctrlDesc, Object cl ) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Object get( String ctrlDesc ) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsKey( String ctrlDesc ) {
		for (CfgLoaderItf cfgloader : cfgloaders) {
			if( cfgloader.containsKey(ctrlDesc) ) {
				return true;
			}
		}
		return false;
	}

	@Override
	public InterfaceType[] getMembraneType( ComponentType ct, String ctrlDesc ) {

		Map<String,Tree> context = getJuliaContextInfo(ct);
		Tree tree = getTree(ctrlDesc,context);

		Tree membraneItfTree = tree.getSubTree(1);
		Tree[] ctrlItfTrees = membraneItfTree.getSubTrees();

		InterfaceType[] its = new InterfaceType[ ctrlItfTrees.length ];
		for (int i = 0; i < its.length; i++) {
			String name = ctrlItfTrees[i].getSubTree(0).toString();
			String signature = ctrlItfTrees[i].getSubTree(1).toString();
			its[i] = new BasicInterfaceType(name,signature,false,false,false);
		}
		return its;
	}

	@Override
	public List<ControllerDesc> getCtrlImplLayers(
		ComponentType ct, String ctrlDesc ) {

		Map<String,Tree> context = getJuliaContextInfo(ct);
		Tree[] ctrlTrees = getCtrlImplTrees(ctrlDesc,context);
		List<ControllerDesc> cdescs = new ArrayList<>();

		for (Tree ctrlTree : ctrlTrees) {

			ControllerDesc cdesc = null;
			if( ctrlTree.getSize() == 0 ) {
				/*
				 * Special case where the name of the controller implementation
				 * is directly specified (not generated with
				 * MixinClassGenerator).
				 */
				String name = ctrlTree.toString();
				cdesc = new ControllerDesc(name,null,null);
			}
			else {
				Tree t = ctrlTree.getSubTree(0);
				Tree[] ts = t.getSubTrees();

				if( ts.length < 2 ) {
					/*
					 * Same case as when ctrlTree.getSize() == 0
					 */
					String name = t.toString();
					cdesc = new ControllerDesc(name,null,null);
				}
				else {
					/*
					 * Skip
					 * [0]: org.objectweb.fractal.julia.asm.MixinClassGenerator
					 * [1]: class name (e.g. NameControllerImpl)
					 */
					String name = ts[1].toString();
					List<String> layers = new ArrayList<>();
					for (int j = 2; j < ts.length; j++) {
						String s = ts[j].toString();
						layers.add(s);
					}
					cdesc = new ControllerDesc(name,layers,ctrlTree);
				}
			}
			cdescs.add(cdesc);
		}

		return cdescs;
	}

	/**
	 * @since 2.2.6
	 */
	@Override
	public String getInterceptorClassGeneratorName( String ctrlDesc ) {

		/*
		 * Mock the contextual information for the attribute-controller.
		 * We don't need it for retrieving the interceptor class generator name
		 * but we mock it in order to prevent the evaluation of the tree to
		 * fail.
		 */
		Map<String,Tree> context = new HashMap<>();
		Tree mockTree =
			new Tree(
				new Tree[]{
					new Tree("attribute-controller"),
					new Tree(Serializable.class.getName())});
		context.put("attributeControllerInterface", mockTree);

		Tree tree = getTree(ctrlDesc,context);

		Tree interceptTree = tree.getSubTree(3);
		if( interceptTree.getSize() == 0 ) {
			return null;
		}

		Tree t0 = interceptTree.getSubTree(0);
		Tree tt0 = t0.getSubTree(0);
		String name = tt0.toString();

		return name;
	}

	@Override
	public String[] getInterceptorSourceCodeGeneratorNames( String ctrlDesc ) {

		/*
		 * Mock the contextual information for the attribute-controller.
		 * We don't need it for retrieving the interceptor class generator name
		 * but we mock it in order to prevent the evaluation of the tree to
		 * fail.
		 */
		Map<String,Tree> context = new HashMap<>();
		Tree mockTree =
			new Tree(
				new Tree[]{
					new Tree("attribute-controller"),
					new Tree(Serializable.class.getName())});
		context.put("attributeControllerInterface", mockTree);

		Tree tree = getTree(ctrlDesc,context);

		Tree interceptTree = tree.getSubTree(3);
		if( interceptTree.getSize() == 0 ) {
			return null;
		}

		Tree t0 = interceptTree.getSubTree(0);

		// [0]: org.objectweb.fractal.julia.asm.InterceptorClassGenerator
		String[] names = new String[ t0.getSize() - 1 ];
		for (int i = 0; i < names.length; i++) {
			Tree tt = t0.getSubTree(i+1);
			names[i] = tt.toString();
		}

		return names;
	}


	// ----------------------------------------------------------------------
	// Implementation specific
	// ----------------------------------------------------------------------

	/**
	 * Return the array of {@link Tree} instances associated to the definition
	 * of controller implementations for the specificied controller descriptor.
	 *
	 * @param ctrlDesc  the control membrane descriptor
	 * @param context   contextual information
	 * @throws NoSuchControllerDescriptorException
	 *         if the specified controller descriptor does not exist
	 */
	private Tree[] getCtrlImplTrees( String ctrlDesc, Map<String,Tree> context ) {

		Tree tree = getTree(ctrlDesc,context);

		/*
		 * [0]: 'interface-class-generator
		 * [1]: control interface declarations tree
		 */
		Tree membraneTree = tree.getSubTree(2);
		Tree[] ctrlTrees = membraneTree.getSubTrees();

		return ctrlTrees;
	}

	/**
	 * Return the Julia contextual information associated with the specified
	 * component type. This contextual information consists in the signature of
	 * the attribute control interface, if there is one. This information is
	 * used when evaluating a Julia configuration tree containing quoted
	 * elements such as <code>'attributeControllerInterface</code>.
	 *
	 * @param ct  the component type
	 * @return    the associated contextual map
	 */
	private static Map<String,Tree> getJuliaContextInfo( ComponentType ct ) {

		Map<String,Tree> context = new HashMap<>();
		try {
			InterfaceType it = ct.getFcInterfaceType("attribute-controller");
			String signature = it.getFcItfSignature();
			Tree tree =
				new Tree(
					new Tree[]{
						new Tree("attribute-controller"),
						new Tree(signature)});
			context.put("attributeControllerInterface", tree);
		}
		catch (NoSuchInterfaceException e) {
			/*
			 * Empty contextual information if there is no attribute-controller
			 * interface.
			 */
		}

		return context;
	}

	private Tree getTree( String ctrlDesc, Map<?,?> context ) {

		Tree tree = null;
		for (CfgLoaderItf cfgloader : cfgloaders) {
			try {
				Tree t = cfgloader.loadTree(ctrlDesc);
				tree = cfgloader.evalTree(t,context);
			}
			catch( NoSuchControllerDescriptorException |
				   IllegalArgumentException e ) {}
		}

		if( tree == null ) {
			throw new NoSuchControllerDescriptorException(ctrlDesc);
		}

		return tree;
	}
}
