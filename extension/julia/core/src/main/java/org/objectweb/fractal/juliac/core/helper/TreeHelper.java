/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.helper;

import org.objectweb.fractal.julia.loader.Tree;

/**
 * This class provides helper methods for the {@link Tree} class.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class TreeHelper {

	/**
	 * Return a string containing the Java code to create a {@link Tree}
	 * similar to the specified parameter.
	 */
	public static StringBuilder javaify( Tree tree ) {

		StringBuilder sb = new StringBuilder();

		sb.append("new "+Tree.class.getName()+'(');
		if( tree.getSize() == 0 ) {
			sb.append('"'+tree.toString()+'"');
		}
		else {
			sb.append("new "+Tree.class.getName()+"[]{");
			Tree[] subtrees = tree.getSubTrees();
			boolean first = true;
			for (Tree subtree : subtrees) {
				if(first) {first=false;} else {sb.append(',');}
				StringBuilder s = javaify(subtree);
				sb.append(s);
			}
			sb.append('}');
		}
		sb.append(')');

		return sb;
	}
}
