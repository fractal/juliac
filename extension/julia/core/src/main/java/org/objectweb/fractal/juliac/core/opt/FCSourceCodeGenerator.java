/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.opt;

import java.io.IOException;

import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.control.attribute.CloneableAttributeController;
import org.objectweb.fractal.julia.loader.BasicLoader;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.InterceptorSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.generator.InterfaceTypeConfigurableItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.Juliac;
import org.objectweb.fractal.juliac.core.conf.CfgLoader;
import org.objectweb.fractal.juliac.core.conf.CfgLoaderItf;
import org.objectweb.fractal.juliac.core.conf.JuliaLoader;
import org.objectweb.fractal.juliac.core.conf.MembraneLoaderItf;
import org.objectweb.fractal.juliac.core.helper.InterfaceTypeHelper;
import org.objectweb.fractal.juliac.core.helper.JuliacHelper;
import org.objectweb.fractal.juliac.core.proxy.AttributeControllerClassGenerator;
import org.objectweb.fractal.juliac.core.proxy.InterceptorClassGenerator;
import org.objectweb.fractal.juliac.core.proxy.InterfaceImplementationClassGenerator;

/**
 * Default root class for source code generators associated to a given
 * optimization level.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.2
 */
public abstract class FCSourceCodeGenerator
implements FCSourceCodeGeneratorItf {

	/** @since 2.7 */
	public static final String DEFAULT_CONFIGURATION = "juliac-bundled.cfg";

	// -----------------------------------------------------------------------
	// Implementation of the JuliacModuleItf interface
	// -----------------------------------------------------------------------

	protected JuliacItf jc;
	protected CfgLoaderItf jloader;
	protected MembraneLoaderItf mloader;

	@Override
	public void init( JuliacItf jc ) throws IOException {

		this.jc = jc;

		// Post initialization phase
		postInit();

		// Register the services
		jc.register(FCSourceCodeGeneratorItf.class,this);
	}

	/**
	 * This post initialization phase is kept separated from the {@link
	 * #init()} method in order to be able to override in subclasses the
	 * statements defined in this method.
	 *
	 * @since 2.4
	 */
	protected void postInit() throws IOException {

		// Initialize the membrane loader
		jloader = new CfgLoader();
		mloader = new JuliaLoader();
		jloader.init(jc);
		mloader.init(jc);

		// Load the default Julia and Juliac membrane definitions
		jloader.loadConfigFile(BasicLoader.DEFAULT_CONFIGURATION);
		jloader.loadConfigFile(DEFAULT_CONFIGURATION);

		// Post initialization phase for the JuliaLoader module
		postInitJuliaLoaderModule();
	}

	/**
	 * This post initialization phase is kept separated from the {@link
	 * #init(Juliac)} method in order to be able to customize in subclasses the
	 * <code>julia.config system</code> property used by this method.
	 *
	 * @since 2.5
	 */
	protected void postInitJuliaLoaderModule() throws IOException {

		/*
		 * Load the Julia configuration files.
		 */
		String prop = jc.getJuliaCfgFiles();
		if( prop!=null && prop.length()!=0 ) {
			String[] filenames = prop.split(",");
			for (String filename : filenames) {
				jloader.loadConfigFile(filename);
			}
		}
	}

	@Override
	public void close( JuliacItf jc ) throws IOException {
		// IOException thrown in subclasses (e.g. osgi/opt/oo)
		jc.unregister(FCSourceCodeGeneratorItf.class,this);
	}


	// -----------------------------------------------------------------------
	// Implementation of the FCSourceCodeGeneratorItf interface
	// -----------------------------------------------------------------------

	@Override
	public boolean test( Object controllerDesc ) {

		if( ! (controllerDesc instanceof String) ) {
			return false;
		}
		String ctrlDesc = (String) controllerDesc;

		/*
		 * Handle the prefix which may be specified in front of the controller
		 * descriptor.
		 */
		String prefix = JuliacHelper.getControllerDescPrefix(ctrlDesc);
		if( prefix != null ) {
			if( ! this.prefix.equals(prefix) ) {
				return false;
			}
			ctrlDesc = JuliacHelper.stripControllerDescPrefix(ctrlDesc);
		}

		/*
		 * Check that the specified controller descriptor is defined in the .cfg
		 * configuration files associated with Juliac.
		 */
		boolean accept = mloader.containsKey(ctrlDesc);
		return accept;
	}

	@Override
	public void setCtrlDescPrefix( String prefix ) {
		this.prefix = prefix;
	}
	private String prefix = "";  // To ensure that prefix is never null

	@Override
	public SourceCodeGeneratorItf generate(
		Type type, Object controllerDesc, Object contentDesc, Object source )
	throws IOException {

		/*
		 * Check parameter consistence. It's safe to perform the casts after
		 * having performed the checks.
		 */
		if( !(type instanceof ComponentType) ) {
			final String msg = "ComponentType instance expected";
			throw new IllegalArgumentException(msg);
		}
		if( !(controllerDesc instanceof String) ) {
			final String msg = "controllerDesc should be a String";
			throw new IllegalArgumentException(msg);
		}
		ComponentType ct = (ComponentType) type;
		String ctrlDesc = (String) controllerDesc;

		/*
		 * Generate the membrane implementation.
		 */
		String contentClassName =
			JuliacHelper.getContentClassName(ctrlDesc,contentDesc);
		MembraneDesc<?> membraneDesc =
			generateMembraneImpl(ct,ctrlDesc,contentClassName,source);

		/*
		 * Generate the initializer class.
		 */
		SourceCodeGeneratorItf scg =
			getInitializerClassGenerator(
				jc,this,membraneDesc,ct,contentDesc,source);
		jc.generateSourceCode(scg);

		return scg;
	}

	@Override
	public SourceCodeGeneratorItf getInterfaceClassGenerator(InterfaceType it) {
		String signature = it.getFcItfSignature();
		Class<?> cl = jc.loadClass(signature);
		String pkgRoot = jc.getPkgRoot();
		return new InterfaceImplementationClassGenerator(it,cl,pkgRoot,false);
	}


	// ------------------------------------------------------------------
	// Implementation specific
	// ------------------------------------------------------------------

	/**
	 * Return the class generator for component initializers. An initializer is
	 * a factory and provides a <code>newFcInstance</code> method for
	 * instantiating components.
	 */
	protected abstract InitializerClassGenerator
	getInitializerClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct, Object contentDesc, Object source );

	/**
	 * Generate the source code of component interfaces associated with the
	 * specified array of interface types.
	 */
	protected void generateInterfaceImpl( InterfaceType[] its, String ctrlDesc )
	throws IOException {
		for (InterfaceType it : its) {
			generateInterfaceImpl(it,ctrlDesc);
			InterfaceType internalit =
				InterfaceTypeHelper.newSymetricInterfaceType(it);
			generateInterfaceImpl(internalit,ctrlDesc);
		}
	}

	/**
	 * Generate the source code of component interfaces associated with the
	 * specified interface type and controller descriptor.
	 */
	protected void generateInterfaceImpl( InterfaceType it, String ctrlDesc )
	throws IOException {

		String signature = it.getFcItfSignature();
		Class<?> cl = jc.loadClass(signature);

		/*
		 * If the interface is a business attribute control interface, generate
		 * an implementation which provides a cloneFcAttributes method.
		 *
		 * The second expression in the && skips cloneable attribute control
		 * interfaces which are provided by parametric membranes.
		 */
		if( AttributeController.class.isAssignableFrom(cl) &&
			! CloneableAttributeController.class.isAssignableFrom(cl) ) {
			String pkgRoot = jc.getPkgRoot();
			SourceCodeGeneratorItf ascg =
				new AttributeControllerClassGenerator(
					it,cl,pkgRoot,null,false);
			jc.generateSourceCode(ascg);
		}

		SourceCodeGeneratorItf iscg = getInterfaceClassGenerator(it);
		jc.generateSourceCode(iscg);
	}

	/**
	 * Generate the implementation of interceptors associated to the specified
	 * component type and the specified membrane descriptor.
	 */
	protected void generateInterceptorImpl(
		InterfaceType[] its, MembraneDesc<?> membraneDesc )
	throws IOException {

		InterfaceTypeConfigurableItf iscg =
			membraneDesc.getInterceptorClassGenerator();
		if( iscg == null ) {
			// No interceptor
			return;
		}

		// Generate the interceptors
		iscg.setMembraneDesc(membraneDesc);
		String pkgRoot = jc.getPkgRoot();
		for (InterfaceType it : its) {
			String signature = it.getFcItfSignature();
			Class<?> cl = jc.loadClass(signature);
			iscg.setInterfaceType(it,cl,pkgRoot);
			if( iscg.match() ) {
				jc.generateSourceCode(iscg);
			}
		}
	}

	/**
	 * Return the interceptor class generator associated to the specified
	 * controller descriptor. Return <code>null</code> if there is no such
	 * generator.
	 *
	 * @since 2.7
	 */
	public InterfaceTypeConfigurableItf getInterceptorClassGenerator(
		String ctrlDesc ) {

		String asm = mloader.getInterceptorClassGeneratorName(ctrlDesc);
		if( asm == null ) {
			return null;
		}

		Class<InterceptorClassGenerator> cl = jc.loadClass(asm);
		InterceptorClassGenerator icg = jc.instantiate(cl);
		InterceptorSourceCodeGeneratorItf[] iscgs =
			(InterceptorSourceCodeGeneratorItf[])
			getInterceptorSourceCodeGenerators(ctrlDesc);
		icg.init(iscgs);

		return icg;
	}

	/**
	 * Return the interceptor source code generators associated to the specified
	 * controller descriptor. Return <code>null</code> if there is no such
	 * generators.
	 *
	 * @since 2.7
	 */
	public InterfaceTypeConfigurableItf[] getInterceptorSourceCodeGenerators(
		String ctrlDesc ) {

		String[] asms =
			mloader.getInterceptorSourceCodeGeneratorNames(ctrlDesc);
		if( asms == null ) {
			return null;
		}

		InterceptorSourceCodeGeneratorItf[] iscgs =
			new InterceptorSourceCodeGeneratorItf[asms.length];
		for (int i = 0; i < iscgs.length; i++) {
			Class<InterceptorSourceCodeGeneratorItf> cl = jc.loadClass(asms[i]);
			iscgs[i] = jc.instantiate(cl);
		}

		return iscgs;
	}
}
