/***
 * Juliac
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.conf;

import java.io.IOException;
import java.util.Map;

import org.objectweb.fractal.julia.loader.Tree;
import org.objectweb.fractal.juliac.api.JuliacModuleItf;
import org.objectweb.fractal.juliac.api.desc.NoSuchControllerDescriptorException;

/**
 * The service interface for retrieving membrane data in <code>.cfg</code>
 * files.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.5
 */
public interface CfgLoaderItf extends JuliacModuleItf {

	/**
	 * Return <code>true</code> if the specified tree name exists.
	 *
	 * @param key  the name of the tree
	 * @return     <code>true</code> if the specified tree name exists
	 * @since 2.5
	 */
	public boolean containsKey( String key );

	/**
	 * Load the specified configuration file.
	 *
	 * @param filename  the name of the configuration file
	 * @throws IOException
	 *         if the file cannot be loaded or if an error occurs while loading the
	 *         file
	 */
	public void loadConfigFile( String filename ) throws IOException;

	/**
	 * Return the specified tree.
	 *
	 * @param name  the name of the tree
	 * @return      the corresponding tree
	 * @throws NoSuchControllerDescriptorException  if the tree does not exist
	 */
	public Tree loadTree( String name );

	/**
	 * Evaluate the specifed tree by substituting quoted elements by their
	 * value.
	 *
	 * @param tree     the source tree
	 * @param context  the contextual map containing values for quoted elements
	 * @return         the evaluated tree
	 * @throws IllegalArgumentException
	 *         if a value is missing for a quoted element
	 */
	public Tree evalTree( final Tree tree, final Map<?,?> context );
}
