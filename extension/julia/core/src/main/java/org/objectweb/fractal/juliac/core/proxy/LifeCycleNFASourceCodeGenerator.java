/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.proxy;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.Interceptor;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.control.lifecycle.InvocationCounterItf;

/**
 * <p>
 * This class generates the source code of lifecycle interceptors.
 * </p>
 *
 * <p>
 * This class specializes the original Juliac lifecycle interceptor generator by
 * always invoking the <code>incrementFcInvocationCounter()</code> and
 * <code>decrementFcInvocationCounter()</code> methods instead of accessing
 * directly the <code>fcInvocationCounter</code> field. This is needed, not only
 * when interceptors are componentized, but also for object interceptors that
 * access controllers implemented in Scala. In this last case, it appears that
 * public fields are privatized when compiled into bytecode by Scala and are
 * then no longer accessible from the generated Java object interceptor.
 * </p>
 *
 * <p>
 * The acronym NFA in the name of the class stands for no field access.
 * </p>
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class LifeCycleNFASourceCodeGenerator
extends LifeCycleSourceCodeGenerator {

	public LifeCycleNFASourceCodeGenerator() {
		super();
	}

	public LifeCycleNFASourceCodeGenerator(
		InterfaceType it, Class<?> proxycl, String pkgRoot,
		MembraneDesc<?> membraneDesc, boolean mergeable ) {

		super(it,proxycl,pkgRoot,membraneDesc,mergeable);
	}

	   @Override
	public void generateFields( ClassSourceCodeVisitor cv ) {
		cv.visitField(
			Modifier.PRIVATE, InvocationCounterItf.class.getName(),
			"_lc", null );
	}

	@Override
	public void generateMethodInitFcController( BlockSourceCodeVisitor mv ) {
		if( ! mergeable ) {
			mv.visitVar("Object","olc","ic.getInterface(\"lifecycle-controller\")");
			BlockSourceCodeVisitor then = mv.visitIf("!(","olc","instanceof",InvocationCounterItf.class.getName(),")");
			BlockSourceCodeVisitor wdo = then.visitWhile("olc","instanceof",Interceptor.class.getName());
			wdo.visitSet("olc", "(("+Interceptor.class.getName()+")olc).getFcItfDelegate()").visitEnd();
			then.visitEnd();

			// Record the reference to the instance implementing the controller
			mv.visitSet("_lc","("+InvocationCounterItf.class.getName()+")","olc");
		}
	}

	@Override
	public void generateProxyMethodBodyBeforeCode(
		BlockSourceCodeVisitor mv, Method proxym ) {

		BlockSourceCodeVisitor sync = mv.visitSync("_lc");
		sync.visitIns("_lc.incrementFcInvocationCounter()");
		sync.visitEnd();
		mv.visitln("      try {");
	}

	@Override
	public void generateProxyMethodBodyAfterReturningCode(
		BlockSourceCodeVisitor mv, Method proxym ) {

		mv.visitln("      }");
		mv.visitln("      finally {");
		BlockSourceCodeVisitor sync = mv.visitSync("_lc");
		sync.visitIns("_lc.decrementFcInvocationCounter()");
		sync.visitEnd();
		mv.visitln("      }");
	}
}
