/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.core.proxy;

import java.lang.reflect.Method;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.InterceptorSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.helper.InterfaceTypeHelper;

/**
 * Abstract implementation for interceptor source code generators.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 * @since 2.0
 */
public abstract class AbstractInterceptorSourceCodeGenerator
extends AbstractProxyClassGenerator
implements InterceptorSourceCodeGeneratorItf {

	public AbstractInterceptorSourceCodeGenerator() {}

	public AbstractInterceptorSourceCodeGenerator(
		InterfaceType it, Class<?> proxycl, String pkgRoot,
		MembraneDesc<?> membraneDesc, boolean mergeable ) {

		super(it,proxycl,pkgRoot,membraneDesc,mergeable);
	}

	@Override
	public String[] getImplementedInterfaceNames() {
		String signature =
			InterfaceTypeHelper.getInterfaceTypeSignature(it,proxycl);
		return new String[]{signature};
	}

	@Override
	public void generateProxyMethodBody(
		BlockSourceCodeVisitor mv, Method proxym ) {

		// Before code
		generateProxyMethodBodyBeforeCode(mv,proxym);

		// Call to the delegate
		generateProxyMethodBodyDelegatingCode(mv,proxym);

		// After code
		generateProxyMethodBodyAfterCode(mv,proxym);

		// Return the result
		generateProxyMethodBodyReturn(mv,proxym);

		// Finally code
		generateProxyMethodBodyAfterReturningCode(mv,proxym);
	}

	@Override
	public void generateMethodInitFcController( BlockSourceCodeVisitor mv ) {}

	@Override
	public void generateMethodClone( BlockSourceCodeVisitor mv ) {}

	@Override
	public void generateMethodOthers( ClassSourceCodeVisitor cv ) {}

	@Override
	public void generateProxyMethodBodyBeforeCode( BlockSourceCodeVisitor mv, Method proxym ) {}

	@Override
	public void generateProxyMethodBodyAfterCode( BlockSourceCodeVisitor mv, Method proxym ) {}

	@Override
	public void generateProxyMethodBodyAfterReturningCode( BlockSourceCodeVisitor mv, Method proxym ) {}

	@Override
	public String getDelegatingInstance( Method proxym ) {
		return mergeable ? "this" : "_impl";
	}
}
