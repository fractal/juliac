/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.desc;

import java.util.List;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.InterfaceTypeConfigurableItf;

/**
 * A membrane descriptor where the structure of the membrane is represented by a
 * list of controller descriptors. This is the regular case for Julia-like
 * object-oriented control membranes.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2
 */
public class SimpleMembraneDesc extends MembraneDesc<List<ControllerDesc>> {

	public SimpleMembraneDesc(
		JuliacItf jc, String name,
		String descriptor, InterfaceType[] ctrlItfTypes, List<ControllerDesc> ctrlDescs,
		InterfaceTypeConfigurableItf icg,
		InterfaceTypeConfigurableItf[] iscgs ) {

		super(jc,name,descriptor,ctrlItfTypes,ctrlDescs,icg,iscgs);
	}

	@Override
	public Class<?> getCtrlImpl( String signature ) {

		Class<?> ctrlItf = jc.loadClass(signature);
		List<ControllerDesc> ctrlDescs = getCtrlDescs();

		for (ControllerDesc ctrlDesc : ctrlDescs) {
			String ctrlImplName = ctrlDesc.getImplName();
			Class<?> cl = jc.loadClass(ctrlImplName);
			if( ctrlItf.isAssignableFrom(cl) ) {
				return cl;
			}
		}

		final String msg =
			"No controller implementing "+signature+
			" in controller descriptor "+getDescriptor();
		throw new IllegalArgumentException(msg);
	}
}
