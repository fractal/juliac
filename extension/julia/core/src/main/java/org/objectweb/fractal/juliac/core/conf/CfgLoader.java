/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.conf;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.julia.loader.Tree;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.NoSuchControllerDescriptorException;

/**
 * A Julia configuration file (.cfg) loader.
 *
 * The code of this class is derived from that of {@link BasicLoader}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.3
 */
public class CfgLoader extends HashMap<String,Tree>
implements CfgLoaderItf {

	private static final long serialVersionUID = -3330622124076930431L;

	/** A transient stream used to parse trees. */
	private InputStream is;

	/** Current line number in {@link #is}. */
	private int line;

	/** Last character read from {@link #is}. */
	private int car;

	/**
	 * The names of configuration files that have been loaded so far.
	 */
	private List<String> filenames = new ArrayList<>();

	/** The class loader used for loading configuration files. */
	private ClassLoader classloader;


	// -----------------------------------------------------------------------
	// Implementation of the JuliacModuleItf interface
	// -----------------------------------------------------------------------

	@Override
	public void init( JuliacItf jc ) throws IOException {
		classloader = jc.getClassLoader();

		// Register the service
		jc.register(CfgLoaderItf.class,this);
	}

	@Override
	public void close( JuliacItf jc ) throws IOException {
		jc.unregister(CfgLoaderItf.class,this);
	}


	// -----------------------------------------------------------------------
	// Implementation of the JuliaLoaderItf interface
	// -----------------------------------------------------------------------

	/**
	 * Return <code>true</code> if the specified tree name exists.
	 *
	 * @param key  the name of the tree
	 * @return     <code>true</code> if the specified tree name exists
	 * @since 2.5
	 */
	@Override
	public boolean containsKey( String key ) {
		return super.containsKey(key);
	}

	/**
	 * Load the specified configuration file.
	 *
	 * @param filename  the name of the configuration file
	 * @throws IOException
	 *         if the file cannot be loaded or if an error occurs while loading the
	 *         file
	 */
	@Override
	public void loadConfigFile( String filename ) throws IOException {

		if( filenames.contains(filename) ) {
			/*
			 * Don't load twice the same filename. This may happen when multiple
			 * optimization levels that refer to the same configuration files
			 * are specified, e.g. <code>COMP:OO</code>.
			 */
			return;
		}

		InputStream is = classloader.getResourceAsStream(filename);
		if( is == null ) {
			String msg = "Cannot load "+filename+". Classloader: ";
			if( classloader instanceof URLClassLoader ) {
				URLClassLoader uc = (URLClassLoader) classloader;
				URL[] urls = uc.getURLs();
				for (URL url : urls) {
					msg += url.toString()+',';
				}
			}
			else {
				msg += classloader.getClass()+": "+classloader;
			}
			throw new IOException(msg);
		}
		try {
			line = 1;
			while (true) {
				Tree def;
				try {
				  def = parseTree(is);
				}
				catch (IOException e) {
					String msg =
						"File '" + filename + "', line " + line + ": " +
						e.getMessage();
					throw new IOException(msg);
				}
				if (def == null) {
					break;
				} else {
					String name = def.getSubTree(0).toString();
					Tree t = def.getSubTree(1);
					put(name,t);
				}
			}
		}
		finally {
			try {
				is.close();
			}
			catch (IOException ignored) {}
		}

		filenames.add(filename);
	}

	/**
	 * Return the specified tree.
	 *
	 * @param name  the name of the tree
	 * @return      the corresponding tree
	 * @throws NoSuchControllerDescriptorException  if the tree does not exist
	 */
	@Override
	public Tree loadTree( String name ) {

		if ( ! containsKey(name) ) {
			String str = Arrays.deepToString(filenames.toArray());
			String msg =
				"Cannot find the '"+name+"' descriptor in the '" + str +
				"' configuration file(s)";
			throw new NoSuchControllerDescriptorException(msg);
		}

		Tree t = get(name);
		return t;
	}

	/**
	 * Evaluate the specifed tree by substituting quoted elements by their
	 * value.
	 *
	 * @param tree     the source tree
	 * @param context  the contextual map containing values for quoted elements
	 * @return         the evaluated tree
	 * @throws IllegalArgumentException
	 *         if a value is missing for a quoted element
	 */
	@Override
	public Tree evalTree( final Tree tree, final Map<?,?> context ) {

		if (tree.getSize() == 0) {
			String var = tree.toString();
			if (var.startsWith("'")) {
			  var = var.substring(1);
			  Tree val = (Tree) context.get(var);
			  if (val == null) {
				  val = loadTree(var);
			  }
			  if (val == null) {
				  throw new IllegalArgumentException(var);
			  }
			  if (!val.toString().equals("QUOTE")) {
				  return evalTree(val,context);
			  }
			}
			return tree;
		}
		else {
			boolean ok = true;
			Tree[] subTrees = tree.getSubTrees();
			for (int i = 0; i < subTrees.length; ++i) {
			  Tree oldSubTree = subTrees[i];
			  Tree newSubTree = evalTree(oldSubTree, context);
			  if (newSubTree != oldSubTree) {
				if (ok) {
				  Tree[] newSubTrees = new Tree[subTrees.length];
				  System.arraycopy(subTrees, 0, newSubTrees, 0, subTrees.length);
				  subTrees = newSubTrees;
				  ok = false;
				}
				subTrees[i] = newSubTree;
			  }
		  }
		  return (ok ? tree : new Tree(subTrees));
		}
	}


	// -------------------------------------------------------------------------
	// Utility methods: tree parsing
	// -------------------------------------------------------------------------

	/**
	 * Creates a tree by parsing the given input stream. This method can be
	 * called several times on the same stream, in order to parse several
	 * consecutive trees, but <i>only</i> if the there is at least one space,
	 * tab or return character between these trees.
	 *
	 * @param is the input stream to be parsed. This input stream must use a
	 *      LISP like syntax, with single line comments beginning with '#', as
	 *      in the following example:
	 *      <pre>
	 *      ( # foo
	 *        foo
	 *        # bar
	 *        (bar foo)
	 *      )</pre>
	 * @return  the tree parsed from the given input stream, or <tt>null</tt> if
	 *      the end of the stream has been reached.
	 * @throws IOException  if an I/O exception occurs during parsing.
	 */
	private Tree parseTree( final InputStream is ) throws IOException {
		try {
			this.is = is;
			read();
			parseSpaces();
			if (car == -1) {
			  return null;
			} else {
			  return parseTree();
			}
		} finally {
			this.is = null;
		}
	}

	/**
	 * Recursive method to parse a tree. The first character of the tree to be
	 * parsed is supposed to have already been read, and available in {@link
	 * #car car}. After parsing, the character immediately following the parsed
	 * tree is also supposed to have already been parsed, and available in
	 * {@link #car car}.
	 *
	 * @return  a tree parsed from the {@link #is is} stream.
	 * @throws IOException  if an I/O exception occurs during parsing.
	 */
	private Tree parseTree() throws IOException {
		int c = car;
		if (c == -1) {
		  throw new IOException("Unexpected end of file");
		} else if (c == ')') {
		  throw new IOException("Unmatched closing parenthesis");
		} else if (c == '(') {
		  // parses a tree of the form "(subTree1 ... subTreeN)"
		  read();
		  List<Tree> subTrees = new ArrayList<>();
		  while (true) {
			c = parseSpaces();
			if (c == ')') {
			  read();
			  return new Tree(subTrees.toArray(new Tree[subTrees.size()]));
			} else {
			  subTrees.add(parseTree());
			}
		  }
		}
		else {
		  // parses a tree of the form "tree"
		  StringBuilder buf = new StringBuilder();
		  while (true) {
			buf.append((char)c);
			c = read();
			if (c == -1 || c == ' ' || c == '\t' || c == '\n' ||
				c == '\r' || c == '#' || c == '(' || c == ')')
			{
			  car = c;
			  return new Tree(buf.toString());
			}
		  }
		}
	}

	/**
	 * Parses spaces and comments until a non space character is found.
	 *
	 * @return  the first non space character found, which is also stored in
	 *      {@link #car car}.
	 * @throws IOException  if I/O exception occurs during parsing.
	 */
	private int parseSpaces() throws IOException {
		int c = car;
		while (c == ' ' || c == '\t' || c == '\n' || c == '\r' || c == '#') {
		  if (c == '#') {
			// parses a single line comment
			do {
			  c = read();
			} while (c != '\n' && c != '\r');
		  }
		  c = read();
		}
		return car = c;
	}

	/**
	 * Reads a char in {@link #is} and updates {@link #line} if needed.
	 *
	 * @return  a char read from {@link #is}.
	 * @throws IOException  if I/O exception occurs during reading.
	 */
	private int read() throws IOException {
		car = is.read();
		if (car == '\n' || car == '\r') {
		  ++line;
		}
		return car;
	}
}
