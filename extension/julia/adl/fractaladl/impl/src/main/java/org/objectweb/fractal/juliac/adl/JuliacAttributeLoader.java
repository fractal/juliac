/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adl;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.attributes.AttributeLoader;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;

/**
 * This class replaces the default attribute loader defined by Fractal ADL which
 * uses Java reflection to check that the signature of an attribute control
 * interface defines the setter and the getter associated to the defined
 * attribute. As the interface associated to the signature may not be available
 * in the classpath, we do not want Fractal ADL to throw an exception when
 * the setter and the getter methods are not found. This class does not perform
 * this check.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class JuliacAttributeLoader extends AttributeLoader {

	// --------------------------------------------------------------------------
	// Implementation of the Loader interface
	// --------------------------------------------------------------------------

	@Override
	public Definition load (final String name, final Map<Object,Object> context)
	throws ADLException {
		Definition d = clientLoader.load(name,context);
		checkNode2(d,context);
		return d;
	}

	// --------------------------------------------------------------------------
	// Checking methods
	// --------------------------------------------------------------------------

	private void checkNode2( final Object node, final Map<Object,Object> context)
	throws ADLException {
//        if (node instanceof AttributesContainer) {
//             checkAttributesContainer((AttributesContainer)node, context);
//        }
		if( node instanceof ComponentContainer ) {
			Component[] comps = ((ComponentContainer)node).getComponents();
			for( int i = 0; i < comps.length; i++ ) {
				checkNode2(comps[i],context);
			}
		}
	}
}
