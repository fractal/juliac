/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adl.stati;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.juliac.core.desc.CompDesc;
import org.objectweb.fractal.juliac.core.opt.AbstractComponentFactoryClassGenerator;

/**
 * This class generates the source code of a component {@link
 * org.objectweb.fractal.api.factory.Factory}. This source code generator relies
 * on a Fractal ADL backend to parse an ADL file.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.1
 */
public class ComponentFactoryClassGenerator
extends AbstractComponentFactoryClassGenerator {

	private String adl;
	private Map<Object,Object> context;
	private Factory juliacADLFactory;

	public ComponentFactoryClassGenerator(
		String adl, String targetname, Factory juliacADLFactory,
		Map<Object,Object> context ) {

		super(targetname);

		this.adl = adl;
		this.juliacADLFactory = juliacADLFactory;
		this.context = context;
	}

	/**
	 * Generate the <code>newFcInstance999</code> methods.
	 *
	 * @param pw     the print writer where the factory is to be outputted
	 * @param declareLocalVars
	 *      <code>true</code> if the variables which hold references to
	 *      components need to be declared
	 */
	protected CompDesc generateNewFcInstanceMethods(
		PrintWriter pw, boolean declareLocalVars )
	throws IOException {

		/*
		 * Provide a list for storing component identifiers. This list is
		 * populated by {@link JuliacImplementationBuilder} and used by the
		 * super class of this class for declaring fields containing references
		 * to components.
		 */
		Map<Object,Object> newcontext = new HashMap<>();
		newcontext.putAll(context);
		List<String> subCompIDs = new ArrayList<>();
		newcontext.put(CONTEXT_ID_COMPIDS,subCompIDs);
		newcontext.put(CONTEXT_ID_PW,pw);

		Object ret = null;
		try {
			ret = juliacADLFactory.newComponent(adl,newcontext);
		}
		catch( ADLException ae ) {
			/*
			 * Don't use new IOException(ae) as IOException#<init>(Throwable)
			 * was introduced in JDK 6 and will generate an error when compiling
			 * with JDK 5.
			 */
			IOException ioe = new IOException();
			ioe.initCause(ae);
			throw ioe;
		}

		if( !(ret instanceof CompDesc) ) {
			String msg =
				"Return value expected to be of type CompDesc. "+
				"Got: "+ret.getClass().getName()+' '+ret;
			throw new IOException(msg);
		}
		CompDesc cdesc = (CompDesc) ret;
		cdesc.setCompIDs(subCompIDs);

		return cdesc;
	}

	/**
	 * The key associated to the list of generated component identifiers in the
	 * context map used by the Fractal ADL parser.
	 *
	 * @since 2.5
	 */
	static final String CONTEXT_ID_COMPIDS = "compids";

	/**
	 * The key associated to the PrintWriter instance in the context map used by
	 * the Fractal ADL parser.
	 *
	 * @since 2.5
	 */
	static final String CONTEXT_ID_PW = "printwriter";
}
