/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adl;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.juliac.adl.stati.ComponentFactoryClassGenerator;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.core.desc.ADLParserSupportItf;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;
import org.objectweb.fractal.juliac.core.desc.ComponentDescFactoryClassGenerator;

/**
 * This class provides a version of Fractal ADL that handles Java code
 * generation for Fractal components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.0
 */
public class FractalADLSupportImpl implements ADLParserSupportItf {

	/**
	 * The Fractal ADL backend component for retrieving the model of an ADL
	 * descriptor.
	 */
	public static final String MODEL_BACKEND =
		"org.objectweb.fractal.juliac.adl.JuliacModelBackend";

	/**
	 * The Fractal ADL backend component which generates the source code of the
	 * {@link org.objectweb.fractal.api.factory.Factory} class for an ADL
	 * descriptor.
	 */
	public static final String STATIC_BACKEND =
		"org.objectweb.fractal.juliac.adl.JuliacStaticBackend";


	// -----------------------------------------------------------------------
	// Implementation of the JuliacModuleItf interface
	// -----------------------------------------------------------------------

	protected JuliacItf jc;

	/**
	 * Initialize the service.
	 */
	public void init( JuliacItf jc ) {
		this.jc = jc;
		jc.register(ADLParserSupportItf.class,this);
	}

	public void close( JuliacItf jc ) {
		jc.unregister(ADLParserSupportItf.class,this);
	}


	// -----------------------------------------------------------------------
	// Implementation of the ADLParserSupportItf interface
	// -----------------------------------------------------------------------

	/**
	 * Return <code>true</code> if the current source code generator handles the
	 * specified ADL descriptor.
	 *
	 * @param adl  the ADL descriptor to be checked
	 * @return     if the ADL descriptor should be accepted
	 * @since 2.2.1
	 */
	public boolean test( String adl ) {
		/*
		 * Do not use File#separatorChar (even on Windows) as it leads to is
		 * being null when the resource is stored in a jar file.
		 */
		String adlFileName = adl.replace('.','/')+".fractal";
		ClassLoader loader = jc.getClassLoader();
		InputStream is = loader.getResourceAsStream(adlFileName);
		boolean exist = (is != null);
		return exist;
	}

	/**
	 * Parse the specified ADL and return a model of it.
	 *
	 * @param adl      the fully-qualified name of the ADL descriptor
	 * @param context  contextual information
	 * @return         the model of the specified ADL
	 */
	public ComponentDesc<?> parse( String adl, Map<Object,Object> context )
	throws IOException {

		String factoryName = getFactoryName();
		Map<Object,Object> newcontext = getContext(context);
		Object ret = null;

		try {
			Factory juliacADLFactory =
				FactoryFactory.getFactory(
					factoryName,
					FractalADLSupportImpl.MODEL_BACKEND,
					new HashMap<>() );
			ret = juliacADLFactory.newComponent(adl,newcontext);
		}
		catch( ADLException ae ) {
			throw new IOException(ae);
		}

		if( !(ret instanceof ComponentDesc) ) {
			final String msg =
				"Return value expected to be of type ComponentDesc. Got: "+
				ret.getClass().getName();
			throw new IOException(msg);
		}

		return (ComponentDesc<?>) ret;
	}

	/**
	 * Generate the source code of a factory class for the specified ADL.
	 *
	 * @param adl         the fully-qualified name of the ADL descriptor
	 * @param targetname  the fully-qualified name of the factory class
	 */
	public void generate( String adl, String targetname )
	throws IOException {

		String factoryName = getFactoryName();
		Factory juliacADLFactory = null;

		try {
			juliacADLFactory =
				FactoryFactory.getFactory(
					factoryName,
					FractalADLSupportImpl.STATIC_BACKEND,
					new HashMap<>() );
		}
		catch( ADLException ae ) {
			throw new IOException(ae);
		}

		/*
		 * ADL descriptor names may contain characters such as minus that are
		 * legal file name characters, but that are not legal as class names.
		 */
		targetname = targetname.replace("-","_minus_");

		Map<Object,Object> context = getContext(null);
		ComponentFactoryClassGenerator cfcg =
			new ComponentFactoryClassGenerator(
				adl, targetname, juliacADLFactory, context );
		jc.generateSourceCode(cfcg);
	}

	/**
	 * Generate the source code of a factory class for the specified model of
	 * the ADL.
	 *
	 * @param cdesc       the model of the ADL descriptor
	 * @param targetname  the fully-qualified name of the factory class
	 * @since 2.5
	 */
	public void generate( ComponentDesc<?> cdesc, String targetname )
	throws IOException {

		ComponentDescFactoryClassGenerator<?> cfcg =
			new ComponentDescFactoryClassGenerator<>(jc,cdesc,targetname);
		jc.generateSourceCode(cfcg);
	}


	// -----------------------------------------------------------------------
	// Implementation specific
	// -----------------------------------------------------------------------

	protected Map<Object,Object> getContext( Map<Object,Object> context ) {

		Map<Object,Object> contextMap =
			context==null ? new HashMap<>() : context;

		/*
		 * Initialize a context which includes all system properties.
		 * This has been added following a discussion with Alessio who wanted to
		 * be able to transmit argument values to compile with Juliac the code
		 * of Fractal ADL.
		 * This is also useful for any ADL which is parameterized.
		 */
		Map<Object,Object> newcontext = new HashMap<>();
		newcontext.putAll(contextMap);

		Map<Object,Object> systemPropertiesMap =
			new HashMap<>(System.getProperties());
		newcontext.putAll(systemPropertiesMap);

		/*
		 * Instantiates YesClassLoader with the Juliac classloader.
		 * If this is not done (e.g. just like instantiating
		 * YesClassLoader without any parent), a default class loader is used.
		 * When run under maven, this default class loader cannot load
		 * resource streams such as the .fractal files which are bundled in
		 * dependent .jar files (such as koch-runtime.jar).
		 * The trick is not needed under eclipse, but the code is kept as this
		 * to preserve compatibility with maven.
		 */
		ClassLoader classLoader = jc.getClassLoader();
		ClassLoader yesClassLoader = new YesClassLoader(classLoader);
		newcontext.put("classloader",yesClassLoader);

		newcontext.put(CONTEXT_ID_JULIAC,jc);

		return newcontext;
	}

	/**
	 * The key associated to the instance of Juliac in the context map used by
	 * the Fractal ADL parser.
	 *
	 * @since 2.5
	 */
	public static final String CONTEXT_ID_JULIAC = "juliac";

	/**
	 * Return the name of the Fractal ADL factory.
	 *
	 * @since 2.6
	 */
	protected String getFactoryName() {
		return "org.objectweb.fractal.juliac.adl.JuliacFactory";
	}
}
