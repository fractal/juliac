/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adl;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.implementations.ControllerContainer;
import org.objectweb.fractal.adl.implementations.ImplementationLoader;

/**
 * This class replaces the default implementation loader defined by Fractal ADL
 * which uses Java reflection to check that the content class of a component
 * implements the declared provided interfaces. As the interfaces may not be
 * available in the classpath, we do not want Fractal ADL to throw an exception.
 * This class does not perform this check.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class JuliacImplementationLoader extends ImplementationLoader {

	@Override
	protected void checkNode( Object node, Map<Object,Object> context )
	throws ADLException {

//        if( node instanceof ImplementationContainer ) {
//            checkImplementationContainer((ImplementationContainer)node, context);
//        }
		if( node instanceof ControllerContainer ) {
			checkControllerContainer((ControllerContainer)node);
		}
		if( node instanceof ComponentContainer ) {
			for (final Component comp : ((ComponentContainer) node).getComponents()) {
				checkNode(comp,context);
			}
		}
	}
}
