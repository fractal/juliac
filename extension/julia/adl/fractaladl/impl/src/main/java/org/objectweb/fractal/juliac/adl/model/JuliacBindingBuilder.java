/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adl.model;

import org.objectweb.fractal.adl.bindings.BindingBuilder;
import org.objectweb.fractal.juliac.core.desc.BindingDesc;
import org.objectweb.fractal.juliac.core.desc.BindingType;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;

/**
 * Implementation of the Fractal ADL backend component which builds a model for
 * binding definitions.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.2
 */
public class JuliacBindingBuilder implements BindingBuilder {

	// ------------------------------------------------------------------------
	// Implementation of the BindingBuilder interface
	// ------------------------------------------------------------------------

	public void bindComponent (
		int type, Object client, String clientItf,
		Object server, String serverItf, Object context )
	throws Exception {

		/*
		 * Check the consistency of arguments.
		 */
		if( ! (client instanceof ComponentDesc) ) {
			String msg = "client should be an instance of ComponentDesc";
			throw new IllegalArgumentException(msg);
		}
		if( ! (server instanceof ComponentDesc) ) {
			String msg = "server should be an instance of ComponentDesc";
			throw new IllegalArgumentException(msg);
		}

		/*
		 * Record the binding.
		 */
		ComponentDesc<?> clt = (ComponentDesc<?>) client;
		ComponentDesc<?> srv = (ComponentDesc<?>) server;
		BindingType bt = BindingType.getType(type);
		BindingDesc bdesc = new BindingDesc(bt,clt,clientItf,srv,serverItf);
		clt.putBinding(clientItf,bdesc);
	}
}
