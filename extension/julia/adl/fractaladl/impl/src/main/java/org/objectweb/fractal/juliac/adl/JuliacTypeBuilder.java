/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adl;

import org.objectweb.fractal.adl.types.TypeBuilder;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.type.BasicComponentType;
import org.objectweb.fractal.julia.type.BasicInterfaceType;

/**
 * Implementation of the backend component which generates interface and
 * component types.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class JuliacTypeBuilder implements TypeBuilder {

  public Object createInterfaceType (
	final String name,
	final String signature,
	final String role,
	final String contingency,
	final String cardinality,
	final Object context) throws Exception {

	  InterfaceType it =
		new BasicInterfaceType(
			name,
			signature,
			TypeInterface.CLIENT_ROLE.equals(role),
			TypeInterface.OPTIONAL_CONTINGENCY.equals(contingency),
			TypeInterface.COLLECTION_CARDINALITY.equals(cardinality));

	  return it;
  }

  public Object createComponentType (
	final String name,
	final Object[] interfaceTypes,
	final Object context) throws Exception {

	  InterfaceType[] its = new InterfaceType[ interfaceTypes.length ];
	  for (int i = 0; i < interfaceTypes.length; i++) {
		  Object type = interfaceTypes[i];

		  // Check the consistency of type
		  if( !(type instanceof InterfaceType) ) {
			  final String msg = type + " should implement InterfaceType";
			  throw new IllegalArgumentException(msg);
		  }
		  its[i] = (InterfaceType) type;
	  }
	  ComponentType ct = new BasicComponentType(its);

	  return ct;
  }
}
