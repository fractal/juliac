/***
 * Juliac
 * Copyright (C) 2007-2021w Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adl;

/**
 * <p>
 * Fractal ADL checks that referenced Java types (e.g. interface signatures,
 * content classes) exist. This is safe but prevents ADLs from being parsed
 * if some types are missing. If fact, in all cases except for attribute
 * controller signatures, Fractal ADL simply checks that loading the class does
 * not throw {@link ClassNotFoundException}.
 * </p>
 *
 * <p>
 * When dealing with the COMP optimization level of Juliac, the content class
 * for a control component is described with a configuration string specifying
 * the list of mixin layers. The content class is thus not loadable until the
 * layers have been mixed. To avoid {@link ClassNotFoundException}s in this
 * case, this class loader pretends this class exists by returning its own class
 * (any other class could also be returned).
 * </p>
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.1
 */
public class YesClassLoader extends ClassLoader {

	public YesClassLoader( ClassLoader parent ) {
	   super(parent);
	}

	@Override
	public Class<?> loadClass( String name ) throws ClassNotFoundException {
		char c = name.charAt(0);
		if( c=='\'' || c=='(' ) {
			return YesClassLoader.class;
		}
		return super.loadClass(name);
	}
}
