/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.loader.Tree;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.SpoonSupportItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.conf.CfgLoaderItf;
import org.objectweb.fractal.juliac.core.helper.JuliacHelper;
import org.objectweb.fractal.juliac.core.proxy.AttributeControllerClassGenerator;

/**
 * Root class for Juliac implementation builder components. This class is
 * extended in subpackages.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.6
 */
public class JuliacAbstractImplementationBuilder {

	/**
	 * Consistency check on the <code>type</code> and
	 * <code>controllerDesc</code> parameters transmitted to {@link
	 * org.objectweb.fractal.adl.implementations.ImplementationBuilder#createComponent(Object, String, String, Object, Object, Object)}.
	 *
	 * @throws IllegalArgumentException
	 */
	protected void check( Object type, Object controllerDesc ) {

		/*
		 * Check the consistency of type.
		 * The consistency of contentDesc is checked below by
		 * Utils#getContentClassName().
		 */
		if( !(type instanceof ComponentType) ) {
			final String msg = "type should implement ComponentType";
			throw new IllegalArgumentException(msg);
		}

		if( !(controllerDesc instanceof String) ) {
			final String msg = "controllerDesc should be a String";
			throw new IllegalArgumentException(msg);
		}
	}

	/**
	 * Return the content class name corresponding to the specified descriptors.
	 * The content descriptor may be a <code>julia.cfg</code> configuration
	 * string.
	 */
	protected String getContentClassName(
		String controllerDescStr, Object contentDesc, Map<?,?> context )
	throws TreeParserException, IOException {

		/*
		 * If the content class is a julia.cfg configuration string (this is the
		 * case for control components), generate the class. Two generators are
		 * suppported:
		 * org.objectweb.fractal.julia.asm.MixinClassGenerator
		 * org.objectweb.fractal.julia.asm.AttributeControllerClassGenerator
		 */
		String contentDescStr =
			JuliacHelper.getContentClassName(controllerDescStr,contentDesc);

		if( contentDescStr != null ) {

			char first = contentDescStr.charAt(0);

			// Stringified representation of a julia.cfg Tree
			if( first=='(' || first=='\'' ) {

				JuliacItf jc = (JuliacItf)
					context.get(FractalADLSupportImpl.CONTEXT_ID_JULIAC);
				CfgLoaderItf jloader = jc.lookupFirst(CfgLoaderItf.class);

				TreeParser tp = new TreeParser(contentDescStr);
				Tree src = tp.parseTree();
				Tree tree = jloader.evalTree(src,context);
				tree = tree.getSubTree(0);

				String generator = tree.getSubTree(0).toString();
				contentDescStr = getContentClassName(jc,tree,generator);
			}
		}

		return contentDescStr;
	}

	/**
	 * Return the content class name corresponding to the specified Julia
	 * configuration {@link Tree}.
	 *
	 * @param jc         the Juliac instance
	 * @param tree       the configuration tree
	 * @param generator  the fully qualified name of the class generator
	 * @throws IllegalArgumentException  if the class generator is not supported
	 */
	private String getContentClassName( JuliacItf jc, Tree tree, String generator )
	throws IOException {

		final String pkgRoot = jc.getPkgRoot();

		if( generator.equals("org.objectweb.fractal.julia.asm.MixinClassGenerator") ) {

			String contentDescStr = tree.getSubTree(1).toString();
			contentDescStr = JuliacHelper.getJuliacGeneratedStrongTypeName(contentDescStr);
			contentDescStr = pkgRoot + contentDescStr;

			if( ! jc.hasBeenGenerated(contentDescStr) ) {
				List<String> ctrlImplLayers = new ArrayList<>();
				for( int i=2 ; i < tree.getSize() ; i++ ) {
					String ctrlImplLayer = tree.getSubTree(i).toString();
					ctrlImplLayers.add(ctrlImplLayer);
				}
				SpoonSupportItf spoon =
					jc.lookupFirst(SpoonSupportItf.class);
				spoon.mixAndGenerate(contentDescStr,ctrlImplLayers);
			}

			return contentDescStr;

		}
		else if( generator.equals("org.objectweb.fractal.julia.asm.AttributeControllerClassGenerator") ) {
			/*
			 * eval is of the form:
			 * (org.objectweb.fractal.julia.asm.AttributeControllerClassGenerator
			 *   (org.objectweb.fractal.julia.conform.components.CAttributes)
			 * )
			 */
			String itfname = tree.getSubTree(1).getSubTree(0).toString();
			InterfaceType it =
				new BasicInterfaceType(null,itfname,false,false,false);
			String signature = it.getFcItfSignature();
			Class<?> cl = jc.loadClass(signature);
			SourceCodeGeneratorItf cg =
				new AttributeControllerClassGenerator(
					it,cl,pkgRoot,null,false);
			String contentDescStr = cg.getTargetTypeName();
			return contentDescStr;

		}
		else {
			final String msg =
				"Unsupported class generator "+generator+" in "+tree.toString();
			throw new IllegalArgumentException(msg);
		}
	}
}
