/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adl.model;

import java.util.Map;

import org.objectweb.fractal.adl.implementations.ImplementationBuilder;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.juliac.adl.JuliacAbstractImplementationBuilder;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;
import org.objectweb.fractal.juliac.core.helper.JuliacHelper;

/**
 * Implementation of the backend component which generates component
 * implementations.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class JuliacImplementationBuilder
extends JuliacAbstractImplementationBuilder
implements ImplementationBuilder {

	private int counter = 0;

	// --------------------------------------------------------------------------
	// Implementation of the ImplementationBuilder interface
	// --------------------------------------------------------------------------

	public Object createComponent (
		final Object type, final String name, final String definition,
		final Object controllerDesc, Object contentDesc, final Object context )
	throws Exception {

		/*
		 * Check the consistency of type and controllerDesc.
		 * The casts can safely be performed after having called check().
		 *
		 * The consistency of contentDesc is checked by
		 * #getContentClassName(String,Object,Map<?,?>).
		 */
		check(type,controllerDesc);

		ComponentType ct = (ComponentType) type;
		String controllerDescStr =
			JuliacHelper.stripControllerDescPrefix( (String) controllerDesc );

		/*
		 * If the content class is a julia.cfg configuration string (this is the
		 * case for control components), generate the class. Two generators are
		 * suppported:
		 * org.objectweb.fractal.julia.asm.MixinClassGenerator
		 * org.objectweb.fractal.julia.asm.AttributeControllerClassGenerator
		 */
		Map<?,?> contextMap = (Map<?,?>) context;
		String contentDescStr =
			getContentClassName(controllerDescStr,contentDesc,contextMap);

		String id = "C" + counter;
		counter++;

		ComponentDesc<?> ret =
			new ComponentDesc<>(
				id, name, definition,
				ct, controllerDescStr, contentDescStr, null );
		return ret;
	}
}
