/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adl.stati;

import org.objectweb.fractal.adl.components.StaticFractalComponentBuilder;

/**
 * Implementation of the backend component which generates composite component
 * content.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.6
 */
public class JuliacComponentBuilder extends StaticFractalComponentBuilder {

	// --------------------------------------------------------------------------
	// Implementation of the ComponentBuilder interface
	// --------------------------------------------------------------------------

	@Override
	public void addComponent(
		final Object superComponent, final Object subComponent,
		final String name, final Object context )
	throws Exception {

		super.addComponent(superComponent,subComponent,name,context);
	}

	@Override
	public void startComponent( final Object component, final Object context )
	throws Exception {

		/*
		 * Indeed nothing.
		 *
		 * We don't want to execute the code which is in the super class since
		 * this is a bug in the static Fractal backend of the Fractal ADL tool
		 * chain. The bug is documented by a
		 * <a href="http://mail.ow2.org/wws/arc/fractal/2008-10/msg00002.html">message</a>
		 * which has been posted on the Fractal mailing list.
		 *
		 * This overriding can be removed when a newer version of Fractal ADL
		 * (superior to 2.3) will have been released.
		 */
	}
}
