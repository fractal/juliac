/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adl.model;

import org.objectweb.fractal.adl.components.ComponentBuilder;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;

/**
 * Implementation of the Fractal ADL backend component which builds a model for
 * the content of composite components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.6
 */
public class JuliacComponentBuilder implements ComponentBuilder {

	public void addComponent(
		final Object superComponent, final Object subComponent,
		final String name, final Object context )
	throws Exception {

		/*
		 * Check the consistency of arguments.
		 */
		if( ! (superComponent instanceof ComponentDesc) ) {
			String msg = "superComponent should be an instance of ComponentDesc";
			throw new IllegalArgumentException(msg);
		}
		if( ! (subComponent instanceof ComponentDesc) ) {
			String msg = "subComponent should be an instance of ComponentDesc";
			throw new IllegalArgumentException(msg);
		}

		/*
		 * Record the parent-child relation.
		 */
		ComponentDesc parent = (ComponentDesc<?>) superComponent;
		ComponentDesc<?> child = (ComponentDesc<?>) subComponent;
		parent.addSubComponent(child);
		child.addSuperComponent(parent);
	}

	public void startComponent( final Object component, final Object context )
	throws Exception {
		// Indeed nothing
	}
}
