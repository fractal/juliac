/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2021 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.conform;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.conform.components.C;
import org.objectweb.fractal.julia.conform.components.I;
import org.objectweb.fractal.julia.conform.controllers.StatController;
import org.objectweb.fractal.util.Fractal;

public class TestInterceptor extends AbstractTest {

  protected Component boot;
  protected TypeFactory tf;
  protected GenericFactory gf;

  protected ComponentType t;
  protected Component r, c, d;

  // -------------------------------------------------------------------------
  // Constructor and setup
  // -------------------------------------------------------------------------

  @BeforeEach
  public void setUp () throws Exception {
	boot = Fractal.getBootstrapComponent();
	tf = Fractal.getTypeFactory(boot);
	gf = Fractal.getGenericFactory(boot);
	t = tf.createFcType(new InterfaceType[] {
	  tf.createFcItfType("server", I.class.getName(), false, false, false),
	  tf.createFcItfType("servers", I.class.getName(), false, false, true),
	  tf.createFcItfType("client", I.class.getName(), true, true, false),
	  tf.createFcItfType("clients", I.class.getName(), true, true, true)
	});
	r = gf.newFcInstance(t, "composite", null);
	c = gf.newFcInstance(t, "statComposite", null);
	d = gf.newFcInstance(t, "statPrimitive", C.class.getName());
	Fractal.getContentController(r).addFcSubComponent(c);
	Fractal.getContentController(c).addFcSubComponent(d);
  }

  // -------------------------------------------------------------------------
  // Test interceptors
  // -------------------------------------------------------------------------

  @Test
  public void testInterceptors () throws Exception {
	Fractal.getBindingController(d).bindFc(
	  "client",
	  d.getFcInterface("server"));
	Fractal.getLifeCycleController(d).startFc();

	StatController sd = (StatController)d.getFcInterface("stat-controller");
	assertEquals(0, sd.getNumberOfMethodCalled());

	I i = (I)d.getFcInterface("server");
	assertEquals(3, i.n(0, (long)3));
	assertEquals(7, sd.getNumberOfMethodCalled());
  }

  @Test
  public void testCompositeInterceptors () throws Exception {
	Fractal.getBindingController(c).bindFc(
	  "server",
	  d.getFcInterface("server"));
	Fractal.getBindingController(d).bindFc(
	  "client",
	  Fractal.getContentController(c).getFcInternalInterface("client"));
	Fractal.getBindingController(c).bindFc(
	  "client",
	  c.getFcInterface("server"));
	Fractal.getLifeCycleController(c).startFc();

	StatController sc = (StatController)c.getFcInterface("stat-controller");
	StatController sd = (StatController)d.getFcInterface("stat-controller");
	assertEquals(0, sc.getNumberOfMethodCalled());
	assertEquals(0, sd.getNumberOfMethodCalled());

	I i = (I)c.getFcInterface("server");
	assertEquals(3, i.n(0, (long)3));
	assertEquals(7, sc.getNumberOfMethodCalled());
	assertEquals(7, sd.getNumberOfMethodCalled());
  }

  @Test
  public void testCollectionInterceptors () throws Exception {
	Fractal.getBindingController(d).bindFc(
	  "clients0",
	  d.getFcInterface("servers0"));
	Fractal.getLifeCycleController(d).startFc();

	StatController sd = (StatController)d.getFcInterface("stat-controller");
	assertEquals(0, sd.getNumberOfMethodCalled());

	I i = (I)d.getFcInterface("servers0");
	assertEquals(3, i.n(0, (long)3));
	assertEquals(7, sd.getNumberOfMethodCalled());
  }

  @Test
  public void testCollectionCompositeInterceptors () throws Exception {
	Fractal.getBindingController(c).bindFc(
	  "servers0",
	  d.getFcInterface("servers0"));
	Fractal.getBindingController(d).bindFc(
	  "clients0",
	  Fractal.getContentController(c).getFcInternalInterface("clients0"));
	Fractal.getBindingController(c).bindFc(
	  "clients0",
	  c.getFcInterface("servers0"));
	Fractal.getLifeCycleController(c).startFc();

	StatController sc = (StatController)c.getFcInterface("stat-controller");
	StatController sd = (StatController)d.getFcInterface("stat-controller");
	assertEquals(0, sc.getNumberOfMethodCalled());
	assertEquals(0, sd.getNumberOfMethodCalled());

	I i = (I)c.getFcInterface("servers0");
	assertEquals(3, i.n(0, (long)3));
	assertEquals(7, sc.getNumberOfMethodCalled());
	assertEquals(7, sd.getNumberOfMethodCalled());
  }
}
