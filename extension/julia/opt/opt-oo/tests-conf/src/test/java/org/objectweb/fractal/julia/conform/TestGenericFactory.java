/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2021 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.conform;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.conform.components.C;
import org.objectweb.fractal.julia.conform.components.CAttributes;
import org.objectweb.fractal.julia.conform.components.I;
import org.objectweb.fractal.julia.conform.components.W;
import org.objectweb.fractal.julia.conform.components.X;
import org.objectweb.fractal.julia.conform.components.Y;
import org.objectweb.fractal.julia.conform.components.Z;
import org.objectweb.fractal.util.Fractal;

public class TestGenericFactory extends AbstractTest {

  protected Component boot;
  protected TypeFactory tf;
  protected GenericFactory gf;

  protected ComponentType t, u;

  protected final static String AC = "attribute-controller/"+PKG+".CAttributes/false,false,false";
  protected final static String sI = "server/"+PKG+".I/false,false,false";
  protected final static String cI = "client/"+PKG+".I/true,false,false";

  // -------------------------------------------------------------------------
  // Constructor and setup
  // -------------------------------------------------------------------------

  @BeforeEach
  public void setUp () throws Exception {
	boot = Fractal.getBootstrapComponent();
	tf = Fractal.getTypeFactory(boot);
	gf = Fractal.getGenericFactory(boot);
	t = tf.createFcType(new InterfaceType[] {
	  tf.createFcItfType("server", I.class.getName(), false, false, false),
	  tf.createFcItfType("client", I.class.getName(), true, false, false)
	});
	u = tf.createFcType(new InterfaceType[] {
	  tf.createFcItfType("attribute-controller", CAttributes.class.getName(), false, false, false),
	  tf.createFcItfType("server", I.class.getName(), false, false, false),
	  tf.createFcItfType("client", I.class.getName(), true, false, false)
	});
  }

  // -------------------------------------------------------------------------
  // Test direct component creation
  // -------------------------------------------------------------------------

  @Test
  public void testFPrimitive () throws Exception {
	Component c = gf.newFcInstance(t, "flatPrimitive", C.class.getName());
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, LC, NC, sI, cI
	})));
  }

  @Test
  public void testFParametricPrimitive () throws Exception {
	Component c = gf.newFcInstance(u, "flatParametricPrimitive", C.class.getName());
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, LC, AC, NC, sI, cI
	})));
  }

  @Test
  public void testPrimitive () throws Exception {
	Component c = gf.newFcInstance(t, "primitive", C.class.getName());
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, LC, SC, NC, sI, cI
	})));
  }

  @Test
  public void testParametricPrimitive () throws Exception {
	Component c = gf.newFcInstance(u, "parametricPrimitive", C.class.getName());
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, LC, SC, AC, NC, sI, cI
	})));
  }

  @Test
  public void testComposite () throws Exception {
	Component c = gf.newFcInstance(t, "composite", null);
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, CC, LC, SC, NC, sI, cI
	})));
  }

  @Test
  public void testParametricComposite () throws Exception {
	Component c = gf.newFcInstance(u, "parametricComposite", C.class.getName());
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, CC, LC, SC, AC, NC, sI, cI
	})));
  }

  // -------------------------------------------------------------------------
  // Test component creation via templates
  // -------------------------------------------------------------------------

  @Test
  public void testFPrimitiveTemplate () throws Exception {
	Component c = gf.newFcInstance(
	  t, "flatPrimitiveTemplate", new Object[] { "flatPrimitive", C.class.getName() });
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, F, NC, sI, cI
	})));
	c = Fractal.getFactory(c).newFcInstance();
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, LC, NC, sI, cI
	})));
  }

  @Test
  public void testFParametricPrimitiveTemplate () throws Exception {
	Component c = gf.newFcInstance(
	  u, "flatParametricPrimitiveTemplate", new Object[] { "flatParametricPrimitive", C.class.getName() });
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, F, AC, NC, sI, cI
	})));
	c = Fractal.getFactory(c).newFcInstance();
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, LC, AC, NC, sI, cI
	})));
  }

  @Test
  public void testPrimitiveTemplate () throws Exception {
	Component c = gf.newFcInstance(
	  t, "primitiveTemplate", new Object[] { "primitive", C.class.getName() });
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, F, SC, NC, sI, cI
	})));
	c = Fractal.getFactory(c).newFcInstance();
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, LC, SC, NC, sI, cI
	})));
  }

  @Test
  public void testParametricPrimitiveTemplate () throws Exception {
	Component c = gf.newFcInstance(
	  u, "parametricPrimitiveTemplate", new Object[] { "parametricPrimitive", C.class.getName() });
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, F, SC, AC, NC, sI, cI
	})));
	c = Fractal.getFactory(c).newFcInstance();
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, LC, SC, AC, NC, sI, cI
	})));
  }

  @Test
  public void testCompositeTemplate () throws Exception {
	Component c = gf.newFcInstance(
	  t, "compositeTemplate", new Object[] { "composite", null });
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, CC, F, SC, NC, sI, cI
	})));
	c = Fractal.getFactory(c).newFcInstance();
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, CC, LC, SC, NC, sI, cI
	})));
  }

  @Test
  public void testParametricCompositeTemplate () throws Exception {
	Component c = gf.newFcInstance(
	  u, "parametricCompositeTemplate", new Object[] { "parametricComposite", C.class.getName() });
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, CC, F, SC, AC, NC, sI, cI
	})));
	c = Fractal.getFactory(c).newFcInstance();
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, CC, LC, SC, AC, NC, sI, cI
	})));
  }

  // -------------------------------------------------------------------------
  // Test component creation errors
  // -------------------------------------------------------------------------

  @Test
  public void testUnknownControllerDescriptor () {
	try {
	  // no such controller descriptor
	  gf.newFcInstance(t, "unknownDescriptor", C.class.getName());
	  fail("");
	} catch (InstantiationException e) {
	}
  }

  @Test
  public void testBadControllerDescriptor1 () {
	try {
	  // error in controller descriptor
	  gf.newFcInstance(t, "badPrimitive", C.class.getName());
	  fail("");
	} catch (InstantiationException e) {
	}
  }

  @Test
  public void testBadControllerDescriptor2 () {
	try {
	  // error in controller descriptor
	  gf.newFcInstance(u, "badParametricPrimitive", C.class.getName());
	  fail("");
	} catch (InstantiationException e) {
	}
  }

  @Test
  public void testContentClassNotFound () {
	try {
	  // no such class
	  gf.newFcInstance(t, "primitive", "UnknownClass");
	  fail("");
	} catch (InstantiationException e) {
	}
  }

  @Test
  public void testContentClassAbstract () {
	try {
	  // X is an abstract class
	  gf.newFcInstance(t, "primitive", W.class.getName());
	  fail("");
	} catch (InstantiationException e) {
	}
  }

  @Test
  public void testContentClassNoDefaultConstructor () {
	try {
	  // X has no public constructor
	  gf.newFcInstance(t, "primitive", X.class.getName());
	  fail("");
	} catch (InstantiationException e) {
	}
  }

  @Test
  public void testContentClassControlInterfaceMissing () {
	try {
	  // Y does not implement BindingController
	  gf.newFcInstance(t, "primitive", Y.class.getName());
	  fail("");
	} catch (InstantiationException e) {
	}
  }

  @Test
  public void testContentClassInterfaceMissing () {
	try {
	  // Z does not implement I
	  gf.newFcInstance(t, "primitive", Z.class.getName());
	  fail("");
	} catch (InstantiationException e) {
	}
  }

  @Test
  public void testTemplateContentClassNotFound () {
	try {
	  // no such class
	  gf.newFcInstance(
		t, "primitiveTemplate", new Object[] { "primitive", "UnknownClass" });
	  fail("");
	} catch (InstantiationException e) {
	}
  }

  @Test
  public void testTemplateContentClassAbstract () {
	try {
	  // X is an abstract class
	  gf.newFcInstance(
		t, "primitiveTemplate", new Object[] { "primitive", W.class.getName() });
	  fail("");
	} catch (InstantiationException e) {
	}
  }

  @Test
  public void testTemplateContentClassNoDefaultConstructor () {
	try {
	  // X has no public constructor
	  gf.newFcInstance(
		t, "primitiveTemplate", new Object[] { "primitive", X.class.getName() });
	  fail("");
	} catch (InstantiationException e) {
	}
  }

  @Test
  public void testTemplateContentClassControlInterfaceMissing () {
	try {
	  // Y does not implement BindingController
	  gf.newFcInstance(
		t, "primitiveTemplate", new Object[] { "primitive", Y.class.getName() });
	  fail("");
	} catch (InstantiationException e) {
	}
  }

  @Test
  public void testTemplateContentClassInterfaceMissing () {
	try {
	  // Z does not implement I
	  gf.newFcInstance(
		t, "primitiveTemplate", new Object[] { "primitive", Z.class.getName() });
	  fail("");
	} catch (InstantiationException e) {
	}
  }

  @Test
  public void testControllerDescHiddenInterfaceUsingPrimitive() throws Exception{
	  Component c = gf.newFcInstance(t, "primitive", C.class.getName());

	  Object controllerDescHiddenItf = null;
	  try {
		  controllerDescHiddenItf = c.getFcInterface("/controllerDesc");
	  } catch (NoSuchInterfaceException e) {
		  e.printStackTrace();
		fail("Cannot find hidden interface '/controllerDesc' for primitive component");
	  }
	  assertEquals(
		  "primitive",controllerDescHiddenItf.toString(),
		  "The controllerDesc for primitive components is not correct");
  }

  /**
   * here i use the ContentController of the composite component
   * @throws Exception
   */
  @Test
  public void testControllerDescHiddenInterfaceComposite () throws Exception {
		Component c = gf.newFcInstance(t, "composite", null);
		checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
				  COMP, BC, CC, LC, SC, NC, sI, cI
				})));
		Object contentDescHiddenItf = null;
		  try {
			  contentDescHiddenItf = c.getFcInterface("/controllerDesc");
		  } catch (NoSuchInterfaceException e) {
			  e.printStackTrace();
			fail("Cannot find hidden interface '/controllerDesc' for composite component ");
		  }
		  assertEquals(
			  "composite",contentDescHiddenItf.toString(),
			  "The controllerDesc for primitive components is not correct");
	  }

  @Test
  public void testPrimitiveUsingInstanceAsContentDesc() throws Exception {
	C content = new C();
	Component c = gf.newFcInstance(t, "primitive", content);
	checkComponent(c, new HashSet<>(Arrays.asList(new String[] {
	  COMP, BC, LC, SC, NC, sI, cI
	})));
  }
}
