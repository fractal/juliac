/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.opt.oo;

import java.io.IOException;
import java.util.List;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.SpoonSupportItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.InterfaceTypeConfigurableItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.desc.ControllerDesc;
import org.objectweb.fractal.juliac.core.desc.SimpleMembraneDesc;
import org.objectweb.fractal.juliac.core.helper.JuliacHelper;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGenerator;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;

/**
 * This class generates the source code associated to Fractal components.
 * The membrane implementation and the initializer implementation are generated.
 *
 * The content, the interceptors and the controllers are kept in separate
 * classes and the controllers are implemented with objects.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class FCOOCtrlSourceCodeGenerator extends FCSourceCodeGenerator {

	// -----------------------------------------------------------------------
	// Implementation of the FCSourceCodeGeneratorItf interface
	// -----------------------------------------------------------------------

	public SimpleMembraneDesc generateMembraneImpl(
		ComponentType ct, String ctrldesc, String contentClassName,
		Object source )
	throws IOException {

		/*
		 * Generate business and control interface implementations.
		 */
		InterfaceType[] its = ct.getFcInterfaceTypes();
		generateInterfaceImpl(its,ctrldesc);
		InterfaceType[] membraneits = mloader.getMembraneType(ct,ctrldesc);
		generateInterfaceImpl(membraneits,ctrldesc);

		/*
		 * Generate controller implementations.
		 */
		List<ControllerDesc> ctrlDescs = mloader.getCtrlImplLayers(ct,ctrldesc);
		generateControllerImplementation(ctrlDescs);

		/*
		 * Retrieve interceptor generators.
		 */
		InterfaceTypeConfigurableItf icg =
			getInterceptorClassGenerator(ctrldesc);
		InterfaceTypeConfigurableItf[] iscgs =
			getInterceptorSourceCodeGenerators(ctrldesc);

		/*
		 * Prepare a membrane descriptor.
		 */
		SimpleMembraneDesc md =
			new SimpleMembraneDesc(jc,null,ctrldesc,membraneits,ctrlDescs,icg,iscgs);

		/*
		 * Generate interceptor implementations.
		 * Interceptors must be generated after controllers as some interceptors
		 * (e.g. the lifecycle interceptor) use controller implementation
		 * class names.
		 */
		generateInterceptorImpl(its,md);
		generateInterceptorImpl(membraneits,md);

		/*
		 * Generate the membrane initializer class.
		 */
		InitializerOOCtrlClassGenerator initcg =
			getInitializerClassGenerator(jc,this,md,ct,contentClassName,source);
		SourceCodeGeneratorItf micg =
			initcg.getMembraneInitializerClassGenerator();
		jc.generateSourceCode(micg);

		return md;
	}

	@Override
	protected InitializerOOCtrlClassGenerator getInitializerClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		return new InitializerOOCtrlClassGenerator(
			jc,fcscg,membraneDesc,ct,contentDesc,source);
	}

	protected void generateControllerImplementation(
		List<ControllerDesc> ctrlDescs )
	throws IOException {

		for (ControllerDesc ctrlDesc : ctrlDescs) {

			List<String> ctrlImplLayers = ctrlDesc.getLayerImplNames();
			String ctrlImplClassName = ctrlDesc.getImplName();

			if( ctrlImplClassName.startsWith("(") ) {
				/*
				 * Skip CloneableAttributeController implementations.
				 * The controller implementation class name is in this case
				 * something like:
				 *
				 * ((attribute-controller org.objectweb.fractal.julia.conform.components.CAttributes))
				 */
				continue;
			}

			if( ctrlImplLayers == null ) {
				/*
				 * Skip the case where the controller is a single class
				 * (not obtained by mixing layers).
				 */
				continue;
			}

			if( ctrlImplClassName.length() == 0 ) {
				/*
				 * For controllers implemented with Scala, the membrane
				 * description is typically of the form:
				 *
				 *         @Controller(
				 *             name="component",
				 *             mixins=ComponentControllerImpl.class)
				 *         protected Component comp;
				 *
				 * with an omitted implName and only one layer.
				 */
				ctrlImplClassName = ctrlDesc.getImplNameOrUniqueLayerImplName();
				ctrlDesc.setImplName(ctrlImplClassName);
				continue;
			}

			ctrlImplClassName =
				JuliacHelper.getJuliacGeneratedStrongTypeName(ctrlImplClassName);
			final String pkgRoot = jc.getPkgRoot();
			ctrlImplClassName = pkgRoot + ctrlImplClassName;
			ctrlDesc.setImplName(ctrlImplClassName);

			// Avoid duplicate generation
			if( ! jc.hasBeenGenerated(ctrlImplClassName) ) {
				SpoonSupportItf spoon = jc.lookupFirst(SpoonSupportItf.class);
				spoon.mixAndGenerate(ctrlImplClassName,ctrlImplLayers);
			}
		}
	}
}
