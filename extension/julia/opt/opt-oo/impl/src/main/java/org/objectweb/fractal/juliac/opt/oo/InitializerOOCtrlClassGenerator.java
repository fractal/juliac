/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.opt.oo;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.JuliacRuntimeException;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.CatchSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.helper.InterfaceTypeHelper;
import org.objectweb.fractal.juliac.core.helper.JuliacHelper;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.opt.InitializerClassGenerator;
import org.objectweb.fractal.juliac.core.proxy.AttributeControllerClassGenerator;

/**
 * Initializer generator for Fractal components with an object-oriented control
 * membrane and where the content, the interceptors and the controllers are kept
 * in separate classes.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 */
public class InitializerOOCtrlClassGenerator
extends InitializerClassGenerator {

	public InitializerOOCtrlClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		super(jc,fcscg,membraneDesc,ct,contentDesc,source);
	}

	@Override
	public String getSuperClassName() {
		MembraneInitializerOOCtrlClassGenerator micg =
			getMembraneInitializerClassGenerator();
		String name = micg.getTargetTypeName();
		return name;
	}

	@Override
	protected void generateGetFcControllerDescMethod( BlockSourceCodeVisitor mv ) {
		mv.visit  ("      return \"");
		mv.visit  (membraneDesc.getDescriptor());
		mv.visitln("\";");
	}

	@Override
	protected void generateGetFcContentDescMethod( BlockSourceCodeVisitor mv ) {
		mv.visit  ("      return ");
		mv.visit  (JuliacHelper.javaifyContentDesc(contentDesc).toString());
		mv.visitln(";");
	}

	@Override
	protected void generateNewFcContentMethod( BlockSourceCodeVisitor mv ) {

		String ctrlDesc = membraneDesc.getDescriptor();
		String contentClassName =
			JuliacHelper.getContentClassName(ctrlDesc,contentDesc);

		if( contentClassName == null ) {
			mv.visitIns("return null");
		}
		else {
			// Handle inner classes
			contentClassName = contentClassName.replace('$','.');
			CatchSourceCodeVisitor cv = mv.visitTry();
			mv.visitVar("Object","content","new",contentClassName+"()");
			mv.visitIns("return content");
			cv = cv.visitCatch(Throwable.class,"t");
			cv.visitIns(
				"throw","new",ChainedInstantiationException.class.getName(),
				"(t,null,\"\")" );
			cv.visitEnd();
		}
	}

	@Override
	protected void generateNewFcInstanceMethod( BlockSourceCodeVisitor mv ) {
		mv.visitVar("Object","content","newFcContent()");
		mv.visitIns("return","newFcInstance(content)");
	}

	@Override
	protected void generateNewFcInstanceContentMethod( BlockSourceCodeVisitor mv ) {

		InterfaceType[] membraneits = membraneDesc.getCtrlItfTypes();
		boolean isComposite = InterfaceTypeHelper.isComposite(membraneits);

		// Check the content
		generateContentChecks(mv);

		// Invoke the newFcInitializationContext method inherited from the MembraneInitializerClassGenerator
		mv.visitln("    "+InitializationContext.class.getName()+" ic = newFcInitializationContext(content);");

		// Store the content in the initialization context
		generateInitializationContextForContent(mv);

		// Declare local variables which will be used in the next steps
		mv.visitln("    "+Interface.class.getName()+" proxy;");
		mv.visitln("    Object intercept;");
		mv.visitln("    "+Component.class.getName()+" proxyForCompCtrl = ("+Component.class.getName()+") ic.interfaces.get(\"component\");");

		// /cloneable-attribute-controller
		boolean attrimplgenerated = false;
		for (int i=0; i < membraneits.length; i++) {
			String itname = membraneits[i].getFcItfName();
			if( itname.equals("/cloneable-attribute-controller") ) {
				generateNFCICMControlInterfaceCAC(mv,membraneits[i]);
				attrimplgenerated = true;
			}
		}

		// Component type
		InterfaceType[] its = ct.getFcInterfaceTypes();
		InterfaceType[] fullits = new InterfaceType[ membraneits.length + its.length ];
		System.arraycopy(membraneits, 0, fullits, 0, membraneits.length);
		System.arraycopy(its, 0, fullits, membraneits.length, its.length);
		mv.visitln("    ic.type = "+InterfaceTypeHelper.javaify(fullits)+";");

		// Business interfaces
		for (int i = 0; i < its.length; i++) {

			String itname = its[i].getFcItfName();

			if( its[i].isFcCollectionItf() ) {
				// Julia naming convention for collection interfaces
				itname = "/collection/" + itname;
			}

			// Interceptors
			if( ! generateInterceptorCreation(mv,its[i],"content") ) {
				mv.visit("    intercept = ");
				mv.visit( its[i].isFcClientItf() ? "null" : "content" );
				mv.visitln(";");
			}

			// External interface
			generateNFICMExternalInterface(mv,its[i],attrimplgenerated);

			// Internal interface (skip attribute control interfaces and primitive components)
			if( ! itname.equals("attribute-controller") && isComposite ) {
				InterfaceType intit = InterfaceTypeHelper.newSymetricInterfaceType(its[i]);
				SourceCodeGeneratorItf itfscg =
					fcscg.getInterfaceClassGenerator(intit);
				String fcitfClassname = itfscg.getTargetTypeName();
				String itstring = InterfaceTypeHelper.javaify(intit).toString();
				mv.visit  ("    proxy = ");
				mv.visit  ("new "+fcitfClassname+"(");
				mv.visit  ("proxyForCompCtrl,\""+itname+"\","+itstring);
				mv.visitln(",true,intercept);");  // interceptor or content or null
				mv.visitln("    ic.internalInterfaces.put(\""+itname+"\",proxy);");
			}
		}

		// Initialize controllers
		mv.visitln("    initFcController(ic);");

		// Return the fcinterface for the component controller
		mv.visitln("    return proxyForCompCtrl;");
	}

	/**
	 * CAC is a shortcut for /cloneable-attribute-controller.
	 *
	 * @since 2.3
	 */
	private void generateNFCICMControlInterfaceCAC(
		BlockSourceCodeVisitor mv, InterfaceType membraneit ) {

		String itname = membraneit.getFcItfName();
		SourceCodeGeneratorItf itfscg =
			fcscg.getInterfaceClassGenerator(membraneit);
		String fcitfClassname = itfscg.getTargetTypeName();
		String itstring = InterfaceTypeHelper.javaify(membraneit).toString();

		InterfaceType attrit = null;
		try {
			attrit = ct.getFcInterfaceType("attribute-controller");
		}
		catch (NoSuchInterfaceException e) {
			throw new JuliacRuntimeException(e);
		}

		String signature = attrit.getFcItfSignature();
		Class<?> cl = jc.loadClass(signature);
		final String pkgRoot = jc.getPkgRoot();
		SourceCodeGeneratorItf attrscg =
			new AttributeControllerClassGenerator(attrit,cl,pkgRoot,null,false);
		String s = attrscg.getTargetTypeName();

		mv.visitIns("Object attrimpl = new ",s,"()");
		mv.visitIns(
			"proxy = ","new ",fcitfClassname,"(","proxyForCompCtrl,\""+itname+
			"\",",itstring,",false,attrimpl)");
		mv.visitIns("ic.interfaces.put(\""+itname+"\",proxy)");
	}

	protected void generateNFICMExternalInterface(
		BlockSourceCodeVisitor mv, InterfaceType it,
		boolean attrimplgenerated ) {

		String itname = it.getFcItfName();

		if( it.isFcCollectionItf() ) {
			// Julia naming convention for collection interfaces
			itname = "/collection/" + itname;
		}

		// External interface
		SourceCodeGeneratorItf itfscg = fcscg.getInterfaceClassGenerator(it);
		String fcitfClassname = itfscg.getTargetTypeName();
		String itstring = InterfaceTypeHelper.javaify(it).toString();
		mv.visit("    proxy = ");
		mv.visit("new "+fcitfClassname+"(");
		mv.visit("proxyForCompCtrl,\""+itname+"\","+itstring+",false,");
		if( itname.equals("attribute-controller") && attrimplgenerated ) {
			mv.visit("attrimpl");
		}
		else {
			mv.visit("intercept");  // interceptor or content or null
		}
		mv.visitln(");");
		mv.visitln("    ic.interfaces.put(\""+itname+"\",proxy);");
	}


	// ----------------------------------------------------------------------
	// Membrane initializer class generator
	// ----------------------------------------------------------------------

	protected MembraneInitializerOOCtrlClassGenerator
	getMembraneInitializerClassGenerator() {
		MembraneInitializerOOCtrlClassGenerator micg =
			new MembraneInitializerOOCtrlClassGenerator(
				jc,fcscg,this,membraneDesc,source);
		return micg;
	}
}
