/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.opt.oo;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;
import org.objectweb.fractal.julia.loader.Initializable;
import org.objectweb.fractal.julia.loader.Tree;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.Constants;
import org.objectweb.fractal.juliac.core.desc.ControllerDesc;
import org.objectweb.fractal.juliac.core.helper.InterfaceTypeHelper;
import org.objectweb.fractal.juliac.core.helper.JuliacHelper;
import org.objectweb.fractal.juliac.core.helper.TreeHelper;
import org.objectweb.fractal.juliac.core.opt.ClassGenerator;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;

/**
 * Generator for object-oriented control membrane initializer. An membrane
 * initializer provides the code which is common to all component initializers
 * for a given membrane descriptor.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 * @since 2.2.2
 */
public class MembraneInitializerOOCtrlClassGenerator extends ClassGenerator {

	private JuliacItf jc;
	protected FCSourceCodeGeneratorItf fcscg;
	protected MembraneDesc<?> membraneDesc;
	protected InitializerOOCtrlClassGenerator icg;
	protected Object source;

	public MembraneInitializerOOCtrlClassGenerator(

		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		InitializerOOCtrlClassGenerator icg, MembraneDesc<?> membraneDesc,
		Object source ) {

		this.jc = jc;
		this.fcscg = fcscg;
		this.icg = icg;
		this.membraneDesc = membraneDesc;
		this.source = source;
	}

	public String getTargetTypeName() {
		String ctrlDesc = membraneDesc.getDescriptor();
		String strong = JuliacHelper.getJuliacGeneratedStrongTypeName(ctrlDesc);
		final String pkgRoot = jc.getPkgRoot();
		String rooted = pkgRoot + strong;
		return rooted;
	}

	@Override
	public int getTypeModifiers() {
		return Modifier.PUBLIC | Modifier.ABSTRACT;
	}

	@Override
	public String getSuperClassName() {
		return Constants.JULIAC_RUNTIME_MEMBRANE_INITIALIZER;
	}

	@Override
	public void generateMethods( ClassSourceCodeVisitor cv ) {
		generateNewFcInitializationContextMethod(cv);
		generateNewFcControllerInstantiationMethod(cv);
	}

	protected void generateNewFcInitializationContextMethod( ClassSourceCodeVisitor cv ) {
		BlockSourceCodeVisitor mv =
			cv.visitMethod(
				Modifier.PUBLIC, null, InitializationContext.class.getName(),
				"newFcInitializationContext", new String[]{"Object content"},
				new String[]{InstantiationException.class.getName()} );
		generateNewFcInitializationContextMethod(mv);
		mv.visitEnd();
	}

	protected void generateNewFcInitializationContextMethod( BlockSourceCodeVisitor mv ) {

		InterfaceType[] membraneits = membraneDesc.getCtrlItfTypes();
		boolean isComposite = InterfaceTypeHelper.isComposite(membraneits);

		// Prepare an InitializationContext
		mv.visitln("    "+InitializationContext.class.getName()+" ic = initFcInitializationContext();");
		if(isComposite) {
			mv.visitln("    ic.internalInterfaces = new "+HashMap.class.getName()+"();");
		}

		// Instantiate controllers
		mv.visitln("    "+Component.class.getName()+" compctrl = newFcControllerInstantiation(ic);");

		// Component controller
		for (int i=0; i < membraneits.length; i++) {

			InterfaceType compctrlit = membraneits[i];
			String itname = compctrlit.getFcItfName();

			// Skip all other controller except component
			if( ! itname.equals("component") ) {
				continue;
			}

			mv.visitSet("Object intercept","null");
			if( ! icg.generateInterceptorCreation(mv,compctrlit,"compctrl") ) {
				mv.visitSet("intercept","compctrl");
			}

			SourceCodeGeneratorItf itfscg =
				fcscg.getInterfaceClassGenerator(compctrlit);
			String fcitfClassname = itfscg.getTargetTypeName();
			generateNCICMProxyCreation(
				mv,true,fcitfClassname,"compctrl","component",compctrlit);
			mv.visit  ("    "+Component.class.getName()+" proxyForCompCtrl = ");
			mv.visitln("("+Component.class.getName()+") proxy;");
		}

		// Controllers other than the component controller
		mv.visitSet("Object ctrl","null");
		for (int i=0; i < membraneits.length; i++) {

			InterfaceType membraneit = membraneits[i];
			String itname = membraneit.getFcItfName();

			// Skip component and /cloneable-attribute-controller
			if( itname.equals("component") ||
				itname.equals("/cloneable-attribute-controller") ) {
				continue;
			}

			// Control external interface
			mv.visitSet(
				"ctrl",
				"getFcController(ic,content,",membraneit.getFcItfSignature(),".class)");

			// Interceptors
			if( ! icg.generateInterceptorCreation(mv,membraneit,"ctrl") ) {
				mv.visitSet("intercept","ctrl");
			}

			// Proxy for the interface
			SourceCodeGeneratorItf itfscg =
				fcscg.getInterfaceClassGenerator(membraneit);
			String fcitfClassname = itfscg.getTargetTypeName();
			generateNCICMProxyCreation(
				mv,false,fcitfClassname,"proxyForCompCtrl",itname,membraneit);

			// Control internal interface
			if( itname.equals("factory") && isComposite ) {
				InterfaceType intit = InterfaceTypeHelper.newSymetricInterfaceType(membraneit);
				itfscg = fcscg.getInterfaceClassGenerator(intit);
				fcitfClassname = itfscg.getTargetTypeName();
				String itstring = InterfaceTypeHelper.javaify(intit).toString();
				mv.visit  ("    proxy = ");
				mv.visit  ("new "+fcitfClassname+"(");
				mv.visit  ("proxyForCompCtrl,\""+itname+"\","+itstring);
				mv.visitln(",true,intercept);");
				mv.visitln("    ic.internalInterfaces.put(\""+itname+"\",proxy);");
			}
		}

		mv.visitIns("return ic");
	}

	protected void generateNewFcControllerInstantiationMethod( ClassSourceCodeVisitor cv ) {
		BlockSourceCodeVisitor mv =
			cv.visitMethod(
				Modifier.PUBLIC, null, Component.class.getName(),
				"newFcControllerInstantiation",
				new String[]{InitializationContext.class.getName()+" ic"},
				new String[]{InstantiationException.class.getName()} );
		generateNewFcControllerInstantiationMethod(mv);
		mv.visitEnd();
	}

	protected void generateNewFcControllerInstantiationMethod( BlockSourceCodeVisitor mv ) {

		List<ControllerDesc> ctrlDescs = (List<ControllerDesc>) membraneDesc.getCtrlDescs();

		mv.visitln("    Object ctrl = null;");

		for (ControllerDesc ctrlDesc : ctrlDescs) {
			String ctrlImplName = ctrlDesc.getImplName();
			if( ctrlImplName.charAt(0) == '(' ) {
				/*
				 * Skip CloneableAttributeController implementations.
				 * The controller implementation class name is in this case
				 * something like:
				 *
				 * ((attribute-controller org.objectweb.fractal.julia.conform.components.CAttributes))
				 */
				continue;
			}
			mv.visitln("    ic.controllers.add(ctrl = new "+ctrlImplName+"());");
			Class<?> cl = jc.loadClass(ctrlImplName);
			if( Initializable.class.isAssignableFrom(cl) ) {
				Tree tree = ctrlDesc.getTree();
				if( tree != null ) {
					mv.visitln("    "+Tree.class.getName()+" tree = "+TreeHelper.javaify(tree)+';');
					mv.visitln("    try {");
					mv.visitln("      (("+Initializable.class.getName()+")ctrl).initialize(tree);");
					mv.visitln("    }");
					mv.visitln("    catch( Exception e ) {");
					mv.visitln("      throw new "+ChainedInstantiationException.class.getName()+"(e,null,\"\");");
					mv.visitln("    }");
				}
			}
			if( Component.class.isAssignableFrom(cl) ) {
				mv.visitln("    "+Component.class.getName()+" compctrl = ("+Component.class.getName()+")ctrl;");
			}
		}

		mv.visitln("    return compctrl;");
	}

	protected void generateNCICMProxyCreation(
		BlockSourceCodeVisitor mv, boolean typedef, String fcitfClassname,
		String delegate, String itname, InterfaceType it ) {

		String itstring = InterfaceTypeHelper.javaify(it).toString();
		mv.visitIns(
			typedef ? Interface.class.getName() : "","proxy = new",
			fcitfClassname,"(",delegate,",\""+itname+"\",",itstring,
			",false,intercept)");
		mv.visitIns("ic.interfaces.put(\""+itname+"\",proxy)");
	}

	@Override
	public Object getSourceType() {
		return source;
	}
}
