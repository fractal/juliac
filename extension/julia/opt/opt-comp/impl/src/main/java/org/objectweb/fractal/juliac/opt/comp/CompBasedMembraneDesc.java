/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.opt.comp;

import java.util.List;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.loader.Tree;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.JuliacRuntimeException;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.InterceptorSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.generator.InterfaceTypeConfigurableItf;
import org.objectweb.fractal.juliac.core.desc.AttributeDesc;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;
import org.objectweb.fractal.juliac.core.proxy.InterceptorClassGenerator;
import org.objectweb.fractal.koch.control.interceptor.InterceptorController;
import org.objectweb.fractal.koch.loader.TreeParser;
import org.objectweb.fractal.koch.loader.TreeParserException;

/**
 * A membrane descriptor where the structure of the membrane is component-based.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2
 */
public class CompBasedMembraneDesc extends MembraneDesc<ComponentDesc<?>> {

	/**
	 * Create a new membrane descriptor.
	 *
	 * With this membrane descriptor, we should be able to deduce the control
	 * interface types <code>ctrlItfTypes</code> from the component-based
	 * membrane <code>ctrlDescs</code>. Yet it happens that in the COMP mode, we
	 * need, in order to generate interceptors, to construct membrane
	 * descriptors where the control interface types are either level 0 types
	 * (e.g. binding-controller) or meta-level types (e.g.
	 * //binding-controller), while keeping the same component-based control
	 * membrane.
	 *
	 * All this is related to the fact that lifecycle interceptors reference the
	 * lifecycle controller implementation classes, and have thus to access the
	 * membrane descriptor. If that was not the case, interceptors could be
	 * generated based only on the intercepted type.
	 */
	public CompBasedMembraneDesc(
		JuliacItf jc, String name, String descriptor,
		InterfaceType[] ctrlItfTypes, ComponentDesc<?> ctrlDescs ) {

		super(jc,name,descriptor,ctrlItfTypes,ctrlDescs,null,null);

		Tree t = getInterceptorGeneratorTree(ctrlDescs);
		if( t != null ) {

			interceptorSourceCodeGenerators =
				new InterfaceTypeConfigurableItf[ t.getSize() - 1 ];
			for (int i = 0; i < interceptorSourceCodeGenerators.length; i++) {
				Tree tti = t.getSubTree(i+1);
				String ttiname = tti.toString();
				interceptorSourceCodeGenerators[i] =
					getInterceptorSourceCodeGeneratorFromName(ttiname);
				interceptorSourceCodeGenerators[i].setMembraneDesc(this);
			}

			Tree tt0 = t.getSubTree(0);
			String tt0name = tt0.toString();
			interceptorClassGenerator =
				getInterceptorClassGeneratorFromName(
					tt0name,interceptorSourceCodeGenerators);
			interceptorClassGenerator.setMembraneDesc(this);
		}
	}

	/**
	 * Return the implementation class of the control component implementing the
	 * specified signature.
	 *
	 * @param signature  the signature
	 * @return           the class of the control component
	 * @throws IllegalArgumentException  if there is no such control component
	 */
	@Override
	public Class<?> getCtrlImpl( String signature ) {

		Class<?> ctrlItf = jc.loadClass(signature);

		Class<?> cl = getCtrlImpl(ctrlDescs,ctrlItf);
		if( cl == null ) {
			final String msg =
				"No control component implementing "+signature+
				" in controller descriptor "+getDescriptor();
			throw new IllegalArgumentException(msg);
		}

		return cl;
	}


	// ------------------------------------------------------------------
	// Implementation specific
	// ------------------------------------------------------------------

	/**
	 * Retrieve the interceptor component associated with the specified
	 * component descriptor and return the {@link Tree} corresponding to the
	 * interceptor generator.
	 *
	 * @since 2.8
	 */
	private Tree getInterceptorGeneratorTree( ComponentDesc<?> cdesc ) {

		ComponentDesc<?> icdesc =
			cdesc.getBoundComponent(
				"//"+InterceptorController.NAME,"mPrimitive");
		if( icdesc == null ) {
			/*
			 * No component bound the the ///interceptor-controller interface.
			 * In the case of the MERGE_CTRL mode, the interceptor component is
			 * merged with the current component.
			 */
			icdesc = cdesc;
		}

		AttributeDesc adesc = icdesc.getAttribute("interceptors");
		if( adesc == null ) {
			// No interceptor generator for this component
			return null;
		}

		/*
		 * Get the configuration string stored in the interceptors property and
		 * construct the corresponding Tree.
		 */
		String cfg = adesc.getValue();
		TreeParser tp = new TreeParser(cfg);
		try {
			Tree t = tp.parseTree();
			return t;
		}
		catch( TreeParserException tpe ) {
			throw new JuliacRuntimeException(tpe);
		}
	}

	/** @since 2.8 */
	protected InterfaceTypeConfigurableItf getInterceptorClassGeneratorFromName(
		String name, InterfaceTypeConfigurableItf[] itcs ) {

		Class<InterceptorClassGenerator> cl = jc.loadClass(name);
		InterceptorClassGenerator o = jc.instantiate(cl);

		// Casting the array as a whole generates a ClassCastException
		InterceptorSourceCodeGeneratorItf[] iscgs =
			new InterceptorSourceCodeGeneratorItf[itcs.length];
		for (int i = 0; i < iscgs.length; i++) {
			iscgs[i] = (InterceptorSourceCodeGeneratorItf) itcs[i];
		}

		o.init(iscgs);

		return o;
	}

	/** @since 2.8 */
	protected InterfaceTypeConfigurableItf getInterceptorSourceCodeGeneratorFromName(
		String name ) {

		Class<InterfaceTypeConfigurableItf> cl = jc.loadClass(name);
		InterfaceTypeConfigurableItf o = jc.instantiate(cl);
		return o;
	}

	/**
	 * Return the implementation class of the control component implementing the
	 * specified control interface. Return <code>null</code> if no control
	 * component implements the specified control interface.
	 *
	 * @param cdesc    the control component
	 * @param ctrlItf  the control interface
	 * @return         the class of the control component or <code>null</code>
	 */
	private <CT> Class<?> getCtrlImpl( ComponentDesc<CT> cdesc, Class<?> ctrlItf ) {

		/*
		 * For component with a content class (primitive), check whether the
		 * content class implements the specified control interface.
		 */
		String contentClassName = cdesc.getContentClassName();
		if( contentClassName != null ) {
			Class<?> cl = jc.loadClass(contentClassName);
			if( ctrlItf.isAssignableFrom(cl) ) {
				return cl;
			}
		}

		/*
		 * Iterate on sub components.
		 */
		List<ComponentDesc<CT>> subs = cdesc.getSubComponents();
		for (ComponentDesc<CT> sub : subs) {
			Class<?> uc = getCtrlImpl(sub,ctrlItf);
			if( uc != null ) {
				return uc;
			}
		}

		// No component implements the specified control interface
		return null;
	}
}
