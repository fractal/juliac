/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.opt.comp;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.InterceptorSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.proxy.InterceptorClassGenerator;

/**
 * A membrane descriptor where the structure of the membrane is represented by a
 * single class, e.g. MPrimitiveImpl or MCompositeImpl. This is a membrane
 * descriptor for control components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2
 */
public class MMembraneDesc extends MembraneDesc<Class<?>> {

	public MMembraneDesc(
		JuliacItf jc, String name,
		String descriptor, InterfaceType[] ctrlItfTypes, Class<?> ctrlDescs ) {

		super(jc,name,descriptor,ctrlItfTypes,ctrlDescs,null,null);

		if( descriptor.equals("mPrimitive") ) {
			init();
		}
	}

	/** @since 2.8 */
	protected void init() {

		InterceptorSourceCodeGeneratorItf iscg =
			new LifeCycleSourceCodeGenerator();
		iscg.setMembraneDesc(this);
		InterceptorSourceCodeGeneratorItf[] iscgs =
			new InterceptorSourceCodeGeneratorItf[] {iscg};
		interceptorSourceCodeGenerators = iscgs;

		interceptorClassGenerator = new InterceptorClassGenerator();
		((InterceptorClassGenerator)interceptorClassGenerator).init(iscgs);
		interceptorClassGenerator.setMembraneDesc(this);
	}

	/**
	 * Return the implementation class of the controller implementing the
	 * specified signature.
	 *
	 * @param signature  the signature
	 * @return           the class of the controller
	 * @throws IllegalArgumentException  if there is no such controller
	 */
	@Override
	public Class<?> getCtrlImpl( String signature ) {

		Class<?> ctrlDescs = getCtrlDescs();
		Class<?> ctrlItf = jc.loadClass(signature);
		Class<?> uc = jc.loadClass(ctrlDescs.getName());

		if( ctrlItf.isAssignableFrom(uc) ) {
			return uc;
		}

		final String msg =
			"No controller implementing "+signature+
			" in controller descriptor "+getDescriptor();
		throw new IllegalArgumentException(msg);
	}
}
