/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.opt.comp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.Interceptor;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.helper.InterfaceTypeHelper;
import org.objectweb.fractal.juliac.core.helper.JuliacHelper;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.opt.InitializerClassGenerator;
import org.objectweb.fractal.juliac.runtime.comp.MembraneInitializer;

/**
 * Initializer generator for Fractal components with a component-based control
 * membrane.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 */
public class InitializerCompCtrlClassGenerator
extends InitializerClassGenerator {

	public InitializerCompCtrlClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		super(jc,fcscg,membraneDesc,ct,contentDesc,source);
	}

	@Override
	public String getSuperClassName() {
		return MembraneInitializer.class.getName();
	}

	@Override
	protected void generateGetFcControllerDescMethod( BlockSourceCodeVisitor mv ) {
		mv.visit  ("      return \"");
		mv.visit  (membraneDesc.getDescriptor());
		mv.visitln("\";");
	}

	@Override
	protected void generateGetFcContentDescMethod( BlockSourceCodeVisitor mv ) {
		mv.visit  ("      return ");
		mv.visit  (JuliacHelper.javaifyContentDesc(contentDesc).toString());
		mv.visitln(";");
	}

	@Override
	protected void generateNewFcContentMethod( BlockSourceCodeVisitor mv ) {

		String ctrlDesc = membraneDesc.getDescriptor();
		String contentClassName =
			JuliacHelper.getContentClassName(ctrlDesc,contentDesc);

		if( contentClassName == null ) {
			mv.visitln("    return null;");
		}
		else {
			mv.visitln("    try {");
			mv.visit  ("      Object content = new ");
			mv.visit  (contentClassName);
			mv.visitln("();");
			mv.visitln("      return content;");
			mv.visitln("    }");
			mv.visitln("    catch( Throwable t ) {");
			mv.visit  ("      throw new ");
			mv.visit  (ChainedInstantiationException.class.getName());
			mv.visitln("(t,null,\"\");");
			mv.visitln("    }");
		}
	}

	@Override
	protected void generateNewFcInstanceMethod( BlockSourceCodeVisitor mv ) {
		mv.visitVar("Object","content","newFcContent()");
		mv.visitIns("return","newFcInstance(content)");
	}

	@Override
	protected void generateNewFcInstanceContentMethod( BlockSourceCodeVisitor mv ) {

		String membraneName = membraneDesc.getName();
		InterfaceType[] mits = membraneDesc.getCtrlItfTypes();

		// Instantiate the membrane factory
		mv.visit  ("    ");
		mv.visit  (Factory.class.getName());
		mv.visit  (" membraneFactory = new ");
		mv.visit  (membraneName);
		mv.visitln("();");

		// Instantiate and start the membrane
		mv.visitSet(
			Component.class.getName()+" membrane",
			"instantiateAndStartMembrane(membraneFactory)");

		// Prepare an InitializationContext
		mv.visitln("    "+InitializationContext.class.getName()+" ic = new "+InitializationContext.class.getName()+"();");
		mv.visitln("    ic.controllers = new "+ArrayList.class.getName()+"();");
		mv.visitln("    ic.interfaces = new "+HashMap.class.getName()+"();");
		mv.visitln("    ic.internalInterfaces = new "+HashMap.class.getName()+"();");

		// Instantiate the content
		generateInitializationContextForContent(mv);

		// Retrieve all controllers
		mv.visitln("    java.util.Map ctrls = new java.util.HashMap();");
		mv.visitIns("retrieveAllControllers(membrane,ic,ctrls)");

		// Component controller
		InterfaceType compctrlit =
			new BasicInterfaceType("component",Component.class.getName(),false,false,false);
		SourceCodeGeneratorItf itfscg =
			fcscg.getInterfaceClassGenerator(compctrlit);
		String fcitfClassname = itfscg.getTargetTypeName();
		String itstring = InterfaceTypeHelper.javaify(compctrlit).toString();
		mv.visit  ("    "+Interface.class.getName()+" compctrlitf = ("+Interface.class.getName()+")");
		mv.visitln("getFcInterface(membrane,\"//component\");");
		mv.visit  ("    "+Component.class.getName()+" compctrl = (");
		mv.visitln(Component.class.getName()+") compctrlitf;");
		mv.visitln("    Object intercept = null;");
		boolean interceptorCreated =
			generateInterceptorCreation(mv,compctrlit,"compctrlitf");
		if( ! interceptorCreated ) {
			mv.visitln("    intercept = compctrl;");
		}
		mv.visit  ("    "+Interface.class.getName()+" proxy = ");
		mv.visit  ("new "+fcitfClassname+"(");
		mv.visitln("compctrl,\"component\","+itstring+",false,intercept);");
		mv.visit  ("    "+Component.class.getName()+" proxyForCompCtrl = ");
		mv.visitln("("+Component.class.getName()+") proxy;");
		if(interceptorCreated) {
			generateInterceptorPostInit(mv,compctrlit,"proxy");
		}
		mv.visitln("    ic.interfaces.put(\"component\",proxy);");

		List<InterfaceType> fullITs = new ArrayList<>();
		fullITs.add(compctrlit);

		mv.visitln("    Object cloneableAttrCtrlImpl = null;");
		mv.visitln("    Object delegate = null;");

		// Controllers other than the component controller
		for (InterfaceType srcit : mits) {
			String name = srcit.getFcItfName();

			if( name.startsWith("//") && !name.equals("//component") ) {

				compctrlit = InterfaceTypeHelper.downToLevel0InterfaceType(srcit);
				itfscg = fcscg.getInterfaceClassGenerator(compctrlit);
				fcitfClassname = itfscg.getTargetTypeName();
				String itname = compctrlit.getFcItfName();
				itstring = InterfaceTypeHelper.javaify(compctrlit).toString();

				// Interceptors
				interceptorCreated =
					generateInterceptorCreation(
						mv,compctrlit,"getFcInterface(membrane,\""+name+"\")");
				if( ! interceptorCreated ) {
					mv.visitln("    intercept = getFcInterface(membrane,\""+name+"\");");
				}

				// Control external interface
				mv.visit  ("    proxy = ");
				mv.visit  ("new "+fcitfClassname+"(");
				mv.visit  ("proxyForCompCtrl,\""+itname+"\","+itstring+",false,");
				mv.visit  ("intercept");
				mv.visitln(");");
				if(interceptorCreated) {
					generateInterceptorPostInit(mv,compctrlit,"proxy");
				}
				mv.visitln("    ic.interfaces.put(\""+itname+"\",proxy);");

				// Control internal interface
				if( name.equals("//factory") ) {
					InterfaceType intit = InterfaceTypeHelper.newSymetricInterfaceType(compctrlit);
					itstring = InterfaceTypeHelper.javaify(intit).toString();

					mv.visit  ("    proxy = ");
					mv.visit  ("new "+fcitfClassname+"(");
					mv.visit  ("proxyForCompCtrl,\""+itname+"\","+itstring+",true,");
					mv.visit  ("intercept");
					mv.visitln(");");
					mv.visitln("    ic.internalInterfaces.put(\""+itname+"\",proxy);");
				}

				// Cloneable attribute controller implementation
				if( name.equals("///cloneable-attribute-controller") ) {
					mv.visitln("    cloneableAttrCtrlImpl = proxy;");
					mv.visitln("    while( cloneableAttrCtrlImpl instanceof "+ComponentInterface.class.getName()+" ) {");
					mv.visitln("       "+ComponentInterface.class.getName()+" ci = ("+ComponentInterface.class.getName()+") cloneableAttrCtrlImpl;");
					mv.visitln("       cloneableAttrCtrlImpl = ci.getFcItfImpl();");
					mv.visitln("    }");
					mv.visitln("    while( cloneableAttrCtrlImpl instanceof "+Interceptor.class.getName()+" ) {");
					mv.visitln("       "+Interceptor.class.getName()+" ci = ("+Interceptor.class.getName()+") cloneableAttrCtrlImpl;");
					mv.visitln("       cloneableAttrCtrlImpl = ci.getFcItfDelegate();");
					mv.visitln("    }");
				}

				// Component type
				if( ! name.startsWith("///") ) {
					fullITs.add(compctrlit);
				}
			}
		}

		// Component type = control type + business type
		InterfaceType[] its = ct.getFcInterfaceTypes();
		fullITs.addAll( Arrays.asList(its) );
		InterfaceType[] full = fullITs.toArray( new InterfaceType[fullITs.size()] );
		mv.visitln("    ic.type = "+InterfaceTypeHelper.javaify(full)+";");

		// Business interfaces
		for (int i = 0; i < its.length; i++) {

			String itname = its[i].getFcItfName();

			if( its[i].isFcCollectionItf() ) {
				// Julia naming convention for collection interfaces
				itname = "/collection/" + itname;
			}

			// Interceptors
			interceptorCreated =
				generateInterceptorCreation(mv,its[i],"content");
			if( ! interceptorCreated ) {
				mv.visit("    intercept = ");
				mv.visit( its[i].isFcClientItf() ? "null" : "content" );
				mv.visitln(";");
			}

			/*
			 * If an implementation of a cloneable attribute controller is
			 * available (this is the case for parametric primitive and
			 * composite templates), let the attribute controller interface
			 * delegate to this instance instead of the content.
			 *
			 * The idea is that the state of the component (as defined by the
			 * attributes) must be cloned when a new instance of the template is
			 * created. Hence, each setting of a attribute must be directed
			 * towards the cloneable attribute controller to let it be cloned
			 * latter on.
			 */
			mv.visit  ("    delegate = ");
			if( itname.equals("attribute-controller") ) {
				mv.visit("cloneableAttrCtrlImpl!=null ? ");
				mv.visit("cloneableAttrCtrlImpl : content");
			}
			else {
				mv.visit("intercept");  // interceptor or content or null
			}
			mv.visitln(";");

			// External interface
			generateNFICMExternalInterface(mv,its[i]);
			if(interceptorCreated) {
				generateInterceptorPostInit(mv,its[i],"proxy");
			}

			// Internal interface (skip attribute control interfaces)
			if( ! itname.equals("attribute-controller") ) {
				InterfaceType intit = InterfaceTypeHelper.newSymetricInterfaceType(its[i]);
				itfscg = fcscg.getInterfaceClassGenerator(intit);
				fcitfClassname = itfscg.getTargetTypeName();
				itstring = InterfaceTypeHelper.javaify(intit).toString();
				mv.visit  ("    proxy = ");
				mv.visit  ("new "+fcitfClassname+"(");
				mv.visitln("proxyForCompCtrl,\""+itname+"\","+itstring+",true,delegate);");
				mv.visitln("    ic.internalInterfaces.put(\""+itname+"\",proxy);");
				if( its[i].isFcCollectionItf() ) {
					mv.visitln("    ic.internalInterfaces.put(\"/juliak"+itname+"\",proxy);");
				}
			}
		}

		/*
		 * Initialize controllers.
		 *
		 * Iterate on ic.controllers which is a List, to initialize the
		 * component controller first, and the ContentController second.
		 * When initialized, controllers (e.g. the interceptor controller) may
		 * assume that the component controller has already been initialized to
		 * retrieve for example, the component type or the array of internal
		 * fcinterfaces.
		 */
		mv.visitIns("initializeControllers(ic,ctrls)");

		// Return the fcinterface for the component controller
		mv.visitln("    return proxyForCompCtrl;");
	}

	protected void generateInterceptorPostInit(
		BlockSourceCodeVisitor mv, InterfaceType it, String delegate ) {

		// Indeed nothing. Overriden in Tinfi.
	}

	protected void generateNFICMExternalInterface(
		BlockSourceCodeVisitor mv, InterfaceType it ) {

		String itname = it.getFcItfName();

		if( it.isFcCollectionItf() ) {
			// Julia naming convention for collection interfaces
			itname = "/collection/" + itname;
		}

		// External interface
		SourceCodeGeneratorItf itfscg =
			fcscg.getInterfaceClassGenerator(it);
		String fcitfClassname = itfscg.getTargetTypeName();
		String itstring = InterfaceTypeHelper.javaify(it).toString();
		mv.visit  ("    proxy = ");
		mv.visit  ("new "+fcitfClassname+"(");
		mv.visitln("proxyForCompCtrl,\""+itname+"\","+itstring+",false,delegate);");
		mv.visitln("    ic.interfaces.put(\""+itname+"\",proxy);");
		if( it.isFcCollectionItf() ) {
			/*
			 * Add a 2nd reference for this interface.
			 * It will be possible to retrieve this reference to dynamically
			 * manage (add and remove) the interceptors associated to a
			 * collection interface. This is not possible with
			 * "/collection/"+name which is never returned as this by
			 * getFcInterface (a clone is always returned).
			 */
			mv.visitln("    ic.interfaces.put(\"/juliak"+itname+"\",proxy);");
		}
	}
}
