/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.opt.comp;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.loader.Tree;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.core.Constants;
import org.objectweb.fractal.juliac.core.desc.ADLParserSupportItf;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;
import org.objectweb.fractal.juliac.core.helper.InterfaceTypeHelper;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGenerator;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.opt.InitializerClassGenerator;
import org.objectweb.fractal.koch.Koch;

/**
 * This class generates the source code associated to Fractal components.
 * The membrane implementation and the initializer implementation are generated.
 *
 * The content, the interceptors and the controllers are kept in separate
 * classes and the controllers are implemented with components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class FCCompCtrlSourceCodeGenerator extends FCSourceCodeGenerator {

	public static final String KOCK_PREFIX = "/koch/";

	/** @since 2.7 */
	public static final String DEFAULT_CONFIGURATION =
		"juliac-comp-bundled.cfg";

	// -----------------------------------------------------------------------
	// Implementation of the JuliacModuleItf interface
	// -----------------------------------------------------------------------

	@Override
	protected void postInitJuliaLoaderModule() throws IOException {
		jloader.loadConfigFile(Koch.DEFAULT_CONFIGURATION);
		jloader.loadConfigFile(DEFAULT_CONFIGURATION);
		super.postInitJuliaLoaderModule();
	}


	// -----------------------------------------------------------------------
	// Implementation of the FCSourceCodeGeneratorItf interface
	// -----------------------------------------------------------------------

	/**
	 * Return <code>true</code> if the current source code generator handles the
	 * specified controller descriptor.
	 *
	 * @param controllerDesc  the controller descriptor to be checked
	 * @return                if <code>controllerDesc</code> should be included
	 * @since 2.2
	 */
	@Override
	public boolean test( Object controllerDesc ) {

		if( ! (controllerDesc instanceof String) ) {
			return false;
		}
		String ctrlDesc = (String) controllerDesc;

		String key = KOCK_PREFIX+ctrlDesc;
		boolean accept = mloader.containsKey(key);
		return accept;
	}

	@Override
	public MembraneDesc<?> generateMembraneImpl(
		ComponentType ct, String ctrldesc, String contentClassName,
		Object source )
	throws IOException {

		/*
		 * Generate business interface implementations.
		 */
		InterfaceType[] its = ct.getFcInterfaceTypes();
		generateInterfaceImpl(its,ctrldesc);

		/*
		 * Generate the implementation of the component-based control membrane.
		 */
		String classname = getMembraneClassName(ctrldesc);
		ComponentDesc<?> cdesc = null;
		if( cdescs.containsKey(classname) ) {
			cdesc = cdescs.get(classname);
		}
		else {
			cdesc = getComponentDescFromADL(ct,ctrldesc,classname);
			cdescs.put(classname,cdesc);
		}

		/*
		 * Prepare the membrane descriptor.
		 *
		 * Prepare a 2nd membrane descriptor with level 0 interface type names
		 * (e.g. stat-controller), instead of meta-level interface type names
		 * (e.g. //stat-controller). The reason is that, when we need to
		 * generate interceptors for control interfaces, such as with Tinfi,
		 * interceptors expect level 0 interface types, not meta-level ones.
		 */
		InterfaceType[] mits = cdesc.getCT().getFcInterfaceTypes();
		MembraneDesc<?> md = getMembraneDesc(jc,classname,ctrldesc,mits,cdesc);

		InterfaceType[] membraneits = InterfaceTypeHelper.downToLevel0InterfaceType(mits);
		MembraneDesc<?> mdintercept =
			getMembraneDesc(jc,classname,ctrldesc,membraneits,cdesc);

		/*
		 * Generate interceptor implementations.
		 * Interceptors must be generated after controllers as some interceptors
		 * (e.g. the lifecycle interceptor) use controller implementation
		 * class names.
		 */
		generateInterceptorImpl(its,mdintercept);
		generateInterceptorImpl(membraneits,mdintercept);

		return md;
	}

	/**
	 * Descriptors of already generated membranes.
	 * The key is the name of the class implementing the membrane.
	 */
	private Map<String,ComponentDesc<?>> cdescs = new HashMap<>();

	@Override
	protected InitializerClassGenerator
	getInitializerClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		return new InitializerCompCtrlClassGenerator(
			jc,fcscg,membraneDesc,ct,contentDesc,source);
	}

	/** @since 2.8 */
	protected MembraneDesc<?> getMembraneDesc(
		JuliacItf jc, String name, String descriptor,
		InterfaceType[] ctrlItfTypes, ComponentDesc<?> ctrlDescs ) {

		return new CompBasedMembraneDesc(jc,name,descriptor,ctrlItfTypes,ctrlDescs);
	}


	// ------------------------------------------------------------------
	// Implementation specific
	// ------------------------------------------------------------------

	/**
	 * @since 2.8
	 */
	protected String getMembraneClassName( String ctrlDesc ) {

		/*
		 * Use the name of the ADL for the factory class, unless this name
		 * corresponds to the name of one the membranes defined in
		 * org.objectweb.fractal.koch.membrane. The reason is that the factory
		 * classes for these membranes are already pre-generated in
		 * koch-runtime-2.5.2.3. When there is such a name conflict, we
		 * generate the factory class in juliac.generated using the controller
		 * descriptor as the name of the factory class.
		 */
		String classname = null;
		String adl = getADLDesc(ctrlDesc);
		if( adl.startsWith("org.objectweb.fractal.koch.membrane") ) {
			classname = Constants.JULIAC_RUNTIME_GENERATED+'.'+ctrlDesc;
		}
		else {
			classname = adl;
		}
		String pkgRoot = jc.getPkgRoot();
		classname = pkgRoot + classname;

		return classname;
	}

	protected ComponentDesc<?> getComponentDescFromADL(
		ComponentType ct, String ctrlDesc, String classname )
	throws IOException {

		Map<Object,Object> cclcontext = new HashMap<>();
		try {
			InterfaceType attrit =
				ct.getFcInterfaceType("attribute-controller");
			String attrsig = attrit.getFcItfSignature();
			cclcontext.put("attributeControllerInterface",new Tree(attrsig));
		}
		catch( NoSuchInterfaceException e ) {}

		/*
		 * Generate the factory for the membrane.
		 */
		String adl = getADLDesc(ctrlDesc);
		ADLParserSupportItf fadl =
			jc.lookupAndFilter(ADLParserSupportItf.class,adl);
		ComponentDesc<?> cdesc = fadl.parse(adl,cclcontext);
		fadl.generate(cdesc,classname);

		return cdesc;
	}

	/**
	 * Return the fully-qualified name of the ADL descriptor associated to the
	 * specified controller descriptor.
	 */
	protected String getADLDesc( String ctrlDesc ) {
		Tree t = jloader.loadTree(KOCK_PREFIX+ctrlDesc);
		// may throw ArrayOutOfBoundException
		String adl = t.getSubTree(0).toString();
		return adl;
	}
}
