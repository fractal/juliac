/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.opt.comp;

import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.Interceptor;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.InterfaceTypeConfigurableItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.helper.InterfaceTypeHelper;
import org.objectweb.fractal.juliac.core.helper.JuliacHelper;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.opt.InitializerClassGenerator;
import org.objectweb.fractal.koch.control.component.ComponentControllerDef;
import org.objectweb.fractal.koch.control.lifecycle.LifeCycleControllerDef;

/**
 * Initializer generator for Fractal mPrimitive and mComposite components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 */
public class InitializermCompCtrlClassGenerator
extends InitializerClassGenerator {

	public InitializermCompCtrlClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		super(jc,fcscg,membraneDesc,ct,contentDesc,source);
	}

	@Override
	protected void generateGetFcControllerDescMethod( BlockSourceCodeVisitor mv ) {
		mv.visit  ("      return \"");
		mv.visit  (membraneDesc.getDescriptor());
		mv.visitln("\";");
	}

	@Override
	protected void generateGetFcContentDescMethod( BlockSourceCodeVisitor mv ) {
		mv.visit  ("      return ");
		mv.visit  (JuliacHelper.javaifyContentDesc(contentDesc).toString());
		mv.visitln(";");
	}

	@Override
	protected void generateNewFcContentMethod( BlockSourceCodeVisitor mv ) {

		String ctrlDesc = membraneDesc.getDescriptor();
		String contentClassName = JuliacHelper.getContentClassName(ctrlDesc,contentDesc);

		mv.visit("    Object content = ");
		if( contentClassName == null ) {
			mv.visitln("null;");
		}
		else {
			mv.visit  ("new ");
			mv.visit  (contentClassName);
			mv.visitln("();");
		}
		mv.visitln("    return content;");
	}

	@Override
	protected void generateNewFcInstanceMethod( BlockSourceCodeVisitor mv ) {
		mv.visitln("    Object content = newFcContent();");
		mv.visitln("    return newFcInstance(content);");
	}

	@Override
	protected void generateNewFcInstanceContentMethod( BlockSourceCodeVisitor mv ) {

		String ctrlDesc = membraneDesc.getDescriptor();
		String contentClassName = JuliacHelper.getContentClassName(ctrlDesc,contentDesc);

		// Instantiate the control class
		String mcompclname = membraneDesc.getName();
		mv.visit  ("    ");
		mv.visit  (mcompclname);
		mv.visit  (" mcomp = new ");
		mv.visit  (mcompclname);
		mv.visitln("(getFcInstanceType(),content);");

		InterfaceType[] its = ct.getFcInterfaceTypes();

		// Prepare a Map for holding interfaces
		mv.visitln("    java.util.Map fcInterfaces = new java.util.HashMap();");
		if( ctrlDesc.equals("mComposite") ) {
			mv.visitln("    java.util.Map fcInternalInterfaces = new java.util.HashMap();");
		}
		mv.visitln("    "+Interface.class.getName()+" proxy = null;");

		// Control interfaces
		InterfaceType[] membraneits = membraneDesc.getCtrlItfTypes();
		for (int i=0 ; i < membraneits.length; i++) {

			String itname = membraneits[i].getFcItfName();
			SourceCodeGeneratorItf itfscg =
				fcscg.getInterfaceClassGenerator(membraneits[i]);
			String fcitfClassname = itfscg.getTargetTypeName();
			String itstring = InterfaceTypeHelper.javaify(membraneits[i]).toString();

			mv.visit  ("    proxy = ");
			mv.visit  ("new "+fcitfClassname+"(");
			mv.visitln("mcomp,\""+itname+"\","+itstring+",false,mcomp);");
			mv.visitln("    fcInterfaces.put(\""+itname+"\",proxy);");
		}

		// Prepare a context for initializing lifecycle interceptors
		boolean interceptgenerated = false;
		if( ctrlDesc.equals("mPrimitive") ) {
			mv.visitln("    "+InitializationContext.class.getName()+" ic = new "+InitializationContext.class.getName()+"();");
			mv.visitln("    ic.interfaces = new java.util.HashMap();");
			mv.visitln("    ic.interfaces.put(\""+ComponentControllerDef.NAME+"\",mcomp);");
			mv.visitln("    ic.interfaces.put(\""+LifeCycleControllerDef.NAME+"\",mcomp);");
			// TODO other interfaces
		}

		// Business interfaces
		for (int i = 0; i < its.length; i++) {

			String itname = its[i].getFcItfName();

			if( its[i].isFcCollectionItf() ) {
				// Julia naming convention for collection interfaces
				itname = "/collection/" + itname;
			}

			// External interface
			SourceCodeGeneratorItf itfscg =
				fcscg.getInterfaceClassGenerator(its[i]);
			String fcitfClassname = itfscg.getTargetTypeName();
			String itstring = InterfaceTypeHelper.javaify(its[i]).toString();
			mv.visit("    proxy = ");
			mv.visit("new "+fcitfClassname+"(");
			mv.visit("mcomp,\""+itname+"\","+itstring+",false,");
			if( contentClassName==null || its[i].isFcClientItf() ) {
				mv.visit("null");
			}
			else {
				mv.visit("content");
			}
			mv.visitln(");");
			mv.visitln("    fcInterfaces.put(\""+itname+"\",proxy);");

			// Lifecycle interceptor
			if( ctrlDesc.equals("mPrimitive") &&
				! its[i].isFcClientItf() &&
				! itname.equals("attribute-controller") ) {

				mv.visit("    ");
				if( ! interceptgenerated ) {
					mv.visit(Interceptor.class.getName()+' ');
					interceptgenerated = true;
				}

				InterfaceTypeConfigurableItf miscg =
					membraneDesc.getInterceptorClassGenerator();
				String signature = its[i].getFcItfSignature();
				Object cl = loadClass(signature);
				String pkgRoot = jc.getPkgRoot();
				miscg.setInterfaceType(its[i],cl,pkgRoot);

				String interceptname = miscg.getTargetTypeName();
				mv.visitln("intercept = new "+interceptname+"();");
				mv.visitln("    intercept.initFcController(ic);");
				mv.visitln("    (("+ComponentInterface.class.getName()+")proxy).setFcItfImpl(intercept);");
				mv.visitln("    intercept.setFcItfDelegate(content);");
			}

			// Internal interfaces
			if( ctrlDesc.equals("mComposite") ) {
				/*
				 * Modification introduced for the ULTRA_COMP mode in order to
				 * obtain a working case for attribute-controller interfaces
				 * exported by the membrane. Such an interface is bound to the
				 * control content component. MCompositeImpl#bindFc sets
				 * internal interfaces in all cases.
				 *
				 * We don't run into this case for the COMP mode since
				 * attribute-controller interfaces delegate the content objects.
				 */
//                if( ! itname.equals("attribute-controller") ) {
					InterfaceType intit = InterfaceTypeHelper.newSymetricInterfaceType(its[i]);
					itfscg = fcscg.getInterfaceClassGenerator(intit);
					fcitfClassname = itfscg.getTargetTypeName();
					itstring = InterfaceTypeHelper.javaify(intit).toString();
					mv.visit  ("    proxy = ");
					mv.visit  ("new "+fcitfClassname+"(");
					mv.visit  ("mcomp,\""+itname+"\","+itstring);
					mv.visitln(",true,null);");
					mv.visitln("    fcInternalInterfaces.put(\""+itname+"\",proxy);");
//                }
			}
		}

		// Set the interfaces
		mv.visitln("    mcomp.setFcInterfaces(fcInterfaces);");
		if( ctrlDesc.equals("mComposite") ) {
			mv.visitln("    mcomp.setFcInternalInterfaces(fcInternalInterfaces);");
		}

		// Return the control instance
		mv.visitln("    return mcomp;");
	}

	@Override
	protected void generateContentCheckForBC( BlockSourceCodeVisitor mv ) {
		// No check for mPrimitive and mComposite
	}
}
