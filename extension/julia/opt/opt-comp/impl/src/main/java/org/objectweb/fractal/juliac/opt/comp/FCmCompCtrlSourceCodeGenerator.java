/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.opt.comp;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.JuliacRuntimeException;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.InterfaceTypeConfigurableItf;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGenerator;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.opt.InitializerClassGenerator;
import org.objectweb.fractal.koch.control.binding.BindingControllerDef;
import org.objectweb.fractal.koch.control.component.ComponentControllerDef;
import org.objectweb.fractal.koch.control.content.ContentControllerDef;
import org.objectweb.fractal.koch.control.content.SuperControllerDef;
import org.objectweb.fractal.koch.control.lifecycle.LifeCycleControllerDef;
import org.objectweb.fractal.koch.control.name.NameControllerDef;
import org.objectweb.fractal.koch.factory.MCompositeImpl;
import org.objectweb.fractal.koch.factory.MPrimitiveImpl;

/**
 * This class generates the source code associated to Fractal components.
 * The membrane implementation and the initializer implementation are generated.
 *
 * This generator handles mPrimitive and mComposite components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class FCmCompCtrlSourceCodeGenerator extends FCSourceCodeGenerator {

	@Override
	protected void postInit() throws IOException {
		// Indeed nothing
		// We don't need membrane loader for this generator
	}

	// -----------------------------------------------------------------------
	// Implementation of the FCSourceCodeGeneratorItf interface
	// -----------------------------------------------------------------------

	@Override
	public boolean test( Object controllerDesc ) {
		boolean accept = membraneClasses.containsKey(controllerDesc);
		return accept;
	}

	@Override
	public MembraneDesc<?> generateMembraneImpl(
		ComponentType ct, String ctrldesc, String contentClassName,
		Object source )
	throws IOException {

		/*
		 * Generate business interface implementations.
		 */
		InterfaceType[] its = ct.getFcInterfaceTypes();
		generateInterfaceImpl(its,ctrldesc);

		/*
		 * Generate control interface implementations.
		 */
		InterfaceType[] membraneits = getMembraneType(ctrldesc);
		generateInterfaceImpl(membraneits,ctrldesc);

		/*
		 * Prepare a membrane descriptor.
		 */
		Class<?> ctrlImpl = getMembraneClass(ctrldesc);
		String name = ctrlImpl.getName();
		MembraneDesc<?> md =
			getMembraneDesc(jc,name,ctrldesc,membraneits,ctrlImpl);

		/*
		 * Generate the lifecycle interceptors associated with control
		 * components for all business interfaces of the control components.
		 */
		if( ctrldesc.equals("mPrimitive") ) {
			String pkgRoot = jc.getPkgRoot();
			InterfaceTypeConfigurableItf icg = md.getInterceptorClassGenerator();
			icg.setMergeable(false);
			for (InterfaceType it : its) {
				String signature = it.getFcItfSignature();
				Object cl = loadClass(signature);
				icg.setInterfaceType(it,cl,pkgRoot);
				if( icg.match() ) {
					jc.generateSourceCode(icg);
				}
			}
		}

		return md;
	}

	@Override
	protected InitializerClassGenerator
	getInitializerClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		return new InitializermCompCtrlClassGenerator(
			jc,fcscg,membraneDesc,ct,contentDesc,source);
	}

	/** @since 2.8 */
	protected MembraneDesc<?> getMembraneDesc(
		JuliacItf jc, String name, String ctrldesc, InterfaceType[] membraneits,
		Class<?> ctrlImpl ) {

		return new MMembraneDesc(jc,name,ctrldesc,membraneits,ctrlImpl);
	}


	// -----------------------------------------------------------------------
	// Implementation specific
	// -----------------------------------------------------------------------

	/**
	 * @since 2.8
	 */
	protected Object loadClass( String signature ) {
		Class<?> cl = jc.loadClass(signature);
		return cl;
	}

	/**
	 * Return the class implementing the membrane of the specified control
	 * component descriptor mPrimitive or mComposite).
	 */
	protected Class<?> getMembraneClass( String ctrldesc ) {

		if( membraneClasses.containsKey(ctrldesc) ) {
			Class<?> membraneClass = membraneClasses.get(ctrldesc);
			return membraneClass;
		}
		else {
			final String msg = "No such controller desc: "+ctrldesc;
			throw new JuliacRuntimeException(msg);
		}
	}

	private static final Map<String,Class<?>> membraneClasses =
		new HashMap<>() {
			private static final long serialVersionUID = -5024933498299059603L;
		{
			put("mPrimitive",MPrimitiveImpl.class);
			put("mComposite",MCompositeImpl.class);
		}};

	/**
	 * Return the control interface types associated to the specified controller
	 * descritptor.
	 *
	 * @param ctrldesc  the controller descriptor
	 * @return          the associated array of control interface types
	 */
	protected InterfaceType[] getMembraneType( String ctrldesc ) {

		if( membraneTypes.containsKey(ctrldesc) ) {
			InterfaceType[] membraneType = membraneTypes.get(ctrldesc);
			return membraneType;
		}
		else {
			final String msg = "No such controller desc: "+ctrldesc;
			throw new JuliacRuntimeException(msg);
		}
	}

	private static final Map<String,InterfaceType[]> membraneTypes =
		new HashMap<>() {
			private static final long serialVersionUID = -5024933498299059603L;
		{
			put("mPrimitive",
				new InterfaceType[]{
					ComponentControllerDef.TYPE,
					BindingControllerDef.TYPE,
					NameControllerDef.TYPE,
					SuperControllerDef.TYPE,
					LifeCycleControllerDef.TYPE });
			put("mComposite",
				new InterfaceType[]{
					ComponentControllerDef.TYPE,
					BindingControllerDef.TYPE,
					NameControllerDef.TYPE,
					SuperControllerDef.TYPE,
					LifeCycleControllerDef.TYPE,
					ContentControllerDef.TYPE });
		}};
}
