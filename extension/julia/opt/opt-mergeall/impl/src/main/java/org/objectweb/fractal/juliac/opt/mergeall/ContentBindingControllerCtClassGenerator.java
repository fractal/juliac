/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.opt.mergeall;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;

import org.objectweb.fractal.api.type.InterfaceType;

import spoon.reflect.factory.Factory;

/**
 * This class generates an implementation of the {@link
 * org.objectweb.fractal.julia.control.binding.ContentBindingController}
 * interface.
 *
 * When the content of a component and the controllers are merged, the
 * implementation of the {@link
 * org.objectweb.fractal.api.control.BindingController} interface assumes that
 * an implementation of the ContentBindingController interface is available.
 * The class which is generated here provides such an implementation.
 *
 * The generated class calls the BindingController methods which are supposed to
 * be defined in the super class.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class ContentBindingControllerCtClassGenerator
extends AbstractProxyCtClassGenerator {

	public ContentBindingControllerCtClassGenerator( Factory factory ) {
		super(factory);
	}

	/**
	 * Return the name of the target class.
	 */
	protected String getTargetClassName( InterfaceType it ) {
		String targetClassName =
			it.getFcItfSignature()+"ContentBindingControllerMixin";
		return targetClassName;
	}

	/**
	 * Return the array of statement code snippets associated to the specified
	 * method.
	 */
	protected String[] getBodySnippets( Method m ) {

		Class<?>[] ptypes = m.getParameterTypes();
		Class<?> rtype = m.getReturnType();

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		if( ! (rtype.isAssignableFrom(void.class)) ) {
			pw.print("return ");
		}
		pw.print("super.");

		String name = m.getName();
		if( name.equals("listFcContent") ) {
			pw.print("listFc");
		}
		else if( name.equals("lookupFcContent") ) {
			pw.print("lookupFc");
		}
		else if( name.equals("bindFcContent") ) {
			pw.print("bindFc");
		}
		else if( name.equals("unbindFcContent") ) {
			pw.print("unbindFc");
		}
		else {
			throw new IllegalArgumentException(name);
		}

		pw.print("(");
		for (int j = 0; j < ptypes.length; j++) {
			if(j!=0)  pw.print(", ");
			pw.print("arg");
			pw.print(j);
		}
		pw.println(")");
		pw.close();
		String snippet = sw.toString();
		return new String[]{snippet};
	}
}
