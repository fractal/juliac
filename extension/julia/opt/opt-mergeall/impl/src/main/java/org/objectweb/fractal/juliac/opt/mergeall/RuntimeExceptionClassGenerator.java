/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.opt.mergeall;

import java.lang.reflect.Modifier;

import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.opt.ClassGenerator;

/**
 * A {@link SourceCodeGeneratorItf} for generating {@link RuntimeException}
 * classes.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 */
public class RuntimeExceptionClassGenerator extends ClassGenerator {

	private String targetClassName;

	/**
	 * @param targetClassName  the name of the exception class
	 */
	public RuntimeExceptionClassGenerator( String targetClassName ) {
		this.targetClassName = targetClassName;
	}

	public String getTargetTypeName() { return targetClassName;}

	@Override
	public String getSuperClassName() {
		return RuntimeException.class.getName();
	}

	@Override
	public void generateMethods( ClassSourceCodeVisitor cv ) {

		BlockSourceCodeVisitor mv =
			cv.visitConstructor(
				Modifier.PUBLIC, null, new String[]{"String msg"}, null );
		mv.visitIns("super(msg)");
		mv.visitEnd();
	}
}
