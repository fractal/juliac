/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.opt.mergeall;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.objectweb.fractal.api.type.InterfaceType;

import spoon.reflect.code.CtBlock;
import spoon.reflect.code.CtCodeSnippetStatement;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtParameter;
import spoon.reflect.declaration.ModifierKind;
import spoon.reflect.factory.ClassFactory;
import spoon.reflect.factory.CoreFactory;
import spoon.reflect.factory.Factory;
import spoon.reflect.factory.MethodFactory;
import spoon.reflect.factory.TypeFactory;
import spoon.reflect.reference.CtTypeReference;

/**
 * Super class for code generators which generate proxy {@link CtClass}es for
 * the methods defined in a specified {@link InterfaceType}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public abstract class AbstractProxyCtClassGenerator {

	// ----------------------------------------------------------------------
	// Protected methods to be defined by subclasses
	// ----------------------------------------------------------------------

	/**
	 * Return the array of statement code snippets associated to the specified
	 * method.
	 */
	protected abstract String[] getBodySnippets( Method m );

	/**
	 * Return the name of the target class.
	 */
	protected abstract String getTargetClassName( InterfaceType it );


	// ----------------------------------------------------------------------
	// Regular constructor and methods
	// ----------------------------------------------------------------------

	public AbstractProxyCtClassGenerator( Factory factory ) {
		this.factory = factory;
	}

	protected Factory factory;

	/**
	 * Generate a proxy class for the methods defined in the specified
	 * {@link InterfaceType}.
	 *
	 * @param it  the  interface type
	 * @param cl  the class corresponding to the interface type
	 * @return    the generated class
	 */
	public CtClass<?> generateCtClass( InterfaceType it, Class<?> cl ) {

		ClassFactory cf = factory.Class();
		MethodFactory mf = factory.Method();
		CoreFactory coref = factory.Core();
		TypeFactory tf = factory.Type();

		String targetClassName = getTargetClassName(it);
		CtClass<?> ctclass = cf.create(targetClassName);

		/*
		 * Let the created class implement the Java interface defined in the
		 * interface type.
		 */
		CtTypeReference<?> ittr = tf.createReference(cl);
		ctclass.addSuperInterface(ittr);

		/*
		 * Iterate over the array of methods defined in the interface type.
		 */
		Method[] methods = cl.getMethods();
		for (Method m : methods) {

			String mname = m.getName();
			Class<?>[] ptypes = m.getParameterTypes();
			Class<?> rtype = m.getReturnType();
			Class<?>[] etypes = m.getExceptionTypes();

			Set<ModifierKind> mods = new HashSet<>();
			mods.add(ModifierKind.PUBLIC);
			CtTypeReference<?> rctype = tf.createReference(rtype);

			// Parameters
			List<CtParameter<?>> params = new ArrayList<>();
			for (int i = 0; i < ptypes.length; i++) {
				CtTypeReference<?> pctype = tf.createReference(ptypes[i]);
				String pname = "arg"+i;
				CtParameter<?> param = mf.createParameter(null,pctype,pname);
				params.add(param);
			}

			// Exceptions
			Set<CtTypeReference<? extends Throwable>> ectype = new HashSet<>();
			for (Class<?> etype : etypes) {
				CtTypeReference<?> tr = tf.createReference(etype);
				ectype.add((CtTypeReference<? extends Throwable>)tr);
			}

			// Body
			CtBlock<?> body = factory.Core().createBlock();
			String[] snippets = getBodySnippets(m);
			for (String snippet : snippets) {
				CtCodeSnippetStatement ctsnippet = coref.createCodeSnippetStatement();
				ctsnippet.setValue(snippet);
				body.insertEnd(ctsnippet);
			}

			// Method creation and insertion
			CtMethod<?> ctmethod =
				mf.create(ctclass,mods,rctype,mname,params,ectype,(CtBlock)body);
			ctclass.addMethod(ctmethod);
		}

		return ctclass;
	}
}
