/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.opt.mergeall;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.InterfaceTypeConfigurableItf;
import org.objectweb.fractal.juliac.api.generator.ProxyClassGeneratorItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.desc.ControllerDesc;
import org.objectweb.fractal.juliac.core.helper.InterfaceTypeHelper;
import org.objectweb.fractal.juliac.core.helper.JuliacHelper;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.opt.InitializerClassGenerator;
import org.objectweb.fractal.juliac.core.proxy.AttributeControllerClassGenerator;

/**
 * Initializer generator for Fractal components with an object-oriented control
 * membrane and with a optimization level where the content, the interceptors
 * and the controllers are merged.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 */
public class InitializerMergeAllClassGenerator
extends InitializerClassGenerator {

	public InitializerMergeAllClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		super(jc,fcscg,membraneDesc,ct,contentDesc,source);
	}

	@Override
	protected void generateGetFcControllerDescMethod( BlockSourceCodeVisitor mv ) {
		mv.visit  ("      return \"");
		mv.visit  (membraneDesc.getDescriptor());
		mv.visitln("\";");
	}

	@Override
	protected void generateGetFcContentDescMethod( BlockSourceCodeVisitor mv ) {
		mv.visit  ("      return ");
		mv.visit  (JuliacHelper.javaifyContentDesc(contentDesc).toString());
		mv.visitln(";");
	}

	/**
	 * @throws IllegalArgumentException
	 * 		if there is not exactly one controller in the membrane descriptor
	 */
	@Override
	protected void generateNewFcInstanceMethod( BlockSourceCodeVisitor mv ) {

		List<ControllerDesc> ctrlDescs = (List<ControllerDesc>) membraneDesc.getCtrlDescs();
		if( ctrlDescs.size() != 1 ) {
			final String msg =
				"membraneDesc.getCtrlDescs should return a list of size 1";
			throw new IllegalArgumentException(msg);
		}

		String membraneClassName = ctrlDescs.get(0).getImplName();
		String ctrlDesc = membraneDesc.getDescriptor();
		InterfaceType[] membraneits = membraneDesc.getCtrlItfTypes();
		String contentClassName = JuliacHelper.getContentClassName(ctrlDesc,contentDesc);
		boolean isComposite = InterfaceTypeHelper.isComposite(membraneits);

		// Instantiate the content
		mv.visitln("    Object content = new "+membraneClassName+"();");

		// Check that the content class implements the server interfaces
		generateContentChecks(mv);

		// Prepare an InitializationContext
		mv.visitln("    "+InitializationContext.class.getName()+" ic = new "+InitializationContext.class.getName()+"();");
		mv.visitln("    ic.controllers = new "+ArrayList.class.getName()+"();");
		mv.visitln("    ic.interfaces = new "+HashMap.class.getName()+"();");
		if(isComposite) {
			mv.visitln("    ic.internalInterfaces = new "+HashMap.class.getName()+"();");
		}

		// Instantiate the content
		generateInitializationContextForContent(mv);

		// Content and controllers are merged
		mv.visitln("    ic.controllers.add(content);");

		// Component controller
		InterfaceType compctrlit =
			new BasicInterfaceType("component",Component.class.getName(),false,false,false);
		SourceCodeGeneratorItf itfscg =
			fcscg.getInterfaceClassGenerator(compctrlit);
		String fcitfClassname = itfscg.getTargetTypeName();
		String itstring = InterfaceTypeHelper.javaify(compctrlit).toString();
		mv.visit  ("    "+Interface.class.getName()+" proxy = ");
		mv.visit  ("new "+fcitfClassname+"(");
		mv.visitln("("+Component.class.getName()+")content,\"component\","+itstring+",false,content);");
		mv.visit  ("    "+Component.class.getName()+" proxyForCompCtrl = ");
		mv.visitln("("+Component.class.getName()+") proxy;");
		mv.visitln("    ic.interfaces.put(\"component\",proxy);");

		boolean attrimplgenerated = false;

		// Controllers other than the component controller
		for (int i = 0; i < membraneits.length; i++) {
			String itname = membraneits[i].getFcItfName();
			if( ! itname.equals("component") ) {
				itfscg = fcscg.getInterfaceClassGenerator(membraneits[i]);
				fcitfClassname = itfscg.getTargetTypeName();
				itstring = InterfaceTypeHelper.javaify(membraneits[i]).toString();

				// Control external interface
				if( itname.equals("/cloneable-attribute-controller") ) {
					InterfaceType attrit = null;
					try {
						attrit = ct.getFcInterfaceType("attribute-controller");
					}
					catch (NoSuchInterfaceException e) {
						throw new RuntimeException(e);
					}
					String signature = attrit.getFcItfSignature();
					Class<?> cl = jc.loadClass(signature);
					final String pkgRoot = jc.getPkgRoot();
					ProxyClassGeneratorItf attrscg =
						new AttributeControllerClassGenerator(
							attrit,cl,pkgRoot,null,false);
					String s = attrscg.getTargetTypeName();
					mv.visitln("    Object attrimpl = new "+s+"();");
					attrimplgenerated = true;
				}
				mv.visit  ("    proxy = ");
				mv.visit  ("new "+fcitfClassname+"(");
				mv.visit  ("proxyForCompCtrl,\""+itname+"\","+itstring+",false,");
				if( itname.equals("/cloneable-attribute-controller") ) {
					mv.visit("attrimpl");
				}
				else {
					mv.visit("content");
				}
				mv.visitln(");");
				mv.visitln("    ic.interfaces.put(\""+itname+"\",proxy);");

				// Control internal interface
				if( itname.equals("factory") && isComposite ) {
					InterfaceType intit = InterfaceTypeHelper.newSymetricInterfaceType(membraneits[i]);
					itfscg = fcscg.getInterfaceClassGenerator(intit);
					fcitfClassname = itfscg.getTargetTypeName();
					itstring = InterfaceTypeHelper.javaify(intit).toString();
					mv.visit  ("    proxy = ");
					mv.visit  ("new "+fcitfClassname+"(");
					mv.visitln("proxyForCompCtrl,\""+itname+"\","+itstring+",true,content);");
					mv.visitln("    ic.internalInterfaces.put(\""+itname+"\",proxy);");
				}
			}
		}

		// Component type
		InterfaceType[] its = ct.getFcInterfaceTypes();
		InterfaceType[] fullits = new InterfaceType[ membraneits.length + its.length ];
		System.arraycopy(membraneits, 0, fullits, 0, membraneits.length);
		System.arraycopy(its, 0, fullits, membraneits.length, its.length);
		mv.visitln("    ic.type = "+InterfaceTypeHelper.javaify(fullits)+";");

		// Business interfaces
		mv.visitln("    Object intercept = null;");
		for (int i = 0; i < its.length; i++) {

			String itname = its[i].getFcItfName();

			if( its[i].isFcCollectionItf() ) {
				// Julia naming convention for collection interfaces
				itname = "/collection/" + itname;
			}

			// Interceptors
			if( its[i].isFcClientItf() ) {
				if( ! generateInterceptorCreation(mv,its[i],"content") ) {
					mv.visitln("    intercept = null;");
				}
			}
			else {
				mv.visit("    intercept = ");
				mv.visit( contentClassName==null ? "null" : "content" );
				mv.visitln(";");
			}

			// External interface
			itfscg = fcscg.getInterfaceClassGenerator(its[i]);
			fcitfClassname = itfscg.getTargetTypeName();
			itstring = InterfaceTypeHelper.javaify(its[i]).toString();
			mv.visit("    proxy = ");
			mv.visit("new "+fcitfClassname+"(");
			mv.visit("proxyForCompCtrl,\""+itname+"\","+itstring+",false,");
			if( itname.equals("attribute-controller") && attrimplgenerated ) {
				mv.visit("attrimpl");
			}
			else {
				mv.visit("intercept");  // interceptor or content or null
			}
			mv.visitln(");");
			mv.visitln("    ic.interfaces.put(\""+itname+"\",proxy);");

			// Internal interface (skip attribute control interfaces and primitive components)
			if( ! itname.equals("attribute-controller") && isComposite ) {
				InterfaceType intit = InterfaceTypeHelper.newSymetricInterfaceType(its[i]);
				itfscg = fcscg.getInterfaceClassGenerator(intit);
				fcitfClassname = itfscg.getTargetTypeName();
				itstring = InterfaceTypeHelper.javaify(intit).toString();
				mv.visit("    proxy = ");
				mv.visit("new "+fcitfClassname+"(");
				mv.visit("proxyForCompCtrl,\""+itname+"\","+itstring);
				mv.visitln(",true,intercept);");
				mv.visitln("    ic.internalInterfaces.put(\""+itname+"\",proxy);");
			}
		}

		// Initialize controllers
		// merged content-controllers + interceptors on client interfaces
		mv.visitln("    for( int i=0 ; i < ic.controllers.size() ; i++ ) {");
		mv.visitln("      Object ctrl = ic.controllers.get(i);");
		mv.visitln("      if( ctrl instanceof "+Controller.class.getName()+" ) {");
		mv.visitln("        (("+Controller.class.getName()+")ctrl).initFcController(ic);");
		mv.visitln("      }");
		mv.visitln("    }");

		// Return the fcinterface for the component controller
		mv.visitln("    return proxyForCompCtrl;");
	}

	@Override
	protected void generateExtraCode( ClassSourceCodeVisitor cv ) {

		InterfaceTypeConfigurableItf icg =
			membraneDesc.getInterceptorClassGenerator();
		InterfaceType[] its = ct.getFcInterfaceTypes();

		/*
		 * Interceptors (inner classes).
		 */
		if( icg != null ) {
			icg.setMembraneDesc(membraneDesc);
			Set<String> inners = new HashSet<>();
			for (InterfaceType it : its) {
				String signature = it.getFcItfSignature();
				Class<?> cl = jc.loadClass(signature);
				final String pkgRoot = jc.getPkgRoot();
				icg.setInterfaceType(it,cl,pkgRoot);
				String inner = icg.getTargetTypeName();
				if( it.isFcClientItf() && icg.match() && !inners.contains(inner) ) {
					ClassSourceCodeVisitor innercv = cv.visitInnerClass();
					icg.generate(innercv);
					inners.add(inner);
				}
			}
		}
	}
}
