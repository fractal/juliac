/***
 * Juliac
 * Copyright (C) 2010-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.opt.mergeall;

import java.util.ArrayList;
import java.util.Collection;

/**
 * List where each element is contained only once. When adding an element to
 * this list, if the same element already exists, it is first removed before
 * being added at the end of the list.
 *
 * The purpose of this class is to manage mixin layer lists where layers must
 * not be inserted several times and only the last occurrence of the layer must
 * be kept in order to preserve layer dependencies.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.3
 */
public class UniqueArrayList<E> extends ArrayList<E> {

	private static final long serialVersionUID = -2287641373748149582L;

	@Override
	public boolean add( E e ) {
		if( contains(e) ) {
			remove(e);
		}
		return super.add(e);
	}

	@Override
	public void add( int index, E element ) {
		if( contains(element) ) {
			remove(element);
		}
		super.add(index,element);
	}

	@Override
	public boolean addAll( Collection<? extends E> c ) {
		boolean changed = false;
		for (E e : c) {
			boolean b = add(e);
			if(b) {
				changed = true;
			}
		}
		return changed;
	}
}
