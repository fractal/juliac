/**
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Romain Rouvoy
 * Contributor: Lionel Seinturier
 */

package example.hw

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.juliac.commons.io.Console

class ClientImpl extends Runnable with BindingController {

	val console = Console.getConsole("hw-scala-")
	console.println("CLIENT created")

	// BindingController implementation
	private var service: Service = _

	def listFc = Array("s")

	def lookupFc(cItf: String) = if (cItf == "s") service else null

	def bindFc(cItf: String, sItf: Object) {
		if (cItf == "s")
			service = sItf.asInstanceOf[Service];
	}

	def unbindFc(cItf: String) {
		if (cItf == "s")
			service = null;
	}

	// Runnable interface implementation
	def run = service print "Hello world"
}
