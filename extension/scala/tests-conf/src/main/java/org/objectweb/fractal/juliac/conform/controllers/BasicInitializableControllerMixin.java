/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.conform.controllers;

import org.objectweb.fractal.julia.loader.Initializable;
import org.objectweb.fractal.julia.loader.Tree;

public abstract class BasicInitializableControllerMixin
implements InitializableController, Initializable {

	// -------------------------------------------------------------------
	// Implementation of the Initializable interface
	// -------------------------------------------------------------------

	public void initialize(Tree args) throws Exception {
		_super_initialize(args);
		int size = args.getSize();
		for (int i = 0; i < size; i++) {
			Tree arg = args.getSubTree(i);
			if( arg.getSize() == 3 && arg.getSubTree(0).equals("my-init-params") ) {
				param1 = arg.getSubTree(1).toString();
				param2 = arg.getSubTree(2).toString();
			}
		}
	}

	private String param1;
	private String param2;

	// -------------------------------------------------------------------
	// Implementation of the InitializableController interface
	// -------------------------------------------------------------------

	public Object getInitTreeValues() {
		return new String[]{param1,param2};
	}

	// ---------------------------------------------------------------------------
	// Fields and methods required by the mixin class in the base class
	// ---------------------------------------------------------------------------

	/**
	 * The {@link Initializable#initialize(Tree)} method overridden by this
	 * mixin.
	 *
	 * @see Initializable#initialize(Tree)
	 */
	public abstract void _super_initialize(final Tree args) throws Exception;
}
