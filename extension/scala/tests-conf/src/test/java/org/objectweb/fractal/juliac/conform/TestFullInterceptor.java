/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.conform;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.conform.AbstractTest;
import org.objectweb.fractal.julia.conform.components.C;
import org.objectweb.fractal.julia.conform.components.I;
import org.objectweb.fractal.julia.conform.controllers.StatController;
import org.objectweb.fractal.util.Fractal;

/**
 * A class for testing interceptors installed on all interfaces of a component
 * including control ones.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.1
 */
public class TestFullInterceptor extends AbstractTest {

	protected Component boot;
	protected TypeFactory tf;
	protected GenericFactory gf;

	protected ComponentType t;
	protected Component d;

	@BeforeEach
	public void setUp() throws InstantiationException, NoSuchInterfaceException {
		boot = Fractal.getBootstrapComponent();
		tf = Fractal.getTypeFactory(boot);
		gf = Fractal.getGenericFactory(boot);
		t = tf.createFcType(new InterfaceType[] {
			tf.createFcItfType("server", I.class.getName(), false, false, false),
			tf.createFcItfType("servers", I.class.getName(), false, false, true),
			tf.createFcItfType("client", I.class.getName(), true, true, false),
			tf.createFcItfType("clients", I.class.getName(), true, true, true)
		});
		d = gf.newFcInstance(t, "stat2Primitive", C.class.getName());
	}

	// -------------------------------------------------------------------------
	// Test interceptors
	// -------------------------------------------------------------------------

	@Test
	public void testInterceptors() throws NoSuchInterfaceException {
		StatController sc = (StatController) d.getFcInterface("stat-controller");
		assertEquals(1,sc.getNumberOfMethodCalled());
	}
}
