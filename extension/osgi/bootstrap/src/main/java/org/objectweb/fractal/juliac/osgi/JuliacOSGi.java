/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.osgi;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.juliac.runtime.Juliac;

/**
 * The Fractal provider class for OSGi.
 *
 * This Fractal provider returns a bootstrap component with a type-factory and
 * a generic-factory interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2
 */
public class JuliacOSGi extends Juliac {

	public JuliacOSGi() {
		super();
	}

	/**
	 * Return the reference of the bootstrap component.
	 */
	public Component newFcInstance(
		Type type, Object controllerDesc, Object contentDesc ) {

		if( bootstrap == null ) {
			bootstrap = new JuliacOSGiBootstrapComponentImpl();
		}

		return bootstrap;
	}


	/** The singleton instance of the bootstrap component. */
	private static Component bootstrap;
}
