package example.hw.server;

import example.hw.itf.Service;
import example.hw.itf.ServiceAttributes;


public class ServerImpl implements Service , ServiceAttributes {

	private String header;
	private int count;

	public ServerImpl() {
		System.out.println("SERVER created");
	}

	public void print(final String msg) {
		System.out.println("Server: begin printing...");
		for (int i = 0 ; i < (count) ; ++i) {
			System.out.println(((header) + msg));
		}
		System.out.println("Server: print done.");
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(final String header) {
		this.header = header;
	}

	public int getCount() {
		return count;
	}

	public void setCount(final int count) {
		this.count = count;
	}
}
