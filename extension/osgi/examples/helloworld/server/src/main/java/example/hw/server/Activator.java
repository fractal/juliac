package example.hw.server;

import java.util.Dictionary;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import example.hw.itf.Service;
import example.hw.itf.ServiceAttributes;

public class Activator implements BundleActivator {

	public void start( BundleContext context ) {

		context.registerService(
			new String[]{ Service.class.getName(), ServiceAttributes.class.getName()},
			new ServerImpl(),
			null
		);

		Bundle bundle = context.getBundle();
		Dictionary dict = bundle.getHeaders();
		String name = (String) dict.get("Bundle-Name");
		System.out.println("Bundle <"+name+"> started");
	}

	public void stop( BundleContext context ) {

		Bundle bundle = context.getBundle();
		Dictionary dict = bundle.getHeaders();
		String name = (String) dict.get("Bundle-Name");
		System.out.println("Bundle <"+name+"> stopped");
	}
}
