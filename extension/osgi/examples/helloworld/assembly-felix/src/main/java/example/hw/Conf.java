package example.hw;

import java.io.IOException;

import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.JuliacModuleItf;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;

public class Conf implements JuliacModuleItf {

	private FCSourceCodeGeneratorItf oo;
	private FCSourceCodeGeneratorItf osgi;

	public void init( JuliacItf jc ) throws IOException {
		oo = new org.objectweb.fractal.juliac.opt.oo.FCOOCtrlSourceCodeGenerator();
		osgi = new org.objectweb.fractal.juliac.osgi.FCOOCtrlSourceCodeGenerator();
		oo.setCtrlDescPrefix("/julia/");
		osgi.setCtrlDescPrefix("/osgi/");
		oo.init(jc);
		osgi.init(jc);
	}

	public void close( JuliacItf jc ) throws IOException {
		osgi.close(jc);
		oo.close(jc);
	}
}
