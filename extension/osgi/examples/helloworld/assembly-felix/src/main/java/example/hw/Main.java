/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package example.hw;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.juliac.osgi.JuliacOSGi;
import org.objectweb.fractal.juliac.osgi.OSGiHelper;
import org.objectweb.fractal.juliac.osgi.PlatformItf;
import org.objectweb.fractal.util.Fractal;

import example.hw.itf.Service;
import example.hw.itf.ServiceAttributes;

/**
 * Launcher class for the Hello World example.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.5
 */
public class Main {

	public static void main( String[] args )
	throws
		ClassNotFoundException, IllegalLifeCycleException,
		NoSuchInterfaceException, InstantiationException,
		java.lang.InstantiationException, IllegalAccessException,
		org.objectweb.fractal.juliac.osgi.revision.OSGiRevisionException {

		try {
			composite();
			osgiPrimitive();
		}
		finally {
			PlatformItf pf = OSGiHelper.getPlatform();
			pf.stop();
		}
	}

	/**
	 * Load and run the example.hw.HelloWorld composite.
	 */
	private static void composite()
	throws
		ClassNotFoundException, IllegalLifeCycleException,
		NoSuchInterfaceException, InstantiationException,
		java.lang.InstantiationException, IllegalAccessException {

		/*
		 * Instantiate the component.
		 */
		Class cl = Class.forName("example.hw.HelloWorld");
		Factory f = (Factory) cl.newInstance();
		Component c = f.newFcInstance();

		/*
		 * Start the component.
		 */
		Fractal.getLifeCycleController(c).startFc();

		/*
		 * Call the run method on the r provided interface.
		 */
		Runnable r = (Runnable) c.getFcInterface("r");
		r.run();

		/*
		 * Stop the component.
		 */
		Fractal.getLifeCycleController(c).stopFc();
	}

	/**
	 * Load and run the server osgiPrimitive component.
	 */
	private static void osgiPrimitive()
	throws
		IllegalLifeCycleException, NoSuchInterfaceException,
		InstantiationException {

		/*
		 * Retrieve the bootstrap component specific to osgiPrimitive
		 * components.
		 */
		System.setProperty("fractal.provider",JuliacOSGi.class.getName());
		Component boot = Fractal.getBootstrapComponent();
		GenericFactory gf = Fractal.getGenericFactory(boot);
		TypeFactory tf = Fractal.getTypeFactory(boot);

		/*
		 * Instantiate the component.
		 */
		Type type =
			tf.createFcType(
				new InterfaceType[]{
					tf.createFcItfType("s",Service.class.getName(),false,false,false),
					tf.createFcItfType("attribute-controller",ServiceAttributes.class.getName(),false,false,false)
				});
		Object contentDesc = "helloworld-interfaces.jar helloworld-server.jar";
		Component c = gf.newFcInstance(type,"osgiPrimitive",contentDesc);

		/*
		 * Set values of component attributes.
		 */
		((ServiceAttributes)Fractal.getAttributeController(c)).setCount(1);
		((ServiceAttributes)Fractal.getAttributeController(c)).setHeader("==>");

		/*
		 * Start the component.
		 */
		Fractal.getLifeCycleController(c).startFc();

		/*
		 * Call the run method on the r provided interface.
		 */
		Service s = (Service) c.getFcInterface("s");
		s.print("Hello World!");

		/*
		 * Stop the component.
		 */
		Fractal.getLifeCycleController(c).stopFc();
	}
}
