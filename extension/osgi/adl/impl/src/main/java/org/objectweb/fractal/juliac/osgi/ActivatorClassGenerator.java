/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.osgi;

import java.lang.reflect.Modifier;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.opt.ClassGenerator;
import org.objectweb.fractal.util.Fractal;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * This class generates the OSGi activator class for a specified ADL descriptor.
 * Fractal server interfaces of the corresponding component are exported as OSGi
 * services, while Fractal client interfaces are bound to service references
 * retrieved from the OSGi registry.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 * @since 2.2.1
 */
public class ActivatorClassGenerator extends ClassGenerator {

	private String factoryClassName;
	private ComponentType ct;
	private String targetClassName;

	public ActivatorClassGenerator(
		String factoryClassName, ComponentType ct, String targetClassName ) {

		this.factoryClassName = factoryClassName;
		this.ct = ct;
		this.targetClassName = targetClassName;
	}

	public String getTargetTypeName() {
		return targetClassName;
	}

	@Override
	public String[] getImplementedInterfaceNames() {
		return new String[]{BundleActivator.class.getName()};
	}

	@Override
	public void generateMethods( ClassSourceCodeVisitor cv ) {

		/*
		 * Field Component c.
		 */
		cv.visitField(Modifier.PRIVATE,Component.class.getName(),"c",null);

		/*
		 * Method start(BundleContext)
		 */
		BlockSourceCodeVisitor mv =
			cv.visitMethod(
				Modifier.PUBLIC, null, "void", "start",
				new String[]{BundleContext.class.getName()+" context"},
				new String[]{Exception.class.getName()} );

		// Instantiate the Fractal component
		mv.visit  ("    ");
		mv.visit  (Bundle.class.getName());
		mv.visitln(" bundle = context.getBundle();");
		mv.visit  ("    Class cl = bundle.loadClass(\"");
		mv.visit  (factoryClassName);
		mv.visitln("\");");
		mv.visitln("    Object o = cl.newInstance();");
		mv.visit  ("    ");
		mv.visit  (Factory.class.getName());
		mv.visit  (" f = (");
		mv.visit  (Factory.class.getName());
		mv.visitln(") o;");
		mv.visitln("    c = f.newFcInstance();");

		// Bind the client interfaces to the services imported by the bundle
		mv.visit  ("    ");
		mv.visit  (ServiceReference.class.getName());
		mv.visitln("[] refs = null;");
		mv.visitln("    Object service = null;");

		InterfaceType[] its = ct.getFcInterfaceTypes();
		for (InterfaceType it : its) {
			if( it.isFcClientItf() ) {
				String name = it.getFcItfName();
				String signature = it.getFcItfSignature();
				mv.visit  ("    refs = context.getServiceReferences(\"");
				mv.visit  (signature);
				mv.visitln("\",null);");
				mv.visitln("    service = context.getService(refs[0]);");
				mv.visit  ("    ");
				mv.visit  (Fractal.class.getName());
				mv.visit  (".getBindingController(c).bindFc(\"");
				mv.visit  (name);
				mv.visitln("\",service);");
			}
		}

		// Register the server interfaces as services exported by the bundle
		mv.visitln("    Object serverItf = null;");
		for (InterfaceType it : its) {
			if( ! it.isFcClientItf() ) {
				String name = it.getFcItfName();
				String signature = it.getFcItfSignature();
				mv.visit  ("    serverItf = c.getFcInterface(\"");
				mv.visit  (name);
				mv.visitln("\");");
				mv.visit  ("    context.registerService(\"");
				mv.visit  (signature);
				mv.visitln("\",serverItf,null);");
			}
		}

		// Start the Fractal component
		mv.visit  ("    ");
		mv.visit  (Fractal.class.getName());
		mv.visitln(".getLifeCycleController(c).startFc();");

		mv.visitEnd();

		/*
		 * Method stop(BundleContext)
		 */
		mv =
			cv.visitMethod(
				Modifier.PUBLIC, null, "void", "stop",
				new String[]{BundleContext.class.getName()+" context"},
				new String[]{Exception.class.getName()} );

		// Stop the Fractal component
		mv.visit  ("    ");
		mv.visit  (Fractal.class.getName());
		mv.visitln(".getLifeCycleController(c).stopFc();");

		mv.visitEnd();
	}
}
