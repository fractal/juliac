/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.osgi;

import java.io.IOException;
import java.util.Map;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.juliac.adl.FractalADLSupportImpl;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.desc.ADLParserSupportItf;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;

/**
 * This ADL parser generates the OSGi activator class for a specified ADL
 * descriptor.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2.1
 */
public class FCActivatorSourceCodeGenerator extends FractalADLSupportImpl {

	@Override
	public void generate( String adl, String targetname )
	throws IOException {

		super.generate(adl,targetname);

		/*
		 * Generate the activator class.
		 */
		ADLParserSupportItf fractaladl =
			jc.lookupAndFilter(ADLParserSupportItf.class,adl);
		Map<Object,Object> context = getContext(null);
		ComponentDesc<?> cdesc = fractaladl.parse(adl,context);
		ComponentType ct = cdesc.getCT();
		String activatorClassName = adl+"Activator";
		SourceCodeGeneratorItf cg =
			new ActivatorClassGenerator(targetname,ct,activatorClassName);
		jc.generateSourceCode(cg);
	}
}
