/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.osgi;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.commons.lang.GenericClass;
import org.objectweb.fractal.juliac.core.Constants;

/**
 * Utility source code generator for proxies associated with membranes which
 * control OSGi components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 * @since 2.1.5
 */
public class DynProxyGenerator {

	/**
	 * Generate the code of the method which sets the value of the delegate and
	 * initialize the map of {@link java.lang.reflect.Method}s implemented by
	 * this delegate.
	 */
	public void generateMethodSetFcItfDelegate(
		BlockSourceCodeVisitor mv, Class<?> proxycl, String impl ) {

		mv.visitln("    "+impl+" = obj;");
		mv.visitln("    if( obj == null ) { return; }");
		mv.visitln("    methodsMap = new java.util.HashMap();");
		mv.visitln("    Class cl = "+impl+".getClass();");

		mv.visitln("    try {");
		Method[] proxymethods = proxycl.getMethods();
		for (int i = 0; i <proxymethods.length; i++) {
			Method proxym = proxymethods[i];
			Class<?>[] ptypes = proxym.getParameterTypes();
			mv.visit  ("      methodsMap.put(\"");
			mv.visit  (proxym.toString());
			mv.visit  ("\",cl.getMethod(\"");
			mv.visit  (proxym.getName());
			mv.visit  ("\",new Class[]{");
			boolean first=true;
			for (Class<?> ptype : ptypes) {
				if(first) {first=false;} else {mv.visit(",");}
				mv.visit(ptype.getName());
				mv.visit(".class");
			}
			mv.visitln("}));");
		}
		mv.visitln("    }");

		/*
		 * NoSuchMethodException should never be thrown as the methods which are
		 * retrieved exist.
		 */
		mv.visit  ("    catch( ");
		mv.visit  (NoSuchMethodException.class.getName());
		mv.visitln(" e ) {");
		mv.visitln("      throw new "+Constants.JULIAC_RUNTIME_EXCEPTION+"(e);");
		mv.visitln("    }");
	}

	/**
	 * Generate the code of the delegation method.
	 */
	public void generateProxyMethodBodyDelegatingCode(
		BlockSourceCodeVisitor mv, Class<?> proxycl, Method proxym,
		String delegatingInstance ) {

		generateProxyMethodBodyDelegatingBeforeCode(mv,proxycl,proxym);
		mv.visit(delegatingInstance);
		generateProxyMethodBodyDelegatingAfterCode(mv,proxycl,proxym);
	}

	/**
	 * Generate the code with takes place before the generation of the
	 * delegation to the instance.
	 */
	private void generateProxyMethodBodyDelegatingBeforeCode(
		BlockSourceCodeVisitor mv, Class<?> proxycl, Method proxym ) {

		GenericClass gc = new GenericClass(proxycl);
		String rtypename = gc.getGenericReturnType(proxym);

		if( ! rtypename.equals("void") ) {
			mv.visitln("    "+rtypename+" ret;");
		}

		/*
		 * Generate a try/cach block for the dynamic invocation.
		 */
		mv.visitln("    try {");

		/*
		 * Invoke dynamically the intercepted method.
		 */
		mv.visit("      ");
		if( ! rtypename.equals("void") ) {
			mv.visit("ret = ");
			if( types.containsKey(rtypename) ) {
				String boxed = types.get(rtypename);
				mv.visit("(("+boxed+")");

			}
			else if( ! rtypename.equals("java.lang.Object") ) {
				mv.visit("("+rtypename+")");
			}
		}
		mv.visit("((");
		mv.visit(Method.class.getName());
		mv.visit(")methodsMap.get(\"");
		mv.visit(proxym.toString());
		mv.visit("\")).invoke(");
	}

	/**
	 * Generate the code with takes place after the generation of the delegation
	 * to the instance.
	 */
	private void generateProxyMethodBodyDelegatingAfterCode(
		BlockSourceCodeVisitor mv, Class<?> proxycl, Method proxym ) {

		Class<?>[] ptypes = proxym.getParameterTypes();
		GenericClass gc = new GenericClass(proxycl);
		String rtypename = gc.getGenericReturnType(proxym);

		mv.visit(",new Object[]{");
		for (int i = 0; i < ptypes.length; i++) {
			if(i!=0)  mv.visit(",");
			String ptypename = ptypes[i].getName();
			if( types.containsKey(ptypename) ) {
				String boxed = types.get(ptypename);
				mv.visit("new "+boxed+"(arg"+Integer.toString(i)+")");
			}
			else {
				mv.visit("arg"+Integer.toString(i));
			}
		}
		mv.visit("})");
		if( types.containsKey(rtypename) ) {
			mv.visit(")."+rtypename+"Value()");

		}
		mv.visitln(";");

		mv.visitln("    }");
		mv.visit  ("    catch( ");
		mv.visit  (InvocationTargetException.class.getName());
		mv.visitln(" e ) {");
		mv.visitln("      throw new "+Constants.JULIAC_RUNTIME_EXCEPTION+"(e);");
		mv.visitln("    }");
		mv.visit  ("    catch( ");
		mv.visit  (IllegalAccessException.class.getName());
		mv.visitln(" e ) {");
		mv.visitln("      throw new "+Constants.JULIAC_RUNTIME_EXCEPTION+"(e);");
		mv.visitln("    }");
	}

	/** Mapping between primitive type names and their boxed counterparts. */
	private static Map<String,String> types =
		new HashMap<String,String>() {
			private static final long serialVersionUID = -2850658294964840917L;
		{
			put( boolean.class.getName(), Boolean.class.getName() );
			put( char.class.getName(), Character.class.getName() );
			put( double.class.getName(), Double.class.getName() );
			put( float.class.getName(), Float.class.getName() );
			put( int.class.getName(), Integer.class.getName() );
			put( long.class.getName(), Long.class.getName() );
			put( short.class.getName(), Short.class.getName() );
		}};
}
