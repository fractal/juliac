/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.osgi;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.proxy.InterceptorClassGenerator;

/**
 * This class generates component interceptor implementations which use dynamic
 * method invocation for delegating calls to the intercepted object.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 * @since 2.1.5
 */
public class DynInterceptorClassGenerator extends InterceptorClassGenerator {

	private DynProxyGenerator dpg = new DynProxyGenerator();

	public DynInterceptorClassGenerator() {
		super();
	}

	@Override
	public String getClassNameSuffix() {
		String suffix = super.getClassNameSuffix();
		return suffix + "Dyn";
	}

	@Override
	public void generateFields( ClassSourceCodeVisitor cv ) {

		/*
		 * Don't provide an initialization expression such as new HashMap() to
		 * methodsMap. The constructor is calling the inherited constructor
		 * (from BasicComponentInterface) which in its turn calls setFcItfImpl.
		 * This method initializes methodsMap but, as when the constructor
		 * returns, the default initialization expressions of fields are
		 * executed, the default expression for methodsMap would override the
		 * initialization performed by setFcItfImpl.
		 */

		cv.visitField( Modifier.PRIVATE, "java.util.Map", "methodsMap", null );

		super.generateFields(cv);
	}

	@Override
	public void generateFieldImpl( ClassSourceCodeVisitor cv ) {
		if( ! mergeable ) {
			cv.visitField( Modifier.PRIVATE, "Object", "_impl", null );
		}
	}

	@Override
	public void generateMethodSetFcItfDelegate( BlockSourceCodeVisitor mv ) {
		dpg.generateMethodSetFcItfDelegate(mv,proxycl,"_impl");
	}

	@Override
	public void generateProxyMethodBodyDelegatingCode(
		BlockSourceCodeVisitor mv, Method proxym ) {

		dpg.generateProxyMethodBodyDelegatingCode(mv,proxycl,proxym,"_impl");
	}
}
