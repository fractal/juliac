/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.osgi;

import java.io.IOException;

import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;

/**
 * Source code generator for osgiPrimitive components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.5
 */
public class FCOOCtrlSourceCodeGenerator
extends org.objectweb.fractal.juliac.opt.oo.FCOOCtrlSourceCodeGenerator {

	private FCOOCtrlGenerator osgig;

	/** Configuration file for OSGi. */
	public static final String DEFAULT_CONFIGURATION = "osgi-bundled.cfg";


	// -----------------------------------------------------------------------
	// Implementation of the JuliacModuleItf interface
	// -----------------------------------------------------------------------

	@Override
	public void init( JuliacItf jc ) throws IOException {
		super.init(jc);
		osgig = new FCOOCtrlGenerator(jc);
	}

	@Override
	protected void postInitJuliaLoaderModule() throws IOException {
		jloader.loadConfigFile(DEFAULT_CONFIGURATION);
		super.postInitJuliaLoaderModule();
	}


	// -----------------------------------------------------------------------
	// Implementation of the FCSourceCodeGeneratorItf interface
	// -----------------------------------------------------------------------

	@Override
	public SourceCodeGeneratorItf generate(
		Type type, Object controllerDesc, Object contentDesc, Object source )
	throws IOException {

		/*
		 * Check parameter consistence.
		 */
		osgig.check(type,controllerDesc,contentDesc);

		SourceCodeGeneratorItf scg =
			super.generate(type,controllerDesc,contentDesc,source);
		return scg;
	}

	@Override
	protected InitializerOOCtrlClassGenerator getInitializerClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		return new InitializerOOCtrlClassGenerator(
			jc,fcscg,membraneDesc,ct,contentDesc,source);
	}

	@Override
	public SourceCodeGeneratorItf getInterfaceClassGenerator(InterfaceType it) {
		SourceCodeGeneratorItf pcg = osgig.getInterfaceClassGenerator(it);
		return pcg;
	}


	// ------------------------------------------------------------------
	// Implementation of methods defined in FCSourceCodeGenerator
	// ------------------------------------------------------------------

	@Override
	protected void generateInterfaceImpl( InterfaceType[] its, String ctrlDesc )
	throws IOException {

		for (InterfaceType it : its) {

			generateInterfaceImpl(it,ctrlDesc);

			/*
			 * Super method generate internal interfaces. We don't need them for
			 * osgiPrimitive.
			 */
		}
	}

	@Override
	protected void generateInterfaceImpl( InterfaceType it, String ctrlDesc )
	throws IOException {

		osgig.generateInterfaceImpl(it);
	}
}
