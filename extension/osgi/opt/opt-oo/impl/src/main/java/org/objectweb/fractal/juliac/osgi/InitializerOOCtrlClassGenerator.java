/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributors: Romain Rouvoy, Philippe Merle
 */

package org.objectweb.fractal.juliac.osgi;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.commons.lang.StringHelper;
import org.objectweb.fractal.juliac.core.helper.ComponentTypeHelper;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;

/**
 * This class generates the source code for initializing Fractal components.
 *
 * This initializer class generator assumes that the content, the interceptors
 * and the controllers are kept in separate classes and that the controllers are
 * implemented with objects.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 * @author Philipe Merle <Philippe.Merle@inria.fr>
 * @since 2.1.5
 */
public class InitializerOOCtrlClassGenerator
extends org.objectweb.fractal.juliac.opt.oo.InitializerOOCtrlClassGenerator {

	private static final String FRACTAL_API_BUNDLE = "fractal-api-bundle.jar";
	private static final String JULIA_RUNTIME_BUNDLE = "julia-runtime-bundle.jar";
	private static final String JULIAC_RUNTIME_OO_BUNDLE = "juliac-runtime-oo-bundle.jar";

	public InitializerOOCtrlClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		super(jc,fcscg,membraneDesc,ct,contentDesc,source);
	}

	/**
	 * Return the name of the class generated by this generator.
	 *
	 * This name generation process is reused in {@link
	 * org.objectweb.fractal.juliac.felix.JuliacOSGiFelixBootstrapComponentImpl}
	 * which provides a Fractal bootstrap component for OSGi/Felix.
	 */
	@Override
	public String getTargetTypeName() {

		/*
		 * In the case of osgiPrimitive component, content descriptors are of
		 * the form 'bundle1 bundle2 ... bundlen', e.g.
		 * osgi-felix-helloworld-interfaces.jar osgi-felix-helloworld-server.jar
		 */
		StringBuilder sb = new StringBuilder();
		String contentDescStr = (String) contentDesc;
		int len = contentDescStr.length();
		for (int i = 0; i < len; i++) {
			char c = contentDescStr.charAt(i);
			// TODO we should also replace all characters which are not legal in package names.
			if( c==' ' || c=='-' ) {c='.';}
			sb.append(c);
		}
		String contentClassName = sb.toString();

		String ctrlDesc = membraneDesc.getDescriptor();
		String membraneClassName = getMembraneClassName(ctrlDesc,contentClassName);
		String hexhashct = ComponentTypeHelper.hexhash(ct);
		String initializerClassName = membraneClassName+"FC"+hexhashct;
		return initializerClassName;
	}

	@Override
	protected void generateNewFcContentMethod( BlockSourceCodeVisitor mv ) {
		mv.visitln("    return null;");
	}

	@Override
	protected void generateContentChecks( BlockSourceCodeVisitor mv ) {
		// Indeed nothing
	}

	@Override
	public void generateInitializationContextForContent( BlockSourceCodeVisitor mv ) {

		/*
		 * Compute the number of client interfaces imported by the component.
		 */
		InterfaceType[] its = ct.getFcInterfaceTypes();
		int nbClientItfs = 0;
		for (InterfaceType it : its) {
			if( it.isFcClientItf() ) {
				nbClientItfs++;
			}
		}

		/*
		 * Retrieve the bundle jar file names specified in the content
		 * descriptor.
		 */
		String contentDescStr = (String) contentDesc;
		int colon = contentDescStr.indexOf(':');
		String bundleDesc = contentDescStr.substring(colon+1);
		String bundleDescStripped =
			StringHelper.trimHeadingAndTrailingBlanks(bundleDesc);
		String[] bundles = bundleDescStripped.split(" ");

		/*
		 * Prepare an array of bundles to hold the default bundles, the
		 * generated bundles associated with client interfaces and the bundles
		 * specified in the content descriptor.
		 */
		mv.visit  ("    "+BundleRevisionItf.class.getName()+"[] bundles = ");
		mv.visitln("new "+BundleRevisionItf.class.getName()+"["+(3+nbClientItfs+bundles.length)+"];");
		mv.visitln("    ClassLoader loader = getClass().getClassLoader();");

		/*
		 * Automatically load the default bundles: Fractal API, Julia Runtime
		 * and Juliac Runtime.
		 */
		mv.visit  ("    bundles[0] = ");
		mv.visit  (OSGiHelper.class.getName());
		mv.visit  (".installBundleIE(\"");
		mv.visit  (FRACTAL_API_BUNDLE);
		mv.visitln("\",loader);");
		mv.visit  ("    bundles[1] = ");
		mv.visit  (OSGiHelper.class.getName());
		mv.visit  (".installBundleIE(\"");
		mv.visit  (JULIA_RUNTIME_BUNDLE);
		mv.visitln("\",loader);");
		mv.visit  ("    bundles[2] = ");
		mv.visit  (OSGiHelper.class.getName());
		mv.visit  (".installBundleIE(\"");
		mv.visit  (JULIAC_RUNTIME_OO_BUNDLE);
		mv.visitln("\",loader);");

		/*
		 * Load the generated bundles associated with client interfaces.
		 */
		int i = 0;
		clientItfIdx = 3;
		for (InterfaceType it : its) {
			if( it.isFcClientItf() ) {
				String signature = it.getFcItfSignature();
				String jarname = signature.replace('.','-') + ".jar";
				mv.visit  ("    bundles["+(3+i)+"] = ");
				mv.visit  (OSGiHelper.class.getName());
				mv.visitln(".installBundleIE(\""+jarname+"\",loader);");
				i++;
			}
		}

		/*
		 * Load the bundles specified in the content descriptor.
		 */
		for (i = 0; i < bundles.length; i++) {
			mv.visit  ("    bundles["+(3+nbClientItfs+i)+"] = ");
			mv.visit  (OSGiHelper.class.getName());
			mv.visit  (".installBundleIE(\"");
			mv.visit  (bundles[i]);
			mv.visitln("\",loader);");
		}

		/*
		 * Start the bundles.
		 */
		mv.visitln("    for( int i = 0 ; i < "+(3+nbClientItfs+bundles.length)+" ; i++ ) {");
		mv.visit  ("      ");
		mv.visit  (OSGiHelper.class.getName());
		mv.visitln(".startBundleIE(bundles[i]);");
		mv.visitln("    }");

		/*
		 * Retrieve the service object of the last bundle. This is supposed to
		 * be the component content object.
		 */
		mv.visit  ("    content = ");
		mv.visit  (OSGiHelper.class.getName());
		mv.visitln(".getServiceObjectIE(bundles["+(2+nbClientItfs+bundles.length)+"]);");

		mv.visitln("    ic.interfaces.put(\"/bundle\",bundles);");

		if( nbClientItfs > 0 ) {
			mv.visitln("    Object bundledProxy = null;");
		}

		super.generateInitializationContextForContent(mv);
	}

	/**
	 * <p>
	 * Index of the current client interfaces in the array of bundles associated
	 * with this component. The initial value is <code>3</code> since three
	 * bundles are loaded by default:
	 * </p>
	 *
	 * <ul>
	 * <li><code>juliac-osgi-bundle-fractal-api.jar</code></li>,
	 * <li><code>juliac-osgi-bundle-julia-runtime.jar</code></li>,
	 * <li><code>juliac-osgi-bundle-juliac-runtime-oo.jar</code></li>.
	 * </ul>
	 *
	 * <p>
	 * The first bundle for a client interface is then stored at index 3.
	 * </p>
	 */
	/*
	 * Potential issue if the object is multi-threaded. The counter will be
	 * shared by different threads. However I don't expect this to be the case.
	 * Or should I?
	 */
	private int clientItfIdx = 3;

	@Override
	protected void generateNFICMExternalInterface(
		BlockSourceCodeVisitor mv, InterfaceType it,
		boolean attrimplgenerated ) {

		super.generateNFICMExternalInterface(mv,it,attrimplgenerated);

		/*
		 * For client interfaces, instruct the bundled proxy to delegate to the
		 * interceptor object.
		 */
		if( it.isFcClientItf() ) {
			mv.visit  ("    bundledProxy = ");
			mv.visit  (OSGiHelper.class.getName());
			mv.visit  (".getServiceObjectIE(bundles[");
			mv.visit  (Integer.toString(clientItfIdx));
			mv.visitln("]);");

			mv.visit("    ");
			mv.visit(ReflectionHelper.class.getName());
			mv.visitln(".initBundledProxy(bundledProxy,proxy);");

			/*
			 * Move to the next client interface.
			 * Patch contributed by Philippe on March 23, 2011 for experiments
			 * conducted by Christophe Munilla.
			 */
			clientItfIdx++;
		}
	}
}
