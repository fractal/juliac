/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.osgi.control.binding;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.ComponentInterface;

/**
 * Mixin layer for implementing the OSGi binding policy.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.6
 */
public abstract class OSGiBasicBindingControllerMixin
implements BindingController {

	// -------------------------------------------------------------------------
	// Private constructor
	// -------------------------------------------------------------------------

	private OSGiBasicBindingControllerMixin() {}

	// -------------------------------------------------------------------------
	// Fields and methods added and overriden by the mixin class
	// -------------------------------------------------------------------------

	public String[] listFc() {

		Object[] itfs = _this_weaveableC.getFcInterfaces();
		int size = 0;
		for (int i = 0; i < itfs.length; i++) {
			Object o = itfs[i];
			Interface itf = (Interface)o;
			InterfaceType it = (InterfaceType) itf.getFcItfType();
			if( it.isFcClientItf() ) {
				size++;
			}
		}

		String[] names = new String[size];
		int k = 0;
		for (int i = 0; i < itfs.length; i++) {
			Object o = itfs[i];
			Interface itf = (Interface)o;
			InterfaceType it = (InterfaceType) itf.getFcItfType();
			if( it.isFcClientItf() ) {
				names[k] = itf.getFcItfName();
				k++;
			}
		}
		return names;
	}

	public Object lookupFc( final String clientItfName )
	throws NoSuchInterfaceException {

		ComponentInterface ci = (ComponentInterface)
			_this_weaveableC.getFcInterface(clientItfName);
		Object dst = ci.getFcItfImpl();
		return dst;
	}

	public void bindFc( final String clientItfName, final Object serverItf )
	throws NoSuchInterfaceException {

		ComponentInterface ci = (ComponentInterface)
			_this_weaveableC.getFcInterface(clientItfName);
		ci.setFcItfImpl(serverItf);
	}

	public void unbindFc( final String clientItfName )
	throws NoSuchInterfaceException {

		ComponentInterface ci = (ComponentInterface)
			_this_weaveableC.getFcInterface(clientItfName);
		ci.setFcItfImpl(null);
	}


	// -------------------------------------------------------------------------
	// Fields and methods required by the mixin class in the base class
	// -------------------------------------------------------------------------

	/**
	 * The {@link Component} interface of the component to which this controller
	 * object belongs.
	 */
	public Component _this_weaveableC;
}
