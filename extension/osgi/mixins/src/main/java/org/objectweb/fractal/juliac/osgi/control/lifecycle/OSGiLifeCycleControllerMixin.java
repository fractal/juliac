/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.osgi.control.lifecycle;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.control.lifecycle.ChainedIllegalLifeCycleException;
import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.OSGiRevisionException;

/**
 * Mixin layer for implementing the OSGi lifecycle policy.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.5
 */
public abstract class OSGiLifeCycleControllerMixin
implements Controller, OSGiLifeCycleController, LifeCycleCoordinator {

	// ------------------------------------------------------------------------
	// Private constructor
	// ------------------------------------------------------------------------

	private OSGiLifeCycleControllerMixin() {}

	// ------------------------------------------------------------------------
	// Implementation of the Controller interface
	// ------------------------------------------------------------------------

	private BundleRevisionItf[] bundles;

	/**
	 * Initializes the fields of this mixin and then calls the overriden method.
	 *
	 * @param ic information about the component to which this controller object
	 *      belongs.
	 * @throws InstantiationException if the initialization fails.
	 */
	public void initFcController( final InitializationContext ic )
	throws InstantiationException {

		/*
		 * Propagate the invocation to other mixin layers.
		 */
		_super_initFcController(ic);

		bundles = (BundleRevisionItf[]) ic.interfaces.get("/bundle");
	}


	// ------------------------------------------------------------------------
	// Implementation of the OSGiFelixLifeCycleController interface
	// ------------------------------------------------------------------------

	/**
	 * Install the bundle associated with the current component.
	 *
	 * @return  the installed bunded
	 * @throws BundleException  in case of problem during the installation
	 */
	public BundleRevisionItf installFcBundle() {
		/*
		 * At some point, installing the bundle is problematic since the bundle
		 * is installed when the component is instanciated and re-installing it
		 * would mean that a new content instance should be dealt with.
		 */
		throw new UnsupportedOperationException();
	}

	/**
	 * Start the bundle associated with the current component.
	 *
	 * Implement {@link LifeCycleCoordinator#setFcStarted()} instead of {@link
	 * org.objectweb.fractal.api.control.LifeCycleController#startFc()} in order
	 * to obtain a consistent behavior when recursively starting a hierarchy
	 * containing the component controlled by this controller.
	 *
	 * Note: {@link
	 * org.objectweb.fractal.api.control.LifeCycleController#startFc()} is not
	 * called recursively.
	 *
	 * @throws IllegalLifeCycleException  in case of problem during the start
	 */
	public boolean setFcStarted() throws IllegalLifeCycleException {

		for (int i = 0; i < bundles.length; i++) {
			try {
				bundles[i].start();
			}
			catch (OSGiRevisionException e) {
				throw new ChainedIllegalLifeCycleException(e,null,"");
			}
		}

		return _super_setFcStarted();
	}

	/**
	 * Stop the bundle associated with the current component.
	 *
	 * Implement {@link LifeCycleCoordinator#setFcStopped()} instead of {@link
	 * org.objectweb.fractal.api.control.LifeCycleController#stopFc()} in order
	 * to obtain a consistent behavior when recursively stopping a hierarchy
	 * containing the component controlled by this controller.
	 *
	 * Note: {@link
	 * org.objectweb.fractal.api.control.LifeCycleController#stopFc()} is not
	 * called recursively.
	 *
	 * @throws IllegalLifeCycleException  in case of problem during the stop
	 */
	public boolean setFcStopped() throws IllegalLifeCycleException {

		for (int i = 0; i < bundles.length; i++) {
			try {
				bundles[i].stop();
			}
			catch (OSGiRevisionException e) {
				throw new ChainedIllegalLifeCycleException(e,null,"");
			}
		}

		return _super_setFcStopped();
	}

	/**
	 * Uninstall the bundle associated with the current component.
	 *
	 * @throws BundleException  in case of problem during the uninstallation
	 */
	public void uninstallFcBundle() throws OSGiRevisionException {
		for (int i = 0; i < bundles.length; i++) {
			bundles[i].uninstall();
		}
	}


	// ------------------------------------------------------------------------
	// Fields and methods required by the mixin class in the base class
	// ------------------------------------------------------------------------

	/**
	 * The {@link Controller#initFcController initFcController} method overriden
	 * by this mixin.
	 *
	 * @param ic information about the component to which this controller object
	 *      belongs.
	 * @throws InstantiationException if the initialization fails.
	 */
	public abstract void _super_initFcController(InitializationContext ic)
	throws InstantiationException;

	public abstract boolean _super_setFcStarted() throws IllegalLifeCycleException;
	public abstract boolean _super_setFcStopped() throws IllegalLifeCycleException;
}
