/***
 * Juliac
 * Copyright (C) 2012-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */

package org.objectweb.fractal.juliac.osgi.revision;

import java.util.logging.Logger;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf;

/**
 * Abstract implementation of the OSGiRevisionItf interface.
 *
 * @param <B>   the Bundle type
 * @param <BC>  the BundleContext type
 *
 * @author Christophe Munilla
 * @since 2.5
 */
public abstract class AbstractOSGi<B,BC> implements OSGiRevisionItf<B,BC>
{
	protected static final Logger LOGGER =
		Logger.getLogger(AbstractOSGi.class.getCanonicalName());

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf#getBundleContext()
	 */
	public BundleContextRevisionItf<B,BC> getBundleContext()
	throws OSGiRevisionException {
		BundleContextRevisionItf<B,BC> context = createContextRevision();
		return context;
	}

	/**
	 * Return a buildin BundleContextRevision using the current revision
	 * and the OSGi BundleContext associated to the current PlatformItf
	 * instance.
	 */
	protected abstract AbstractBundleContext<B,BC> createContextRevision();
}
