/**
 * Juliac
 * Copyright (C) 2012-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */

package org.objectweb.fractal.juliac.osgi.revision;

/**
 * A key for a map of bundles.
 *
 * @author Christophe Munilla
 * @since 2.5
 */
public class BundleKey {

	protected final long bundleId;
	protected final String symbolicName;

	public BundleKey( long bundleId, String symbolicName ) {
		this.bundleId = bundleId;
		this.symbolicName = symbolicName;
	}

	@Override
	public boolean equals( Object object ) {
		if( object instanceof BundleKey ) {
			return equals((BundleKey)object);

		}
		else if( long.class.isAssignableFrom(object.getClass()) ) {
			return equals(long.class.cast(object).longValue());

		}
		else if( object instanceof String ) {
			return equals((String)object);
		}
		return false;
	}

	public boolean equals( BundleKey key ) {
		return ((key.bundleId>-1 && key.bundleId==bundleId)
				||(key.symbolicName!=null && key.symbolicName.equals(symbolicName)));
	}

	public boolean equals( long bundleId ) {
		return (bundleId>-1 && this.bundleId==bundleId);
	}

	public boolean equals(String symbolicName) {
		return (symbolicName!=null && symbolicName.equals(this.symbolicName));
	}
}
