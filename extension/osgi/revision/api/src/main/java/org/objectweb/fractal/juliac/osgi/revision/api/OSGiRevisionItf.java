/**
 * Juliac
 * Copyright (C) 2012-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */

package org.objectweb.fractal.juliac.osgi.revision.api;

import org.objectweb.fractal.juliac.osgi.revision.OSGiRevisionException;

/**
 * An intermediary with an OSGi Framework
 *
 * @param <B>   the Bundle type
 * @param <BC>  the BundleContext type
 *
 * @author Christophe Munilla
 * @since 2.5
 */
public interface OSGiRevisionItf<B,BC> {

	public static final String OSGi_REVISION_ID = "OSGi_REVISION_ID";

	/*
	 * These four static fields have to be redefined if necessary by each
	 * specific revision implementation.
	 */
	public static String BUNDLE_CLASSNAME = "org.osgi.framework.Bundle";
	public static String BUNDLECONTEXT_CLASSNAME = "org.osgi.framework.BundleContext";
	public static String BUNDLEACTIVATOR_CLASSNAME = "org.osgi.framework.BundleActivator";
	public static String SERVICEREFERENCE_CLASSNAME = "org.osgi.framework.ServiceReference";

	/**
	 * Return the BundleContext wrapped into a BundleContextRevisionItf
	 * implementation instance.
	 */
	public BundleContextRevisionItf<B,BC> getBundleContext()
	throws OSGiRevisionException;

	/**
	 * Return the inner ClassLoader of the framework.
	 */
	public ClassLoader getFrameworkClassLoader();
}
