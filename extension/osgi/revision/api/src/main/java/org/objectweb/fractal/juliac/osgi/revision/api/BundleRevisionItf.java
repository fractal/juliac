/**
 * Juliac
 * Copyright (C) 2012-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */

package org.objectweb.fractal.juliac.osgi.revision.api;

import java.io.InputStream;
import java.net.URL;
import java.util.Dictionary;
import java.util.Enumeration;

import org.objectweb.fractal.juliac.osgi.revision.OSGiRevisionException;

/**
 * A wrapper for an OSGi Bundle
 *
 * @param <B>   the Bundle type
 * @param <BC>  the BundleContext type
 *
 * @author Christophe Munilla
 * @since 2.5
 */
public interface BundleRevisionItf<B,BC> {

	/**
	 * Return the wrapped Bundle.
	 */
	public B getBundle();

	/**
	 * Return the parent BundleContextRevisionItf implementation instance.
	 */
	public BundleContextRevisionItf<B,BC> getBundleContext();

	/**
	 * Return the headers of the wrapped Bundle.
	 */
	public Dictionary<String,String> getHeaders();

	/**
	 * Return the name (i.e 'Bundle-SymbolicName' header value) of the wrapped
	 * Bundle.
	 */
	public String getSymbolicName();

	/**
	 * Return the location of the wrapped Bundle.
	 */
	public String getLocation();

	/**
	 * Start the wrapped Bundle.
	 */
	public void start() throws OSGiRevisionException;

	/**
	 * Stop the wrapped Bundle.
	 */
	public void stop() throws OSGiRevisionException;

	/**
	 * Update the wrapped Bundle.
	 */
	public void update() throws OSGiRevisionException;

	/**
	 * Update the wrapped bundle using the stream passed on as a parameter.
	 */
	public void update( InputStream stream ) throws OSGiRevisionException;

	/**
	 * Remove the wrapped bundle from the BundleContext.
	 *
	 * @throws OSGiRevisionException
	 */
	public void uninstall() throws OSGiRevisionException;

	/**
	 * Return the service object exported by the wrapped bundle.
	 */
	public Object getServiceObject();

	/**
	 * Load a class in the bundle.
	 *
	 * @param className  the canonical name of the searched class
	 * @return           the class
	 * @throws ClassNotFoundException  if the class cannot been found
	 */
	public Class<?> loadClass( String className ) throws ClassNotFoundException;

	/**
	 * Retrieve a resource in the Bundle, and search in others if it has not
	 * been found.
	 *
	 * @param resourceName  the name (relative path) of the searched resource
	 * @return              the URL of the resource if it has been found.
	 *                         <code>null</code> otherwise.
	 */
	public URL getResource( String resourceName );

	/**
	 * Return an enumeration of resources existing in the directory relative
	 * path passed on as a parameter.
	 */
	public Enumeration<String> getEntryPaths( String path );

	/**
	 * Return the URL of the resource which relative path is passed on as a
	 * parameter, or <code>null</code> if it doesn't exist.
	 */
	public URL getEntry( String path );

	/**
	 * Return the identifier of the wrapped bundle.
	 */
	public long getBundleId();

	/**
	 * Return the integer value identifier of state of the wrapped bundle.
	 */
	public int getState();

}
