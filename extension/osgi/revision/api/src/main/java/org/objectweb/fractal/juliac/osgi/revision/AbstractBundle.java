/**
 * Juliac
 * Copyright (C) 2012-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */

package org.objectweb.fractal.juliac.osgi.revision;

import java.util.logging.Logger;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;

/**
 * Abstract implementation of the BundleRevisionItf interface
 *
 * @param <B>   the Bundle type
 * @param <BC>  the BundleContext type
 *
 * @author Christophe Munilla
 * @since 2.5
 */
public abstract class AbstractBundle<B,BC> implements BundleRevisionItf<B,BC> {
	/**
	 * static Logger
	 */
	protected static final Logger LOGGER = Logger.getLogger(AbstractBundle.class.getCanonicalName());

	protected B bundle;
	protected AbstractBundleContext<B,BC> context;

	/**
	 * Constructor
	 */
	protected AbstractBundle(AbstractBundleContext<B,BC> context, B bundle)
	{
		this.bundle = bundle;
		this.context = context;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#
	 * getBundleContext()
	 */
	public BundleContextRevisionItf<B,BC> getBundleContext()
	{
		return context;
	}
}
