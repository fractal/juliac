/**
 * Juliac
 * Copyright (C) 2012-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */

package org.objectweb.fractal.juliac.osgi.revision;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;

/**
 * Abstract implementation of the BundleContextRevisionItf interface
 *
 * @param <B>   the Bundle type
 * @param <BC>  the BundleContext type
 *
 * @author Christophe Munilla
 * @since 2.5
 */
public abstract class AbstractBundleContext<B,BC>
implements BundleContextRevisionItf<B,BC> {

	protected static final Logger LOGGER =
		Logger.getLogger(AbstractBundleContext.class.getCanonicalName());

	private AbstractOSGi<B,BC> revision;
	protected BC bundleContext;
	protected static Map<BundleKey,AbstractBundle<?,?>> bundles =
		new HashMap<>();

	protected AbstractBundleContext(
		AbstractOSGi<B,BC> revision, BC bundleContext) {

		this.setRevision(revision);
		this.bundleContext = bundleContext;
	}

	/**
	 * Return the wrapped BundleContext.
	 */
	public BC getBundleContext()
	{
		return this.bundleContext;
	}

	/**
	 * Register the AbstractBundle passed on as a parameter.
	 */
	public void registerBundle( AbstractBundle<B,BC> bundle )
	{
		if(bundle.getBundle() != bundles.get(new BundleKey(bundle.getBundleId(),null)))
		{
			bundles.put(
					new BundleKey(bundle.getBundleId(),bundle.getSymbolicName()),
					bundle);
		}
	}

	/**
	 * Return a previously registered AbstractBundle using its identifier.
	 */
	@SuppressWarnings("unchecked")
	protected BundleRevisionItf<B,BC> getRegisteredBundle(long bundleId)
	{
		return (BundleRevisionItf<B, BC>) bundles.get(new BundleKey(bundleId,null));
	}


	/**
	 * Return a previously registered AbstractBundle using its name.
	 *
	 * @param symbolicName
	 *          the name of the AbstractBundle to return
	 */
	@SuppressWarnings("unchecked")
	protected BundleRevisionItf<B,BC> getRegisteredBundle(String symbolicName)
	{
		return (BundleRevisionItf<B, BC>) bundles.get(new BundleKey(-1,symbolicName));
	}

	/**
	 * Define the AbstractOSGi instance to which this AbstractBundleContext is associated
	 */
	public void setRevision(AbstractOSGi<B,BC> revision)
	{
		this.revision = revision;
	}

	/**
	 * Return the AbstractOSGi instance to which this AbstractBundleContext is associated
	 */
	public AbstractOSGi<B,BC> getRevision()
	{
		return revision;
	}

}
