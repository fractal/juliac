/**
 * Juliac
 * Copyright (C) 2012-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */

package org.objectweb.fractal.juliac.osgi.revision.api;

import java.io.InputStream;
import java.net.URL;
import java.util.Dictionary;

import org.objectweb.fractal.juliac.osgi.revision.OSGiRevisionException;
import org.objectweb.fractal.juliac.osgi.revision.event.BundleListenerRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.event.FrameworkListenerRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.event.ServiceListenerRevisionItf;

/**
 * Wrapper for an OSGi BundleContext
 *
 * @param <B>   the Bundle type
 * @param <BC>  the BundleContext type
 *
 * @author Christophe Munilla
 * @since 2.5
 */
public interface BundleContextRevisionItf<B,BC> {

	/**
	 * Return the BundleRevisionItf instance associated to the current
	 * BundleContextRevisionItf one
	 */
	public BundleRevisionItf<B,BC> getBundle();

	/**
	 * Return the bundle, wrapped in a BundleRevisionItf implementation
	 * instance which identifier is passed as a parameter.
	 *
	 * @param bundleId  the bundle identifier
	 */
	public BundleRevisionItf<B,BC> getBundle( long bundleId );

	/**
	 * Return an array of bundles, wrapped in BundleRevisionItf implementation
	 * instances, installed in the BundleContext.
	 */
	public BundleRevisionItf<B,BC>[] getBundles();

	/**
	 * Test whether the filter passed on as a parameter is a valid (cf. LDAP
	 * format) one.
	 *
	 * @param filter  the string filter
	 * @return        <code>true</code> if the filter is valid,
	 *                <code>false</code> otherwise.
	 */
	public boolean validateFilter( String filter );

	/**
	 * Register a service in the BundleContext, and return a registration
	 * identifier.
	 *
	 * @param className  the class name of the service
	 * @param service    the service object
	 * @param properties
	 *         the properties associated to the service in the BundleContext
	 * @return  the registration identifier
	 */
	public int registerService(
		String className, Object service,
		Dictionary<String,String> properties );

	/**
	 * Unregister the service associated to the registration identifier passed
	 * as a parameter.
	 *
	 * @param registrationId  the registration identifier
	 */
	public void unregisterService( int registrationId );

	/**
	 * Return the first service object found which class name is passed as a
	 * parameter and respecting the filter passed as a parameter.
	 *
	 * @param className  the class name of the service
	 * @param filter     registered properties of the searched service
	 */
	public Object getService( String className, String filter );

	/**
	 * Return the service object which registration identifier is passed as a
	 * parameter.
	 *
	 * @param bundleId  the bundle identifier
	 * @return          the service object if it exists,
	 *                     <code>null</code> otherwise.
	 */
	public Object getService( int registrationId );

	/**
	 * Return the bundle, wrapped in a BundleRevisionItf implementation
	 * instance, promoting the first occurrence found of the service which
	 * class name is the same as the one passed as a parameter and which
	 * properties are equals to those defined in the filter string passed as a
	 * parameter.
	 *
	 * @param className  the class name of the service
	 * @param filter     the properties of the service
	 * @return  the bundle which promote the service wrapped in a
	 *          BundleRevisionItf implementation instance.
	 */
	public BundleRevisionItf<B,BC> getServiceBundle(
		String className, String filter );

	/**
	 * Return the list of found service objects which class name is passed as a
	 * parameter and respecting the filter also passed as a parameter.
	 *
	 * @param className
	 *            the class name of the Service
	 * @param filter
	 *            registered properties of the services
	 * @return
	 *          the array of Service objects if they exist, NULL otherwise
	 */
	public Object[] getServices( String className, String filter );

	/**
	 * Register the BundleListener wrapped in the BundleListenerRevisionItf implementation instance
	 * passed on as a parameter.
	 *
	 * @param bundleListener
	 *          the BundleListenerRevisionItf implementation instance containing the BundleListener
	 *           to register
	 */
	public void registerBundleListener(
		BundleListenerRevisionItf<B,BC> bundleListener );

	/**
	 * Unregister the BundleListener wrapped in the BundleListenerRevisionItf implementation instance
	 * passed on as a parameter.
	 *
	 * @param bundleListener
	 *          the BundleListenerRevisionItf implementation instance containing the BundleListener
	 *          to unregister
	 */
	public void unregisterBundleListener(
		BundleListenerRevisionItf<B,BC> bundleListener );

	/**
	 * Register the ServiceListener wrapped in the ServiceListenerRevisionItf implementation instance
	 * passed on as a parameter.
	 *
	 * @param serviceListener
	 *          the ServiceListenerRevisionItf implementation instance containing the ServiceListener
	 *          to unregister
	 */
	public void registerServiceListener(
		ServiceListenerRevisionItf serviceListener );

	/**
	 * Register a ServiceListener wrapped in the ServiceListenerRevisionItf implementation instance
	 * passed on as a parameter which will be addressee of the filter-compliant-services events
	 *
	 * @param serviceListener
	 *          the ServiceListenerRevisionItf implementation instance containing the ServiceListener
	 *          to register
	 * @param filter
	 *          the string filter to define spied services
	 */
	public void registerServiceListener(
		ServiceListenerRevisionItf serviceListener, String filter );

	/**
	 * Unregister the ServiceListener wrapped in the ServiceListenerRevisionItf implementation instance
	 * passed on as a parameter
	 *
	 * @param serviceListener
	 *          the ServiceListenerRevisionItf implementation instance containing the ServiceListener
	 *          to unregister
	 */
	public void unregisterServiceListener(
		ServiceListenerRevisionItf serviceListener );

	/**
	 * Register the FrameworkListener wrapped in the FrameworkListenerRevisionItf implementation instance
	 * passed on as a parameter
	 *
	 * @param frameworkListener
	 *          the the FrameworkListenerRevisionItf implementation instance
	 *          containing the FrameworkListener to register
	 */
	public void registerFrameworkListener(
		FrameworkListenerRevisionItf frameworkListener );

	/**
	 * Unregister the FrameworkListener wrapped in the FrameworkListenerRevisionItf implementation instance
	 * passed on as a parameter
	 *
	 * @param frameworkListener
	 *          the the FrameworkListenerRevisionItf implementation instance
	 *          containing the FrameworkListener to unregister
	 */
	public void unregisterFrameworkListener(
		FrameworkListenerRevisionItf frameworkListener );

	/**
	 * Install a Bundle using the jar file location passed on as a parameter and
	 * return its BundleRevisionItf wrapper
	 *
	 * @param location
	 *          the jar file location
	 * @return
	 *          the Bundle identifier
	 */
	public BundleRevisionItf<B,BC> installBundle( String location )
	throws OSGiRevisionException;

	/**
	 * Install a Bundle using the jar file InputStream and location passed on as
	 * a parameter and return its BundleRevisionItf wrapper
	 *
	 * @param location
	 *            the jar file location
	 * @param bundleStream
	 *            the jar file InputStream
	 */
	public BundleRevisionItf<B,BC> installBundle(
		String location, InputStream bundleStream )
	throws OSGiRevisionException;

	/**
	 * Load a class in the BundleContext
	 *
	 * @param className
	 *          the canonical name of the searched class
	 * @return
	 *          the Class if it has been found. Null otherwise
	 * @throws ClassNotFoundException
	 */
	public Class<?> loadClass( String className ) throws ClassNotFoundException;

	/**
	 * Retrieve a resource in the BundleContext
	 *
	 * @param resourceName
	 *          the (relative path) name of the searched resource
	 * @return
	 *          the URL of the resource if it has been found. Null Otherwise
	 */
	public URL getResource( String resourceName );

	/**
	 * Return a property of the associated BundleContext
	 *
	 * @param key
	 *          the property name
	 * @return
	 *          the value of the property
	 */
	public String getProperty( String key );

	/**
	 * Return the name of the Event which type is
	 * passed on as a parameter
	 *
	 * @param serviceEventType
	 *          the ServiceEvent type value
	 * @return
	 *          the ServiceEvent name
	 */
	public String mapServiceEvent( int serviceEventType );

	/**
	 * Return the name of the Event which type is
	 * passed on as a parameter
	 *
	 * @param bundleEventType
	 *          the BundleEvent type value
	 * @return
	 *          the BundleEvent name
	 */
	public String mapBundleEvent( int bundleEventType );

	/**
	 * Return the name of the Event which type is
	 * passed on as a parameter
	 *
	 * @param frameworkEventType
	 *          the FrameworkEvent type value
	 * @return
	 *          the FrameworkEvent name
	 */
	public String mapFrameworkEvent( int frameworkEventType );

	/**
	 * Return the name of the Bundle's state which value is
	 * passed on as a parameter
	 *
	 * @param bundleState
	 *          the State value
	 * @return
	 *          the State name
	 */
	public String mapBundleState( int bundleState );
}
