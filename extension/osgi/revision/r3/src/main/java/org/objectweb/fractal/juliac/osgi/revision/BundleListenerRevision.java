/**
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */
package org.objectweb.fractal.juliac.osgi.revision;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.event.BundleListenerRevisionItf;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleListener;

/**
 * BundleListener implementation for an OSGi-R3 environment
 */
public class BundleListenerRevision implements BundleListener
{
	public static final int INSTALLED = BundleEvent.INSTALLED;
	public static final int STARTED = BundleEvent.STARTED;
	public static final int STOPPED = BundleEvent.STOPPED;
	public static final int UNINSTALLED = BundleEvent.UNINSTALLED;
	public static final int UPDATED = BundleEvent.UPDATED;

	protected BundleListenerRevisionItf<Bundle,BundleContext> bundleListener;
	protected BundleContextRevisionItf<Bundle, BundleContext> context;

	/**
	 * @param bundleListener
	 * @param bundleContextRevisionImpl
	 */
	public BundleListenerRevision(
			BundleContextRevisionItf<Bundle,BundleContext> context,
			BundleListenerRevisionItf<Bundle,BundleContext> bundleListener)
	{
		this.context = context;
		this.bundleListener = bundleListener;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.osgi.framework.BundleListener#
	 * bundleChanged(org.osgi.framework.BundleEvent)
	 */
	public void bundleChanged(BundleEvent event)
	{
		this.bundleListener.bundleChanged(
				context.getBundle(event.getBundle().getBundleId()),
				event.getType());
	}
}
