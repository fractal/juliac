/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */
package org.objectweb.fractal.juliac.osgi.revision.api;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;

import org.osgi.framework.Bundle;

/**
 * Define methods of a ClassLoader implemented specifically for an OSGi-R3 platform
 */
public interface BundleContextClassLoader
{
	/**
	 * Load a class presents in the BundleContext
	 *
	 * @param className
	 *          the canonical name of the class to load
	 * @return
	 *          the class if it has been found
	 * @throws ClasNotFoundException
	 *          if the class has not been found
	 */
	Class<?> loadClass(String className) throws ClassNotFoundException;

	/**
	 * Load a class presents in the Bundle which identifier is passed on as a
	 * parameter
	 *
	 * @param className
	 *          the canonical name of the class to load
	 * @param bundleId
	 *          the identifier of the Bundle where to search in
	 * @return
	 *          the class if it has been found
	 * @throws ClasNotFoundException
	 *          if the class has not been found
	 */
	Class<?> loadClass(String className,long bundleId) throws ClassNotFoundException;

	/**
	 * Search and return the URL of the resource which name (relative path) is passed
	 * on as a parameter if it can be found in the BundleContext
	 *
	 * @param resourceName
	 *          the name (relative path) of the searched resource
	 * @return
	 *          the URL of the resource if it has been found
	 *          null otherwise
	 */
	URL getResource(String resourceName);

	/**
	 * Search and Return the URL of the resource which name (relative path) is passed
	 * on as a parameter in the Bundle which identifier is also passed on as a
	 * parameter
	 *
	 * @param resourceName
	 *          the name (relative path) of the searched resource
	 * @param bundleId
	 *          the identifier of the Bundle where to search in
	 * @return
	 *          the resource's URL if it has been found. Null Otherwise
	 */
	URL getEntry(String resourceName, long bundleId);

	/**
	 * Enumerate entries existing in the Bundle which identifier is passed on as a parameter
	 * under the location which path is passed on as a parameter
	 *
	 * @param path
	 * @param bundleId
	 * @return
	 */
	Enumeration<String> getEntryPaths(String path, long bundleId);

	/**
	 * Fill in the list passed on as a parameter with all found resources
	 * which name (relative path) is equal (respect the pattern) to the
	 * 'resourceName' parameter
	 *
	 * @param resourceName
	 *          the name (relative path) of the searched resources
	 * @param resources
	 *          the list to fill in
	 * @throws IOException
	 */
	Enumeration<URL> getResources(String resourceName) throws IOException;

	/**
	 * Update the current BundleContextClassLoader
	 *
	 * @param bundleId
	 *          the Bundle which is going to be uninstalled
	 */
	void uninstallBundle(long bundleId);

	/**
	 * Update the current BundleContextClassLoader
	 *
	 * @param bundle
	 *          the new Bundle to register
	 */
	void registerBundle(Bundle bundle);

	/**
	 * Update the current BundleContextClassLoader
	 *
	 * @param bundle
	 *          the Bundle to update
	 */
	void updateBundle(Bundle bundle);
}
