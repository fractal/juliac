/**
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */
package org.objectweb.fractal.juliac.osgi.revision;

import org.objectweb.fractal.juliac.osgi.revision.event.ServiceListenerRevisionItf;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;

/**
 * ServiceListener implementation for an OSGi-R3 environment
 */
public class ServiceListenerRevision implements ServiceListener
{
	public static final int MODIFIED = ServiceEvent.MODIFIED;
	public static final int REGISTERED = ServiceEvent.REGISTERED;
	public static final int UNREGISTERING = ServiceEvent.UNREGISTERING;

	private ServiceListenerRevisionItf serviceListener;
	private AbstractBundleContext<Bundle, BundleContext> bundleContext;

	/**
	 * Constructor
	 *
	 * @param serviceListener
	 */
	public ServiceListenerRevision(ServiceListenerRevisionItf serviceListener,
			AbstractBundleContext<Bundle,BundleContext> bundleContext)
	{
		this.serviceListener = serviceListener;
		this.bundleContext = bundleContext;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.osgi.framework.ServiceListener#serviceChanged(org.osgi.framework.ServiceEvent)
	 */
	public void serviceChanged(ServiceEvent event)
	{
	   Object service = bundleContext.getBundleContext().getService(
			   event.getServiceReference());

	   if(serviceListener.serviceChanged(service,event.getType()))
	   {
		   bundleContext.getBundleContext().ungetService(
				   event.getServiceReference());
	   }
	}
}
