/**
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */
package org.objectweb.fractal.juliac.osgi.revision;

import java.io.InputStream;
import java.net.URL;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextClassLoader;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.event.BundleListenerRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.event.FrameworkListenerRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.event.ServiceListenerRevisionItf;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleException;
import org.osgi.framework.BundleListener;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.FrameworkListener;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;


/**
 * BundleContextRevisionItf implementation for an OSGi-R3 environment
 */
public class BundleContextRevision
extends AbstractBundleContext<Bundle,BundleContext>
{

	protected static Logger logger = Logger.getLogger(BundleContextRevision.class.getCanonicalName());

	protected Hashtable<Integer, ServiceRegistration> registrations;
	protected Hashtable<BundleListenerRevisionItf<Bundle,BundleContext>,BundleListener> bundleListeners;
	protected Hashtable<ServiceListenerRevisionItf,ServiceListener> serviceListeners;
	protected Hashtable<FrameworkListenerRevisionItf,FrameworkListener> frameworkListeners;

	private BundleContextClassLoader classLoader;


	/**
	 * Constructor
	 *
	 * @param OSGiRevision
	 * @param systemBundleContext
	 * @param classLoader
	 */
	public BundleContextRevision(OSGiRevision osGiRevision,
			BundleContext bundleContext,
			BundleContextClassLoader classLoader)
	{
		super(osGiRevision,bundleContext);
		this.classLoader = classLoader;
		this.registrations = new Hashtable<>();
		this.bundleListeners = new Hashtable<>();
		this.serviceListeners = new Hashtable<>();
		this.frameworkListeners = new Hashtable<>();
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#getBundles()
	 */
	public BundleRevisionItf<Bundle, BundleContext>[] getBundles()
	{
		Bundle[] bundles = super.bundleContext.getBundles();
		BundleRevisionItf<Bundle, BundleContext>[] bundlesR = new BundleRevisionItf[bundles.length];
		int n = 0;
		for(;n<bundles.length;n++)
		{
			Bundle bundle = bundles[n];
			BundleRevision bundleR = null;
			bundleR = (BundleRevision) super.getRegisteredBundle(bundle.getBundleId());
			if(bundleR == null)
			{
				bundleR = new BundleRevision(getClassLoader(), this,bundle);
				super.registerBundle(bundleR);
			}
			bundlesR[n] = bundleR;
		}
		return bundlesR;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * installBundle(java.lang.String)
	 */
	public BundleRevisionItf<Bundle,BundleContext>
	installBundle(String location) throws OSGiRevisionException
	{
		try
		{
			Bundle bundle = super.bundleContext.installBundle(location);
			BundleRevision bundleR = null;
			bundleR = (BundleRevision) super.getRegisteredBundle(bundle.getBundleId());
			if(bundleR == null)
			{
				bundleR = new BundleRevision(getClassLoader(), this,bundle);
				super.registerBundle(bundleR);
			}
			return bundleR;

		} catch (BundleException e)
		{
			logger.log(Level.WARNING,e.getMessage(),e);
			throw new OSGiRevisionException(e.getMessage());
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * installBundle(java.lang.String, java.io.InputStream)
	 */
	public BundleRevisionItf<Bundle,BundleContext>
	installBundle(String location, InputStream bundleStream)
			throws OSGiRevisionException
	{
		try
		{
			Bundle bundle = super.bundleContext.installBundle(location,
					bundleStream);
			BundleRevision bundleR = null;
			bundleR = (BundleRevision) super.getRegisteredBundle(bundle.getBundleId());
			if(bundleR == null)
			{
				bundleR = new BundleRevision(getClassLoader(), this,bundle);
				super.registerBundle(bundleR);
			}
			return bundleR;

		} catch (BundleException e)
		{
			logger.log(Level.WARNING,e.getMessage(),e);
			throw new OSGiRevisionException(e.getMessage());
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#getBundle()
	 */
	public BundleRevisionItf<Bundle, BundleContext> getBundle()
	{
		Bundle bundle = super.bundleContext.getBundle();
		if(bundle != null)
		{
			BundleRevision bundleR = null;
			bundleR = (BundleRevision) super.getRegisteredBundle(bundle.getBundleId());
			if(bundleR == null)
			{
				bundleR = new BundleRevision(getClassLoader(), this,bundle);
				super.registerBundle(bundleR);
			}
			return bundleR;
		}
		return null;

	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * getBundle(int)
	 */
	public BundleRevisionItf<Bundle,BundleContext> getBundle(long bundleId)
	{
		Bundle bundle = super.bundleContext.getBundle(bundleId);
		if(bundle != null)
		{
			BundleRevision bundleR = null;
			bundleR = (BundleRevision) super.getRegisteredBundle(bundle.getBundleId());
			if(bundleR == null)
			{
				bundleR = new BundleRevision(getClassLoader(), this,bundle);
				super.registerBundle(bundleR);
			}
			return bundleR;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * registerService(java.lang.String, java.lang.Object, java.util.Dictionary)
	 */
	public int registerService(String className, Object service,
			Dictionary<String, String> properties)
	{
		ServiceRegistration registration = super.bundleContext.registerService(
				className, service, properties);

		logger.log(Level.INFO,"Register '" + className + "' service : " + registration);

		int registrationId = registration.hashCode();
		registrations.put(new Integer(registrationId), registration);
		return registrationId;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * unregisterService(int)
	 */
	public void unregisterService(int registrationId)
	{
		ServiceRegistration registration = registrations.get(new Integer(
				registrationId));

		if (registration != null)
		{
			registration.unregister();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * getService(java.lang.String, java.lang.String)
	 */
	public Object getService(String className, String filter)
	{
		if(super.bundleContext == null)
		{
			return null;
		}
		ServiceReference reference = null;
		Object service = null;

		if (!validateFilter(filter))
		{
			reference = super.bundleContext.getServiceReference(className);
		} else
		{
			try
			{
				ServiceReference[] references = super.bundleContext
						.getServiceReferences(className, filter);

				if(references != null && references.length>0)
				{
					reference = references[0];
				}
			} catch (InvalidSyntaxException e)
			{
				e.printStackTrace();
			}
		}
		if (reference != null)
		{
			service = super.bundleContext.getService(reference);
		}
		return service;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * getServiceFromRegistration(int)
	 */
	public Object getService(int registrationId)
	{
		Object service = null;
		ServiceRegistration registration = registrations.get(new Integer(registrationId));
		if (registration != null)
		{
			ServiceReference reference = registration.getReference();
			service = super.bundleContext.getService(reference);
		}
		return service;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * getServiceBundle(java.lang.String, java.lang.String)
	 */
	public BundleRevisionItf<Bundle, BundleContext> getServiceBundle(
			String className, String filter)
	{
		ServiceReference reference = null;

		if (!validateFilter(filter))
		{
			reference = super.bundleContext.getServiceReference(className);
		} else
		{
			try
			{
				ServiceReference[] references = super.bundleContext
						.getServiceReferences(className, filter);
				reference = references[0];

			} catch (InvalidSyntaxException e)
			{
				e.printStackTrace();
			}
		}
		if (reference != null)
		{
			BundleRevision bundleR = null;
			bundleR = (BundleRevision) super.getRegisteredBundle(reference.getBundle().getBundleId());
			if(bundleR == null)
			{
				bundleR = new BundleRevision(getClassLoader(),this,reference.getBundle());
				super.registerBundle(bundleR);
			}
			return bundleR;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * getServices(java.lang.String, java.lang.String)
	 */
	public Object[] getServices(String className, String filter)
	{
		String validFilter = null;
		ServiceReference[] references = null;
		Object[] services = null;

		if (validateFilter(filter))
		{
			validFilter = filter;
		}
		try
		{
			references = super.bundleContext.getServiceReferences(
					className, validFilter);
		} catch (InvalidSyntaxException e)
		{
			e.printStackTrace();
		}
		if (references != null)
		{
			services = new Object[references.length];
			int n = 0;
			for (; n < references.length; n++)
			{
				services[n] = super.bundleContext.getService(references[n]);
			}
		}
		return services;
	}

	/**
	 * {@inheritDoc}
	 * @throws ClassNotFoundException
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * loadClass(java.lang.String)
	 */
	public Class<?> loadClass(String className) throws ClassNotFoundException
	{
		Class<?> clazz = this.classLoader.loadClass(className);
		return clazz;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * getResource(java.lang.String)
	 */
	public URL getResource(String resourceName)
	{
		URL resourceURL = this.classLoader.getResource(resourceName);
		return resourceURL;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * validateFilter(java.lang.String)
	 */
	public boolean validateFilter(String filter)
	{
		try
		{
			super.bundleContext.createFilter(filter);
			return true;

		} catch (InvalidSyntaxException e)
		{
			logger.log(Level.CONFIG,e.getMessage());

		} catch(NullPointerException e)
		{
			logger.log(Level.CONFIG,e.getMessage());
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * registerBundleListener(org.objectweb.fractal.juliac.osgi.revision.event.BundleListenerRevisionItf)
	 */
	public void registerBundleListener(BundleListenerRevisionItf<Bundle,BundleContext> bundleListener)
	{
		BundleListener listener = new BundleListenerRevision(this,bundleListener);
		super.bundleContext.addBundleListener(listener);
		this.bundleListeners.put(bundleListener, listener);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * unregisterBundleListener(org.objectweb.fractal.juliac.osgi.revision.event.BundleListenerRevisionItf)
	 */
	public void unregisterBundleListener(BundleListenerRevisionItf<Bundle,BundleContext> bundleListener)
	{
		BundleListener listener = bundleListeners.get(bundleListener);
		if(listener != null)
		{
			bundleContext.removeBundleListener(listener);
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * registerServiceListener(org.objectweb.fractal.juliac.osgi.revision.event.ServiceListenerRevisionItf)
	 */
	public void registerServiceListener(ServiceListenerRevisionItf serviceListener)
	{
		ServiceListener listener = new ServiceListenerRevision(serviceListener,this);
		super.bundleContext.addServiceListener(listener);
		this.serviceListeners.put(serviceListener, listener);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * registerServiceListener(org.objectweb.fractal.juliac.osgi.revision.event.ServiceListenerRevisionItf,
	 *  java.lang.String)
	 */
	public void registerServiceListener(
			ServiceListenerRevisionItf serviceListener, String filter)
	{
		ServiceListener listener = new ServiceListenerRevision(
				serviceListener,this);

		if(this.validateFilter(filter))
		{
			try
			{
				super.bundleContext.addServiceListener(listener,filter);
				this.serviceListeners.put(serviceListener, listener);

			} catch (InvalidSyntaxException e)
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * unregisterServiceListener(org.objectweb.fractal.juliac.osgi.revision.event.ServiceListenerRevisionItf)
	 */
	public void unregisterServiceListener(
			ServiceListenerRevisionItf serviceListener)
	{
		ServiceListener listener = serviceListeners.get(serviceListener);
		if(listener != null)
		{
			bundleContext.removeServiceListener(listener);
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * registerFrameworkListener(org.objectweb.fractal.juliac.osgi.revision.event.FrameworkListenerRevisionItf)
	 */
	public void registerFrameworkListener(
			FrameworkListenerRevisionItf frameworkListener)
	{
		FrameworkListener listener = new FrameworkListenerRevision(frameworkListener);
		super.bundleContext.addFrameworkListener(listener);
		this.frameworkListeners.put(frameworkListener, listener);

	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * unregisterFrameworkListener(org.objectweb.fractal.juliac.osgi.revision.event.FrameworkListenerRevisionItf)
	 */
	public void unregisterFrameworkListener(
			FrameworkListenerRevisionItf frameworkListener)
	{
		FrameworkListener listener = frameworkListeners.get(frameworkListener);
		if(listener != null)
		{
			bundleContext.removeFrameworkListener(listener);
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * getProperty(java.lang.String)
	 */
	public String getProperty(String key)
	{
		return bundleContext.getProperty(key);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#mapServiceEvent(int)
	 */
	public String mapServiceEvent(int serviceEventType)
	{
		String serviceEventName = null;

		switch(serviceEventType)
		{
			case ServiceEvent.MODIFIED: serviceEventName = "MODIFIED";
			break;
			case ServiceEvent.REGISTERED: serviceEventName = "REGISTERED";
			break;
			case ServiceEvent.UNREGISTERING: serviceEventName = "UNREGISTERING";
			break;
			default :
		}
		return serviceEventName;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * mapBundleEvent(int)
	 */
	public String mapBundleEvent(int bundleEventType)
	{
		String bundleEventName = null;

		switch(bundleEventType)
		{
			case BundleEvent.INSTALLED: bundleEventName = "INSTALLED";
			break;
			case BundleEvent.STARTED: bundleEventName = "STARTED";
			break;
			case BundleEvent.STOPPED: bundleEventName = "STOPPED";
			break;
			case BundleEvent.UNINSTALLED: bundleEventName = "UNINSTALLED";
			break;
			case BundleEvent.UPDATED : bundleEventName = "UPDATED";
			break;
			default :
		}
		return bundleEventName;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * mapFrameworkEvent(int)
	 */
	public String mapFrameworkEvent(int frameworkEventType)
	{
		String frameworkEventName = null;

		switch(frameworkEventType)
		{
			case FrameworkEvent.ERROR: frameworkEventName = "ERROR";
			break;
			case FrameworkEvent.PACKAGES_REFRESHED: frameworkEventName = "PACKAGES_REFRESHED";
			break;
			case FrameworkEvent.STARTED: frameworkEventName = "STARTED";
			break;
			case FrameworkEvent.STARTLEVEL_CHANGED: frameworkEventName = "STARTLEVEL_CHANGED";
			break;
			default :
		}
		return frameworkEventName;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf#
	 * mapBundleState(int)
	 */
	public String mapBundleState(int bundleState)
	{
		String bundleStateName = null;

		switch(bundleState)
		{
			case Bundle.ACTIVE: bundleStateName = "ACTIVE";
			break;
			case  Bundle.INSTALLED : bundleStateName = "INTALLED";
			break;
			case Bundle.RESOLVED: bundleStateName = "RESOLVED";
			break;
			case Bundle.STARTING: bundleStateName = "STARTING";
			break;
			case Bundle.STOPPING: bundleStateName = "STOPPING";
			break;
			case Bundle.UNINSTALLED: bundleStateName = "UNINSTALLED";
			break;
			default :
		}
		return bundleStateName;
	}

	/**
	 * @return the classLoader
	 */
	public BundleContextClassLoader getClassLoader()
	{
		return classLoader;
	}
}
