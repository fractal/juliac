/**
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */
package org.objectweb.fractal.juliac.osgi.revision;

import java.io.InputStream;
import java.net.URL;
import java.util.Dictionary;
import java.util.Enumeration;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextClassLoader;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.ServiceReference;

/**
 * BundleRevisionItf implementation for an OSGi-R3 environment
 */
public class BundleRevision extends AbstractBundle<Bundle,BundleContext>
{
   private BundleContextClassLoader classLoader;

   /**
	 * @param classLoader
	 * @param bundleContextRevision
	 * @param bundle
	 */
	public BundleRevision(BundleContextClassLoader classLoader,
			BundleContextRevision bundleContextRevision, Bundle bundle)
	{
		super(bundleContextRevision, bundle);
		this.classLoader = classLoader;
		this.classLoader.registerBundle(bundle);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getBundle()
	 */
	public Bundle getBundle()
	{
		return super.bundle;
	}

   /**
	* {@inheritDoc}
	*
	* @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getLocation()
	*/
   public String getLocation()
   {
	   String location = null;

	   if (super.bundle != null)
	   {
		   location = super.bundle.getLocation();
	   }
	   return location;
   }

   /**
	* {@inheritDoc}
	*
	* @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getHeaders()
	*/
   @SuppressWarnings("unchecked")
   public Dictionary<String, String> getHeaders()
   {
	   Dictionary<String, String> headers = null;

	   if (super.bundle != null)
	   {
		   headers = super.bundle.getHeaders();
	   }
	   return headers;
   }

   /**
	* {@inheritDoc}
	*
	* @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#start()
	*/
   public void start() throws OSGiRevisionException
   {
	   if (super.bundle != null)
	   {
		   try
		   {
			   super.bundle.start();
		   } catch (BundleException e)
		   {
			   e.printStackTrace();
			   throw new OSGiRevisionException(e.getMessage());
		   }
	   }
   }

   /**
	* {@inheritDoc}
	*
	* @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#stop()
	*/
   public void stop() throws OSGiRevisionException
   {
	   if (super.bundle != null)
	   {
		   try
		   {
			   super.bundle.stop();
		   } catch (BundleException e)
		   {
			   e.printStackTrace();
			   throw new OSGiRevisionException(e.getMessage());
		   }
	   }
   }

   /**
	* {@inheritDoc}
	*
	* @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#update()
	*/
   public void update() throws OSGiRevisionException
   {
	   if (super.bundle != null)
	   {
		   try
		   {
			   super.bundle.update();
			   this.classLoader.updateBundle(super.bundle);

		   } catch (BundleException e)
		   {
			   e.printStackTrace();
			   throw new OSGiRevisionException(e.getMessage());
		   }
	   }
   }

   /**
	* {@inheritDoc}
	*
	* @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#update(java.io.InputStream)
	*/
   public void update(InputStream stream) throws OSGiRevisionException
   {
	   if (super.bundle != null)
	   {
		   try
		   {
			   super.bundle.update(stream);
			   this.classLoader.updateBundle(super.bundle);

		   } catch (BundleException e)
		   {
			   e.printStackTrace();
			   throw new OSGiRevisionException(e.getMessage());
		   }
	   }
   }

   /**
	* {@inheritDoc}
	*
	* @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#uninstall()
	*/
   public void uninstall() throws OSGiRevisionException
   {
	   if (super.bundle != null)
	   {
		   try
		   {
			   this.classLoader.uninstallBundle(super.bundle.getBundleId());
			   super.bundle.uninstall();

		   } catch (BundleException e)
		   {
			   e.printStackTrace();
			   throw new OSGiRevisionException(e.getMessage());
		   }
	   }
   }

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf#getBundleSymbolicName
	 *      (long)
	 */
	public String getSymbolicName()
	{
		String symbolicName = null;
		if (super.bundle != null)
		{
			symbolicName = getHeaders().get("Bundle-SymbolicName") ;
		}
		return symbolicName;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf#getServiceFromRegistration(long)
	 */
	public Object getServiceObject()
	{
		Object service = null;

		if (super.bundle != null)
		{
			ServiceReference[] srs = bundle.getRegisteredServices();
			if (srs != null && srs.length > 0)
			{
				ServiceReference sr = srs[0];
				service = super.context.getBundleContext().getService(sr);
			}
		}
		return service;
	}


	/**
	 * {@inheritDoc}
	 *
	 * @see
	 * org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#loadClass(java
	 * .lang.String)
	 */
	public Class<?> loadClass(String className) throws ClassNotFoundException
	{
		Class<?> clazz = null;
		if(super.bundle != null && classLoader != null)
		{
			  clazz = classLoader.loadClass(className,bundle.getBundleId());
		}
		return clazz;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see
	 * org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getResource
	 * (java.lang.String)
	 */
	public URL getResource(String resourceName)
	{
		URL res = null;
		if(super.bundle != null)
		{
		  res = bundle.getResource(resourceName);
		}
		return res;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getEntryPath(java.lang.String)
	 */
	public Enumeration<String> getEntryPaths(String path)
	{
		if(super.bundle != null && classLoader != null)
		{
			 return classLoader.getEntryPaths(path,bundle.getBundleId());
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getEntry(java.lang.String)
	 */
	public URL getEntry(String path)
	{
		URL res = null;
		if(super.bundle != null && classLoader != null)
		{
			 res = classLoader.getEntry(path,bundle.getBundleId());
		}
		return res;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getBundleId()
	 */
	public long getBundleId()
	{
		long identifier = -1;

		if(super.bundle != null)
		{
			identifier = bundle.getBundleId();
		}
		return identifier;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getState()
	 */
	public int getState()
	{
		int bundleState = -1;

		if(super.bundle != null)
		{
			bundleState = super.bundle.getState();
		}
		return bundleState;
	}
}
