/**
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */
package org.objectweb.fractal.juliac.osgi.revision;

import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextClassLoader;
import org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

/**
 * OSGiRevisionItf implementation for an OSGi-R3 environment
 */
public class OSGiRevision extends AbstractOSGi<Bundle,BundleContext>
{

	//the canonical class name of the associated BundleContextClassLoader implementation
	public static final String BUNDLECONTEXT_CLASSLOADER_IMPLEMENTATION =
		"org.objectweb.fractal.juliac.osgi.revision.BundleContextClassLoaderImpl";

	protected BundleContextClassLoader classLoader;
	protected BundleContext systemBundleContext;

	/**
	 * Constructor
	 *
	 * @param systemBundleContext
	 */
	public OSGiRevision(BundleContext systemBundleContext)
	{
		this.systemBundleContext = systemBundleContext;
		try
		{
			Class<?> bundleContextClassLoaderImplClass = getClass().getClassLoader().loadClass(
					BUNDLECONTEXT_CLASSLOADER_IMPLEMENTATION);

			this.classLoader = (BundleContextClassLoader)
				bundleContextClassLoaderImplClass.getDeclaredMethod("getBundleContextClassLoader",
					new Class<?>[]{ClassLoader.class}).invoke(null,
							new Object[]{ Thread.currentThread().getContextClassLoader() });

		} catch (ClassNotFoundException e)
		{
			LOGGER.log(Level.WARNING,e.getMessage(),e);

		} catch (IllegalArgumentException e)
		{
			LOGGER.log(Level.WARNING,e.getMessage(),e);

		} catch (SecurityException e)
		{
			LOGGER.log(Level.WARNING,e.getMessage(),e);

		} catch (IllegalAccessException e)
		{
			LOGGER.log(Level.WARNING,e.getMessage(),e);

		} catch (InvocationTargetException e)
		{
			LOGGER.log(Level.WARNING,e.getMessage(),e);

		} catch (NoSuchMethodException e)
		{
			LOGGER.log(Level.WARNING,e.getMessage(),e);
		}
		System.setProperty(OSGiRevisionItf.OSGi_REVISION_ID, "osgi-r3");
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.AbstractOSGi#createContextRevision(org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf, java.lang.Object)
	 */
	@Override
	protected AbstractBundleContext<Bundle, BundleContext> createContextRevision()
	{
		return new BundleContextRevision(this,systemBundleContext,classLoader);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf#
	 * getFrameworkClassLoader()
	 */
	public ClassLoader getFrameworkClassLoader()
	{
		//return (ClassLoader) this.classLoader;
		return Bundle.class.getClassLoader();
	}
}
