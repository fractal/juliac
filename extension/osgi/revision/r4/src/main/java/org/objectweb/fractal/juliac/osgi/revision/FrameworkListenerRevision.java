/**
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */
package org.objectweb.fractal.juliac.osgi.revision;

import org.objectweb.fractal.juliac.osgi.revision.event.FrameworkListenerRevisionItf;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.FrameworkListener;

/**
 * FrameworkListener implementation for an OSGi-R4 environment
 */
public class FrameworkListenerRevision implements FrameworkListener
{
	public static final int ERROR = FrameworkEvent.ERROR;
	public static final int INFO = FrameworkEvent.INFO;
	public static final int PACKAGES_REFRESHED = FrameworkEvent.PACKAGES_REFRESHED;
	public static final int STARTED = FrameworkEvent.STARTED;
	public static final int STARTLEVEL_CHANGED = FrameworkEvent.STARTLEVEL_CHANGED;
	public static final int STOPPED = FrameworkEvent.STOPPED;
	public static final int STOPPED_BOOTCLASSPATH_MODIFIED = FrameworkEvent.STOPPED_BOOTCLASSPATH_MODIFIED;
	public static final int STOPPED_UPDATE = FrameworkEvent.STOPPED_UPDATE;
	public static final int WAIT_TIMEDOUT = FrameworkEvent.WAIT_TIMEDOUT;
	public static final int WARNING = FrameworkEvent.WARNING;

	private FrameworkListenerRevisionItf frameworkListener;

	/**
	 * @param frameworkListener
	 */
	public FrameworkListenerRevision(FrameworkListenerRevisionItf frameworkListener)
	{
		this.frameworkListener = frameworkListener;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.osgi.framework.FrameworkListener#
	 * frameworkEvent(org.osgi.framework.FrameworkEvent)
	 */
	public void frameworkEvent(FrameworkEvent event)
	{
		this.frameworkListener.frameworkEvent(event.getType());
	}
}
