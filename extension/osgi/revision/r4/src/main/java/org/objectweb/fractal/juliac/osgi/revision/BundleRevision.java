/**
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */
package org.objectweb.fractal.juliac.osgi.revision;

import java.io.InputStream;
import java.net.URL;
import java.util.Dictionary;
import java.util.Enumeration;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.ServiceReference;

/**
 * BundleRevisionItf implementation for an OSGi-R4 environment
 */
public class BundleRevision extends AbstractBundle<Bundle,BundleContext>
{
	/**
	 * @param context
	 * @param bundle
	 */
	protected BundleRevision(
			AbstractBundleContext<Bundle, BundleContext> context, Bundle bundle)
	{
		super(context, bundle);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getBundle()
	 */
	public Bundle getBundle()
	{
		return super.bundle;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getSymbolicName()
	 */
	public String getSymbolicName()
	{
		String symbolicName = null;
		if (super.bundle != null)
		{
			symbolicName = super.bundle.getSymbolicName();
		}
		return symbolicName;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getLocation()
	 */
	public String getLocation()
	{
		String location = null;

		if (super.bundle != null)
		{
			location = super.bundle.getLocation();
		}
		return location;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getHeaders()
	 */
	@SuppressWarnings("unchecked")
	public Dictionary<String, String> getHeaders()
	{
		Dictionary<String, String> headers = null;

		if (super.bundle != null)
		{
			headers = super.bundle.getHeaders();
		}
		return headers;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#start()
	 */
	public void start() throws OSGiRevisionException
	{
		if (super.bundle != null)
		{
			try
			{
				super.bundle.start();
			} catch (BundleException e)
			{
				e.printStackTrace();
				throw new OSGiRevisionException(e.getMessage());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#stop()
	 */
	public void stop() throws OSGiRevisionException
	{
		if (super.bundle != null)
		{
			try
			{
				super.bundle.stop();

			} catch (BundleException e)
			{
				e.printStackTrace();
				throw new OSGiRevisionException(e.getMessage());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#update()
	 */
	public void update() throws OSGiRevisionException
	{
		if (super.bundle != null)
		{
			try
			{
				super.bundle.update();
			} catch (BundleException e)
			{
				e.printStackTrace();
				throw new OSGiRevisionException(e.getMessage());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#update(java.io.InputStream)
	 */
	public void update(InputStream stream) throws OSGiRevisionException
	{
		if (super.bundle != null)
		{
			try
			{
				super.bundle.update(stream);
			} catch (BundleException e)
			{
				e.printStackTrace();
				throw new OSGiRevisionException(e.getMessage());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#uninstall()
	 */
	public void uninstall() throws OSGiRevisionException
	{
		if (super.bundle != null)
		{
			try
			{
				super.bundle.uninstall();

			} catch (BundleException e)
			{
				e.printStackTrace();
				throw new OSGiRevisionException(e.getMessage());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getServiceObject()
	 */
	public Object getServiceObject()
	{
		Object service = null;

		if (super.bundle != null)
		{
			ServiceReference[] srs = super.bundle.getRegisteredServices();
			if (srs != null && srs.length > 0)
			{
				ServiceReference sr = srs[0];
				service = super.bundle.getBundleContext().getService(sr);
			}
		}
		return service;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#loadClass(java.lang.String)
	 */
	public Class<?> loadClass(String className) throws ClassNotFoundException
	{
		Class<?> clazz = null;
		if(super.bundle != null)
		{
		  clazz = super.bundle.loadClass(className);
		}
		return clazz;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getEntryPath(java.lang.String)
	 */
	public Enumeration<String> getEntryPaths(String path)
	{
		if (super.bundle != null)
		{
			return bundle.getEntryPaths(path);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getEntry(java.lang.String)
	 */
	public URL getEntry(String path)
	{
		if (super.bundle != null)
		{
			return bundle.getEntry(path);
		}
		return null;

	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getResource(java.lang.String)
	 */
	public URL getResource(String resourceName)
	{
		URL res = null;
		if(super.bundle != null)
		{
		  res = super.bundle.getResource(resourceName);
		}
		return res;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getBundleId()
	 */
	public long getBundleId()
	{
		long identifier = -1;

		if(super.bundle != null)
		{
			identifier = bundle.getBundleId();
		}
		return identifier;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf#getState()
	 */
	public int getState()
	{
		int bundleState = -1;

		if(super.bundle != null)
		{
			bundleState = super.bundle.getState();
		}
		return bundleState;
	}
}
