/**
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */
package org.objectweb.fractal.juliac.osgi.revision;

import org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

/**
 * OSGiRevisionItf implementation for an OSGi-R4 environment
 */
public class OSGiRevision extends AbstractOSGi<Bundle,BundleContext>
{

	/**
	 * The BundleContext associated to the PlatformItf instance
	 * wich has created this OSGiRevisionItf instance
	 *
	 */
	private BundleContext systemBundleContext;

	private ClassLoader classLoader;

	/**
	 * Constructor
	 *
	 * @param systemBundleContext
	 */
	public OSGiRevision(BundleContext systemBundleContext)
	{
		this.systemBundleContext = systemBundleContext;
		System.setProperty(OSGiRevisionItf.OSGi_REVISION_ID, "osgi-r4");

		this.classLoader = Bundle.class.getClassLoader();
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.AbstractOSGi#
	 * createContextRevision(org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf, java.lang.Object)
	 */
	@Override
	protected AbstractBundleContext<Bundle, BundleContext> createContextRevision()
	{
		return new BundleContextRevision(this,this.systemBundleContext);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf#getFrameworkClassLoader()
	 */
	public ClassLoader getFrameworkClassLoader()
	{
		return classLoader;
	}
}
