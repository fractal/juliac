/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.osgi;

import org.eclipse.core.runtime.adaptor.LocationManager;
import org.eclipse.osgi.baseadaptor.BaseAdaptor;
import org.eclipse.osgi.framework.adaptor.FrameworkAdaptor;
import org.eclipse.osgi.framework.internal.core.Framework;
import org.eclipse.osgi.framework.internal.core.FrameworkProperties;
import org.objectweb.fractal.juliac.osgi.revision.OSGiRevision;
import org.objectweb.fractal.juliac.osgi.revision.OSGiRevisionException;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.startlevel.StartLevel;

/**
 * Helper methods for dealing with OSGi/Felix.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2.3
 */
public class PlatformImpl implements PlatformItf {

	private OSGiRevisionItf<Bundle,BundleContext> r4Revision = null;

	public BundleContextRevisionItf<?,?> getBundleContext()
	throws OSGiRevisionException {

		if( r4Revision == null ) {
			Framework equinox = getEquinox();
			BundleContext systemBundleContext = equinox.getSystemBundleContext();
			r4Revision = new OSGiRevision(systemBundleContext);
		}

		BundleContextRevisionItf<?,?> bundleContextRevision =
			r4Revision.getBundleContext();
		return bundleContextRevision;
	}

	public void stop() {
		Framework equinox = getEquinox();
		equinox.shutdown(0);
	}


	// ---------------------------------------------------------------------
	// Implementation specific
	// ---------------------------------------------------------------------

	/** The instance of the OSGi/Equinox framework. */
	private static Framework equinox = null;

	/**
	 * Return the singleton instance of the OSGi/Equinox framework.
	 */
	private static Framework getEquinox()
	{
		if (equinox == null)
		{
			FrameworkProperties.initializeProperties();
			LocationManager.initializeLocations();
			FrameworkAdaptor adaptor = new BaseAdaptor(new String[0]);
			equinox = new Framework(adaptor);
			equinox.launch();

			BundleContext bci = equinox.getSystemBundleContext();
			ServiceReference reference =
				bci.getServiceReference(StartLevel.class.getName());

			StartLevel startService = (StartLevel) bci.getService(reference);
			startService.setStartLevel(4);
		}

		return equinox;
	}
}
