/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Christophe Munilla
 */

package org.objectweb.fractal.juliac.osgi;

import java.io.InputStream;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;
import org.objectweb.fractal.juliac.osgi.revision.OSGiRevisionException;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;
import org.objectweb.fractal.juliac.runtime.RuntimeException;

/**
 * Helper methods for dealing with OSGi.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Christophe Munilla
 * @since 2.1.5
 */
public class OSGiHelper {

	/** The name of the platform implementation. */
	public static final String PLATFORM_IMPL_CLASS_NAME =
		"org.objectweb.fractal.juliac.osgi.PlatformImpl";

	/** The platform instance. */
	private static PlatformItf pf;

	/**
	 * Return a reference to the OSGi platform.
	 *
	 * @since 2.2.3
	 */
	public static PlatformItf getPlatform() {

		if( pf == null ) {
			try {
				Class cl = ReflectionHelper.loadClass(PLATFORM_IMPL_CLASS_NAME);
				pf = (PlatformItf) cl.newInstance();

			}
			catch ( Exception e ) {
				throw new RuntimeException(e);
			}
		}

		return pf;
	}

	/**
	 * Install the bundle referenced by the specified bundle jar file name.
	 *
	 * @param bundleJar  the jar file name
	 * @param loader
	 *         the class loader where the bundle jar file can be loaded as a
	 *      resource stream
	 * @return  the bundle
	 */
	public static BundleRevisionItf installBundle(
		String bundleJar, ClassLoader loader )
	throws OSGiRevisionException {

		InputStream is = loader.getResourceAsStream(bundleJar);
		PlatformItf platform = getPlatform();
		BundleContextRevisionItf bundleContext = platform.getBundleContext();
		BundleRevisionItf bundle = bundleContext.installBundle(bundleJar,is);
		return bundle;
	}

	/**
	 * Return the service object exported by the specified bundle.
	 *
	 * @param bundle  the bundle
	 * @return        the corresponding service object
	 */
	public static Object getServiceObject( BundleRevisionItf bundle ) {
		return bundle.getServiceObject();
	}

	/**
	 * Install the bundle referenced by the specified bundle jar file name.
	 *
	 * @param bundleJar  the jar file name
	 * @param loader
	 *         the class loader where the bundle jar file can be loaded as a
	 *      resource stream
	 * @return   the bundle
	 */
	public static BundleRevisionItf installBundleIE(
		String bundleJar, ClassLoader loader )
	throws InstantiationException {

		try {
			return installBundle( bundleJar, loader );
		}
		catch( OSGiRevisionException e ) {
			throw new ChainedInstantiationException(e, null, e.getMessage());
		}
	}

	/**
	 * Start the specified bundle.
	 */
	public static void startBundleIE( BundleRevisionItf bundle )
	throws InstantiationException {

		try {
			bundle.start();
		}
		catch( OSGiRevisionException e ) {
			throw new ChainedInstantiationException(e,null,e.getMessage());
		}
	}

	/**
	 * Return the service object exported by the specified bundle.
	 *
	 * @param bundle  the bundle
	 * @return        the corresponding service object
	 */
	public static Object getServiceObjectIE( BundleRevisionItf bundle ) {
		return getServiceObject(bundle);
	}
}
