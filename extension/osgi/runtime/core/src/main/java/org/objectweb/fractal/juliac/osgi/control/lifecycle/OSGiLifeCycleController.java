/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Christophe Munilla
 */

package org.objectweb.fractal.juliac.osgi.control.lifecycle;

import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.osgi.revision.OSGiRevisionException;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;

/**
 * Lifecycle control interface for OSGi primitive components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Christophe Munilla
 * @since 2.1.5
 */
public interface OSGiLifeCycleController extends LifeCycleController {

	/** <code>NAME</code> of the lifecycle controller. */
	public static final String NAME = "lifecycle-controller";

	/** <code>TYPE</code> of the lifecycle controller. */
	public static final InterfaceType TYPE =
		new BasicInterfaceType(
			NAME,
			OSGiLifeCycleController.class.getName(),
			false, false, false );

	/**
	 * Install the bundle associated with the current component.
	 *
	 * @return  the installed bunded
	 * @throws OSGiRevisionException  in case of problem during the installation
	 */
	public BundleRevisionItf installFcBundle() throws OSGiRevisionException;

	/**
	 * Uninstall the bundle associated with the current component.
	 *
	 * @throws OSGiRevisionException
	 *         in case of problem during the uninstallation
	 */
	public void uninstallFcBundle() throws OSGiRevisionException;
}
