/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Christophe Munilla
 */

package org.objectweb.fractal.juliac.osgi;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;
import org.objectweb.fractal.juliac.runtime.ClassLoaderFcItf;
import org.objectweb.fractal.juliac.runtime.ClassLoaderItf;
import org.objectweb.fractal.juliac.runtime.Juliac;
import org.objectweb.fractal.juliac.runtime.RuntimeException;

/**
 * This class provides some helper methods for dealing with Java reflection in
 * the context of the OSGi extension of Juliac.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Christophe Munilla
 * @since 2.1.6
 */
public class ReflectionHelper {

	/**
	 * Method for dynamically invoking the
	 * <code>setFcItfDelegate(Object delegate)</code> method on the specified
	 * object.
	 *
	 * @param o         the proxy object
	 * @param delegate  the value of the <code>delegate</code> parameter when
	 *                  invoking <code>setFcItfDelegate</code>
	 * @throws InstantiationException
	 *      wraps any exception which may have been thrown during the dynamic
	 *      invocation
	 */
	public static void initBundledProxy( Object o, Object delegate )
	throws InstantiationException {

		Class cl = o.getClass();

		try {
			// Invoke setFcItfDelegate
			Method m = cl.getDeclaredMethod("setFcItfDelegate",new Class[]{Object.class});
			m.invoke(o,new Object[]{delegate});
		}
		catch (NoSuchMethodException e) {
			throw new ChainedInstantiationException(e,null,e.getMessage());
		}
		catch (IllegalAccessException e) {
			throw new ChainedInstantiationException(e,null,e.getMessage());
		}
		catch (InvocationTargetException e) {
			throw new ChainedInstantiationException(e,null,e.getMessage());
		}
	}

	/**
	 * Load the class whose name is specified with the classloader interface
	 * provided by the Juliac bootstrap component.
	 *
	 * @throws ClassNotFoundException  if the class can not be loaded
	 */
	public static Class loadClass( String name )
	throws ClassNotFoundException {

		ClassLoaderItf cl = null;
		try {
			Component boot = new Juliac().newFcInstance();
			cl = (ClassLoaderItf) boot.getFcInterface(ClassLoaderFcItf.NAME);
		}
		catch( NoSuchInterfaceException nsie ) {
			/*
			 * Shouldn't occur since Juliac provides a classloader interface.
			 */
			throw new RuntimeException(nsie);
		}

		Class c = cl.loadClass(name);
		return c;
	}
}
