/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.osgi;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.felix.framework.Felix;
import org.apache.felix.framework.cache.BundleCache;
import org.apache.felix.framework.util.FelixConstants;
import org.apache.felix.framework.util.StringMap;
import org.objectweb.fractal.juliac.osgi.revision.OSGiRevision;
import org.objectweb.fractal.juliac.osgi.revision.OSGiRevisionException;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;

/**
 * Helper methods for dealing with OSGi/Felix.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2.3
 */
public class PlatformImpl implements PlatformItf
{

	private OSGiRevisionItf<Bundle,BundleContext> r4Revision = null;

	/**
	 * Return the OSGiRevisionItf associated to the platform
	 */
	@Override
	public BundleContextRevisionItf<?,?> getBundleContext() throws OSGiRevisionException
	{
		if (r4Revision == null)
		{
			try
			{
				r4Revision = new OSGiRevision(getFelix().getBundleContext());

			} catch (IOException|BundleException e)
			{
				e.printStackTrace();
			}
		}
		return r4Revision.getBundleContext();
	}

	/**
	 * Stop the platform.
	 */
	@Override
	public void stop() throws OSGiRevisionException
	{
		Felix felix;
		try
		{
			felix = getFelix();
			felix.stop();

		} catch (IOException e)
		{
			e.printStackTrace();
			throw new OSGiRevisionException(e.getMessage());

		} catch (BundleException e)
		{
			e.printStackTrace();
			throw new OSGiRevisionException(e.getMessage());
		}
	}

	// ---------------------------------------------------------------------
	// Implementation specific
	// ---------------------------------------------------------------------

	/** The instance of the OSGi/Felix framework. */
	private static Felix felix = null;

	/**
	 * Return the singleton instance of the OSGi/Felix framework.
	 */
	@SuppressWarnings("unchecked")
	private static Felix getFelix() throws IOException, BundleException
	{
		if (felix == null)
		{
			/*
			 * Prepare a cache directory for Felix.
			 */
			File tmpFile = File.createTempFile("felix", ".tmp");
			tmpFile.delete();
			// Retrieve the directory containing temporary files
			File dir = tmpFile.getParentFile();
			File cache = new File(dir, "felix");
			// Delete files which may have been stored by a previous run
			deleteAllRecursively(cache);
			cache.mkdir();

			String systemPackages =  System.getProperty("org.osgi.framework.system.packages",
									"org.osgi.framework; version=1.4.0,"
									+ "org.osgi.service.packageadmin; version=1.2.0,"
									+ "org.osgi.service.startlevel; version=1.1.0,"
									+ "org.osgi.service.url; version=1.0.0");
			/*
			 * Properties for configuring Felix.
			 */
			Map<String,String> configMap = new StringMap(false);
			configMap.put(Constants.FRAMEWORK_SYSTEMPACKAGES,systemPackages);
			configMap.put(BundleCache.CACHE_PROFILE_DIR_PROP,
					cache.getAbsolutePath());
			configMap.put(FelixConstants.EMBEDDED_EXECUTION_PROP, "true");

			/*
			 * Start Felix.
			 */
			felix = new Felix(configMap, null);
			felix.start();
		}

		return felix;
	}

	/**
	 * Delete recursively all the files contained in the specified file if the
	 * specified file is a directory, and the specified file itself.
	 *
	 * @param f
	 *            the file to delete
	 */
	private static void deleteAllRecursively(File f)
	{

		if (f.isDirectory())
		{
			File[] subs = f.listFiles();
			for (int i = 0; i < subs.length; i++)
			{
				deleteAllRecursively(subs[i]);
			}
		}
		f.delete();
	}
}
