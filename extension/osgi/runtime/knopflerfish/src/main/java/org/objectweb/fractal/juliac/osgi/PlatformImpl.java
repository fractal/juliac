/***
 * Juliac
 * Copyright (C) 2011-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */

package org.objectweb.fractal.juliac.osgi;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.knopflerfish.framework.FrameworkFactoryImpl;
import org.objectweb.fractal.juliac.osgi.revision.OSGiRevision;
import org.objectweb.fractal.juliac.osgi.revision.OSGiRevisionException;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.launch.Framework;
import org.osgi.framework.launch.FrameworkFactory;

/**
 * Helper methods for dealing with OSGi/Knopflerfish.
 *
 * @author Christophe Munilla <christophe.munilla@inria.fr>
 * @since 2.4
 */
public class PlatformImpl implements PlatformItf
{

	private OSGiRevisionItf<Bundle,BundleContext> r4Revision = null;

	/**
	 * Return the OSGiRevisionItf associated to the platform
	 */
	@Override
	public BundleContextRevisionItf<?,?> getBundleContext() throws OSGiRevisionException
	{
		if (r4Revision == null)
		{
			try
			{
				r4Revision = new OSGiRevision(getKnopflerfish(
						).getBundleContext());

			} catch (IOException|BundleException e)
			{
				e.printStackTrace();
			}
		}
		return r4Revision.getBundleContext();
	}

	/**
	 * Stop the platform.
	 */
	@Override
	public void stop() throws OSGiRevisionException
	{
		Framework knopflerfish;
		try
		{
			knopflerfish = getKnopflerfish();
			knopflerfish.stop(0);

		} catch (IOException|BundleException e)
		{
			e.printStackTrace();
			throw new OSGiRevisionException(e.getMessage());
		}
	}

	// ---------------------------------------------------------------------
	// Implementation specific
	// ---------------------------------------------------------------------

	/** The instance of the OSGi/Knopflerfish framework. */
	private static Framework knopflerfish = null;

	/**
	 * Return the singleton instance of the OSGi/Knopflerfish framework.
	 */
	private static Framework getKnopflerfish() throws IOException,
			BundleException
	{

		Map<String, String> fwProps= new HashMap<>();
		/* Properties for configuring Knopflerfish. */
		if (knopflerfish == null)
		{
			/* Prepare a cache directory for Knopflerfish. */
			File tmpFile = null;
			try
			{
				tmpFile = File.createTempFile("knopflerfish", ".tmp");
			} catch (IOException e)
			{
				e.printStackTrace();
			}
			tmpFile.delete();
			// Retrieve the directory containing temporary files
			File dir = tmpFile.getParentFile();
			File cache = new File(dir, "knopflerfish");
			// Delete files which may have been stored by a previous run
			deleteAllRecursively(cache);
			cache.mkdir();

			String systemPackages =  System.getProperty("org.osgi.framework.system.packages","");

			fwProps.put("org.osgi.provisioning.spid", "knopflerfish");
			fwProps.put("org.osgi.framework.storage", cache.getAbsolutePath());
			fwProps.put("org.knopflerfish.osgi.setcontextclassloader", "false");
			fwProps.put("org.knopflerfish.osgi.registerserviceurlhandler",
					"true");
			fwProps.put("org.knopflerfish.framework.debug.classloader", "false");
			fwProps.put("org.knopflerfish.framework.debug.automanifest",
					"false");
			fwProps.put("org.knopflerfish.framework.debug.packages", "false");
			fwProps.put("org.knopflerfish.framework.debug.patch", "false");
			fwProps.put("org.knopflerfish.framework.debug.lazy_activation",
					"false");
			fwProps.put("org.knopflerfish.framework.debug.permissions", "false");
			fwProps.put("org.knopflerfish.framework.debug.certificates",
					"false");
			fwProps.put("org.knopflerfish.framework.debug.startlevel", "true");
			fwProps.put("org.knopflerfish.framework.debug.framework", "true");
			fwProps.put("org.knopflerfish.framework.debug.service_reference",
					"false");
			fwProps.put("org.knopflerfish.framework.debug.bundle_resource",
					"true");
			;
			fwProps.put("org.knopflerfish.framework.debug.url", "false");
			fwProps.put("org.knopflerfish.framework.debug.hooks", "false");
			fwProps.put("org.knopflerfish.framework.debug.errors", "true");
			fwProps.put("org.knopflerfish.log.memory.size", "250");
			fwProps.put("org.knopflerfish.log.level", "debug");
			fwProps.put("org.knopflerfish.log.grabio", "false");
			fwProps.put("org.knopflerfish.log.file", "false");
			fwProps.put("org.knopflerfish.log.out", "true");
			fwProps.put("org.knopflerfish.gosg.jars", "");
			fwProps.put("org.knopflerfish.consoletelnet.port", "2323");
			fwProps.put("org.knopflerfish.consoletelnet.user", "admin");
			fwProps.put("org.knopflerfish.consoletelnet.pwd", "admin");
			fwProps.put("org.knopflerfish.prodver", "5.1.6");
			fwProps.put("org.knopflerfish.bundle.cm.store", "cmdir");
			fwProps.put("org.knopflerfish.http.dnslookup", "false");
			fwProps.put("org.osgi.service.http.port", "8080");

			fwProps.put("org.knopflerfish.framework.system.packages.base",systemPackages);
			fwProps.put("org.knopflerfish.framework.system.packages.file", "");

			String[] versions = System.getProperty("java.version").split("\\.");
			String version = versions[0] + "." + versions[1];
			fwProps.put("org.knopflerfish.framework.system.packages.version",
					version);
			fwProps.put("org.knopflerfish.framework.bundlestorage", "file");
			fwProps.put("org.knopflerfish.framework.bundlestorage.checksigned",
					"true");
			fwProps.put(
					"org.knopflerfish.framework.bundlestorage.file.reference",
					"true");
			fwProps.put("org.knopflerfish.framework.bundlestorage.file.unpack",
					"true");
			fwProps.put(
					"org.knopflerfish.framework.bundlestorage.file.trusted",
					"true");
			fwProps.put(
					"org.knopflerfish.framework.bundlestorage.file.always_unpack",
					"false");
			fwProps.put("org.knopflerfish.framework.startlevel.compat", "false");
			fwProps.put("org.knopflerfish.framework.patch.configurl",
					"!!/patches.props");
			fwProps.put("org.knopflerfish.framework.automanifest.config",
					"!!/automanifest.props");
			fwProps.put(
					"org.knopflerfish.framework.is_doublechecked_locking_safe",
					"true");
			fwProps.put("org.knopflerfish.framework.strictbootclassloading",
					"false");
			fwProps.put("org.knopflerfish.framework.all_signed", "false");
			fwProps.put("org.knopflerfish.framework.automanifest", "false");
			fwProps.put("org.knopflerfish.framework.main.class.activation", "");
			fwProps.put("org.knopflerfish.framework.patch", "false");
			fwProps.put("org.knopflerfish.framework.ldap.nocache", "false");
			fwProps.put("org.knopflerfish.framework.service.permissionadmin",
					"true");
			fwProps.put(
					"org.knopflerfish.framework.service.conditionalpermissionadmin",
					"true");
			fwProps.put("org.knopflerfish.framework.patch.dumpclasses.dir",
					"patchedclasses");
			fwProps.put("org.knopflerfish.framework.patch.dumpclasses", "false");
			fwProps.put("org.knopflerfish.startlevel.use", "true");
			fwProps.put("org.knopflerfish.framework.validator", "none");

			FrameworkFactory factory = new FrameworkFactoryImpl();
			knopflerfish = factory.newFramework(fwProps);

			try
			{
				knopflerfish.start();
			} catch (BundleException e)
			{
				e.printStackTrace();
			}
		}
		return knopflerfish;
	}

	/**
	 * Delete recursively all the files contained in the specified file if the
	 * specified file is a directory, and the specified file itself.
	 *
	 * @param f
	 *            the file to delete
	 */
	private static void deleteAllRecursively(File f)
	{

		if (f.isDirectory())
		{
			File[] subs = f.listFiles();
			for (int i = 0; i < subs.length; i++)
			{
				deleteAllRecursively(subs[i]);
			}
		}
		f.delete();
	}
}
