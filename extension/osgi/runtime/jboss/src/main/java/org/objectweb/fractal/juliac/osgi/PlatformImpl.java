/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Munilla
 */

package org.objectweb.fractal.juliac.osgi;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jboss.net.protocol.URLStreamHandlerFactory;
import org.jboss.osgi.spi.framework.OSGiBootstrap;
import org.jboss.osgi.spi.framework.OSGiBootstrapProvider;
import org.objectweb.fractal.juliac.osgi.revision.OSGiRevision;
import org.objectweb.fractal.juliac.osgi.revision.OSGiRevisionException;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.launch.Framework;

/**
 * Helper methods for dealing with the OSGi JBoss Framework.
 *
 * @author Christophe Munilla <Christophe.Munilla@inria.fr>
 * @since 2.5
 */
public class PlatformImpl implements PlatformItf
{

	private OSGiRevisionItf<Bundle,BundleContext> r4Revision = null;

	/**
	 * Return the OSGiRevisionItf associated to the platform
	 */
	public BundleContextRevisionItf<?,?> getBundleContext() throws OSGiRevisionException
	{
		if (r4Revision == null)
		{
			try
			{
				r4Revision = new OSGiRevision(getJBoss().getBundleContext());

			} catch (IOException|BundleException e)
			{
				LOGGER.log(Level.WARNING, e.getMessage(),e);
			}
		}
		return r4Revision.getBundleContext();
	}

	/**
	 * Stop the platform.
	 */
	@Override
	public void stop() throws OSGiRevisionException
	{
		try
		{
			getJBoss().stop();

		} catch (BundleException|IOException e)
		{
			LOGGER.log(Level.WARNING, e.getMessage(),e);
			throw new OSGiRevisionException(e.getMessage());
		}
	}

	// ---------------------------------------------------------------------
	// Implementation specific
	// ---------------------------------------------------------------------

	private static Logger LOGGER = Logger.getLogger(PlatformImpl.class.getCanonicalName());

	/** The instance of the OSGi JBoss Framework */
	private static Framework jboss;

	/**
	 * Return the singleton instance of the JBoss OSGi framework.
	 */
	private static Framework getJBoss() throws IOException, BundleException
	{
		if (jboss == null)
		{
			/*
			 * Prepare a cache directory for JBoss.
			 */
			File tmpFile = File.createTempFile("jboss", ".tmp");
			tmpFile.delete();
			// Retrieve the directory containing temporary files
			File dir = tmpFile.getParentFile();
			File cache = new File(dir, "jboss");
			// Delete files which may have been stored by a previous run
			deleteAllRecursively(cache);
			cache.mkdir();

			URL log4jConfFileURL = getResource("log4j.xml");
			System.setProperty("log4j.configuration",log4jConfFileURL.toExternalForm());

			System.setProperty("org.osgi.service.http.port", "8891");
			System.setProperty("osgi.server.home", cache.getAbsolutePath());
			System.setProperty("org.osgi.framework.storage",
					new StringBuilder(cache.getAbsolutePath())
							.append(File.separator).append("data")
							.append(File.separator).append("osgi-store")
							.toString());

			String deployPath = new StringBuilder(cache.getAbsolutePath())
					.append(File.separator).append("deploy").toString();

			new File(deployPath).mkdir();

			System.setProperty("org.jboss.osgi.hotdeploy.scandir", deployPath);
			System.setProperty("org.osgi.framework.storage.clean",
					"onFirstInit");
			System.setProperty("jboss.bind.address", "localhost");
			System.setProperty("jboss.vfs.leakDebugging", "true");
			System.setProperty("java.net.preferIPv4Stack", "true");

			String handlerPkgs = System.getProperty("java.protocol.handler.pkgs", "");

			System.setProperty("java.protocol.handler.pkgs",
					new StringBuilder(handlerPkgs)
							.append(handlerPkgs.length() > 0 ? "|" : "")
							.append("org.jboss.net.protocol|")
							.append("org.jboss.virtual.protocol|")
							.append("org.jboss.vfs.protocol").toString());

			try( InputStream configurationFileStream = getResource(
					"jboss-osgi-framework.properties").openStream() ) {
				try
				{
					URLStreamHandlerFactory urlStreamHandlerFactory = new URLStreamHandlerFactory();
					URL.setURLStreamHandlerFactory(urlStreamHandlerFactory);

				} catch (Throwable e)
				{
					LOGGER.info("URL.setURLStreamHandlerFactory Error :" + e.getMessage());
				}

				OSGiBootstrapProvider bootProvider = OSGiBootstrap.getBootstrapProvider();
				bootProvider.configure(configurationFileStream);
				jboss = bootProvider.getFramework();
				jboss.start();
			}
		}
		return jboss;
	}

	private static URL getResource(String resourceName)
	{
		URL result = Thread.currentThread().getContextClassLoader().getResource(resourceName);
		return result;
	}

	/**
	 * Delete recursively all the files contained in the specified file if the
	 * specified file is a directory, and the specified file itself.
	 *
	 * @param f
	 *            the file to delete
	 */
	private static void deleteAllRecursively(File f)
	{

		if (f.isDirectory())
		{
			File[] subs = f.listFiles();
			for (int i = 0; i < subs.length; i++)
			{
				deleteAllRecursively(subs[i]);
			}
		}
		f.delete();
	}

}
