package example.hw;

import java.util.Map;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.SuperController;
import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.fraclet.types.Cardinality;
import org.objectweb.fractal.juliac.adlet.api.BinderItf;
import org.objectweb.fractal.juliac.adlet.api.annotation.Binding;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;
import org.objectweb.fractal.juliac.adlet.api.annotation.StaticMetamodel;
import org.objectweb.fractal.juliac.commons.io.Console;
import org.objectweb.fractal.util.ContentControllerHelper;

@Component(provides=@Interface(name="r",signature=Runnable.class))
@StaticMetamodel("HelloWorld_")
public class HelloWorld {

	@Component(provides=@Interface(name="in",signature=Runnable.class))
	static class Front implements Runnable {

		private Console console;

		public Front() {
			console = Console.getConsole("hw-adlet-");
		}

		public void run() {
			console.println("In Front id: "+id);

			// Retrieve component server2
			org.objectweb.fractal.api.Component parent =
				sc.getFcSuperComponents()[0];
			org.objectweb.fractal.api.Component server2 =
				ContentControllerHelper.getSubComponentByName(parent,"server2");

			// Set the value of the count attribute exposed by server2 to 2
			ServerImplAttributesItf attrCtrl;
			try {
				attrCtrl = (ServerImplAttributesItf)
					server2.getFcInterface("attribute-controller");
			}
			catch( NoSuchInterfaceException nsie ) {
				throw new RuntimeException(nsie);
			}
			attrCtrl.setWriteOnlyCount(2);

			out.run();
		}

		@Requires
		private Runnable out;

		@Attribute("default")
		private String id;

		@Attribute
		private int not_initialized;

		@Controller(name="super-controller")
		private SuperController sc;
	}

	@Component(provides=@Interface(name="r",signature=Runnable.class))
	static class Container {

		@Requires
		private PrinterItf s;

		@Requires(cardinality=Cardinality.COLLECTION)
		private Map<String,PrinterItf> col;

		@Component
		private ClientImpl client;
	}

	@Component
	private ServerImpl server;

	@Component
	private ServerImpl server1;

	@Component(name="server2")
	private ServerImpl s2;

	@Binding
	public static void binder( BinderItf binder ) {

		binder.export( HelloWorld_.r, HelloWorld_.Front.in );
		binder.normal( HelloWorld_.Front.out, HelloWorld_.Container.r );
		binder.export( HelloWorld_.Container.r, HelloWorld_.Container.client.r );
		binder.impor( HelloWorld_.Container.client.s, HelloWorld_.Container.s );
		binder.normal( HelloWorld_.Container.s, HelloWorld_.server.s );

		binder.impor( HelloWorld_.Container.client.col.setSuffix("1"), HelloWorld_.Container.col.setSuffix("1") );
		binder.impor( HelloWorld_.Container.client.col.setSuffix("2"), HelloWorld_.Container.col.setSuffix("2") );
		binder.normal( HelloWorld_.Container.col.setSuffix("1"), HelloWorld_.server1.s );
		binder.normal( HelloWorld_.Container.col.setSuffix("2"), HelloWorld_.server2.s );
	}
}
