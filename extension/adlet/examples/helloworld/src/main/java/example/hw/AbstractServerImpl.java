package example.hw;

import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Lifecycle;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.fraclet.types.Access;
import org.objectweb.fractal.fraclet.types.Step;
import org.objectweb.fractal.juliac.commons.io.Console;

public abstract class AbstractServerImpl implements PrinterItf {

	@Attribute(value="-> ")
	protected String header;

	@Attribute(value="1",mode=Access.WRITE_ONLY,set="setWriteOnlyCount")
	protected int count;

	protected Console console;

	public AbstractServerImpl() {
		console = Console.getConsole("hw-adlet-");
	}

	@Lifecycle(step=Step.CREATE)
	private void onCreate() {
		console.println("SERVER created");
	}

	@Controller(name="name-controller")
	protected NameController nc;
}
