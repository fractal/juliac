package example.hw;

import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;
import org.objectweb.fractal.juliac.adlet.api.annotation.StaticMetamodel;

@Component(
	provides={
		@Interface(name="s",signature=PrinterItf.class),
		@Interface(
			name="attribute-controller",
			signature=ServerImplAttributesItf.class)})
@StaticMetamodel("ServerImpl_")
public class ServerImpl extends AbstractServerImpl {

	public void print(final String msg) {
		console.println("Server "+nc.getFcName()+": begin printing...");
		for (int i = 0 ; i < (count) ; ++i) {
			console.println(header + msg);
		}
		console.println("Server: print done.");
	}
}
