package example.hw;

import org.objectweb.fractal.api.control.AttributeController;

public interface ServerImplAttributesItf extends AttributeController {

	public String getHeader();
	public void setHeader( String header );

	public void setWriteOnlyCount( int count );
}
