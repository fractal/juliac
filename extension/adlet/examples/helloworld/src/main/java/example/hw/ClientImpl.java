package example.hw;

import java.util.Map;
import java.util.TreeMap;

import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.fraclet.annotations.Lifecycle;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.fraclet.types.Cardinality;
import org.objectweb.fractal.fraclet.types.Step;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;
import org.objectweb.fractal.juliac.commons.io.Console;

@Component(provides=@Interface(name="r",signature=Runnable.class))
public class ClientImpl implements Runnable {

	private Console console;

	public ClientImpl() {
		console = Console.getConsole("hw-adlet-");
	}

	@Lifecycle(step=Step.CREATE)
	public void onCreate() {
		console.println("CLIENT created");
	}

	public void run() {
		s.print(nc.getFcName()+": Hello world");
		for (PrinterItf s : col.values()) {
			s.print("Hello world");
		}
	}

	@Requires
	private PrinterItf s;

	@Requires(cardinality=Cardinality.COLLECTION)
	private Map<String,PrinterItf> col = new TreeMap<>();

	@Controller(name="name-controller")
	private NameController nc;

	@Lifecycle(step=Step.START)
	public void onStart() {
		console.println("CLIENT starting");
	}

	@Lifecycle(step=Step.STOP)
	private void onStop() {
		console.println("CLIENT stopping");
	}
}
