package example.comanche;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintStream;

import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;

@Component(provides=@Interface(name="rh",signature=RequestHandler.class))
public class RequestAnalyzer implements RequestHandler {

	@Requires(name="a")
	private RequestHandler rh;

	@Requires
	private Logger l;

	public void handleRequest(Request r) throws IOException {
		r.in = new InputStreamReader(r.s.getInputStream());
		r.out = new PrintStream(r.s.getOutputStream());
		String rq = new LineNumberReader(r.in).readLine();
		l.log(rq);
		if (rq.startsWith("GET ")) {
			r.url = rq.substring(5 ,rq.indexOf(' ' ,4));
			rh.handleRequest(r);
		}
		r.out.close();
		r.s.close();
	}
}
