package example.comanche;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.types.Cardinality;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;

@Component(provides=@Interface(name="rh",signature=RequestHandler.class))
public class RequestDispatcher implements RequestHandler {

	@Requires(name="h",cardinality=Cardinality.COLLECTION)
	private Map<String,RequestHandler> handlers = new TreeMap<>();

	public void handleRequest(Request r) {
		for (RequestHandler h : handlers.values()) {
			try {
				h.handleRequest(r);
				return ;
			} catch (IOException e) {
			}
		}
	}
}
