package example.comanche;

import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;

@Component(provides=@Interface(name="l",signature=Logger.class))
public class BasicLogger implements Logger {

	public void log(String msg) {
		System.out.println(msg);
	}
}
