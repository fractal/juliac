/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package example.comanche;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.junit.jupiter.api.Test;

/**
 * Program for testing the Comanche example.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.0
 */
public class ComancheTestCase {

	@Test
	public void testComanche() throws Exception {

		ThreadMain comanche = new ThreadMain();
		comanche.start();

		// Waiting for Comanche to start up.
		Thread.sleep(3000);

		try {
			// Load the root page. Should be empty.
			try {
				load( "http://localhost:8042", 0, new int[]{} );
				fail("");
			}
			catch(FileNotFoundException fne) {}

			// Load a well-known logo.
			load( "http://localhost:8042/src/main/resources/example/comanche/gnu.jpg",
				  0x14a6,
				  new int[]{255,216,255,224,0}
				  );

			// Stop Comanche.
			comanche.interrupt();
		}
		catch(Exception e) {
			throw comanche.e == null ? e : comanche.e;
		}
	}

	private void load(
		String urlStr, int expectedLength, int[] expectedBytes )
	throws IOException {

		System.out.println("URL : "+urlStr);
		System.out.println("Expected length : "+expectedLength);
		System.out.print("Expected bytes  : ");
		for (int i = 0; i < expectedBytes.length; i++) {
			System.out.print(expectedBytes[i]+" ");
		}
		System.out.println();

		URL url = new URL(urlStr);
		URLConnection uc = url.openConnection();
		InputStream is = uc.getInputStream();

		System.out.print("Bytes          : ");
		int i,length=0;
		boolean differs = false;
		while( (i=is.read()) != -1 ) {
			if( length < expectedBytes.length ) {
				System.out.print(i+" ");
				if( i != expectedBytes[length] ) {
					differs = true;
				}
			}
			length++;
		}
		System.out.println();
		System.out.println("Length         : "+length);

		assertFalse(differs);
		assertEquals(length,expectedLength);
	}

	/**
	 * Call main in a thread and store potential exception in a field.
	 */
	private static class ThreadMain extends Thread {
		public Exception e;
		@Override
		public void run() {
			try {
				String name = getClass().getPackage().getName()+".ComancheUM";

				@SuppressWarnings("unchecked")
				Class<? extends Runnable> cl =
					(Class<? extends Runnable>) Class.forName(name);
				Runnable r = cl.getConstructor().newInstance();
				r.run();
			}
			catch(Exception e) {
				this.e = e;
			}
		}
	}

}
