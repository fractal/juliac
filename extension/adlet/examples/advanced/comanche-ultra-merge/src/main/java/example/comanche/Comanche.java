package example.comanche;

import org.objectweb.fractal.juliac.adlet.api.BinderItf;
import org.objectweb.fractal.juliac.adlet.api.annotation.Binding;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;
import org.objectweb.fractal.juliac.adlet.api.annotation.StaticMetamodel;

@Component(provides=@Interface(name="r",signature=Runnable.class))
@StaticMetamodel("Comanche_")
public class Comanche {

	@Component
	private FrontEnd fe;

	@Component
	private BackEnd be;

	@Binding
	public static void binder( BinderItf binder ) {
		binder.export(Comanche_.r, Comanche_.fe.r);
		binder.normal(Comanche_.fe.rh, Comanche_.be.rh);
	}
}
