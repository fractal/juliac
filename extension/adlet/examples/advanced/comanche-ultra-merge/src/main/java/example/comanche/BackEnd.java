package example.comanche;

import org.objectweb.fractal.juliac.adlet.api.BinderItf;
import org.objectweb.fractal.juliac.adlet.api.annotation.Binding;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;
import org.objectweb.fractal.juliac.adlet.api.annotation.StaticMetamodel;

@Component(provides=@Interface(name="rh",signature=RequestHandler.class))
@StaticMetamodel("BackEnd_")
public class BackEnd {

	@Component
	private RequestAnalyzer ra;

	@Component(provides=@Interface(name="rh",signature=RequestHandler.class))
	static class Handler {

		@Component
		private RequestDispatcher rd;

		@Component
		private FileRequestHandler frh;

		@Component
		private ErrorRequestHandler erh;
	}

	@Component
	private BasicLogger l;


	@Binding
	public static void binder( BinderItf binder ) {

		binder.export(BackEnd_.rh, BackEnd_.ra.rh);
		binder.normal(BackEnd_.ra.l, BackEnd_.l.l);
		binder.normal(BackEnd_.ra.a, BackEnd_.Handler.rh);

		binder.export(BackEnd_.Handler.rh, BackEnd_.Handler.rd.rh);
		binder.normal(BackEnd_.Handler.rd.h.setSuffix("0"), BackEnd_.Handler.frh.rh);
		binder.normal(BackEnd_.Handler.rd.h.setSuffix("1"), BackEnd_.Handler.erh.rh);
	}
}
