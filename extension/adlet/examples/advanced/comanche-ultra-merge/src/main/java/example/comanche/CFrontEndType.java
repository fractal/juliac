package example.comanche;

import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;

@Component(provides=@Interface(name="r",signature=Runnable.class))
public class CFrontEndType {

	@Requires
	protected RequestHandler rh;
}
