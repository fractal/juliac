package example.comanche;

import org.objectweb.fractal.juliac.adlet.api.BinderItf;
import org.objectweb.fractal.juliac.adlet.api.annotation.Binding;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;
import org.objectweb.fractal.juliac.adlet.api.annotation.StaticMetamodel;

@Component(provides=@Interface(name="r",signature=Runnable.class))
@StaticMetamodel("FrontEnd_")
public class FrontEnd extends CFrontEndType {

	@Component
	private RequestReceiver rr;

	@Component
	private MultiThreadScheduler s;

	@Binding
	public static void binder( BinderItf binder ) {
		binder.export(FrontEnd_.r, FrontEnd_.rr.r);
		binder.impor(FrontEnd_.rr.rh, FrontEnd_.rh);
		binder.normal(FrontEnd_.rr.s, FrontEnd_.s.s);
	}
}
