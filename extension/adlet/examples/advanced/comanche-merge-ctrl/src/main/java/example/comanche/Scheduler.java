package example.comanche;


public interface Scheduler {
	void schedule(Runnable task);
}
