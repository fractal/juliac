package example.comanche;

import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;

@Component(provides=@Interface(name="rh",signature=RequestHandler.class))
public class ErrorRequestHandler implements RequestHandler {
	public void handleRequest(Request r) {
		r.out.print("HTTP/1.0 404 Not Found\n\n");
		r.out.print("<html>Document not found.</html>");
	}
}
