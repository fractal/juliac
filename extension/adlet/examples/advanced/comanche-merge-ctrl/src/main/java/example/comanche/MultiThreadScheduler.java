package example.comanche;

import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;

@Component(provides=@Interface(name="s",signature=Scheduler.class))
public class MultiThreadScheduler implements Scheduler {
	public void schedule(Runnable task) {
		new Thread(task).start();
	}
}
