package example.comanche;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;

@Component(provides=@Interface(name="r",signature=Runnable.class))
public class RequestReceiver implements Runnable {

	@Requires
	private Scheduler s;

	@Requires
	private RequestHandler rh;

	public void run() {
		ServerSocket ss = null;
		try {
			ss = new ServerSocket(8042);
			System.out.println("Comanche HTTP Server ready on port 8042.");
			System.out.println("Load http://localhost:8042/src/main/resources/example/comanche/gnu.jpg");
			while (true) {
				final Socket socket = ss.accept();
				s.schedule(new Runnable() {
					public void run() {
						try {
							rh.handleRequest(new Request(socket));
						} catch (IOException e) {
						}
					}

				});
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if( ss != null ) {
				try {
					ss.close();
				}
				catch( IOException ioe ) {}
			}
		}
	}
}
