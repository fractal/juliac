package example.hw;

import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;
import org.objectweb.fractal.juliac.commons.io.Console;

@Component(provides=@Interface(name="r",signature=Runnable.class))
public class ClientImpl implements Runnable {

	public void run() {
		Console console = Console.getConsole("hw-ultra-merge-");
		console.println("CLIENT");
		s.print("Hello world");
	}

	@Requires
	private Service s;
}
