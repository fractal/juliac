package example.hw;

import org.objectweb.fractal.juliac.adlet.api.BinderItf;
import org.objectweb.fractal.juliac.adlet.api.annotation.Binding;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;
import org.objectweb.fractal.juliac.adlet.api.annotation.StaticMetamodel;

@Component(provides=@Interface(name="r",signature=Runnable.class))
@StaticMetamodel("HelloWorld_")
public class HelloWorld {

	@Component
	private ClientImpl c;

	@Component
	private ServerImpl s;

	@Binding
	public static void binder( BinderItf binder ) {
		binder.export(HelloWorld_.r, HelloWorld_.c.r);
		binder.normal(HelloWorld_.c.s, HelloWorld_.s.s);
	}
}
