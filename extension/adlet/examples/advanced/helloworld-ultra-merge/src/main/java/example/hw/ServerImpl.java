package example.hw;

import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;
import org.objectweb.fractal.juliac.commons.io.Console;

@Component(provides=@Interface(name="s",signature=Service.class))
public class ServerImpl implements Service {

	@Attribute(value="-> ")
	private String header;

	@Attribute(value="1")
	private int count;

	public void print(final String msg) {
		Console console = Console.getConsole("hw-ultra-merge-");
		console.println("SERVER");
		console.println("Server: begin printing...");
		for (int i = 0 ; i < (count) ; ++i) {
			console.println(header + msg);
		}
		console.println("Server: print done.");
	}
}
