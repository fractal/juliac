/***

 * Juliac
 * Copyright (C) 2019-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.Elements;

import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.juliac.adlet.core.proxy.AttributeControlFcInterfaceImplementationClassGenerator;
import org.objectweb.fractal.juliac.adlet.core.proxy.AttributeControlImplementationGenerator;
import org.objectweb.fractal.juliac.adlet.core.proxy.AttributeControlInterfaceGenerator;
import org.objectweb.fractal.juliac.api.JuliacRuntimeException;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;

/**
 * This annotation processor handles @{@link Attribute} annotated fields and
 * generates attribute control Java interfaces, implementations, and Fractal
 * interfaces.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public class AttributeProcessor extends AbstractJuliacProcessor {

	// -------------------------------------------------------------------
	// Implementation of the AbstractProcessor methods
	// -------------------------------------------------------------------

	@Override
	 public Set<String> getSupportedAnnotationTypes() {
		Set<String> annotations = new LinkedHashSet<>();
		annotations.add(Attribute.class.getCanonicalName());
		return annotations;
	}

	@Override
	public SourceVersion getSupportedSourceVersion() {
		return SourceVersion.latestSupported();
	}

	@Override
	public boolean process(
		Set<? extends TypeElement> annotations, RoundEnvironment roundEnv ) {

		initJuliac(processingEnv);

		/*
		 * Process @{@link Mixin}-annotated classes.
		 */
		Set<? extends Element> elements =
			roundEnv.getElementsAnnotatedWith(Attribute.class);
		Map<TypeElement,List<VariableElement>> fields = new HashMap<>();
		for (Element element : elements) {
			if( element.getKind().isField() ) {
				Element cl = element;
				do { cl = cl.getEnclosingElement(); }
				while( ! cl.getKind().isClass() );
				List<VariableElement> l;
				if( fields.containsKey(cl) ) {
					l = fields.get(cl);
				}
				else {
					l = new ArrayList<>();
					fields.put((TypeElement)cl,l);
				}
				l.add((VariableElement)element);
			}
		}

		try { generate(fields); }
		catch( IOException ioe ) {
			throw new JuliacRuntimeException(ioe);
		}

		closeJuliac();

		return true;
	}


	// -------------------------------------------------------------------
	// Implementation specific
	// -------------------------------------------------------------------

	private void generate( Map<TypeElement,List<VariableElement>> fields )
	throws IOException {

		for( Map.Entry<TypeElement,List<VariableElement>> entry : fields.entrySet() ) {

			TypeElement adl = entry.getKey();
			List<VariableElement> attributeFields = entry.getValue();
			Elements elements = processingEnv.getElementUtils();
			String fqname = elements.getBinaryName(adl).toString();

			// Generate the attribute control interface
			SourceCodeGeneratorItf acig =
				new AttributeControlInterfaceGenerator(
					adl, fqname.replace('$','_'), attributeFields,
					processingEnv );
			jc.generateSourceCode(acig);
			String attributeControlInterfaceName = acig.getTargetTypeName();

			// Generate the attribute controller implementation
			SourceCodeGeneratorItf acg =
				new AttributeControlImplementationGenerator(
					adl, fqname.replace('$','_'), attributeFields,
					fqname.replace('$','.'), attributeControlInterfaceName,
					processingEnv );
			jc.generateSourceCode(acg);

			// Generate the Fractal interface implementation
			SourceCodeGeneratorItf fcscg =
				new AttributeControlFcInterfaceImplementationClassGenerator(
					adl, attributeFields, attributeControlInterfaceName,
					processingEnv );
			jc.generateSourceCode(fcscg);
		}
	}
}
