/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */
package org.objectweb.fractal.juliac.adlet.core;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.JavaFileObject;

import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.types.Cardinality;
import org.objectweb.fractal.fraclet.types.Constants;
import org.objectweb.fractal.fraclet.types.Contingency;
import org.objectweb.fractal.juliac.adlet.api.Mandatory;
import org.objectweb.fractal.juliac.adlet.api.Optional;
import org.objectweb.fractal.juliac.adlet.api.Provided;
import org.objectweb.fractal.juliac.adlet.api.ProvidedCollection;
import org.objectweb.fractal.juliac.adlet.api.ProvidedCollectiongInterfaceImpl;
import org.objectweb.fractal.juliac.adlet.api.ProvidedImpl;
import org.objectweb.fractal.juliac.adlet.api.Required;
import org.objectweb.fractal.juliac.adlet.api.RequiredCollection;
import org.objectweb.fractal.juliac.adlet.api.RequiredCollectiongInterfaceImpl;
import org.objectweb.fractal.juliac.adlet.api.RequiredImpl;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.StaticMetamodel;
import org.objectweb.fractal.juliac.adlet.core.helper.AnnotatedConstructHelper;
import org.objectweb.fractal.juliac.adlet.core.helper.AnnotationMirrorHelper;
import org.objectweb.fractal.juliac.api.JuliacRuntimeException;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.FileSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.visit.FileSourceCodeWriter;

/**
 * This Java annotation processor generates a static metamodel for
 * @{@link StaticMetamodel}-annotated Adlet component descriptors.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public class StaticMetamodelProcessor extends AbstractProcessor {

	// -------------------------------------------------------------------
	// Implementation of the AbstractProcessor methods
	// -------------------------------------------------------------------

	@Override
	 public Set<String> getSupportedAnnotationTypes() {
		Set<String> annotations = new LinkedHashSet<>();
		annotations.add(StaticMetamodel.class.getCanonicalName());
		return annotations;
	}

	@Override
	public SourceVersion getSupportedSourceVersion() {
		return SourceVersion.latestSupported();
	}

	@Override
	public boolean process(
		Set<? extends TypeElement> annotations, RoundEnvironment roundEnv ) {

		Set<? extends Element> elements =
			roundEnv.getElementsAnnotatedWith(StaticMetamodel.class);

		for (Element element : elements) {
			if( element.getKind() == ElementKind.CLASS ) {
				try { process((TypeElement)element); }
				catch( IOException ioe ) {
					throw new JuliacRuntimeException(ioe);
				}
			}
		}

		return true;
	}


	// -------------------------------------------------------------------
	// Implementation specific
	// -------------------------------------------------------------------

	/**
	 * Generate the metamodel for a @{@link StaticMetamodel}-annotated class.
	 *
	 * @param element  the @{@link StaticMetamodel}-annotated class
	 */
	private void process( TypeElement element ) throws IOException {

		/*
		 * Retrieve the name of the class where the metamodel is to be
		 * generated.
		 */
		String targetClassName =
			AnnotatedConstructHelper.getAnnotationParamValue(
				element,StaticMetamodel.class,"value",processingEnv).toString();

		int lastdot = targetClassName.lastIndexOf('.');
		String packageName, className;
		if( lastdot == -1 ) {
			className = targetClassName;
			// When no package name is specified, the value refers to the same
			// package as the one of the annotated element
			lastdot = element.toString().lastIndexOf('.');
			if( lastdot == -1 ) {
				packageName = null;
			}
			else {
				packageName = element.toString().substring(0,lastdot);
				targetClassName = packageName+'.'+className;
			}
		}
		else {
			packageName = targetClassName.substring(0,lastdot);
			className = targetClassName.substring(lastdot+1);
		}

		/*
		 * Generate the metamodel class.
		 */
		Filer filer = processingEnv.getFiler();
		JavaFileObject sourceFile =
			filer.createSourceFile(targetClassName,element);

		try( PrintWriter pw = new PrintWriter(sourceFile.openWriter()) ) {
			FileSourceCodeVisitor cw = new FileSourceCodeWriter(pw);

			if( packageName != null ) {
				cw.visitPackageName(packageName);
			}

			cw.visitComment("/** @see "+element.toString()+" */");

			ClassSourceCodeVisitor cv = cw.visitPublicClass();
			cv.visit(Modifier.PUBLIC, className, null, null, null);
			process(element,cv,className,Object.class.getName());
			int modifiers = Modifier.FINAL + Modifier.PUBLIC + Modifier.STATIC;
			cv.visitField(modifiers, "String", "_this", "\""+element+"\"");
			cv.visitEnd();
		}
		catch( IOException ioe ) {
			sourceFile.delete();
			throw ioe;
		}
	}

	private static final String BOTTOM = "_Bottom";

	/**
	 * Generate the metamodel for the specified element.
	 *
	 * @param element    the component to be processed
	 * @param cv         the class visitor to generate the metamodel class
	 * @param className  the name of the metamodel class
	 * @param outterClassName
	 *         the name of the outter class or <code>null</code> if this is the top
	 *         level class
	 */
	private void process(
		TypeElement element, ClassSourceCodeVisitor cv, String className,
		String outterClassName ) {

		Elements elements = processingEnv.getElementUtils();
		Types types = processingEnv.getTypeUtils();

		/*
		 * Generate an empty _Bottom class.
		 */
		ClassSourceCodeVisitor bottomcv = cv.visitInnerClass();
		bottomcv.visit(
			Modifier.PUBLIC+Modifier.STATIC,BOTTOM,null,outterClassName,null);
		bottomcv.visitEnd();

		/*
		 * Generate fields for provided interfaces.
		 */
		Collection<?> provides = (Collection<?>)
			AnnotatedConstructHelper.getAnnotationParamValue(
				element, Component.class, "provides", processingEnv);

		for (Object provide : provides) {
			AnnotationMirror am =
				(AnnotationMirror) ((AnnotationValue)provide).getValue();
			String name = (String)
				AnnotationMirrorHelper.getParameterValue(am,elements,"name");

			// Skip attribute-controller
			if( name.equals("attribute-controller") ) {
				continue;
			}

			Object signature =
				AnnotationMirrorHelper.getParameterValue(
					am,elements,"signature");
			Object cardinality =
				AnnotationMirrorHelper.getParameterValue(
					am,elements,"cardinality");
			Object contingency =
				AnnotationMirrorHelper.getParameterValue(
					am,elements,"contingency");

			int modifiers = Modifier.PUBLIC + Modifier.STATIC;
			StringBuilder type = new StringBuilder();
			StringBuilder expression = new StringBuilder("new ");

			if( cardinality.toString().equals(Cardinality.COLLECTION.toString()) ) {
				// Collection interface
				type.append(ProvidedCollection.class.getName());
				expression.append(ProvidedCollectiongInterfaceImpl.class.getName());
			}
			else {
				// Singleton interface
				type.append(Provided.class.getName());
				expression.append(ProvidedImpl.class.getName());
			}

			StringBuilder generics = new StringBuilder();
			generics.append('<');
			generics.append(signature);
			generics.append(',');
			generics.append(outterClassName);
			generics.append(',');
			generics.append(className);
			generics.append(',');
			generics.append(BOTTOM);
			generics.append(',');
			if( contingency.toString().equals(Contingency.OPTIONAL.toString()) ) {
				generics.append(Optional.class.getName());
			}
			else {
				generics.append(Mandatory.class.getName());
			}
			generics.append('>');
			type.append(generics);

			expression.append("<>(");
			expression.append(className);
			expression.append(".class,\"");
			expression.append(name);
			expression.append("\")");

			// Replace illegal characters in the field name
			name = name.replace('-','_');
			name = name.replace('/','_');

			cv.visitComment("/** @see "+element.toString()+'#'+name+" */");
			cv.visitField(modifiers,type.toString(),name,expression.toString());
		}

		/*
		 * Generate fields for required interfaces.
		 */
		List<? extends Element> members = elements.getAllMembers(element);
		List<VariableElement> fields = ElementFilter.fieldsIn(members);
		for (VariableElement field : fields) {
			Requires requires = field.getAnnotation(Requires.class);
			if( requires != null ) {

				String name = (String)
					AnnotatedConstructHelper.getAnnotationParamValue(
						field,Requires.class,"name",processingEnv);
				if( name.equals("") ) {
					name = field.getSimpleName().toString();
				}

				Object cardinality =
					AnnotatedConstructHelper.getAnnotationParamValue(
						field,Requires.class,"cardinality",processingEnv);
				Object contingency =
					AnnotatedConstructHelper.getAnnotationParamValue(
						field,Requires.class,"contingency",processingEnv);
				Object signature =
					AnnotatedConstructHelper.getAnnotationParamValue(
						field,Requires.class,"signature",processingEnv);

				int modifiers = Modifier.PUBLIC + Modifier.STATIC;
				StringBuilder type = new StringBuilder();
				StringBuilder expression = new StringBuilder("new ");

				if( cardinality.toString().equals(Cardinality.COLLECTION.toString()) ) {
					// Collection interface
					if( signature.toString().equals(Constants.class.getName()) ) {
						DeclaredType dt = (DeclaredType) field.asType();
						List<? extends TypeMirror> pts = dt.getTypeArguments();
						signature = pts.get(1);
					}
					type.append(RequiredCollection.class.getName());
					expression.append(RequiredCollectiongInterfaceImpl.class.getName());
				}
				else {
					// Singleton interface
					if( signature.toString().equals(Constants.class.getName()) ) {
						signature = field.asType();
					}
					type.append(Required.class.getName());
					expression.append(RequiredImpl.class.getName());
				}

				StringBuilder generics = new StringBuilder();
				generics.append('<');
				generics.append(signature);
				generics.append(',');
				generics.append(outterClassName);
				generics.append(',');
				generics.append(className);
				generics.append(',');
				generics.append(BOTTOM);
				generics.append(',');
				if( contingency.toString().equals(Contingency.OPTIONAL.toString()) ) {
					generics.append(Optional.class.getName());
		        }
				else {
					generics.append(Mandatory.class.getName());
				}
				generics.append('>');
				type.append(generics);

				expression.append("<>(");
				expression.append(className);
				expression.append(".class,\"");
				expression.append(name);
				expression.append("\")");

				// Replace illegal characters in the field name
				name = name.replace('-','_');
				name = name.replace('/','_');

				cv.visitComment("/** @see "+element.toString()+'#'+field.getSimpleName()+" */");
				cv.visitField(
					modifiers, type.toString(), name, expression.toString() );
			}
		}

		/*
		 * Generate classes for subcomponents.
		 * @Component annotated fields.
		 */
		for (VariableElement field : fields) {
			Component sub = field.getAnnotation(Component.class);
			if( sub != null ) {
				TypeMirror tm = field.asType();
				TypeElement te = (TypeElement) types.asElement(tm);
				int modifiers = Modifier.PUBLIC + Modifier.STATIC;
				String name =
					sub.name().length() == 0 ?
					field.getSimpleName().toString() :
					sub.name();
				ClassSourceCodeVisitor innercv = cv.visitInnerClass();
				innercv.visitComment("/** @see "+element.toString()+'#'+field.getSimpleName()+" */");
				innercv.visit(modifiers,name,null,null,null);
				process(te,innercv,className+'.'+name,className);
				innercv.visitEnd();

				modifiers = Modifier.FINAL + Modifier.PUBLIC + Modifier.STATIC;
				cv.visitField(modifiers, "String", name+"_", "\""+te+"\"");
			}
		}

		/*
		 * Generate classes for subcomponents.
		 * @Component annotated inner classes.
		 */
		List<TypeElement> inners = ElementFilter.typesIn(members);
		for (TypeElement inner : inners) {
			Component sub = inner.getAnnotation(Component.class);
			if( sub != null ) {
				int modifiers = Modifier.PUBLIC + Modifier.STATIC;
				String name =
					sub.name().length() == 0 ?
					inner.getSimpleName().toString() :
					sub.name();
				ClassSourceCodeVisitor innercv = cv.visitInnerClass();
				innercv.visitComment("/** @see "+inner.toString()+" */");
				innercv.visit(modifiers,name,null,null,null);
				process(inner,innercv,className+'.'+name,className);
				innercv.visitEnd();

				modifiers = Modifier.FINAL + Modifier.PUBLIC + Modifier.STATIC;
				cv.visitField(modifiers, "String", name+"_", "\""+className+'.'+name+"\"");
			}
		}
	}
}
