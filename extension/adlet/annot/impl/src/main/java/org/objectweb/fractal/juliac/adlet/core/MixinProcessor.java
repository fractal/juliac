/***

 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.core;

import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.Filer;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;

import org.objectweb.fractal.juliac.adlet.api.annotation.Mixin;
import org.objectweb.fractal.juliac.adlet.core.helper.AnnotatedConstructHelper;
import org.objectweb.fractal.juliac.api.JuliacRuntimeException;
import org.objectweb.fractal.juliac.api.SpoonSupportItf;
import org.objectweb.fractal.juliac.core.SourceFileDir;

/**
 * This annotation processor handles the mixing of layers specified by
 * @{@link Mixin}-annotated classes.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public class MixinProcessor extends AbstractJuliacProcessor {

	// -------------------------------------------------------------------
	// Implementation of the AbstractProcessor methods
	// -------------------------------------------------------------------

	@Override
	 public Set<String> getSupportedAnnotationTypes() {
		Set<String> annotations = new LinkedHashSet<>();
		annotations.add(Mixin.class.getCanonicalName());
		return annotations;
	}

	@Override
	public SourceVersion getSupportedSourceVersion() {
		return SourceVersion.latestSupported();
	}

	@Override
	public boolean process(
		Set<? extends TypeElement> annotations, RoundEnvironment roundEnv ) {

		initJuliac(processingEnv);

		/*
		 * Process @{@link Mixin}-annotated classes.
		 */
		Set<? extends Element> elements =
			roundEnv.getElementsAnnotatedWith(Mixin.class);
		for (Element element : elements) {
			if( element.getKind().isClass() ) {
				try { processMixin((TypeElement)element); }
				catch( IOException ioe ) {
					throw new JuliacRuntimeException(ioe);
				}
			}
		}

		closeJuliac();

		return true;
	}


	// -------------------------------------------------------------------
	// Implementation specific
	// -------------------------------------------------------------------

	/**
	 * Mixes layers specified by @{@link Mixin}-annotated classes
	 *
	 * @param cl  the @{@link Mixin}-annotated class
	 */
	private void processMixin( TypeElement cl ) throws IOException {

		String targetClassName = (String)
			AnnotatedConstructHelper.getAnnotationParamValue(
				cl,Mixin.class,"impl",processingEnv);

		/*
		 * Retrieve the names of the layers.
		 */
		Collection<?> layers = (Collection<?>)
			AnnotatedConstructHelper.getAnnotationParamValue(
				cl,Mixin.class,"layers",processingEnv);
		List<String> mixins = new ArrayList<>();
		for (Object layer : layers) {
			String mixin = layer.toString();
			// Remove trailing .class
			mixin = mixin.substring(0,mixin.length()-".class".length());
			mixins.add(mixin);
		}

		/*
		 * Retrieve an output file and the Spoon service.
		 */
		Filer filer = processingEnv.getFiler();
		JavaFileObject sourceFile =
			filer.createSourceFile(targetClassName,cl);
		SpoonSupportItf spoon = jc.lookupFirst(SpoonSupportItf.class);

		/*
		 * Mix the layers with Spoon.
		 */
		try( Writer writer = sourceFile.openWriter() ) {
			spoon.mix(targetClassName,mixins,writer);
		}
		catch( IOException ioe ) {
			sourceFile.delete();
			throw ioe;
		}

		/*
		 * Add the generated file to the current compilation round.
		 */
		URI uri = sourceFile.toUri();
		jc.addGenerated(new SourceFileDir(uri,targetClassName,"java"),null);
	}
}
