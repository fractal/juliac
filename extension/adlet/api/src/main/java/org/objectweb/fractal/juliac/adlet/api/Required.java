/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.api;

/**
 * <p>The interface for declaring a required component interface.</p>
 *
 * <p>
 * When defining a binding, TYPE must be a supertype of the corresponding type
 * of the provided component interface. When defining a normal binding, PARENT
 * is used to enforce that the bound components belong to the same parent
 * component. When defining export and import bindings, CURRENT and BOTTOM are
 * used to enforce that the bound components belong to the current component and
 * one of its direct subcomponents.
 * </p>
 *
 * @param <TYPE>
 *     the type of this component interface
 * @param <PARENT>
 *     the type of the parent component containing the component that
 *     defines this component interface
 * @param <CURRENT>
 *     the type of the component that defines this component interface
 * @param <BOTTOM>
 *     a generated subtype of PARENT
 * @param <CTGY>
 *     the contingency of this component interface
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public interface Required<TYPE,PARENT,CURRENT,BOTTOM,CTGY>
extends ComponentInterfaceItf {
}
