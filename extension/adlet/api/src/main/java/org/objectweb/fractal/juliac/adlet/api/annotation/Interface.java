/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.api.annotation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.objectweb.fractal.fraclet.types.Cardinality.SINGLETON;
import static org.objectweb.fractal.fraclet.types.Constants.EMPTY;
import static org.objectweb.fractal.fraclet.types.Contingency.MANDATORY;

import java.lang.annotation.Retention;

import org.objectweb.fractal.fraclet.types.Cardinality;
import org.objectweb.fractal.fraclet.types.Constants;
import org.objectweb.fractal.fraclet.types.Contingency;

/**
 * Description of a component interface.
 *
 * The definition of this annotation is based on the Fraclet
 * @{@link org.objectweb.fractal.fraclet.annotations.Interface} annotation and
 * extends it with two fields: {@link #cardinality()} and
 * {@link #contingency()}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
@Retention(RUNTIME)
public @interface Interface {

	/**
	 * The name of the interface.
	 */
	String name() default EMPTY;

	/**
	 * The Java type of the interface.
	 */
	Class<?> signature() default Constants.class;

	/**
	 * The cardinality of the interface.
	 */
	Cardinality cardinality() default SINGLETON;

	/**
	 * The contingency of the interface.
	 */
	Contingency contingency() default MANDATORY;
}
