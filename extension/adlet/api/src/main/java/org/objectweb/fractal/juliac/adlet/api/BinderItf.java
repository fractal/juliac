/***
 * Juliac
 * Copyright (C) 2019-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.api;

/**
 * This interface defines bindings between required and provided component
 * interfaces.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public interface BinderItf {

	/**
	 * Record a normal binding between two components contained in the same
	 * parent component of type PARENT. The type PTYPE of the provided interface
	 * must be a subtype of the type RTYPE of the required interface.
	 *
	 * @param r  the required interface from the source component
	 * @param p  the provided interface from the target component
	 */
	public <RTYPE, PTYPE extends RTYPE, PARENT, RCTGY, PCTGY extends RCTGY>
	void normal( Required<RTYPE,PARENT,?,?,RCTGY> r, Provided<PTYPE,PARENT,?,?,PCTGY> p );

	/**
	 * Record an export binding of a provided interface from a subcomponent to
	 * its parent component. The type PTYPE of the provided interface must be a
	 * subtype of the type RTYPE of the required interface. The sub to parent
	 * component relation is enforced by PBOTTOM being a subtype of RCURRENT.
	 *
	 * @param r  the provided interface from the parent component
	 * @param p  the provided interface from the subcomponent
	 */
	public <RTYPE, PTYPE extends RTYPE, RCURRENT, PBOTTOM extends RCURRENT, RCTGY, PCTGY extends RCTGY>
	void export(
		Provided<RTYPE,?,RCURRENT,?,RCTGY> r, Provided<PTYPE,?,?,PBOTTOM,PCTGY> p );

	/**
	 * Record an import binding of a required interface from a parent component
	 * to one of its subcomponents. The type PTYPE of the provided interface
	 * must be a subtype of the type RTYPE of the required interface. The parent
	 * to sub component relation is enforced by RBOTTOM being a subtype of
	 * PCURRENT.
	 *
	 * @param r  the required interface from the subcomponent
	 * @param p  the required interface from the parent component
	 */
	public <RTYPE, PTYPE extends RTYPE, RBOTTOM extends PCURRENT, PCURRENT, RCTGY, PCTGY extends RCTGY>
	void impor(
		Required<RTYPE,?,?,RBOTTOM,RCTGY> r, Required<PTYPE,?,PCURRENT,?,PCTGY> p );
}
