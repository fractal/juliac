/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.api.generator;

import javax.lang.model.element.ExecutableElement;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.generator.InterfaceTypeConfigurableItf;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;

/**
 * Interface for {@link InterfaceType}-based proxy class generators. The
 * generated classes are proxies for a given {@link InterfaceType}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public interface ProxyClassGeneratorItf extends InterfaceTypeConfigurableItf {

	/**
	 * Return the name of the suffix which is appended to generated class names.
	 */
	public String getClassNameSuffix();

	public void generateProxyMethod( ClassSourceCodeVisitor cv, ExecutableElement proxym );
	public void generateProxyMethodBody( BlockSourceCodeVisitor mv, ExecutableElement proxym );
	public void generateProxyMethodBodyDelegatingCode( BlockSourceCodeVisitor mv, ExecutableElement proxym );
	public void generateProxyMethodBodyReturn( BlockSourceCodeVisitor mv, ExecutableElement proxym );

	public String getDelegatingInstance( ExecutableElement proxym );
	public String getDelegatingMethodName( ExecutableElement proxym );
}
