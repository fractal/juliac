/***
 * Juliac
 * Copyright (C) 2018-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.controller.comp;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;

/**
 * This mixin layer provides {@link #weaveableC} field of type {@link Component}
 * to control components that require the component control component.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public abstract class UseComponentMixin {

	// -------------------------------------------------------------------------
	// Private constructor
	// -------------------------------------------------------------------------

	private UseComponentMixin() {
	}

	// -------------------------------------------------------------------------
	// Fields and methods added and overriden by the mixin class
	// -------------------------------------------------------------------------

	/**
	 * The {@link Component} interface of the component to which this controller
	 * object belongs.
	 */

	@Requires(name="//component")
	public Component weaveableC;

	/**
	 * The {@link Component} interface of the component to which this controller
	 * object belongs.
	 */

	public Component weaveableOptC;

	/**
	 * Initializes the fields of this mixin and then calls the overriden method.
	 *
	 * @param ic information about the component to which this controller object
	 *           belongs.
	 * @throws InstantiationException if the initialization fails.
	 */

	public void initFcController(final InitializationContext ic) throws InstantiationException {
//		weaveableC = (Component) ic.getInterface("component");
		weaveableOptC = weaveableC;
		_super_initFcController(ic);
	}

	// -------------------------------------------------------------------------
	// Fields and methods required by the mixin class in the base class
	// -------------------------------------------------------------------------

	/**
	 * The {@link Controller#initFcController initFcController} method overriden by
	 * this mixin.
	 *
	 * @param ic information about the component to which this controller object
	 *           belongs.
	 * @throws InstantiationException if the initialization fails.
	 */

	public abstract void _super_initFcController(InitializationContext ic) throws InstantiationException;
}
