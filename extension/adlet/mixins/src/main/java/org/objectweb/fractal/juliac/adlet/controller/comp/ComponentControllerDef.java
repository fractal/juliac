/***
 * Juliac
 * Copyright (C) 2018-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.controller.comp;

import org.objectweb.fractal.julia.BasicComponentMixin;
import org.objectweb.fractal.julia.BasicControllerMixin;
import org.objectweb.fractal.julia.TypeComponentMixin;
import org.objectweb.fractal.juliac.adlet.api.annotation.Mixin;
import org.objectweb.fractal.juliac.fraclet.control.FracletComponentMixin;
import org.objectweb.fractal.koch.control.interceptor.CollectionItfComponentMixin;

/**
 * Implementation of the component controller.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
@Mixin(
	impl=ComponentControllerDef.NAME,
	layers={
		BasicControllerMixin.class,
		FracletComponentMixin.class,
		BasicComponentMixin.class,
		TypeComponentMixin.class,
		CollectionItfComponentMixin.class
	}
)
public class ComponentControllerDef {
	public static final String NAME = "juliac.generated.ComponentImpl";
}
