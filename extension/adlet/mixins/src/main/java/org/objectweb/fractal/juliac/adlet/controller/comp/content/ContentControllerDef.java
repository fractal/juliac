/***
 * Juliac
 * Copyright (C) 2018-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.controller.comp.content;

import org.objectweb.fractal.julia.BasicControllerMixin;
import org.objectweb.fractal.julia.control.content.BasicContentControllerMixin;
import org.objectweb.fractal.julia.control.content.BindingContentMixin;
import org.objectweb.fractal.julia.control.content.CheckContentMixin;
import org.objectweb.fractal.julia.control.content.LifeCycleContentMixin;
import org.objectweb.fractal.julia.control.content.SuperContentMixin;
import org.objectweb.fractal.julia.control.content.TypeContentMixin;
import org.objectweb.fractal.julia.control.lifecycle.UseLifeCycleControllerMixin;
import org.objectweb.fractal.juliac.adlet.api.annotation.Mixin;
import org.objectweb.fractal.juliac.adlet.controller.comp.UseComponentMixin;
import org.objectweb.fractal.koch.control.interceptor.CollectionItfContentControllerMixin;

/**
 * Implementation of the content controller.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
@Mixin(
	impl=ContentControllerDef.NAME,
	layers={
		BasicControllerMixin.class,
		UseComponentMixin.class,
		BasicContentControllerMixin.class,
		CheckContentMixin.class,
		TypeContentMixin.class,
		CollectionItfContentControllerMixin.class,
		BindingContentMixin.class,
		UseLifeCycleControllerMixin.class,
		LifeCycleContentMixin.class,
		SuperContentMixin.class
	}
)
public class ContentControllerDef {
	public static final String NAME = "juliac.generated.ContentControllerImpl";
}
