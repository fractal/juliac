/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.core;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;

import javax.annotation.processing.Filer;
import javax.annotation.processing.FilerException;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.tools.JavaFileObject;

import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.visit.FileSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.Juliac;
import org.objectweb.fractal.juliac.core.visit.FileSourceCodeWriter;

/**
 * A specialization of the core {@link Juliac} source code generator for the
 * annotation processing tool of the JDK.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public class JuliacAP extends Juliac {

	protected JuliacAP() {}

	// ----------------------------------------------------------------------
	// Methods for generating component source code
	// ----------------------------------------------------------------------

	@Override
	public URI generateSourceCodeOverride( SourceCodeGeneratorItf scg )
	throws IOException {

		String targetClassName = scg.getTargetTypeName();
		Element element = (Element) scg.getSourceType();

		ProcessingEnvironment env = getProcessingEnvironment();

		Filer filer = env.getFiler();
		JavaFileObject sourceFile = null;

		try {
			sourceFile = filer.createSourceFile(targetClassName,element);
		}
		catch( FilerException fe ) {
			/*
			 * The file has already been generated previously by a processor.
			 *
			 * In theory, we wouldn't need to do that if we were able to keep an
			 * instance of Juliac (and thus compilation rounds and generated
			 * files) between two runs of the annotation processor. Yet it
			 * happens that when jc is declared static in JuliacProcessor, the
			 * generation fails under Eclipse with an NPE and a missing stack
			 * trace to trace back the location where it occurred. Catching
			 * FilerException is a way to avoid this NPE and to keep Eclipse
			 * functioning with the annotation processors.
			 */
			return URI.create("");
		}

		try( PrintWriter pw = new PrintWriter(sourceFile.openWriter()) ) {
			FileSourceCodeVisitor cw = new FileSourceCodeWriter(pw);
			scg.generate(cw);
		}
		catch( IOException ioe ) {
			sourceFile.delete();
			throw ioe;
		}

		URI uri = sourceFile.toUri();
		return uri;
	}
}
