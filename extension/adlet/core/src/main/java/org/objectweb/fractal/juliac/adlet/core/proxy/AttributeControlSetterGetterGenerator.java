/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.core.proxy;

import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;

import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.types.Access;
import org.objectweb.fractal.juliac.adlet.core.helper.AnnotatedConstructHelper;
import org.objectweb.fractal.juliac.adlet.core.helper.VariableElementHelper;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.opt.ClassGenerator;

/**
 * Root class for generating setter and getter methods for attribute-based
 * generators.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public abstract class AttributeControlSetterGetterGenerator
extends ClassGenerator {

	protected TypeElement adl;
	protected List<VariableElement> attributeFields;
	private ProcessingEnvironment env;

	public AttributeControlSetterGetterGenerator(
		TypeElement adl, List<VariableElement> attributeFields,
		ProcessingEnvironment env ) {

		this.adl = adl;
		this.attributeFields = attributeFields;
		this.env = env;
	}

	@Override
	public int getTypeModifiers() {
		int mod = super.getTypeModifiers();
		Set<javax.lang.model.element.Modifier> modifiers = adl.getModifiers();
		if( modifiers.contains(javax.lang.model.element.Modifier.ABSTRACT) ) {
			mod |= Modifier.ABSTRACT;
		}
		return mod;
	}

	@Override
	public void generateMethods( ClassSourceCodeVisitor cv ) {

		for (VariableElement attributeField : attributeFields) {

			// Retrieve the value of the mode parameter in @Attribute
			// o is of class com.sun.tools.javac.code.Symbol$VarSymbol
			// toString() is a workaround to avoid having to deal with it
			Object o =
				AnnotatedConstructHelper.getAnnotationParamValue(
					attributeField, Attribute.class, "mode", env );
			String mode = o.toString();

			/*
			 * Generate the setter method.
			 */
			if( mode.equals(Access.READ_WRITE.toString()) ||
				mode.equals(Access.WRITE_ONLY.toString()) ) {

				String setterName = (String)
					AnnotatedConstructHelper.getAnnotationParamValue(
						attributeField, Attribute.class, "set", env );
				if( setterName.length() == 0 ) {
					String name =
						VariableElementHelper.getAttributeName(
							attributeField,env);
					setterName = "set"+name;
				}

				generateSetterMethod(cv,attributeField,setterName);
			}

			/*
			 * Generate the getter method.
			 */
			if( mode.equals(Access.READ_WRITE.toString()) ||
				mode.equals(Access.READ_ONLY.toString()) ) {

				String getterName = (String)
					AnnotatedConstructHelper.getAnnotationParamValue(
						attributeField, Attribute.class, "get", env );
				if( getterName.length() == 0 ) {
					String name =
						VariableElementHelper.getAttributeName(
							attributeField, env);
					getterName = "get"+name;
				}

				generateGetterMethod(cv,attributeField,getterName);
			}
		}
	}

	protected abstract void generateSetterMethod(
		ClassSourceCodeVisitor cv, VariableElement attributeField,
		String setterName );

	protected abstract void generateGetterMethod(
		ClassSourceCodeVisitor cv, VariableElement attributeField,
		String getterName );

	@Override
	public TypeElement getSourceType() {
		return adl;
	}
}
