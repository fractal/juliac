/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.core.desc;

import java.io.PrintWriter;
import java.util.List;

import javax.lang.model.element.TypeElement;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.juliac.commons.lang.ClassHelper;
import org.objectweb.fractal.juliac.core.Juliac;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;

/**
 * This class generates the source code of a component {@link
 * org.objectweb.fractal.api.factory.Factory} by visiting the model of an ADL
 * file. This class is specialized for the annotation processing tool of the
 * JDK.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public class ComponentDescFactoryClassGenerator
extends org.objectweb.fractal.juliac.core.desc.ComponentDescFactoryClassGenerator<TypeElement> {

	private TypeElement element;
	private boolean binderDefined = false;

	public ComponentDescFactoryClassGenerator(
		Juliac jc, ComponentDescAdlet<TypeElement> cdesc, String targetname,
		TypeElement element ) {

		super(jc,cdesc,targetname);
		this.element = element;
	}

	@Override
	protected void generateBindings( ComponentDesc<TypeElement> desc, PrintWriter pw ) {

		ComponentDescAdlet<TypeElement> cdesc =
			(ComponentDescAdlet<TypeElement>) desc;

		final List<String> bmns = cdesc.getBinderMethodNames();

		if( bmns.size() != 0 ) {

			if( !binderDefined ) {
				pw.print  (BinderImpl.class.getName());
				pw.println(" binder = null;");
				binderDefined = true;
			}

			final Object mm = cdesc.getMetamodelClass();
			for (String bmn : bmns) {
				pw.print  ("binder = new ");
				pw.print  (BinderImpl.class.getName());
				pw.println("();");
				pw.print  (bmn.replace('$','.'));
				pw.println("(binder);");
				pw.print  ("binder.bind( new java.util.HashMap<Class<?>,");
				pw.print  (Component.class.getName());
				pw.println(">() {{");
				generateAssociationForBinding(cdesc,mm.toString(),pw);
				pw.println(" }} );");
			}
		}

		// Iterate on subcomponents
		List<ComponentDesc<TypeElement>> subs = cdesc.getSubComponents();
		for (ComponentDesc<TypeElement> sub : subs) {
			generateBindings(sub,pw);
		}
	}

	private void generateAssociationForBinding(
		ComponentDesc<TypeElement> cdesc, String mmprefix, PrintWriter pw ) {

		final String id = cdesc.getID();

		pw.print  ("put(");
		pw.print  (mmprefix);
		pw.print  (".class,");
		pw.print  (id);
		pw.println(");");

		// Iterate on subcomponents
		List<ComponentDesc<TypeElement>> subs = cdesc.getSubComponents();
		for (ComponentDesc<TypeElement> sub : subs) {
			final String submmprefix = mmprefix+'.'+sub.getName();
			generateAssociationForBinding(sub,submmprefix,pw);
		}
	}

	@Override
	protected String getValueDelimiter(
		String type, String itfName, String setterName ) {

		String delimiter = ClassHelper.getValueDelimiterForType(type);
		return delimiter;
	}

	@Override
	public TypeElement getSourceType() {
		return element;
	}
}
