/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.core.desc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.juliac.adlet.api.BinderItf;
import org.objectweb.fractal.juliac.adlet.api.Provided;
import org.objectweb.fractal.juliac.adlet.api.Required;
import org.objectweb.fractal.juliac.core.desc.BindingType;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;
import org.objectweb.fractal.util.Fractal;

/**
 * This class defines bindings between required and provided component
 * interfaces.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public class BinderImpl implements BinderItf {

	/**
	 * Struct-like class for storing the description of a binding.
	 *
	 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
	 */
	private static class BindingDesc {

		private Class<?> source;
		private String sciname;
		private Class<?> target;
		private String tciname;

		private BindingDesc(
			Class<?> source, String sciname,
			Class<?> target, String tciname ) {

			this.source = source;
			this.sciname = sciname;
			this.target = target;
			this.tciname = tciname;
		}

		/** @since 2.8 */
		@Override
		public String toString() {
			return source.getName()+'.'+sciname+"->"+target.getName()+'.'+tciname;
		}
	}

	/**
	 * Normal bindings.
	 * @since 2.8
	 */
	private List<BindingDesc> normals = new ArrayList<>();

	/**
	 * Export bindings.
	 * @since 2.8
	 */
	private List<BindingDesc> exports = new ArrayList<>();

	/** Import bindings. */
	private List<BindingDesc> imports = new ArrayList<>();

	@Override
	public <RTYPE, PTYPE extends RTYPE, PARENT, RCTGY, PCTGY extends RCTGY>
	void normal( Required<RTYPE,PARENT,?,?,RCTGY> r, Provided<PTYPE,PARENT,?,?,PCTGY> p ) {

		BindingDesc bd =
			new BindingDesc(
				r.getComponent(), r.getInterfaceName(),
				p.getComponent(), p.getInterfaceName() );
		normals.add(bd);
	}

	@Override
	public <RTYPE, PTYPE extends RTYPE, RCURRENT, PBOTTOM extends RCURRENT, RCTGY, PCTGY extends RCTGY>
	void export(
		Provided<RTYPE,?,RCURRENT,?,RCTGY> r, Provided<PTYPE,?,?,PBOTTOM,PCTGY> p ) {

		BindingDesc bd =
			new BindingDesc(
				r.getComponent(), r.getInterfaceName(),
				p.getComponent(), p.getInterfaceName() );
		exports.add(bd);
	}

	@Override
	public <RTYPE, PTYPE extends RTYPE, RBOTTOM extends PCURRENT, PCURRENT, RCTGY, PCTGY extends RCTGY>
	void impor(
		Required<RTYPE,?,?,RBOTTOM,RCTGY> r, Required<PTYPE,?,PCURRENT,?,PCTGY> p ) {

		BindingDesc bd =
			new BindingDesc(
				r.getComponent(), r.getInterfaceName(),
				p.getComponent(), p.getInterfaceName() );
		imports.add(bd);
	}

	/**
	 * Bind the components according to the recorded bindings. This method is
	 * invoked by a component factory.
	 *
	 * @param mapping
	 *         the associations between component classes at the metalevel and
	 *        corresponding component instances
	 */
	public void bind( Map<Class<?>,Component> mapping )
	throws
		NoSuchInterfaceException, IllegalBindingException,
		IllegalLifeCycleException {

		// Normal bindings
		for (BindingDesc binding : normals) {
			Component src = mapping.get(binding.source);
			Component target = mapping.get(binding.target);
			BindingController bc = Fractal.getBindingController(src);
			Object targetItf = target.getFcInterface(binding.tciname);
			bc.bindFc(binding.sciname,targetItf);
		}

		// Export bindings
		for (BindingDesc binding : exports) {
			Component src = mapping.get(binding.source);
			Component target = mapping.get(binding.target);
			BindingController bc = Fractal.getBindingController(src);
			Object targetItf = target.getFcInterface(binding.tciname);
			bc.bindFc(binding.sciname,targetItf);
		}

		// Import bindings
		for (BindingDesc binding : imports) {
			Component src = mapping.get(binding.source);
			Component target = mapping.get(binding.target);
			BindingController bc = Fractal.getBindingController(src);
			ContentController cc = Fractal.getContentController(target);
			Object targetItf = cc.getFcInternalInterface(binding.tciname);
			bc.bindFc(binding.sciname,targetItf);
		}
	}

	/**
	 * Record the bindings defined in mapping.
	 *
	 * @param mapping
	 *        the associations between component classes at the metalevel and
	 *        corresponding component descriptors.
	 * @since 2.8
	 */
	public void record( Map<Class<?>,ComponentDesc<?>> mapping ) {

		record(mapping,normals,BindingType.NORMAL);
		record(mapping,exports,BindingType.EXPORT);
		record(mapping,imports,BindingType.IMPORT);
	}

	/**
	 * @since 2.8
	 */
	private void record(
		Map<Class<?>,ComponentDesc<?>> mapping,
		List<BindingDesc> bindings, BindingType type ) {

		for (BindingDesc binding : bindings) {
			ComponentDesc<?> src = mapping.get(binding.source);
			ComponentDesc<?> target = mapping.get(binding.target);
			assert src!=null : "Unexpected null maping for "+binding.source.getName();
			assert target!=null : "Unexpected null maping for "+binding.target.getName();
			String cltItfName = binding.sciname;
			String srvItfName = binding.tciname;
			org.objectweb.fractal.juliac.core.desc.BindingDesc bdesc =
				new org.objectweb.fractal.juliac.core.desc.BindingDesc(
					type, src, cltItfName, target, srvItfName);
			src.putBinding(cltItfName,bdesc);
		}
	}

}
