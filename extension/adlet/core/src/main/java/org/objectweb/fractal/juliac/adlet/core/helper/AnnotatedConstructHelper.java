/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.core.helper;

import java.lang.annotation.Annotation;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.AnnotatedConstruct;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

import org.objectweb.fractal.juliac.commons.lang.ClassHelper;

/**
 * This class provides helper methods for the {@link AnnotatedConstruct}
 * interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public class AnnotatedConstructHelper {

	/**
	 * Return the value of the parameter associated with the specified
	 * annotation type on the specified construct. Return <code>null</code> if
	 * no such parameter exists.
	 *
	 * @param construct       the annotated construct
	 * @param annotationType  the annotation type
	 * @param name            the name of the parameter
	 * @param env             the processing environment
	 * @return                the value of the parameter
	 */
	public static Object getAnnotationParamValue(
		AnnotatedConstruct construct,
		Class<? extends Annotation> annotationType, String name,
		ProcessingEnvironment env ) {

		Elements elements = env.getElementUtils();
		Types types = env.getTypeUtils();

		TypeMirror atm = ClassHelper.getTypeMirror(annotationType,elements);

		List<? extends AnnotationMirror> ams = construct.getAnnotationMirrors();
		for (AnnotationMirror am : ams) {
			TypeMirror tm = am.getAnnotationType();
			boolean isSameType = types.isSameType(tm,atm);
			if(isSameType) {
				Object value =
					AnnotationMirrorHelper.getParameterValue(am,elements,name);
				return value;
			}
		}

		return null;
	}
}
