/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.core.proxy;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;

import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.CatchSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;

/**
 * This class generates attribute controller implementations for Fraclet
 * annotated components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class AttributeControlImplementationGenerator
extends AttributeControlSetterGetterGenerator {

	private String name;
	private String contentClassName;
	private String attributeControlInterfaceName;

	public AttributeControlImplementationGenerator(
		TypeElement adl, String name, List<VariableElement> attributeFields,
		String contentClassName, String attributeControlInterfaceName,
		ProcessingEnvironment env ) {

		super(adl,attributeFields,env);

		this.name = name;
		this.contentClassName = contentClassName;
		this.attributeControlInterfaceName = attributeControlInterfaceName;
	}

	public String getTargetTypeName() {
		return name + "AttributesImpl";
	}

	@Override
	public String getSuperClassName() { return contentClassName; }

	@Override
	public String[] getImplementedInterfaceNames() {
		return new String[]{attributeControlInterfaceName};
	}

	protected void generateSetterMethod(
		ClassSourceCodeVisitor cv, VariableElement attributeField,
		String setterName ) {

		TypeMirror type = attributeField.asType();
		String typeName = type.toString();
		Set<javax.lang.model.element.Modifier> modifiers =
			attributeField.getModifiers();
		String attributeFieldName = attributeField.getSimpleName().toString();

		BlockSourceCodeVisitor bv =
			cv.visitMethod(
				Modifier.PUBLIC, null, "void", setterName,
				new String[]{typeName+" value"},null);

		if( modifiers.contains(javax.lang.model.element.Modifier.PRIVATE) ) {
			// Use reflection to set private fields
			CatchSourceCodeVisitor catchv = bv.visitTry();
			Element element = attributeField.getEnclosingElement();
			TypeElement declaringClass = (TypeElement) element;
			String declaringClassName =
				declaringClass.getQualifiedName().toString();
			catchv.visitVar(
				Field.class.getName(),"f",
				declaringClassName+
				".class.getDeclaredField(\""+attributeFieldName+"\")" );
			catchv.visitIns("f.setAccessible(true)");
			catchv.visitIns("f.set(this,value)");
			catchv = catchv.visitCatch(Exception.class,"e");
			catchv.visitIns(
				"throw new "+RuntimeException.class.getName()+"(e)" );
			catchv.visitEnd();
		}
		else {
			bv.visitSet("this."+attributeFieldName,"value");
		}
		bv.visitEnd();
	}

	protected void generateGetterMethod(
		ClassSourceCodeVisitor cv, VariableElement attributeField,
		String getterName ) {

		TypeMirror type = attributeField.asType();
		String typeName = type.toString();
		Set<javax.lang.model.element.Modifier> modifiers =
			attributeField.getModifiers();
		String attributeFieldName = attributeField.getSimpleName().toString();

		BlockSourceCodeVisitor bv =
			cv.visitMethod(
				Modifier.PUBLIC, null, typeName, getterName, null, null );
		if( modifiers.contains(javax.lang.model.element.Modifier.PRIVATE) ) {
			// Use reflection to set private fields
			CatchSourceCodeVisitor catchv = bv.visitTry();
			Element element = attributeField.getEnclosingElement();
			TypeElement declaringClass = (TypeElement) element;
			String declaringClassName =
				declaringClass.getQualifiedName().toString();
			catchv.visitVar(
				Field.class.getName(),"f",
				declaringClassName+
				".class.getDeclaredField(\""+attributeFieldName+"\")" );
			catchv.visitIns("f.setAccessible(true)");
			catchv.visitIns("return ("+typeName+") f.get(this)");
			catchv = catchv.visitCatch(Exception.class,"e");
			catchv.visitIns(
				"throw new "+RuntimeException.class.getName()+"(e)" );
			catchv.visitEnd();
		}
		else {
			bv.visitIns("return","this."+attributeFieldName);
		}
		bv.visitEnd();
	}

	@Override
	public TypeElement getSourceType() {
		return adl;
	}
}
