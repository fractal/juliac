/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.core.helper;

import java.util.HashMap;
import java.util.Map;

import javax.lang.model.type.TypeMirror;

/**
 * This class provides helper methods for the {@link TypeMirror} interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public class TypeMirrorHelper {

	private static final Map<String,String> nulvalues =
		new HashMap<>() {
			private static final long serialVersionUID = 3375192572131629836L;
		{
			put("boolean","false");
			put("char"," ");
			put("byte","(byte)0");
			put("short","(short)0");
			put("int","0");
			put("long","0");
			put("float","0.0f");
			put("double","0.0");
		}};

	/**
	 * Return the string representation of the null value associated to the
	 * specified type mirror.
	 */
	public static String getNullValue( TypeMirror tm ) {
		String s = tm.toString();
		if( nulvalues.containsKey(s) ) {
			String ret = nulvalues.get(s);
			return ret;
		}
		return "null";
	}
}
