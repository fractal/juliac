/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.core.helper;

import java.util.List;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;

/**
 * This class provides helper methods for the {@link ExecutableElement}
 * interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public class ExecutableElementHelper {

	/**
	 * Return the specified getter method in the specified list of methods.
	 * Return <code>null</code> if no such getter method exists.
	 *
	 * @param methods  the methods
	 * @param name     the name of the getter method
	 * @param tm       the type associated with the getter
	 * @param types    the type utility
	 * @return         the corresponding getter method or <code>null</code>
	 */
	public static ExecutableElement getGetterMethod(
		List<ExecutableElement> methods, String name, TypeMirror tm,
		Types types ) {

		for (ExecutableElement method : methods) {
			String methodname = method.getSimpleName().toString();
			if( methodname.equals(name) ) {
				List<? extends VariableElement> params = method.getParameters();
				if( params.size() == 0 ) {
					TypeMirror ret = method.getReturnType();
					boolean isSameType = types.isSameType(tm,ret);
					if( isSameType ) {
						return method;
					}
				}
			}
		}

		return null;
	}

	/**
	 * Return the specified setter method in the specified list of methods.
	 * Return <code>null</code> if no such getter method exists.
	 *
	 * @param methods  the methods
	 * @param name     the name of the setter method
	 * @param tm       the type associated with the setter
	 * @param types    the type utility
	 * @return         the corresponding setter method or <code>null</code>
	 */
	public static ExecutableElement getSetterMethod(
		List<ExecutableElement> methods, String name, TypeMirror tm,
		Types types ) {

		for (ExecutableElement method : methods) {
			String methodname = method.getSimpleName().toString();
			if( methodname.equals(name) ) {
				List<? extends VariableElement> params = method.getParameters();
				if( params.size() == 1 ) {
					TypeMirror param = params.get(0).asType();
					boolean isSameType = types.isSameType(tm,param);
					if( isSameType ) {
						TypeMirror ret = method.getReturnType();
						if( ret.toString().equals("void") ) {
							return method;
						}
					}
				}
			}
		}

		return null;
	}
}
