/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.core.helper;

import java.util.Map;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.util.Elements;

/**
 * This class provides helper methods for the {@link AnnotationMirror}
 * interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public class AnnotationMirrorHelper {

	/**
	 * Return the value of the specified parameter associated with the specified
	 * annotation mirror.
	 *
	 * @param am        the annotation mirror
	 * @param elements  the elements
	 * @param name      the name of the parameter
	 * @return          the value of the parameter or
	 *                     <code>null</code> if no such parameter exists
	 */
	public static Object getParameterValue(
		AnnotationMirror am, Elements elements, String name ) {

		Map<? extends ExecutableElement, ? extends AnnotationValue>
		defaults =
			elements.getElementValuesWithDefaults(am);
		for (ExecutableElement ee : defaults.keySet()) {
			String simpleName = ee.getSimpleName().toString();
			if( simpleName.equals(name) ) {
				AnnotationValue annotationValue = defaults.get(ee);
				Object value = annotationValue.getValue();
				return value;
			}
		}

		return null;
	}
}
