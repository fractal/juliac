/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.adlet.core.proxy;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.Interceptor;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.proxy.SimpleSourceCodeGeneratorInterceptionType;
import org.objectweb.fractal.juliac.core.proxy.SimpleSourceCodeGeneratorMode;

/**
 * This class provides a simple framework for generating the source code of
 * component interceptors. The API provided by this class is similar to the one
 * provided by {@link org.objectweb.fractal.julia.asm.SimpleCodeGenerator}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 */
public abstract class SimpleSourceCodeGenerator
extends AbstractInterceptorSourceCodeGenerator {

	// -----------------------------------------------------------------------
	// Methods which must be implemented by subclasses
	// -----------------------------------------------------------------------

	/**
	 * Returns the type of this code generator.
	 */
	protected abstract SimpleSourceCodeGeneratorMode getMode();

	/**
	 * Returns the name of the interface provided by the controller object to be
	 * associated to the interceptor.
	 */
	protected abstract String getControllerInterfaceName();

	/**
	 * Returns the type of the interception code to be generated for the given
	 * method.
	 */
	protected abstract SimpleSourceCodeGeneratorInterceptionType
		getInterceptionType( ExecutableElement m );

	/**
	 * Returns the type of the interception code to be generated for the given
	 * method.
	 */
	protected abstract SimpleSourceCodeGeneratorInterceptionType
		getInterceptionType( Method m );

	/**
	 * @return the name of the method to be called on the associated controller
	 *      object, before each intercepted call.
	 */
	protected abstract String getPreMethodName();

	/**
	 * Returns the name of the method to be called on the associated controller
	 * object, after each intercepted call.
	 */
	protected abstract String getPostMethodName();

	/**
	 * Returns the type of the context to be passed from the "pre method" to the
	 * "post method".
	 */
	protected abstract Class<?> getContextType();

	/**
	 * Returns the String to be passed as argument to the pre and post methods,
	 * for the given method.
	 */
	protected abstract String getMethodName( ExecutableElement m );


	// -----------------------------------------------------------------------
	// Methods for checking the arguments
	// -----------------------------------------------------------------------

	/**
	 * Check that the arguments given to this source code generator are legal.
	 *
	 * @thows IllegalArgumentException  if an argument is not legal
	 */
	private void checkArguments() {

		Class<?> ctrlImpl = getCtrlImpl();

		/*
		 * Check that the control interface provides a 'preMethodName' method.
		 */
		Class<?> contextType = getContextType();
		Method preMethod = null;
		String preMethodName = getPreMethodName();
		try {
			preMethod = ctrlImpl.getMethod(preMethodName,String.class);
		}
		catch( NoSuchMethodException nsme ) {
			final String msg =
				"No such method "+preMethodName+"(String) in "+
				ctrlImpl.getName();
			throw new IllegalArgumentException(msg);
		}
		if( ! contextType.isAssignableFrom(preMethod.getReturnType()) ) {
			final String msg =
				preMethodName+"(String) in "+ctrlImpl.getName()+
				" should return "+contextType.getName()+" instead of "+
				preMethod.getReturnType().getName();
			throw new IllegalArgumentException(msg);
		}

		/*
		 * Check whether 'postMethodName' must be checked.
		 */
		Method[] methods = ctrlImpl.getMethods();
		boolean checkPost = false;
		for (Method method : methods) {
			SimpleSourceCodeGeneratorInterceptionType interceptionType =
				getInterceptionType(method);
			if( interceptionType==SimpleSourceCodeGeneratorInterceptionType.NORMAL ||
				interceptionType==SimpleSourceCodeGeneratorInterceptionType.FINALLY) {
				checkPost = true;
			}
		}

		if(checkPost) {

			/*
			 * Check that the value returned by getPostMethodName() is non
			 * empty.
			 */
			String postMethodName = getPostMethodName();
			if( postMethodName==null || postMethodName.length()==0 ) {
				final String msg = "postMethodName should be non empty";
				throw new IllegalArgumentException(msg);
			}

			/*
			 * Check that the control interface provides a 'postMethodName'
			 * method.
			 */
			if( contextType == void.class ) {
				try {
					ctrlImpl.getMethod(postMethodName,String.class);
				}
				catch( NoSuchMethodException nsme ) {
					final String msg =
						"No such method "+postMethodName+"(String) in "+
						ctrlImpl.getName();
					throw new IllegalArgumentException(msg);
				}
			}
			else {
				try {
					ctrlImpl.getMethod(postMethodName,String.class,contextType);
				}
				catch( NoSuchMethodException nsme ) {
					final String msg =
						"No such method "+postMethodName+"(String,"+
						contextType.getName()+") in "+ctrlImpl.getName();
					throw new IllegalArgumentException(msg);
				}
			}
		}
		else {
			String postMethodName = getPostMethodName();
			if( postMethodName != null ) {
				final String msg =
					"A postMethodName ("+postMethodName+
					") has been defined but will never be invoked as the "+
					"interceptionType is EMPTY";
				throw new IllegalArgumentException(msg);
			}
		}
	}

	/**
	 * Return the class which implements the controller associated with the
	 * interceptor generated by this generator.
	 *
	 * @throws IllegalArgumentException
	 * 		if the interface name is missing in the controller descriptor
	 */
	protected Class<?> getCtrlImpl() {

		/*
		 * Check that a control interface with a name equals to the value
		 * returned by getControllerInterfaceName() exists.
		 */
		String name = getControllerInterfaceName();
		InterfaceType ctrlItfType = null;
		InterfaceType[] its = membraneDesc.getCtrlItfTypes();
		for (InterfaceType it : its) {
			String itname = it.getFcItfName();
			if( itname.equals(name) ) {
				ctrlItfType = it;
			}
		}
		if( ctrlItfType == null ) {
			final String msg =
				"No such interface "+name+" in controller descriptor "+
				membraneDesc.getDescriptor();
			throw new IllegalArgumentException(msg);
		}

		/*
		 * Retrieve the name of the controller class which implements the
		 * ctrlItfType control interface.
		 */
		String signature = ctrlItfType.getFcItfSignature();
		Class<?> ctrlImpl = membraneDesc.getCtrlImpl(signature);
		return ctrlImpl;
	}


	// -----------------------------------------------------------------------
	// Constructors and methods
	// -----------------------------------------------------------------------

	public SimpleSourceCodeGenerator() {
		super();
	}

	public SimpleSourceCodeGenerator(
		InterfaceType it, TypeElement proxycl, String pkgRoot,
		MembraneDesc<?> membraneImpl, boolean mergeable ) {

		super(it,proxycl,pkgRoot,membraneImpl,mergeable);
	}

	/**
	 * Return true if some code must be generated by the current source file
	 * generator.
	 */
	public boolean match() {

		// Skip control interfaces
		String itname = it.getFcItfName();
		if( itname.endsWith("-controller") || itname.equals("component") ) {
			return false;
		}

		// Analyze the mode
		SimpleSourceCodeGeneratorMode mode = getMode();
		boolean match = mode.match(it);
		return match;
	}

	@Override
	public void generateFields( ClassSourceCodeVisitor cv ) {

		/*
		 * Check that the value returned by getPreMethodName() is non empty.
		 * This has to be checked in all cases which is not the case for
		 * 'postMethodName' which can be skipped if the interceptionType is
		 * EMPTY.
		 */
		String preMethodName = getPreMethodName();
		if( preMethodName==null || preMethodName.length()==0 ) {
			final String msg = "preMethodName should be non empty";
			throw new IllegalArgumentException(msg);
		}

		if( ! mergeable ) {
			/*
			 * The checks are relevant only if the membrane implementation is
			 * available which is not the case in the merge mode. In this mode,
			 * the interception layer is generated prior to the generation of
			 * the whole membrane (merged) implementation.
			 */
			checkArguments();
			Class<?> ctrlImpl = getCtrlImpl();
			cv.visitField(Modifier.PRIVATE, ctrlImpl.getName(), "_ctrl", null);
		}
	}

	@Override
	public void generateMethodInitFcController( BlockSourceCodeVisitor mv ) {

		if( ! mergeable ) {

			Class<?> ctrlImpl = getCtrlImpl();

			mv.visitVar("Object","octrl","ic.getInterface(\""+getControllerInterfaceName()+"\")");
			BlockSourceCodeVisitor then = mv.visitIf("!(","octrl","instanceof",ctrlImpl.getName(),")");
			BlockSourceCodeVisitor wdo = then.visitWhile("octrl","instanceof",Interceptor.class.getName());
			wdo.visitSet("octrl","(("+Interceptor.class.getName()+")octrl).getFcItfDelegate()").visitEnd();
			then.visitEnd();

			// Record the reference to the instance implementing the controller
			mv.visitSet("_ctrl","("+ctrlImpl.getName()+")","octrl");
		}
	}

	@Override
	public void generateMethodClone( BlockSourceCodeVisitor mv ) {
		mv.visitSet("clone._ctrl","_ctrl");
	}

	@Override
	public void generateMethodOthers( ClassSourceCodeVisitor cv ) {

		if( mergeable ) {

			Class<?> contextType = getContextType();
			String preMethodName = getPreMethodName();
			String postMethodName = getPostMethodName();

			/*
			 * Declare an abstract _this_ method to invoke 'preMethodName'.
			 */
			BlockSourceCodeVisitor mv =
				cv.visitMethod(
					Modifier.PUBLIC + Modifier.ABSTRACT, null,
					contextType.getName(), "_this_"+preMethodName,
					new String[]{String.class.getName()+" arg0"}, null );
			mv.visitEnd();

			/*
			 * Declare an abstract _this_ method to invoke 'postMethodName'.
			 */
			if( postMethodName!=null && postMethodName.length()!=0 ) {
				String[] args =
					contextType == void.class ?
					new String[]{String.class.getName()+" arg0"} :
					new String[]{String.class.getName()+" arg0",contextType.getName()+" arg1"};
				mv =
					cv.visitMethod(
						Modifier.PUBLIC + Modifier.ABSTRACT, null,
						"void", "_this_"+postMethodName, args, null );
				mv.visitEnd();
			}
		}
	}

	@Override
	public void generateProxyMethodBodyBeforeCode(
		BlockSourceCodeVisitor mv, ExecutableElement proxym ) {

		Class<?> contextType = getContextType();
		SimpleSourceCodeGeneratorInterceptionType interceptionType =
			getInterceptionType(proxym);
		String preMethodName = getPreMethodName();

		String line = "";
		if( contextType != void.class ) {
			line += contextType.getName() + " ctx = ";
		}
		line += mergeable?"_this_":"_ctrl.";
		mv.visitIns(line+preMethodName+"(\""+getMethodName(proxym)+"\")");
		if( interceptionType == SimpleSourceCodeGeneratorInterceptionType.FINALLY ) {
			mv.visitln("try {");
		}
	}

	@Override
	public void generateProxyMethodBodyAfterCode(
		BlockSourceCodeVisitor mv, ExecutableElement proxym ) {

		SimpleSourceCodeGeneratorInterceptionType interceptionType =
			getInterceptionType(proxym);

		if( interceptionType == SimpleSourceCodeGeneratorInterceptionType.NORMAL ) {

			Class<?> contextType = getContextType();
			String postMethodName = getPostMethodName();

			/*
			 * Check that postMethodName is non empty. This test couldn't have
			 * been done before as it may be legal that postMethodName be empty
			 * for the EMPTY interception type.
			 */

			if( postMethodName==null || postMethodName.length()==0 ) {
				final String msg = "postMethodName should be non empty";
				throw new IllegalArgumentException(msg);
			}

			String line = mergeable?"_this_":"_ctrl.";
			line+=postMethodName+"(\""+getMethodName(proxym)+"\"";
			if( contextType != void.class ) {
				line+=",ctx";
			}
			mv.visitIns(line+")");
		}
	}

	@Override
	public void generateProxyMethodBodyAfterReturningCode(
		BlockSourceCodeVisitor mv, ExecutableElement proxym ) {

		SimpleSourceCodeGeneratorInterceptionType interceptionType =
			getInterceptionType(proxym);

		if( interceptionType==SimpleSourceCodeGeneratorInterceptionType.FINALLY ) {

			Class<?> contextType = getContextType();
			String postMethodName = getPostMethodName();

			/*
			 * Check that postMethodName is non empty. This test couldn't have
			 * been done before as it may be legal that postMethodName be empty
			 * for the EMPTY interception type.
			 */

			if( postMethodName==null || postMethodName.length()==0 ) {
				final String msg = "postMethodName should be non empty";
				throw new IllegalArgumentException(msg);
			}

			mv.visitln("    } finally {");
			String line = mergeable ? "_this_":"_ctrl.";
			line += postMethodName+"(\""+getMethodName(proxym)+"\"";
			if( contextType != void.class ) {
				line += ",ctx";
			}
			mv.visitIns(line+")");
			mv.visitln("    }");
		}
	}
}
