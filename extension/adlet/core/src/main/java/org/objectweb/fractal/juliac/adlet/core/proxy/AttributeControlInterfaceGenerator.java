/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.core.proxy;

import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;

import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.types.Access;
import org.objectweb.fractal.juliac.adlet.core.helper.AnnotatedConstructHelper;
import org.objectweb.fractal.juliac.adlet.core.helper.VariableElementHelper;
import org.objectweb.fractal.juliac.api.visit.InterfaceSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.opt.InterfaceGenerator;

/**
 * This class generates attribute control interfaces for Fraclet annotated
 * components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class AttributeControlInterfaceGenerator extends InterfaceGenerator {

	private TypeElement adl;
	private String name;
	private List<VariableElement> attributeFields;
	private ProcessingEnvironment env;

	public AttributeControlInterfaceGenerator(
		TypeElement adl, String name, List<VariableElement> attributeFields,
		ProcessingEnvironment env ) {

		this.adl = adl;
		this.name = name;
		this.attributeFields = attributeFields;
		this.env = env;
	}

	public String getTargetTypeName() {
		return name + "AttributesItf";
	}

	@Override
	protected String[] getExtendedInterfaceNames() {
		return new String[]{AttributeController.class.getName()};
	}

	@Override
	protected void generateMethods( InterfaceSourceCodeVisitor iv ) {

		for (VariableElement attributeField : attributeFields) {

			// Retrieve the type of the attribute
			TypeMirror tm = attributeField.asType();
			String typeName = tm.toString();

			// Retrieve the value of the mode parameter in @Attribute
			// o is of class com.sun.tools.javac.code.Symbol$VarSymbol
			// toString() is a workaround to avoid having to deal with it
			Object o =
				AnnotatedConstructHelper.getAnnotationParamValue(
					attributeField, Attribute.class, "mode", env );
			String mode = o.toString();

			/*
			 * Generate the setter method.
			 */
			if( mode.equals(Access.READ_WRITE.toString()) ||
				mode.equals(Access.WRITE_ONLY.toString()) ) {

				String setterName = (String)
					AnnotatedConstructHelper.getAnnotationParamValue(
						attributeField, Attribute.class, "set", env );
				if( setterName.length() == 0 ) {
					String name =
						VariableElementHelper.getAttributeName(
							attributeField,env);
					setterName = "set"+name;
				}

				iv.visitMethod(
					null,"void",setterName,new String[]{typeName+" value"},
					null);
			}

			/*
			 * Generate the getter method.
			 */
			if( mode.equals(Access.READ_WRITE.toString()) ||
				mode.equals(Access.READ_ONLY.toString()) ) {

				String getterName = (String)
					AnnotatedConstructHelper.getAnnotationParamValue(
						attributeField, Attribute.class, "get", env );
				if( getterName.length() == 0 ) {
					String name =
						VariableElementHelper.getAttributeName(
							attributeField,env);
					getterName = "get"+name;
				}

				iv.visitMethod(null,typeName,getterName,null,null);
			}
		}
	}

	@Override
	public TypeElement getSourceType() {
		return adl;
	}
}
