/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.core;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;

import org.objectweb.fractal.juliac.api.JuliacRuntimeException;
import org.objectweb.fractal.juliac.api.SpoonSupportItf;
import org.objectweb.fractal.juliac.core.CmdLineOptions;
import org.objectweb.fractal.juliac.core.Juliac;

/**
 * This class implements the code shared by all processors that use Juliac.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public abstract class AbstractJuliacProcessor extends AbstractProcessor {

	protected JuliacAP jc;

	/**
	 * Initialize {@link Juliac}.
	 *
	 * @param processingEnv
	 *         the context information provided by the annotation processing
	 *         environment
	 */
	protected void initJuliac( ProcessingEnvironment processingEnv ) {

		/*
		 * Initialize a properly configured instance of Juliac.
		 */
		jc = new JuliacAP();
		jc.setProcessingEnvironment(processingEnv);

		Map<String,String> options = processingEnv.getOptions();

		/*
		 * Retrieve options and flags.
		 * Assume default values when missing.
		 */
		String key = "juliac."+CmdLineOptions.SRCS.name().toLowerCase();
		String s = options.get(key);
		String[] srcs = s==null ? new String[0] : s.split(File.pathSeparator);
		try {
			jc.addSrcs(srcs);
		}
		catch( IOException ioe ) {
			throw new JuliacRuntimeException(ioe);
		}

		key = "juliac."+CmdLineOptions.MIXINS.name().toLowerCase();
		s = options.get(key);
		String[] srclibs = s==null ? new String[0] : s.split(File.pathSeparator);
		jc.addSrclibs(srclibs);

		/*
		 * For processors that use Spoon.
		 */
		try {
			SpoonSupportItf spoon = jc.lookupFirst(SpoonSupportItf.class);
			ClassLoader classLoader = getClass().getClassLoader();
			spoon.setSpoonInputClassLoader(classLoader);
		}
		catch( NoSuchElementException jre ) {
			// Spoon is not configured as a module to be loaded. Do nothing.
		}
	}

	/**
	 * Close {@link Juliac}.
	 */
	protected void closeJuliac() {
		jc.unregisterAll();
	}
}
