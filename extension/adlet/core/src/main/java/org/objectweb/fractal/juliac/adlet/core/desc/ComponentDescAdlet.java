/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.core.desc;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;

/**
 * This class extends the default Juliac
 * {@link org.objectweb.fractal.juliac.core.desc.ComponentDesc} class to enable
 * storing the definition of
 * @{@link org.objectweb.fractal.juliac.adlet.Binding}-annotated methods
 * associated with a component descriptor.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public class ComponentDescAdlet<CT> extends ComponentDesc<CT> {

	/**
	 * Store the fully-qualified class and method names of
	 * @{@link org.objectweb.fractal.juliac.adlet.Binding}-annotated methods.
	 * The signature of the methods are expected to be
	 * <code>static (Binder): void</code>.
	 */
	private List<String> methods = new ArrayList<>();

	/**
	 * The metamodel class for the current component descriptor.
	 */
	private Object mm;

	/**
	 * The class of the membrane associated to the current component.
	 *
	 * @since 2.8
	 */
	private Class<?> membraneClass;

	public ComponentDescAdlet(
		String id, String name, String definition, ComponentType ct,
		String ctrlDesc, String contentClassName, CT source, Object mm ) {

		super(id,name,definition,ct,ctrlDesc,contentClassName,source);
		this.mm = mm;
	}

	public void addBinderMethodName( String name ) { methods.add(name); }
	public List<String> getBinderMethodNames() { return methods; }

	public Object getMetamodelClass() { return mm; }

	/** @since 2.8 */
	public void setMembraneClass( Class<?> membraneClass ) { this.membraneClass = membraneClass; }

	/** @since 2.8 */
	public Class<?> getMembraneClass() { return membraneClass; }
}
