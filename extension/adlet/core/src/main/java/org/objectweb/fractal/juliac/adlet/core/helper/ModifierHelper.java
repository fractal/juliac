/***
 * Juliac
 * Copyright (C) 2019-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.core.helper;

import java.util.HashMap;
import java.util.Map;

import javax.lang.model.element.Modifier;

/**
 * This class provides helper methods for the {@link Modifier} enum.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public class ModifierHelper {

	/**
	 * Return the {@link java.lang.reflect.Modifier} corresponding to the
	 * specified {@link Modifier}.

	 * @throws IllegalArgumentException
	 * 		if the specified {@link Modifier} does not exist
	 */
	public static int toReflectModifier( Modifier m ) {

		if( ! map.containsKey(m) ) {
			throw new IllegalArgumentException("No such modifier: "+m);
		}

		int i = map.get(m);
		return i;
	}

	private static final Map<Modifier,Integer> map =
		new HashMap<>() {
			private static final long serialVersionUID = -7458450325492536253L;
		{
			put(Modifier.ABSTRACT, java.lang.reflect.Modifier.ABSTRACT);
			put(Modifier.DEFAULT, 0);
			put(Modifier.FINAL, java.lang.reflect.Modifier.FINAL);
			put(Modifier.NATIVE, java.lang.reflect.Modifier.NATIVE);
			put(Modifier.PRIVATE, java.lang.reflect.Modifier.PRIVATE);
			put(Modifier.PROTECTED, java.lang.reflect.Modifier.PROTECTED);
			put(Modifier.PUBLIC, java.lang.reflect.Modifier.PUBLIC);
			put(Modifier.STATIC, java.lang.reflect.Modifier.STATIC);
			put(Modifier.STRICTFP, java.lang.reflect.Modifier.STRICT);
			put(Modifier.SYNCHRONIZED, java.lang.reflect.Modifier.SYNCHRONIZED);
			put(Modifier.TRANSIENT, java.lang.reflect.Modifier.TRANSIENT);
			put(Modifier.VOLATILE, java.lang.reflect.Modifier.VOLATILE);
		}};
}
