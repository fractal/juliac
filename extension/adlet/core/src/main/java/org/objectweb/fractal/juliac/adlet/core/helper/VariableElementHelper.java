/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.core.helper;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.VariableElement;

import org.objectweb.fractal.fraclet.annotations.Attribute;

/**
 * This class provides helper methods for the {@link VariableElement} interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public class VariableElementHelper {

	/**
	 * Return the name of the attribute corresponding to the specified
	 * @{@link Attribute} annotated field. The name is either the value of the
	 * name parameter of the @{@link Attribute} annotation if specified, or the
	 * name of the field if not. The first letter of the returned name is
	 * uppercased to follow the naming convention in getter and setter methods.
	 *
	 * @param attributeField  the @{@link Attribute} annotated field
	 * @param env             the corresponding processing environment
	 * @return                the name of the attribute
	 */
	public static String getAttributeName(
		VariableElement attributeField, ProcessingEnvironment env ) {

		// Retrieve the value of the name parameter in @Attribute
		String name = (String)
			AnnotatedConstructHelper.getAnnotationParamValue(
				attributeField, Attribute.class, "name", env );

		// If no name is provided, use the name of the field
		if( name.length() == 0 ) {
			name = attributeField.getSimpleName().toString();
		}

		// Upper case the first letter of the attribute name
		name = name.substring(0,1).toUpperCase() + name.substring(1);

		return name;
	}
}
