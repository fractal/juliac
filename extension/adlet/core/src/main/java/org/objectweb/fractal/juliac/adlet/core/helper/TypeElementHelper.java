/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.core.helper;

import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;

/**
 * This class provides helper methods for the {@link TypeElement} interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public class TypeElementHelper {

	/**
	 * Add to the specified list, all members declared by the specified type
	 * element and by parent elements, except {@link java.lang.Object}. All
	 * members are included whatever their access modifier is.
	 */
	public static void addAllMembers( TypeElement cl, List<Element> list ) {

		// Stop when java.lang.Object is reached
		if( cl.toString().equals(Object.class.getName()) ) {
			return;
		}

		// Declared methods
		List<? extends Element> enclosedElements = cl.getEnclosedElements();
		list.addAll(enclosedElements);

		// Recurse in the parent class
		TypeMirror tm = cl.getSuperclass();
		if( tm.getKind() == TypeKind.NONE ) {
			// Kind is TypeKind.NONE for interfaces
			List<? extends TypeMirror> itfs = cl.getInterfaces();
			for (TypeMirror itftm : itfs) {
				DeclaredType dt = (DeclaredType) itftm;
				TypeElement itfte = (TypeElement) dt.asElement();
				addAllMembers(itfte,list);
			}
		}
		else {
			DeclaredType dt = (DeclaredType) cl.getSuperclass();
			TypeElement supercl = (TypeElement) dt.asElement();
			addAllMembers(supercl,list);
		}
	}

	/**
	 * Return all members declared by the specified type element and by parent
	 * elements, except {@link java.lang.Object}. All members are included
	 * whatever their access modifier is.
	 *
	 * This method is a workaround for {@link
	 * javax.lang.model.util.Elements#getAllMembers(javax.lang.model.element.TypeElement)}
	 * that does not return private elements of parent elements.
	 */
	public static List<? extends Element> getAllMembers( TypeElement cl ) {
		List<Element> list = new ArrayList<>();
		addAllMembers(cl,list);
		return list;
	}
}
