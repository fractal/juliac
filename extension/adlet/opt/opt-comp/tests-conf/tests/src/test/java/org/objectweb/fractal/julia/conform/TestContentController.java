/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2021 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.conform;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.julia.conform.components.CCSComposite;
import org.objectweb.fractal.julia.conform.components.CCSPrimitive;
import org.objectweb.fractal.util.Fractal;

public class TestContentController extends AbstractTest {

  protected Component c, d, e;

  // -------------------------------------------------------------------------
  // Constructor and setup
  // -------------------------------------------------------------------------

  @BeforeEach
  public void setUpComponents () throws Exception {
	  Factory fcomposite = (Factory)
		  Class.forName(CCSComposite.class.getName()+"Factory").
		  getConstructor().newInstance();
	 c = fcomposite.newFcInstance();
	 d = fcomposite.newFcInstance();
	 Factory fprimitive = (Factory)
		  Class.forName(CCSPrimitive.class.getName()+"Factory").
		  getConstructor().newInstance();
	 e = fprimitive.newFcInstance();
  }

  // -------------------------------------------------------------------------
  // Test add and remove
  // -------------------------------------------------------------------------

  @Test
  public void testAddAndRemove () throws Exception {
	ContentController cc = Fractal.getContentController(c);
	cc.addFcSubComponent(e);
	assertTrue(Arrays.asList(cc.getFcSubComponents()).contains(e));
	cc.removeFcSubComponent(e);
	assertTrue(!Arrays.asList(cc.getFcSubComponents()).contains(e));
  }

  // -------------------------------------------------------------------------
  // Test add errors
  // -------------------------------------------------------------------------

  @Test
  public void testAlreadySubComponent () throws Exception {
	ContentController cc = Fractal.getContentController(c);
	cc.addFcSubComponent(e);
	try {
	  cc.addFcSubComponent(e);
	  fail("");
	} catch (IllegalContentException e) {
	}
  }

  @Test
  public void testWouldCreateCycle1 () throws Exception {
	ContentController cc = Fractal.getContentController(c);
	try {
	  cc.addFcSubComponent(c);
	  fail("");
	} catch (IllegalContentException e) {
	}
  }

  @Test
  public void testWouldCreateCycle2 () throws Exception {
	ContentController cc = Fractal.getContentController(c);
	ContentController cd = Fractal.getContentController(d);
	cc.addFcSubComponent(d);
	try {
	  cd.addFcSubComponent(c);
	  fail("");
	} catch (IllegalContentException e) {
	}
  }

  // -------------------------------------------------------------------------
  // Test remove errors
  // -------------------------------------------------------------------------

  @Test
  public void testNotASubComponent () throws Exception {
	ContentController cc = Fractal.getContentController(c);
	try {
	  cc.removeFcSubComponent(d);
	  fail("");
	} catch (IllegalContentException e) {
	}
  }

  @Test
  public void testWouldCreateNonLocalExportBinding () throws Exception {
	ContentController cc = Fractal.getContentController(c);
	cc.addFcSubComponent(e);
	Fractal.getBindingController(c).bindFc("server", e.getFcInterface("server"));
	try {
	  cc.removeFcSubComponent(e);
	  fail("");
	} catch (IllegalContentException e) {
	}
  }

  @Test
  public void testWouldCreateNonLocalImportBinding () throws Exception {
	ContentController cc = Fractal.getContentController(c);
	cc.addFcSubComponent(e);
	Fractal.getBindingController(e).bindFc("client", cc.getFcInternalInterface("client"));
	try {
	  cc.removeFcSubComponent(e);
	  fail("");
	} catch (IllegalContentException e) {
	}
  }

  @Test
  public void testWouldCreateNonLocalNormalBinding () throws Exception {
	ContentController cc = Fractal.getContentController(c);
	cc.addFcSubComponent(d);
	cc.addFcSubComponent(e);
	Fractal.getBindingController(d).bindFc("client", e.getFcInterface("server"));
	try {
	  cc.removeFcSubComponent(e);
	  fail("");
	} catch (IllegalContentException e) {
	}
  }
}
