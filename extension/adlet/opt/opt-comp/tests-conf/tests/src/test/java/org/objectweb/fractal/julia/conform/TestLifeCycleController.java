/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2021 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.conform;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.julia.conform.components.CCSColFlatPrimitive;
import org.objectweb.fractal.julia.conform.components.I;
import org.objectweb.fractal.util.Fractal;

public class TestLifeCycleController extends AbstractTest {

  protected Component c, d;

  // -------------------------------------------------------------------------
  // Constructor and setup
  // -------------------------------------------------------------------------

  @BeforeEach
  public void setUpComponents () throws Exception {
	  Factory f = (Factory)
		  Class.forName(CCSColFlatPrimitive.class.getName()+"Factory").
		  getConstructor().newInstance();
	  c = f.newFcInstance();
	  d = f.newFcInstance();
  }

  // -------------------------------------------------------------------------
  // Test started and stopped states
  // -------------------------------------------------------------------------

  @Test
  public void testStarted () throws Exception {
	Fractal.getBindingController(c).bindFc("client", d.getFcInterface("server"));
	assertEquals("STOPPED", Fractal.getLifeCycleController(c).getFcState());
	Fractal.getLifeCycleController(c).startFc();
	assertEquals("STARTED", Fractal.getLifeCycleController(c).getFcState());
	final I i = (I)c.getFcInterface("server");
	Thread t = new Thread(new Runnable() {
	  public void run () { i.m(true); }
	});
	t.start();
	t.join(50);
	assertTrue(!t.isAlive());
  }

  @Test
  public void testStopped () throws Exception {
	final I i = (I)c.getFcInterface("server");
	Thread t = new Thread(new Runnable() {
	  public void run () { i.m(true); }
	});
	t.start();
	t.join(50);
	assertTrue(t.isAlive());
  }

  // -------------------------------------------------------------------------
  // Test errors in start
  // -------------------------------------------------------------------------

  @Test
  public void testMandatoryInterfaceNotBound () throws Exception {
	try {
	  Fractal.getLifeCycleController(c).startFc();
	  fail("");
	} catch (IllegalLifeCycleException e) {
	  assertEquals("STOPPED", Fractal.getLifeCycleController(c).getFcState());
	}
  }

  // -------------------------------------------------------------------------
  // Test invalid operations in started state
  // -------------------------------------------------------------------------

  @Test
  public void testUnbindNotStopped () throws Exception {
	Fractal.getBindingController(c).bindFc("client", d.getFcInterface("server"));
	Fractal.getBindingController(c).bindFc("clients0", d.getFcInterface("server"));
	Fractal.getLifeCycleController(c).startFc();
	try {
	  Fractal.getBindingController(c).unbindFc("client");
	  fail("");
	} catch (IllegalLifeCycleException e) {
	}
	try {
	  Fractal.getBindingController(c).unbindFc("clients0");
	  fail("");
	} catch (IllegalLifeCycleException e) {
	}
  }
}
