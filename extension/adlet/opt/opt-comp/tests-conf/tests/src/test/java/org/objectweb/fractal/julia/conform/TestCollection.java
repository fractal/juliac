/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2021 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.conform;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.julia.conform.components.CCSColOptComposite;
import org.objectweb.fractal.julia.conform.components.CCSColOptPrimitive;
import org.objectweb.fractal.julia.conform.components.I;
import org.objectweb.fractal.util.Fractal;

public class TestCollection extends AbstractTest {

  protected final static String serverI   = "server/"+PKG+".I/false,false,false";
  protected final static String servers0I = "servers0/"+PKG+".I/false,false,true";
  protected final static String servers1I = "servers1/"+PKG+".I/false,false,true";
  protected final static String servers2I = "servers2/"+PKG+".I/true,false,true";
  protected final static String servers3I = "servers3/"+PKG+".I/true,false,true";
  protected final static String clientI   = "client/"+PKG+".I/true,true,false";
  protected final static String clients0I = "clients0/"+PKG+".I/true,true,true";
  protected final static String clients1I = "clients1/"+PKG+".I/true,true,true";
  protected final static String clients2I = "clients2/"+PKG+".I/false,true,true";
  protected final static String clients3I = "clients3/"+PKG+".I/false,true,true";

  // -------------------------------------------------------------------------
  // Test component instantiation
  // -------------------------------------------------------------------------

  @Test
  public void testPrimitiveWithCollection () throws Exception {

	  Factory f = (Factory)
		  Class.forName(CCSColOptPrimitive.class.getName()+"Factory").
		  getConstructor().newInstance();
	 Component c = f.newFcInstance();

	checkComponent(c, new HashSet<String>(Arrays.asList(new String[] {
	  COMP, BC, LC, SC, NC, serverI, clientI
	})));
  }

  @Test
  public void testCompositeWithCollection () throws Exception {

	  Factory f = (Factory)
		  Class.forName(CCSColOptComposite.class.getName()+"Factory").
		  getConstructor().newInstance();
	 Component c = f.newFcInstance();

	 checkComponent(c, new HashSet<String>(Arrays.asList(new String[] {
	  COMP, BC, CC, LC, SC, NC, serverI, clientI
	})));
  }

  // -------------------------------------------------------------------------
  // Test lazy interface creation through getFc(Internal)Interface
  // -------------------------------------------------------------------------

  @Test
  public void testPrimitiveGetFcInterface () throws Exception {

	Factory f = (Factory)
		  Class.forName(CCSColOptPrimitive.class.getName()+"Factory").
		  getConstructor().newInstance();
	Component c = f.newFcInstance();

	Fractal.getLifeCycleController(c).startFc();
	Interface i;
	i = (Interface)c.getFcInterface("servers0");
	assertEquals(servers0I, getItf(i, false), "Bad interface");
	checkInterface((I)i);
	i = (Interface)c.getFcInterface("servers1");
	assertEquals(servers1I, getItf(i, false), "Bad interface");
	checkInterface((I)i);
	i = (Interface)c.getFcInterface("clients0");
	assertEquals(clients0I, getItf(i, false), "Bad interface");
	i = (Interface)c.getFcInterface("clients1");
	assertEquals(clients1I, getItf(i, false), "Bad interface");
  }

  @Test
  public void testCompositeGetFcInterface () throws Exception {

	Factory f = (Factory)
		  Class.forName(CCSColOptComposite.class.getName()+"Factory").
		  getConstructor().newInstance();
	Component c = f.newFcInstance();

	Interface i;
	i = (Interface)c.getFcInterface("servers0");
	assertEquals(servers0I, getItf(i, false), "Bad interface");
	i = (Interface)c.getFcInterface("servers1");
	assertEquals(servers1I, getItf(i, false), "Bad interface");
	i = (Interface)c.getFcInterface("clients0");
	assertEquals(clients0I, getItf(i, false), "Bad interface");
	i = (Interface)c.getFcInterface("clients1");
	assertEquals(clients1I, getItf(i, false), "Bad interface");

	ContentController cc = Fractal.getContentController(c);
	i = (Interface)cc.getFcInternalInterface("servers2");
	assertEquals(servers2I, getItf(i, false), "Bad interface");
	i = (Interface)cc.getFcInternalInterface("servers3");
	assertEquals(servers3I, getItf(i, false), "Bad interface");
	i = (Interface)cc.getFcInternalInterface("clients2");
	assertEquals(clients2I, getItf(i, false), "Bad interface");
	i = (Interface)cc.getFcInternalInterface("clients3");
	assertEquals(clients3I, getItf(i, false), "Bad interface");
  }

  // -------------------------------------------------------------------------
  // Test errors of lazy interface creation through getFc(Internal)Interface
  // -------------------------------------------------------------------------

  @Test
  public void testPrimitiveNoSuchCollectionItf () throws Exception {

	Factory f = (Factory)
		  Class.forName(CCSColOptPrimitive.class.getName()+"Factory").
		  getConstructor().newInstance();
	Component c = f.newFcInstance();

	try {
	  c.getFcInterface("server0");
	  fail("");
	} catch (NoSuchInterfaceException e) {
	}
	try {
	  c.getFcInterface("client0");
	  fail("");
	} catch (NoSuchInterfaceException e) {
	}
  }

  @Test
  public void testCompositeNoSuchCollectionItf () throws Exception {

	Factory f = (Factory)
		  Class.forName(CCSColOptComposite.class.getName()+"Factory").
		  getConstructor().newInstance();
	Component c = f.newFcInstance();

	try {
	  c.getFcInterface("server0");
	  fail("");
	} catch (NoSuchInterfaceException e) {
	}
	try {
	  c.getFcInterface("client0");
	  fail("");
	} catch (NoSuchInterfaceException e) {
	}
  }
}
