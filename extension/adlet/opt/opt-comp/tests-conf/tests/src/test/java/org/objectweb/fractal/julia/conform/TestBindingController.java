/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2021 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.conform;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.julia.conform.components.CCSColFlatPrimitive;
import org.objectweb.fractal.julia.conform.components.CCSIJFlatPrimitive;
import org.objectweb.fractal.util.Fractal;

public class TestBindingController extends AbstractTest {

  protected Component c, d, e;

  protected boolean isTemplate;

  protected final static String serverI   = "server/"+PKG+".I/false,false,false";
  protected final static String clientI   = "client/"+PKG+".I/true,false,false";
  protected final static String clients0I = "clients0/"+PKG+".I/true,false,true";

  // -------------------------------------------------------------------------
  // Constructor and setup
  // -------------------------------------------------------------------------

  @BeforeEach
  public void setUpComponents () throws Exception {
	  Factory f = (Factory)
		  Class.forName(CCSColFlatPrimitive.class.getName()+"Factory").
		  getConstructor().newInstance();
	  c = f.newFcInstance();
	  d = f.newFcInstance();
	  f = (Factory)
		  Class.forName(CCSIJFlatPrimitive.class.getName()+"Factory").
		  getConstructor().newInstance();
	  e = f.newFcInstance();
  }

  // -------------------------------------------------------------------------
  // Test list, lookup, bind, unbind
  // -------------------------------------------------------------------------

  @Test
  public void testList () throws Exception {
	BindingController bc = Fractal.getBindingController(c);
	checkList(bc, new String[] { "client" });
  }

  @Test
  public void testBindLookupUnbind () throws Exception {
	BindingController bc = Fractal.getBindingController(c);
	bc.bindFc("client", d.getFcInterface("server"));
	checkList(bc, new String[] { "client" });
	assertEquals(d.getFcInterface("server"), bc.lookupFc("client"));
	bc.unbindFc("client");
	assertEquals(null, bc.lookupFc("client"));
  }

  @Test
  public void testCollectionBindLookupUnbind () throws Exception {
	BindingController bc = Fractal.getBindingController(c);
	bc.bindFc("clients0", d.getFcInterface("server"));
	checkList(bc, new String[] { "client", "clients0" });
	assertEquals(d.getFcInterface("server"), bc.lookupFc("clients0"));
	bc.unbindFc("clients0");
	try {
		assertEquals(null, bc.lookupFc("clients0"));
	  } catch (NoSuchInterfaceException e) {
		checkList(bc, new String[] { "client" });
	  }
  }

  protected void checkList (BindingController bc, String[] expected) {
	String[] names = bc.listFc();
	Set<String> nameSet = new HashSet<>();
	for (int i = 0; i < names.length; ++i) {
	  String name = names[i];
	  if (!nameSet.add(name)) {
		fail("Duplicated interface name: " + name);
	  }
	}
	assertEquals(new HashSet<String>(Arrays.asList(expected)), nameSet);
  }

  // -------------------------------------------------------------------------
  // Test errors in lookup, bind, unbind
  // -------------------------------------------------------------------------

  @Test
  public void testNoSuchInterfaceLookup () {
	try {
	  Fractal.getBindingController(c).lookupFc("c");
	  fail("");
	} catch (NoSuchInterfaceException e) {
	}
  }

  @Test
  public void testNoSuchInterfaceBind () throws Exception {
	try {
	  Fractal.getBindingController(c).bindFc("c", d.getFcInterface("server"));
	  fail("");
	} catch (NoSuchInterfaceException e) {
	}
  }

  @Test
  public void testNotAServerInterface () throws Exception {
	try {
	  Fractal.getBindingController(c).bindFc("client", c.getFcInterface("client"));
	  fail("");
	} catch (IllegalBindingException e) {
	}
  }

  @Test
  public void testWrongType () throws Exception {
	try {
	  Fractal.getBindingController(c).bindFc("client", e.getFcInterface("serverJ"));
	  fail("");
	} catch (IllegalBindingException e) {
	}
  }

  @Test
  public void testMandatoryToOptional () throws Exception {
	try {
	  Fractal.getBindingController(c).bindFc("client", e.getFcInterface("serverI"));
	  fail("");
	} catch (IllegalBindingException e) {
	}
  }

  @Test
  public void testAlreadyBound () throws Exception {
	BindingController bc = Fractal.getBindingController(c);
	bc.bindFc("client", d.getFcInterface("server"));
	try {
	  bc.bindFc("client", d.getFcInterface("server"));
	  fail("");
	} catch (IllegalBindingException e) {
	}
	bc.bindFc("clients0", d.getFcInterface("server"));
	try {
	  bc.bindFc("clients0", d.getFcInterface("server"));
	  fail("");
	} catch (IllegalBindingException e) {
	}
  }

  @Test
  public void testNoSuchInterfaceUnind () throws Exception {
	try {
	  Fractal.getBindingController(c).unbindFc("c");
	  fail("");
	} catch (NoSuchInterfaceException e) {
	}
  }

  @Test
  public void testNotBound () throws Exception {
	try {
	   Fractal.getBindingController(c).unbindFc("client");
	  fail("");
	} catch (IllegalBindingException e) {
	}
  }
}
