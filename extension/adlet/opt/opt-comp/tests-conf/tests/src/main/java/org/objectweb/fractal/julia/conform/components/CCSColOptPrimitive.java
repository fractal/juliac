/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2021 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.conform.components;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.types.Cardinality;
import org.objectweb.fractal.fraclet.types.Contingency;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;

@Component(
	provides={
		@Interface(name="server", signature=I.class),
		@Interface(name="servers", cardinality=Cardinality.COLLECTION, signature=I.class)
})
public class CCSColOptPrimitive implements I,J {

  private boolean x11;

  @Requires(name="client", contingency=Contingency.OPTIONAL)
  private I i;

  @Requires(name="clients",cardinality=Cardinality.COLLECTION, contingency=Contingency.OPTIONAL)
  private Map<String,I> j = new HashMap<>();

  // FUNCTIONAL INTERFACE

  public void m (boolean v) {}
  public void m (byte v) {}
  public void m (char v) {}
  public void m (short v) {}
  public void m (int v) {}
  public void m (long v) {}
  public void m (float v) {}
  public void m (double v) {}
  public void m (String v) {}
  public void m (String[] v) {}

  public boolean n (boolean v, String[] w) {
	return v | x11; // for write only attribute tests
  }

  public byte n (byte v, String w) {
	return v;
  }

  public char n (char v, double w) {
	return v;
  }

  public short n (short v, float w) {
	return v;
  }

  public int n (int v, long w) {
	if (i != null) {
	  // for interceptors tests
	  return w == 0 ? v : i.n(v + 1, w - 1);
	} else if (j.size() > 0) {
	  // for interceptors tests
	  return w == 0 ? v : ((I)j.values().iterator().next()).n(v + 1, w - 1);
	} else {
	  return v;
	}
  }

  public long n (long v, int w) {
	return v;
  }

  public float n (float v, short w) {
	return v;
  }

  public double n (double v, char w) {
	return v;
  }

  public String n (String v, byte w) {
	return v;
  }

  public String[] n (String[] v, boolean w) {
	return v;
  }
}
