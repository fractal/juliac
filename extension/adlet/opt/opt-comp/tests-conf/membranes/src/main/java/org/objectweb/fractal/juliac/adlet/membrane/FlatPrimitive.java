/***
 * Juliac
 * Copyright (C) 2019-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.membrane;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;
import org.objectweb.fractal.juliac.adlet.api.BinderItf;
import org.objectweb.fractal.juliac.adlet.api.annotation.Binding;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;
import org.objectweb.fractal.juliac.adlet.api.annotation.StaticMetamodel;
import org.objectweb.fractal.juliac.adlet.control.component.ComponentController;
import org.objectweb.fractal.juliac.adlet.control.component.LifeCycleController;
import org.objectweb.fractal.juliac.adlet.control.component.LifeCycleInterceptorController;
import org.objectweb.fractal.juliac.adlet.control.component.NameController;
import org.objectweb.fractal.koch.control.binding.BindingControllerDef;
import org.objectweb.fractal.koch.control.component.ComponentControllerDef;
import org.objectweb.fractal.koch.control.component.FlatContainerBindingController;
import org.objectweb.fractal.koch.control.interceptor.InterceptorController;
import org.objectweb.fractal.koch.control.lifecycle.LifeCycleControllerDef;
import org.objectweb.fractal.koch.control.name.NameControllerDef;
import org.objectweb.fractal.koch.factory.MCompositeImpl;

/**
 * Description of the flat primitive membrane.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
@Component(
	provides= {
		@Interface(name="//"+ComponentControllerDef.NAME, signature=org.objectweb.fractal.api.Component.class),
		@Interface(name="//"+NameControllerDef.NAME, signature=org.objectweb.fractal.api.control.NameController.class),
		@Interface(name="//"+LifeCycleControllerDef.NAME, signature=LifeCycleCoordinator.class),
		@Interface(name="//"+BindingControllerDef.NAME, signature=BindingController.class),
		@Interface(name="//"+InterceptorController.NAME, signature=InterceptorController.class),
	}, mdesc=MCompositeImpl.class)
@StaticMetamodel("FlatPrimitive_")
public class FlatPrimitive {

	public static final String NAME = "flatPrimitive";

	@Component(name="Comp")
	private ComponentController comp;

	@Component(name="NC")
	private NameController nc;

	@Component(name="LC")
	private LifeCycleController lc;

	@Component(name="BC")
	private FlatContainerBindingController bc;

	@Component(name="IC")
	private LifeCycleInterceptorController ic;

	@Binding
	public static void binder( BinderItf binder ) {

		binder.export(FlatPrimitive_.__component, FlatPrimitive_.Comp.__component);
		binder.export(FlatPrimitive_.__name_controller, FlatPrimitive_.NC.__name_controller);
		binder.export(FlatPrimitive_.__lifecycle_controller, FlatPrimitive_.LC.__lifecycle_controller);
		binder.export(FlatPrimitive_.__binding_controller, FlatPrimitive_.BC.__binding_controller);
		binder.export(FlatPrimitive_.___interceptor_controller, FlatPrimitive_.IC.___interceptor_controller);

		binder.normal(FlatPrimitive_.BC.__component, FlatPrimitive_.Comp.__component);
		binder.normal(FlatPrimitive_.BC.__lifecycle_controller, FlatPrimitive_.LC.__lifecycle_controller);
		binder.normal(FlatPrimitive_.LC.__component, FlatPrimitive_.Comp.__component);
		binder.normal(FlatPrimitive_.IC.__component, FlatPrimitive_.Comp.__component);
		binder.normal(FlatPrimitive_.IC.__lifecycle_controller, FlatPrimitive_.LC.__lifecycle_controller);
		binder.normal(FlatPrimitive_.IC.__binding_controller, FlatPrimitive_.BC.__binding_controller);
	}
}
