/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.controller;

import java.lang.reflect.Method;

import javax.lang.model.element.ExecutableElement;

import org.objectweb.fractal.juliac.adlet.core.proxy.SimpleSourceCodeGenerator;
import org.objectweb.fractal.juliac.core.proxy.SimpleSourceCodeGeneratorInterceptionType;
import org.objectweb.fractal.juliac.core.proxy.SimpleSourceCodeGeneratorMode;

/**
 * This class generates the source code of component interceptors for the stat
 * controller. This controller counts incoming and outgoing operation
 * invocations. This class is adapted from {@link
 * org.objectweb.fractal.julia.conform.controllers.StatCodeGenerator}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class StatSourceCodeGenerator extends SimpleSourceCodeGenerator {

	public SimpleSourceCodeGeneratorMode getMode() {
		return SimpleSourceCodeGeneratorMode.IN_OUT;
	}

	protected String getControllerInterfaceName() {
		return "stat-controller";
	}

	protected SimpleSourceCodeGeneratorInterceptionType getInterceptionType(Method m) {
		return SimpleSourceCodeGeneratorInterceptionType.EMPTY;
	}

	protected SimpleSourceCodeGeneratorInterceptionType getInterceptionType(ExecutableElement m) {
		return SimpleSourceCodeGeneratorInterceptionType.EMPTY;
	}

	protected String getPreMethodName() {
		return "incrementFcCounter";
	}

	protected String getPostMethodName() {
		return null;
	}

	protected Class<?> getContextType() {
		return Void.TYPE;
	}

	protected String getMethodName(final ExecutableElement m) {
		return m.getSimpleName().toString();
	}

	public String getClassNameSuffix() {
		/*
		 * Generate different suffixes dependending on the implementation class
		 * of the interceptor controller. This does not make a difference in the
		 * COMP mode, but matters in the MERGE_CTRL mode where the merged class
		 * is not the same for primitive and composite components.
		 */
		String signature = StatController.class.getName();
		Class<?> cl = membraneDesc.getCtrlImpl(signature);
		String name = cl.getName();
		int hash = name.hashCode();
		String hexhash = Integer.toHexString(hash);
		String suffix = "Stat"+hexhash;
		return suffix;
	}
}
