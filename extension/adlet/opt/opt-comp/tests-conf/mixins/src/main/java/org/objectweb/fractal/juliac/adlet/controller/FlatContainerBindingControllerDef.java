/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.controller;

import org.objectweb.fractal.julia.BasicControllerMixin;
import org.objectweb.fractal.julia.control.binding.CheckBindingMixin;
import org.objectweb.fractal.julia.control.binding.ContainerBindingControllerMixin;
import org.objectweb.fractal.julia.control.binding.InterceptorBindingMixin;
import org.objectweb.fractal.julia.control.binding.LifeCycleBindingMixin;
import org.objectweb.fractal.julia.control.binding.TypeBindingMixin;
import org.objectweb.fractal.julia.control.lifecycle.UseLifeCycleControllerMixin;
import org.objectweb.fractal.juliac.adlet.api.annotation.Mixin;
import org.objectweb.fractal.juliac.adlet.controller.comp.UseComponentMixin;
import org.objectweb.fractal.juliac.fraclet.control.binding.FracletBindingControllerMixin;

/**
 * Implementation of the binding controller for primitive components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
@Mixin(
	impl=FlatContainerBindingControllerDef.NAME,
	layers={
		BasicControllerMixin.class,
		ContainerBindingControllerMixin.class,
		FracletBindingControllerMixin.class,
		UseComponentMixin.class,
		InterceptorBindingMixin.class,
		CheckBindingMixin.class,
		TypeBindingMixin.class,
		UseLifeCycleControllerMixin.class,
		LifeCycleBindingMixin.class
	}
)
public class FlatContainerBindingControllerDef {
	public static final String NAME = "juliac.generated.FlatContainerBindingControllerImpl";
}
