/***
 * Juliac
 * Copyright (C) 2018-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.opt.comp;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;

import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.adlet.adl.ADLClassToComponentDescAdlet;
import org.objectweb.fractal.juliac.adlet.core.desc.ComponentDescAdlet;
import org.objectweb.fractal.juliac.adlet.core.proxy.InterfaceImplementationClassGenerator;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.InterfaceTypeConfigurableItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;
import org.objectweb.fractal.juliac.core.helper.InterfaceTypeHelper;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.opt.InitializerClassGenerator;

/**
 * This class generates the source code associated to Fractal Adlet annotated
 * components. The membrane implementation and the initializer implementation
 * are generated.
 *
 * The content, the interceptors and the controllers are kept in separate
 * classes and the controllers are implemented with components.
 *
 * This class is specialized for the annotation processing tool of the JDK.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public class FCCompCtrlSourceCodeGenerator implements FCSourceCodeGeneratorItf {

	// -----------------------------------------------------------------------
	// Implementation of the JuliacModuleItf interface
	// -----------------------------------------------------------------------

	protected JuliacItf jc;

	@Override
	public void init( JuliacItf jc ) throws IOException {
		this.jc = jc;
		jc.register(FCSourceCodeGeneratorItf.class,this);
	}

	@Override
	public void close( JuliacItf jc ) throws IOException {
		jc.unregister(FCSourceCodeGeneratorItf.class,this);
	}


	// -----------------------------------------------------------------------
	// Implementation of the FCSourceCodeGeneratorItf interface
	// -----------------------------------------------------------------------

	@Override
	public boolean test( Object controllerDesc ) {
		boolean accept =
			! controllerDesc.equals("mPrimitive") &&
			! controllerDesc.equals("mComposite");
		return accept;
	}

	@Override
	public SourceCodeGeneratorItf generate( ComponentDesc<?> desc )
	throws IOException {

		ComponentDescAdlet<TypeElement> cdesc = (ComponentDescAdlet<TypeElement>) desc;

		ComponentType ct = cdesc.getCT();
		Object contentDesc = cdesc.getContentClassName();
		Object source = cdesc.getSource();

		/*
		 * Generate the membrane implementation.
		 */
		MembraneDesc<?> membraneDesc = generateMembraneImpl(cdesc);

		/*
		 * Generate the initializer class.
		 */
		SourceCodeGeneratorItf scg =
			getInitializerClassGenerator(
				jc,this,membraneDesc,ct,contentDesc,source);
		jc.generateSourceCode(scg);

		return scg;
	}

	@Override
	public SourceCodeGeneratorItf generate(
		Type type, Object controllerDesc, Object contentDesc, Object source )
	throws IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public MembraneDesc<?> generateMembraneImpl(
		ComponentType ct, String ctrldesc, String contentClassName,
		Object source )
	throws IOException {

		/*
		 * Throw IllegalArgumentException to make JuliacCmdLine#main understand
		 * that this method is not supported. The case occurs with
		 * juliac-adlet-membranes-mergectrl when generating code for membrane
		 * descriptors, e.g. org.objectweb.fractal.koch.membrane.Composite.
		 *
		 * Do not throw UnsupportedOperationException.
		 */

		throw new IllegalArgumentException();
	}

	/**
	 * Descriptors of already generated membranes.
	 * The key is the name of the class implementing the membrane.
	 */
	private Map<String,ComponentDescAdlet<Class<?>>> cdescs = new HashMap<>();

	@Override
	public void setCtrlDescPrefix( String prefix ) {
		throw new UnsupportedOperationException();
	}

	@Override
	public SourceCodeGeneratorItf getInterfaceClassGenerator(InterfaceType it) {

		ProcessingEnvironment env = jc.getProcessingEnvironment();

		String signature = it.getFcItfSignature();
		Elements elements = env.getElementUtils();
		TypeElement cl = elements.getTypeElement(signature);
		String pkgRoot = jc.getPkgRoot();

		return new InterfaceImplementationClassGenerator(it,cl,pkgRoot,false);
	}


	// ------------------------------------------------------------------
	// Implementation specific
	// ------------------------------------------------------------------

	private MembraneDesc<?> generateMembraneImpl( ComponentDescAdlet<TypeElement> cd )
	throws IOException {

		/*
		 * Generate business interface implementations.
		 */
		ComponentType ct = cd.getCT();
		String ctrldesc = cd.getCtrlDesc();
		InterfaceType[] its = ct.getFcInterfaceTypes();
		generateInterfaceImpl(its);

		/*
		 * Generate the implementation of the component-based control membrane.
		 */
		String classname = cd.getMembraneClass().getName() + getMembraneClassNameSuffix();
		ComponentDescAdlet<Class<?>> cdesc = null;
		if( cdescs.containsKey(classname) ) {
			cdesc = cdescs.get(classname);
		}
		else {
			cdesc = getComponentDescFromADL(cd);
			cdescs.put(classname,cdesc);
		}

		/*
		 * Prepare the membrane descriptor.
		 *
		 * Prepare a 2nd membrane descriptor with level 0 interface type names
		 * (e.g. stat-controller), instead of meta-level interface type names
		 * (e.g. //stat-controller). The reason is that, when we need to
		 * generate interceptors for control interfaces, such as with Tinfi,
		 * interceptors expect level 0 interface types, not meta-level ones.
		 */
		InterfaceType[] mits = cdesc.getCT().getFcInterfaceTypes();
		MembraneDesc<?> md = getMembraneDesc(jc,classname,ctrldesc,mits,cdesc);

		InterfaceType[] membraneits = InterfaceTypeHelper.downToLevel0InterfaceType(mits);
		MembraneDesc<?> mdintercept =
			getMembraneDesc(jc,classname,ctrldesc,membraneits,cdesc);

		/*
		 * Generate interceptor implementations.
		 * Interceptors must be generated after controllers as some interceptors
		 * (e.g. the lifecycle interceptor) use controller implementation
		 * class names.
		 */
		generateInterceptorImpl(its,mdintercept);
		generateInterceptorImpl(membraneits,mdintercept);

		return md;
	}

	protected InitializerClassGenerator
	getInitializerClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		return new InitializerCompCtrlClassGenerator(
			jc,fcscg,membraneDesc,ct,contentDesc,source);
	}

	private MembraneDesc<?> getMembraneDesc(
		JuliacItf jc, String name, String descriptor,
		InterfaceType[] ctrlItfTypes, ComponentDesc<?> ctrlDescs ) {

		return new CompBasedMembraneDesc(jc,name,descriptor,ctrlItfTypes,ctrlDescs);
	}

	private void generateInterfaceImpl( InterfaceType[] its )
	throws IOException {
		for (InterfaceType it : its) {
			generateInterfaceImpl(it);
			InterfaceType internalit =
				InterfaceTypeHelper.newSymetricInterfaceType(it);
			generateInterfaceImpl(internalit);
		}
	}

	private void generateInterfaceImpl( InterfaceType it )
	throws IOException {

		SourceCodeGeneratorItf iscg = getInterfaceClassGenerator(it);
		jc.generateSourceCode(iscg);
	}

	private void generateInterceptorImpl(
		InterfaceType[] its, MembraneDesc<?> membraneDesc )
	throws IOException {

		InterfaceTypeConfigurableItf iscg =
			membraneDesc.getInterceptorClassGenerator();
		if( iscg == null ) {
			// No interceptor
			return;
		}

		// Generate the interceptors
		iscg.setMembraneDesc(membraneDesc);
		ProcessingEnvironment env = jc.getProcessingEnvironment();
		Elements elements = env.getElementUtils();
		String pkgRoot = jc.getPkgRoot();
		for (InterfaceType it : its) {
			String signature = it.getFcItfSignature();
			TypeElement cl = elements.getTypeElement(signature);
			iscg.setInterfaceType(it,cl,pkgRoot);
			if( iscg.match() ) {
				jc.generateSourceCode(iscg);
			}
		}
	}

	protected ComponentDescAdlet<Class<?>> getComponentDescFromADL(
		ComponentDescAdlet<TypeElement> cd )
	throws IOException {

		Class<?> membrane = cd.getMembraneClass();
		ComponentDescAdlet<Class<?>> cdesc = ADLClassToComponentDescAdlet.get(membrane,jc);

		return cdesc;
	}

	protected String getMembraneClassNameSuffix() {
		return "Factory";
	}
}
