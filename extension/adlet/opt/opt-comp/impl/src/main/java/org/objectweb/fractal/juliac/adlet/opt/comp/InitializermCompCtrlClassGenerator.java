/***
 * Juliac
 * Copyright (C) 2019-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.opt.comp;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;

/**
 * Initializer generator for Fractal mPrimitive and mComposite components.
 *
 * This class is specialized for the annotation processing tool of the JDK.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public class InitializermCompCtrlClassGenerator
extends org.objectweb.fractal.juliac.opt.comp.InitializermCompCtrlClassGenerator {

	public InitializermCompCtrlClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		super(jc,fcscg,membraneDesc,ct,contentDesc,source);
	}

	@Override
	protected Object loadClass( String signature ) {
		ProcessingEnvironment env = jc.getProcessingEnvironment();
		Elements elements = env.getElementUtils();
		TypeElement cl = elements.getTypeElement(signature);
		return cl;
	}
}
