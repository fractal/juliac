/***
 * Juliac
 * Copyright (C) 2019-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.opt.comp;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.adlet.api.generator.InterceptorSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.adlet.core.proxy.InterceptorClassGenerator;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.generator.InterfaceTypeConfigurableItf;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;

/**
 * A membrane descriptor where the structure of the membrane is component-based.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public class CompBasedMembraneDesc
extends org.objectweb.fractal.juliac.opt.comp.CompBasedMembraneDesc {

	public CompBasedMembraneDesc(
		JuliacItf jc, String name, String descriptor,
		InterfaceType[] ctrlItfTypes, ComponentDesc<?> ctrlDescs ) {

		super(jc,name,descriptor,ctrlItfTypes,ctrlDescs);
	}


	// ------------------------------------------------------------------
	// Implementation specific
	// ------------------------------------------------------------------

	@Override
	protected InterfaceTypeConfigurableItf getInterceptorClassGeneratorFromName(
		String name, InterfaceTypeConfigurableItf[] itcs ) {

		Class<InterceptorClassGenerator> cl = jc.loadClass(name);
		InterceptorClassGenerator o = jc.instantiate(cl);

		// Casting the array as a whole generates a ClassCastException
		InterceptorSourceCodeGeneratorItf[] iscgs =
			new InterceptorSourceCodeGeneratorItf[itcs.length];
		for (int i = 0; i < iscgs.length; i++) {
			iscgs[i] = (InterceptorSourceCodeGeneratorItf) itcs[i];
		}

		o.init(iscgs);

		return o;
	}

	@Override
	protected InterfaceTypeConfigurableItf getInterceptorSourceCodeGeneratorFromName(
		String name ) {

		Class<InterfaceTypeConfigurableItf> cl = jc.loadClass(name);
		InterfaceTypeConfigurableItf o = jc.instantiate(cl);
		return o;
	}
}
