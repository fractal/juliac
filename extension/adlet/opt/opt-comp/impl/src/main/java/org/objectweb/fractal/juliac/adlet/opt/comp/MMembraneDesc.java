/***
 * Juliac
 * Copyright (C) 2019-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.opt.comp;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.adlet.api.generator.InterceptorSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.adlet.core.proxy.InterceptorClassGenerator;
import org.objectweb.fractal.juliac.api.JuliacItf;

/**
 * A membrane descriptor where the structure of the membrane is represented by a
 * single class, e.g. MPrimitiveImpl or MCompositeImpl. This is a membrane
 * descriptor for control components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public class MMembraneDesc
extends org.objectweb.fractal.juliac.opt.comp.MMembraneDesc {

	public MMembraneDesc(
		JuliacItf jc, String name,
		String descriptor, InterfaceType[] ctrlItfTypes, Class<?> ctrlDescs ) {

		super(jc,name,descriptor,ctrlItfTypes,ctrlDescs);
	}

	@Override
	protected void init() {

		InterceptorSourceCodeGeneratorItf iscg =
			new LifeCycleSourceCodeGenerator();
		iscg.setMembraneDesc(this);
		InterceptorSourceCodeGeneratorItf[] iscgs =
			new InterceptorSourceCodeGeneratorItf[] {iscg};
		interceptorSourceCodeGenerators = iscgs;

		interceptorClassGenerator = new InterceptorClassGenerator();
		((InterceptorClassGenerator)interceptorClassGenerator).init(iscgs);
		interceptorClassGenerator.setMembraneDesc(this);
	}
}
