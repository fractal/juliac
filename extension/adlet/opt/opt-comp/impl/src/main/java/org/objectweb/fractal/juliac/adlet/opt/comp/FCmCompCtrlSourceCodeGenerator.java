/***
 * Juliac
 * Copyright (C) 2019-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.opt.comp;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;

import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.adlet.core.proxy.InterfaceImplementationClassGenerator;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.JuliacRuntimeException;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.InterfaceTypeConfigurableItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.helper.InterfaceTypeHelper;
import org.objectweb.fractal.juliac.core.helper.JuliacHelper;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.opt.InitializerClassGenerator;
import org.objectweb.fractal.juliac.fraclet.runtime.comp.MPrimitiveImpl;
import org.objectweb.fractal.koch.control.binding.BindingControllerDef;
import org.objectweb.fractal.koch.control.component.ComponentControllerDef;
import org.objectweb.fractal.koch.control.content.ContentControllerDef;
import org.objectweb.fractal.koch.control.content.SuperControllerDef;
import org.objectweb.fractal.koch.control.lifecycle.LifeCycleControllerDef;
import org.objectweb.fractal.koch.control.name.NameControllerDef;
import org.objectweb.fractal.koch.factory.MCompositeImpl;

/**
 * This class generates the source code associated to Fractal components.
 * The membrane implementation and the initializer implementation are generated.
 *
 * This generator handles mPrimitive and mComposite components.
 *
 * This class is specialized for the annotation processing tool of the JDK.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public class FCmCompCtrlSourceCodeGenerator implements FCSourceCodeGeneratorItf {

	// -----------------------------------------------------------------------
	// Implementation of the JuliacModuleItf interface
	// -----------------------------------------------------------------------

	protected JuliacItf jc;

	@Override
	public void init( JuliacItf jc ) throws IOException {
		this.jc = jc;
		jc.register(FCSourceCodeGeneratorItf.class,this);
	}

	@Override
	public void close( JuliacItf jc ) throws IOException {
		jc.unregister(FCSourceCodeGeneratorItf.class,this);
	}


	// -----------------------------------------------------------------------
	// Implementation of the FCSourceCodeGeneratorItf interface
	// -----------------------------------------------------------------------

	@Override
	public boolean test( Object controllerDesc ) {
		boolean accept =
			controllerDesc.equals("mPrimitive") ||
			controllerDesc.equals("mComposite");
		return accept;
	}

	@Override
	public MembraneDesc<?> generateMembraneImpl(
		ComponentType ct, String ctrldesc, String contentClassName,
		Object source )
	throws IOException {

		/*
		 * Generate business interface implementations.
		 */
		InterfaceType[] its = ct.getFcInterfaceTypes();
		generateInterfaceImpl(its);

		/*
		 * Generate control interface implementations.
		 */
		InterfaceType[] membraneits = getMembraneType(ctrldesc);
		generateInterfaceImpl(membraneits);

		/*
		 * Prepare a membrane descriptor.
		 */
		Class<?> ctrlImpl = getMembraneClass(ctrldesc);
		String name = ctrlImpl.getName();
		MembraneDesc<?> md =
			getMembraneDesc(jc,name,ctrldesc,membraneits,ctrlImpl);

		/*
		 * Generate the lifecycle interceptors associated with control
		 * components for all business interfaces of the control components.
		 */
		if( ctrldesc.equals("mPrimitive") ) {
			String pkgRoot = jc.getPkgRoot();
			InterfaceTypeConfigurableItf icg = md.getInterceptorClassGenerator();
			icg.setMergeable(false);
			for (InterfaceType it : its) {
				String signature = it.getFcItfSignature();
				Object cl = loadClass(signature);
				icg.setInterfaceType(it,cl,pkgRoot);
				if( icg.match() ) {
					jc.generateSourceCode(icg);
				}
			}
		}

		return md;
	}

	@Override
	public void setCtrlDescPrefix( String prefix ) {
		throw new UnsupportedOperationException();
	}

	@Override
	public SourceCodeGeneratorItf generate(
		Type type, Object controllerDesc, Object contentDesc, Object source )
	throws IOException {

		/*
		 * Check parameter consistence. It's safe to perform the casts after
		 * having performed the checks.
		 */
		if( !(type instanceof ComponentType) ) {
			final String msg = "ComponentType instance expected";
			throw new IllegalArgumentException(msg);
		}
		if( !(controllerDesc instanceof String) ) {
			final String msg = "controllerDesc should be a String";
			throw new IllegalArgumentException(msg);
		}
		ComponentType ct = (ComponentType) type;
		String ctrlDesc = (String) controllerDesc;

		/*
		 * Generate the membrane implementation.
		 */
		String contentClassName =
			JuliacHelper.getContentClassName(ctrlDesc,contentDesc);
		MembraneDesc<?> membraneDesc =
			generateMembraneImpl(ct,ctrlDesc,contentClassName,source);

		/*
		 * Generate the initializer class.
		 */
		SourceCodeGeneratorItf scg =
			getInitializerClassGenerator(
				jc,this,membraneDesc,ct,contentDesc,source);
		jc.generateSourceCode(scg);

		return scg;
	}

	@Override
	public SourceCodeGeneratorItf getInterfaceClassGenerator(InterfaceType it) {

		ProcessingEnvironment env = jc.getProcessingEnvironment();

		String signature = it.getFcItfSignature();
		Elements elements = env.getElementUtils();
		TypeElement cl = elements.getTypeElement(signature);
		String pkgRoot = jc.getPkgRoot();

		return new InterfaceImplementationClassGenerator(it,cl,pkgRoot,false);
	}


	// -----------------------------------------------------------------------
	// Implementation specific
	// -----------------------------------------------------------------------

	private InitializerClassGenerator
	getInitializerClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		return new InitializermCompCtrlClassGenerator(
			jc,fcscg,membraneDesc,ct,contentDesc,source);
	}

	private void generateInterfaceImpl( InterfaceType[] its )
	throws IOException {
		for (InterfaceType it : its) {
			generateInterfaceImpl(it);
			InterfaceType internalit =
				InterfaceTypeHelper.newSymetricInterfaceType(it);
			generateInterfaceImpl(internalit);
		}
	}

	/**
	 * Return the class implementing the membrane of the specified control
	 * component descriptor mPrimitive or mComposite).
	 */
	private Class<?> getMembraneClass( String ctrldesc ) {

		if( ctrldesc.equals("mPrimitive") ) {
			return MPrimitiveImpl.class;
		}

		if( ctrldesc.equals("mComposite") ) {
			return MCompositeImpl.class;
		}

		final String msg = "No such controller desc: "+ctrldesc;
		throw new JuliacRuntimeException(msg);
	}

	private MembraneDesc<?> getMembraneDesc(
		JuliacItf jc, String name, String ctrldesc, InterfaceType[] membraneits,
		Class<?> ctrlImpl ) {

		return new MMembraneDesc(jc,name,ctrldesc,membraneits,ctrlImpl);
	}

	private void generateInterfaceImpl( InterfaceType it )
	throws IOException {

		SourceCodeGeneratorItf iscg = getInterfaceClassGenerator(it);
		jc.generateSourceCode(iscg);
	}

	/**
	 * Return the control interface types associated to the specified controller
	 * descritptor.
	 *
	 * @param ctrldesc  the controller descriptor
	 * @return          the associated array of control interface types
	 */
	private InterfaceType[] getMembraneType( String ctrldesc ) {

		if( membraneTypes.containsKey(ctrldesc) ) {
			InterfaceType[] membraneType = membraneTypes.get(ctrldesc);
			return membraneType;
		}
		else {
			final String msg = "No such controller desc: "+ctrldesc;
			throw new JuliacRuntimeException(msg);
		}
	}

	private static final Map<String,InterfaceType[]> membraneTypes =
		new HashMap<>() {
			private static final long serialVersionUID = -5024933498299059603L;
		{
			put("mPrimitive",
				new InterfaceType[]{
					ComponentControllerDef.TYPE,
					BindingControllerDef.TYPE,
					NameControllerDef.TYPE,
					SuperControllerDef.TYPE,
					LifeCycleControllerDef.TYPE });
			put("mComposite",
				new InterfaceType[]{
					ComponentControllerDef.TYPE,
					BindingControllerDef.TYPE,
					NameControllerDef.TYPE,
					SuperControllerDef.TYPE,
					LifeCycleControllerDef.TYPE,
					ContentControllerDef.TYPE });
		}};

	private Object loadClass( String signature ) {
		ProcessingEnvironment env = jc.getProcessingEnvironment();
		Elements elements = env.getElementUtils();
		TypeElement cl = elements.getTypeElement(signature);
		return cl;
	}
}
