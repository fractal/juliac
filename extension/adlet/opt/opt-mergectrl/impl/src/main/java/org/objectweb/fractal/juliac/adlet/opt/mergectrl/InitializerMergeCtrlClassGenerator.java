/***
 * Juliac
 * Copyright (C) 2019-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.opt.mergectrl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.Interceptor;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.adlet.opt.comp.InitializerCompCtrlClassGenerator;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.CatchSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ThenSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.helper.InterfaceTypeHelper;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.runtime.MembraneInitializer;

/**
 * Initializer generator for Fractal components with a merged control membrane.
 *
 * This class is specialized for the annotation processing tool of the JDK.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public class InitializerMergeCtrlClassGenerator
extends InitializerCompCtrlClassGenerator {

	public InitializerMergeCtrlClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		super(jc,fcscg,membraneDesc,ct,contentDesc,source);
	}

	@Override
	public String getSuperClassName() {
		return MembraneInitializer.class.getName();
	}

	@Override
	protected void generateNewFcInstanceContentMethod( BlockSourceCodeVisitor mv ) {

		String membraneName = membraneDesc.getName();
		InterfaceType[] mits = membraneDesc.getCtrlItfTypes();

		// Instantiate the membrane
		mv.visitSet( "Object membrane", "new "+membraneName+"()" );

		// Prepare an InitializationContext
		mv.visitSet(InitializationContext.class.getName()+" ic","initFcInitializationContext()");
		mv.visitSet("ic.internalInterfaces","new",HashMap.class.getName(),"()");

		// Initialize the content part of the initialization context
		generateInitializationContextForContent(mv);

		// Initialize the controllers part of the initialization context
		mv.visitIns("ic.controllers.add(membrane)");

		// Component controller
		InterfaceType compctrlit =
			new BasicInterfaceType(
				"component",Component.class.getName(),false,false,false);
		SourceCodeGeneratorItf itfscg =
			fcscg.getInterfaceClassGenerator(compctrlit);
		String fcitfClassname = itfscg.getTargetTypeName();
		String itstring = InterfaceTypeHelper.javaify(compctrlit).toString();
		mv.visitSet(
			Component.class.getName()+" compctrl",
			"(",Component.class.getName(),") membrane");
		mv.visitSet("Object intercept","null");
		boolean interceptorCreated =
			generateInterceptorCreation(mv,compctrlit,"membrane");
		if( ! interceptorCreated ) {
			mv.visitSet("intercept","compctrl");
		}
		mv.visit  ("    "+Interface.class.getName()+" proxy = ");
		mv.visit  ("new "+fcitfClassname+"(");
		mv.visitln("compctrl,\"component\","+itstring+",false,intercept);");
		mv.visit  ("    "+Component.class.getName()+" proxyForCompCtrl = ");
		mv.visitln("("+Component.class.getName()+") proxy;");
		if(interceptorCreated) {
			generateInterceptorPostInit(mv,compctrlit,"proxy");
		}
		mv.visitIns("ic.interfaces.put(\"component\",proxy)");

		List<InterfaceType> fullITs = new ArrayList<>();
		fullITs.add(compctrlit);

		mv.visitSet("Object cloneableAttrCtrlImpl","null");
		mv.visitSet("Object delegate","null");

		// Controllers other than the component controller
		for (InterfaceType srcit : mits) {
			String name = srcit.getFcItfName();

			if( name.startsWith("//") && !name.equals("//component") ) {

				compctrlit = InterfaceTypeHelper.downToLevel0InterfaceType(srcit);
				itfscg = fcscg.getInterfaceClassGenerator(compctrlit);
				fcitfClassname = itfscg.getTargetTypeName();
				String itname = compctrlit.getFcItfName();
				itstring = InterfaceTypeHelper.javaify(compctrlit).toString();

				// Interceptors
				interceptorCreated =
					generateInterceptorCreation(mv,compctrlit,"membrane");
				if( ! interceptorCreated ) {
					mv.visitSet("intercept","membrane");
				}

				// Control external interface
				mv.visit  ("    proxy = ");
				mv.visit  ("new "+fcitfClassname+"(");
				mv.visit  ("proxyForCompCtrl,\""+itname+"\","+itstring+",false,");
				mv.visit  ("intercept");
				mv.visitln(");");
				if(interceptorCreated) {
					generateInterceptorPostInit(mv,compctrlit,"proxy");
				}
				mv.visitIns("ic.interfaces.put(\""+itname+"\",proxy)");

				// Control internal interface
				if( name.equals("//factory") ) {
					InterfaceType intit = InterfaceTypeHelper.newSymetricInterfaceType(compctrlit);
					itstring = InterfaceTypeHelper.javaify(intit).toString();

					mv.visit  ("    proxy = ");
					mv.visit  ("new "+fcitfClassname+"(");
					mv.visit  ("proxyForCompCtrl,\""+itname+"\","+itstring+",true,");
					mv.visit  ("intercept");
					mv.visitln(");");
					mv.visitln("    ic.internalInterfaces.put(\""+itname+"\",proxy);");
				}

				// Cloneable attribute controller implementation
				if( name.equals("///cloneable-attribute-controller") ) {
					mv.visitln("    cloneableAttrCtrlImpl = proxy;");
					mv.visitln("    while( cloneableAttrCtrlImpl instanceof "+ComponentInterface.class.getName()+" ) {");
					mv.visitln("       "+ComponentInterface.class.getName()+" ci = ("+ComponentInterface.class.getName()+") cloneableAttrCtrlImpl;");
					mv.visitln("       cloneableAttrCtrlImpl = ci.getFcItfImpl();");
					mv.visitln("    }");
					mv.visitln("    while( cloneableAttrCtrlImpl instanceof "+Interceptor.class.getName()+" ) {");
					mv.visitln("       "+Interceptor.class.getName()+" ci = ("+Interceptor.class.getName()+") cloneableAttrCtrlImpl;");
					mv.visitln("       cloneableAttrCtrlImpl = ci.getFcItfDelegate();");
					mv.visitln("    }");
				}

				// Component type
				if( ! name.startsWith("///") ) {
					fullITs.add(compctrlit);
				}
			}
		}

		// Component type = control type + business type
		InterfaceType[] its = ct.getFcInterfaceTypes();
		fullITs.addAll( Arrays.asList(its) );
		InterfaceType[] full = fullITs.toArray( new InterfaceType[fullITs.size()] );
		mv.visitSet("ic.type",InterfaceTypeHelper.javaify(full));

		// Business interfaces
		for (int i = 0; i < its.length; i++) {

			String itname = its[i].getFcItfName();

			if( its[i].isFcCollectionItf() ) {
				// Julia naming convention for collection interfaces
				itname = "/collection/" + itname;
			}

			// Interceptors
			interceptorCreated =
				generateInterceptorCreation(mv,its[i],"content");
			if( ! interceptorCreated ) {
				mv.visit("    intercept = ");
				mv.visit( its[i].isFcClientItf() ? "null" : "content" );
				mv.visitln(";");
			}

			/*
			 * If an implementation of a cloneable attribute controller is
			 * available (this is the case for parametric primitive and
			 * composite templates), let the attribute controller interface
			 * delegate to this instance instead of the content.
			 *
			 * The idea is that the state of the component (as defined by the
			 * attributes) must be cloned when a new instance of the template is
			 * created. Hence, each setting of a attribute must be directed
			 * towards the cloneable attribute controller to let it be cloned
			 * latter on.
			 */
			mv.visit  ("    delegate = ");
			if( itname.equals("attribute-controller") ) {
				mv.visit("cloneableAttrCtrlImpl!=null ? ");
				mv.visit("cloneableAttrCtrlImpl : content");
			}
			else {
				mv.visit("intercept");  // interceptor or content or null
			}
			mv.visitln(";");

			// External interface
			generateNFICMExternalInterface(mv,its[i]);
			if(interceptorCreated) {
				generateInterceptorPostInit(mv,its[i],"proxy");
			}

			// Internal interface (skip attribute control interfaces)
			if( ! itname.equals("attribute-controller") ) {
				InterfaceType intit = InterfaceTypeHelper.newSymetricInterfaceType(its[i]);
				itfscg = fcscg.getInterfaceClassGenerator(intit);
				fcitfClassname = itfscg.getTargetTypeName();
				itstring = InterfaceTypeHelper.javaify(intit).toString();
				mv.visit  ("    proxy = ");
				mv.visit  ("new "+fcitfClassname+"(");
				mv.visitln("proxyForCompCtrl,\""+itname+"\","+itstring+",true,delegate);");
				mv.visitln("    ic.internalInterfaces.put(\""+itname+"\",proxy);");
				if( its[i].isFcCollectionItf() ) {
					mv.visitln("    ic.internalInterfaces.put(\"/juliak"+itname+"\",proxy);");
				}
			}
		}

		/*
		 * Initialize controllers.
		 *
		 * Invoke methods whose name starts with initFcController in the merged
		 * controller.
		 */
		mv.visitSet(Method.class.getName()+"[] ms","membrane.getClass().getMethods()");
		BlockSourceCodeVisitor bv = mv.visitFor(Method.class.getName(),"m",":","ms");
		ThenSourceCodeVisitor tv = bv.visitIf("m.getName().startsWith(\"initFcController\")");
		CatchSourceCodeVisitor cv = tv.visitTry();
		cv.visitIns("m.invoke(membrane,ic)");
		CatchSourceCodeVisitor cv1 =
			cv.visitMultiCatch(
				"e",IllegalAccessException.class,InvocationTargetException.class);
		cv1.visitIns("throw","new",ChainedInstantiationException.class.getName(),"(e,proxyForCompCtrl,\"\")");
		cv1.visitEnd();
		tv.visitEnd();
		bv.visitEnd();

		// Intialize interceptors
		mv.visitIns("initFcController(ic)");

		// Return the fcinterface for the component controller
		mv.visitIns("return proxyForCompCtrl");
	}
}
