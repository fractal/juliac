/***
 * Juliac
 * Copyright (C) 2019-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.opt.mergectrl;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.lang.model.element.TypeElement;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.juliac.adlet.core.desc.ComponentDescAdlet;
import org.objectweb.fractal.juliac.adlet.opt.comp.FCCompCtrlSourceCodeGenerator;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.core.desc.AttributeDesc;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.opt.InitializerClassGenerator;

/**
 * This class generates the source code associated to Fractal Adlet annotated
 * components. The membrane implementation and the initializer implementation
 * are generated.
 *
 * The content, the interceptors and the controllers are kept in separate
 * classes and the controllers are merged in a single object.
 *
 * This class is specialized for the annotation processing tool of the JDK.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public class FCMergeCtrlSourceCodeGenerator
extends FCCompCtrlSourceCodeGenerator {

	@Override
	protected InitializerClassGenerator
	getInitializerClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct,
		Object contentDesc, Object source ) {

		return new InitializerMergeCtrlClassGenerator(
			jc,fcscg,membraneDesc,ct,contentDesc,source);
	}


	// ------------------------------------------------------------------
	// Implementation specific
	// ------------------------------------------------------------------

	@Override
	protected ComponentDescAdlet<Class<?>> getComponentDescFromADL(
		ComponentDescAdlet<TypeElement> cd )
	throws IOException {

		ComponentDescAdlet<Class<?>> cdesc = super.getComponentDescFromADL(cd);

		// Create a merged version of the membrane architecture
		String umid = cdesc.getID();
		String umname = cdesc.getName();
		String umdefinition = cdesc.getDefinition();
		ComponentType umct = cdesc.getCT();
		String umclassname = cd.getMembraneClass().getName() + getMembraneClassNameSuffix();
		Class<?> umsource = cdesc.getSource();
		Object ummm = cdesc.getMetamodelClass();

		ComponentDescAdlet<Class<?>> umcdesc =
			new ComponentDescAdlet<>(
				umid,umname,umdefinition,umct,"mPrimitive",umclassname,
				umsource,ummm);

		// Retrive attribute definitions and add them to the merged version
		copyAttributeDescs(cdesc,umcdesc);

		return umcdesc;
	}

	@Override
	protected String getMembraneClassNameSuffix() {
		return "UM";
	}

	/**
	 * Recursively traverse the source component descriptor and add the
	 * attribute definitions to the target component descriptor.
	 *
	 * @throws IOException
	 * 		in case of duplicate attribute definition in the source component
	 * 		descriptor
	 */
	private void copyAttributeDescs(
		ComponentDesc<Class<?>> src ,ComponentDesc<?> target )
	throws IOException {

		// Add attributes
		Set<String> attrNames = src.getAttributeNames();
		for (String attrName : attrNames) {

			if( target.getAttribute(attrName) != null ) {
				String msg =
					"Cannot merge due to duplicate attribute definition: "+
					attrName+" in "+src;
				throw new IOException(msg);
			}

			AttributeDesc adesc = src.getAttribute(attrName);
			target.putAttribute(attrName,adesc);
		}

		// Traverse the component hierarchy
		List<ComponentDesc<Class<?>>> subs = src.getSubComponents();
		for (ComponentDesc<Class<?>> sub : subs) {
			copyAttributeDescs(sub,target);
		}
	}
}
