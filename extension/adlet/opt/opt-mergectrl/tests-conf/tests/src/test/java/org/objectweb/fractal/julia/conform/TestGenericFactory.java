/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2021 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.conform;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.jupiter.api.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.julia.conform.components.CCSComposite;
import org.objectweb.fractal.julia.conform.components.CCSFlatParametricPrimitive;
import org.objectweb.fractal.julia.conform.components.CCSFlatPrimitive;
import org.objectweb.fractal.julia.conform.components.CCSParametricComposite;
import org.objectweb.fractal.julia.conform.components.CCSParametricPrimitive;
import org.objectweb.fractal.julia.conform.components.CCSPrimitive;

public class TestGenericFactory extends AbstractTest {

  protected final static String AC = "attribute-controller/"+PKG+".CAttributes/false,false,false";
  protected final static String sI = "server/"+PKG+".I/false,false,false";
  protected final static String cI = "client/"+PKG+".I/true,false,false";

  // -------------------------------------------------------------------------
  // Test direct component creation
  // -------------------------------------------------------------------------

  @Test
  public void testFPrimitive () throws Exception {

	  Factory f = (Factory)
		  Class.forName(CCSFlatPrimitive.class.getName()+"Factory").
		  getConstructor().newInstance();
	  Component c = f.newFcInstance();

	checkComponent(c, new HashSet<String>(Arrays.asList(new String[] {
	  COMP, BC, LC, NC, sI, cI
	})));
  }

  @Test
  public void testFParametricPrimitive () throws Exception {

	  Factory f = (Factory)
		  Class.forName(CCSFlatParametricPrimitive.class.getName()+"Factory").
		  getConstructor().newInstance();
	  Component c = f.newFcInstance();

	checkComponent(c, new HashSet<String>(Arrays.asList(new String[] {
	  COMP, BC, LC, AC, NC, sI, cI
	})));
  }

  @Test
  public void testPrimitive () throws Exception {

	Factory f = (Factory)
		  Class.forName(CCSPrimitive.class.getName()+"Factory").
		  getConstructor().newInstance();
	Component c = f.newFcInstance();
	checkComponent(c, new HashSet<String>(Arrays.asList(new String[] {
	  COMP, BC, LC, SC, NC, sI, cI
	})));
  }

  @Test
  public void testParametricPrimitive () throws Exception {

	Factory f = (Factory)
		  Class.forName(CCSParametricPrimitive.class.getName()+"Factory").
		  getConstructor().newInstance();
	Component c = f.newFcInstance();
	checkComponent(c, new HashSet<String>(Arrays.asList(new String[] {
	  COMP, BC, LC, SC, AC, NC, sI, cI
	})));
  }

  @Test
  public void testComposite () throws Exception {

	  Factory f = (Factory)
		  Class.forName(CCSComposite.class.getName()+"Factory").
		  getConstructor().newInstance();
	Component c = f.newFcInstance();
	checkComponent(c, new HashSet<String>(Arrays.asList(new String[] {
	  COMP, BC, CC, LC, SC, NC, sI, cI
	})));
  }

  @Test
  public void testParametricComposite () throws Exception {

	  Factory f = (Factory)
		  Class.forName(CCSParametricComposite.class.getName()+"Factory").
		  getConstructor().newInstance();
	Component c = f.newFcInstance();
	checkComponent(c, new HashSet<String>(Arrays.asList(new String[] {
	  COMP, BC, CC, LC, SC, AC, NC, sI, cI
	})));
  }
}
