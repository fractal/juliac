/***
 * Julia
 * Copyright (C) 2005-2021 Inria, France Telecom, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.conform.components;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.types.Cardinality;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;

@Component
public class DCPrimitive implements K {

	private boolean v = false;

	@Requires(name="client")
	private K i;

	@Requires(name="clients",cardinality=Cardinality.COLLECTION)
	private Map<String,K> j = new HashMap<>();

	// K

	public boolean get() {
		return v;
	}

	public void set(boolean v) {
		this.v = v;
	}

}
