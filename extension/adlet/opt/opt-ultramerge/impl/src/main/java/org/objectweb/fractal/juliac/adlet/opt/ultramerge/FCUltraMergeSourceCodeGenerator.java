/***
 * Juliac
 * Copyright (C) 2018-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.opt.ultramerge;

import java.io.IOException;
import java.util.Map;

import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.juliac.adlet.adl.ADLClassToComponentDescAdlet;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.core.desc.ComponentDescAdlet;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.core.desc.ADLParserSupportItf;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;
import org.objectweb.fractal.juliac.fraclet.opt.ultramerge.FracletUltraMerge;
import org.objectweb.fractal.juliac.spoon.AbstractUltraMerge;

/**
 * Optimization level source code generator which merges all component business
 * code in a single class. This source code generator handles component business
 * code annotated with the Adlet framework.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public class FCUltraMergeSourceCodeGenerator implements ADLParserSupportItf {

	// -----------------------------------------------------------------------
	// Implementation of the JuliacModuleItf interface
	// -----------------------------------------------------------------------

	/** Suffix appended to generated classes. */
	public static final String SUFFIX = "UM";

	protected JuliacItf jc;

	@Override
	public void init( JuliacItf jc ) {
		this.jc = jc;
		jc.register(ADLParserSupportItf.class,this);
	}

	@Override
	public void close( JuliacItf jc ) {
		jc.unregister(ADLParserSupportItf.class,this);
	}


	// -----------------------------------------------------------------------
	// Implementation of the ADLParserSupportItf interface
	// -----------------------------------------------------------------------

	@Override
	public boolean test( String adl ) {

		/*
		 * Return true if this is a @Component-annotated class.
		 */
		Class<?> cl = jc.loadClass(adl);
		return cl.isAnnotationPresent(Component.class);
	}

	@Override
	public ComponentDesc<Class<?>> parse(
		String adl, Map<Object,Object> context )
	throws IOException {

		Class<Factory> cl = jc.loadClass(adl);
		ComponentDescAdlet<Class<?>> cdesc =
			ADLClassToComponentDescAdlet.get(cl,jc);

		return cdesc;
	}

	@Override
	public void generate( ComponentDesc<?> cdesc, String targetname )
	throws IOException {
		AbstractUltraMerge aum = new FracletUltraMerge(jc);
		aum.generate(cdesc,targetname+SUFFIX);
	}
}
