/***
 * Juliac
 * Copyright (C) 2018-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.control.component;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;
import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;
import org.objectweb.fractal.juliac.fraclet.runtime.comp.MPrimitiveImpl;
import org.objectweb.fractal.koch.control.binding.BindingControllerDef;
import org.objectweb.fractal.koch.control.content.SuperControllerDef;
import org.objectweb.fractal.koch.control.lifecycle.LifeCycleControllerDef;

import juliac.generated.ContainerBindingControllerImpl;

/**
 * Definition of the binding controller for primitive components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
@Component(
	provides=
		@Interface(
			name="//"+BindingControllerDef.NAME,
			signature=BindingController.class),
	mdesc=MPrimitiveImpl.class)
public class ContainerBindingController extends ContainerBindingControllerImpl {

	@Requires(name="//"+SuperControllerDef.NAME)
	private SuperControllerNotifier sc;

	@Requires(name="//"+LifeCycleControllerDef.NAME)
	private LifeCycleCoordinator lc;
}
