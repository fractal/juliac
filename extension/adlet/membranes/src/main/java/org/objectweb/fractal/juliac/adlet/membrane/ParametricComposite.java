/***
 * Juliac
 * Copyright (C) 2018-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.membrane;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;
import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;
import org.objectweb.fractal.juliac.adlet.api.BinderItf;
import org.objectweb.fractal.juliac.adlet.api.annotation.Binding;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;
import org.objectweb.fractal.juliac.adlet.api.annotation.StaticMetamodel;
import org.objectweb.fractal.juliac.adlet.control.component.ComponentController;
import org.objectweb.fractal.juliac.adlet.control.component.CompositeBindingController;
import org.objectweb.fractal.juliac.adlet.control.component.CompositeLifeCycleController;
import org.objectweb.fractal.juliac.adlet.control.component.ContentController;
import org.objectweb.fractal.juliac.adlet.control.component.NameController;
import org.objectweb.fractal.juliac.adlet.control.component.SuperController;
import org.objectweb.fractal.koch.control.binding.BindingControllerDef;
import org.objectweb.fractal.koch.control.component.ComponentControllerDef;
import org.objectweb.fractal.koch.control.content.ContentControllerDef;
import org.objectweb.fractal.koch.control.content.SuperControllerDef;
import org.objectweb.fractal.koch.control.lifecycle.LifeCycleControllerDef;
import org.objectweb.fractal.koch.control.name.NameControllerDef;
import org.objectweb.fractal.koch.factory.MCompositeImpl;

/**
 * Definition of the parametric composite membrane.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
@Component(
	provides= {
		@Interface(name="//"+ComponentControllerDef.NAME, signature=org.objectweb.fractal.api.Component.class),
		@Interface(name="//"+NameControllerDef.NAME, signature=org.objectweb.fractal.api.control.NameController.class),
		@Interface(name="//"+LifeCycleControllerDef.NAME, signature=LifeCycleCoordinator.class),
		@Interface(name="//"+BindingControllerDef.NAME, signature=BindingController.class),
		@Interface(name="//"+SuperControllerDef.NAME, signature=SuperControllerNotifier.class),
		@Interface(name="//"+ContentControllerDef.NAME, signature=org.objectweb.fractal.api.control.ContentController.class),
	}, mdesc=MCompositeImpl.class)
@StaticMetamodel("ParametricComposite_")
public class ParametricComposite {

	public static final String NAME = "parametricComposite";

	@Component(name="Comp")
	private ComponentController comp;

	@Component(name="NC")
	private NameController nc;

	@Component(name="LC")
	private CompositeLifeCycleController lc;

	@Component(name="BC")
	private CompositeBindingController bc;

	@Component(name="SC")
	private SuperController sc;

	@Component(name="CC")
	private ContentController cc;

	@Binding
	public static void binder( BinderItf binder ) {

		binder.export(ParametricComposite_.__component, ParametricComposite_.Comp.__component);
		binder.export(ParametricComposite_.__name_controller, ParametricComposite_.NC.__name_controller);
		binder.export(ParametricComposite_.__lifecycle_controller, ParametricComposite_.LC.__lifecycle_controller);
		binder.export(ParametricComposite_.__binding_controller, ParametricComposite_.BC.__binding_controller);
		binder.export(ParametricComposite_.__super_controller, ParametricComposite_.SC.__super_controller);
		binder.export(ParametricComposite_.__content_controller, ParametricComposite_.CC.__content_controller);

		binder.normal(ParametricComposite_.BC.__component, ParametricComposite_.Comp.__component);
		binder.normal(ParametricComposite_.BC.__super_controller, ParametricComposite_.SC.__super_controller);
		binder.normal(ParametricComposite_.BC.__lifecycle_controller, ParametricComposite_.LC.__lifecycle_controller);
		binder.normal(ParametricComposite_.BC.__content_controller, ParametricComposite_.CC.__content_controller);
		binder.normal(ParametricComposite_.LC.__component, ParametricComposite_.Comp.__component);
		binder.normal(ParametricComposite_.CC.__component, ParametricComposite_.Comp.__component);
		binder.normal(ParametricComposite_.CC.__lifecycle_controller, ParametricComposite_.LC.__lifecycle_controller);
	}
}
