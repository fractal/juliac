/***
 * Juliac
 * Copyright (C) 2018-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.membrane;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;
import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;
import org.objectweb.fractal.juliac.adlet.api.BinderItf;
import org.objectweb.fractal.juliac.adlet.api.annotation.Binding;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;
import org.objectweb.fractal.juliac.adlet.api.annotation.StaticMetamodel;
import org.objectweb.fractal.juliac.adlet.control.component.ComponentController;
import org.objectweb.fractal.juliac.adlet.control.component.ContainerBindingController;
import org.objectweb.fractal.juliac.adlet.control.component.LifeCycleController;
import org.objectweb.fractal.juliac.adlet.control.component.LifeCycleInterceptorController;
import org.objectweb.fractal.juliac.adlet.control.component.NameController;
import org.objectweb.fractal.juliac.adlet.control.component.SuperController;
import org.objectweb.fractal.koch.control.binding.BindingControllerDef;
import org.objectweb.fractal.koch.control.component.ComponentControllerDef;
import org.objectweb.fractal.koch.control.content.SuperControllerDef;
import org.objectweb.fractal.koch.control.interceptor.InterceptorController;
import org.objectweb.fractal.koch.control.lifecycle.LifeCycleControllerDef;
import org.objectweb.fractal.koch.control.name.NameControllerDef;
import org.objectweb.fractal.koch.factory.MCompositeImpl;

/**
 * Definition of the parametric primitive membrane.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
@Component(
	provides= {
		@Interface(name="//"+ComponentControllerDef.NAME, signature=org.objectweb.fractal.api.Component.class),
		@Interface(name="//"+NameControllerDef.NAME, signature=org.objectweb.fractal.api.control.NameController.class),
		@Interface(name="//"+LifeCycleControllerDef.NAME, signature=LifeCycleCoordinator.class),
		@Interface(name="//"+BindingControllerDef.NAME, signature=BindingController.class),
		@Interface(name="//"+SuperControllerDef.NAME, signature=SuperControllerNotifier.class),
		@Interface(name="//"+InterceptorController.NAME, signature=InterceptorController.class),
	}, mdesc=MCompositeImpl.class)
@StaticMetamodel("ParametricPrimitive_")
public class ParametricPrimitive {

	public static final String NAME = "parametricPrimitive";

	@Component(name="Comp")
	private ComponentController comp;

	@Component(name="NC")
	private NameController nc;

	@Component(name="LC")
	private LifeCycleController lc;

	@Component(name="BC")
	private ContainerBindingController bc;

	@Component(name="SC")
	private SuperController sc;

	@Component(name="IC")
	private LifeCycleInterceptorController ic;

	@Binding
	public static void binder( BinderItf binder ) {

		binder.export(ParametricPrimitive_.__component, ParametricPrimitive_.Comp.__component);
		binder.export(ParametricPrimitive_.__name_controller, ParametricPrimitive_.NC.__name_controller);
		binder.export(ParametricPrimitive_.__lifecycle_controller, ParametricPrimitive_.LC.__lifecycle_controller);
		binder.export(ParametricPrimitive_.__binding_controller, ParametricPrimitive_.BC.__binding_controller);
		binder.export(ParametricPrimitive_.__super_controller, ParametricPrimitive_.SC.__super_controller);
		binder.export(ParametricPrimitive_.___interceptor_controller, ParametricPrimitive_.IC.___interceptor_controller);

		binder.normal(ParametricPrimitive_.BC.__component, ParametricPrimitive_.Comp.__component);
		binder.normal(ParametricPrimitive_.BC.__super_controller, ParametricPrimitive_.SC.__super_controller);
		binder.normal(ParametricPrimitive_.BC.__lifecycle_controller, ParametricPrimitive_.LC.__lifecycle_controller);
		binder.normal(ParametricPrimitive_.LC.__component, ParametricPrimitive_.Comp.__component);
		binder.normal(ParametricPrimitive_.IC.__component, ParametricPrimitive_.Comp.__component);
		binder.normal(ParametricPrimitive_.IC.__lifecycle_controller, ParametricPrimitive_.LC.__lifecycle_controller);
		binder.normal(ParametricPrimitive_.IC.__binding_controller, ParametricPrimitive_.BC.__binding_controller);
	}
}
