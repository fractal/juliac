/***
 * Juliac
 * Copyright (C) 2019-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.adl;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.julia.BasicComponentInterface;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.helper.JuliacHelper;
import org.objectweb.fractal.juliac.core.visit.CodeHelper;
import org.objectweb.fractal.juliac.fraclet.core.AttributeControlSetterGetterGenerator;

/**
 * This class generates the implementation of Fractal attribute control
 * interfaces.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public class AttributeControlFcInterfaceImplementationClassGenerator
extends AttributeControlSetterGetterGenerator
{

	private String signature;

	public AttributeControlFcInterfaceImplementationClassGenerator(
		List<Field> attributeFields, String signature ) {

		super(attributeFields);
		this.signature = signature;
	}

	@Override
	public String getTargetTypeName() {
		String name =
			JuliacHelper.getJuliacGeneratedStrongTypeName(signature);
		String suffix = "FcItf";
		String targetClassName = name + suffix;
		return targetClassName;
	}

	@Override
	public String getSuperClassName() {
		return BasicComponentInterface.class.getName();
	}

	@Override
	public String[] getImplementedInterfaceNames() {
		return new String[]{signature};
	}

	@Override
	public void generateFields( ClassSourceCodeVisitor cv ) {
		cv.visitField( Modifier.PRIVATE, signature, "impl", null);
	}

	@Override
	public void generateConstructors( ClassSourceCodeVisitor cv ) {

		// public constructor()
		cv.visitConstructor(Modifier.PUBLIC, null, null, null).visitEnd();

		// public constructor(Component,String,Type,boolean,Object)
		String[] parameters = new String[] {
			Component.class.getName() + " component", "String s",
			Type.class.getName() + " type", "boolean flag", "Object obj" };
		BlockSourceCodeVisitor mv =
			cv.visitConstructor(Modifier.PUBLIC, null, parameters, null);
		mv.visitIns("super(component,s,type,flag,obj)").visitEnd();
	}

	@Override
	public void generateMethods( ClassSourceCodeVisitor cv ) {
		super.generateMethods(cv);
		generateMethodGetFcItfImpl(cv);
		generateMethodSetFcItfImpl(cv);
	}

	@Override
	protected void generateSetterMethod(
		ClassSourceCodeVisitor cv, Field attributeField, String setterName ) {

		Class<?> type = attributeField.getType();
		String typeName = type.getName();

		BlockSourceCodeVisitor mv =
			cv.visitMethod(
				Modifier.PUBLIC, null, "void", setterName,
				new String[]{typeName+" value"}, null);
		generateProxyMethodBodyBeforeCode(mv);
		mv.visitIns("impl."+setterName+"(value)");
		mv.visitEnd();
	}

	@Override
	protected void generateGetterMethod(
		ClassSourceCodeVisitor cv, Field attributeField, String getterName ) {

		Class<?> type = attributeField.getType();
		String typeName = type.getName();

		BlockSourceCodeVisitor mv =
			cv.visitMethod(
				Modifier.PUBLIC, null, typeName, getterName, null, null );
		generateProxyMethodBodyBeforeCode(mv);
		mv.visitIns("return","impl."+getterName+"()");
		mv.visitEnd();
	}

	private void generateProxyMethodBodyBeforeCode(BlockSourceCodeVisitor mv) {
		BlockSourceCodeVisitor then = mv.visitIf("impl","==","null");
		then.visitIns(
			CodeHelper._throw(
				NullPointerException.class,
				"\"Trying to invoke a method on a client or server interface whose complementary interface is not bound.\""));
		then.visitEnd();
	}

	private void generateMethodGetFcItfImpl( ClassSourceCodeVisitor cv ) {
		BlockSourceCodeVisitor mv = cv.visitMethod(Modifier.PUBLIC, null,
				"Object", "getFcItfImpl", null, null);
		mv.visitIns("return","impl").visitEnd();
	}

	private void generateMethodSetFcItfImpl( ClassSourceCodeVisitor cv ) {
		BlockSourceCodeVisitor mv = cv.visitMethod(Modifier.PUBLIC, null,
				"void", "setFcItfImpl", new String[] { "Object obj" }, null);
		mv.visitSet("impl","("+signature+")obj").visitEnd();
	}
}
