/***
 * Juliac
 * Copyright (C) 2019-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.adl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.types.Access;
import org.objectweb.fractal.fraclet.types.Cardinality;
import org.objectweb.fractal.fraclet.types.Contingency;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.adlet.api.annotation.Binding;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;
import org.objectweb.fractal.juliac.adlet.api.annotation.StaticMetamodel;
import org.objectweb.fractal.juliac.adlet.core.desc.ComponentDescAdlet;
import org.objectweb.fractal.juliac.adlet.core.helper.AnnotatedConstructHelper;
import org.objectweb.fractal.juliac.adlet.core.helper.TypeElementHelper;
import org.objectweb.fractal.juliac.adlet.core.helper.TypeMirrorHelper;
import org.objectweb.fractal.juliac.adlet.core.proxy.AttributeControlFcInterfaceImplementationClassGenerator;
import org.objectweb.fractal.juliac.adlet.core.proxy.AttributeControlImplementationGenerator;
import org.objectweb.fractal.juliac.adlet.core.proxy.AttributeControlInterfaceGenerator;
import org.objectweb.fractal.juliac.adlet.membrane.Composite;
import org.objectweb.fractal.juliac.adlet.membrane.ParametricComposite;
import org.objectweb.fractal.juliac.adlet.membrane.ParametricPrimitive;
import org.objectweb.fractal.juliac.adlet.membrane.Primitive;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.desc.AttributeDesc;
import org.objectweb.fractal.juliac.core.helper.InterfaceTypeHelper;

/**
 * Generate the {@link ComponentDescAdlet} instance corresponding to the ADL
 * definition represented by an instance of a {@link TypeElement}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public class ADLTypeElementToComponentDescAdlet {

	public static ComponentDescAdlet<TypeElement> get( TypeElement adl, JuliacItf jc )
	throws IOException {

		ADLTypeElementToComponentDescAdlet o =
			new ADLTypeElementToComponentDescAdlet(jc);
		ComponentDescAdlet<TypeElement> cdesc = o.toComponentDesc(adl);

		return cdesc;
	}

	private ADLTypeElementToComponentDescAdlet( JuliacItf jc ) {
		this.jc = jc;
		processingEnv = jc.getProcessingEnvironment();
	}

	private JuliacItf jc;
	private ProcessingEnvironment processingEnv;
	private int compidx = 0;

	private ComponentDescAdlet<TypeElement> toComponentDesc( TypeElement adl )
	throws IOException {

		final String id = "C"+(compidx++);
		final Elements elements = processingEnv.getElementUtils();
		final String fqname = elements.getBinaryName(adl).toString();

		final Object mm =
			AnnotatedConstructHelper.getAnnotationParamValue(
				adl,StaticMetamodel.class,"value",processingEnv);

		ComponentDescAdlet<TypeElement> cdesc =
			new ComponentDescAdlet<>(id,null,fqname,null,null,null,adl,mm);

		/*
		 * Retrieve elements from adl to compute the architecture description.
		 */
		List<? extends Element> members = TypeElementHelper.getAllMembers(adl);
		List<VariableElement> fields = ElementFilter.fieldsIn(members);
		List<TypeElement> subs = ElementFilter.typesIn(members);

		/*
		 * Store cdesc and its subcomponents in order to be able to retrieve
		 * components by the name of the @Component-annotated class when
		 * analyzing a binding.
		 */
		Map<String,ComponentDescAdlet<?>> mcomps = new HashMap<>();
		mcomps.put(fqname,cdesc);

		/*
		 * Component name.
		 */
		Component component = adl.getAnnotation(Component.class);
		String compname =
			component.name().length() == 0 ?
			adl.getSimpleName().toString() :
			component.name();
		cdesc.setName(compname);

		/*
		 * The name of the attribute control interface if some attribute are
		 * defined. This name is either provided by the developer as a provided
		 * interface in the component (provides attribute of the @Component
		 * annotation), or generated by AttributeControlInterfaceGenerator.
		 */
		String attributeControlInterfaceName = null;

		/*
		 * Retrieve provided and required interfaces.
		 */
		List<InterfaceType> lits = new ArrayList<>();

		// Provided interface types
		Interface[] interfaces = component.provides();
		for (Interface interf : interfaces) {
			String interfname = interf.name();
			String interfsignature;
			try {
				interfsignature = interf.signature().getName();
			}
			catch( MirroredTypeException mte ) {
				TypeMirror tm = mte.getTypeMirror();
				interfsignature = tm.toString();
			}
			if( interfname.equals("attribute-controller") ) {
				attributeControlInterfaceName = interfsignature;
			}
			boolean isOptional = interf.contingency().equals(Contingency.OPTIONAL);
			boolean isCollection = interf.cardinality().equals(Cardinality.COLLECTION);
			InterfaceType it =
				new BasicInterfaceType(
					interfname,interfsignature,false,isOptional,isCollection);
			lits.add(it);
		}

		// Required interface types
		for (VariableElement field : fields) {
			Requires requires = field.getAnnotation(Requires.class);
			if( requires != null ) {
				InterfaceType it = toInterfaceType(field);
				lits.add(it);
			}
		}

		/*
		 * Generate code related to attribute controllers.
		 */
		List<VariableElement> attributeFields = new ArrayList<>();
		for (VariableElement field : fields) {
			Attribute attribute = field.getAnnotation(Attribute.class);
			if( attribute != null ) {
				attributeFields.add(field);
			}
		}

		String contentClassName = null;
		if( attributeFields.isEmpty() ) {
			contentClassName = fqname;
		}
		else {

			// Check whether a attribute control interface has been declared
			if( attributeControlInterfaceName == null ) {

				// Generate the attribute control interface
				SourceCodeGeneratorItf acig =
					new AttributeControlInterfaceGenerator(
						adl, fqname.replace('$','_'), attributeFields,
						processingEnv );
				jc.generateSourceCode(acig);
				attributeControlInterfaceName = acig.getTargetTypeName();

				// Add the attribute control interface type to the component type
				InterfaceType it =
					new BasicInterfaceType(
						"attribute-controller", attributeControlInterfaceName,
						false, false, false );
				lits.add(it);
			}

			// Generate the attribute controller implementation
			SourceCodeGeneratorItf acg =
				new AttributeControlImplementationGenerator(
					adl, fqname.replace('$','_'), attributeFields,
					fqname.replace('$','.'), attributeControlInterfaceName,
					processingEnv );
			jc.generateSourceCode(acg);
			contentClassName = acg.getTargetTypeName();

			// Generate the Fractal interface implementation
			SourceCodeGeneratorItf fcscg =
				new AttributeControlFcInterfaceImplementationClassGenerator(
					adl, attributeFields, attributeControlInterfaceName,
					processingEnv );
			jc.generateSourceCode(fcscg);
		}

		/*
		 * Set the component type.
		 */
		InterfaceType[] its = lits.toArray( new InterfaceType[lits.size()] );
		ComponentType ct = InterfaceTypeHelper.newBasicComponentType(its);
		cdesc.setComponentType(ct);

		/*
		 * Subcomponents.
		 *
		 * @Component annotated inner classes. Inner classes must be static to
		 * enable the generated factory to instantiate it.
		 */
		for (TypeElement sub : subs) {
			Component subcomponent = sub.getAnnotation(Component.class);
			if( subcomponent != null ) {

				ComponentDescAdlet<TypeElement> subcdesc = toComponentDesc(sub);
				cdesc.addSubComponent(subcdesc);
				subcdesc.addSuperComponent(cdesc);
				mcomps.put(sub.toString(),subcdesc);
			}
		}

		/*
		 * Subcomponents and properties.
		 *
		 * @Component annotated fields.
		 * @Attribute annotated fields.
		 */
		Types types = processingEnv.getTypeUtils();
		for (VariableElement field : fields) {

			// Subcomponent
			Component subcomponent = field.getAnnotation(Component.class);
			if( subcomponent != null ) {

				TypeMirror tm = field.asType();
				Element e = types.asElement(tm);
				ComponentDescAdlet<TypeElement> subcdesc =
					toComponentDesc((TypeElement)e);
				cdesc.addSubComponent(subcdesc);
				subcdesc.addSuperComponent(cdesc);

				String subcdescname =
					subcomponent.name().length() == 0 ?
					field.getSimpleName().toString() :
					subcomponent.name();
				subcdesc.setName(subcdescname);
				mcomps.put(e.toString(),subcdesc);
			}

			// Attribute
			Attribute attribute = field.getAnnotation(Attribute.class);
			if( attribute != null ) {
				String aname =
					attribute.name().length() == 0 ?
					field.getSimpleName().toString() :
					attribute.name();
				TypeMirror type = field.asType();
				String value =
					attribute.value().length() == 0 ?
					TypeMirrorHelper.getNullValue(type) :
					attribute.value();
				String typeName = type.toString();
				String setterName =
					attribute.mode().equals(Access.READ_ONLY) ?
						null :
						attribute.set().length() == 0 ?
							"set"+aname.substring(0,1).toUpperCase()+aname.substring(1):
							attribute.set();
				AttributeDesc adesc =
					new AttributeDesc(
						cdesc, attributeControlInterfaceName, aname, value,
						typeName, setterName );
				cdesc.putAttribute(aname,adesc);
			}
		}

		/*
		 * Bindings.
		 */
		List<ExecutableElement> methods = ElementFilter.methodsIn(members);
		for (ExecutableElement method : methods) {
			Binding binding = method.getAnnotation(Binding.class);
			if( binding != null ) {
				handleBinding(fqname,method,cdesc);
			}
		}

		/*
		 * Membrane.
		 */
		String o =
			AnnotatedConstructHelper.getAnnotationParamValue(
				adl, Component.class, "mdesc", processingEnv).toString();

		// Membrane identifier
		if( o.equals("void") ) {

			// Default membrane definition
			if( cdesc.getBindingDescs().isEmpty() &&
				cdesc.getSubComponents().isEmpty() ) {

				// No binding, no subcomponent, the component is primitive
				if( cdesc.getAttributeNames().isEmpty() ) {
					// No attribute
					cdesc.setCtrlDesc("primitive");
					cdesc.setMembraneClass(Primitive.class);
				}
				else {
					// At least one attribute, the component is parametric
					cdesc.setCtrlDesc("parametricPrimitive");
					cdesc.setMembraneClass(ParametricPrimitive.class);
				}
			}
			else {

				// At least one binding or subcomponent, the component is
				// composite
				if( cdesc.getAttributeNames().isEmpty() ) {
					// No attribute
					cdesc.setCtrlDesc("composite");
					cdesc.setMembraneClass(Composite.class);
				}
				else {
					// At least one attribute, the component is parametric
					cdesc.setCtrlDesc("parametricComposite");
					cdesc.setMembraneClass(ParametricComposite.class);
				}
			}
		}
		else {

			// Custom membrane definition
			String[] parts = o.split("\\.");
			String sname = parts[parts.length-1];
			String mid =
				sname.equals("MPrimitiveImpl") ? "mPrimitive" :
				sname.equals("MCompositeImpl") ? "mComposite" :
				sname.substring(0,1).toLowerCase()+sname.substring(1);
			cdesc.setCtrlDesc(mid);

			if( ! mid.equals("mPrimitive") && ! mid.equals("mComposite") ) {
				Class<?> cl = jc.loadClass(o.toString());
				cdesc.setMembraneClass(cl);
			}
		}

		/*
		 * Component content class name.
		 */
		if( cdesc.getBindingDescs().isEmpty() &&
			cdesc.getSubComponents().isEmpty() ) {

			// No binding, no subcomponent, the component is primitive
			cdesc.setContentClassName(contentClassName);
		}
		else {

			// At least one binding or subcomponent, the component is composite
			if( cdesc.getAttributeNames().isEmpty() ) {
				cdesc.setContentClassName(null);
			}
			else {
				cdesc.setContentClassName(contentClassName);
			}
		}

		return cdesc;
	}

	/**
	 * Handle a @{@link Binding}-annotated method.
	 *
	 * @param fqname  the fully-qualified name of the class defining the method
	 * @param method  the @{@link Binding}-annotated method
	 * @param cdesc   the component descriptor where the binding needs to be registered
	 */
	private void handleBinding(
		String fqname, ExecutableElement method,
		ComponentDescAdlet<TypeElement> cdesc ) {

		String fqmn = fqname+'.'+method.getSimpleName();
		cdesc.addBinderMethodName(fqmn);
	}

	/**
	 * Return the interface type associated with the specified @{@link Requires}
	 * annotated field.
	 *
	 * @param field  the field
	 * @return       the interface type
	 */
	private InterfaceType toInterfaceType( VariableElement field ) {

		Requires requires = field.getAnnotation(Requires.class);

		String name =
			requires.name().length() == 0 ?
				field.getSimpleName().toString() :
				requires.name();
		boolean isOptional = requires.contingency().isOptional();
		boolean isCollection = requires.cardinality().isCollection();

		TypeMirror typemirror = field.asType();
		if( isCollection ) {
			List<? extends TypeMirror> typeParameters =
				((DeclaredType) typemirror).getTypeArguments();
			typemirror = typeParameters.get(1);
		}

		String signature = typemirror.toString();
		InterfaceType it =
			new BasicInterfaceType(name,signature,true,isOptional,isCollection);
		return it;
	}
}
