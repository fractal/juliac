/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.adl;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;

import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.core.AbstractJuliacProcessor;
import org.objectweb.fractal.juliac.adlet.core.desc.ComponentDescAdlet;
import org.objectweb.fractal.juliac.adlet.core.desc.ComponentDescFactoryClassGenerator;
import org.objectweb.fractal.juliac.api.JuliacRuntimeException;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;

/**
 * This Java annotation processor handles Adlet component assembly descriptors.
 * An Adlet component assembly descriptor is a @{@link Component}-annotated Java
 * class that describes an component assembly.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public class AdletProcessor extends AbstractJuliacProcessor {

	// -------------------------------------------------------------------
	// Implementation of the AbstractProcessor methods
	// -------------------------------------------------------------------

	@Override
	 public Set<String> getSupportedAnnotationTypes() {
		Set<String> annotations = new LinkedHashSet<>();
		annotations.add(Component.class.getCanonicalName());
		return annotations;
	}

	@Override
	public SourceVersion getSupportedSourceVersion() {
		return SourceVersion.latestSupported();
	}

	@Override
	public boolean process(
		Set<? extends TypeElement> annotations, RoundEnvironment roundEnv ) {

		initJuliac(processingEnv);

		Set<? extends Element> elements =
			roundEnv.getElementsAnnotatedWith(Component.class);

		for (Element element : elements) {
			if( element.getKind() == ElementKind.CLASS ) {
				try { process((TypeElement)element); }
				catch( IOException ioe ) {
					throw new JuliacRuntimeException(ioe);
				}
			}
		}

		closeJuliac();

		return true;
	}


	// -------------------------------------------------------------------
	// Implementation specific
	// -------------------------------------------------------------------

	/**
	 * Generate the source code for a @{@link Component}-annotated class.
	 *
	 * @param element  the @{@link Component}-annotated class
	 */
	private void process( TypeElement element )
	throws IOException {

		final ComponentDescAdlet<TypeElement> cdesc =
			ADLTypeElementToComponentDescAdlet.get(element,jc);

		final Elements elements = processingEnv.getElementUtils();
		final String targetname = elements.getBinaryName(element)+"Factory";
		final SourceCodeGeneratorItf cdfcg =
			new ComponentDescFactoryClassGenerator(jc,cdesc,targetname,element);

		jc.generateSourceCode(cdfcg);
	}

}
