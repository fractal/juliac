/***
 * Juliac
 * Copyright (C) 2019-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.adl;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.TypeMirror;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.types.Access;
import org.objectweb.fractal.fraclet.types.Cardinality;
import org.objectweb.fractal.fraclet.types.Contingency;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.adlet.api.BinderItf;
import org.objectweb.fractal.juliac.adlet.api.annotation.Binding;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;
import org.objectweb.fractal.juliac.adlet.api.annotation.StaticMetamodel;
import org.objectweb.fractal.juliac.adlet.core.desc.BinderImpl;
import org.objectweb.fractal.juliac.adlet.core.desc.ComponentDescAdlet;
import org.objectweb.fractal.juliac.adlet.membrane.Composite;
import org.objectweb.fractal.juliac.adlet.membrane.ParametricComposite;
import org.objectweb.fractal.juliac.adlet.membrane.ParametricPrimitive;
import org.objectweb.fractal.juliac.adlet.membrane.Primitive;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.commons.lang.ClassHelper;
import org.objectweb.fractal.juliac.core.desc.AttributeDesc;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;
import org.objectweb.fractal.juliac.core.helper.InterfaceTypeHelper;
import org.objectweb.fractal.juliac.fraclet.core.AttributeControlImplementationGenerator;
import org.objectweb.fractal.juliac.fraclet.core.AttributeControlInterfaceGenerator;

/**
 * Generate the {@link ComponentDescAdlet} instance corresponding to the ADL
 * definition represented by an instance of a {@link Class}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.8
 */
public class ADLClassToComponentDescAdlet {

	public static ComponentDescAdlet<Class<?>> get( Class<?> adl, JuliacItf jc )
	throws IOException {

		ADLClassToComponentDescAdlet o = new ADLClassToComponentDescAdlet(jc);
		ComponentDescAdlet<Class<?>> cdesc = o.toComponentDesc(adl);
		o.buildBindings(adl,cdesc);

		return cdesc;
	}

	private ADLClassToComponentDescAdlet( JuliacItf jc ) {
		this.jc = jc;
	}

	private JuliacItf jc;
	private int compidx = 0;

	private ComponentDescAdlet<Class<?>> toComponentDesc( Class<?> adl )
	throws IOException {

		final String id = "C"+(compidx++);
		final String fqname = adl.getName();

		final StaticMetamodel sm = adl.getAnnotation(StaticMetamodel.class);
		final String mm = sm==null ? null : sm.value();

		ComponentDescAdlet<Class<?>> cdesc =
			new ComponentDescAdlet<>(id,null,fqname,null,null,null,adl,mm);

		/*
		 * Retrieve elements from adl to compute the architecture description.
		 */
		Field[] fields = ClassHelper.getAllFields(adl);
		Class<?>[] subs = ClassHelper.getAllClasses(adl);

		/*
		 * Store cdesc and its subcomponents in order to be able to retrieve
		 * components by the name of the @Component-annotated class when
		 * analyzing a binding.
		 */
		Map<String,ComponentDescAdlet<?>> mcomps = new HashMap<>();
		mcomps.put(fqname,cdesc);

		/*
		 * Component name.
		 */
		Component component = adl.getAnnotation(Component.class);
		String compname =
			component.name().length() == 0 ?
			adl.getSimpleName().toString() :
			component.name();
		cdesc.setName(compname);

		/*
		 * The name of the attribute control interface if some attribute are
		 * defined. This name is either provided by the developer as a provided
		 * interface in the component (provides attribute of the @Component
		 * annotation), or generated by AttributeControlInterfaceGenerator.
		 */
		String attributeControlInterfaceName = null;

		/*
		 * Retrieve provided and required interfaces.
		 */
		List<InterfaceType> lits = new ArrayList<>();

		// Provided interface types
		Interface[] interfaces = component.provides();
		for (Interface interf : interfaces) {
			String interfname = interf.name();
			String interfsignature;
			try {
				interfsignature = interf.signature().getName();
			}
			catch( MirroredTypeException mte ) {
				TypeMirror tm = mte.getTypeMirror();
				interfsignature = tm.toString();
			}
			if( interfname.equals("attribute-controller") ) {
				attributeControlInterfaceName = interfsignature;
			}
			boolean isOptional = interf.contingency().equals(Contingency.OPTIONAL);
			boolean isCollection = interf.cardinality().equals(Cardinality.COLLECTION);
			InterfaceType it =
				new BasicInterfaceType(
					interfname,interfsignature,false,isOptional,isCollection);
			lits.add(it);
		}

		// Required interface types
		for (Field field : fields) {
			Requires requires = field.getAnnotation(Requires.class);
			if( requires != null ) {
				InterfaceType it = toInterfaceType(field);
				lits.add(it);
			}
		}

		/*
		 * Generate code related to attribute controllers.
		 */
		List<Field> attributeFields = new ArrayList<>();
		for (Field field : fields) {
			Attribute attribute = field.getAnnotation(Attribute.class);
			if( attribute != null ) {
				attributeFields.add(field);
			}
		}

		String contentClassName = null;
		if( attributeFields.isEmpty() ) {
			contentClassName = fqname;
		}
		else {

			// Check whether a attribute control interface has been declared
			if( attributeControlInterfaceName == null ) {

				// Generate the attribute control interface
				SourceCodeGeneratorItf acig =
					new AttributeControlInterfaceGenerator(
						fqname.replace('$','_'), attributeFields );
				jc.generateSourceCode(acig);
				attributeControlInterfaceName = acig.getTargetTypeName();

				// Add the attribute control interface type to the component type
				InterfaceType it =
					new BasicInterfaceType(
						"attribute-controller", attributeControlInterfaceName,
						false, false, false );
				lits.add(it);
			}

			// Generate the attribute controller implementation
			SourceCodeGeneratorItf acg =
				new AttributeControlImplementationGenerator(
					fqname.replace('$','_'), attributeFields,
					fqname.replace('$','.'), attributeControlInterfaceName );
			jc.generateSourceCode(acg);
			contentClassName = acg.getTargetTypeName();

			// Generate the Fractal interface implementation
			SourceCodeGeneratorItf fcscg =
				new AttributeControlFcInterfaceImplementationClassGenerator(
					attributeFields, attributeControlInterfaceName );
			jc.generateSourceCode(fcscg);
		}

		/*
		 * Set the component type.
		 */
		InterfaceType[] its = lits.toArray( new InterfaceType[lits.size()] );
		ComponentType ct = InterfaceTypeHelper.newBasicComponentType(its);
		cdesc.setComponentType(ct);

		/*
		 * Subcomponents.
		 *
		 * @Component annotated inner classes. Inner classes must be static to
		 * enable the generated factory to instantiate it.
		 */
		for (Class<?> sub : subs) {
			Component subcomponent = sub.getAnnotation(Component.class);
			if( subcomponent != null ) {

				ComponentDescAdlet<Class<?>> subcdesc = toComponentDesc(sub);
				cdesc.addSubComponent(subcdesc);
				subcdesc.addSuperComponent(cdesc);
				mcomps.put(sub.toString(),subcdesc);
			}
		}

		/*
		 * Subcomponents and properties.
		 *
		 * @Component annotated fields.
		 * @Attribute annotated fields.
		 */
		for (Field field : fields) {

			// Subcomponent
			Component subcomponent = field.getAnnotation(Component.class);
			if( subcomponent != null ) {

				Class<?> sub = field.getType();
				ComponentDescAdlet<Class<?>> subcdesc = toComponentDesc(sub);
				cdesc.addSubComponent(subcdesc);
				subcdesc.addSuperComponent(cdesc);

				String subcdescname =
					subcomponent.name().length() == 0 ?
					field.getName().toString() :
					subcomponent.name();
				subcdesc.setName(subcdescname);
				mcomps.put(sub.getName(),subcdesc);
			}

			// Attribute
			Attribute attribute = field.getAnnotation(Attribute.class);
			if( attribute != null ) {
				String aname =
					attribute.name().length() == 0 ?
					field.getName().toString() :
					attribute.name();
				Class<?> type = field.getType();
				String value =
					attribute.value().length() == 0 ?
					ClassHelper.getNullValue(type) :
					attribute.value();
				String typeName = type.toString();
				String setterName =
					attribute.mode().equals(Access.READ_ONLY) ?
						null :
						attribute.set().length() == 0 ?
							"set"+aname.substring(0,1).toUpperCase()+aname.substring(1):
							attribute.set();
				AttributeDesc adesc =
					new AttributeDesc(
						cdesc, attributeControlInterfaceName, aname, value,
						typeName, setterName );
				cdesc.putAttribute(aname,adesc);
			}
		}

		/*
		 * Bindings.
		 */
		Method[] methods = ClassHelper.getAllMethods(adl);
		for (Method method : methods) {
			Binding binding = method.getAnnotation(Binding.class);
			if( binding != null ) {
				handleBinding(fqname,method,cdesc);
			}
		}

		/*
		 * Membrane.
		 */
		Component comp = adl.getAnnotation(Component.class);
		Class<?> mdesc = comp.mdesc();

		// Membrane identifier
		if( mdesc.equals(void.class) ) {

			// Default membrane definition
			if( cdesc.getBindingDescs().isEmpty() &&
				cdesc.getSubComponents().isEmpty() ) {

				// No binding, no subcomponent, the component is primitive
				if( cdesc.getAttributeNames().isEmpty() ) {
					// No attribute
					cdesc.setCtrlDesc("primitive");
					cdesc.setMembraneClass(Primitive.class);
				}
				else {
					// At least one attribute, the component is parametric
					cdesc.setCtrlDesc("parametricPrimitive");
					cdesc.setMembraneClass(ParametricPrimitive.class);
				}
			}
			else {

				// At least one binding or subcomponent, the component is
				// composite
				if( cdesc.getAttributeNames().isEmpty() ) {
					// No attribute
					cdesc.setCtrlDesc("composite");
					cdesc.setMembraneClass(Composite.class);
				}
				else {
					// At least one attribute, the component is parametric
					cdesc.setCtrlDesc("parametricComposite");
					cdesc.setMembraneClass(ParametricComposite.class);
				}
			}
		}
		else {

			// Custom membrane definition
			String sname = mdesc.getSimpleName();
			String mid =
				sname.equals("MPrimitiveImpl") ? "mPrimitive" :
				sname.equals("MCompositeImpl") ? "mComposite" :
				sname.substring(0,1).toLowerCase()+sname.substring(1);
			cdesc.setCtrlDesc(mid);

			if( ! mid.equals("mPrimitive") && ! mid.equals("mComposite") ) {
				cdesc.setMembraneClass(mdesc);
			}
		}

		/*
		 * Component content class name.
		 */
		if( cdesc.getBindingDescs().isEmpty() &&
			cdesc.getSubComponents().isEmpty() ) {

			// No binding, no subcomponent, the component is primitive
			cdesc.setContentClassName(contentClassName);
		}
		else {

			// At least one binding or subcomponent, the component is composite
			if( cdesc.getAttributeNames().isEmpty() ) {
				cdesc.setContentClassName(null);
			}
			else {
				cdesc.setContentClassName(contentClassName);
			}
		}

		return cdesc;
	}

	/**
	 * Handle a @{@link Binding}-annotated method.
	 *
	 * @param fqname  the fully-qualified name of the class defining the method
	 * @param method  the @{@link Binding}-annotated method
	 * @param cdesc   the component descriptor where the binding needs to be registered
	 */
	private void handleBinding(
		String fqname, Method method, ComponentDescAdlet<Class<?>> cdesc ) {

		String fqmn = fqname+'.'+method.getName();
		cdesc.addBinderMethodName(fqmn);
	}

	/**
	 * Return the interface type associated with the specified @{@link Requires}
	 * annotated field.
	 *
	 * @param field  the field
	 * @return       the interface type
	 */
	private InterfaceType toInterfaceType( Field field ) {

		Requires requires = field.getAnnotation(Requires.class);

		String name =
			requires.name().length() == 0 ?
				field.getName().toString() :
				requires.name();
		boolean isOptional = requires.contingency().isOptional();
		boolean isCollection = requires.cardinality().isCollection();

		Class<?> typemirror = field.getType();
		String signature;
		if( isCollection ) {
			TypeVariable<?>[] typeParameters = typemirror.getTypeParameters();
			signature = typeParameters[1].getTypeName();
		}
		else {
			signature = typemirror.toString();
		}

		InterfaceType it =
			new BasicInterfaceType(name,signature,true,isOptional,isCollection);
		return it;
	}
	/**
	 * Traverse the specified component descriptor and record the associations
	 * between the metamodel classes and their corresponding component or
	 * subcomponent descriptors.
	 *
	 * @param cdesc  the component descriptor
	 * @param mm     the name of the metamodel class
	 * @param map    the map storing associations between metamodel classes and descriptors
	 */
	private void buildMMMap(
		ComponentDesc<Class<?>> cdesc, String mm, Map<Class<?>,ComponentDesc<?>> map ) {

		// Record the current metamodel class
		Class<?> mmcl = jc.loadClass(mm);
		map.put(mmcl,cdesc);

		// Traverse subcomponents
		List<ComponentDesc<Class<?>>> subs = cdesc.getSubComponents();
		for (ComponentDesc<Class<?>> sub : subs) {

			String subname = sub.getName();
			String submmclname = mm+'$'+subname;
			buildMMMap(sub,submmclname,map);

			Object submm = ((ComponentDescAdlet<?>)sub).getMetamodelClass();
			if( submm != null ) {
				// @StaticMetamodel annotated class
				String subclname = sub.getDefinition();
				Class<?> subcl = jc.loadClass(subclname);
				submmclname = subcl.getPackageName()+'.'+submm;
				buildMMMap(sub,submmclname,map);
			}
		}
	}

	/**
	 * Traverse the specified component descriptor and record the bindings in
	 * the specified binder.
	 */
	private void buildBinder(
		ComponentDescAdlet<Class<?>> cdesc, Class<?> cl, BinderImpl binder )
	throws IOException {

		// Invoke binder methods
		List<String> bmns = cdesc.getBinderMethodNames();
		for (String bmn : bmns) {
			String name = bmn.substring(bmn.lastIndexOf('.')+1);
			invokeBinderMethod(cl,name,binder);
		}

		// Traverse subcomponents
		List<ComponentDesc<Class<?>>> subs = cdesc.getSubComponents();
		for (ComponentDesc<Class<?>> sub : subs) {
			String subclname = sub.getDefinition();
			Class<?> subcl = jc.loadClass(subclname);
			buildBinder((ComponentDescAdlet<Class<?>>)sub,subcl,binder);
		}
	}

	/**
	 * Invoke on the specified class, the static (BinderItf):void method whose
	 * name is specified.
	 */
	private void invokeBinderMethod( Class<?> cl, String name, BinderItf binder )
	throws IOException {
		try {
			Method m = cl.getMethod(name,BinderItf.class);
			m.invoke(null,binder);
		}
		catch(
			InvocationTargetException | NoSuchMethodException |
			IllegalAccessException e ) {

			throw new IOException(e);
		}
	}
	/**
	 * Build bindings by invoking the binder methods.
	 */
	private void buildBindings( Class<?> adl, ComponentDescAdlet<Class<?>> cdesc )
	throws IOException {

		Map<Class<?>,ComponentDesc<?>> map = new HashMap<>();
		String mm = adl.getPackageName()+'.'+cdesc.getMetamodelClass();
		buildMMMap(cdesc,mm,map);

		BinderImpl binder = new BinderImpl();
		buildBinder(cdesc,adl,binder);
		binder.record(map);
	}
}
