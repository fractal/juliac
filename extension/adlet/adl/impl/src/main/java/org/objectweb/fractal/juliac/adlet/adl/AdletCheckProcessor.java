/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.adlet.adl;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic.Kind;

import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.types.Access;
import org.objectweb.fractal.fraclet.types.Constants;
import org.objectweb.fractal.juliac.adlet.api.BinderItf;
import org.objectweb.fractal.juliac.adlet.api.annotation.Binding;
import org.objectweb.fractal.juliac.adlet.api.annotation.Component;
import org.objectweb.fractal.juliac.adlet.api.annotation.Interface;
import org.objectweb.fractal.juliac.adlet.core.helper.AnnotatedConstructHelper;
import org.objectweb.fractal.juliac.adlet.core.helper.ExecutableElementHelper;
import org.objectweb.fractal.juliac.commons.lang.ClassHelper;

/**
 * This Java annotation processor performs consistency checks on Adlet component
 * assembly descriptors.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public class AdletCheckProcessor extends AbstractProcessor {

	// -------------------------------------------------------------------
	// Implementation of the AbstractProcessor methods
	// -------------------------------------------------------------------

	@Override
	 public Set<String> getSupportedAnnotationTypes() {
		Set<String> annotations = new LinkedHashSet<>();
		annotations.add(Component.class.getCanonicalName());
		return annotations;
	}

	@Override
	public SourceVersion getSupportedSourceVersion() {
		return SourceVersion.latestSupported();
	}

	@Override
	public boolean process(
		Set<? extends TypeElement> annotations, RoundEnvironment roundEnv ) {

		Set<? extends Element> elements =
			roundEnv.getElementsAnnotatedWith(Component.class);

		for (Element element : elements) {
			if( element.getKind() == ElementKind.CLASS ) {
				process((TypeElement)element);
			}
		}

		return false;
	}


	// -------------------------------------------------------------------
	// Implementation specific
	// -------------------------------------------------------------------

	/**
	 * Perform consistency checks.
	 *
	 * @param adl  the @{@link Component}-annotated class
	 */
	private void process( TypeElement adl ) {

		Elements elements = processingEnv.getElementUtils();
		Messager messager = processingEnv.getMessager();

		List<? extends Element> members = elements.getAllMembers(adl);
		List<VariableElement> fields = ElementFilter.fieldsIn(members);
		List<TypeElement> subs = ElementFilter.typesIn(members);

		/*
		 * Provided interfaces.
		 *
		 * Check that at most one provided attribute-controller interface is
		 * declared.
		 */
		String attributeControlInterfaceName = null;
		Component component = adl.getAnnotation(Component.class);
		Interface[] interfaces = component.provides();
		for (Interface interf : interfaces) {
			String interfname = interf.name();
			if( interfname.length() == 0 ) {
				final String msg = "Empty provided interface name";
				messager.printMessage(Kind.ERROR,msg,adl);
			}
			String interfsignature;
			try {
				interfsignature = interf.signature().getName();
			}
			catch( MirroredTypeException mte ) {
				TypeMirror tm = mte.getTypeMirror();
				interfsignature = tm.toString();
			}
			if( interfname.equals("attribute-controller") ) {
				if( attributeControlInterfaceName != null ) {
					final String msg =
						"Only one attribute-controller interface allowed in "+
						adl+". Got: "+attributeControlInterfaceName+" and "+
						interfsignature;
					messager.printMessage(Kind.ERROR,msg,adl);
				}
				attributeControlInterfaceName = interfsignature;
				checkAttributeControlInterface(
					attributeControlInterfaceName,fields);
			}
		}

		/*
		 * Required interfaces.
		 */
		for (VariableElement field : fields) {
			Requires requires = field.getAnnotation(Requires.class);
			if( requires != null ) {
				checkRequiredInterfaceField(field);
			}
		}

		/*
		 * Subcomponents.
		 *
		 * @Component annotated inner classes. Check that inner classes are
		 * static. This is required to enable the generated factory to
		 * instantiate it.
		 */
		for (TypeElement sub : subs) {
			Component subcomponent = sub.getAnnotation(Component.class);
			if( subcomponent != null ) {

				Set<Modifier> modifiers = sub.getModifiers();
				if( ! modifiers.contains(Modifier.STATIC) ) {
					final String msg =
						"Inner class "+sub.toString()+
						" must be declared static to enable the generated "+
						"factory to instantiate it";
					messager.printMessage(Kind.ERROR,msg,sub);
				}
			}
		}

		/*
		 * Bindings.
		 */
		List<ExecutableElement> methods = ElementFilter.methodsIn(members);
		for (ExecutableElement method : methods) {
			Binding binding = method.getAnnotation(Binding.class);
			if( binding != null ) {

				// Check that the signature is static (BinderItf): void
				boolean isStatic = method.getModifiers().contains(Modifier.STATIC);
				boolean returnsVoid = method.getReturnType().toString().equals("void");
				boolean oneArg = method.getParameters().size() == 1;
				boolean binderItfArg =
					oneArg ?
					method.getParameters().get(0).asType().toString().equals(BinderItf.class.getName()) :
					false;
				if( !isStatic || !returnsVoid || !oneArg || !binderItfArg ) {
					final String msg =
						"The signature of a @"+Binding.class.getName()+
						" annotated method should be static ("+
						BinderItf.class.getName()+"(): void "+method.getParameters().get(0).asType();
					messager.printMessage(Kind.ERROR,msg,method);
				}
			}
		}
	}

	/**
	 * Check the consistency of the specified attribute control interface.
	 *
	 * @param name    the attribute control interface name
	 * @param fields  the fields defined in the source type
	 */
	private void checkAttributeControlInterface(
		String name, List<VariableElement> fields ) {

		Elements elements = processingEnv.getElementUtils();
		Types types = processingEnv.getTypeUtils();
		Messager messager = processingEnv.getMessager();

		/*
		 * Retrieve setter and getter methods defined in interface name.
		 */
		TypeElement te = elements.getTypeElement(name);
		List<? extends Element> members = elements.getAllMembers(te);
		List<ExecutableElement> methods = ElementFilter.methodsIn(members);

		/*
		 * Retrieve @Attribute annotated fields.
		 */
		for (VariableElement field : fields) {
			Attribute attr = field.getAnnotation(Attribute.class);
			if( attr != null ) {
				Access access = attr.mode();
				String attrname =
					attr.name().length() == 0 ?
					field.getSimpleName().toString() :
					attr.name();
				attrname =
					attrname.substring(0,1).toUpperCase() +
					attrname.substring(1);
				TypeMirror attrtype = field.asType();

				/*
				 * Check that the attribute control interface defines the getter
				 * method corresponding to the attribute.
				 */
				if( access.equals(Access.READ_ONLY) ||
					access.equals(Access.READ_WRITE) ) {

					String gettername =
						attr.get().length() == 0 ? "get"+attrname : attr.get();
					ExecutableElement getter =
						ExecutableElementHelper.getGetterMethod(
							methods, gettername, attrtype, types);
					if( getter == null ) {
						String msg = "No getter method "+gettername+" in "+name;
						messager.printMessage(Kind.ERROR,msg,field);
					}
				}

				/*
				 * Check that the attribute control interface defines the setter
				 * method corresponding to the attribute.
				 */
				if( access.equals(Access.WRITE_ONLY) ||
					access.equals(Access.READ_WRITE) ) {

					String settername =
						attr.set().length() == 0 ? "set"+attrname : attr.set();
					ExecutableElement setter =
						ExecutableElementHelper.getSetterMethod(
							methods, settername, attrtype, types);
					if( setter == null ) {
						String msg = "No setter method "+settername+" in "+name;
						messager.printMessage(Kind.ERROR,msg,field);
					}
				}
			}
		}
	}

	/**
	 * Check the consistency of the specified @{@link Requires}-annotated field.
	 */
	private void checkRequiredInterfaceField( VariableElement field ) {

		Messager messager = processingEnv.getMessager();
		Requires requires = field.getAnnotation(Requires.class);

		/*
		 * The default value for requires.signature() is Constant.class.
		 * When trying to access requires.signatures(), the JVM complains at
		 * runtime that "Attempt to access Class object for TypeMirror". Hence
		 * the following instruction to retrieve it as a TypeMirror instead.
		 */
		TypeMirror requiresSignature = (TypeMirror)
			AnnotatedConstructHelper.getAnnotationParamValue(
				field, Requires.class, "signature", processingEnv );

		boolean isCollection = requires.cardinality().isCollection();
		boolean isExplicit =
			! requiresSignature.toString().equals(Constants.class.getName());

		TypeMirror typemirror = field.asType();
		Elements elements = processingEnv.getElementUtils();
		Types types = processingEnv.getTypeUtils();
		if( isCollection ) {

			// Check that field is of type Map
			TypeMirror tmMap = ClassHelper.getTypeMirror(Map.class,elements);
			tmMap = types.erasure(tmMap);
			boolean isSameType =
				types.isSameType(tmMap,types.erasure(typemirror));
			if( ! isSameType ) {
				final String msg =
					"Field "+field+" for collection interface expected to be "+
					"of type java.util.Map<String,V>. Got instead: "+typemirror;
				messager.printMessage(Kind.ERROR,msg,field);
			}

			if( typemirror instanceof DeclaredType ) {

				List<? extends TypeMirror> typeParameters =
					((DeclaredType) typemirror).getTypeArguments();

				// Check that field is of type Map<K,V>
				if( typeParameters.size() != 2 ) {
					final String msg =
						"Field "+field+" for collection interface expected to "+
						"be of type java.util.Map<String,V>. Got instead: "+
						typemirror;
					messager.printMessage(Kind.ERROR,msg,field);
				}

				// Check that field is of type Map<String,V>
				TypeMirror tmString =
					ClassHelper.getTypeMirror(String.class,elements);
				isSameType = types.isAssignable(typeParameters.get(0),tmString);
				if( ! isSameType ) {
					final String msg =
						"Field "+field+" for collection interface expected to "+
						"be of type java.util.Map<String,V>. Got instead: "+
						typeParameters.get(0);
					messager.printMessage(Kind.ERROR,msg,field);
				}

				typemirror = typeParameters.get(1);
			}
			else {
				final String msg =
					"Field "+field+" for collection interface expected to be "+
					"of type java.util.Map<String,V>. Got instead: "+typemirror;
				messager.printMessage(Kind.ERROR,msg,field);
			}
		}

		if( isExplicit ) {
			// Check that requiresSignature is the same as typemirror
			boolean isAssignable =
				types.isAssignable(requiresSignature,typemirror);
			if( ! isAssignable ) {
				String msg = "Field "+field+" for ";
				msg += isCollection ?
					"collection interface expected to be of type "+
					"java.util.Map<String,"+requiresSignature+">" :
					"singleton interface expected to be of type "+
					requiresSignature;
				msg += ". Got instead: "+typemirror;
				messager.printMessage(Kind.ERROR,msg,field);
			}
		}
	}
}
