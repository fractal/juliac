# Adlet

Adlet is a Java-based architecture description language (ADL). Adlet enables to describe in pure Java syntax a software architecture. Two main patterns are supported: composite component, and typed binding. The composite component pattern enables to describe a hierarchy of parent and sub components. Typed bindings enable to describe the communication links between provided and required component interfaces. These two patterns are implemented in pure Java syntax. This means that an Adlet software architecture descriptor is a fully legal Java program with no particular language extension. This enables reusing the full stack of tools (unit testing, processing, editing, compiling, etc.) available for this language. As such Adlet can be seen a programming style for describing software architectures with the Java language.

More information on Adlet can be found in this report: Lionel Seinturier, "[Adlet: A Java-based Architecture Description Language](https://hal.inria.fr/hal-01964792)", Inria Research Report RR-9242, December 2018. If you use Adlet for academic purposes, this report can be cited as:

```
@techreport{seinturier:hal-01964792,
  TITLE = {{Adlet: A Java-based Architecture Description Language}},
  AUTHOR = {Seinturier, Lionel},
  URL = {https://hal.inria.fr/hal-01964792},
  TYPE = {Research Report},
  NUMBER = {RR-9242},
  INSTITUTION = {{Inria Lille - Nord Europe}},
  YEAR = {2018},
  MONTH = Dec,
  KEYWORDS = {software ; component ; logiciel ; architecture ; composant ; Java ; Fractal},
  PDF = {https://hal.inria.fr/hal-01964792/file/RR-9242.pdf},
  HAL_ID = {hal-01964792},
  HAL_VERSION = {v1},
}
```
