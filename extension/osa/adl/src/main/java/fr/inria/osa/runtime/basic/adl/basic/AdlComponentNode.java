/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl.basic;

import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Fractal;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.xml.sax.InputSource;

import fr.inria.osa.runtime.basic.AdlLoader;
import fr.inria.osa.runtime.basic.Pair;
import fr.inria.osa.runtime.basic.adl.AdlDumpableNodeInterface;
import fr.inria.osa.runtime.basic.adl.AdlUtil;
import fr.inria.osa.runtime.basic.adl.basic.AdlInterfaceNode.InterfaceCardinality;
import fr.inria.osa.runtime.basic.adl.basic.AdlInterfaceNode.InterfaceContingency;
import fr.inria.osa.runtime.basic.adl.basic.AdlInterfaceNode.InterfaceRole;
import fr.inria.osa.runtime.basic.adl.cached.AdlComponentNodeWithCache;
import fr.inria.osa.runtime.basic.adl.cached.AdlComponentNodeWithParameters;
import fr.inria.osa.simapis.basic.exceptions.OsaAPIException;

/**
 * Represent all type of Fractal component.
 */
public class AdlComponentNode implements AdlDumpableNodeInterface {

  /**
   * Enumeration of component type.
   */
  public enum AdlComponentType {
	/** The component type is undefined. */
	UNDEFINED,
	/** The component type is primitive. */
	PRIMITIVE,
	/** The component type is composite. */
	COMPOSITE
  }

  /** The type of this component. */
  private AdlComponentType                                         type_;

  /** The name of the component. */
  private String                                                   name_;

  /**
   * The list of component extended by this one as (ParentName,
   * parameterList).
   */
  private ArrayList<Pair<String, ArrayList<Pair<String, String>>>> parents_;

  /** Reference to the real instance of this shared component. */
  private String                                                   sharedDefinition_;

  /**
   * The root component used to resolve <code>sharedDefinition_</code>
   * .
   */
  private AdlComponentNode                                         sharedComponentRoot_;

  /** The list of interfaces of this component. */
  // private AdlInterfaceList interfaces_;
  private Map<String, AdlInterfaceNode>                            interfacesMap_;

  /** The sub component list of this composite. */
  // private ArrayList<AdlComponentNode> subComponents_;
  private Map<String, AdlComponentNode>                            subComponents_;

  /** The attribute of this component. */
  private AdlAttributesList                                        attributes_;

  /** Exoevents associated with this component. */
  private AdlExoEventsList                                         exoEvents_;

  /** The binding list of this composite. */
  // private AdlBindingList bindings_;
  private Map<String, AdlBindingNode>                              bindingMap_;

  /** The content of this component when it's a primitive. */
  private String                                                   content_;

  /** The name of the controller description. */
  private String                                                   controller_;

  /** Say if this component and his child have been merged. */
  private boolean                                                  isMerged_;

  /** Say if this component is a shared one. */
  private boolean                                                  isShared_;

  /** The fractal component. */
  private Component                                                component_;

  /** Set of binded interface for this component as string. */
  private HashSet<String>                                          bindedInterface_;

  /** The set of shared component pointing to this component. */
  private HashSet<AdlComponentNode>                                sharedComponent_;

  /** Say if this component is a root. */
  private boolean                                                  isRoot_;

  // FIXME
  private AdlComponentNodeWithParameters                           generator_;


  /**
   * {@inheritDoc}
   */
  @Override
  public final String toString() {
	return name_;
  }


  /**
   * Initialize this component.
   *
   * @param name
   *        The name of the newly created component.
   * @param isRoot
   *        Say if this component is a root one.
   */
  public AdlComponentNode(final String name, final boolean isRoot) {
	this(name, null, isRoot);
  }


  /**
   * Initialize this new ADL Component node.
   *
   * @param name
   *        The name of this new component.
   * @param generator
   *        The generator used to make it (Only used in Cached
   *        version)
   * @param isRoot
   *        Say if this component is a root (root in a fractal file).
   */
  public AdlComponentNode(final String name,
						  final AdlComponentNodeWithParameters generator,
						  final boolean isRoot) {
	type_ = AdlComponentType.UNDEFINED;
	name_ = name;
	isRoot_ = isRoot;
	generator_ = generator;
	parents_ = new ArrayList<>();
	// interfaces_ = new AdlInterfaceList();
	interfacesMap_ = new HashMap<>();
	// subComponents_ = new ArrayList<AdlComponentNode>();
	subComponents_ = new HashMap<>();
	// bindings_ = new AdlBindingList();
	bindingMap_ = new HashMap<>();
	attributes_ = new AdlAttributesList();
	bindedInterface_ = new HashSet<>();
	exoEvents_ = new AdlExoEventsList();
	sharedComponent_ = new HashSet<>();
	isMerged_ = false;
	isShared_ = false;
  }


  /**
   * FIXME
   *
   * @return
   */
  public AdlComponentNodeWithParameters getGenerator() {
	return generator_;
  }


  /**
   * Say if this component is a root one.
   *
   * @return <code>true</code> if this component is root and
   *         <code>false</code> otherwise.
   */
  public final boolean isRoot() {
	return isRoot_;
  }


  /**
   * Give the type of this component.
   *
   * @return The type of this component.
   */
  public final AdlComponentType getType() {
	return type_;
  }


  /**
   * Give the name of this component.
   *
   * @return The name of this component.
   */
  public final String getName() {
	return name_;
  }


  /**
   * Add a given component name to the list of parent of this.
   *
   * @param parent
   *        The component name added to the list of parent.
   * @param parameter
   *        The valued parameter list associated with this parent.
   */
  public final void addParent(final String parent,
	final ArrayList<Pair<String, String>> parameter) {
	parents_.add(new Pair<String, ArrayList<Pair<String, String>>>(parent,
																   parameter));
  }


  /**
   * Give the list of parents of this component.
   *
   * @return The list of parent of this component.
   */
  public final ArrayList<Pair<String, ArrayList<Pair<String, String>>>> getParents() {
	return parents_;
  }


  /**
   * Set the shared definition of this component.
   *
   * @param root
   *        The root component used to resolve the shared definition.
   * @param sharedDefinition
   *        The shared definition of this component.
   */
  public final void setSharedDefinition(final AdlComponentNode root,
	final String sharedDefinition) {
	isShared_ = true;
	sharedComponentRoot_ = root;
	sharedDefinition_ = sharedDefinition;
  }


  /**
   * Give the shared definition of this component.
   *
   * @return The shared definition of this component.
   */
  public final String getSharedDefinition() {
	return sharedDefinition_;
  }


  /**
   * Add a new interface to this component.
   *
   * @param adlSharedInterface
   *        The interface to add.
   */
  public final void addSharedInterface(final AdlInterfaceNode adlSharedInterface) {
	// interfaces_.addShared(adlSharedInterface);
	final AdlInterfaceNode oldInterface =
	  interfacesMap_.get(adlSharedInterface.getName());
	if (oldInterface != null) {
	  interfacesMap_.put(adlSharedInterface.getName(), adlSharedInterface);
	  adlSharedInterface.merge(oldInterface);
	  return;
	}
	interfacesMap_.put(adlSharedInterface.getName(), adlSharedInterface);
  }


  /**
   * Add a new interface to this component.
   *
   * @param adlInterface
   *        The interface to add.
   */
  public final void addInterface(final AdlInterfaceNode adlInterface) {
	// interfaces_.add(adlInterface);
	final AdlInterfaceNode oldInterface =
	  interfacesMap_.get(adlInterface.getName());
	if (oldInterface != null) {
	  oldInterface.merge(adlInterface);
	  return;
	}
	interfacesMap_.put(adlInterface.getName(), adlInterface);
  }


  /**
   * Give the collection of interfaces of this component.
   *
   * @return The collection of interface of this component.
   */
  public final Map<String, AdlInterfaceNode> getInterfaces() {
	return interfacesMap_;
  }


  /**
   * Add a given component to the list of sub component of this one.
   *
   * @param subComponent
   *        The component to add to the list of sub component of this
   *        one.
   */
  public final void addSubcomponent(final AdlComponentNode subComponent) {
	// subComponents_.add(subComponent);
	subComponents_.put(subComponent.getName(), subComponent);
	setType(AdlComponentType.COMPOSITE);
  }


  /**
   * Give the list of sub component of this component.
   *
   * @return The list of sub component of this component.
   */
  public final Map<String, AdlComponentNode> getSubComponents() {
	// return subComponents_;
	return subComponents_;
  }


  /**
   * Give the sub component of this component named <code>name</code>
   * or <code>null</code> if none.
   *
   * @param name
   *        The searched component name.
   * @return The sub component named <code>name</code> if this
   *         component exist and <code>null</code> otherwise.
   */
  public final AdlComponentNode getSubComponentNamed(final String name) {
	return subComponents_.get(name);
  }


  /**
   * Give the root of the path pointing to the real component
   * referenced by this shared one.
   *
   * @return The root of the path pointing to the real component
   *         referenced by this shared one.
   */
  public final AdlComponentNode getSharedComponentRoot() {
	if (!isShared()) {
	  throw new IllegalArgumentException("This component is not shared");
	}
	return sharedComponentRoot_;
  }


  /**
   * Give the component referenced by this shared component.
   *
   * @return The component referenced by this shared component.
   */
  private AdlComponentNode getSharedReference() {
	if (!isShared()) {
	  throw new IllegalArgumentException("This component is not shared");
	}

	String[] path = getSharedDefinition().split("/");
	AdlComponentNode currentComponent;

	// Initialize the root component
	currentComponent = sharedComponentRoot_;

	// Find the referenced component
	for (int i = 0; i < path.length; i++) {
	  if (path[i].equals(".")) {
		continue;
	  }
	  currentComponent = currentComponent.getSubComponentNamed(path[i]);
	  if (currentComponent == null) {
		throw new IllegalArgumentException(
										   "Can't find component referenced by shared component");
	  }
	}
	return currentComponent;
  }


  /**
   * Give the real component referenced by a shared component. If the
   * referenced component is also a shared component we iterate.
   *
   * @return The real component referenced by this shared one.
   */
  public final AdlComponentNode getRealComponent() {
	AdlComponentNode result = getSharedReference();
	while (result.isShared()) {
	  result = result.getSharedReference();
	}
	return result;
  }


  /**
   * Add a given attribute to this component.
   *
   * @param attribute
   *        The attribute.
   */
  public final void addAttribute(final AdlAttributesNode attribute) {
	setType(AdlComponentType.PRIMITIVE);
	attributes_.add(attribute);
  }


  /**
   * Give the attributes of this component.
   *
   * @return The attributes of this component.
   */
  public final AdlAttributesList getAttributes() {
	return attributes_;
  }


  /**
   * Add an exoevent to this component.
   *
   * @param newEvent
   *        The new event to add.
   */
  public final void addExoEvent(final AdlExoEventNode newEvent) {
	exoEvents_.addEvent(newEvent);
  }


  /**
   * Give exoEvents list associated with this component.
   *
   * @return The exoEvents list associated with this component.
   */
  public final AdlExoEventsList getExoEvents() {
	return exoEvents_;
  }


  /**
   * Add a binding to this component.
   *
   * @param binding
   *        The binding to add.
   */
  public final void addBinding(final AdlBindingNode binding) {
	// bindings_.add(binding);
	bindingMap_.put(binding.getClient(), binding);
  }


  /**
   * Give the list of bindings of this component.
   *
   * @return The list of binding of this component.
   */
  public final Map<String, AdlBindingNode> getBindings() {
	// return bindings_;
	return bindingMap_;
  }


  /**
   * Set the content of this component.
   *
   * @param content
   *        The content of this component.
   */
  public final void setContent(final String content) {
	content_ = content;
	setType(AdlComponentType.PRIMITIVE);
  }


  /**
   * Give the content of this component.
   *
   * @return The content of this component or <code>null</code> if not
   *         defined.
   */
  public final String getContent() {
	return content_;
  }


  /**
   * Set controller description.
   *
   * @param controller
   *        The name of the controller description.
   */
  public final void setController(final String controller) {
	controller_ = controller;
  }


  /**
   * Give the controller description.
   *
   * @return The controller description or <code>null</code> if not
   *         define.
   */
  public final String getController() {
	return controller_;
  }


  /**
   * Add the given name to the set of already binded interface.
   *
   * @param interfaceName
   *        The interface name.
   */
  public final void setInterfaceAsBinded(final String interfaceName) {
	bindedInterface_.add(interfaceName);
  }


  /**
   * Say if an interface is already binded.
   *
   * @param interfaceName
   *        The interface name.
   * @return <code>true</code> if interface is already binded and
   *         <code>false</code> otherwise.
   */
  public final boolean isBindedInterface(final String interfaceName) {
	return bindedInterface_.contains(interfaceName);
  }


  /**
   * Set this component as merged, this mean that all inheritance as
   * been resolved.
   */
  public final void setMerged() {
	isMerged_ = true;
  }


  /**
   * Say if this component as been merged.
   *
   * @return <code>true</code> if this component have been merged and
   *         <code>false</code> otherwise.
   */
  public final boolean isMerged() {
	return isMerged_;
  }


  /**
   * Say if this component as been marked as shared.
   *
   * @return <code>true</code> if this component have been marked as
   *         shared and <code>false</code> otherwise.
   */
  public final boolean isShared() {
	return isShared_;
  }


  /**
   * Resolve inheritance.
   */
  public void merge() {

	// Merging my sub components
	for (AdlComponentNode currentSubComponent : getSubComponents().values()) {
	  currentSubComponent.merge();
	}

	// Merging with my parents
	for (Pair<String, ArrayList<Pair<String, String>>> currentParent : getParents()) {
	  String currentParentFilename = currentParent.getKey().replace('.', '/');
	  InputSource source =
		new InputSource(this.getClass().getResourceAsStream(
		  "/" + currentParentFilename + ".fractal"));
	  AdlLoader loader =
		new AdlLoader(currentParent.getKey(), source, currentParent.getValue(),
					  new BasicAdlNodeFactory());
	  AdlComponentNode currentParentNode;
	  try {
		loader.parse();
		currentParentNode = loader.getRoot();
		currentParentNode.merge();
		AdlUtil.mergeSharedComponent(loader.getSharedComponentNode());
		AdlUtil.extendNode(currentParentNode, this, this);
	  } catch (Exception e) {
		if (!(e instanceof IllegalArgumentException)) {
		  throw new IllegalArgumentException("Merge error in "
											 + currentParentFilename + ": "
											 + e.getMessage());
		}
		throw new IllegalArgumentException(e.getMessage());
	  }
	}
	parents_.clear();
	setMerged();
  }


  /**
   * {@inheritDoc}
   */
  public final void dump(final PrintStream stream, final boolean isRoot) {
	if (isRoot) {
	  stream.printf("<definition name=\"%s\" ", getName());
	} else {
	  stream.printf("<component name=\"%s\" ", getName());
	}

	if (isShared()) {
	  stream.printf("definition=\"%s\"/>\n", getSharedDefinition());
	  return;
	}

	if (getParents().size() != 0) {
	  stream.print("extends=\"");
	  stream.printf("%s", getParents().get(0));
	  for (int i = 1; i < getParents().size(); i++) {
		stream.printf(",%s", getParents().get(i));
	  }
	}

	stream.print(">\n");
	// Dump interface
	for (AdlInterfaceNode current : getInterfaces().values()) {
	  current.dump(stream, false);
	}

	// Dump content
	if (content_ != null) {
	  stream.printf("<content class=\"%s\"/>\n", content_);
	}

	// Dump attributes
	if (getAttributes().size() != 0) {
	  getAttributes().dump(stream, false);
	}

	// Dump sub component
	for (AdlComponentNode current : getSubComponents().values()) {
	  current.dump(stream, false);
	}

	// Dump bindings
	for (AdlBindingNode currentBinding : getBindings().values()) {
	  currentBinding.dump(stream, false);
	}

	// Dump controller description
	if (getController() != null) {
	  stream.printf("<controller desc=\"%s\"/>\n", getController());
	}

	// Dump exoEvents
	if (getExoEvents().size() != 0) {
	  getExoEvents().dump(stream, false);
	}

	// End of component
	if (isRoot) {
	  stream.printf("</definition>\n");
	} else {
	  stream.printf("</component>\n");
	}

  }


  /**
   * Add the given shared component to the list of shared component
   * referencing this component.
   *
   * @param sharedComponent
   *        The shared component referencing this component.
   */
  public final void addToSharedComponentSet(
	final AdlComponentNode sharedComponent) {
	if (!sharedComponent.isShared()) {
	  throw new IllegalArgumentException("The component " + sharedComponent
										 + " is not shared");
	}
	sharedComponent_.add(sharedComponent);
  }


  /**
   * Give the set of shared component referencing this component.
   *
   * @return The set of shared component referencing this component.
   */
  public final HashSet<AdlComponentNode> getSharedComponentSet() {
	return sharedComponent_;
  }


  /**
   * Set the type of this component.
   *
   * @param type
   *        The type of this component.
   */
  private void setType(final AdlComponentType type) {
	switch (type_) {
	  case UNDEFINED:
		type_ = type;
		break;
	  default:
		if (type_ != type) {
		  throw new IllegalArgumentException("Try to change component " + name_
											 + " from " + type_ + " to " + type);
		}
		break;
	}
  }

  /**
   * Create or give a previously created Fractal component
   * corresponding to this ADL component.
   *
   * @return The Fractal component corresponding to this component.
   * @throws InstantiationException
   * @throws NoSuchInterfaceException
   * @throws IllegalContentException
   * @throws IllegalLifeCycleException
   * @throws IllegalBindingException
   *         If error occurs (in Fractal library) during creation
   *         process.
   * @throws NoSuchMethodException
   * @throws SecurityException
   * @throws InvocationTargetException
   * @throws IllegalAccessException
   * @throws IllegalArgumentException
   *         If error occurs during reflection process (used during
   *         attributes setting).
   */

  public static int        cnt1 = 0;
  public static int        cnt2 = 0;
  public static int        cnt4 = 0;

  private static Component boot = null;


  public final Component getOrCreateComponent() throws InstantiationException,
	NoSuchInterfaceException, IllegalContentException,
	IllegalLifeCycleException, IllegalBindingException,
	NoSuchMethodException, IllegalAccessException,
	InvocationTargetException, OsaAPIException {

	if (component_ == null) {
	  createComponent();
	}
	return component_;
  }


  private void createComponent() throws
	InstantiationException, NoSuchInterfaceException,
	IllegalContentException, IllegalLifeCycleException,
	IllegalBindingException, NoSuchMethodException, IllegalAccessException,
	InvocationTargetException, OsaAPIException {
	// Merge component if not yet done.
	if (!isMerged()) {
	  merge();
	}
	// If shared component return the component corresponding to
	// real one.
	if (isShared_) {
	  component_ = getRealComponent().getOrCreateComponent();
	  return;
	}
	long t1 = System.currentTimeMillis();
	if (boot == null) {
	  boot = Fractal.getBootstrapComponent();
	}
	TypeFactory typeFactory = (TypeFactory) boot.getFcInterface("type-factory");
	// Create interfaces
	InterfaceType[] interfaces = new InterfaceType[getInterfaces().size()];
	int currentIndex = 0;
	for (AdlInterfaceNode currentInterface : getInterfaces().values()) {
	  interfaces[currentIndex++] =
		typeFactory.createFcItfType(currentInterface.getName(),
		  currentInterface.getSignature(),
		  currentInterface.getRole() == InterfaceRole.client,
		  currentInterface.getContingency() == InterfaceContingency.optional,
		  currentInterface.getCardinality() == InterfaceCardinality.collection);
	}
	ComponentType componentType = typeFactory.createFcType(interfaces);
	GenericFactory componentFactory =
	  (GenericFactory) boot.getFcInterface("generic-factory");

	cnt1 += System.currentTimeMillis() - t1;
	String controller;
	switch (type_) {
	  case PRIMITIVE:
		long t2 = System.currentTimeMillis();
		// Create the component
		controller = getController();
		if (controller == null) {
		  controller = "primitive";
		}
		component_ =
		  componentFactory.newFcInstance(componentType, controller,
			getContent());

		// Manage attributes
		for (AdlAttributesNode currentAttribute : attributes_) {
		  currentAttribute.instantiate(this, component_);
		}

		// Manage exoEvents
		for (AdlExoEventNode currentEvent : exoEvents_) {
		  currentEvent.instantiate(this, component_);
		}
		cnt2 += System.currentTimeMillis() - t2;
		break;
	  case COMPOSITE:
		// Create the component
		controller = getController();
		if (controller == null) {
		  controller = "composite";
		}
		component_ =
		  componentFactory.newFcInstance(componentType, controller, null);

		// Add sub components
		ContentController contentController =
		  (ContentController) component_.getFcInterface("content-controller");
		for (AdlComponentNode currentSubNode : getSubComponents().values()) {
		  contentController
						   .addFcSubComponent(currentSubNode
															.getOrCreateComponent());
		}

		// Add bindings
		for (AdlBindingNode currentBinding : getBindings().values()) {
		  currentBinding.instantiate(this, component_);
		}
		break;
	  case UNDEFINED:
		long t4 = System.currentTimeMillis();
		System.err.println("Component " + name_ + " is undefined");
		controller = getController();
		if (controller == null) {
		  controller = "composite";
		}
		component_ =
		  componentFactory.newFcInstance(componentType, controller, null);
		cnt4 += System.currentTimeMillis() - t4;
		break;
	}
  }


  /**
   * Make a strict and deep copy of this component. This method must
   * be called after merging.
   *
   * @param oldToNewComponent
   *        A table giving correspondence between <i>old</i> component
   *        and new one.
   * @param sharedComponentSet
   *        FIXME
   * @return The newly created component.
   */
  @SuppressWarnings("unchecked")
protected final AdlComponentNode clone(
	final HashMap<AdlComponentNode, AdlComponentNode> oldToNewComponent,
	final HashSet<HashSet<AdlComponentNode>> sharedComponentSet) {
	if (!isMerged()) {
	  throw new IllegalArgumentException(
										 "The component must be merged before cloning");
	}
	AdlComponentNode result;
	if (generator_ != null) {
	  result = new AdlComponentNodeWithCache(name_, generator_, true);
	} else {
	  result =
		new AdlComponentNodeWithCache(name_,
									  (AdlComponentNodeWithParameters) this,
									  true);
	}
	oldToNewComponent.put(this, result);

	// Copy Attributes list
	for (AdlAttributesNode currentAttribute : attributes_) {
	  result.attributes_.add(currentAttribute.clone());
	}

	// Copy Bindings list
	for (AdlBindingNode currentBinding : getBindings().values()) {
	  result.getBindings().put(currentBinding.getClient(),
		currentBinding.clone());
	}

	// Copy Content
	result.content_ = content_;

	// Copy Controller
	result.controller_ = controller_;

	// Copy Events list
	for (AdlExoEventNode currentEvent : exoEvents_) {
	  result.exoEvents_.addEvent(currentEvent.clone());
	}

	// Copy Interface list
	for (AdlInterfaceNode currentInterface : getInterfaces().values()) {
	  result.getInterfaces().put(currentInterface.getName(),
		currentInterface.clone());
	}

	// Copy merged flag
	result.isMerged_ = isMerged_;

	// Copy Shared flag
	result.isShared_ = isShared_;

	// Copy Parent list (Perhaps not necessary)
	// result.parents_ = (ArrayList<Pair<String,
	// ArrayList<Pair<String, String>>>>) parents_.clone();

	if (isShared_) {
	  // Copy shared component root
	  AdlComponentNode newSharedComponentRoot =
		oldToNewComponent.get(sharedComponentRoot_);
	  assert (newSharedComponentRoot != null);
	  result.sharedComponentRoot_ = newSharedComponentRoot;
	  result.sharedDefinition_ = sharedDefinition_;
	}


	result.sharedComponent_ =
	  (HashSet<AdlComponentNode>) sharedComponent_.clone();


	sharedComponentSet.add(result.sharedComponent_);

	// Copy subcomponent
	for (AdlComponentNode currentSubComponent : getSubComponents().values()) {
	  AdlComponentNode subComponentCopy =
		currentSubComponent.clone(oldToNewComponent, sharedComponentSet);
	  result.getSubComponents().put(subComponentCopy.getName(),
		subComponentCopy);
	}
	result.type_ = type_;

	return result;
  }

}
