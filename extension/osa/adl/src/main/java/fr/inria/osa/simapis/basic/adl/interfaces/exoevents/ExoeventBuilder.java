/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.simapis.basic.adl.interfaces.exoevents;

import fr.inria.osa.simapis.basic.simulation.ExoEventsTypes;

/**
 * FIXME : Missing JavaDoc.
 */
public interface ExoeventBuilder {
  /**
   * FIXME : Missing JavaDoc.
   *
   * @param component
   *        FIXME : Missing JavaDoc.
   * @param itf
   *        FIXME : Missing JavaDoc.
   * @param name
   *        FIXME : Missing JavaDoc.
   * @param type_
   *        FIXME : Missing JavaDoc.
   * @param time
   *        FIXME : Missing JavaDoc.
   * @param method
   *        FIXME : Missing JavaDoc.
   * @param param
   *        FIXME : Missing JavaDoc.
   * @param context
   *        FIXME : Missing JavaDoc.
   * @throws Exception
   *         FIXME : Missing JavaDoc.
   */
  void setExoevent(Object component, String itf, String name, ExoEventsTypes type_,
	String time, String method, String param, Object context) throws Exception;
}
