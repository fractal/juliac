/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl.basic;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import fr.inria.osa.runtime.basic.adl.AdlDumpableNodeInterface;

/**
 * List of exoevent present in primitive ADL component.
 *
 */
public class AdlExoEventsList implements AdlDumpableNodeInterface ,
  Iterable<AdlExoEventNode> {

  /** The signature associated with this exoevent. */
  private String                     signature_;

  /** The list of attributes. */
//  private ArrayList<AdlExoEventNode> exoEvents_;
  private Map<String,AdlExoEventNode> exoEventsMap_;


  /**
   * Initialize this attributes.
   */
  public AdlExoEventsList() {
//    exoEvents_ = new ArrayList<AdlExoEventNode>();
	exoEventsMap_ = new HashMap<>();
  }


  /**
   * Set the signature of this attributes.
   *
   * @param signature
   *        The signature of this attributes.
   */
  public void setSignature(String signature) {
	signature_ = signature;
  }


  /**
   * Add an event.
   *
   * @param newEvent
   *        The new event to add.
   */
  public void addEvent(AdlExoEventNode newEvent) {
//    exoEvents_.add(newEvent);
	exoEventsMap_.put(newEvent.getName(), newEvent);
  }


  /**
   * Return the exoevent named <code>eventName</code> or
   * <code>null</code> if none exist in the list.
   *
   * @param eventName
   *        The name of searched event.
   * @return The exoevent named <code>eventName</code> or
   *         <code>null</code> if none exist in the list.
   */
  private AdlExoEventNode getExoEventByName(String eventName) {
//    for (AdlExoEventNode currentEvent : exoEvents_) {
//      if (currentEvent.getName().equals(eventName)) {
//        return currentEvent;
//      }
//    }
//    return null;
	return exoEventsMap_.get(eventName);
  }


  /**
   * Merge a given exoevents list with this one.
   *
   * @param toMerge
   *        The list to merge (can be <code>null</code>);
   * @param override
   *        If <code>true</code> then exoevent with same name are
   *        overridden.
   */
  public void merge(AdlExoEventsList toMerge, boolean override) {
	if (toMerge != null) {
	  for (AdlExoEventNode currentExoEvent : toMerge.exoEventsMap_.values()) {
		AdlExoEventNode oldExoEvent =
		  getExoEventByName(currentExoEvent.getName());
		if (oldExoEvent != null) {
		  if (override) {
			exoEventsMap_.put(currentExoEvent.getName(),currentExoEvent);
		  }
		} else {
		  exoEventsMap_.put(currentExoEvent.getName(),currentExoEvent);
		}
	  }
	}
  }


  /**
   * Give the number of events in this event list.
   *
   * @return The number of events in this event list.
   */
  public int size() {
//    return exoEvents_.size();
	return exoEventsMap_.size();
  }


  /**
   * {@inheritDoc}
   */
  public void dump(PrintStream stream, boolean isRoot) {
	stream.print("<exoevents ");
	if (signature_ != null) {
	  stream.printf("signature=\"%s\"", signature_);
	}
	stream.print(">\n");
	for (AdlExoEventNode current : exoEventsMap_.values()) {
	  current.dump(stream);
	}
	stream.print("</exoevents>\n");

  }


  public Iterator<AdlExoEventNode> iterator() {
//    return exoEvents_.iterator();
	return exoEventsMap_.values().iterator();
  }

}
