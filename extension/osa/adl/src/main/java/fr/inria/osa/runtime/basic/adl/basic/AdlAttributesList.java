/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl.basic;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import fr.inria.osa.runtime.basic.adl.AdlDumpableNodeInterface;

/**
 * This class manage the attribute list that can be define in each
 * primitive component. This list is define a signature representing
 * the Java interface defining method to set attributes and a list of
 * pair associating a name with a value.
 */
public class AdlAttributesList implements AdlDumpableNodeInterface,
  Iterable<AdlAttributesNode> {

  /** The signature of this attributes. */
  private String                       signature_;

  /** The list of attributes. */
//  private ArrayList<AdlAttributesNode> attributes_;
  private Map<String, AdlAttributesNode> attributesMap_;


  /**
   * Initialize this attributes.
   */
  public AdlAttributesList() {
//    attributes_ = new ArrayList<AdlAttributesNode>();
	attributesMap_ = new HashMap<>();
  }


  /**
   * Set the signature of this attributes.
   *
   * @param signature
   *        The signature of this attributes.
   */
  public final void setSignature(final String signature) {
	signature_ = signature;
  }


  /**
   * Return the attribute named <code>attributeName</code> or
   * <code>null</code> if none exist in the list.
   *
   * @param attributeName
   *        The name of searched attribute.
   * @return The attribute named <code>attributeName</code> or
   *         <code>null</code> if none exist in the list.
   */
  private AdlAttributesNode getAttributesByName(final String attributeName) {
//    for (AdlAttributesNode currentAttribute : attributes_.v) {
//      if (currentAttribute.getName().equals(attributeName)) {
//        return currentAttribute;
//      }
//    }
//    return null;
	return attributesMap_.get(attributeName);
  }


  /**
   * Merge a given attributes list with this one.
   *
   * @param toMerge
   *        The list to merge (can be <code>null</code>);
   * @param override
   *        Say an already existing attributes must be overridden by
   *        the new definition.
   */
  public final void merge(final AdlAttributesList toMerge,
	final boolean override) {
	if (toMerge != null) {
	  for (AdlAttributesNode currentAttribute : toMerge.attributesMap_.values()) {
		AdlAttributesNode oldAttribute =
		  getAttributesByName(currentAttribute.getName());
		if (oldAttribute != null) {
		  if (override) {
			attributesMap_.put(currentAttribute.getName(),currentAttribute);
		  }
		} else {
		  attributesMap_.put(currentAttribute.getName(),currentAttribute);
		}
	  }
	}
  }


  /**
   * Add a given attribute to this attribute list.
   *
   * @param attribute
   *        The new attribute.
   */
  public final void add(final AdlAttributesNode attribute) {
//    attributes_.add(attribute);
	attributesMap_.put(attribute.getName(), attribute);
  }


  /**
   * Give the number of defined attributes.
   *
   * @return The number of defined attributes.
   */
  public final int size() {
//    return attributes_.size();
	return attributesMap_.size();
  }


  /**
   * {@inheritDoc}
   */
  public final Iterator<AdlAttributesNode> iterator() {
//    return attributes_.iterator();
	return attributesMap_.values().iterator();
  }


  /**
   * {@inheritDoc}
   */
  public final void dump(final PrintStream stream,
	final boolean isRoot) {
	stream.print("<attributes ");
	if (signature_ != null) {
	  stream.printf("signature=\"%s\"", signature_);
	}
	stream.print(">\n");
	for (AdlAttributesNode current : attributesMap_.values()) {
	  current.dump(stream, false);
	}
	stream.print("</attributes>\n");
  }
}
