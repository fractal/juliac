/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl.cached;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import fr.inria.osa.runtime.basic.AbstractAdlNodeFactory;
import fr.inria.osa.runtime.basic.Pair;
import fr.inria.osa.runtime.basic.adl.ParameterizedStringSolver;
import fr.inria.osa.runtime.basic.adl.basic.AdlAttributesNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlBindingNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlComponentNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlExoEventNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlInterfaceNode;

public class AdlComponentNodeWithParameters extends AdlComponentNode {

  /**
   * Associate each root component with the list of shared component
   * defined in the corresponding file.
   */
  static HashMap<AdlComponentNode, ArrayList<AdlComponentNode>> rootToSharedComponent_;

  /**
   * Associate filename (without fractal post-fix) with the
   * corresponding cached node.
   */
  static HashMap<String, AdlComponentNodeWithParameters>        filenameToCachedNode_;

  /** The list of arguments define in fractal file. */
  private ArrayList<Pair<String, String>>                       argumentsList_;

  /**
   * The parameterized version of attributes list (signature can be
   * parameterized).
   */
  private AdlAttributesListWithParameters                       initialAttributesList_;

  /**
   * The parameterized version of exo events list (signature can be
   * parameterized).
   */
  private AdlExoEventsListWithParameters                        initialExoEventsList_;

  /** Say if the name contains parameters. */
  private boolean                                               nameIsParameterized_;

  /** The parameterized version of name. */
  private String                                                parameterizedName_;

  /** Say if the controller contains parameters. */
  private boolean                                               controllerIsParameterized_;

  /** The controller of this component without parameter solving. */
  private String                                                parameterizedController_;

  /** Say if the content contains parameters. */
  private boolean                                               contentIsParameterized_;

  /** The parameterized version of content. */
  private String                                                parameterizedContent_;

  /** Say if the shared definition contains parameters. */
  private boolean                                               sharedDefinitionIsParameterized_;

  /**
   * The shared definition of this component without parameter
   * solving.
   */
  private String                                                parameterizedSharedDefinition_;

  /**
   * The list of subcomponent define in the fractal file before make
   * merge.
   */
  private ArrayList<AdlComponentNode>                           initialSubComponentList_;

  /**
   * The list of interfaces define in the fractal file before make
   * merge.
   */
  private ArrayList<AdlInterfaceNodeWithParameters>             initialInterfacesList_;

  /**
   * The list of bindings define in the fractal file before make
   * merge.
   */
  private ArrayList<AdlBindingNodeWithParameters>               initialBindingsList_;

  /** Say if parents contains parameters. */
  private boolean                                               parentsIsParameterized_;

  /** The parameterized parent definition. */
  private String                                                parameterizedParents_;

  static {
	filenameToCachedNode_ =
	  new HashMap<>();
	rootToSharedComponent_ =
	  new HashMap<>();
  }


  /**
   * Initialize this component.
   *
   * @param parameterizedName
   *        The parameterized name of this component.
   * @param name
   *        The name of this component.
   * @param isRoot
   *        Say if this component is a root.
   */
  protected AdlComponentNodeWithParameters(final String parameterizedName,
										   final String name,
										   final boolean isRoot) {
	super(name, isRoot);
	parameterizedName_ = parameterizedName;
	if (ParameterizedStringSolver.containParameters(parameterizedName)) {
	  nameIsParameterized_ = true;
	}
	initialSubComponentList_ = new ArrayList<>();
	initialInterfacesList_ = new ArrayList<>();
	initialBindingsList_ = new ArrayList<>();
	initialAttributesList_ = new AdlAttributesListWithParameters();
	initialExoEventsList_ = new AdlExoEventsListWithParameters();
  }


  /**
   * Create a new instance of this parameterized component.
   *
   * @param parametersList
   *        The parameters list.
   * @param root
   *        The root used to manage shared definition path or
   *        <code>null</code> if this component is root.
   * @return The newly created component.
   */
  final AdlComponentNode newInstance(
	final ArrayList<Pair<String, String>> parametersList,
	final AdlComponentNode root) {
	assert ((isRoot() || root != null) && (root != null || isRoot()));
	if (argumentsList_ != null) {
	  AbstractAdlNodeFactory.updateParametersWithDefaultValue(parametersList,
		argumentsList_);
	} else if (isRoot()) {
	  // Copy this component
	  HashMap<AdlComponentNode, AdlComponentNode> oldToNewComponent =
		new HashMap<>();
	  HashSet<HashSet<AdlComponentNode>> sharedComponentSet =
		new HashSet<>();
	  AdlComponentNode copy = clone(oldToNewComponent, sharedComponentSet);

	  // Update sharedComponentSet
	  for (HashSet<AdlComponentNode> currentPointingSet : sharedComponentSet) {
		@SuppressWarnings("unchecked")
		HashSet<AdlComponentNode> currentCopy =
		  (HashSet<AdlComponentNode>) currentPointingSet.clone();
		currentPointingSet.clear();
		for (AdlComponentNode pointingComponent : currentCopy) {
		  AdlComponentNode newPointingComponent =
			oldToNewComponent.get(pointingComponent);
		  assert (newPointingComponent != null);
		  currentPointingSet.add(newPointingComponent);
		}
	  }
	  return copy;
	}

	ParameterizedStringSolver solver =
	  new ParameterizedStringSolver(parametersList);
	AdlComponentNode result;

	// Initialize the new instance.
	assert (parameterizedName_ != null);
	if (nameIsParameterized_) {
	  result =
		new AdlComponentNodeWithCache(solver.getString(parameterizedName_),
									  this, isRoot());
	} else {
	  result =
		new AdlComponentNodeWithCache(parameterizedName_, this, isRoot());
	}

	// Manage parents
	if (parameterizedParents_ != null) {
	  ArrayList<Pair<String, ArrayList<Pair<String, String>>>> parentsList;
	  if (parentsIsParameterized_) {
		parentsList =
		  AbstractAdlNodeFactory
								.parseExtendsString(solver
														  .getString(parameterizedParents_));
	  } else {
		parentsList =
		  AbstractAdlNodeFactory.parseExtendsString(parameterizedParents_);

	  }
	  for (Pair<String, ArrayList<Pair<String, String>>> currentParent : parentsList) {
		result.addParent(currentParent.getKey(), currentParent.getValue());
	  }
	}

	// Manage shared component
	if (parameterizedSharedDefinition_ != null) {
	  // Set shared definition
	  assert (root != null);
	  assert (parameterizedParents_ == null);
	  if (sharedDefinitionIsParameterized_) {
		result.setSharedDefinition(root,
		  solver.getString(parameterizedSharedDefinition_));
	  } else {
		result.setSharedDefinition(root, parameterizedSharedDefinition_);
	  }
	  ArrayList<AdlComponentNode> currentSharedList =
		rootToSharedComponent_.get(root);
	  if (currentSharedList == null) {
		currentSharedList = new ArrayList<>();
		rootToSharedComponent_.put(root, currentSharedList);
	  }
	  currentSharedList.add(result);
	}

	// Manage interfaces
	for (AdlInterfaceNode currentInterface : initialInterfacesList_) {
	  result
			.addInterface(((AdlInterfaceNodeWithParameters) currentInterface)
																			 .newInstance(solver));
	}

	// Manage Controller
	if (parameterizedController_ != null) {
	  if (controllerIsParameterized_) {
		result.setController(solver.getString(parameterizedController_));
	  } else {
		result.setController(parameterizedController_);
	  }
	}

	switch (getType()) {
	  case PRIMITIVE:

		// Manage content

		if (parameterizedContent_ != null) {
		  if (contentIsParameterized_) {
			result.setContent(solver.getString(parameterizedContent_));
		  } else {
			result.setContent(parameterizedContent_);
		  }
		}

		// Manage attributes
		if (initialAttributesList_.getParameterizedSignature() != null) {
		  result.getAttributes().setSignature(
			initialAttributesList_.getSignature(solver));
		  for (AdlAttributesNode currentAttribute : initialAttributesList_) {
			if (currentAttribute instanceof AdlAttributesNodeWithParameters) {
			  result
					.addAttribute(((AdlAttributesNodeWithParameters) currentAttribute)
																					  .newInstance(solver));
			} else {
			  result.addAttribute(currentAttribute.clone());
			}
		  }
		}

		// Manage Exoevents
		if (initialExoEventsList_.getParameterizedSignature() != null) {
		  result.getExoEvents().setSignature(
			initialExoEventsList_.getSignature(solver));
		}
		for (AdlExoEventNode currentEvent : initialExoEventsList_) {
		  if (currentEvent instanceof AdlExoEventNodeWithParameters) {
			result
				  .addExoEvent(((AdlExoEventNodeWithParameters) currentEvent)
																			 .newInstance(solver));
		  } else {
			result.addExoEvent(currentEvent.clone());
		  }
		}
		break;
	  case COMPOSITE:
		// Manage subcomponent
		for (AdlComponentNode currentSubComponent : initialSubComponentList_) {
		  AdlComponentNode newRoot = isRoot() ? result : root;
		  if (currentSubComponent instanceof AdlComponentNodeWithParameters) {
			result
				  .addSubcomponent(((AdlComponentNodeWithParameters) currentSubComponent)
																						 .newInstance(
																						   solver
																								 .getValuedParameterList(),
																						   newRoot));
		  } else {
			assert (currentSubComponent instanceof AdlComponentNodeWithCache);
			result
				  .addSubcomponent(currentSubComponent
													  .getGenerator()
													  .newInstance(
														solver
															  .getValuedParameterList(),
														newRoot));

		  }
		}

		// Manage bindings
		for (AdlBindingNode currentBinding : initialBindingsList_) {
		  if (currentBinding instanceof AdlBindingNodeWithParameters) {
			result
				  .addBinding(((AdlBindingNodeWithParameters) currentBinding)
																			 .newInstance(solver));
		  } else {
			result.addBinding(currentBinding.clone());
		  }
		}
		break;
	  case UNDEFINED:

		break;
	}
	return result;
  }


  /**
   * Add parents to this parameterized component.
   *
   * @param parameterizedParents
   *        The parameterized parent string.
   * @param parentDefinition
   *        The resolved parent string.
   * @param root
   *        The root used to manage shared definition path.
   */
  public final void addParent(final String parameterizedParents,
	final String parentDefinition, final AdlComponentNode root) {
	parameterizedParents_ = parameterizedParents;
	if (ParameterizedStringSolver.containParameters(parameterizedParents)) {
	  parentsIsParameterized_ = true;
	}
	ArrayList<Pair<String, ArrayList<Pair<String, String>>>> parentsList =
	  AbstractAdlNodeFactory.parseExtendsString(parentDefinition);
	for (Pair<String, ArrayList<Pair<String, String>>> currentParent : parentsList) {
	  if (currentParent.getKey().contains("/")) {
		assert (root != null);
		setSharedDefinition(root, parameterizedParents, currentParent.getKey());
		parameterizedParents_ = null;
	  } else {
		super.addParent(currentParent.getKey(), currentParent.getValue());
	  }
	}
  }


  /**
   * Set the content of this parameterized component.
   *
   * @param parameterizedContent
   *        The parameterized content.
   * @param content
   *        The content.
   */
  public final void setContent(final String parameterizedContent, final String content) {
	super.setContent(content);
	parameterizedContent_ = parameterizedContent;
	if (ParameterizedStringSolver.containParameters(content)) {
	  contentIsParameterized_ = true;
	}
  }


  /**
   * Set the controller of this parameterized component.
   *
   * @param parameterizedController
   *        The parameterized controller.
   * @param controller
   *        The controller.
   */
  public final void setController(final String parameterizedController, final String controller) {
	super.setController(controller);
	parameterizedController_ = parameterizedController;
	if (ParameterizedStringSolver.containParameters(controller)) {
	  contentIsParameterized_ = true;
	}
  }


  /**
   * Set the shared definition of this component.
   *
   * @param root
   *        The root of the shared definition path.
   * @param parameterizedSharedDefinition
   *        The parameterized shared definition.
   * @param sharedDefinition
   *        The shared definition.
   */
  public final void setSharedDefinition(final AdlComponentNode root,
	final String parameterizedSharedDefinition, final String sharedDefinition) {
	super.setSharedDefinition(root, sharedDefinition);
	parameterizedSharedDefinition_ = parameterizedSharedDefinition;
	if (ParameterizedStringSolver
								 .containParameters(parameterizedSharedDefinition)) {
	  sharedDefinitionIsParameterized_ = true;
	}
  }


  /**
   * {@inheritDoc}
   */
  @Override
  public final void merge() {
	CachedUtility.merge(this);
  }


  /**
   * Set the argument list of this component.
   *
   * @param argumentList
   *        The argument list of this component.
   */
  public final void setArgumentList(final ArrayList<Pair<String, String>> argumentList) {
	argumentsList_ = argumentList;
  }


  /**
   * Add a component to the list of initial sub component.
   *
   * @param subComponent
   *        The sub component to add.
   */
  final void addSubComponentToInitialList(final AdlComponentNode subComponent) {
	initialSubComponentList_.add(subComponent);
  }


  /**
   * Add a binding to the list of initial bindings.
   *
   * @param binding
   *        The binding to add.
   */
  final void addBindingToInitialList(final AdlBindingNodeWithParameters binding) {
	initialBindingsList_.add(binding);
  }


  /**
   * Add an interface to the list of initial interfaces.
   *
   * @param newInterface
   *        The interface to add.
   */
  final void addInterfaceToInitialList(final AdlInterfaceNodeWithParameters newInterface) {
	initialInterfacesList_.add(newInterface);
  }


  /**
   * Add an attribute to the list of initial attributes.
   *
   * @param attribute
   *        The attribute to add.
   */
  public final void addAttributeToInitialList(final AdlAttributesNode attribute) {
	initialAttributesList_.add(attribute);
  }


  /**
   * Give the list of attributes that was present in the initial
   * definition of this component (before merging with parents).
   *
   * @return The list of attributes that was present in the initial
   *         definition of this component (before merging with
   *         parents).
   */
  public final AdlAttributesListWithParameters getInitialAttributesList() {
	return initialAttributesList_;
  }


  /**
   * Add an event to the list of initial event.
   *
   * @param newEvent
   *        The event to add.
   */
  public final void addExoEventToInitialList(final AdlExoEventNode newEvent) {
	initialExoEventsList_.addEvent(newEvent);
  }


  /**
   * Give the list of exo event that was present in the initial
   * definition of this component (before merging with parents).
   *
   * @return The list of exo event that was present in the initial
   *         definition of this component (before merging with
   *         parents).
   */
  public final AdlExoEventsListWithParameters getInitialExoEventsList() {
	return initialExoEventsList_;
  }

}
