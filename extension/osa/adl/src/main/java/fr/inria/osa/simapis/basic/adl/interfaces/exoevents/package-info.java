/**
 * Interfaces defined for the Fractal ADL extension to support the scheduling of exogeneous events.
 *
 * @author odalle
 */
package fr.inria.osa.simapis.basic.adl.interfaces.exoevents;
