/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl.basic;

import java.io.PrintStream;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;

import fr.inria.osa.runtime.basic.adl.AdlInstantiableNodeInterface;
import fr.inria.osa.simapis.basic.simulation.ExoEventsTypes;
import fr.inria.osa.simapis.basic.simulation.SimulationInternalSchedulingItf;
import fr.inria.osa.simapis.basic.exceptions.OsaAPIException;

/**
 * Exoevent present in ADL component.
 */
public class AdlExoEventNode implements AdlInstantiableNodeInterface, Cloneable {

  // FIXME : You must manage parameter of exoevents

  /** The name of this event. */
  private String name_;

  /** The type of this event. */
  //private String type_;
  private ExoEventsTypes type_;

  /** The time associated with this event. */
  private long   time_;

  /** The method associated with this event. */
  private String method_;


  /**
   * Initialize this new ExoEvent.
   *
   * @param name
   *        The name of this new event.
   * @param type
   *        The type of this new event.
   * @param time
   *        The time associated with this new event.
   * @param method
   *        The method associated with this new event.
   */
  public AdlExoEventNode(String name, String type, long time, String method) {
	name_ = name;
	type_ = ExoEventsTypes.lookup(type);
	time_ = time;
	method_ = method;
  }


  /**
   * Give the name of this exo event.
   *
   * @return The name of this exoevent.
   */
  public String getName() {
	return name_;
  }


  /**
   * Give the method name associated with this event.
   *
   * @return The name of the method associated with this event.
   */
  public String getMethod() {
	return method_;
  }


  /**
   * Give the time associated to this event.
   *
   * @return The time associated with this event.
   */
  public long getTime() {
	return time_;
  }

  /**
   * Gives the type of the event.
   *
   * <p>The current supported types are:
   * <ul>
   * <li> {@literal ZeroTimeEvent}: An event whose processing starts and completes at
   *     the same simulation time.
   * <li> {@literal StartProcess}: An event whose processing is not guaranteed to complete
   *     at the same simulation time. Processing of such an event requires a new process (or thread)
   *     to be started.
   * </ul>
   *
   * @return
   *     A string containing the type name.
   */
  public ExoEventsTypes getType() {
	  return type_;
  }


  /**
   * Print this exoevent to the given stream.
   *
   * @param stream
   *        The stream used to print this event.
   */
  public void dump(PrintStream stream) {
	stream.printf(
	  "<exoevent name=\"%s\" type=\"%s\" time=\"%s\" method=\"%s\" />\n",
	  name_, type_, time_, method_);

  }


  /**
   * Instantiate this exoevent in the given Fractal component.
   *
   * @param adlComponent
   *        The ADL component containing this exoevent.
   * @param fractalComponent
   *        The Fractal component in which this exoevent must be
   *        created.
   * @throws NoSuchInterfaceException
   *         If the Fractal component does have a
   *         "simulation-controller" interface.
   * @throws OsaAPIException
   *         See {@link fr.inria.osa.simapis.basic.SimulationControllerAPI#scheduleMyself(String, Object[], long, boolean)}
   */
  public void instantiate(
	AdlComponentNode adlComponent,
	Component fractalComponent) throws NoSuchInterfaceException, OsaAPIException {
	SimulationInternalSchedulingItf schedController =
		(SimulationInternalSchedulingItf) fractalComponent.getFcInterface("scheduling-controller");
	schedController.scheduleMyself(getMethod(), null, getTime(), getType());

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public AdlExoEventNode clone() {
	return new AdlExoEventNode(name_, type_.name(), time_, method_);
  }

}
