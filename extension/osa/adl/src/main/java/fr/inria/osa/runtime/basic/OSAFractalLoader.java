/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.util.Fractal;
import org.xml.sax.InputSource;

import fr.inria.osa.runtime.basic.adl.ParameterIterator;
import fr.inria.osa.runtime.basic.adl.cached.CachedAdlNodeFactory;

public class OSAFractalLoader {

  /**
   * @param args
   * @throws FileNotFoundException
   */
  public static void main(String[] args) {

	if (args.length != 1) {
	  System.err.println("OSAFractalLoader file.fractal\n");
	  System.exit(1);
	}
	String filename = args[0];
	ArrayList<Pair<String, String>> parametersList =
	  new ArrayList<>();

	int openBracketIndex = args[0].indexOf('(');
	if (openBracketIndex != -1) {
	  String parameters = args[0].replaceFirst("^.*\\(", "");
	  parameters = parameters.replaceFirst("\\)", "");
	  // Add parameter
	  Iterator<Pair<String, String>> argumentIterator =
		new ParameterIterator(parameters, "=>");
	  while (argumentIterator.hasNext()) {
		Pair<String, String> argAndValue = argumentIterator.next();
		if (argAndValue.getValue() == null) {
		  throw new IllegalArgumentException(
											 "The parameter valuation must be parameterName=>parameterValue");
		}
		parametersList.add(argAndValue);
	  }
	  filename = args[0].substring(0, openBracketIndex);
	}
	filename = filename.replace('.', '/');

	InputSource source =
	  new InputSource(Class.class.getClass().getResourceAsStream(
		"/" + filename + ".fractal"));
	AdlLoader loader =
	  new AdlLoader(filename, source, parametersList,
					new CachedAdlNodeFactory());

	try {
	  loader.parse();
//      long before = System.currentTimeMillis();
	  Component c = loader.launch();
//      System.out.println("Instantiation time: "+ (System.currentTimeMillis()-before)/1000);
//      before = System.currentTimeMillis();
	  Fractal.getLifeCycleController(c).startFc();
//      System.out.println("Initialisation time: "+ (System.currentTimeMillis()-before)/1000);
//      before = System.currentTimeMillis();
	  ((Runnable)c.getFcInterface("r")).run();
//      System.out.println("Execution time: "+ (System.currentTimeMillis()-before)/1000);

//      AdlComponentNode root = loader.getRoot();
//      root.merge();
//
//      //PrintStream stream = System.out;
//      PrintStream stream = new PrintStream(args[1]);
//      stream.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
//      stream
//            .println("<!DOCTYPE definition PUBLIC \"-//objectweb.org//DTD Fractal ADL 2.0//EN\" \"classpath://osa/util/adl/stdsim.dtd\">");
//      root.dump(stream, true);
	} catch (Exception e) {
	  e.printStackTrace();
	}
  }

}
