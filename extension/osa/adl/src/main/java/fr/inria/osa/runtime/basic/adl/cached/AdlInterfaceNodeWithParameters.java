/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl.cached;

import fr.inria.osa.runtime.basic.adl.ParameterizedStringSolver;
import fr.inria.osa.runtime.basic.adl.basic.AdlInterfaceNode;

public class AdlInterfaceNodeWithParameters extends AdlInterfaceNode {


  /** The parameterized name of this interface. */
  private String parameterizedName_;

  /** The parameterized signature of this interface. */
  private String parameterizedSignature_;

  /** The parameterized cardinality of this interface. */
  private String parameterizedCardinality_;

  /** The parameterized contingency of this interface. */
  private String parameterizedContingency_;

  /** The parameterized role of this interface. */
  private String parameterizedRole_;


  /**
   * Initialize this parameterized interface.
   *
   * @param parameterizedName
   *        The name without parameter solving.
   * @param name
   *        The name with parameter solving.
   */
  protected AdlInterfaceNodeWithParameters(String parameterizedName, String name) {
	super(name);
	parameterizedName_ = parameterizedName;
  }


  /**
   * Set the cardinality of this interface.
   *
   * @param parameterizedCardinality
   *        The parameterized version of cardinality.
   * @param cardinality
   *        The resolved version of cardinality.
   */
  public void setCardinality(String parameterizedCardinality, String cardinality) {
	if (cardinality.equals("collection")) {
	  setCardinality(InterfaceCardinality.collection);
	} else {
	  setCardinality(InterfaceCardinality.singleton);
	}
	parameterizedCardinality_ = parameterizedCardinality;
  }

  /**
   * Set the contingency of this interface.
   *
   * @param parameterizedContingency
   *        The parameterized version of contingency.
   * @param contindency
   *        The resolved version of contingency.
   */
  public void setContingency(String parameterizedContingency, String contindency) {
	if (contindency.equals("mandatory")) {
	  setContingency(InterfaceContingency.mandatory);
	} else {
	  setContingency(InterfaceContingency.optional);
	}
	parameterizedContingency_ = parameterizedContingency;
  }

  /**
   * Set the role of this interface.
   *
   * @param parameterizedRole
   *        The parameterized version of role.
   * @param role
   *        The resolved version of role.
   */
  public void setRole(String parameterizedRole, String role) {
	if (role.equals("client")) {
	  setRole(InterfaceRole.client);
	} else {
	  setRole(InterfaceRole.server);
	}
	parameterizedRole_ = parameterizedRole;
  }

  /**
   * Set the signature of this interface.
   *
   * @param parameterizedSignature
   *        The parameterized version of signature.
   * @param signature
   *        The resolved version of signature.
   */
  public void setSignature(String parameterizedSignature, String signature) {
	setSignature(signature);
	parameterizedSignature_ = parameterizedSignature;
  }



  /**
   * Create a new {@link AdlInterfaceNode} bases on this parameterized
   * interface.
   *
   * @param solver
   *        The solver used to find parameter value.
   * @return The newly created interface.
   */
  AdlInterfaceNode newInstance(ParameterizedStringSolver solver) {
	AdlInterfaceNodeWithParameters result =
	  new AdlInterfaceNodeWithParameters(parameterizedName_,solver.getString(parameterizedName_));

	// Set signature
	if (parameterizedSignature_ != null) {
	  result.setSignature(parameterizedSignature_,solver.getString(parameterizedSignature_));
	}

	// Set cardinality
	if (parameterizedCardinality_ != null) {
	  result.setCardinality(parameterizedCardinality_, solver.getString(parameterizedCardinality_));
	}
	// Set contingency
	if (parameterizedContingency_ != null) {
	  result.setContingency(parameterizedContingency_, solver.getString(parameterizedContingency_));
	}
	// Set role
	if (parameterizedRole_ != null) {
	  result.setRole(parameterizedRole_, solver.getString(parameterizedRole_));
	}
	return result;
  }

}
