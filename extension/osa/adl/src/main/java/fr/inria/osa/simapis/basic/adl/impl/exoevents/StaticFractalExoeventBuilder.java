/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.simapis.basic.adl.impl.exoevents;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Map;

import fr.inria.osa.simapis.basic.adl.interfaces.exoevents.ExoeventAttributes;
import fr.inria.osa.simapis.basic.adl.interfaces.exoevents.ExoeventBuilder;
import fr.inria.osa.simapis.basic.simulation.ExoEventsTypes;


/**
 * A Java based implementation of the {@link ExoeventBuilder}
 * interface.
 */
public class StaticFractalExoeventBuilder implements ExoeventBuilder,
  ExoeventAttributes {

  /**
   * Print a given message on a given stream.
   *
   * @param message
   *        The message to print.
   * @param printStream
   *        The stream.
   * @return Always <code>true</code>
   */
  public static boolean debugMsg(final String message,
	final PrintStream printStream) {
	printStream.println(message + "\n");
	return true;
  }

  /**
   * FIXME : Missing JavaDoc.
   */
  private String exoeventType_;


  /** {@inheritDoc} */
  public final String getExoeventType() {
	return exoeventType_;
  }


  /** {@inheritDoc} */
  public final void setExoevent(final Object component, final String itf,
	final String name, final ExoEventsTypes type, final String time,
	final String method, final String param,
	final Object context) throws Exception {

	assert StaticFractalExoeventBuilder.debugMsg("ADL compiler: setExoevent('"
												 + name + "'," + itf + "."
												 + method + "(" + param + "),"
												 + type + "," + time + ")",
	  System.err);

	// final Interface fcItf = (Interface) ((Component)
	// component).getFcInterface(itf);
	// final SimulationController sc = (SimulationController)
	// ((Component)
	// component).getFcInterface("simulation-controller");
	// String[] result;
	// if (param == null){
	// result = null;
	// }
	// else {
	// result = param.split(",");
	// }
	// sc.scheduleMyself(method, result, Long.parseLong(time));

	@SuppressWarnings("unchecked")
	final PrintWriter pw =
	  ((Map<String, PrintWriter>) context).get("printwriter");

	// FIXME : Check casts in the following string
	pw.println("((fr.inria.osa.simapis.basic.SimulationProcessAPI)"
			   + " ((org.objectweb.fractal.api.Component) " + component
			   + ").getFcInterface(\"simulation-controller\"))"
			   + ".scheduleMyself(\"" + method + "\", " + param
			   + "==null?null:\"" + param
			   + "\".split(\",\"), fr.inria.osa.simapis.basic.TimeUnit.parseTime(\"" + time
			   + "\"));");

  }


  /** {@inheritDoc} */
  public final void setExoeventType(final String exoeventType) {
	exoeventType_ = exoeventType;
  }

}
