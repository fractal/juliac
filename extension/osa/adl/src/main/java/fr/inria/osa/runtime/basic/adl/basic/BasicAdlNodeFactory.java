/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl.basic;

import java.util.ArrayList;

import fr.inria.osa.runtime.basic.AbstractAdlNodeFactory;
import fr.inria.osa.runtime.basic.AdlNodeFactoryInterface;
import fr.inria.osa.runtime.basic.Pair;

/**
 * This factory correspond to basic ADL node.
 */
public class BasicAdlNodeFactory extends AbstractAdlNodeFactory implements
  AdlNodeFactoryInterface {

  /**
   * {@inheritDoc}
   */
  public AdlAttributesNode newAdlAttributesNode(String name, String value) {
	return new AdlAttributesNode(getString(name), getString(value));
  }


  /**
   * {@inheritDoc}
   */
  public AdlBindingNode newAdlBindingNode(String client, String server) {
	return new AdlBindingNode(getString(client), getString(server));
  }


  /**
   * {@inheritDoc}
   */
  public AdlComponentNode newAdlComponentNode(String pameterizedName,
	boolean isRoot) {
	return new AdlComponentNode(getString(pameterizedName), isRoot);
  }


  /**
   * {@inheritDoc}
   */
  public AdlExoEventNode newAdlExoEventNode(String name, String type,
	String time, String method) {
	return new AdlExoEventNode(getString(name), getString(type),
							   Long.parseLong(getString(time)),
							   getString(method));
  }


  /**
   * {@inheritDoc}
   */
  public AdlInterfaceNode newAdlInterfaceNode(String name) {
	return new AdlInterfaceNode(getString(name));
  }


  /**
   * {@inheritDoc}
   */
  public void setController(AdlComponentNode component, String controller) {
	component.setController(getString(controller));
  }


  /**
   * {@inheritDoc}
   */
  public void setContent(AdlComponentNode component, String content) {
	component.setContent(getString(content));
  }


  /**
   * {@inheritDoc}
   */
  public void setExoEventsSignature(AdlComponentNode component,
	String exoEventsSignature) {
	component.getExoEvents().setSignature(getString(exoEventsSignature));
  }


  /**
   * {@inheritDoc}
   */
  public void setAttributesSignature(AdlComponentNode component,
	String attributesSignature) {
	component.getAttributes().setSignature(getString(attributesSignature));
  }


  /**
   * {@inheritDoc}
   */
  public void addParent(AdlComponentNode component, String extendsString,
	AdlComponentNode root) {
	ArrayList<Pair<String, ArrayList<Pair<String, String>>>> parents =
	  AbstractAdlNodeFactory.parseExtendsString(getString(extendsString));
	for (Pair<String, ArrayList<Pair<String, String>>> currentParent : parents) {
	  if (currentParent.getKey().contains("/")) {
		assert (root != null && parents.size() == 1);
		component.setSharedDefinition(root, currentParent.getKey());
	  } else {
		component.addParent(currentParent.getKey(), currentParent.getValue());
	  }
	}

  }


  /**
   * {@inheritDoc}
   */
  public void addSubComponent(AdlComponentNode component,
	AdlComponentNode subComponent) {
	component.addSubcomponent(subComponent);
  }


  /**
   * {@inheritDoc}
   */
  public void addBinding(AdlComponentNode component, AdlBindingNode binding) {
	component.addBinding(binding);
  }


  /**
   * {@inheritDoc}
   */
  public void addInterface(AdlComponentNode component,
	AdlInterfaceNode newInterface) {
	component.addInterface(newInterface);
  }


  /**
   * {@inheritDoc}
   */
  public void addAttribute(AdlComponentNode component,
	AdlAttributesNode attribute) {
	component.addAttribute(attribute);
  }


  /**
   * {@inheritDoc}
   */
  public void addExoEvent(AdlComponentNode component, AdlExoEventNode exoEvent) {
	component.addExoEvent(exoEvent);
  }



}
