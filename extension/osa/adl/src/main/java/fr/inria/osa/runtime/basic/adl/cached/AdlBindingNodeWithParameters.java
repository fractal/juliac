/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl.cached;

import fr.inria.osa.runtime.basic.adl.ParameterizedStringSolver;
import fr.inria.osa.runtime.basic.adl.basic.AdlBindingNode;

public class AdlBindingNodeWithParameters extends AdlBindingNode {

  /** The parameterized client part of this binding. */
  private String parameterizedClient_;
  /** The parameterized server part of this binding. */
  private String parameterizedServer_;


  /**
   * Initialized this parameterized binding.
   *
   * @param parameterizedClient
   *        The client part without parameter solving.
   * @param parameterizedServer
   *        The server part without parameter solving.
   * @param client
   *        The client part with parameter solving.
   * @param server
   *        The server part with parameter solving.
   */
  protected AdlBindingNodeWithParameters(String parameterizedClient,
									String parameterizedServer, String client,
									String server) {
	super(client, server);
	parameterizedClient_ = parameterizedClient;
	parameterizedServer_ = parameterizedServer;
  }


  /**
   * Create a new instance of this parameterized binding.
   *
   * @param solver
   *        The parameterized string solver used to set parameters
   *        value.
   * @return The newly created binding.
   */
  AdlBindingNode newInstance(ParameterizedStringSolver solver) {
	return new AdlBindingNode(solver.getString(parameterizedClient_),
							  solver.getString(parameterizedServer_));
  }

}
