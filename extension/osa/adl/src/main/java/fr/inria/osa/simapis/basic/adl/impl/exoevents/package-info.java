/**
 * A Fractal ADL extension to support the scheduling of exogeneous events
 * directly from the ADL
 *
 * @author odalle
 */
package fr.inria.osa.simapis.basic.adl.impl.exoevents;
