/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

import fr.inria.osa.runtime.basic.Pair;

/**
 * Manage XML attribute that contain ADL parameters.
 */
public class XMLAttributesList {

  /** The attributes list. */
  private ArrayList<Pair<String, String>> attributesList_;

  /**
   * Initialize this new attributes list.
   *
   */
  public XMLAttributesList() {
	attributesList_ = new ArrayList<>();
  }


  /**
   * Add an attribute to this list.
   *
   * @param name
   *        The name of the attribute.
   * @param value
   *        The value of the attribute.
   */
  public final void addAttributes(final String name, final String value) {
	attributesList_.add(new Pair<String, String>(name, value));
  }


  /**
   * Give an iterator on this attribute list.
   *
   * @return An iterator on this attribute list.
   */
  public final Iterator<Pair<String, String>> iterator() {
	return attributesList_.iterator();
  }


  /**
   * Give the size of this attributes list.
   *
   * @return The size of this list.
   */
  public final int size() {
	return attributesList_.size();
  }


  /**
   * Say if this attribute list contains an attribute with the given
   * name.
   *
   * @param name
   *        The name of the attribute.
   * @return <code>true</code> if this list contains an attribute with
   *         the given name and <code>false</code> otherwise.
   */
  public final boolean contains(final String name) {
	for (Pair<String, String> current : attributesList_) {
	  if (current.getKey().compareTo(name) == 0) {
		return true;
	  }
	}
	return false;
  }


  /**
   * Give the value associated with the given attribute name.
   *
   * @param name
   *        The name of the attribute.
   * @return The value associated with the given attribute name.
   */
  public final String getAttributeValue(final String name) {
	for (Pair<String, String> current : attributesList_) {
	  if (current.getKey().compareTo(name) == 0) {
		return current.getValue();
	  }
	}
	throw new NoSuchElementException("The attribute named " + name
									 + " does't belong to this list");
  }

}
