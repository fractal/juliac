/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import fr.inria.osa.runtime.basic.adl.ParameterIterator;
import fr.inria.osa.runtime.basic.adl.basic.AdlComponentNode;
import fr.inria.osa.runtime.basic.adl.cached.CachedAdlNodeFactory;
import fr.inria.osa.simapis.basic.exceptions.OsaAPIException;

public class AdlLoader {

  /** Associate each filename with the component define in this file */

  /** The MGL sax reader */
  private InternalSaxReader realReader_;

  /** The reader */
  private XMLReader         saxReader_;

  /** The name of the XML file */
  @SuppressWarnings("unused")
private String            filename_;

  /** The input source used to read XML file */
  private InputSource       source_;

  /** Say if the file is parsed */
  private boolean           alreadyParse_ = false;


  /**
   * Create a new SAX reader.
   *
   * @param filename
   *        The filename (used for cache)
   * @param source
   *        The {@link InputSource} used to read XML stream.
   * @param factory
   *        The factory used to create object read in this file.
   */
  public AdlLoader(String filename, InputSource source,
				   AbstractAdlNodeFactory factory) {
	this(filename, source, new ArrayList<Pair<String, String>>(), factory);
  }


  /**
   * Create a new loader.
   *
   * @param filename
   *        The filename.
   * @param source
   *        The file content as InputSource.
   * @param parameters
   *        The list of valued parameters.
   * @param factory
   *        The factory used to create object read in this file.
   */
  public AdlLoader(String filename, InputSource source,
				   ArrayList<Pair<String, String>> parameters,
				   AbstractAdlNodeFactory factory) {
	filename_ = filename;
	source_ = source;
	realReader_ = new InternalSaxReader(filename, parameters, factory);
  }


  public static AdlLoader CreateAdlLoader(String chaine) {
	String filename = chaine;
	ArrayList<Pair<String, String>> parametersList =
	  new ArrayList<>();

	int openBracketIndex = chaine.indexOf('(');
	if (openBracketIndex != -1) {
	  String parameters = chaine.replaceFirst("^.*\\(", "");
	  parameters = parameters.replaceFirst("\\)", "");
	  // Add parameter
	  Iterator<Pair<String, String>> argumentIterator =
		new ParameterIterator(parameters, "=>");
	  while (argumentIterator.hasNext()) {
		Pair<String, String> argAndValue = argumentIterator.next();
		if (argAndValue.getValue() == null) {
		  throw new IllegalArgumentException(
											 "The parameter valuation must be parameterName=>parameterValue");
		}
		parametersList.add(argAndValue);
	  }
	  filename = chaine.substring(0, openBracketIndex);
	}
	filename = filename.replace('.', '/');

	InputSource source =
	  new InputSource(Class.class.getClass().getResourceAsStream(
		"/" + filename + ".fractal"));
	return new AdlLoader(filename, source, parametersList, new CachedAdlNodeFactory());
  }


  /**
   * Parse and load the ADL file.
   *
   * @throws ParserConfigurationException
   */
  public void parse() {
	if (!alreadyParse_) {
	  alreadyParse_ = true;
	  try {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(false);
		// saxReader_ = XMLReaderFactory.createXMLReader();
		saxReader_ =
		  XMLReaderFactory
						  .createXMLReader("org.apache.xerces.parsers.SAXParser"); //$NON-NLS-1$

		// Desactivate DTD validation (validate with schema)
		saxReader_.setFeature("http://xml.org/sax/features/validation", false); //$NON-NLS-1$
		saxReader_.setContentHandler(realReader_);
		saxReader_.setEntityResolver(realReader_);
		saxReader_.parse(source_);
	  } catch (IOException e) {
		e.printStackTrace();
	  } catch (SAXException e) {
		e.printStackTrace();
	  }
	}
  }

  /**
   * Launch the application.
   *
   * @throws SecurityException
   * @throws IllegalArgumentException
   * @throws InstantiationException
   * @throws NoSuchInterfaceException
   * @throws IllegalContentException
   * @throws IllegalLifeCycleException
   * @throws IllegalBindingException
   * @throws NoSuchMethodException
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   * @throws ParserConfigurationException
 * @throws OsaAPIException
   */
  public Component launch() throws
	InstantiationException, NoSuchInterfaceException, IllegalContentException,
	IllegalLifeCycleException, IllegalBindingException, NoSuchMethodException,
	IllegalAccessException, InvocationTargetException,
	OsaAPIException {
	AdlComponentNode root = getRoot();
	root.merge();
	Component c = root.getOrCreateComponent();
	return c;
  }



  /**
   * Give the root component of ADL file.
   *
   * @return The root component of the ADL file.
   * @throws ParserConfigurationException
   */
  public AdlComponentNode getRoot() {
	if (!alreadyParse_)
	  parse();
	return realReader_.getRoot();
  }


  /**
   * Give the list of shared component find in this file.
   *
   * @return The list of shared component find in this file.
   */
  public ArrayList<AdlComponentNode> getSharedComponentNode() {
	return realReader_.getSharedComponentNode();
  }
}
