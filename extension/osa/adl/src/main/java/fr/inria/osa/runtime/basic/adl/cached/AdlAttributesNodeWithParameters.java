/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl.cached;

import fr.inria.osa.runtime.basic.adl.ParameterizedStringSolver;
import fr.inria.osa.runtime.basic.adl.basic.AdlAttributesNode;

public class AdlAttributesNodeWithParameters extends AdlAttributesNode {

  /** The name of this attributes without parameters resolution. */
  private String parameterizedName_;

  /** The value of this attributes without parameters resolution. */
  private String parameterizedValue_;


  /**
   * Initialize this ADL attributes.
   *
   * @param parameterizedName
   *        The name of this attribute without parameter solving.
   * @param parameterizedValue
   *        The value of this attribute without parameter solving.
   * @param name
   *        The name of this attribute with parameter solving.
   * @param value
   *        The value of this attribute with parameter solving.
   */
  protected AdlAttributesNodeWithParameters(String parameterizedName,
									   String parameterizedValue, String name,
									   String value) {
	super(name, value);
	parameterizedName_ = parameterizedName;
	parameterizedValue_ = parameterizedValue;
  }


  /**
   * Create a new instance of this parameterized attributes.
   *
   * @param solver
   *        The parameterized string solver used to set parameters
   *        value.
   * @return The newly created attributes.
   */
  AdlAttributesNode newInstance(ParameterizedStringSolver solver) {
	return new AdlAttributesNode(solver.getString(parameterizedName_),
								 solver.getString(parameterizedValue_));
  }

}
