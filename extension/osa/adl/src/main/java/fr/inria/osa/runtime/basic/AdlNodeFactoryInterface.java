/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic;

import fr.inria.osa.runtime.basic.adl.basic.AdlAttributesNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlBindingNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlComponentNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlExoEventNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlInterfaceNode;

/**
 * Factory used to create each type of ADL node. This factory is used
 * by {@link InternalSaxReader}.
 */
public interface AdlNodeFactoryInterface {

  /**
   * Create a new {@link AdlAttributesNode}
   *
   * @param name
   *        The name of the attribute.
   * @param value
   *        The value of the attribute.
   * @return The newly created attribute.
   */
  AdlAttributesNode newAdlAttributesNode(String name, String value);


  /**
   * Create a new {@link AdlBindingNode}.
   *
   * @param client
   *        The client part of the binding.
   * @param server
   *        The server part of the binding.
   * @return The newly created binding.
   */
  AdlBindingNode newAdlBindingNode(String client, String server);


  /**
   * Create a new {@link AdlComponentNode}
   *
   * @param parameterizedName
   *        The parameterized name of new ADL component.
   * @param isRoot
   *        Say if this component is a root definition.
   * @return The newly create ADL component.
   */
  AdlComponentNode newAdlComponentNode(String parameterizedName, boolean isRoot);


  /**
   * Create a new {@link AdlExoEventNode}
   *
   * @param name
   *        The name of this new event.
   * @param type
   *        The type of this new event.
   * @param time
   *        The time associated with this new event.
   * @param method
   *        The method associated with this new event.
   * @return The newly create ADL exoevent.
   */
  AdlExoEventNode newAdlExoEventNode(String name, String type, String time,
	String method);


  /**
   * Create a new {@link AdlInterfaceNode}
   *
   * @param name
   *        The name of the interface.
   * @return The newly created ADL interface.
   */
  AdlInterfaceNode newAdlInterfaceNode(String name);


  /**
   * Set the controller of a given component.
   *
   * @param component
   *        The component.
   * @param controller
   *        The controller.
   */
  void setController(AdlComponentNode component, String controller);


  /**
   * Set the content of a given component.
   *
   * @param component
   *        The component.
   * @param content
   *        The content.
   */
  void setContent(AdlComponentNode component, String content);


  /**
   * Set the signature of exoevents of a given component.
   *
   * @param component
   *        The component.
   * @param exoEventsSignature
   *        The exovents signature.
   */
  void setExoEventsSignature(AdlComponentNode component,
	String exoEventsSignature);


  /**
   * Set the signature of attributes of a given component.
   *
   * @param component
   *        The component.
   * @param attributesSignature
   *        The attributes signature.
   */
  void setAttributesSignature(AdlComponentNode component,
	String attributesSignature);


  /**
   * Parse the given extends string and add each couple
   * (parent,parameter) to the parent of the given component.
   *
   * @param component
   *        The component.
   * @param extendsString
   *        The string describing the extends of the given component.
   * @param root
   *        The root used for shared definition path.
   */
  void addParent(AdlComponentNode component, String extendsString,
	AdlComponentNode root);


  /**
   * Add a sub component to a given component.
   *
   * @param component
   *        The super component.
   * @param subComponent
   *        The sub component to add.
   */
  void addSubComponent(AdlComponentNode component, AdlComponentNode subComponent);


  /**
   * Add a binding to a given component.
   *
   * @param component
   *        The component.
   * @param binding
   *        The binding to add.
   */
  void addBinding(AdlComponentNode component, AdlBindingNode binding);


  /**
   * Add an interface to a given component.
   *
   * @param component
   *        The component.
   * @param newInterface
   *        The interface to add.
   */
  void addInterface(AdlComponentNode component, AdlInterfaceNode newInterface);


  /**
   * Add an exoEvent to a given component.
   *
   * @param component
   *        The component.
   * @param exoEvent
   *        The exoEvent to add to the given component.
   */
  void addExoEvent(AdlComponentNode component, AdlExoEventNode exoEvent);

  /**
   * Add an attribute to a given component.
   *
   * @param component
   *        The component.
   * @param attribute
   *        The attribute to add to the given component.
   */
  void addAttribute(AdlComponentNode component, AdlAttributesNode attribute);

}
