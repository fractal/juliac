/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.inria.osa.runtime.basic.Pair;

/**
 * Instantiate classic string from parameterized string according to
 * parameter value.
 */
public class ParameterizedStringSolver {

  /** Pattern used to know if string contains parameters. */
  private static Pattern containsParameters_;

  static {
	containsParameters_ = Pattern.compile(".*\\$\\{.*\\}.*");
  }


  /**
   * Say if a given string contains parameters.
   *
   * @param string
   *        The string.
   * @return <code>true</code> if the given string contains parameters
   *         and <code>false</code> otherwise.
   */
  public static boolean containParameters(final String string) {
	Matcher match = containsParameters_.matcher(string);
	return match.matches();
  }

  /** Set of defined parameters. */
  private HashMap<String, String>         parameters_;

  /** The list of valued parameters used to initialize this solver. */
  private ArrayList<Pair<String, String>> valuedParameterList_;


  /**
   * Initialize this parameterized string solver.
   *
   * @param valuedParameterList
   *        The list of valued parameters.
   */
  @SuppressWarnings("unchecked")
  public ParameterizedStringSolver(
								   final ArrayList<Pair<String, String>> valuedParameterList) {
	valuedParameterList_ =
	  (ArrayList<Pair<String, String>>) valuedParameterList.clone();
	parameters_ = new HashMap<>();
	for (Pair<String, String> currentValuedParameter : valuedParameterList) {
	  if (currentValuedParameter.getKey() == null
		  || currentValuedParameter.getValue() == null) {
		throw new IllegalArgumentException(
										   "Some parameters name or value is null");
	  }
	  parameters_.put(currentValuedParameter.getKey(),
		currentValuedParameter.getValue());
	}
  }


  /**
   * Give the string represented by this parameterized string.
   *
   * @param string
   *        The string that must be resolved.
   * @return The string represented by this parameterized string
   *         according to parameters values.
   */
  public final String getString(final String string) {
	String result = string;
	Set<String> usedParameter = findParameters(string);
	for (String currentParam : usedParameter) {
	  String value = parameters_.get(currentParam);
	  if (value == null) {
		throw new IllegalArgumentException("Parameter " + currentParam
										   + " is not fixed");
	  }
	  result = result.replaceAll("\\$\\{" + currentParam + "\\}", value);
	}
	return result;
  }


  /**
   * Find all parameters present in a given string.
   *
   * @param string
   *        The string.
   * @return The set of parameters present in the given string.
   */
  private Set<String> findParameters(final String string) {
	HashSet<String> result = new HashSet<>();
	String tmpString = string;
	while (tmpString.length() != 0) {
	  tmpString = tmpString.replaceFirst("^[^$]*", "");
	  if (tmpString.startsWith("${")) {
		tmpString = tmpString.replaceFirst("\\$\\{", "");
		String[] split = tmpString.split("\\}");
		assert (split[0].length() != 0);
		result.add(split[0]);
		tmpString = tmpString.replaceFirst("[^\\}]*?\\}", "");
	  }
	}
	return result;
  }


  /**
   * Give a copy of the valued parameters list used to create this
   * {@link ParameterizedStringSolver}.
   *
   * @return A copy of the valued parameters list used to create this
   *         {@link ParameterizedStringSolver}.
   */
  @SuppressWarnings("unchecked")
  public final ArrayList<Pair<String, String>> getValuedParameterList() {
	return (ArrayList<Pair<String, String>>) valuedParameterList_.clone();
  }
}
