/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl.cached;

import java.util.ArrayList;

import org.xml.sax.InputSource;

import fr.inria.osa.runtime.basic.AdlLoader;
import fr.inria.osa.runtime.basic.Pair;
import fr.inria.osa.runtime.basic.adl.AdlUtil;
import fr.inria.osa.runtime.basic.adl.basic.AdlComponentNode;

public class CachedUtility {
  /**
   * Merge the component tree present in a given file with a given
   * component tree. We must note that the file is not always read, we
   * can used a cached version of the component tree.
   *
   * @param component
   *        The main component tree.
   * @param filename
   *        The filename to parse (without post-fix .fractal)
   * @param parametersList
   *        The list of parameter.
   */
  static void mergeWithFile(AdlComponentNode component, String filename,
	ArrayList<Pair<String, String>> parametersList) {
	AdlComponentNode result;
	String currentParentFilename = filename;
	if (AdlComponentNodeWithParameters.filenameToCachedNode_
															.containsKey(currentParentFilename)) {
	  AdlComponentNodeWithParameters cachedNode =
		AdlComponentNodeWithParameters.filenameToCachedNode_
															.get(currentParentFilename);
	  result = cachedNode.newInstance(parametersList, null);
	  result.merge();
	  if (AdlComponentNodeWithParameters.rootToSharedComponent_.get(result) != null) {
		AdlUtil
			   .mergeSharedComponent(AdlComponentNodeWithParameters.rootToSharedComponent_
																						  .get(result));
		AdlComponentNodeWithParameters.rootToSharedComponent_.remove(result);
	  }
	  AdlUtil.extendNode(result, component,component);
	} else {
	  InputSource source =
		new InputSource(component.getClass().getResourceAsStream(
		  "/" + currentParentFilename + ".fractal"));
	  AdlLoader loader =
		new AdlLoader(filename, source, parametersList,
					  new CachedAdlNodeFactory());
	  try {
		loader.parse();
		result = loader.getRoot();
		AdlComponentNodeWithParameters.filenameToCachedNode_.put(
		  currentParentFilename, (AdlComponentNodeWithParameters) result);
		((AdlComponentNodeWithParameters) result).merge();
		AdlUtil.mergeSharedComponent(loader.getSharedComponentNode());
		AdlUtil.extendNode(result, component,component);
	  } catch (Exception e) {
		if (!(e instanceof IllegalArgumentException)) {
		  e.printStackTrace();
		  throw new IllegalArgumentException("Merge error in "
											 + currentParentFilename + ": "
											 + e.getMessage());

		}
		throw new IllegalArgumentException(e.getMessage());
	  }
	}
  }


  /**
   * Apply merge on the given component.
   *
   * @param component
   *        The component that will be merge.
   */
  public static void merge(AdlComponentNode component) {

	// Merging my sub components
	for (AdlComponentNode currentSubComponent : component.getSubComponents().values()) {
	  currentSubComponent.merge();
	}

	// Merging with my parents
	for (Pair<String, ArrayList<Pair<String, String>>> currentParent : component
																				.getParents()) {
	  if (!component.isShared()) {
		String currentParentFilename = currentParent.getKey().replace('.', '/');
		mergeWithFile(component, currentParentFilename,
		  currentParent.getValue());
	  }
	}
	component.getParents().clear();
	component.setMerged();
  }
}
