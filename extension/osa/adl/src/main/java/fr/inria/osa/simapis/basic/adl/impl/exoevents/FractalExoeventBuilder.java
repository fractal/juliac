/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.simapis.basic.adl.impl.exoevents;

import java.io.PrintStream;

import org.objectweb.fractal.api.Component;

import fr.inria.osa.simapis.basic.ProcessModelingAPI;
import fr.inria.osa.simapis.basic.adl.interfaces.exoevents.ExoeventAttributes;
import fr.inria.osa.simapis.basic.adl.interfaces.exoevents.ExoeventBuilder;
import fr.inria.osa.simapis.basic.simulation.ExoEventsTypes;
import fr.inria.osa.simapis.basic.simulation.SimulationInternalSchedulingItf;
import fr.inria.osa.simapis.basic.TimeUnit;

/**
 * A Java based implementation of the {@link ExoeventBuilder}
 * interface.
 */
public class FractalExoeventBuilder implements ExoeventBuilder,
  ExoeventAttributes {

  /**
   * Print a given message on a given stream.
   *
   * @param message
   *        The message to print.
   * @param printStream
   *        The stream.
   * @return Always <code>true</code>
   */
  public static boolean debugMsg(final String message,
	final PrintStream printStream) {
	printStream.println(message + "\n");
	return true;
  }

  /**
   * FIXME : Missing JavaDoc.
   */
  private String exoeventType_;


  /** {@inheritDoc} */
  public final String getExoeventType() {
	return exoeventType_;
  }


  /** {@inheritDoc} */
  public final void setExoevent(final Object component, final String itf,
	final String name, final ExoEventsTypes type, final String time,
	final String method, final String param,
	final Object context) throws Exception {

	assert FractalExoeventBuilder.debugMsg("ADL compiler: setExoevent('" + name
										   + "'," + itf + "." + method + "("
										   + param + ")," + type + "," + time
										   + ")", System.err);

	// final Interface fcItf = (Interface) ((Component)
	// component).getFcInterface(itf);
	final SimulationInternalSchedulingItf sc =
	  (SimulationInternalSchedulingItf) ((Component) component).getFcInterface("scheduling-controller");
	String[] result;
	if (param == null) {
	  result = null;
	} else {
	  result = param.split(",");
	}
	sc.scheduleMyself(method, result, TimeUnit.parseTime(time), type);

  }


  /** {@inheritDoc} */
  public final void setExoeventType(final String exoeventType) {
	exoeventType_ = exoeventType;
  }

}
