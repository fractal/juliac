/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;

import fr.inria.osa.runtime.basic.adl.AdlDumpableNodeInterface;
import fr.inria.osa.runtime.basic.adl.ParameterIterator;
import fr.inria.osa.runtime.basic.adl.ParameterizedStringSolver;
import fr.inria.osa.runtime.basic.adl.XMLAttributesList;
import fr.inria.osa.runtime.basic.adl.basic.AdlComponentNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlInterfaceNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlInterfaceNode.InterfaceCardinality;
import fr.inria.osa.runtime.basic.adl.basic.AdlInterfaceNode.InterfaceContingency;
import fr.inria.osa.runtime.basic.adl.basic.AdlInterfaceNode.InterfaceRole;

public abstract class AbstractAdlNodeFactory implements AdlNodeFactoryInterface {

  /** The solver used to create String from parameterized string. */
  private ParameterizedStringSolver solver_;


  /**
   * Give the string from a parameterized string.
   *
   * @param parameterizedString
   *        The given parameterized string.
   * @return The string with all parameters resolved..
   */
  protected String getString(String parameterizedString) {
	if (solver_ == null) {
	  throw new IllegalArgumentException(
										 "The parameterized string solver have not been define.");
	}
	return solver_.getString(parameterizedString);
  }


  /**
   * Give the list of argument with their default value (when define)
   * corresponding to the given argument string.
   *
   * @param arguments
   *        The string representing arguments definition.
   * @return The list of argument with their default value (when
   *         define) corresponding to the given argument string.
   */
  protected ArrayList<Pair<String, String>> parseArgumentsList(String arguments) {
	Iterator<Pair<String, String>> argumentIterator =
	  new ParameterIterator(arguments, "=");
	ArrayList<Pair<String, String>> result =
	  new ArrayList<>();
	while (argumentIterator.hasNext()) {
	  result.add(argumentIterator.next());
	}
	return result;
  }


  /**
   * Update the given parameters list with the default value
   * parameters.
   *
   * @param givenValuedParametersList
   *        The given arguments list.
   * @param argumentsList
   *        The list of defined argument with their default value when
   *        define.
   */
  public static void updateParametersWithDefaultValue(
	ArrayList<Pair<String, String>> givenValuedParametersList,
	ArrayList<Pair<String, String>> argumentsList) {
	int index = 0;
	Iterator<Pair<String, String>> argumentIterator = argumentsList.iterator();
	while (argumentIterator.hasNext()) {
	  Pair<String, String> argAndValue = argumentIterator.next();
	  if (index < givenValuedParametersList.size()) {
		if (givenValuedParametersList.get(index).getKey() == null) {
		  givenValuedParametersList.set(index,
			new Pair<String, String>(argAndValue.getKey(),
									 givenValuedParametersList.get(index)
															  .getValue()));
		}
	  }
	  if (argAndValue.getValue() != null) {
		boolean find = false;
		for (int i = 0; i < givenValuedParametersList.size(); i++) {
		  if (argAndValue.getKey().equals(
			givenValuedParametersList.get(i).getKey())) {
			find = true;
			break;
		  }
		}
		if (!find) {
		  givenValuedParametersList.add(argAndValue);
		}
	  }
	  index++;
	}
  }


  /**
   * Parse the given extends (or definition) string and give the
   * corresponding couple list (parentName,parametersValue).
   *
   * @param extendsString
   *        The string representing parent definition.
   * @return The list of couple (parentName,parametersValue).
   */
  @SuppressWarnings("unchecked")
  public static ArrayList<Pair<String, ArrayList<Pair<String, String>>>> parseExtendsString(
	String extendsString) {
	ArrayList<Pair<String, ArrayList<Pair<String, String>>>> result =
	  new ArrayList<>();
	char[] extendsStringAsByte = extendsString.toCharArray();
	int nbBracket = 0;
	int beginParentName = 0;
	int beginParameters = 0;
	String parameterString;
	String parentName = null;
	ArrayList<Pair<String, String>> parameters =
	  new ArrayList<>();
	for (int i = 0; i < extendsStringAsByte.length; i++) {
	  if (extendsStringAsByte[i] == '(') {
		if (nbBracket == 0) {
		  parentName =
			new String(extendsStringAsByte, beginParentName, i
															 - beginParentName);
		  beginParameters = i + 1;
		}
		nbBracket++;
	  } else if (extendsStringAsByte[i] == ')') {
		nbBracket--;
		if (nbBracket == 0) {
		  parameterString =
			new String(extendsStringAsByte, beginParameters, i
															 - beginParameters);
		  ParameterIterator parameterIterator =
			new ParameterIterator(parameterString, "=>");
		  while (parameterIterator.hasNext()) {
			parameters.add(parameterIterator.next());
		  }
		}
	  } else if (extendsStringAsByte[i] == ',' && nbBracket == 0) {
		if (parameters.size() == 0) {
		  parentName =
			new String(extendsStringAsByte, beginParentName, i
															 - beginParentName);
		}
		ArrayList<Pair<String, String>> parametersCopy =
		  (ArrayList<Pair<String, String>>) parameters.clone();
		Pair<String, ArrayList<Pair<String, String>>> newParent =
		  new Pair<>(parentName,
															parametersCopy);
		result.add(newParent);
		beginParentName = i + 1;
		parameters.clear();
	  }
	}
	if (parameters.size() == 0) {
	  parentName =
		new String(extendsStringAsByte, beginParentName,
				   extendsStringAsByte.length - beginParentName);
	}
	result.add(new Pair<String, ArrayList<Pair<String, String>>>(parentName,
																 parameters));
	return result;
  }


  /**
   * Create a new component from definition node.
   *
   * @param stack
   *        FIXME
   * @param attributesList
   *        The attribute list associated with this node.
   * @param valuedParametersList
   *        FIXME
   * @return The created ADL component.
   */
  public AdlComponentNode createComponentFromDefinition(
	Stack<AdlDumpableNodeInterface> stack, XMLAttributesList attributesList,
	ArrayList<Pair<String, String>> valuedParametersList) {
	AdlComponentNode result;

	// Create a new component
	// FIXME : Name can contains parameters ?
	String componentName = attributesList.getAttributeValue("name");

	if (attributesList.contains("arguments")) {
	  ArrayList<Pair<String, String>> argumentList =
		parseArgumentsList(attributesList.getAttributeValue("arguments"));
	  AbstractAdlNodeFactory.updateParametersWithDefaultValue(valuedParametersList, argumentList);
	}
	for (int i = 0; i < valuedParametersList.size(); i++) {
	  if (valuedParametersList.get(i).getKey() == null) {
		throw new IllegalArgumentException(
										   "Parameters is inconstant (find a value without name)");
	  }
	}
	ParameterizedStringSolver parameterizedStringSolver =
	  new ParameterizedStringSolver(valuedParametersList);

	solver_ = parameterizedStringSolver;
	result = newAdlComponentNode(componentName, true);

	if (attributesList.contains("extends")) {
	  addParent(result, attributesList.getAttributeValue("extends"), null);
	}
	stack.push(result);
	return result;
  }


  /**
   * Create a new component from component node.
   *
   * @param stack
   *        The stack used to store new component.
   * @param attributesList
   *        The attribute list associated with this node.
   * @param root
   *        The current root component (used as root of shared
   *        definition).
   */
  public void createComponentFromComponentNode(
	Stack<AdlDumpableNodeInterface> stack, XMLAttributesList attributesList,
	AdlComponentNode root) {
	String componentName = attributesList.getAttributeValue("name");

	AdlComponentNode newComponent = newAdlComponentNode(componentName, false);
	if (attributesList.contains("definition")) {
	  addParent(newComponent, attributesList.getAttributeValue("definition"),
		root);
	}
	stack.push(newComponent);
  }


  /**
   * Create a new interface.
   *
   * @param stack
   *        TODO
   * @param attributesList
   *        The attribute list associated with this node.
   */
  public void createInterface(Stack<AdlDumpableNodeInterface> stack,
	XMLAttributesList attributesList) {
	String interfaceName = getString(attributesList.getAttributeValue("name"));

	AdlInterfaceNode current = newAdlInterfaceNode(interfaceName);
	if (attributesList.contains("role")) {
	  String interfaceRole =
		getString(attributesList.getAttributeValue("role"));
	  if (interfaceRole.compareToIgnoreCase("server") == 0) {
		current.setRole(InterfaceRole.server);
	  } else {
		current.setRole(InterfaceRole.client);
	  }
	}
	if (attributesList.contains("signature")) {
	  String interfaceSignature =
		getString(attributesList.getAttributeValue("signature"));
	  current.setSignature(interfaceSignature);
	}
	if (attributesList.contains("cardinality")) {
	  String interfaceCardinality =
		getString(attributesList.getAttributeValue("cardinality"));
	  if (interfaceCardinality.compareToIgnoreCase("singleton") == 0) {
		current.setCardinality(InterfaceCardinality.singleton);
	  } else {
		current.setCardinality(InterfaceCardinality.collection);
	  }
	}
	if (attributesList.contains("contingency")) {
	  String interfaceContingency =
		getString(attributesList.getAttributeValue("contingency"));
	  if (interfaceContingency.compareToIgnoreCase("optional") == 0) {
		current.setContingency(InterfaceContingency.optional);
	  } else {
		current.setContingency(InterfaceContingency.mandatory);
	  }
	}
	stack.push(current);
  }

}
