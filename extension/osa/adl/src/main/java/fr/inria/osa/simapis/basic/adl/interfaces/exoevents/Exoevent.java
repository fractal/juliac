/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.simapis.basic.adl.interfaces.exoevents;

/**
 * ADL extension module to support exogenous event insertion. This is
 * the AST interface definition for the new ExoEvent node with six
 * attributes : a unique name (identifier) for the event (to avoid
 * duplicates in case the event is inserted in a shared component),
 * the type of the event (SOC, EOR, ...), the currentTime_ of the
 * event, the method name and the interface to which the method belong
 * and an optional parameter.
 *
 * @author odalle FIXME : All methods must be documented
 */
public interface Exoevent {
  /**
   * FIXME : Give the name of the method associated with this event.
   *
   * @return The name of the method associated with this event.
   */
  String getMethod();


  /**
   * Give the name of this event.
   *
   * @return The name of this event.
   */
  String getName();


  /**
   * Give the parameter of the method associated with this event.
   *
   * @return The parameter of the method associated with this event.
   */
  String getParam();


  /**
   * Return the currentTime_ of this event.
   *
   * @return The currentTime_ of this event.
   */
  String getTime();


  /**
   * Give the type of this event.
   *
   * @return The type of this event.
   */
  String getType();


  /**
   * FIXME : Set the method name associated with this event ?
   *
   * @param method
   *        The method name. FIXME : <code>null</code> is a valid
   *        value ?
   */
  void setMethod(String method);


  /**
   * Set the name of this event.
   *
   * @param name
   *        The new name of this event.
   */
  void setName(String name);


  /**
   * FIXME : Set the parameter of the method associated with this
   * event.
   *
   * @param parameter
   *        The parameter of the method associated with this event.
   *        FIXME : <code>null</code> is a valid value ?
   */
  void setParam(String parameter);


  /**
   * Set the currentTime_ of this event.
   *
   * @param time
   *        The currentTime_ of this event.
   */
  void setTime(String time);


  /**
   * Set the type of this event.
   *
   * @param type
   *        The type of this event.
   */
  void setType(String type);
}
