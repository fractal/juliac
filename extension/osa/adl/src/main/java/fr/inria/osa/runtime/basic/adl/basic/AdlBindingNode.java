/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl.basic;

import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;

import fr.inria.osa.runtime.basic.adl.AdlDumpableNodeInterface;
import fr.inria.osa.runtime.basic.adl.AdlInstantiableNodeInterface;
import fr.inria.osa.simapis.basic.exceptions.OsaAPIException;

/**
 * ADL binding representation.
 */
public class AdlBindingNode implements AdlDumpableNodeInterface,
  AdlInstantiableNodeInterface, Cloneable {

  /** The client part of this binding. */
  private String client_;

  /** The server part of this binding. */
  private String server_;


  /**
   * Initialize this binding.
   *
   * @param client
   *        The client part of the binding.
   * @param server
   *        The server part of the binding.
   */
  public AdlBindingNode(final String client, final String server) {
	client_ = client;
	server_ = server;
  }


  /**
   * Give the client part of the binding.
   *
   * @return The client part of the binding as String.
   */
  public final String getClient() {
	return client_;
  }


  /**
   * Give the server part of the binding.
   *
   * @return The server part of the binding as String.
   */
  public final String getServer() {
	return server_;
  }


  /**
   * {@inheritDoc}
   */
  public final void dump(final PrintStream stream,
	final boolean isRoot) {
	stream.printf("<binding client=\"%s\" server=\"%s\"/>\n", client_, server_);
  }


  /**
   * Java instantiation of this binding in a given component.
   *
   * @param adlComponent
   *        The adl component containing this binding.
   * @param fractalComponent
   *        The fractal component in which this binding is created.
   * @throws InstantiationException
   * @throws NoSuchInterfaceException
   * @throws IllegalContentException
   * @throws IllegalLifeCycleException
   * @throws IllegalBindingException
   * @throws NoSuchMethodException
   * @throws InvocationTargetException
   * @throws IllegalAccessException
 * @throws OsaAPIException
   * @throws IllegalArgumentException
   * @throws SecurityException
   */
  public final void instantiate(final AdlComponentNode adlComponent,
	final Component fractalComponent) throws InstantiationException,
	NoSuchInterfaceException, IllegalContentException,
	IllegalLifeCycleException, IllegalBindingException, NoSuchMethodException,
	InvocationTargetException, IllegalAccessException, OsaAPIException {
	AdlComponentNode adlClientNode;
	String clientInterfaceName;
	String serverInterfaceName;
	Component client;
	Component server;

	// Managing client part of binding
	if (getClient().matches(".*[.].*")) {
	  String[] binding = getClient().split("[.]");
	  if (binding[0].equals("this")) {
		adlClientNode = adlComponent;
		client = fractalComponent;
	  } else {
		adlClientNode = adlComponent.getSubComponentNamed(binding[0]);
		if (adlClientNode.isShared()) {
		  adlClientNode = adlClientNode.getRealComponent();
		}
		client = adlClientNode.getOrCreateComponent();
	  }
	  clientInterfaceName = binding[1];
	} else {
	  adlClientNode = adlComponent;
	  client = fractalComponent;
	  clientInterfaceName = getClient();
	}

	if (adlClientNode.isBindedInterface(clientInterfaceName.intern())) {
	  return;
	}
	// Managing server part of binding
	adlClientNode.setInterfaceAsBinded(clientInterfaceName.intern());
	if (getServer().matches(".*[.].*")) {
	  String[] binding = getServer().split("[.]");
	  if (binding[0].equals("this")) {
		server = fractalComponent;
	  } else {
		server =
		  adlComponent.getSubComponentNamed(binding[0]).getOrCreateComponent();
	  }
	  serverInterfaceName = binding[1];
	} else {
	  server = fractalComponent;
	  serverInterfaceName = getServer();
	}

	Object serverInterface;
	if (server == fractalComponent) {
	  serverInterface =
		((ContentController) server.getFcInterface("content-controller"))
																		 .getFcInternalInterface(serverInterfaceName);
	} else {
	  serverInterface = server.getFcInterface(serverInterfaceName);
	}
	((BindingController) client.getFcInterface("binding-controller")).bindFc(
	  clientInterfaceName, serverInterface);

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public final AdlBindingNode clone() {
	try {
	  return (AdlBindingNode) super.clone() ;
	} catch (CloneNotSupportedException e) {
	  // Never exception
	}
	return null;
  }

}
