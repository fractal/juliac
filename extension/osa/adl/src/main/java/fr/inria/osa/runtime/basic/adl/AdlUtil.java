/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl;

import java.util.ArrayList;

import fr.inria.osa.runtime.basic.adl.basic.AdlAttributesNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlBindingNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlComponentNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlExoEventNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlInterfaceNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlComponentNode.AdlComponentType;

/**
 * Various static method used to merge ADL component.
 */
public final class AdlUtil {

  /**
   * Private constructor to forbid creation.
   */
  private AdlUtil() {
	// Nothing to do
  }
  /**
   * Modify field childNode that are not define according to parent
   * node. This operation correspond to childNode extending parent
   * node.
   *
   * @param parentNode
   *        The extended node.
   * @param childNode
   *        The extending node.
   * @param newRoot
   *        The new root used to redefine shared component definition.
   */
  public static void extendNode(final AdlComponentNode parentNode,
	final AdlComponentNode childNode, final AdlComponentNode newRoot) {

	assert (parentNode.getType() == childNode.getType()
			|| parentNode.getType() == AdlComponentType.UNDEFINED || childNode
																			  .getType() == AdlComponentType.UNDEFINED);

	if (childNode.isShared()) {

	  parentNode.setSharedDefinition(childNode.getSharedComponentRoot(),
		childNode.getSharedDefinition());
	} else {
	  for (AdlComponentNode currentSharedComponent : parentNode
															   .getSharedComponentSet()) {
		currentSharedComponent.setSharedDefinition(newRoot,
		  currentSharedComponent.getSharedDefinition());
	  }
	}

	// Adding interfaces
	for (AdlInterfaceNode currentInterface : parentNode.getInterfaces().values()) {
	  childNode.addInterface(currentInterface);
	}

	// Update node2 fields that are not been set
	if (childNode.getContent() == null && parentNode.getContent() != null) {
	  childNode.setContent(parentNode.getContent());
	}

	// Update attributes
	if (parentNode.getAttributes().size() != 0) {
	  if (childNode.getAttributes().size() == 0) {
		for (AdlAttributesNode currentAttribute : parentNode.getAttributes()) {
		  childNode.addAttribute(currentAttribute);
		}
	  } else {
		childNode.getAttributes().merge(parentNode.getAttributes(), false);
	  }
	}

	// Update controller if not define in child
	if (childNode.getController() == null && parentNode.getController() != null) {
	  childNode.setController(parentNode.getController());
	}

	// Update exoevent
	if (parentNode.getExoEvents().size() != 0) {
	  if (childNode.getExoEvents().size() == 0) {
		for (AdlExoEventNode currentEvent : parentNode.getExoEvents()) {
		  childNode.addExoEvent(currentEvent);
		}
	  } else {
		childNode.getExoEvents().merge(parentNode.getExoEvents(), false);
	  }
	}

	// Merge subcomponent
	for (AdlComponentNode parentSubComponent : parentNode.getSubComponents().values()) {
	  AdlComponentNode childSubComponent =
		childNode.getSubComponentNamed(parentSubComponent.getName());
	  if (childSubComponent != null) {
		AdlUtil.extendNode(parentSubComponent, childSubComponent, newRoot);
	  } else {
		childNode.addSubcomponent(parentSubComponent);
	  }
	}

	// Merge bindings
	if (parentNode.getBindings() != null) {
	  for (AdlBindingNode currentBinding : parentNode.getBindings().values()) {
		childNode.addBinding(currentBinding);
	  }
	}
  }


  /**
   * Override fields of realNode with those are defined in sharedNode.
   * This correspond to modifying a node by a shared reference.
   *
   * @param realNode
   *        The extended node.
   * @param sharedNode
   *        The extending node.
   */
  public static void mergeShared(final AdlComponentNode realNode,
	final AdlComponentNode sharedNode) {

	assert (realNode.getType() == sharedNode.getType()
			|| realNode.getType() == AdlComponentType.UNDEFINED || sharedNode
																			 .getType() == AdlComponentType.UNDEFINED);

	// Adding interfaces
	for (AdlInterfaceNode currentInterface : sharedNode.getInterfaces().values()) {
	  realNode.addSharedInterface(currentInterface);
	}

	// Update node2 fields that are not been set
	if (sharedNode.getContent() != null) {
	  realNode.setContent(sharedNode.getContent());
	}

	// Update attributes
	if (sharedNode.getAttributes().size() != 0) {
	  if (realNode.getAttributes().size() == 0) {
		for (AdlAttributesNode currentAttribute : sharedNode.getAttributes()) {
		  realNode.addAttribute(currentAttribute);
		}
	  } else {
		realNode.getAttributes().merge(sharedNode.getAttributes(), true);
	  }
	}

	// Update controller if not define in child
	if (sharedNode.getController() != null) {
	  realNode.setController(sharedNode.getController());
	}

	// Update exoevent
	if (sharedNode.getExoEvents().size() != 0) {
	  if (realNode.getExoEvents().size() == 0) {
		for (AdlExoEventNode currentEvent : sharedNode.getExoEvents()) {
		  realNode.addExoEvent(currentEvent);
		}
	  } else {
		realNode.getExoEvents().merge(sharedNode.getExoEvents(), true);
	  }
	}

	// Merge subcomponent
	for (AdlComponentNode sharedSubComponent : sharedNode.getSubComponents().values()) {
	  AdlComponentNode realSubComponent =
		realNode.getSubComponentNamed(sharedSubComponent.getName());
	  if (realSubComponent != null) {
		AdlUtil.mergeShared(realSubComponent, sharedSubComponent);
	  } else {
		realNode.addSubcomponent(sharedSubComponent);
	  }
	}

	// Merge bindings
	if (sharedNode.getBindings() != null) {
	  for (AdlBindingNode currentBinding : sharedNode.getBindings().values()) {
		realNode.addBinding(currentBinding);
	  }
	}
  }


  /**
   * Manage merging of shared component on a given tree. More
   * precisely apply modification imply by shared component definition
   * to real component.
   *
   * @param sharedComponentList
   *        The list of shared component to merge.
   */
  public static void mergeSharedComponent(
	final ArrayList<AdlComponentNode> sharedComponentList) {
	for (AdlComponentNode currentSharedComponent : sharedComponentList) {
	  AdlComponentNode currentComponent;
	  currentComponent = currentSharedComponent.getRealComponent();
	  AdlUtil.mergeShared(currentComponent, currentSharedComponent);
	  currentComponent.addToSharedComponentSet(currentSharedComponent);
	}
  }

}
