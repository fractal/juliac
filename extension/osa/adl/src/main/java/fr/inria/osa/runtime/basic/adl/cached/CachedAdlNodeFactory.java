/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl.cached;

import java.util.ArrayList;
import java.util.Stack;

import fr.inria.osa.runtime.basic.AbstractAdlNodeFactory;
import fr.inria.osa.runtime.basic.AdlNodeFactoryInterface;
import fr.inria.osa.runtime.basic.Pair;
import fr.inria.osa.runtime.basic.adl.AdlDumpableNodeInterface;
import fr.inria.osa.runtime.basic.adl.XMLAttributesList;
import fr.inria.osa.runtime.basic.adl.basic.AdlAttributesNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlBindingNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlComponentNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlExoEventNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlInterfaceNode;

/**
 * This factory correspond to cached ADL node.
 */
public class CachedAdlNodeFactory extends AbstractAdlNodeFactory implements
  AdlNodeFactoryInterface {

  /**
   * {@inheritDoc}
   */
  public AdlAttributesNode newAdlAttributesNode(String name, String value) {
	return new AdlAttributesNodeWithParameters(name, value, getString(name),
											   getString(value));
  }


  /**
   * {@inheritDoc}
   */
  public AdlBindingNode newAdlBindingNode(String client, String server) {
	return new AdlBindingNodeWithParameters(client, server, getString(client),
											getString(server));
  }


  /**
   * {@inheritDoc}
   */
  public AdlComponentNode newAdlComponentNode(String parameterizedName,
	boolean isRoot) {
	return new AdlComponentNodeWithParameters(parameterizedName,
											  getString(parameterizedName),
											  isRoot);
  }


  /**
   * {@inheritDoc}
   */
  public AdlExoEventNode newAdlExoEventNode(String name, String type,
	String time, String method) {
	return new AdlExoEventNodeWithParameters(name, type, time, method,
											 getString(name), getString(type),
											 Long.parseLong(getString(time)),
											 getString(method));
  }


  /**
   * {@inheritDoc}
   */
  public AdlInterfaceNode newAdlInterfaceNode(String name) {
	return new AdlInterfaceNodeWithParameters(name, getString(name));
  }


  /**
   * {@inheritDoc}
   */
  public void setAttributesSignature(AdlComponentNode component,
	String attributesSignature) {
	assert (component instanceof AdlComponentNodeWithParameters);
	((AdlComponentNodeWithParameters) component)
												.getInitialAttributesList()
												.setSignature(
												  attributesSignature,
												  getString(attributesSignature));
	component.getAttributes().setSignature(attributesSignature);
  }


  /**
   * {@inheritDoc}
   */
  public void setContent(AdlComponentNode component, String content) {
	((AdlComponentNodeWithParameters) component).setContent(content,
	  getString(content));
  }


  /**
   * {@inheritDoc}
   */
  public void setController(AdlComponentNode component, String controller) {
	((AdlComponentNodeWithParameters) component).setController(controller,
	  getString(controller));
  }


  /**
   * {@inheritDoc}
   */
  public void setExoEventsSignature(AdlComponentNode component,
	String exoEventsSignature) {
	assert (component instanceof AdlComponentNodeWithParameters);
	((AdlComponentNodeWithParameters) component)
												.getInitialExoEventsList()
												.setSignature(
												  exoEventsSignature,
												  getString(exoEventsSignature));
	component.getExoEvents().setSignature(exoEventsSignature);
  }


  /**
   * {@inheritDoc}
   */
  public void addParent(AdlComponentNode component, String extendsString,
	AdlComponentNode root) {

	((AdlComponentNodeWithParameters) component).addParent(extendsString,
	  getString(extendsString), root);
  }


  /**
   * {@inheritDoc}
   */
  @Override
  public void createInterface(Stack<AdlDumpableNodeInterface> stack,
	XMLAttributesList attributesList) {
	String interfaceName = attributesList.getAttributeValue("name");
	String interfaceRole = null;
	String interfaceSignature = null;
	String interfaceCardinality = null;
	String interfaceContingency = null;
	AdlInterfaceNode current;

	if (attributesList.contains("role")) {
	  interfaceRole = attributesList.getAttributeValue("role");
	}
	if (attributesList.contains("signature")) {
	  interfaceSignature = attributesList.getAttributeValue("signature");
	}
	if (attributesList.contains("cardinality")) {
	  interfaceCardinality = attributesList.getAttributeValue("cardinality");
	}
	if (attributesList.contains("contingency")) {
	  interfaceContingency = attributesList.getAttributeValue("contingency");
	}

	current = newAdlInterfaceNode(interfaceName);
	if (interfaceCardinality != null) {
	  ((AdlInterfaceNodeWithParameters) current).setCardinality(
		interfaceCardinality, getString(interfaceCardinality));
	}
	if (interfaceRole != null) {
	  ((AdlInterfaceNodeWithParameters) current).setRole(interfaceRole,
		getString(interfaceRole));
	}
	if (interfaceSignature != null) {
	  ((AdlInterfaceNodeWithParameters) current).setSignature(
		interfaceSignature, getString(interfaceSignature));
	}
	if (interfaceContingency != null) {
	  ((AdlInterfaceNodeWithParameters) current).setContingency(
		interfaceContingency, getString(interfaceContingency));
	}
	stack.push(current);
  }


  @Override
  public AdlComponentNode createComponentFromDefinition(
	Stack<AdlDumpableNodeInterface> stack, XMLAttributesList attributesList,
	ArrayList<Pair<String, String>> valuedParametersList) {
	AdlComponentNode result =
	  super.createComponentFromDefinition(stack, attributesList,
		valuedParametersList);
	if (attributesList.contains("arguments")) {
	  ArrayList<Pair<String, String>> argumentList =
		parseArgumentsList(attributesList.getAttributeValue("arguments"));
	  ((AdlComponentNodeWithParameters) result).setArgumentList(argumentList);
	}
	return result;
  }


  /**
   * {@inheritDoc}
   */
  public void addSubComponent(AdlComponentNode component,
	AdlComponentNode subComponent) {
	component.addSubcomponent(subComponent);
	((AdlComponentNodeWithParameters) component)
												.addSubComponentToInitialList(subComponent);
  }


  /**
   * {@inheritDoc}
   */
  public void addBinding(AdlComponentNode component, AdlBindingNode binding) {
	component.addBinding(binding);
	((AdlComponentNodeWithParameters) component)
												.addBindingToInitialList((AdlBindingNodeWithParameters) binding);

  }


  /**
   * {@inheritDoc}
   */
  public void addInterface(AdlComponentNode component,
	AdlInterfaceNode newInterface) {
	component.addInterface(newInterface);
	((AdlComponentNodeWithParameters) component)
												.addInterfaceToInitialList((AdlInterfaceNodeWithParameters) newInterface);

  }


  /**
   * {@inheritDoc}
   */
  public void addAttribute(AdlComponentNode component,
	AdlAttributesNode attribute) {
	component.addAttribute(attribute);
	((AdlComponentNodeWithParameters) component).addAttributeToInitialList(attribute);
  }


  /**
   * {@inheritDoc}
   */
  public void addExoEvent(AdlComponentNode component, AdlExoEventNode exoEvent) {
	component.addExoEvent(exoEvent);
	((AdlComponentNodeWithParameters) component).addExoEventToInitialList(exoEvent);
  }

}
