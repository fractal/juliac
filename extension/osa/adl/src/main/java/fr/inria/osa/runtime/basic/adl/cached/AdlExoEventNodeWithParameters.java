/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl.cached;

import fr.inria.osa.runtime.basic.adl.ParameterizedStringSolver;
import fr.inria.osa.runtime.basic.adl.basic.AdlExoEventNode;

public class AdlExoEventNodeWithParameters extends AdlExoEventNode {

  /** The parameterized name of this exoevent. */
  private String parameterizedName_;

  /** The parameterized type of this exoevent. */
  private String parameterizedType_;

  /** The parameterized time of this exoevent. */
  private String parameterizedTime_;

  /** The parameterized method of this exoevent. */
  private String parameterizedMethod_;


  /**
   * Initialize this parameterized exoevent.
   *
   * @param parameterizedName
   *        The parameterized name.
   * @param parameterizedType
   *        The parameterized type.
   * @param parameterizedTime
   *        The parameterized time.
   * @param parameterizedMethod
   *        The parameterized method.
   * @param name
   *        The name.
   * @param type
   *        The type.
   * @param time
   *        The time.
   * @param method
   *        The method.
   */
  protected AdlExoEventNodeWithParameters(String parameterizedName,
									 String parameterizedType,
									 String parameterizedTime,
									 String parameterizedMethod, String name,
									 String type, long time, String method) {
	super(name, type, time, method);
	parameterizedName_ = parameterizedName;
	parameterizedType_ = parameterizedType;
	parameterizedTime_ = parameterizedTime;
	parameterizedMethod_ = parameterizedMethod;
  }


  /**
   * Create a new instance of this parameterized exoevent.
   *
   * @param solver
   *        The parameterized string solver used to set parameters
   *        value.
   * @return The newly created exoevent.
   */
  AdlExoEventNode newInstance(ParameterizedStringSolver solver) {
	return new AdlExoEventNode(
							   solver.getString(parameterizedName_),
							   solver.getString(parameterizedType_),
							   Long.parseLong(solver.getString(parameterizedTime_)),
							   solver.getString(parameterizedMethod_));
  }
}
