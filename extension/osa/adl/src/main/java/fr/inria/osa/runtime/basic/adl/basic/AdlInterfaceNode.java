/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl.basic;

import java.io.PrintStream;

import fr.inria.osa.runtime.basic.adl.AdlDumpableNodeInterface;

/**
 * The ADL interface is the intermediate representation of component
 * interface used during merge process.
 */
public class AdlInterfaceNode implements AdlDumpableNodeInterface,Cloneable {

  /** Enumeration for coding interface role. */
  public enum InterfaceRole {
	/** Default value */
	defaultRole,
	/** Server role. */
	server,
	/** Client role. */
	client
  }

  /** Enumeration for coding interface contingency. */
  public enum InterfaceContingency {
	/** Default value */
	defaultContingency,
	/** Mandatory interface. */
	mandatory,
	/** Optional interface. */
	optional
  }

  /** Enumeration for coding interface cardinality. */
  public enum InterfaceCardinality {
	/** Default value */
	defaultCardinality,
	/** Singleton interface. */
	singleton,
	/** Collection interface. */
	collection
  }

  /** The name of this interface. */
  private String               name_;

  /** The type of the interface (client or server). */
  private InterfaceRole        role_        = InterfaceRole.defaultRole;

  /** The signature of this interface. */
  private String               signature_;

  /** The contingency of this interface. */
  private InterfaceContingency contingency_ =
											  InterfaceContingency.defaultContingency;

  /** The cardinality of this interface. */
  private InterfaceCardinality cardinality_ =
											  InterfaceCardinality.defaultCardinality;


  /**
   * Create a new interface definition.
   *
   * @param name
   *        The name of this interface.
   */
  public AdlInterfaceNode(String name) {
	name_ = name;
  }


  /**
   * Give the name of this interface.
   *
   * @return Give the name of this interface.
   */
  public String getName() {
	return name_;
  }


  /**
   * Give the signature of this interface.
   *
   * @return Give the signature of this interface.
   */
  public String getSignature() {
	return signature_;
  }


  /**
   * Give the role of this interface.
   *
   * @return The role of this interface.
   */
  public InterfaceRole getRole() {
	return role_;
  }


  /**
   * Give the cardinality of this interface.
   *
   * @return The cardinality of this interface.
   */
  public InterfaceCardinality getCardinality() {
	return cardinality_;
  }


  /**
   * Give the contingency of this interface.
   *
   * @return The contingency of this interface.
   */
  public InterfaceContingency getContingency() {
	return contingency_;
  }


  /**
   * Update this interface with a given one.
   *
   * @param update
   *        The interface updating this one.
   */
  public void merge(AdlInterfaceNode update) {
	if (role_.equals(InterfaceRole.defaultRole)) {
	  role_ = update.role_;
	}
	if (signature_ != null) {
	  signature_ = update.signature_;
	}
	if (contingency_.equals(InterfaceContingency.defaultContingency)) {
	  contingency_ = update.contingency_;
	}
	if (cardinality_.equals(InterfaceCardinality.defaultCardinality)) {
	  cardinality_ = update.cardinality_;
	}
  }


  /**
   * Set the role of this interface.
   *
   * @param role
   *        The role of this interface (server or client).
   */
  public void setRole(InterfaceRole role) {
	role_ = role;
  }


  /**
   * Set the signature of this interface.
   *
   * @param signature
   *        The signature of this interface.
   */
  public void setSignature(String signature) {
	assert (signature != null);
	signature_ = signature;
  }


  /**
   * Set contingency of this interface.
   *
   * @param contingency
   *        The contingency of this interface.
   */
  public void setContingency(InterfaceContingency contingency) {
	contingency_ = contingency;
  }


  /**
   * Set the cardinality of this interface.
   *
   * @param cardinality
   *        The cardinality of this interface.
   */
  public void setCardinality(InterfaceCardinality cardinality) {
	cardinality_ = cardinality;
  }


  /**
   * {@inheritDoc}
   */
  public void dump(PrintStream stream, boolean isRoot) {
	String role =
	  role_.equals(InterfaceRole.client)
		|| role_.equals(InterfaceRole.defaultRole) ? "client" : "server";
	String cardinality =
	  cardinality_.equals(InterfaceCardinality.singleton)
		|| cardinality_.equals(InterfaceCardinality.defaultCardinality)
																	   ? "singleton"
																	   : "collection";
	String contingency =
	  contingency_.equals(InterfaceContingency.mandatory)
		|| contingency_.equals(InterfaceContingency.defaultContingency)
																	   ? "mandatory"
																	   : "optional";
	stream
		  .printf(
			"<interface signature=\"%s\" cardinality=\"%s\"  role=\"%s\" name=\"%s\"  contingency=\"%s\"/>\n",
			signature_, cardinality, role, name_, contingency);
  }

  @Override
  public AdlInterfaceNode clone() {
	try {
	  return (AdlInterfaceNode) super.clone();
	} catch (CloneNotSupportedException e) {
	  // Can't happen
	}
	return null;
  }

}
