/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import fr.inria.osa.runtime.basic.adl.AdlDumpableNodeInterface;
import fr.inria.osa.runtime.basic.adl.XMLAttributesList;
import fr.inria.osa.runtime.basic.adl.basic.AdlBindingNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlComponentNode;
import fr.inria.osa.runtime.basic.adl.basic.AdlInterfaceNode;

/**
 * Internal SAX reader for loading ADL file as a AD-HOC memory
 * representation.
 */
public class InternalSaxReader extends DefaultHandler {

  /** The filename currently parsed. */
  private String                          filename_;

  /** Factory used for ADL node creation. */
  private AbstractAdlNodeFactory          factory_;

  /** Store last string read by parser */
  private String                          buffer_;

  /** Stack storing currently building node. */
  private Stack<AdlDumpableNodeInterface> stack_;

  /** The root component of this file */
  private AdlComponentNode                root_;

  /** List of shared component find in this file. */
  private ArrayList<AdlComponentNode>     sharedComponentList_;

  /**
   * The list of parameters for parsing the ADL file. The form of the
   * list is (parameterName,parameterValue).
   */
  private ArrayList<Pair<String, String>> valuedParametersList_;


  /**
   * Initialize this parser with a given valued parameter list.
   *
   * @param filename
   *        The filename currently parsed.
   * @param valuedParametersList
   *        The list of already valued parameter.
   * @param factory
   *        The factory that must be used for ADL node creation.
   */
  public InternalSaxReader(
						   String filename,
						   ArrayList<Pair<String, String>> valuedParametersList,
						   AbstractAdlNodeFactory factory) {
	filename_ = filename;
	stack_ = new Stack<>();
	sharedComponentList_ = new ArrayList<>();
	valuedParametersList_ = valuedParametersList;
	factory_ = factory;
  }


  /**
   * Give the name of file currently parsed.
   *
   * @return The name of file currently parsed.
   */
  public String getFileName() {
	return filename_;
  }


  /**
   * This methods is overridden to manage DOCTYPE tag.
   *
   * @throws SAXException
   *         This exception is not throw by this implementation.
   */
  @Override
  public InputSource resolveEntity(
	final String publicId, final String systemId)
	throws SAXException {
	if (systemId.startsWith("classpath://")) {
	  String content = "<?xml version='1.0' encoding='UTF-8'?>";
	  ByteArrayInputStream stream =
		new ByteArrayInputStream(content.getBytes());
	  return new InputSource(stream);
	}
	return null;

  }


  /**
   * {@inheritDoc}
   */
  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
	super.characters(ch, start, length);
	String tmp = new String(ch, start, length);
	if (buffer_ == null) {
	  buffer_ = new String();
	}
	buffer_ = buffer_.concat(tmp);
  }


  /**
   * {@inheritDoc}
   */
  @Override
  public void endElement(String uri, String localName, String qName)
	throws SAXException {
	super.endElement(uri, localName, qName);
	if (localName.compareToIgnoreCase("definition") == 0) {
	  root_ = (AdlComponentNode) stack_.pop();
	} else if (localName.compareToIgnoreCase("component") == 0) {
	  AdlComponentNode current = (AdlComponentNode) stack_.pop();
	  factory_.addSubComponent(((AdlComponentNode) stack_.peek()), current);
	} else if (localName.compareToIgnoreCase("interface") == 0) {
	  AdlInterfaceNode currentInterface = ((AdlInterfaceNode) stack_.pop());
	  factory_
			  .addInterface(((AdlComponentNode) stack_.peek()), currentInterface);
	} else if (localName.compareToIgnoreCase("binding") == 0) {
	  AdlBindingNode currentBinding = ((AdlBindingNode) stack_.pop());
	  factory_.addBinding(((AdlComponentNode) stack_.peek()), currentBinding);
	}
  }


  /**
   * {@inheritDoc}
   */
  @Override
  public void startElement(String uri, String localName, String name,
	Attributes attributes) throws SAXException {
	super.startElement(uri, localName, name, attributes);

	// Save attribute in list
	XMLAttributesList attributesList = new XMLAttributesList();
	for (int index = 0; index < attributes.getLength(); index++) {
	  attributesList.addAttributes(attributes.getLocalName(index),
		attributes.getValue(index));
	}
	if (localName.compareToIgnoreCase("definition") == 0) {
	  root_ =
		factory_.createComponentFromDefinition(stack_, attributesList,
		  valuedParametersList_);
	} else if (localName.compareToIgnoreCase("component") == 0) {
	  factory_.createComponentFromComponentNode(stack_, attributesList, root_);
	  if (((AdlComponentNode) stack_.peek()).isShared()) {
		sharedComponentList_.add((AdlComponentNode) stack_.peek());
	  }
	} else if (localName.compareToIgnoreCase("interface") == 0) {
	  factory_.createInterface(stack_, attributesList);
	} else if (localName.compareToIgnoreCase("binding") == 0) {
	  String client = attributesList.getAttributeValue("client");
	  String server = attributesList.getAttributeValue("server");
	  AdlBindingNode current = factory_.newAdlBindingNode(client, server);
	  stack_.push(current);
	} else if (localName.compareToIgnoreCase("controller") == 0) {
	  String controller = attributesList.getAttributeValue("desc");
	  factory_.setController(((AdlComponentNode) stack_.peek()), controller);
	} else if (localName.compareToIgnoreCase("content") == 0) {
	  String content = attributesList.getAttributeValue("class");
	  factory_.setContent(((AdlComponentNode) stack_.peek()), content);
	} else if (localName.compareToIgnoreCase("exoevents") == 0) {
	  if (attributesList.contains("signature")) {
		factory_.setExoEventsSignature(((AdlComponentNode) stack_.peek()),
		  attributesList.getAttributeValue("signature"));
	  }
	} else if (localName.compareToIgnoreCase("exoevent") == 0) {
	  String eventName = attributesList.getAttributeValue("name");
	  String eventType = attributesList.getAttributeValue("type");
	  String eventTime = attributesList.getAttributeValue("time");
	  String eventMethod = attributesList.getAttributeValue("method");

	  factory_.addExoEvent(((AdlComponentNode) stack_.peek()),
		factory_.newAdlExoEventNode(eventName, eventType, eventTime,
		  eventMethod));
	} else if (localName.compareToIgnoreCase("attributes") == 0) {
	  if (attributesList.contains("signature")) {
		factory_.setAttributesSignature(((AdlComponentNode) stack_.peek()),
		  attributesList.getAttributeValue("signature"));
	  }
	} else if (localName.compareToIgnoreCase("attribute") == 0) {
	  String attributeName = attributesList.getAttributeValue("name");
	  String attributeValue = attributesList.getAttributeValue("value");
	  factory_.addAttribute(((AdlComponentNode) stack_.peek()),
		factory_.newAdlAttributesNode(attributeName, attributeValue));
	} else {
	  // Default action
	}

  }


  /**
   * Give the root component of the file parse by this reader.
   *
   * @return The root component of the currently parsed file.
   */
  public AdlComponentNode getRoot() {
	return root_;
  }


  /**
   * Give the list of shared component find in this file.
   *
   * @return The list of shared component find in this file.
   */
  public ArrayList<AdlComponentNode> getSharedComponentNode() {
	return sharedComponentList_;
  }

}
