/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl.cached;

import fr.inria.osa.runtime.basic.adl.ParameterizedStringSolver;
import fr.inria.osa.runtime.basic.adl.basic.AdlAttributesList;

public class AdlAttributesListWithParameters extends AdlAttributesList {

  /** The parameterized signature. */
  private String parameterizedSignature_;


  /**
   * Set the signature of this parameterized attributes list.
   *
   * @param parameterizedSignature
   *        The parameterized signature.
   * @param signature
   *        The signature.
   */
  public void setSignature(String parameterizedSignature, String signature) {
	super.setSignature(signature);
	parameterizedSignature_ = parameterizedSignature;
  }


  /**
   * Give the signature of this parameterized attributes list
   * according to parameter solver.
   *
   * @param solver
   *        The parameters solver.
   * @return The signature of this attributes list.
   */
  String getSignature(ParameterizedStringSolver solver) {
	return solver.getString(parameterizedSignature_);
  }

  /**
   * Give the parameterized signature of this parameterized attributes list
   *
   * @return The parameterized signature of this attributes list.
   */
  String getParameterizedSignature() {
	return parameterizedSignature_;
  }

}
