/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl;

import java.util.Iterator;
import java.util.NoSuchElementException;

import fr.inria.osa.runtime.basic.Pair;

/**
 * This iterator is used to build individual parameter from string
 * representation of arguments list.
 */
public class ParameterIterator implements Iterator<Pair<String, String>> {

  /** The array of arguments. */
  private String[] parameters_;

  /**
   * The string used as allocation (between parameter name and
   * parameter value).
   */
  private String   allocationString_;

  /** The index of the next returned argument. */
  private int      nextArgumentIndex_ = 0;


  /**
   * Create the arguments iterator from string representation of
   * argument list.
   *
   * @param arguments
   *        The string representation of arguments list.
   * @param allocationString
   *        The string used to associate an parameter name with a
   *        value.
   */
  public ParameterIterator(final String arguments, final String allocationString) {
	parameters_ = arguments.split(",");
	allocationString_ = allocationString;
  }


  /**
   * {@inheritDoc}
   */
  @Override
  public final boolean hasNext() {
	return nextArgumentIndex_ < parameters_.length;
  }


  /**
   * {@inheritDoc}
   */
  @Override
  public final Pair<String, String> next() throws NoSuchElementException {
	String argument = parameters_[nextArgumentIndex_++];

	String[] argAndValue = argument.split(allocationString_);
	if (argAndValue.length == 1) {
	  if (allocationString_.equals("=>")) {
		return new Pair<>(null, argAndValue[0]);
	  }
	  return new Pair<>(argAndValue[0], null);
	}
	return new Pair<>(argAndValue[0], argAndValue[1]);
  }


  /**
   * {@inheritDoc}
   */
  @Override
  public final void remove() {
	throw new UnsupportedOperationException("No remove for this Iterator");
  }

}
