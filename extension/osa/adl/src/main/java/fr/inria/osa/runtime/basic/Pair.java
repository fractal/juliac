/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic;

/**
 * A simple pair implementation.
 *
 * @param <K>
 *        The key type.
 * @param <V>
 *        The value type.
 */
public class Pair<K, V> {
  /** The key of the pair */
  private K key_;

  /** The value of this pair */
  private V value_;


  /**
   * Initialize this pair with the given key and value.
   *
   * @param key
   *        The key of this pair.
   * @param value
   *        The value of this pair.
   */
  public Pair(K key, V value) {
	key_ = key;
	value_ = value;
  }


  /**
   * Give the key of this pair.
   *
   * @return The key of this pair.
   */
  public K getKey() {
	return key_;
  }


  /**
   * Give the value of this pair.
   *
   * @return The value of this pair.
   */
  public V getValue() {
	return value_;
  }


  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
	return "(" + key_ + "," + value_ + ")";
  }

}
