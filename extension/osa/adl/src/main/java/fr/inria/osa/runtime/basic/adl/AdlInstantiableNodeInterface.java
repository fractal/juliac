/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl;

import org.objectweb.fractal.api.Component;

import fr.inria.osa.runtime.basic.adl.basic.AdlComponentNode;

/**
 * Each ADL node that manage his instantiation must implement this interface.
 *
 */
public interface AdlInstantiableNodeInterface {

  /**
   * Instantiate this ADL node in a given fractal component.
   *
   * @param adlComponent
   *        The ADL component containing this ADL node.
   * @param fractalComponent
   *        The fractal component in which this ADL node must
   *        instantiate.
   * @throws Exception If errors occurs during Fractal instantiation.
   */
  void instantiate(AdlComponentNode adlComponent, Component fractalComponent) throws Exception;
}
