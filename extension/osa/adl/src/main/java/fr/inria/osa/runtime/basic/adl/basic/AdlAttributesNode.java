/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.adl.basic;

import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;

import fr.inria.osa.runtime.basic.adl.AdlDumpableNodeInterface;
import fr.inria.osa.runtime.basic.adl.AdlInstantiableNodeInterface;

/**
 * This class associate an attribute name with a value. The attributes
 * are stored and managed by a {@link AdlAttributesList}.
 */
public class AdlAttributesNode implements AdlDumpableNodeInterface,
  AdlInstantiableNodeInterface, Cloneable {

  /** The name of the attribute. */
  private String name_;

  /** The value of the attribute. */
  private String value_;


  /**
   * Initialize this attributes.
   *
   * @param name
   *        The name of this attribute.
   * @param value
   *        The value of this attribute.
   */
  public AdlAttributesNode(final String name, final String value) {
	name_ = name;
	value_ = value;
  }


  /**
   * Give the name of this attribute.
   *
   * @return The name of this attribute.
   */
  public final String getName() {
	return name_;
  }


  /**
   * Give the value of this attribute.
   *
   * @return The value of this attribute.
   */
  public final String getValue() {
	return value_;
  }


  /**
   * {@inheritDoc}
   */
  public final void dump(final PrintStream stream,
	 final boolean isRoot) {
	stream.printf("<attribute value=\"%s\" name=\"%s\"/>\n", getValue(),
	  getName());
  }


  /**
   * Java instantiation of this attribute in a given component.
   *
   * @param adlComponent
   *        The ADL component containing this attribute.
   * @param fractalComponent
   *        The Fractal component.
   * @throws IllegalAccessException
   *         If error occurs in attribute setting method invocation.
   * @throws InvocationTargetException
   *         If error occurs in attribute setting method invocation.
   * @throws NoSuchInterfaceException
   *         If the given component is not a primitive.
   */

  public final void instantiate(
	final AdlComponentNode adlComponent,
	final Component fractalComponent) throws IllegalAccessException,
	InvocationTargetException, NoSuchInterfaceException {
	Method[] methods =
	  fractalComponent.getFcInterface("/content").getClass().getMethods();
	Method setter = null;
	Type paramType = null;
	String searchMethodName = "set" + getName();
	for (int i = 0; i < methods.length; i++) {
	  if (methods[i].getName().equalsIgnoreCase(searchMethodName)) {
		setter = methods[i];
		Type[] parameter = methods[i].getGenericParameterTypes();
		assert (parameter.length == 1);
		paramType = parameter[0];
		break;
	  }
	}
	if (paramType == String.class) {
	  setter.invoke(fractalComponent.getFcInterface("/content"), getValue());
	} else if (paramType == Integer.TYPE) {
	  setter.invoke(fractalComponent.getFcInterface("/content"),
		Integer.parseInt(getValue()));
	} else if (paramType == Long.TYPE) {
	  setter.invoke(fractalComponent.getFcInterface("/content"),
		Long.parseLong(getValue()));
	} else if (paramType == Double.TYPE) {
	  setter.invoke(fractalComponent.getFcInterface("/content"),
		Double.parseDouble(getValue()));
	} else if (paramType == Boolean.TYPE) {
	  setter.invoke(fractalComponent.getFcInterface("/content"),
		Boolean.parseBoolean(getValue()));
	} else {
	  System.out.println("Warning parameter type " + paramType
						 + " is not supported");
	}
  }


  /**
   * {@inheritDoc}
   */
  @Override
  public final AdlAttributesNode clone() {
	return new AdlAttributesNode(name_, value_);
  }
}
