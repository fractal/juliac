/** ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.engines.basic.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Semaphore;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.util.Fractal;

import com.google.common.base.Supplier;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.SetMultimap;

import fr.inria.osa.logger.basic.SimulationLogger;
import fr.inria.osa.simapis.basic.exceptions.OsaRuntimeException;
import fr.inria.osa.simapis.basic.exceptions.SimSchedulingException;
import fr.inria.osa.simapis.basic.simulation.ExoEventsTypes;
import fr.inria.osa.simapis.basic.simulation.SharedSuperSchedulerItf;
import fr.inria.osa.simapis.basic.simulation.SimulationControllerAPI;

/**
 *
 * {@inheritDoc}
 *
 * The super-scheduler provides a global scheduling service to the
 * simulation components.
 *
 * Each simulation component manages its own event queue.
 *
 * Several options are available for implementing this distributed
 * coordination, but we can distinguish two main approaches:
 *
 * - one-way client-only calls with a local management thread:
 * Simulation component with at least one pending event allocate a dedicated
 * management thread that calls superscheduler methods. The management thread
 * can be blocked until either a simulation API call is issued from the
 * business code (executed by event processing threads) or until the
 * superscheduler decides so. The problem wth this approach is that this
 * synchronisation is complex especially when trying to stick with the one
 * way scheme (the scheduler cannot call the component): in this case
 * the local management thread MUST ONLY block while calling a supersched
 * method, such that the supersched can realease it when needed without
 * calling back a method of the sim component. Following, if the thread needs
 * to be woken up following an API call from an event thread, then this
 * API call must unlock the management thread, or do some of the work itself.
 * In the end, we may have synchronisations involving multiple threads, which
 * can rapidly become a nightmare. Most of the complexity of this solution
 * results from the need of a callback mechanism between the supersched
 * and the simulation component.
 *
 * - two-ways calls (with a supersched API and supersched callback API).
 * In this case, the superscheduler can use the callback API to wake
 * up threads sleeping in the simulation component. No additional thread is
 * needed for local management, the superscheduler thread can execute
 * the management code itself, and even the synchronous business code
 * if needed. This second option is the one chosen in this version of the
 * scheduler. However, the two-way scheme requires double-binding between
 * the superscheduler and simulation component.
 *
 *
 * Here are time-sequences for various scenarios:
 *
 * 1/ initial invariant state: superscheduler thread ready for a new iteration,
 * all other simulation threads are sleeping (eg. blocked on a semaphore). All
 * simulation components with at least one pending event are assumed to
 * have called wakeMeUp with parameters corresponding to first pending event.
 *
 * 2/ superscheduler.mainloop() looks for first entry in the global pending event set.
 *
 * 3a/ case ASYNC: superscheduler CALLS simc continue(current_time, sync=f)
 *
 * 4/ (SC Thread) simC dequeues next pending event and advances local time
 *
 * 5/ (SC Thread) simC looks up queue head and calls wakeMeUp with next entry
 *
 * 6async/ (SC Thread) simC starts a new thread and invokes the dequeued event
 * execution
 *
 * 7a/ (SC Thread) continue returns
 *
 * 7b/ (simC Thread) The threads executes as long as possible for the
 * current date until it either (8a) terminates or it (8b) reaches a
 * simulation wait instruction
 *
 * 8a/ (simC Thread) the running thread calls the
 * eventComplete(current time, sync=f)
 * which wakes up the SC Thread while simC Thread releases itself.
 *
 * 8b/ (simC Thread, possibly in another simC) creates a wake up event
 * for resuming execution later when the wait delay has expired. If the
 * time of resuming is earlier than previous head of queue in current simC,
 * simC thread calls wakeMeUp to update status. Finally, the simC thread calls
 * eventStopped(current time) and goes to sleep until the wake up event
 * is called.
 *
 * 8-async/ (SC Thread) superscheduler.main() goes to sleep until either
 * eventComplete() or eventStopped() is called.
 *
 * 9/ (SC Thread) eventComplete/eventStopped update the current
 * simulation time and supersched.main() returns to 1/
 *
 * 3b/ case SYNC: superscheduler calls simc continue(current_time, sync=t)
 *
 * 4/ - 5/ idem
 *
 * 6-sync/ (SC thread) invokes the dequeued event
 *
 * 7-sync/ (SC thread) executes business code until
 * eventComplete(current time, sync=t) is called.
 * NB: the sync flag can be automatically assigned by assessing if
 * current thread == SC Thread. (eventStopped not allowed in sync mode)
 *
 * 8-sync/ jump to 9/
 *
 * @author odalle
 */
@org.objectweb.fractal.fraclet.annotations.Component
public class SharedSuperScheduler implements SharedSuperSchedulerItf {

	/** The current simulation time. */
	private long currentSimulationTime_;

	private static final SimulationLogger logger_ = new SimulationLogger(SharedSuperScheduler.class);


	/**
	 * Each simulation component with at least one event pending is placed in this queue
	 * once for its earliest pending event time.
	 * We could use Guava's SortedSetMultimap with the TreeMultimap implementation which would
	 * make the following one-liner much simpler but we don't need values to be sorted so
	 * we end up building our own custom multimap with keys backed up by a TreeMap and
	 * values backed up by a simple List (instead of a SortedSet for TreeMultimap).
	 * Hence this quite awful one-liner instruction.
	 * We don't use the synchronized version because the generics parsing results in
	 * a Spoon compile error.
	 * Since Each controller can only appear exactly once, we could try to use Guava's
	 * BiMap, which also saves the time to remove previous insertions, but unfortunately
	 * BiMaps are not sorted, unless created as immutable and backedup by a treeMap, which
	 * is not what we want.
	 */
	private final Multimap<Long, SimulationControllerAPI> timeWaitQueue_ =
			Multimaps.newMultimap(
					new TreeMap<>(),
					new Supplier<>() {
						@Override
						public List<SimulationControllerAPI> get() {
							return Lists.newArrayList();
						}
					});


	/**
	 * Another multimap to keep track of which simulation components have threads blocked on a
	 * given condition. This one uses Hash maps to back up both keys and values, so we can
	 * use the predefined HashMultimap.
	 */
	private final SetMultimap<String,SimulationControllerAPI> conditionWaitQueue_ =
			HashMultimap.<String,SimulationControllerAPI>create();


	/**
	 * Set of components to which this super-scheduler is attached
	 */
	private final Set<Component> boundComponents_ = new HashSet<>();

	/**
	 * External reference to this component so we can self introspect.
	 * Caveat: a strict implementation of the component model may prohibit
	 * passing the external reference of this object.
	 */
	private Component self_;

	/**
	 * This semaphore is used to control the super-scheduler thread execution
	 * while simulation threads are running.
	 */
	private final Semaphore runSema_ = new Semaphore(0,false);

	/**
	 * Initialize the super scheduler.
	 */
	public SharedSuperScheduler() {
		currentSimulationTime_ = 0;
		logger_.setTimeApi(this);
	}

	/**
	 * Call init method on each simulation component.
	 */
	private final void startSimulationComponents() {
		logger_.debug("SS starting simulation. (bound to {})", this.boundComponents_.size());

		for (final Component comp: this.boundComponents_) {
			SimulationControllerAPI simC=null;
			try {
				simC = (SimulationControllerAPI)comp.getFcInterface("simulation-controller");
				simC.init();
			} catch (NoSuchInterfaceException e) {
				logger_.debug("SuperSched.run(): failed to retrieve simulation controller from bound cmponent! ({})", comp);
				e.printStackTrace();
			}
		}
	}

	/**
	 * Call quit method on each simulation component.
	 */
	private final void stopSimulationComponents() {
		logger_.debug("SS stopping simulation. (bound to {})", this.boundComponents_.size());

		for (final Component comp: this.boundComponents_) {
			SimulationControllerAPI simC=null;
			try {
				simC = (SimulationControllerAPI)comp.getFcInterface("simulation-controller");
				simC.quit();
			} catch (NoSuchInterfaceException e) {
				logger_.warn("SuperSched.run(): failed to retrieve simulation controller from bound cmponent! ({})", comp,e);
			}
		}
	}

	/**
	 * End simulation after stopping each simulation component.
	 * Warning: no guarantee that the simulation end in a consistent state: if any
	 * events are still pending for the current simulation time thay will be lost.
	 */
	private final void endSimulation() {
		logger_.debug("SS: Ending simulation a t={}", currentSimulationTime_);
		stopSimulationComponents();
	}

	public final void runSimulation() {

		logger_.debug("Super scheduler starting execution...");
		startSimulationComponents();

		while (true) {

			if (timeWaitQueue_.isEmpty()) {
				endSimulation();
				break;
			}

			// Iterates on keyset according to the natural order of Long
			// because the timeWaitQueue multimap is backed up by a TreeMap
			long nextTime = timeWaitQueue_.keySet().iterator().next();
			if (nextTime != currentSimulationTime_) {
				logger_.debug("SS: advancing time to {}", nextTime);
				currentSimulationTime_ = nextTime;
			}

			for ( SimulationControllerAPI sc : timeWaitQueue_.asMap().remove(nextTime)) {
				try {
					sc.resumeNext(currentSimulationTime_);
				} catch (SimSchedulingException e) {
					e.printStackTrace();
					throw new RuntimeException("SS: Resume event failed:",e);
				}
			}

		} // while true
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public final void run() {
		runSimulation();
	}

	@Override
	public void startSimulation() {
		// Runs in the same thread as the ADL parser
		run();
	}

	/**
	 * Give the current simulation time.
	 *
	 * @return The current simulation time.
	 */
	@Override
	public final long getSimulationTime() {
		return currentSimulationTime_;
	}

	/**
	 * Add a given simulation controller to the set of manager simulation
	 * controller.
	 *
	 * @param simulationController
	 *            The simulation controller that must be managed.
	 */
	@Override
	public final synchronized void register(
			final SimulationControllerAPI simulationController) {
		logger_.debug("SS: register called from {}",simulationController);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.simulation.SuperSchedulerItf#wakeMeUp(fr.inria.osa.simapis.basic.simulation.SimulationControllerAPI, long, fr.inria.osa.simapis.basic.simulation.ExoEventsTypes, long, long)
	 */
	@Override
	public final synchronized void wakeMeUp(
			final SimulationControllerAPI simulationController,
			final long wakeUpTime, final ExoEventsTypes type,
			final long eventId, final long previousTime) {

		logger_.debug("SS: wake me up called with time={}",wakeUpTime);

		if (previousTime > 0)
			timeWaitQueue_.remove(previousTime, simulationController);
		timeWaitQueue_.put(wakeUpTime, simulationController);

	}


	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.simulation.SuperSchedulerItf#cancelWaitCond(fr.inria.osa.simapis.basic.simulation.SimulationControllerAPI, java.lang.String)
	 */
	@Override
	public synchronized void cancelWaitCond(
			final SimulationControllerAPI simulationController,
			final String condition) {
		assert (conditionWaitQueue_.containsKey(condition));
		//assert (reverseCondMap_.containsKey(simulationController));

		// FIXME : We do the exact same thing on both maps. There must be a smarter/faster way.
		// I must implement a 2-way hash-map
		// TODO: Add reversmap update?

		this.conditionWaitQueue_.remove(condition, simulationController);

		//Set<String> conditions = reverseCondMap_.get(simulationController);
		//conditions.remove(condition);
		//if (conditions.isEmpty())
		//    reverseCondMap_.remove(simulationController);
		//else
		//    reverseCondMap_.put(simulationController, conditions);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.simulation.SuperSchedulerItf#wakeMeUpCond(fr.inria.osa.simapis.basic.simulation.SimulationControllerAPI, java.lang.String)
	 */
	@Override
	public synchronized void wakeMeUpCond(final SimulationControllerAPI simulationController, final String condition) {
		// A simulation component notifies that it has a thread blocked on condition

		conditionWaitQueue_.put(condition, simulationController);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.simulation.SuperSchedSyncItf#eventComplete()
	 */
	@Override
	public void eventComplete() {
		runSema_.release();
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.simulation.SuperSchedSyncItf#eventStopped()
	 */
	@Override
	public void eventStopped() {
		runSema_.release();
	}


	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.simulation.SuperSchedSyncItf#eventStartedAsync()
	 */
	@Override
	public void eventStartedAsync() {
		try {
			runSema_.acquire();
		} catch (InterruptedException e) {
			throw new RuntimeException("Super sched wait interrupted!",e);
		}
	}


	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.engines.basic.impl.SharedSuperSchedulerItf#deployAndBind(org.objectweb.fractal.api.Component)
	 */
	@Override
	public void deployAndBind(Component superSched, Component rootComponent)
	throws OsaRuntimeException {

		if ((self_ == null) && (superSched == null))
			throw new OsaRuntimeException("Superscheduler deploy: missing super scheduler component");

		if ((superSched != null) && (self_ == null)) {
			// We need a super sched reference and we have a candidate

			try {
				// This is just to check if we find a SS itf
				@SuppressWarnings("unused")
				Object dummy = superSched.getFcInterface("superschedulersvc");
			} catch (NoSuchInterfaceException e) {
				// Not a simulation super scheduler
				throw new OsaRuntimeException(
						"Superscheduler deploy: " +
						"superSched parameter is not a super scheduler ("+superSched+").",e);
			}
			logger_.debug("Deploy and bind: setting SS ref to this component: {}",superSched.getFcType());
			self_ = superSched;
		}

		if (rootComponent == null)
			throw new OsaRuntimeException("Superscheduler deploy: missing root component");

		// Lookup all components in root
		Component[] subComponents = null;
		try {
			subComponents = Fractal.getContentController(rootComponent).getFcSubComponents();
		} catch (NoSuchInterfaceException e) {
			// No content controller means this root is a primitive: end of recursion
			logger_.debug("This component (%s) has no sub component, recursion done.",rootComponent,e);
			return;
		}

		// bind to all simulation components if not yet in the list of bound components
		for (Component comp: subComponents) {
			SimulationControllerAPI simC = null;
			logger_.debug("SubComponent: {}",comp.getFcType());

			try {
				simC = (SimulationControllerAPI)comp.getFcInterface("simulation-controller");
			} catch (NoSuchInterfaceException e) {
				logger_.debug("No simulation controller, ignoring this component: {}",comp.getFcType());
				// A component with no simulation API. Probably a composite but
				// this is a weak assumption, best to ignore for now.
			}

			// We found a simulation component. Ignore if it is already bound.
			if (boundComponents_.contains(comp)) continue;

			logger_.debug("This simulation component is not bound yet: {}",simC);

			// Bind to this component
			if (simC != null) {
				try {
					Fractal.getBindingController(comp).bindFc("superscheduler", self_.getFcInterface("superschedulersvc"));
					boundComponents_.add(comp);
					logger_.debug("Bound SS to this component: {}", simC);
				} catch (NoSuchInterfaceException e) {
					throw new OsaRuntimeException("Superscheduler deploy: Can't find BC on super scheduler!",e);
				} catch (IllegalBindingException e) {
					throw new OsaRuntimeException("Superscheduler deploy: Weird, can't bind to this sim component!",e);
				} catch (IllegalLifeCycleException e) {
					throw new OsaRuntimeException("Superscheduler deploy: binding to this sim component blocked.",e);
				}
			}

			Component[] subSubComponents;
			// Check if this is a composite and if so, if it already contains a SS
			try {
				subSubComponents = Fractal.getContentController(comp).getFcSubComponents();
			} catch (NoSuchInterfaceException e) {
				// Not a composite, ignore.
				continue;
			}

			// Is one of the sub-sub-components a SS?
			boolean found = false;
			for (Component subComp: subSubComponents) {
				if (subComp.equals(self_)) {
					found = true;
					break;
				}
			}

			// already contains a SS (happens with shared component)
			if (found) continue;

			logger_.debug("Recursion: adding SS to this component: {}",comp);

			try {
				Fractal.getContentController(comp).addFcSubComponent(self_);
			} catch (IllegalContentException e) {
				throw new OsaRuntimeException("Superscheduler deploy: failed to get sub-component content",e);
			} catch (IllegalLifeCycleException e) {
				throw new OsaRuntimeException("Superscheduler deploy: failed to insert SS recusively",e);
			} catch (NoSuchInterfaceException e) {
				throw new OsaRuntimeException("Superscheduler deploy: failed to get CC interface",e);
			}

			// Last but not least: iterate recursively
			deployAndBind(null, comp);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.SuperSchedSyncItf#releaseOneOnCondition(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean releaseOneOnCondition(final String condition, final String result) {
		Set<SimulationControllerAPI> set = conditionWaitQueue_.get(condition);
		if (set.isEmpty()) return false;

		SimulationControllerAPI simC = set.iterator().next();
		try {
			return simC.resumeOneBlocked(condition, result);
		} catch (SimSchedulingException e) {
			logger_.error("Lost sleeping thread on condition {}!",condition);
			throw new RuntimeException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.SuperSchedSyncItf#releaseAllOnCondition(java.lang.String, java.lang.String)
	 */
	@Override
	public int releaseAllOnCondition(final String condition, final String result) {
		Set<SimulationControllerAPI> set = conditionWaitQueue_.get(condition);
		if (set.isEmpty()) return 0;

		int count =0;
		for (SimulationControllerAPI simC: set) {
			try {
				count += simC.resumeAllBlocked(condition, result);
			} catch (SimSchedulingException e) {
				logger_.error("Lost sleeping thread on condition {}!",condition);
				throw new RuntimeException(e);
			}
		}

		return count;
	}

}
