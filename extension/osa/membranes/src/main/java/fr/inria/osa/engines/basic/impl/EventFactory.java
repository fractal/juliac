/** ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.engines.basic.impl;

import org.apache.commons.pool.BasePoolableObjectFactory;

public class EventFactory extends BasePoolableObjectFactory {
  // for makeObject we'll simply return a new buffer
  public Object makeObject() {
	return new Event();
  }


  // when an object is returned to the pool,
  // we'll clear it out
  public void passivateObject(Object obj) {
	//Event event = (Event) obj;

  }

  // for all other methods, the no-op
  // implementation in BasePoolableObjectFactory
  // will suffice
}
