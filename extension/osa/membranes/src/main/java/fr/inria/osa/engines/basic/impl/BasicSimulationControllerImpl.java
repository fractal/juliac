package fr.inria.osa.engines.basic.impl;
/** ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/


import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.PriorityQueue;

import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.util.Fractal;


import fr.inria.osa.simapis.basic.simulation.ExoEventsTypes;
import fr.inria.osa.simapis.basic.simulation.SimulationControllerAPI;
import fr.inria.osa.simapis.basic.EventModelingAPI;
import fr.inria.osa.simapis.basic.simulation.SimulationInternalSchedulingItf;
import fr.inria.osa.simapis.basic.simulation.SuperSchedulerItf;
import fr.inria.osa.simapis.basic.exceptions.IllegalEventMethodException;
import fr.inria.osa.simapis.basic.exceptions.OsaAPIException;
import fr.inria.osa.simapis.basic.exceptions.SimSchedulingException;
import fr.inria.osa.simapis.basic.exceptions.UnknownEventMethodException;
import fr.inria.osa.logger.basic.SimulationLogger;


/**
 * Implements the basic (event-driven) simulation controller interface back-end.
 *
 * <p> This is the simulation component side of the simulation engine.
 * @see SharedSuperScheduler the super-scheduler
 *
 * @author jribault
 * @author odalle
 */
@Component
@Membrane(controller = "mPrimitive")
public class BasicSimulationControllerImpl implements
	SimulationControllerAPI, SimulationInternalSchedulingItf,
	EventModelingAPI, Controller {

	private static final SimulationLogger LOGGER = new SimulationLogger(BasicSimulationControllerImpl.class);

	/**
	 * The initial capacity of the priority queue used by simulation controller
	 * to store events.
	 */
	protected static final int INITIAL_PRIORITY_QUEUE_CAPACITY = 11;


	//private boolean init_ = false;

	/** Event priority queue based on event currentTime_. */
	protected final PriorityQueue<Event> eventPriorityQueue_;

	/** A sequence number is assigned to each event so they can be canceled */
	protected long eventSequenceId_ = 0;

	/** Fractal context used to access component content. */
	protected Object content_;

	private long lastRequestedWakeUpTime_ = -1L;

	protected ObjectPool pool_ = new GenericObjectPool(new EventFactory(), -1,
			GenericObjectPool.WHEN_EXHAUSTED_GROW, -1, -1, -1, false, false,
			-1, 3, 5000, false, -1, true);

	@Requires(name = "component")
	org.objectweb.fractal.api.Component component;

	/**
	 * Shortcut reference to the super-scheduler interface.
	 * Initialized by the {@link #init} method.
	 */
	protected SuperSchedulerItf superSchedInterface_;

	/**
	 * Shortcut reference to the local simulation-controller interface.
	 * Initialized by the {@link #init} method. This reference is
	 * used as a callback in requests to the super-scheduler.
	 * FIXME: find a cleaner way, eg. using a generic component id.
	 */
	protected SimulationControllerAPI localSchedInterface_;


	/**
	 * Initialize this simulation controller.
	 */
	public BasicSimulationControllerImpl() {

		eventPriorityQueue_ = new PriorityQueue<>(
				BasicSimulationControllerImpl.INITIAL_PRIORITY_QUEUE_CAPACITY,
				new Comparator<>() {
					public int compare(
							final Event firstEvent,
							final Event secondEvent) {
						return Long.signum(firstEvent.getTime() - secondEvent.getTime());
					}
				});

	}

	public SimulationLogger getLogger() {
		return LOGGER;
	}

	/**
	 * Schedules an event to execute a method of a given class at a given time.
	 *
	 * @param instance
	 *    a reference to the object that owns the method to be executed.
	 *    For business methods, this parameter should be set to the component's
	 *    content, and for control methods, it should be set to the corresponding controller.
	 * @param methodName
	 *    name of the method to be executed
	 * @param parameters
	 *    an array of parameters to given to the method to be executed
	 * @param time
	 *    absolute simulation time at which to execute the event
	 * @param type
	 *    Type of event (see {@link ExoEventsTypes})
	 *
	 * @return event Id
	 */
	protected long scheduleMethod(Object instance, final String methodName,
			final Object[] parameters, long time, ExoEventsTypes type) {
		long eventId=0;

		getLogger().debug("scheduleMethod 1/2: Queueing event [{},{}]",methodName,time);

		Event prevHead = this.eventPriorityQueue_.peek();

		try {
			Event event = (Event) (pool_.borrowObject());
			event.setInstance(instance);
			event.setMethod(methodName);
			event.setMethodParameters(parameters);
			event.setTime(time);

			event.setEventType(type);
			this.eventPriorityQueue_.add(event);
			eventId=this.eventSequenceId_++;
			event.setEventId(eventId);
			if ((superSchedInterface_ != null) && ((prevHead == null) || (time < prevHead.getTime()))) {
				superSchedInterface_.wakeMeUp(localSchedInterface_, time, type, eventId,this.lastRequestedWakeUpTime_);
				this.lastRequestedWakeUpTime_ = time;
			}
		} catch (final SecurityException e) {
			e.printStackTrace();
		} catch (final NoSuchMethodException e) {
			e.printStackTrace();
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		getLogger().debug("scheduleMethod 2/2: Queued event with id={}",eventId);
		return eventId;
	}


	/*
	 * (non-Javadoc)
	 * @see org.objectweb.fractal.julia.Controller#initFcController(org.objectweb.fractal.julia.InitializationContext)
	 */
	public void initFcController(final InitializationContext arg0) {

		if (arg0.content instanceof Object[]) {
			content_ = ((Object[]) arg0.content)[2];
			//content_ = ((Object[]) arg0.content)[1];
		} else {
			content_ = arg0.content;
		}

		getLogger().debug("initFcController called ({})...",this.content_);
	}



	/**
	 * Implementation of the fio.simapis.basic.simulation.SimulationInternalSchedulingItf
	 */

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.simulation.SimulationInternalSchedulingItf#scheduleMyself(java.lang.String, java.lang.Object[], long, fr.inria.osa.simapis.basic.simulation.ExoEventsTypes)
	 */
	public long scheduleMyself(String methodName, Object[] parameters,
			long time, ExoEventsTypes type) throws OsaAPIException {

		if (! type.equals(ExoEventsTypes.SYNC_ACTION))
			throw new OsaAPIException("Trying to schedule an Illegal event type for this component");

		return scheduleMethod(content_, methodName, parameters, time, type);
	}


	/**
	 * Notify super-scheduler of the next coming event for this simulation component.
	 *
	 */
	private void scheduleNextPendingEvent() {
		final Event next = eventPriorityQueue_.peek();

		if (next != null) {
			superSchedInterface_.wakeMeUp(localSchedInterface_,
					next.getTime(), next.getEventType(), next.getEventId(), this.lastRequestedWakeUpTime_);
			this.lastRequestedWakeUpTime_ = next.getTime();
		} else {
			getLogger().debug("No pending event for {}.",content_);
		}
	}


	/**
	 * Implementation of the fio.simapis.basic.simulation.SimulationControllerAPI
	 */

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.simulation.SimulationControllerAPI#init()
	 */
	public void init() {
		//System.err.println("Entering init:"+this.content_);
		try {
			superSchedInterface_ = (SuperSchedulerItf) component
					.getFcInterface("superscheduler");
			localSchedInterface_ = (SimulationControllerAPI) component
					.getFcInterface("simulation-controller");
			getLogger().setTimeApi(superSchedInterface_);
		} catch (NoSuchInterfaceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		//init_ = true;

		//superSchedInterface_.register(localSchedInterface_);
		scheduleNextPendingEvent();
	}


	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.simulation.SimulationControllerAPI#resumeNext(long)
	 */
	public boolean resumeNext(long currentTime) throws SimSchedulingException {
		// This method is called by the super-scheduler thread to notify the
		// simulation controller of a simulation component that its next pending
		// event can be executed.

		final Event first = eventPriorityQueue_.poll();

		getLogger().debug("resumeNext({})", currentTime);

		if (first == null) {
			getLogger().error("resumeNext: called but queue is empty!");
			return false;
		}

		// This informs the super-scheduler of the time of the next pending
		// event after
		// processing the first found on the queue. We could call this only when
		// the time is
		// expected to change, but calling possibly more than necessary is safe.
		scheduleNextPendingEvent();

		if (first.getTime() != currentTime)
			throw new SimSchedulingException();

		getLogger().debug("Sync action (non bloquing)...");
		try {
			first.invoke();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.simulation.SimulationControllerAPI#resumeOneBlocked(java.lang.String, java.lang.String)
	 */
	public synchronized boolean resumeOneBlocked(final  String condition, final  String result)
	throws SimSchedulingException {
		getLogger().debug("SS called resumeOneBlocked(cond={},res={}). Unsupported by action, ignored.",condition,result);
		return false;
	}


	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.simulation.SimulationControllerAPI#resumeAllBlocked(java.lang.String, java.lang.String)
	 */
	public synchronized int resumeAllBlocked(final  String condition, final  String result)
	throws SimSchedulingException {
		// Unsupported
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.simulation.SimulationControllerAPI#quit()
	 */
	public void quit() {
		// Not much cleaning to do
	}

	/**
	 * Implementation of the fio.simapis.basic.EventModelingAPI
	 */

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.EventModelingAPI#scheduleEventMyself(java.lang.String, java.lang.Object[], long)
	 */
	public long scheduleEventMyself(String methodName, Object[] parameters,
			long time) throws UnknownEventMethodException,
			IllegalEventMethodException {
		try {
			return scheduleMyself(methodName,parameters,time,ExoEventsTypes.SYNC_ACTION);
		} catch (OsaAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.EventModelingAPI#cancelEvent(long)
	 */
	public boolean cancelEvent(long eventId) {

		getLogger().debug("running cancelEvent(id={}",eventId);
		Iterator<Event> iter = this.eventPriorityQueue_.iterator();

		while (iter.hasNext()) {
			Event e = iter.next();
			if (e.getEventId() == eventId) {
				iter.remove();
				return true;
			}
		}
		return false;
	}

	/**
	 * Implementation of the fio.simapis.basic.SimulationTimeAPI
	 */

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.SimulationTimeAPI#getSimulationTime()
	 */
	public long getSimulationTime() {
		//return localTime_;
		return superSchedInterface_.getSimulationTime();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		NameController nc;
		try {
			nc = Fractal.getNameController((org.objectweb.fractal.api.Component) this);
		} catch (NoSuchInterfaceException e) {
			return "<No name>";
		}

		return nc.getFcName();
	}


}
