package fr.inria.osa.engines.basic.impl;
/** ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/


import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.util.Fractal;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import fr.inria.osa.logger.basic.SimulationLogger;
import fr.inria.osa.simapis.basic.EventModelingAPI;
import fr.inria.osa.simapis.basic.ProcessModelingAPI;
import fr.inria.osa.simapis.basic.exceptions.IllegalEventMethodException;
import fr.inria.osa.simapis.basic.exceptions.OsaAPIException;
import fr.inria.osa.simapis.basic.exceptions.SimSchedulingException;
import fr.inria.osa.simapis.basic.exceptions.UnknownEventMethodException;
import fr.inria.osa.simapis.basic.simulation.ExoEventsTypes;
import fr.inria.osa.simapis.basic.simulation.SimulationControllerAPI;
import fr.inria.osa.simapis.basic.simulation.SimulationInternalSchedulingItf;
import fr.inria.osa.simapis.basic.simulation.SimulationWaitRequestItf.WAIT_REQ_TYPE;
import fr.inria.osa.simapis.basic.simulation.WaitRequest;


/**
 * Implements the simulation controller interface back-end.
 *
 * <p> This is the simulation component side of the simulation engine.
 * @see SharedSuperScheduler the super-scheduler
 *
 * @author jribault
 * @author odalle
 */
@Component
@Membrane(controller = "mPrimitive")
public class ProcessSimulationControllerImpl extends BasicSimulationControllerImpl implements
	SimulationControllerAPI, SimulationInternalSchedulingItf,
	ProcessModelingAPI, EventModelingAPI, Controller {

	private static final SimulationLogger LOGGER = new SimulationLogger(ProcessSimulationControllerImpl.class);

	/** WaitRequest used to block threads, indexed by event id **/
	private final Map<Long,WaitRequest> blockedQueue_ = new HashMap<>();

	private long lastRequestedWakeUpTime_ = -1L;


	/**
	 * FIXME : Missing JavaDoc.
	 */
	private ExecutorService tpes_ = Executors.newCachedThreadPool();

	/*
	 * This boolean flag is used to remember if the local semaphore is available or released.
	 * (Turns to false when it released, true otherwise).
	 * FIXME: This seems to be some kind of unsafe synchronization around the
	 * safe synchronization provided by the semaphore. A sgfer solution has to
	 * be found to replace this.
	 */
	boolean state = true;

	/**
	 * Keep track of pending wait requests indexed by condition.
	 * Since multiple threads may be waiting on the same condition, the requests are saved
	 * in a FIFO.
	 **/
	private Multimap<String,WaitRequest> pendingRequests_ = HashMultimap.<String,WaitRequest>create();

	/**
	 * Initialize this simulation controller.
	 */
	public ProcessSimulationControllerImpl() {
		super();
	}

	@Override
	public SimulationLogger getLogger() {
		return LOGGER;
	}

	/**
	 * Transfers execution control from super-scheduler thread to simulation thread
	 * @param wr
	 */
	public void releaseAndWait(WaitRequest wr) {
		wr.release();
		superSchedInterface_.eventStartedAsync();
	}

	/**
	 * Schedules the execution of the <code>methodName</code>
	 * method of this controller with given parameters at a given time.
	 *
	 * <p> Calling this method results in queueing a new
	 * {@link fr.inria.osa.simapis.basic#ExoEventsType.CONTROL_ACTION}
	 * event containing a callback to the {@link #releaseAndWait(WaitRequest)} method
	 *
	 * @param methodName
	 *            The method name.
	 * @param parameters
	 *            The method's parameters.
	 * @param time
	 *            The execution time.
	 * @param type
	 *            The event type.
	 *
	 * @return event Id
	 */
	private final long scheduleControl(final String methodName,
			final Object[] parameters, long time) {
		return scheduleMethod(this, methodName, parameters, time, ExoEventsTypes.CONTROL_ACTION);
	}

	/**
	 * Implementation of the fio.simapis.basic.simulation.SimulationInternalSchedulingItf
	 */

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.simulation.SimulationInternalSchedulingItf#scheduleMyself(java.lang.String, java.lang.Object[], long, fr.inria.osa.simapis.basic.simulation.ExoEventsTypes)
	 */
	@Override
	public long scheduleMyself(String methodName, Object[] parameters,
			long time, ExoEventsTypes type) throws OsaAPIException {
		return scheduleMethod(content_, methodName, parameters, time, type);
	}

	/**
	 * Notify super-scheduler of the next coming event for this simulation component.
	 *
	 */
	private void scheduleNextPendingEvent() {
		final Event next = eventPriorityQueue_.peek();

		if (next != null) {
			superSchedInterface_.wakeMeUp(localSchedInterface_,
					next.getTime(), next.getEventType(), next.getEventId(), this.lastRequestedWakeUpTime_);
			this.lastRequestedWakeUpTime_ = next.getTime();
		} else {
			LOGGER.debug("No pending event for {}.",content_);
		}
	}

	/**
	 * Implementation of the fio.simapis.basic.simulation.SimulationControllerAPI
	 */

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.simulation.SimulationControllerAPI#resumeNext(long)
	 */
	@Override
	public boolean resumeNext(long currentTime) throws SimSchedulingException {
		// This method is called by the super-scheduler thread to notify the
		// simulation controller of a simulation component that its next pending
		// event can be executed.

		final Event first = eventPriorityQueue_.poll();

		LOGGER.debug("resumeNext({})",currentTime);

		if (first == null) {
			LOGGER.error("resumeNext: called but queue is empty!");
			return false;
		}

		// This informs the super-scheduler of the time of the next pending event after
		// processing the first found on the queue. We could call this only when the time is
		// expected to change, but calling possibly more than necessary is safe.
		scheduleNextPendingEvent();

		if (first.getTime() != currentTime)
			throw new SimSchedulingException();

		switch (first.getEventType()) {
		case START_PROCESS:

			tpes_.execute(new Runnable() {
				public void run() {
					try {
						first.invoke();
						superSchedInterface_.eventComplete();
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			});
			// SuperSched thread must wait here until the simulation thread comes back to supersched
			// Different ways to come back:
			// - invoke returns here, the thread is done
			// - the thread calls wait, which is notified to Supersched using eventStopped()
			superSchedInterface_.eventStartedAsync();
			return true; // Because a new thread is used

		case CONTROL_ACTION:
			LOGGER.debug("Control action (bloquing)...");
			try {
				first.invoke();
			} catch (Exception e) {
				e.printStackTrace();
			}
			// This type of event is used exclusively to restart a
			// sim thread blocked on wait. It must block the SS thread.
			// FIXME: We should not make this kind of assumption
			// superSchedInterface_.eventStartedAsync();
			return true;

		case SYNC_ACTION:
			LOGGER.debug("Sync action (non bloquing)...");
			try {
				first.invoke();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return false;

		case CONT_PROCESS:
			LOGGER.debug("Cont process (bloquing)...");
			blockedQueue_.remove(first.getEventId()).release();
			superSchedInterface_.eventStartedAsync();
			return true; // because it CANNOT be the SC thread
		}
		throw new RuntimeException("resume: Unknown type of event!");
	}

	private void processResumeRequest(final WaitRequest wr,final String result) {
		wr.setResult(result); // Removes the timedOut status
		Event event = (Event) wr.getEvent();
		this.blockedQueue_.remove(event);
		this.eventPriorityQueue_.remove(event);
		wr.release();
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.simulation.SimulationControllerAPI#resumeOneBlocked(java.lang.String, java.lang.String)
	 */
	@Override
	public synchronized boolean resumeOneBlocked(final  String condition, final  String result)
	throws SimSchedulingException {
		LOGGER.debug("SS called resumeOneBlocked(cond={},res={})",condition,result);
		// WARNING: This method is meant to be called by SuperScheduler, after ALL events pending
		// the current simulation time have been processed.

		if (! pendingRequests_.containsKey(condition))
			throw new RuntimeException("Attempt to wake up a thread on a condition that does not exist.");

		Collection<WaitRequest> list = pendingRequests_.get(condition);
		WaitRequest wr = list.iterator().next();
		pendingRequests_.remove(condition, wr);
		processResumeRequest(wr,result);
		return ! list.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.simulation.SimulationControllerAPI#resumeAllBlocked(java.lang.String, java.lang.String)
	 */
	@Override
	public synchronized int resumeAllBlocked(final  String condition, final  String result)
	throws SimSchedulingException {

		// WARNING: This method is meant to be called by SuperScheduler, after ALL events pending
		// the current simulation time have been processed.

		if (! pendingRequests_.containsKey(condition))
			throw new RuntimeException("Attempt to wake up a thread on a condition that does not exist.");

		//Collection<WaitRequest> list = pendingRequests_.get(condition);

		 Collection <WaitRequest> set =  pendingRequests_.removeAll(condition);

		for (WaitRequest wr: set) {
			processResumeRequest(wr,result);
		}
		superSchedInterface_.cancelWaitCond(localSchedInterface_, condition);

		return set.size();
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.simulation.SimulationControllerAPI#quit()
	 */
	@Override
	public void quit() {
		this.tpes_.shutdownNow();
	}



	/**
	 * Implementation of the fio.simapis.basic.ProcessModelingAPI
	 */

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.ProcessModelingAPI#scheduleProcessMyself(java.lang.String, java.lang.Object[], long)
	 */
	@Override
	public long scheduleProcessMyself(String methodName, Object[] parameters,
			long time) throws UnknownEventMethodException,
			IllegalEventMethodException {
		try {
			return scheduleMyself(methodName,parameters,time,ExoEventsTypes.START_PROCESS);
		} catch (OsaAPIException e) {
			e.printStackTrace();
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.ProcessModelingAPI#waitOnConditionForDelay(java.lang.String, long)
	 */
	@Override
	public String waitOnConditionForDelay(String condition, long delay)
			throws InterruptedException {
		// Create wait request
		// This should block the sim thread and resume the SS thread
		WaitRequest wr = new WaitRequest(WAIT_REQ_TYPE.WAIT);

		// Set as default, and remove if released before timer goes off
		wr.setTimedOut();

		LOGGER.debug("running waitOnConditionForDelay({},{})",condition,delay);

		// Add request in pending request list
		this.pendingRequests_.put(condition, wr);

		// Schedule timeout callback
		long eventId = scheduleControl("releaseAndWait", new Object[] { wr }, getSimulationTime()
				+ (delay < 0 ? 0 : delay));
		// Tell super scheduler about the the new condition
		this.superSchedInterface_.wakeMeUpCond(localSchedInterface_, condition);

		// Put this thread to sleep
		// transfer control to super scheduler

		// Resume SS thread
		superSchedInterface_.eventStopped();

		try {
			// block sim thread

			LOGGER.debug("waitOnConditionForDelay({},{}): going to sleep.",condition,delay);
			wr.acquire();
			LOGGER.debug("waitOnConditionForDelay({},{}): resume...",condition,delay);



			// Remove event from wait queues
			if (wr.isTimedOut()) {
				LOGGER.debug("waitOnConditionForDelay({},{}): Timed out!",condition,delay);
				this.pendingRequests_.remove(condition, wr);
				return null;
			} else cancelEvent(eventId);


		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		LOGGER.debug("waitOnConditionForDelay({},{}): released with result={}",condition,delay,wr.getResult());
		return wr.getResult();
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.ProcessModelingAPI#waitForDelay(long)
	 */
	@Override
	public void waitForDelay(long delay) {

		// This should block the sim thread and resume the SS thread
		// The releaseAndWait callback does the opposite
		WaitRequest wr = new WaitRequest(WAIT_REQ_TYPE.WAIT);
		scheduleControl("releaseAndWait", new Object[] { wr }, getSimulationTime()
				+ (delay < 0 ? 0 : delay));

		// Resume SS thread
		superSchedInterface_.eventStopped();

		try {
			// block sim thread
			wr.acquire();

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.ProcessModelingAPI#releaseOneOnCondition(java.lang.String, java.lang.String)
	 */
	@Override
	public synchronized boolean releaseOneOnCondition(final String condition, final String result) {

		// WARNING: This method is meant to be called by a simulation thread.

		return this.superSchedInterface_.releaseOneOnCondition(condition, result);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.ProcessModelingAPI#releaseAllOnCondition(java.lang.String, java.lang.String)
	 */
	@Override
	public synchronized
	int releaseAllOnCondition(String condition, String result) {

		return this.superSchedInterface_.releaseAllOnCondition(condition, result);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		NameController nc;
		try {
			nc = Fractal.getNameController((org.objectweb.fractal.api.Component) this);
		} catch (NoSuchInterfaceException e) {
			return "<No name>";
		}

		return nc.getFcName();
	}
}
