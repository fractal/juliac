/** ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.model.basic.helloworld;

import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.extensions.Membrane;


import fr.inria.osa.logger.basic.SimulationLogger;
import fr.inria.osa.simapis.basic.AbstractProcessModel;

/**
 * Hello part of the hello-world example.
 *
 * <p> Implements the business part of the Hello component. The
 * control part is generated automatically using AOP techniques
 * such as AspectJ or Spoon visitors.
 *
 * @author odalle
 *
 */
@Component
@Membrane(controller = "simPrimitive")
public class Hello extends AbstractProcessModel implements HelloItf{


	public Hello() {
		super(new SimulationLogger(Hello.class));
	}

	@Requires(name = "world")
	private WorldItf addWorld;

	public void generateHello() {
		logger_.debug("Running generateHello()...");
		addWorld.printWorld("Hello");
	}

	public void releaseWorld() {
		logger_.debug("Running releaseWorld()...");
		getSimProcessApi().releaseOneOnCondition("print", "Hello");
	}

}
