/** ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.model.basic.helloworld;

import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.extensions.Membrane;

import fr.inria.osa.logger.basic.SimulationLogger;
import fr.inria.osa.simapis.basic.AbstractProcessModel;

@Component
@Membrane(controller = "simPrimitive")
public class World extends AbstractProcessModel implements WorldItf {

	public World() {
		super(new SimulationLogger(World.class));
	}

	/**
	 * World with print action is delayed by a fixed amount of time.
	 */
	public void printWorld(String msg) {
		long start = getSimProcessApi().getSimulationTime();
		logger_.debug("Before waiting at t={}", start);
		getSimProcessApi().waitForDelay(10);
		logger_.info("{} World", msg);
		long end = getSimProcessApi().getSimulationTime();
		logger_.debug("World{}: finished work at t={}", start, end);
		assert ((start + 10) == end);
	}

	/**
	 * World with print action delayed by a wait on a condition. The message
	 * received from hello is not passed as a parameter but received from
	 * the wait call, to illustrate the channel mechanism between the
	 * releaser and the releasee(s).
	 */
	public void waitHello() {
		long start = getSimProcessApi().getSimulationTime();
		logger_.debug("Before waiting at t={}", start);
		try {
			String msg = getSimProcessApi()
					.waitOnConditionForDelay("print", 10);
			logger_.info("{} World", msg);

		} catch (InterruptedException e) {
			logger_.error("Wait interrupted", e);
		}
		long end = getSimProcessApi().getSimulationTime();
		logger_.debug("World{}: finished work at t={}", start, end);
	}

}

// Before = 8 + wait =18
// Before = 10 + wait = 20
// Before = 12 + wait = 22
// Before = 12 + wait = 22
// World = 18 (was 8)
// Finished = 18 (was 8)
// Before = 20 + wait = 30
// World = 20 (was 10)
// Finished = 20 (was 10)
// Before = 22 + wait = 32
// World = 22 (was 12)
// World = 22 (was 12)
// Finished = 22 (was 12)
// Finished = 22 (was 12)
// World = 30 (was 20)
// Finished = 30 (was 20)
// World = 32 (was 22)
// Finished = 32 (was 22)
