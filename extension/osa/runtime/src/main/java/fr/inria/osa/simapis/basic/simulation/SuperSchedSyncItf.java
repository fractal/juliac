package fr.inria.osa.simapis.basic.simulation;

public interface SuperSchedSyncItf {

	/**
	 * Tells the superscheduler that the asynchronous processing of an event
	 * is complete so that the super-scheduler thread can proceed with
	 * next event
	 */
	void eventComplete();

	/**
	 * Tells the superscheduler that the asynchronus processing of an event
	 * is blocked in model on a wait instruction
	 */
	void eventStopped();

	/**
	 * Tells the superscheduler that an asynchronous event processing has started.
	 */
	void eventStartedAsync();

	/**
	 * Requests the superscheduler to request the wake-up of one simulation
	 * thread sleeping on the given condition.
	 *
	 * <p> The superscheduler looks up in its list of simulation schedulers if
	 * one of them has registered such a sleeping thread. The request is passed on
	 * to the first scheduler found along with the result value to transfered to
	 * the woken up thread.
	 *
	 * @param condition
	 *             The condition on which a thread is expected to be waiting
	 *
	 * @param result
	 *             The result value to be transfered from the releaser to the
	 *             woken up thread
	 * @return true
	 *             If the release operation suceded and a thread was actually
	 *             found sleeping on the requested condition
	 */
	public boolean releaseOneOnCondition(final String condition, String result);


	/**
	 * Requests the superscheduler to request the wake-up of one simulation
	 * thread sleeping on the given condition.
	 *
	 * <p> The superscheduler looks up in its list of simulation schedulers if
	 * one of them has registered such a sleeping thread. The request is passed on
	 * to the first scheduler found along with the result value to transfered to
	 * the woken up thread.
	 *
	 * @param condition
	 *             The condition on which a thread is expected to be waiting
	 *
	 * @param result
	 *             The result value to be transfered from the releaser to the
	 *             woken up thread
	 * @return Count of threads woken up
	 *             The number of threads for which the release operation succeded
	 *             and a thread was actually found sleeping on the requested
	 *             condition
	 */
	public int releaseAllOnCondition(final String condition, String result);

}
