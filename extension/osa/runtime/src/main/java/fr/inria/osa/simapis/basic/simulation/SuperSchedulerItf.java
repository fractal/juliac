/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.simapis.basic.simulation;

import org.objectweb.fractal.fraclet.annotations.Interface;

import fr.inria.osa.simapis.basic.AbstractProcessModel;
import fr.inria.osa.simapis.basic.ProcessModelingAPI;
import fr.inria.osa.simapis.basic.SimulationTimeAPI;

/**
 * The super-scheduler is in charge of the synchronization and coordination between the
 * OSA SimulationModel components.
 *
 * <p>The SuperScheduler is a standard Fractal component. It is connected (bound) to
 * every {@link AbstractProcessModel} component to which it provides a number of centralized
 * functions to ensure the proper execution of the simulation, such as:
 * <ul>
 * <li> Starting and stopping the simulation
 * <li> Keeping track of the global (virtual) time of the simulation
 * <li> Queuing and activating the components according to the time order of their
 * next pending events
 * <li> Synchronization of user defined wait queues
 * </ul>
 *
 *
 * @author odalle
 * @author jribault
 * @see
 *     {@link fr.inria.osa.model.basic.helloworld} gives a more detailed description
 *     of OSA operations based on a simple example.
 */
@Interface(name = "superschedulersvc")
public interface SuperSchedulerItf extends java.lang.Runnable, SimulationTimeAPI, SuperSchedSyncItf {

	/**
	 * This method is used to launch the simulation once all the components
	 * are instantiated and deployed.
	 */
	void startSimulation();

	/**
	 * This method is used by a simulation component to register itself
	 * with the superscheduler.
	 *
	 * Using this method is useless when the superscheduler is able to
	 * introspect its bindings.
	 *
	 * @param simulationController
	 *        Simulation interface of the component that self registers.
	 */
	void register(final SimulationControllerAPI simulationController);


	/**
	 * This method is used by a simulation component to report its first
	 * pending event in its local scheduler.
	 * The following situations can happen depending on the type of
	 * this first event (see {@link ExoEventsTypes}):
	 * <ul>
	 * <li> {@literal START_PROCESS}: This event corresponds to the
	 * beginning of a new process. The super-scheduler will have to
	 * create (or allocate) a new Thread to process this event.
	 * <li> {@literal CONT_PROCESS}: This event corresponds to the
	 * restarting of a process that was stopped using a wait simulation
	 * call. This event is scheduled at the time the process is put
	 * to sleep, which implies that the super-scheduler will have to block
	 * the calling thread once the event is scheduled.
	 * <li> {@literal SYNC_ACTION}: This event corresponds to the
	 * scheduled execution of an event that will not consume any
	 * simulation time. This kind of synchronous event can be executed
	 * directly by the super-scheduler thread.
	 * <li> {@literal CONTROL_ACTION}: This event corresponds to the
	 * scheduled execution of a synchronous event that will execute a
	 * control method of the associated simulation component (eg. to quit
	 * simulation).
	 * </ul>
	 *
	 * @param simulationController
	 *        Simulation interface of the simulation component
	 * @param wakeUpTime
	 *        Time of the first pending event
	 * @param type
	 *           Type of event
	 * @param eventId
	 *           Event id (assigned independently by each simulation
	 *        component.)
	 * @param previousTime
	 *        The wake up time used the last time this method was called by
	 *        the same simulation controller
	 */
	void wakeMeUp(final SimulationControllerAPI simulationController,
			final long wakeUpTime, final ExoEventsTypes type,
			final long eventId, final long previousTime);


	/**
	 * This method is called by the simulation component to register a new
	 * wait condition after a call to {@link ProcessModelingAPI#waitOnConditionForDelay(String, long)}
	 *
	 * @param simulationController
	 * @param condition
	 */
	void wakeMeUpCond(final SimulationControllerAPI simulationController, final String condition);


	/**
	 * This method is called by the simulation component to un-register a
	 * wait condition after the last blocked thread on this condition has
	 * resumed execution
	 *
	 * @param simulationController
	 * @param condition
	 */
	void cancelWaitCond(final SimulationControllerAPI simulationController, final String condition);


}
