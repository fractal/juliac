package fr.inria.osa.simapis.basic.simulation;

import fr.inria.osa.simapis.basic.EventModelingAPI;
import fr.inria.osa.simapis.basic.exceptions.OsaAPIException;

public interface SimulationInternalSchedulingItf {

	/**
	   * Schedule a new method call at a given time.
	   *
	   * This actually implements the event-driven paradigm in which an action
	   * is scheduled for execution at a particular time.
	   *
	   * <p>Contrary to the pure event-driven model, the processing of such events may not be
	   * instantaneous. Unless otherwise stated (depending on type parameter), the processing
	   * of such a scheduled method may require an arbitrary amount of simulation time.
	   *
	   * <p> This amount of time is obtained using the process model primitives, such
	   * as waiting for a delay or a synchronization.
	   *
	   * In order to safely mix both the event-driven and process-based approaches, and unless
	   * the type of event is synchronous, its execution is transfered to new thread.
	   * In the particular case in which the event is synchronous, the processing
	   * is handled directly by the scheduling thread, which is faster, but
	   * prohibits using the process model primitives. In case the event type is synchronous,
	   * the event is handled delegated to {@link EventDrivenAPI#scheduleEventMyself(String, Object[], long).}
	   *
	   * <p> The schedule action returns an event id that can be used for cancellation using
	   * {@link EventModelingAPI#cancelEvent(long)}.
	   *
	   * @param methodName
	   *        The method that must be called.
	   * @param parameters
	   *        The method's parameters.
	   * @param time
	   *        The simulation time at which the method must be
	   *        executed.
	   * @param type
	   *         The type of event, as specified by {@link ExoEventsTypes}
	   * @return
	   *         The event id
	   *
	   */
	  long scheduleMyself(String methodName, Object[] parameters, long time, ExoEventsTypes type)
	  throws OsaAPIException;

}
