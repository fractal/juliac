/** ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.simapis.basic.simulation;

import java.util.concurrent.Semaphore;

/**
 * Simple implementation of the {@link SimulationWaitRequestItf} interface.
 *
 * @author odalle
 * @see fr.inria.osa.simapis.basic.simulation.SimulationWaitRequestItf
 */
public class WaitRequest implements SimulationWaitRequestItf {

	public static final String TIMEDOUT = "TimeOut";
	private final Semaphore sema_ = new Semaphore(0);
	private String result_;
	private SimulationEventItf event_;
	private final WAIT_REQ_TYPE type_;


	public WaitRequest(WAIT_REQ_TYPE type){
		this.type_ = type;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.SimulationWaitRequestItf#getType()
	 */
	public WAIT_REQ_TYPE getType() { return this.type_;}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.SimulationWaitRequestItf#setResult(java.lang.String)
	 */
	public synchronized void setResult(final String result) {
		if (result.equals(TIMEDOUT))
			throw new RuntimeException("Illegal result "+TIMEDOUT+" is a reserved keyword");
		result_ = result;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.SimulationWaitRequestItf#setTimedOut()
	 */
	public synchronized void setTimedOut() {
		result_ = TIMEDOUT;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.SimulationWaitRequestItf#getEventId()
	 */
	public SimulationEventItf getEvent() {
		return this.event_;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.SimulationWaitRequestItf#setEventId(long)
	 */
	public void setEvent(final SimulationEventItf event) {
		this.event_ = event;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.SimulationWaitRequestItf#isTimedOut()
	 */
	public boolean isTimedOut() {
		return (result_ != null) && result_.equals(TIMEDOUT);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.osa.simapis.basic.SimulationWaitRequestItf#getResult()
	 */
	public final synchronized String getResult() {
		return this.result_;
	}


	/**
	 * Used to block the simulation thread on a with condition.
	 *
	 * <p>This is just a delegate method for {@link
	 * java.lang.concurrent.Semaphore#acquire() }
	 * @throws InterruptedException
	 * @see java.lang.concurrent.Semaphore#acquire()
	 */
	public void acquire() throws InterruptedException {
		this.sema_.acquire();
	}


	/**
	 * Used to resume the simulation thread previously blocked
	 * on a wait condition.
	 *
	 * <p>This is just a delegate method for {@link
	 * java.lang.concurrent.Semaphore#releasee() }
	 * @throws InterruptedException
	 * @see java.lang.concurrent.Semaphore#release()
	 */
	public void release() {
		this.sema_.release();
	}
}
