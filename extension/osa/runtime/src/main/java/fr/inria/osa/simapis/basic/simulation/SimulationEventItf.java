/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.simapis.basic.simulation;

import java.lang.reflect.InvocationTargetException;

/**
 *
 * Defines the API for OSA events.
 *
 * <p>
 * OSA events are similar to functor objects, that is an object that contains a
 * method to be executed along with its arguments, and the virtual simulation
 * time at which to schedule the execution. However, to be consistent with the
 * encapsulation principles of Fractal, events can only be created and executed
 * within the component that owns the (the class that owns the) method scheduled
 * by the event. Because of this limitation the OSA event API is mostly internal
 * because events are created internally by the simulation controller of the
 * component that owns the method scheduled in the event.
 *
 * @author odalle
 *
 */
public interface SimulationEventItf {

	/**
	 * Give the event processed currentTime_ (simulation currentTime_).
	 *
	 * @return The event processed currentTime_.
	 */
	public long getTime();

	/**
	 * @param method
	 *            the method to set
	 */
	public void setMethod(String method);

	/**
	 * @param methodParameters
	 *            the methodParameters to set
	 */
	public void setMethodParameters(Object[] methodParameters);

	/**
	 * @param time
	 *            the time to set
	 */
	public void setTime(long time);

	/**
	 * Executes the method designated by the event using the parameters of the
	 * event.
	 *
	 * @throws IllegalArgumentException
	 * @throws SecurityException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	public void invoke() throws
			IllegalAccessException, InvocationTargetException,
			NoSuchMethodException;

	/**
	 * Give the method associated with this event.
	 *
	 * @return The method called when this event is processed.
	 */
	public String getEvtMethod();

	/**
	 * Give the parameters of event method.
	 *
	 * @return The array of event method parameters.
	 */
	public Object[] getEvtParam();

	/**
	 * Assigns a type to the event.
	 *
	 *
	 * @param eventType
	 * @see {@link fr.inria.osa.simapis.basic.simulation.ExoEventsTypes}
	 */
	public void setEventType(ExoEventsTypes eventType);

	public ExoEventsTypes getEventType();

	/**
	 * Assigns a unique id.
	 *
	 * @param id
	 */
	public void setEventId(long id);

	/**
	 * Get the event id.
	 *
	 * @return the event id
	 */
	public long getEventId();

	/**
	 * Set the object instance that owns the method to be executed by the event.
	 *
	 * @param instance
	 */
	public void setInstance(Object instance);

	/**
	 * Get the event object instance.
	 *
	 * @return the event object instance.
	 */
	public Object getEvtInstance();

}
