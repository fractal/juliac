/** ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.simapis.basic.simulation;

import java.lang.reflect.InvocationTargetException;

/**
 * Simulation events. Each event is formalized by a method call at a given
 * simulation time. The event also keeps track of the instance of the simulation
 * component to which the method belongs to.
 *
 */
public abstract class AbstractEvent implements SimulationEventItf {

	/** The method_ to call. */
	private String method_;

	/** The parameters of the method_. */
	private Object[] methodParameters_;

	/** The event simulation time_. */
	private long time_;

	/** An identifier used for designating events **/
	private long eventId_;

	private ExoEventsTypes eventType_;

	private Object instance_;

	/**
	 * @param method_
	 *            the method_ to set
	 */
	public void setMethod(String method) {
		method_ = method;
	}

	/**
	 * @param methodParameters_
	 *            the methodParameters_ to set
	 */
	public void setMethodParameters(Object[] methodParameters) {
		methodParameters_ = methodParameters;
	}

	/**
	 * @param time_
	 *            the time_ to set
	 */
	public void setTime(long time) {
		time_ = time;
	}

	/**
	 * Initialize this new event.
	 *
	 * @param method
	 *            The method to call for processed this event.
	 * @param methodParameters
	 *            The parameters of processed event method.
	 * @param time
	 *            The simulation time at which this event must be processed.
	 */
	public AbstractEvent(final String method, final Object[] methodParameters,
			final long time, final ExoEventsTypes type, Object instance) {
		this.method_ = method;
		this.methodParameters_ = methodParameters;
		this.time_ = time;
		this.setEventType(type);
		this.instance_ = instance;
	}

	/**
	 * Initialize this new event.
	 */
	public AbstractEvent() {
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.inria.osa.simapis.basic.SimulationEventItf#invoke()
	 */
	public void invoke() throws
			IllegalAccessException, InvocationTargetException,
			NoSuchMethodException {

		getEvtInstance().getClass().getMethod(getEvtMethod(), getEvtArgTypes())
				.invoke(getEvtInstance(), getEvtParam());
	}

	protected Class<?>[] getEvtArgTypes() {
		Object[] parameters = getEvtParam();
		Class<?>[] argtypes;
		if (parameters == null) {
			argtypes = null;
		} else {
			argtypes = new Class<?>[parameters.length];
			for (int i = 0; i < parameters.length; i++) {
				argtypes[i] = parameters[i].getClass();
			}
		}
		return argtypes;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.inria.osa.simapis.basic.SimulationEventItf#getEvtMethod()
	 */
	public final String getEvtMethod() {
		return method_;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.inria.osa.simapis.basic.SimulationEventItf#getEvtParam()
	 */
	public final Object[] getEvtParam() {
		return methodParameters_;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * fr.inria.osa.simapis.basic.SimulationEventItf#setEventType(fr.inria.osa
	 * .simapis.basic.ExoEventsTypes)
	 */
	public void setEventType(ExoEventsTypes eventType_) {
		this.eventType_ = eventType_;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.inria.osa.simapis.basic.SimulationEventItf#getEventType()
	 */
	public ExoEventsTypes getEventType() {
		return eventType_;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.inria.osa.simapis.basic.SimulationEventItf#getTime()
	 */
	public final long getTime() {
		return time_;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.inria.osa.simapis.basic.SimulationEventItf#setEventId(long)
	 */
	public void setEventId(long id) {
		this.eventId_ = id;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.inria.osa.simapis.basic.SimulationEventItf#getEventId()
	 */
	public long getEventId() {
		return this.eventId_;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * fr.inria.osa.simapis.basic.SimulationEventItf#setInstance(java.lang.Object
	 * )
	 */
	public void setInstance(Object instance) {
		this.instance_ = instance;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.inria.osa.simapis.basic.SimulationEventItf#getEvtInstance()
	 */
	public Object getEvtInstance() {
		return this.instance_;
	}

	/**
	 * Give the <code>String</code> representation of this event.
	 *
	 * @return The <code>String</code> representation of this event.
	 */
	@Override
	public final String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(getEvtInstance().getClass() + "." + method_ + "( ");
		for (int i = 0; i < methodParameters_.length; i++) {
			sb.append(methodParameters_[i] + " ");
		}
		sb.append(") at " + time_);
		return sb.toString();
	}

}
