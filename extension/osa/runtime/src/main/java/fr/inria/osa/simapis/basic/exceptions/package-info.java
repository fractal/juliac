/**
 * Exceptions from OSA APIs.
 *
 * @author odalle
 */
package fr.inria.osa.simapis.basic.exceptions;
