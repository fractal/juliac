/**
 * APIs used in OSA either internally between OSA components or externally
 * for OSA end users to develop their simulations models.
 *
 * @author odalle
 */
package fr.inria.osa.simapis.basic;
