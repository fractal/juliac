/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.runtime.basic.launcher;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.util.Fractal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.osa.simapis.basic.simulation.SharedSuperSchedulerItf;

public class Launcher {

	private static final Logger logger_ =
		LoggerFactory.getLogger(Launcher.class);

  public static void main(String[] args) throws Exception {

	logger_.trace("#################Launch#####################");
	Class<Factory> cl = (Class<Factory>) Class.forName(args[0]);
	Factory factory = cl.getConstructor().newInstance();
	Component appli = factory.newFcInstance();

	logger_.trace("#################InitSimu#####################");
	// We need to retrieve the SuperScheduler from the root component
	Component[] topLevelComps =
		Fractal.getContentController(appli).getFcSubComponents();
	Component sharedSuperSchedulerComp = null;
	SharedSuperSchedulerItf superSchedItf = null;

	for (Component comp: topLevelComps) {
		try {
			logger_.debug("Launch: is {} the SS?", comp);
			superSchedItf = (SharedSuperSchedulerItf)
				comp.getFcInterface("superschedulersvc");
		}
		catch (NoSuchInterfaceException e) {
			continue;
		}
		// No exception means we've found the SS
		sharedSuperSchedulerComp = comp;
		break;
	}

	if (sharedSuperSchedulerComp == null) {
		throw new RuntimeException("Launcher: failed to retrieve SharedSS!");
	}

	logger_.debug("Launch: SS Found ({})",sharedSuperSchedulerComp);
	// Need to call LCC to unlock SS server itf
	Fractal.getLifeCycleController(sharedSuperSchedulerComp).startFc();
	// Deploy SS
	superSchedItf.deployAndBind(sharedSuperSchedulerComp, appli);

	logger_.trace("#################StartFractal#####################");
	Fractal.getLifeCycleController(appli).startFc();


	logger_.trace("#################Run#####################");
	superSchedItf.startSimulation();

	logger_.trace("########### Run Complete (time={}) #############",superSchedItf.getSimulationTime());
  }
}
