/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.simapis.basic;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.objectweb.fractal.fraclet.types.Contingency;

import fr.inria.osa.simapis.basic.simulation.SimulationLoggerItf;
import fr.inria.osa.simapis.basic.simulation.SuperSchedulerItf;

@Component
@Membrane(controller = "simPrimitive")
public abstract class AbstractEventModel {

	/** The super scheduler. */
	@SuppressWarnings("unused")
	@Requires(name = "superscheduler", contingency = Contingency.OPTIONAL)
	private SuperSchedulerItf superScheduler_;

	public final SimulationLoggerItf logger_;

	public AbstractEventModel(SimulationLoggerItf logger){
		logger_ = logger;
	}

	/** The simulation controller. */
	@Controller(name="modeling-event-controller")
	private EventModelingAPI simEventApi_;

	protected EventModelingAPI getSimProcessApi(){
		return simEventApi_;
	}

	public synchronized void bindFc(String id, Object ref) throws NoSuchInterfaceException {
		if (id.equals("component")) {
			this.simEventApi_ = ((ProcessModelingAPI)(((org.objectweb.fractal.api.Component)(ref)).getFcInterface("modeling-event-controller")));
			logger_.setTimeApi(this.simEventApi_);
		}

		throw new NoSuchInterfaceException((("Client interface \'" + id) + "\' is undefined."));
	}

}
