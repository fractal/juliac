/**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<!--                Open Simulation Architecture (OSA)                  -->
<!--                                                                    -->
<!--      This software is distributed under the terms of the           -->
<!--           CECILL-C FREE SOFTWARE LICENSE AGREEMENT                 -->
<!--  (see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html) -->
<!--                                                                    -->
<!--  Copyright © 2006-2015 Université Nice Sophia Antipolis            -->
<!--  Contact author: Olivier Dalle (olivier.dalle@unice.fr)            -->
<!--                                                                    -->
<!--  Parts of this software development were supported and hosted by   -->
<!--  INRIA from 2006 to 2015, in the context of the common research    -->
<!--  teams of INRIA and I3S, UMR CNRS 7172 (MASCOTTE, COATI, OASIS and -->
<!--  SCALE).                                                           -->
<!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
package fr.inria.osa.simapis.basic.simulation;


/**
 * The list of events currently supported in OSA.
 * Each event is associated with an action and time-stamped
 * with a scheduled date of execution for that action.
 * Depending on the type of event, the action is either fixed or
 * given as a parameter.
 *
 * The current list of events:
 * <ul>
 *  <li>{@literal START_PROCESS}: Start a new simulation process. A Process is a sequence of actions
 *  that can occur at different times of the simulation. In other words
 *  the simulation time at the begin and at the end of the process MAY
 *     be different. This event requires a method name, a list of string parameters,
 *  and a time
 *  <li>{@literal CONT_PROCESS}: Since a process may last in time, its execution steps may interleave
 *     with the ones of other processes or synchronous actions. This cont
 *     event is used to schedule at which time a sleeping process is
 *     scheduled to resume.
 *  <li>{@literal SYNC_ACTION}: An action scheduled to be fully executed at a given time of the
 *  simulation. As opposed to the Process, the begin and end time of
 *     this event are always identical.
 *  <li>{@literal CONTROL_ACTION}: A synchronous event similar to the previous SYNC_ACTION, except the
 *  associated action is a control method rather than a business method
 *  as in the previous cases.
 *  This event is used for example to schedule the end of the simulation
 *  at a given time even if all pending events have not been processed yet.
 * </ul>
 *
 * @author odalle
 *
 */
public enum ExoEventsTypes {

	START_PROCESS,
	CONT_PROCESS,
	SYNC_ACTION,
	CONTROL_ACTION;

	/**
	 * A predicate to check whether a string corresponds to the name
	 * of a known event. Note that the comparison is case insensitive.
	 *
	 * @param str
	 *             A string to check
	 * @return
	 *             true if given string is a known event
	 */
	public static boolean contains(String str){
		for (ExoEventsTypes evt: values()) {
			if (evt.name().equalsIgnoreCase(str)) return true;
		}
		return false;
	}

	/**
	 * A lookuo function to retrieve the enum element that corresponds
	 * to a given name. Note the search is case insensitive.
	 *
	 * @param str
	 *             A name to lookup
	 * @return
	 *             A ExoEventEnum entry if a match is found, null otherwise.
	 */
	public static ExoEventsTypes lookup(String str) {
		for (ExoEventsTypes evt: values()) {
			if (evt.name().equalsIgnoreCase(str)) return evt;
		}
		return null;
	}
}
