/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.commons.lang.reflect;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class provides helper methods for the {@link Method} class.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2.6
 */
public class MethodHelper {

	/**
	 * Check whether the specified method is a getter.
	 *
	 * @param getter  the method to be checked
	 * @throws IllegalArgumentException
	 *      if <code>getter</code> is not a getter method
	 */
	public static void checkGetterMethod( Method getter ) {

		String name = getter.getName();
		if( ! name.startsWith("get") ) {
			final String msg =
				"The name of a getter method should start with get: "+getter;
			throw new IllegalArgumentException(msg);
		}
		Class<?>[] ptypes = getter.getParameterTypes();
		if( ptypes.length != 0 ) {
			final String msg =
				"A getter method should not define any parameter: "+getter;
			throw new IllegalArgumentException(msg);
		}
	}

	/**
	 * Check whether the specified methods are a valid pair of setter/getter
	 * methods for the same property.
	 *
	 * @throws IllegalArgumentException  if this is not the case
	 */
	public static void checkMatchingSetterGetterMethods(
		Method setter, Method getter ) {

		checkSetterMethod(setter);
		checkGetterMethod(getter);

		String setterPropName = getSetterPropertyName(setter);
		String getterPropName = getGetterPropertyName(getter);
		if( ! setterPropName.equals(getterPropName) ) {
			final String msg =
				"Property names differ: "+setterPropName+" vs "+getterPropName;
			throw new IllegalArgumentException(msg);
		}

		Class<?> setterPropType = getSetterPropertyType(setter);
		Class<?> getterPropType = getGetterPropertyType(getter);
		if( ! setterPropType.equals(getterPropType) ) {
			final String msg =
				"Property types differ: "+setterPropType+" vs "+getterPropType;
			throw new IllegalArgumentException(msg);
		}
	}

	/**
	 * Check whether the specified method is a setter.
	 *
	 * @param setter  the method to be checked
	 * @throws IllegalArgumentException
	 *      if <code>setter</code> is not a setter method
	 */
	public static void checkSetterMethod( Method setter ) {

		String name = setter.getName();
		if( ! name.startsWith("set") ) {
			final String msg =
				"The name of a setter method should start with set: "+setter;
			throw new IllegalArgumentException(msg);
		}
		Class<?> rtype = setter.getReturnType();
		if( ! rtype.equals(void.class) ) {
			final String msg = "A setter method should return void: "+setter;
			throw new IllegalArgumentException(msg);
		}
		Class<?>[] ptypes = setter.getParameterTypes();
		if( ptypes.length != 1 ) {
			final String msg =
				"A setter method should define only one parameter: "+setter;
			throw new IllegalArgumentException(msg);
		}
	}

	/**
	 * Check whether the specified method is a setter for the specified
	 * property.
	 *
	 * @param setter    the method to be checked
	 * @param proptype  the property type
	 * @throws IllegalArgumentException
	 *      if the method is not a setter for the specified property
	 */
	public static void checkSetterMethod( Method setter, Class<?> proptype ) {

		Class<?> type = getSetterPropertyType(setter);

		if( ! proptype.isAssignableFrom(type) ) {
			final String msg =
				"Method "+setter+
				" is not a setter method for property type "+proptype.getName();
			throw new IllegalArgumentException(msg);
		}
	}

	/**
	 * Return the getter method corresponding to the specified setter.
	 *
	 * @throws NoSuchMethodException  if the getter does not exist
	 */
	public static Method getGetterForSetter( Method setter )
	throws NoSuchMethodException {

		Class<?> cl = setter.getDeclaringClass();

		// First search in declared methods
		Method[] methods = cl.getDeclaredMethods();
		for (Method method : methods) {
			if( isMatchingSetterGetter(setter,method) ) {
				return method;
			}
		}

		// First search in declared methods
		methods = cl.getMethods();
		for (Method method : methods) {
			if( isMatchingSetterGetter(setter,method) ) {
				return method;
			}
		}

		throw new NoSuchMethodException();
	}

	/**
	 * Return the name of the property set by the specified getter method.
	 *
	 * @param getter  a getter method
	 * @return        the name of the property set by <code>getter</code>
	 * @throws IllegalArgumentException
	 *      if <code>getter</code> is not a getter method
	 */
	public static String getGetterPropertyName( Method getter ) {

		checkGetterMethod(getter);

		String name = getter.getName();
		name = name.substring(3);   // Skip get
		name = name.substring(0,1).toLowerCase() + name.substring(1);

		return name;
	}

	/**
	 * Return the type of the property set by the specified getter method.
	 *
	 * @param getter  a setter method
	 * @return        the type of the property set by <code>getter</code>
	 * @throws IllegalArgumentException
	 *      if <code>getter</code> is not a getter method
	 */
	public static Class<?> getGetterPropertyType( Method getter ) {

		checkGetterMethod(getter);

		Class<?> rtype = getter.getReturnType();
		return rtype;
	}

	/**
	 * Return the type of the property set by the specified setter method.
	 *
	 * @param setter  a setter method
	 * @return        the type of the property set by <code>setter</code>
	 * @throws IllegalArgumentException
	 *      if <code>setter</code> is not a setter method
	 */
	public static Class<?> getSetterPropertyType( Method setter ) {

		checkSetterMethod(setter);

		Class<?>[] ptypes = setter.getParameterTypes();
		return ptypes[0];
	}

	/**
	 * Return the name of the property set by the specified setter method.
	 *
	 * @param setter  a setter method
	 * @return        the name of the property set by <code>setter</code>
	 * @throws IllegalArgumentException
	 *      if <code>setter</code> is not a setter method
	 */
	public static String getSetterPropertyName( Method setter ) {

		checkSetterMethod(setter);

		String name = setter.getName();
		name = name.substring(3);   // Skip set
		name = name.substring(0,1).toLowerCase() + name.substring(1);

		return name;
	}

	/**
	 * Strip modifiers, return type, class name and throw clause from the
	 * signature of the specified method.
	 *
	 * For a method signature of the form
	 *      <code>public void org.ow2.frascati.tinfi.reflect.UtilTestCase$Target.init() throws java.lang.RuntimeException</code>
	 * this method returns
	 *   <code>init()</code>
	 */
	public static String getShortSignature( Method src ) {
		String signature = src.toString();
		int parenthesis = signature.indexOf('(');
		String s = signature.substring(0,parenthesis);
		int methoddot = s.lastIndexOf('.');
		String ret = signature.substring(methoddot+1);
		int thr = ret.indexOf(" throws");
		if( thr != -1 ) {
			ret = ret.substring(0,thr);
		}
		return ret;
	}

	/**
	 * Return the names of the type variables declared by the specified method.
	 * Return an empty array if the current method does not declare any type
	 * variable.
	 */
	public static String[] getTypeParameterNames( Method method ) {

		TypeVariable<Method>[] tvs = method.getTypeParameters();
		if( tvs.length == 0 ) {
			return new String[0];
		}

		String[] tps = new String[ tvs.length ];
		for (int i = 0; i < tvs.length; i++) {
			TypeVariable<Method> tv = tvs[i];
			tps[i] = tv.getName();
		}

		return tps;
	}

	/**
	 * Dynamically invoke the specified setter method on the specified object
	 * with the specified value.
	 *
	 * @param content  the object
	 * @param setter   the setter method
	 * @param value    the value
	 */
	public static void invokeSetter( Object content, Method setter, Object value )
	throws IllegalAccessException, InvocationTargetException {

		// Check whether this is a setter
		boolean b = isSetterMethod(setter);
		if(!b) {
			return;
		}

		Class<?>[] ptypes = setter.getParameterTypes();
		if( value == null ) {
			// Null value cannot be injected on primitive types
			if( ! ptypes[0].isPrimitive() ) {
				setter.invoke(content,value);
			}
		}
		else {
			// Check that the parameter is assignable from the value
			Class<?> cl = value.getClass();
			if( ptypes[0].isAssignableFrom(cl) ) {
				setter.invoke(content,value);
			}
		}
	}

	/**
	 * Return <code>true</code> if the specified method is a getter.
	 *
	 * @param getter  the method to be checked
	 * @return  <code>true</code> if this is a getter
	 */
	public static boolean isGetterMethod( Method getter ) {
		try {
			checkGetterMethod(getter);
			return true;
		}
		catch( IllegalArgumentException iae ) {
			return false;
		}
	}

	/**
	 * Return <code>true</code> if the specified pair of methods are valid
	 * setter/getter for the same property.
	 */
	public static boolean isMatchingSetterGetter(
		Method setter, Method getter ) {

		try {
			checkMatchingSetterGetterMethods(setter,getter);
			return true;
		}
		catch( IllegalArgumentException iae ) {
			return false;
		}
	}

	/**
	 * Return <code>true</code> if the specified method is a setter.
	 */
	public static boolean isSetterMethod( Method setter ) {
		try {
			checkSetterMethod(setter);
			return true;
		}
		catch( IllegalArgumentException iae ) {
			return false;
		}
	}

	/**
	 * Return <code>true</code> if the source method overrides the target
	 * method.
	 *
	 * @param src     the source method
	 * @param target  the target method
	 */
	public static boolean override( Method src, Method target ) {

		boolean b = sameSignature(src,target);
		if(!b) {
			return false;
		}

		Class<?> srccl = src.getDeclaringClass();
		Class<?> targetcl = target.getDeclaringClass();

		if( targetcl.isAssignableFrom(srccl) ) {
			/*
			 * If targetcl is assignable from srccl, this means that src (the
			 * declaring class of) is a subtype of target (idem.) Then, since
			 * target and src share the same signature, src overrides target.
			 */
			return true;
		}

		return false;
	}

	/**
	 * Return a copy of the specified array where overridden methods have been
	 * removed.
	 */
	public static Method[] removeOverridden( Method[] methods ) {
		List<Method> res = new ArrayList<>();
		res.addAll(Arrays.asList(methods));
		for (int i = 0; i < methods.length; i++) {
			for (int j = 0; j < methods.length; j++) {
				if(j==i) { continue; }
				boolean b = override(methods[j],methods[i]);
				if(b) {
					res.remove(methods[i]);
				}
			}
		}
		return res.toArray(new Method[res.size()]);
	}

	/**
	 * Return <code>true</code> if the target and source methods share the same
	 * signature.
	 *
	 * @param src     the source method
	 * @param target  the target method
	 */
	public static boolean sameSignature( Method src, Method target ) {
		String srcsig = getShortSignature(src);
		String targetsig = getShortSignature(target);
		boolean b = srcsig.equals(targetsig);
		return b;
	}
}
