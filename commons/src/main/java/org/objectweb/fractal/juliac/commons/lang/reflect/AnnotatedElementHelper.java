/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.commons.lang.reflect;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.AnnotatedElement;

/**
 * This class provides helper methods for the {@link AccessibleObject} class.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class AnnotatedElementHelper {

	/**
	 * Return the annotation associated with the specified annotated element
	 * whose type name is one of those contained in the specified varargs.
	 * Return <code>null</code> if no such annotation is found.
	 *
	 * @param ae               the annotated element
	 * @param annotClassNames  the varargs of annotation type names
	 */
	public static Annotation getAnnotation(
		AnnotatedElement ae, String... annotClassNames ) {

		Annotation[] annots = ae.getAnnotations();
		for (String annotClassName : annotClassNames) {
			for (Annotation annot : annots) {
				String name = annot.annotationType().getName();
				if( name.equals(annotClassName) ) {
					return annot;
				}
			}
		}
		return null;
	}
}
