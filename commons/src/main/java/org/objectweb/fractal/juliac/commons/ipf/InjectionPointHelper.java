/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.commons.ipf;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;

/**
 * This class provides utility methods for handling injection points.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class InjectionPointHelper {

	/**
	 * Return the value of the specified injection point for the specified
	 * object. This method smooths exceptions to runtime.
	 *
	 * @param ip      the injection point
	 * @param target  the target object
	 * @return        the value of the injection point for the target object
	 */
	public static <A extends Annotation> Object
	get( InjectionPoint<A> ip, Object target ) {
		try {
			return ip.get(target);
		}
		catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
		catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Set the value of the specified injection point for the specified object.
	 * This method smooths exceptions to runtime.
	 *
	 * @param ip      the injection point
	 * @param target  the target object
	 * @param value   the value
	 */
	public static <A extends Annotation> void
	set( InjectionPoint<A> ip, Object target, Object value ) {
		try {
			ip.set(target,value);
		}
		catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
		catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}
}
