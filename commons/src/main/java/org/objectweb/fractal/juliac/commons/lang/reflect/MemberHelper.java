/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.commons.lang.reflect;

import java.lang.reflect.Member;
import java.lang.reflect.Modifier;

/**
 * This class provides helper methods for the {@link Member} class.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class MemberHelper {

	/**
	 * Return <code>true</code> if the specified member is statically accessible
	 * from the specified type name.
	 *
	 * @param member  the member
	 * @param name    the fully qualified name of the source type
	 */
	public static boolean isAccessibleWithStaticInvocation(
		Member member, String name ) {

		int mod = member.getModifiers();

		if( Modifier.isPrivate(mod) ) { return false; }
		if( Modifier.isPublic(mod) ) { return true; }

		/*
		 * protected or package public. Check that the declaring class of the
		 * member is in the same package as the type.
		 */
		int l = name.lastIndexOf('.');
		String pname = name.substring(0,l);

		String fpname = member.getDeclaringClass().getPackage().getName();
		boolean b = fpname.equals(pname);
		return b;
	}
}
