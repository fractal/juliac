/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.commons.lang;

/**
 * This class provides helper methods for the {@link String} class.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class StringHelper {

	/**
	 * Split the specified string using the first encountered delimiter
	 * character and return the substrings before and after in an array of
	 * size 2. If there is no such delimiter character, return an array of size
	 * 0.
	 *
	 * @param str        the input string
	 * @param index  the delimiter character
	 * @return           the array
	 */
	public static String[] around( String str, char delimiter ) {
		int index = str.indexOf(delimiter);
		if( index == -1 ) {
			return new String[0];
		}
		String[] ret = new String[2];
		ret[0] = str.substring(0,index);
		ret[1] = str.substring(index+1);
		return ret;
	}

	/**
	 * Return a copy of the specified string, with leading and trailing blank
	 * characters (whitespace, tab and newline) omitted.
	 */
	public static String trimHeadingAndTrailingBlanks( String src ) {

		if( src == null ) {
			return null;
		}

		// i is the index of the first non blank character
		int i=-1 , len=src.length();
		char c;
		do {
			i++;
			if( i == len ) {
				// Either the string is empty or contains only blank characters
				return "";
			}
			c = src.charAt(i);
		}
		while( c==' ' || c=='\t' || c=='\n' );

		// j is the index of the first non blank character from the end of src
		int j = src.length();
		do {
			j--;
			c = src.charAt(j);
		}
		while( c==' ' || c=='\t' || c=='\n' );

		String trim = src.substring(i,j+1);
		return trim;
	}
}
