/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.commons.lang.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * This class provides helper methods for the {@link Annotation} interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class AnnotationHelper {

	/**
	 * Return the value of the parameter associated with the specified
	 * annotation.
	 *
	 * @param annot  the annotation
	 * @param name   the parameter name
	 * @return
	 *         the parameter value or <code>null</code> if the annotation is
	 *         <code>null</code> or if the annotation does not define the specified
	 *         parameter name
	 */
	public static <T> T getAnnotationParamValue(
		Annotation annot, String name ) {

		if( annot == null ) {
			return null;
		}

		Class<? extends Annotation> annotcl = annot.annotationType();
		try {
			Method meth = annotcl.getMethod(name);
			@SuppressWarnings("unchecked")
			T value = (T) meth.invoke(annot);
			return value;
		}
		catch (NoSuchMethodException e) {
			return null;
		}
		catch (IllegalAccessException e) {
			return null;
		}
		catch (InvocationTargetException e) {
			return null;
		}
	}
}
