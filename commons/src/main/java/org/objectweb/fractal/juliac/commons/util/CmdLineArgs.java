/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.commons.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class provides a way for retrieving options, flags and free values
 * transmitted as command line arguments.
 *
 * Options and flags are supposed to be string starting with two minus
 * characters. Options are followed by a value.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.2
 */
public class CmdLineArgs<F extends Enum<F>,O extends Enum<O>> {

	private Set<String> flags = new HashSet<>();
	private Set<String> options = new HashSet<>();
	private Map<String,String> values = new HashMap<>();
	private List<String> files = new ArrayList<>();

	/**
	 * Register a new flag.
	 *
	 * @param flag  the flag to register
	 * @throws IllegalArgumentException
	 *      if flag is already registered as an option
	 */
	public void registerFlag( F flag ) {
		String key = "--"+flag.toString().toLowerCase();
		if( options.contains(key) ) {
			final String msg = key+" already registered as an option";
			throw new IllegalArgumentException(msg);
		}
		flags.add(key);
	}

	/**
	 * Register new flags.
	 *
	 * @param flags  the flags to register
	 * @throws IllegalArgumentException
	 *      if one of the flags is already registered as an option
	 * @since 2.2.4
	 */
	public void registerFlags( F[] flags ) {
		for (F flag : flags) {
			registerFlag(flag);
		}
	}

	/**
	 * Register a new option.
	 *
	 * @param option  the option to register
	 * @throws IllegalArgumentException
	 *      if option is already registered as a flag
	 */
	public void registerOption( O option ) {
		String key = "--"+option.toString().toLowerCase();
		if( flags.contains(key) ) {
			final String msg = key+" already registered as a flag";
			throw new IllegalArgumentException(msg);
		}
		options.add(key);
	}

	/**
	 * Register new options.
	 *
	 * @param options  the options to register
	 * @throws IllegalArgumentException
	 *      if one of the options is already registered as an option
	 * @since 2.2.4
	 */
	public void registerOptions( O[] options ) {
		for (O option : options) {
			registerOption(option);
		}
	}

	/**
	 * Return <code>true</code> if the specified flag is set in the command line
	 * parsed by {@link #parse(String)}.
	 */
	public boolean isFlagSet( F flag ) {
		String key = "--"+flag.toString().toLowerCase();
		return values.containsKey(key);
	}

	/**
	 * Return the value of the specified option in the command line parsed by
	 * {@link #parse(String)}.
	 */
	public String getOptionValue( O option ) {
		String key = "--"+option.toString().toLowerCase();
		return values.get(key);
	}

	/**
	 * Return the list of files in in the command line parsed by
	 * {@link #parse(String[])}. Files are the strings in the command line
	 * which are neither options, nor flags.
	 */
	public List<String> getFiles() {
		return files;
	}

	/**
	 * Parse the specified command line. Character cases and minus
	 * characters are ignored, e.g. --modulepath, --module-path and
	 * --MODULEPATH are considered as the same option or flag.
	 *
	 * @param args  the command line to parse
	 * @throws IllegalArgumentException  if an option is missing a value
	 */
	public void parse( String[] args ) {

		for (int i = 0; i < args.length; i++) {

			String arg = args[i];
			String key = arg.toLowerCase();

			// Remove minus characters except heading ones
			int fnm = 0;
			while( fnm < key.length() && key.charAt(fnm) == '-' ) {
				fnm++;
			}
			if( fnm < key.length() ) {
				key =
					key.substring(0,fnm) +
					key.substring(fnm).replace("-","");
			}

			if( flags.contains(key) ) {
				values.put(key,null);
			}
			else if( options.contains(key) ) {
				if( i == args.length-1 ) {
					final String msg = "Missing value for option: "+arg;
					throw new IllegalArgumentException(msg);
				}
				i++;
				values.put(key,args[i]);
			}
			else if( key.charAt(0) == '-' ) {
				final String msg = "Illegal option: "+arg;
				throw new IllegalArgumentException(msg);
			}
			else {
				files.add(arg);
			}
		}
	}
}
