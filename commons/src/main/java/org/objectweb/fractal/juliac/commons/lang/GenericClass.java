/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Jonathan Labejof
 */

package org.objectweb.fractal.juliac.commons.lang;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Wrapper class for {@link Class} that performs generic type resolution.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Jonathan Labejof <Jonathan.Labejof@inria.fr>
 * @since 1.3
 */
public class GenericClass {

	public GenericClass( Class<?> cl ) {

		this.cl = cl;

		if( cl.isPrimitive() ) {
			// Primitive types cannot declare generics
			return;
		}

		Deque<Class<?>> genericInterfaces = new ArrayDeque<>();
		Set<Class<?>> interfacesAlreadyAnalyzed = new HashSet<>();

		genericInterfaces.add(cl);

		while( ! genericInterfaces.isEmpty() ) {
			Class<?> genericInterface = genericInterfaces.pop();
			Type[] ginterfaces = genericInterface.getGenericInterfaces();
			for( Type ginterface : ginterfaces ) {
				if( ginterface instanceof ParameterizedType ){
					ParameterizedType parameterizedType = (ParameterizedType) ginterface;
					Class<?> rawType = (Class<?>)parameterizedType.getRawType();
					if( ! interfacesAlreadyAnalyzed.add(rawType) ) {
						continue;
					}
					TypeVariable<?>[] rawTypeTypeParameters = rawType.getTypeParameters();
					Type[] typeArguments = parameterizedType.getActualTypeArguments();
					for(int i=0; i<rawTypeTypeParameters.length; i++){
						TypeVariable<?> rawTypeTypeParameter = rawTypeTypeParameters[i];
						Type typeArgument = typeArguments[i];
						Type typeVariableValue = genericTypeValues.get(typeArgument);
						if( typeVariableValue == null ){
							genericTypeValues.put(rawTypeTypeParameter, typeArgument);
						}
						else {
							genericTypeValues.put(rawTypeTypeParameter, typeVariableValue);
						}
					}
					genericInterfaces.push(rawType);
				}
				else if( ginterface instanceof Class<?> ){
					genericInterfaces.push((Class<?>)ginterface);
				}
				else {
					final String msg =
						"SuperGenericInterface is not a class: " +
						ginterface + "(" + ginterface.getClass() + ")";
					throw new RuntimeException(msg);
				}
			}
		}
	}

	/**
	 * The class associated with the current {@link GenericClass}.
	 */
	private Class<?> cl;

	/**
	 * Map from generic types to their instantiated values.
	 *
	 * @author Jonathan Labejof <Jonathan.Labejof@inria.fr>
	 * @since 2.2.5
	 */
	private Map<Type,Type> genericTypeValues = new HashMap<>();

	/**
	 * Return the string representations of the type variables declared by the
	 * current type.
	 *
	 * @since 2.1.1
	 */
	public String[] getTypeParameters() {
		TypeVariable<?>[] tvs = cl.getTypeParameters();
		String[] rets = new String[tvs.length];
		for (int i = 0; i < tvs.length; i++) {
			StringBuilder sb = new StringBuilder();
			String s = tvs[i].toString();
			sb.append(s);
			Type[] bounds = tvs[i].getBounds();
			if( ! (bounds.length==0 || bounds[0].equals(Object.class) ) ) {
				sb.append(" extends ");
				for (int j = 0; j < bounds.length; j++) {
					if(j!=0) sb.append(" & ");
					Type bound = bounds[j];
					s = toString(bound);
					sb.append(s);
				}
			}
			rets[i] = sb.toString();
		}
		return rets;
	}

	/**
	 * @since 2.2.6
	 */
	@Override
	public boolean equals( Object other ) {
		if( ! (other instanceof GenericClass) ) {
			return false;
		}
		String name = toString(cl);
		String othername = ((GenericClass)other).toString(((GenericClass)other).cl);
		return name.equals(othername);
	}

	@Override
	public String toString() {
		return toString(cl);
	}

	/**
	 * <p>
	 * Return a string representation of the specified type. Resolve generic
	 * types. Return a source code compliant version for arrays and inner types.
	 * </p>
	 *
	 * <p>
	 * This method is a replacement for {@link Class#getName()} that, for array
	 * types, return something like <code>Ljava.lang.Object;</code> instead of
	 * <code>Object[]</code>, and for inner types, return something like
	 * <code>Outter$Inner</code> instead of <code>Outter.Inner</code>.
	 * </p>
	 *
	 * @since 2.1.1
	 * @see ClassHelper#getName(Class)
	 */
	public String toString( Type type ) {

		type = resolveGenericType(type);

		if( type instanceof Class<?> ) {
			Class<?> cl = (Class<?>) type;
			if( cl.isArray() ) {
				Class<?> element = cl.getComponentType();
				element = (Class<?>) resolveGenericType(element);
				String s = toString(element)+"[]";
				return s;
			}
			else {
				String name = cl.getName();
				if( name.indexOf('$') != -1 ) {
					// Inner types
					name = name.replace('$','.');
				}
				return name;
			}
		}
		else {
			// Generic type
			if( type instanceof ParameterizedType ) {
				ParameterizedType parameterizedType = (ParameterizedType) type;
				Type rawType = parameterizedType.getRawType();
				String s = toString(rawType);
				StringBuilder sb = new StringBuilder(s);
				Type[] actualTypeArguments =
					parameterizedType.getActualTypeArguments();
				for(int i=0; i<actualTypeArguments.length; i++){
					sb.append( i==0 ? '<' : ',' );
					s = toString(actualTypeArguments[i]);
					sb.append(s);
				}
				if( actualTypeArguments.length > 0 ){
					sb.append('>');
				}
				return sb.toString();
			}
			else {
				return type.toString();
			}
		}
	}

	/**
	 * Return the string representations of the generic exception types for the
	 * specified method.
	 *
	 * @since 2.1
	 */
	public String[] getGenericExceptionTypes( Method method ) {
		Type[] throwns = method.getGenericExceptionTypes();
		String[] gets = new String[ throwns.length ];
		for (int i = 0; i < throwns.length; i++) {
			String typename = toString(throwns[i]);
			gets[i] = typename;
		}
		return gets;
	}

	/**
	 * Return a string representation of the generic return type for the
	 * specified method.
	 *
	 * @since 2.1
	 */
	public String getGenericReturnType( Method method ) {
		Type type = method.getGenericReturnType();
		String typename = toString(type);
		return typename;
	}

	/**
	 * Return a string representation of the parameters for the specified
	 * method. Each element in the returned array contains the parameter type
	 * followed by the space character, followed by the type name.
	 */
	public String[] getParameters( Method method ) {

		Type[] gpts = method.getGenericParameterTypes();
		String[] params = new String[ gpts.length ];

		for (int i = 0; i < gpts.length; i++) {
			String typename = toString(gpts[i]);
			StringBuilder sb = new StringBuilder(typename);
			sb.append(" arg");
			sb.append(i);
			params[i] = sb.toString();
		}

		return params;
	}

	/**
	 * Return the string representations of the type variables declared by the
	 * specified method. Return an empty array if the specified method does not
	 * declare any type variable.
	 *
	 * @since 2.1
	 */
	public String[] getTypeParameters( Method method ) {

		TypeVariable<Method>[] tvs = method.getTypeParameters();
		if( tvs.length == 0 ) {
			return new String[0];
		}

		String[] tps = new String[ tvs.length ];
		for (int i = 0; i < tvs.length; i++) {

			TypeVariable<Method> tv = tvs[i];
			Type[] bounds = tv.getBounds();
			StringBuilder sb = new StringBuilder( tv.getName() );

			if( bounds.length!=1 || ! Object.class.equals(bounds[0]) ) {
				sb.append(" extends ");
				for (int j = 0; j < bounds.length; j++) {
					if(j!=0) sb.append(" & ");
					String s = toString(bounds[j]);
					sb.append(s);
				}
			}
			tps[i] = sb.toString();
		}

		return tps;
	}


	// --------------------------------------------------------------------
	// Implementation specific
	// --------------------------------------------------------------------

	/**
	 * If the specified parameter is generic, this method returns its
	 * instantiated value if one has been declared. Else this method returns the
	 * specified parameter.
	 *
	 * @author Jonathan Labejof <Jonathan.Labejof@inria.fr>
	 * @since 2.2.5
	 */
	private final Type resolveGenericType( final Type type ) {
		Type result = type;
		if( type instanceof TypeVariable<?> &&
			genericTypeValues.containsKey((TypeVariable<?>)type) ) {
				result = genericTypeValues.get(type);
		}
		return result;
	}
}
