/***
 * Juliac
 * Copyright (C) 2010-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.commons.lang.reflect;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Field;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Class for testing the functionalities of {@link FieldHelper}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2.6
 */
public class FieldHelperTestCase {

	private Field srccontext, targetcontext, targetctx;

	@BeforeEach
	public void setUp() throws NoSuchFieldException {
		srccontext = Src.class.getDeclaredField("context");
		targetcontext = Target.class.getDeclaredField("context");
		targetctx = Target.class.getDeclaredField("ctx");
	}

	@Test
	public void overrideField() {
		assertEquals(true,FieldHelper.override(targetcontext,srccontext));
		assertEquals(false,FieldHelper.override(srccontext,targetcontext));
		assertEquals(false,FieldHelper.override(targetctx,srccontext));
	}

	@Test
	public void removeOverridenFields() {
		Field[] fields = new Field[]{srccontext,targetcontext,targetctx};
		Field[] actuals = FieldHelper.removeOverridden(fields);
		Field[] expecteds = new Field[]{targetcontext,targetctx};
		assertArrayEquals(expecteds,actuals);
	}

	@SuppressWarnings("unused")
	private static class Src {
		protected String context;
		public void init() throws RuntimeException {}
		public boolean not( String s, FieldHelperTestCase utc ) { return false; }
	}

	@SuppressWarnings("unused")
	private static class Target extends Src {
		protected Object context;
		protected String ctx;
		@Override
		public void init() {}
		public boolean not( String s ) { return false; }
	}
}
