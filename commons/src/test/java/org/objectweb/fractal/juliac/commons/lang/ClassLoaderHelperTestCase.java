/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.commons.lang;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Class for testing the functionalities of {@link ClassLoaderHelper}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class ClassLoaderHelperTestCase {

	@Test
	public void testGetClassPathEntries() throws IOException {

		Path tmp1 = Files.createTempDirectory("testGetClassPathEntries");
		Path tmp2 = Files.createTempDirectory("testGetClassPathEntries");
		String name1 = tmp1.toString();
		String name2 = tmp2.toString();

		ClassLoader classLoader =
			new ClassLoader(
				new URLClassLoader(
					new URL[]{new URL("file://"+name1),new URL("file://"+name2)},
					new ClassLoader(
						new URLClassLoader(new URL[]{new URL("file://"+name1)},null)
					){}
				)
			){};

		List<File> cpes = ClassLoaderHelper.getClassPathEntries(classLoader);

		assertEquals(3,cpes.size());
		assertTrue(cpes.get(0).equals(new File(name1)));
		assertTrue(cpes.get(1).equals(new File(name2)));
		assertTrue(cpes.get(2).equals(new File(name1)));
	}
}
