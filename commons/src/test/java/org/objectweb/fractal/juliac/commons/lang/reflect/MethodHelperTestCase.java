/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.commons.lang.reflect;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Method;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Class for testing the functionalities of the {@link MethodHelper} class.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class MethodHelperTestCase {

	private Method srcinit, srcnot, targetinit, targetnot;

	@BeforeEach
	public void setUp() throws NoSuchMethodException {

		srcinit = Src.class.getMethod("init");
		srcnot = Src.class.getMethod("not",String.class,MethodHelperTestCase.class);
		targetinit = Target.class.getMethod("init");
		targetnot = Target.class.getMethod("not",String.class);
	}

	@Test
	public void getShortSignature() {
		assertEquals("init()",MethodHelper.getShortSignature(srcinit));
		assertEquals("init()",MethodHelper.getShortSignature(targetinit));
	}

	@Test
	public void sameSignature() {
		assertEquals(true,MethodHelper.sameSignature(srcinit,targetinit));
	}

	@Test
	public void overrideMethod() {
		assertEquals(true,MethodHelper.override(targetinit,srcinit));
		assertEquals(false,MethodHelper.override(srcinit,targetinit));
		assertEquals(false,MethodHelper.override(targetnot,srcnot));
	}

	@Test
	public void removeOverridenMethods() {
		Method[] methods = new Method[]{srcinit,targetinit,srcnot,targetnot};
		Method[] actuals = MethodHelper.removeOverridden(methods);
		Method[] expecteds = new Method[]{targetinit,srcnot,targetnot};
		assertArrayEquals(expecteds,actuals);
	}

	@SuppressWarnings("unused")
	private static class Src {
		protected String context;
		public void init() throws RuntimeException {}
		public boolean not( String s, MethodHelperTestCase utc ) { return false; }
	}

	@SuppressWarnings("unused")
	private static class Target extends Src {
		protected Object context;
		protected String ctx;
		@Override
		public void init() {}
		public boolean not( String s ) { return false; }
	}
}
