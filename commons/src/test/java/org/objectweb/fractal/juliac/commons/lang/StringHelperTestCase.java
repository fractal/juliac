/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.commons.lang;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Class for testing the functionalities of {@link StringHelper}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2
 */
public class StringHelperTestCase {

	@Test
	public void testAround() {
		assertArrayEquals( new String[0], StringHelper.around("noslash",'/'));
		assertArrayEquals( new String[]{"before","after"}, StringHelper.around("before/after",'/'));
		assertArrayEquals( new String[]{"before","//after"}, StringHelper.around("before///after",'/'));
		assertArrayEquals( new String[]{"","//after"}, StringHelper.around("///after",'/'));
		assertArrayEquals( new String[]{"before",""}, StringHelper.around("before/",'/'));
	}

	@Test
	public void testTrimHeadingAndTrailingBlank() {
		String src = "  hello\t\n ";
		String res = StringHelper.trimHeadingAndTrailingBlanks(src);
		assertEquals("hello",res);
	}

	@Test
	public void testTrimBlankInTheMiddle() {
		String src = "  hello world\t \nfoo bar\t\n ";
		String res = StringHelper.trimHeadingAndTrailingBlanks(src);
		assertEquals("hello world\t \nfoo bar",res);
	}

	@Test
	public void testTrimEmpty() {
		String src = "";
		String res = StringHelper.trimHeadingAndTrailingBlanks(src);
		assertEquals("",res);
	}

	@Test
	public void testTrimOnlyBlanks() {
		String src = " \n\t \n";
		String res = StringHelper.trimHeadingAndTrailingBlanks(src);
		assertEquals("",res);
	}
}
