/***
 * Juliac
 * Copyright (C) 2010-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.commons.lang;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Class for testing the functionalities of {@link ClassHelper}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2.5
 */
public class ClassHelperTestCase {

	@Test
	public void testGetTypeParameterNames() {
		String[] tps = ClassHelper.getTypeParameterNames(C.class);
		String[] expecteds = new String[]{"T","S","R"};
		assertArrayEquals(expecteds,tps);
	}

	final private static class
		C<T,S extends Serializable & Cloneable,R extends Comparable<? super T>> {
	}

	@Test
	public void testGetAllImplementedInterfaces() {
		List<Class<?>> implementeds = new ArrayList<Class<?>>();
		ClassHelper.addAllImplementedInterfaces(C1.class,implementeds);
		assertEquals(5,implementeds.size());
		assertEquals(I1.class,implementeds.get(0));
		assertEquals(I2.class,implementeds.get(1));
		assertEquals(I2.class,implementeds.get(2));
		assertEquals(I3.class,implementeds.get(3));
		assertEquals(I4.class,implementeds.get(4));
	}

	class C1 extends C2 implements I1,I2 {}
	class C2 extends C3 implements I2,I3,I4 {}
	class C3 extends C4 {}
	class C4 {}
	interface I1 {}
	interface I2 {}
	interface I3 {}
	interface I4 {}
}
