/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.commons.io;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Class for testing the functionalities of {@link Console}s.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class ConsoleTestCase {

	private Console console;

	@BeforeEach
	public void setUp() {
		console = Console.getConsole("test");
	}

	@AfterEach
	public void tearDown() {
		console.close();
	}

	@Test
	public void testEqualsOneLine() {
		final String expected = "Hello World!";
		console.print(expected);
		console.assertEquals(new String[]{expected});
	}

	@Test
	public void testEqualsSeveralLines() {
		final String expected0 = "Hello ";
		final String expected1 = "World!";
		console.println(expected0);
		console.println(expected1);
		console.assertEquals(new String[]{expected0,expected1});
	}

	@Test
	public void testDiffersOneLine() {
		console.print('a');
		assertThrows(
			IllegalArgumentException.class,
			() -> console.assertEquals(new String[]{"b"}));
	}

	@Test
	public void testDiffersMoreThanExpected() {
		console.println("ab");
		console.println("cd");
		assertThrows(
			IllegalArgumentException.class,
			() -> console.assertEquals(new String[]{"ab","c"}));
	}

	@Test
	public void testDiffersFewerThanExpected() {
		console.println("ab");
		console.println("c");
		assertThrows(
			IllegalArgumentException.class,
			() -> console.assertEquals(new String[]{"ab","cd"}));
	}
}
