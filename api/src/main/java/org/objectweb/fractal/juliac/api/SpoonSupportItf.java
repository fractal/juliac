/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Frederic Loiret
 */

package org.objectweb.fractal.juliac.api;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

/**
 * This interface describes the services provided by Spoon to Juliac.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Frederic Loiret <Frederic.Loiret@inria.fr>
 * @since 1.3
 */
public interface SpoonSupportItf extends JuliacModuleItf {

	/**
	 * Mix the layers whose class names are specified and generate the resulting
	 * mixed class.
	 *
	 * @param targetClassName  the name of the generated mixed class
	 * @param mixins           the class names of the layers
	 */
	public void mixAndGenerate( String targetClassName, List<String> mixins )
	throws IOException;

	/**
	 * Mix the specified layers and write the resulting mixed class in the
	 * specified writer.
	 *
	 * @param targetClassName  the name of the generated mixed class
	 * @param mixins       the class names of the layers
	 * @param writer       the writer where the generated mixed class is dumped
	 * @since 2.7
	 */
	public void mix(
		String targetClassName, List<String> mixins, Writer writer )
	throws IOException;

	/**
	 * Return the Spoon factory. This method has been added to meet a
	 * requirement from Hulotte that provides its own custom Spoon factory.
	 *
	 * @author Frederic Loiret <Frederic.Loiret@inria.fr>
	 * @since 2.2.2
	 */
	public <T> T getSpoonFactory();

	/**
	 * Define the class loader that Spoon must use.
	 *
	 * This method has been added for Adlet when a project that uses
	 * MixinProcessor is invoked from maven-compiler-plugin. The issue does not
	 * arise with Eclipse. So far this method is used only by Adlet.
	 *
	 * When building the code model for julia-mixins in MixinProcessor, Spoon
	 * complains that it cannot load classes from julia-runtime. These classes
	 * can be loaded from the class loader getClass().getClassLoader().
	 * MixinProcessor sets Juliac to use this class loader.
	 *
	 * @param classLoader  the class loader
	 * @see MixinProcessor#init
	 * @since 2.7
	 */
	public void setSpoonInputClassLoader( ClassLoader classLoader );

	/**
	 * Dump the specified code element in its corresponding source file. The
	 * code element is kept as an object although a CtType is expected. The
	 * rationale is that we do not want to depend from Spoon in the juliac-core
	 * module. A runtime type check is performed in the implementation that is
	 * defined in the juliac-spoon module.
	 *
	 * @param element  the code element to dump
	 * @throws IllegalArgumentException
	 *                 if the code element is not an instance of CtType
	 *
	 * @since 2.5
	 */
	public void generateSourceCode( Object element ) throws IOException;
}
