/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.api.visit;

/**
 * A visitor for a block of source code.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 */
public interface BlockSourceCodeVisitor {

	/**
	 * Visit a piece of the source code of the block.
	 *
	 * @param code  the visited piece of code
	 * @deprecated 2.3
	 */
	@Deprecated(since="2.3")
	void visit( Object code );

	/**
	 * Visit a line of the source code of the block.
	 *
	 * @param code  the visited piece of code
	 * @return      the source code of the block currently generated
	 * @deprecated 2.3
	 */
	@Deprecated(since="2.3")
	BlockSourceCodeVisitor visitln( Object... code );

	/**
	 * Visit a block comment (single line comment).
	 *
	 * @return  the source code of the block currently generated
	 * @since 2.3
	 */
	BlockSourceCodeVisitor visitComment(String comment);

	/**
	 * Visit the begining of the block.
	 *
	 * @return  the source code of the block currently generated
	 */
	BlockSourceCodeVisitor visitBegin();

	/**
	 * Visit a source code instruction of the block.
	 *
	 * @param code  the visited instruction
	 * @return      the source code of the block currently generated
	 * @since 2.3
	 */
	BlockSourceCodeVisitor visitIns( Object... code );

	/**
	 * Visit a source code variable of the block.
	 *
	 * @param code  the visited variable declaration
	 * @return      the source code of the block currently generated
	 */
	BlockSourceCodeVisitor visitVar( Object type, String name, Object... code );

	/**
	 * Visit a source code assignment of the block.
	 *
	 * @param code  the visited variable assignment
	 * @return      the source code of the block currently generated
	 * @since 2.3
	 */
	BlockSourceCodeVisitor visitSet( String name, Object... code );

	/**
	 * Visit a source code if instruction in the block.
	 *
	 * @param condition  the branching condition
	 * @return           the source code of the then block to be generated
	 * @since 2.3
	 */
	ThenSourceCodeVisitor visitIf(Object... condition);

	/**
	 * Visit a source code while loop in the block.
	 *
	 * @param condition  the loop stop condition
	 * @return           the source code of the while block to be generated
	 * @since 2.3
	 */
	BlockSourceCodeVisitor visitWhile(Object... condition);

	/**
	 * Visit a source code for loop in the block.
	 *
	 * @param condition  the loop stop condition
	 * @return           the source code of the for block to be generated
	 * @since 2.3
	 */
	BlockSourceCodeVisitor visitFor(Object... condition);

	/**
	 * Visit a source code try/catch block.
	 *
	 * @return  the source code of the try block to be generated
	 * @since 2.3
	 */
	CatchSourceCodeVisitor visitTry();

	/**
	 * Visit a source code synchronized block.
	 *
	 * @param obj  the synchronized object
	 * @return     the source code of the try block to be generated
	 * @since 2.3
	 */
	BlockSourceCodeVisitor visitSync(String obj);

	/**
	 * Visit the end of the block.
	 */
	void visitEnd();
}
