/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.api.visit;

/**
 * A visitor for the source code of a class.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 */
public interface ClassSourceCodeVisitor {

	/**
	 * Visit a comment.
	 *
	 * @param comment  the comment string
	 * @since 2.7
	 */
	public void visitComment( String comment );

	/**
	 * Visit the declaration of the class.
	 *
	 * @param modifiers                  access modifiers
	 * @param name                       class name
	 * @param genericTypeParameters      generic parameters
	 * @param superClassName             name of the super class
	 * @param implementedInterfaceNames  implemented interface names
	 */
	public void visit(
		int modifiers, String name, String[] genericTypeParameters,
		String superClassName, String[] implementedInterfaceNames );

	/**
	 * Visit a static block of code.
	 *
	 * @return  a visitor for visiting the static block of code.
	 */
	public BlockSourceCodeVisitor visitStaticPart();

	/**
	 * Visit a field.
	 *
	 * @param modifiers   access modifiers
	 * @param type        type
	 * @param name        name
	 * @param expression  initialization expression
	 */
	public void visitField( int modifiers, String type, String name, String expression );

	/**
	 * Visit a constructor.
	 *
	 * @param modifiers              access modifiers
	 * @param genericTypeParameters  generic parameters
	 * @param parameters             parameters
	 * @param exceptions             thrown types
	 * @return  a visitor for visiting the source code of a constructor
	 */
	public BlockSourceCodeVisitor visitConstructor(
		int modifiers, String[] genericTypeParameters, String[] parameters,
		String[] exceptions );

	/**
	 * Visit a method.
	 *
	 * @param modifiers       access modifiers
	 * @param typeParameters  generic type parameters
	 * @param returnType      return type
	 * @param name            name
	 * @param parameters      parameters
	 * @param exceptions      thrown types
	 * @return  a visitor for visiting the source code of a method
	 */
	public BlockSourceCodeVisitor visitMethod(
		int modifiers, String[] typeParameters, String returnType,
		String name, String[] parameters, String[] exceptions );

	/**
	 * Visit an inner class.
	 *
	 * @return  a visitor for visiting an inner class.
	 */
	public ClassSourceCodeVisitor visitInnerClass();

	/**
	 * Visit the end of the class source code.
	 */
	public void visitEnd();
}
