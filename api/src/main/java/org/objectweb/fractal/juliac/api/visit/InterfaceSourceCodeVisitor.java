/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.api.visit;

/**
 * A visitor for the source code of an interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public interface InterfaceSourceCodeVisitor {

	/**
	 * Visit the declaration of the interface.
	 *
	 * @param modifiers               access modifiers
	 * @param name                    interface name
	 * @param genericTypeParameters   generic parameters
	 * @param extendedInterfaceNames  extended interface names
	 */
	public void visit(
		int modifiers, String name, String[] genericTypeParameters,
		String[] extendedInterfaceNames );

	/**
	 * Visit a field.
	 *
	 * @param type        field type
	 * @param name        field name
	 * @param expression  field initialization expression
	 */
	public void visitField( String type, String name, String expression );

	/**
	 * Visit a method.
	 *
	 * @param typeParameters  generic type parameters
	 * @param returnType      return type
	 * @param name            name
	 * @param parameters      parameters
	 * @param exceptions      thrown types
	 */
	public void visitMethod(
		String[] typeParameters, String returnType, String name,
		String[] parameters, String[] exceptions );

	/**
	 * Visit the end of the interface source code.
	 */
	public void visitEnd();
}
