/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.api.desc;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.generator.InterfaceTypeConfigurableItf;

/**
 * A struct-like class for holding data about a control membrane.
 *
 * @param <T>  the type of the controller descriptor
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public abstract class MembraneDesc<T> {

	protected JuliacItf jc;
	protected String name;
	protected String descriptor;
	private InterfaceType[] ctrlItfTypes;
	protected T ctrlDescs;
	protected InterfaceTypeConfigurableItf interceptorClassGenerator;
	protected InterfaceTypeConfigurableItf[] interceptorSourceCodeGenerators;

	/**
	 * Create a new membrane descriptor.
	 *
	 * @param jc            the instance of Juliac
	 * @param name          the membrane name (only relevant for component-based membranes)
	 * @param descriptor    the membrane descriptor (e.g. primitive)
	 * @param ctrlItfTypes  the control interface types
	 * @param ctrlDescs     the structure of the membrane
	 * @param icg           the interceptor class generator
	 * @param iscgs         the interceptor source code generators
	 */
	public MembraneDesc(
		JuliacItf jc, String name,
		String descriptor, InterfaceType[] ctrlItfTypes, T ctrlDescs,
		InterfaceTypeConfigurableItf interceptorClassGenerator,
		InterfaceTypeConfigurableItf[] interceptorSourceCodeGenerators ) {

		this.jc = jc;
		this.name = name;
		this.descriptor = descriptor;
		this.ctrlItfTypes = ctrlItfTypes;
		this.ctrlDescs = ctrlDescs;
		this.interceptorClassGenerator = interceptorClassGenerator;
		this.interceptorSourceCodeGenerators = interceptorSourceCodeGenerators;
	}

	public String getName() {
		return name;
	}

	/**
	 * Return the Julia membrane descriptor (e.g. primitive).
	 */
	public String getDescriptor() {
		return descriptor;
	}

	/**
	 * Return the array of control interface types implemented by this membrane.
	 */
	public InterfaceType[] getCtrlItfTypes() {
		return ctrlItfTypes;
	}

	/**
	 * Return the list of controller descriptors associated with this membrane.
	 */
	public T getCtrlDescs() {
		return ctrlDescs;
	}

	/**
	 * Return the interceptor class generator associated with this membrane.
	 */
	public InterfaceTypeConfigurableItf getInterceptorClassGenerator() {
		return interceptorClassGenerator;
	}

	/**
	 * Return the interceptor source code generators associated with this
	 * membrane.
	 */
	public InterfaceTypeConfigurableItf[] getInterceptorSourceCodeGenerators() {
		return interceptorSourceCodeGenerators;
	}

	/**
	 * Return the implementation class of the controller implementing the
	 * specified signature.
	 *
	 * @param signature  the signature
	 * @return           the class of the controller
	 * @throws IllegalArgumentException  if there is no such controller
	 * @since 2.2
	 */
	public abstract Class<?> getCtrlImpl( String signature );

	@Override
	public String toString() {
		return descriptor;
	}
}
