/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.api.generator;

import org.objectweb.fractal.juliac.api.visit.FileSourceCodeVisitor;

/**
 * Interface for type generators.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public interface TypeGeneratorItf extends SourceCodeGeneratorItf {

	/**
	 * Generate the file headers.
	 */
	public void generateFileHeaders( FileSourceCodeVisitor fv );

	/**
	 * Return the name of the generated package.
	 */
	public String getPackageName();

	/**
	 * Generate the import statements.
	 */
	public void generateImports( FileSourceCodeVisitor fv );

	/**
	 * Return the modifiers for the generated type.
	 */
	public int getTypeModifiers();

	/**
	 * Return the simple name of the generated type.
	 */
	public String getSimpleTypeName();

	/**
	 * Return the generic type parameters of the generated type.
	 */
	public String[] getGenericTypeParameters();
}
