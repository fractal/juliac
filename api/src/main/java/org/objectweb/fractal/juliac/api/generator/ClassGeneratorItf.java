/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.api.generator;

import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;

/**
 * Interface for class generators.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public interface ClassGeneratorItf extends TypeGeneratorItf {

	public void generate( ClassSourceCodeVisitor cv );

	public String getSuperClassName();
	public String[] getImplementedInterfaceNames();

	public void generateStaticParts( ClassSourceCodeVisitor cv );
	public void generateFields( ClassSourceCodeVisitor cv );
	public void generateConstructors( ClassSourceCodeVisitor cv );
	public void generateMethods( ClassSourceCodeVisitor cv );
}
