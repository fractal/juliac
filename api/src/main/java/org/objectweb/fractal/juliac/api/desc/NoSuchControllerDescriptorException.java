/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.api.desc;

import org.objectweb.fractal.juliac.api.JuliacRuntimeException;

/**
 * Exception thrown whenever a controller descriptor can not be found in the
 * Julia configuration files transmitted to Juliac.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.2
 */
public class NoSuchControllerDescriptorException extends JuliacRuntimeException {

	private static final long serialVersionUID = -8367270039242541890L;

	/**
	 * @since 2.3
	 */
	public NoSuchControllerDescriptorException( String msg ) {
		super(msg);
	}
}
