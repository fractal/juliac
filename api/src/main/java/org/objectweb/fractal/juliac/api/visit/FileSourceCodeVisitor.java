/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.api.visit;

/**
 * A visitor for a file which contains some Java source code.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.1
 */
public interface FileSourceCodeVisitor {

	/**
	 * Visit a comment.
	 *
	 * @param comment  the comment string
	 * @since 2.7
	 */
	public void visitComment( String comment );

	/**
	 * Visit the package name.
	 *
	 * @param name  the package name
	 */
	public void visitPackageName( String name );

	/**
	 * Visit an import clause.
	 *
	 * @param name  the imported type
	 */
	public void visitImport( String name );

	/**
	 * Visit a public class defined in this file.
	 *
	 * @return  a visitor for visiting the source code of the public class
	 */
	public ClassSourceCodeVisitor visitPublicClass();

	/**
	 * Visit a public interface defined in this file.
	 *
	 * @return  a visitor for visiting the source code of the public interface
	 */
	public InterfaceSourceCodeVisitor visitPublicInterface();
}
