/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.opt;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;

import org.junit.jupiter.api.Test;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.FileSourceCodeVisitor;
import org.objectweb.fractal.juliac.commons.io.Console;
import org.objectweb.fractal.juliac.core.visit.FileSourceCodeWriter;

/**
 * Class for testing the functionalities of the {@link ClassGenerator} class.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public class ClassGeneratorHelperTestCase {

	/**
	 * Test that static blocks are generated after static fields. This is
	 * needed when static blocks refer to static fields.
	 */
	@Test
	public void testStaticBlocks() throws IOException {

		CharArrayWriter caw = new CharArrayWriter();
		PrintWriter pw = new PrintWriter(caw);
		FileSourceCodeVisitor fv = new FileSourceCodeWriter(pw);
		SourceCodeGeneratorItf cg = new StaticBlockClassGenerator();
		cg.generate(fv);
		pw.close();
		caw.close();

		Console console = Console.getConsole("static-block-class-generator-");
		char[] cars = caw.toCharArray();
		for (char c : cars) {
			console.print(c);
		}
		console.close();
		console.assertEquals(expecteds);
	}

	private static class StaticBlockClassGenerator extends ClassGenerator {

		@Override
		public String getTargetTypeName() {
			return "ClassWithStaticBlocksAndFields";
		}

		@Override
		public void generateFileHeaders( FileSourceCodeVisitor fv ) {}

		@Override
		public void generateStaticParts( ClassSourceCodeVisitor cv ) {
			BlockSourceCodeVisitor bv = cv.visitStaticPart();
			bv.visitIns("System.out.println(var)");
			bv.visitEnd();
		}

		@Override
		public void generateFields( ClassSourceCodeVisitor cv ) {
			cv.visitField(Modifier.STATIC, "int", "var", null);
		}
	}

	final private static String[] expecteds =
		new String[]{
			"public class ClassWithStaticBlocksAndFields {",
			"",
			"  static int var;",
			"  static  {",
			"    System.out.println(var);",
			"  }",
			"",
			"}",
		};
}
