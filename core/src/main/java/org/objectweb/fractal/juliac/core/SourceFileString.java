/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core;

import java.io.File;
import java.io.IOException;

/**
 * Implementation for {@link SourceFile}s stored in a String.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2.1
 */
public class SourceFileString extends SourceFile {

	public SourceFileString( String qname, String content, String ext ) {
		super(new File(".").toURI(),qname,ext);
		this.content = content;
	}

	@Override
	public CharSequence getCharContent( boolean ignoreEncodingErrors ) throws IOException {
		return content;
	}

	private CharSequence content;
}
