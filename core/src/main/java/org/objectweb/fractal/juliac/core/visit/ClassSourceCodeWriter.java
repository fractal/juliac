/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.core.visit;

import static com.google.common.base.Preconditions.checkState;

import java.io.PrintWriter;
import java.lang.reflect.Modifier;

import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;

import com.google.common.base.Joiner;

/**
 * A {@link ClassSourceCodeVisitor} that generates the source code of a class.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 */
public class ClassSourceCodeWriter extends AbstractCodeWriter
implements ClassSourceCodeVisitor {

	private String className;

	public ClassSourceCodeWriter( PrintWriter pw ) {
		super(pw);
	}

	public ClassSourceCodeWriter( PrintWriter pw, int level ) {
		super(pw,level);
	}

	@Override
	public void visitComment( String comment ) {
		writeln(comment);
	}

	@Override
	public void visit(
		int modifiers, String name, String[] genericTypeParameters,
		String superClassName, String[] implementedInterfaceNames ) {

		if( state < DECLARATION_STATE ) { state = DECLARATION_STATE; }
		checkState(
			state == DECLARATION_STATE,
			"Class declaration cannot be visited at this stage of the visit" );

		this.className = name;

		write(
			Modifier.toString(modifiers),
			"class", name + generics(genericTypeParameters) );
		if( superClassName != null ) {
			writeln();
			write("extends",superClassName);
		}
		if( implementedInterfaceNames != null ) {
			writeln();
			write("implements",Joiner.on(",").join(implementedInterfaceNames));
		}
		writeln(" {");
		writeln();
		indent++;

		state = BODY_STATE;
	}

	@Override
	public BlockSourceCodeVisitor visitStaticPart() {

		checkState(
			state == BODY_STATE,
			"Class body cannot be visited at this stage of the visit" );

		write("static");

		return new BlockSourceCodeWriter(writer(),indent,false).visitBegin();
	}

	@Override
	public void visitField(
		int modifiers, String type, String name, String expression ) {

		checkState(
			state == BODY_STATE,
			"Class body cannot be visited at this stage of the visit" );

		String value = (expression != null) ? " = "+expression : "";
		writeln(Modifier.toString(modifiers),type,name+value+";");
	}

	@Override
	public BlockSourceCodeVisitor visitConstructor(
		int modifiers, String[] genericTypeParameters, String[] parameters,
		String[] exceptions ) {

		checkState(
			state == BODY_STATE,
			"Class body cannot be visited at this stage of the visit" );

		write(
			Modifier.toString(modifiers),
			generics(genericTypeParameters) + className +
			parameters(parameters) + exceptions(exceptions) );
		this.indented = false;

		return new BlockSourceCodeWriter(writer(),indent,false).visitBegin();
	}

	@Override
	public BlockSourceCodeVisitor visitMethod(
		int modifiers, String[] typeParameters, String returnType,
		String name, String[] parameters, String[] exceptions ) {

		checkState(
			state == BODY_STATE,
			"Class method cannot be visited at this stage of the visit" );

		write(
			Modifier.toString(modifiers),
			generics(typeParameters) + returnType,
			name + parameters(parameters) + exceptions(exceptions) );

		BlockSourceCodeVisitor bv =
			Modifier.isAbstract(modifiers) ?
			new AbstractSourceCodeWriter(writer()) :
			new BlockSourceCodeWriter(writer(),indent,false);
		this.indented = false;

		return bv.visitBegin();
	}

	@Override
	public ClassSourceCodeVisitor visitInnerClass() {

		checkState(
			state == BODY_STATE,
			"Class body cannot be visited at this stage of the visit" );

		return new ClassSourceCodeWriter(writer(),indent);
	}

	@Override
	public void visitEnd() {

		if( state==DECLARATION_STATE || state==BODY_STATE) { state = END_STATE; }
		checkState(
			state == END_STATE,
			"Class end cannot be visited at this stage of the visit" );

		indent--;
		writeln("}");

		state = FINAL_STATE;
	}


	// ----------------------------------------------------------------------
	// Implementation specific
	// ----------------------------------------------------------------------

	private final String generics( final String[] genericTypeParameters ) {
		if( genericTypeParameters!=null && genericTypeParameters.length!=0 ) {
			return "<"+Joiner.on(",").join(genericTypeParameters)+">";
		}
		return "";
	}

	private final String parameters( final String[] parameters ) {
		if( parameters!=null && parameters.length!=0 ) {
			return "("+Joiner.on(",").join(parameters)+")";
		}
		return "()";
	}

	private final String exceptions( final String[] exceptions ) {
		if( exceptions!=null && exceptions.length!=0 ) {
			return " throws "+Joiner.on(",").join(exceptions);
		}
		return "";
	}


	// ----------------------------------------------------------------------
	// Visitor state constants
	// ----------------------------------------------------------------------
	// TODO refactor to enum
	private int state = DECLARATION_STATE;
	private static final int DECLARATION_STATE = 1;
	private static final int BODY_STATE = 2;
	private static final int END_STATE = 3;
	private static final int FINAL_STATE = 4;
}
