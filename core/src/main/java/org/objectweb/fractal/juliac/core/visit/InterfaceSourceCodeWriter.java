/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.visit;

import static com.google.common.base.Preconditions.checkState;

import java.io.PrintWriter;
import java.lang.reflect.Modifier;

import org.objectweb.fractal.juliac.api.visit.InterfaceSourceCodeVisitor;

import com.google.common.base.Joiner;

/**
 * A {@link InterfaceSourceCodeVisitor} that generates the source code of an
 * interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public class InterfaceSourceCodeWriter extends AbstractCodeWriter
implements InterfaceSourceCodeVisitor {

	public InterfaceSourceCodeWriter( PrintWriter pw ) {
		super(pw);
	}

	public InterfaceSourceCodeWriter( PrintWriter pw, int level ) {
		super(pw,level);
	}

	@Override
	public void visit(
		int modifiers, String name, String[] genericTypeParameters,
		String[] extendedInterfaceNames ) {

		checkState(
			state.equals(States.DECLARATION_STATE),
			"Interface declaration cannot be visited at this stage of the visit" );

		writeln();
		write(Modifier.toString(modifiers),"interface",name+generics(genericTypeParameters));
		if( extendedInterfaceNames != null ) {
			writeln();
			write("extends",Joiner.on(",").join(extendedInterfaceNames));
		}
		writeln(" {");
		writeln();
		indent++;

		state = States.BODY_STATE;
	}

	@Override
	public void visitField( String type, String name, String expression ) {

		checkState(
			state.equals(States.BODY_STATE),
			"Interface body cannot be visited at this stage of the visit" );

		String value = (expression != null) ? "= "+expression : "";
		writeln("final static public",type,name+value+";");
	}

	@Override
	public void visitMethod(
		String[] typeParameters, String returnType, String name,
		String[] parameters, String[] exceptions ) {

		checkState(
			state == States.BODY_STATE,
			"Interace method cannot be visited at this stage of the visit" );

		writeln(
			generics(typeParameters) + returnType,
			name+parameters(parameters) + exceptions(exceptions)+";" );
		this.indented = false;
	}

	@Override
	public void visitEnd() {

		if( state.equals(States.DECLARATION_STATE) ||
			state.equals(States.BODY_STATE) ) {
			state = States.END_STATE;
		}
		checkState(
			state == States.END_STATE,
			"Interface end cannot be visited at this stage of the visit");

		indent--;
		writeln("}");

		state = States.FINAL_STATE;
	}


	// ----------------------------------------------------------------------
	// Implementation specific
	// ----------------------------------------------------------------------

	private final String generics( final String[] genericTypeParameters ) {
		if( genericTypeParameters!=null && genericTypeParameters.length!=0 ) {
			return "<"+Joiner.on(",").join(genericTypeParameters)+">";
		}
		return "";
	}

	private final String parameters( final String[] parameters ) {
		if( parameters!=null && parameters.length!=0 ) {
			return "("+Joiner.on(",").join(parameters)+")";
		}
		return "()";
	}

	private final String exceptions( final String[] exceptions ) {
		if( exceptions!=null && exceptions.length!=0 ) {
			return " throws "+Joiner.on(",").join(exceptions);
		}
		return "";
	}


	// ----------------------------------------------------------------------
	// Visitor state constants
	// ----------------------------------------------------------------------

	private static enum States {
		DECLARATION_STATE, BODY_STATE, END_STATE, FINAL_STATE }

	private States state = States.DECLARATION_STATE;
}
