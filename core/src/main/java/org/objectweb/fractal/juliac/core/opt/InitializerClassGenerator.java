/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.core.opt;

import java.lang.reflect.Modifier;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.Interceptor;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.InterfaceTypeConfigurableItf;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.CatchSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.core.Constants;
import org.objectweb.fractal.juliac.core.helper.ComponentTypeHelper;
import org.objectweb.fractal.juliac.core.helper.JuliacHelper;

/**
 * Root class for initializer generators.
 *
 * The generated classes are factories for components. These factories provide a
 * <code>newFcInstance</code> method for instantiating components.
 *
 * @param <M>  the type of the membrane descriptor
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 */
public class InitializerClassGenerator
extends ClassGenerator {

	protected JuliacItf jc;
	protected FCSourceCodeGeneratorItf fcscg;
	protected MembraneDesc<?> membraneDesc;
	protected ComponentType ct;
	protected Object contentDesc;
	protected Object source;

	/**
	 * @param jc            the Juliac instance
	 * @param fcscg         the component source code generator
	 * @param membraneDesc  the membrane descriptor
	 * @param ct            the component type
	 * @param contentDesc   the component content descriptor
	 * @param source        the code element for the component
	 */
	public InitializerClassGenerator(
		JuliacItf jc, FCSourceCodeGeneratorItf fcscg,
		MembraneDesc<?> membraneDesc, ComponentType ct, Object contentDesc,
		Object source ) {

		this.jc = jc;
		this.fcscg = fcscg;
		this.membraneDesc = membraneDesc;
		this.ct = ct;
		this.contentDesc = contentDesc;
		this.source = source;
	}

	@Override
	public String[] getImplementedInterfaceNames() {
		return new String[]{Constants.JULIAC_RUNTIME_FACTORY};
	}

	@Override
	public void generateMethods( ClassSourceCodeVisitor cv ) {
		generateGetFcInstanceTypeMethod(cv);
		generateGetFcControllerDescMethod(cv);
		generateGetFcContentDescMethod(cv);
		generateNewFcContentMethod(cv);
		generateNewFcInstanceMethod(cv);
		generateNewFcInstanceContentMethod(cv);
		generateExtraCode(cv);
	}

	@Override
	public String getTargetTypeName() {
		String ctrlDesc = membraneDesc.getDescriptor();
		String contentClassName = JuliacHelper.getContentClassName(ctrlDesc,contentDesc);
		String membraneClassName = getMembraneClassName(ctrlDesc,contentClassName);
		String hexhashct = ComponentTypeHelper.hexhash(ct);
		String initializerClassName = membraneClassName+"FC"+hexhashct;
		return initializerClassName;
	}

	/**
	 * Return the class name of the membrane associated to the specified
	 * descriptor.
	 *
	 * @throws IllegalArgumentException  if the specified descriptor is not a string
	 */
	public String getMembraneClassName( Object ctrlDesc, String contentClassName ) {

		if( !(ctrlDesc instanceof String) ) {
			final String msg = "ctrlDesc should be a String";
			throw new IllegalArgumentException(msg);
		}

		/*
		 * Handle the case where the controller descriptor contains a prefix
		 * such as /julia/. In this case replace slashes which would lead to
		 * illegal class names by underscores.
		 */
		String ctrlDescStr = ((String) ctrlDesc).replace('/', '_');

		String membraneClassName =
			contentClassName == null ?
			JuliacHelper.getJuliacGeneratedStrongTypeName(ctrlDescStr) :
			contentClassName+"FC"+ctrlDescStr;
		String pkgRoot = jc.getPkgRoot();
		String rootedMembraneClassName = pkgRoot + membraneClassName;

		return rootedMembraneClassName;
	}


	// -----------------------------------------------------------------------
	// Method generators
	// -----------------------------------------------------------------------

	protected void generateGetFcInstanceTypeMethod( ClassSourceCodeVisitor cv ) {
		BlockSourceCodeVisitor mv =
			cv.visitMethod(
				Modifier.PUBLIC, null, Type.class.getName(),
				"getFcInstanceType", null, null );
		generateGetFcInstanceTypeMethod(mv);
		mv.visitEnd();
	}
	protected void generateGetFcInstanceTypeMethod( BlockSourceCodeVisitor mv ) {
		CatchSourceCodeVisitor cv = mv.visitTry();
		cv.visitIns("return",ComponentTypeHelper.javaify(ct).toString());
		cv = cv.visitCatch(InstantiationException.class, "ie");
		cv.visitIns("throw","new",Constants.JULIAC_RUNTIME_EXCEPTION+"(ie)");
		cv.visitEnd();
	}

	protected void generateGetFcControllerDescMethod( ClassSourceCodeVisitor cv ) {
		BlockSourceCodeVisitor mv =
			cv.visitMethod(
				Modifier.PUBLIC, null, "Object", "getFcControllerDesc", null,
				null );
		generateGetFcControllerDescMethod(mv);
		mv.visitEnd();
	}
	protected void generateGetFcControllerDescMethod( BlockSourceCodeVisitor mv ) {
		mv.visitIns("throw","new","UnsupportedOperationException()");
	}

	protected void generateGetFcContentDescMethod( ClassSourceCodeVisitor cv ) {
		BlockSourceCodeVisitor mv =
			cv.visitMethod(
				Modifier.PUBLIC, null, "Object", "getFcContentDesc", null, null );
		generateGetFcContentDescMethod(mv);
		mv.visitEnd();
	}
	protected void generateGetFcContentDescMethod( BlockSourceCodeVisitor mv ) {
		mv.visitIns("throw","new","UnsupportedOperationException()");
	}

	protected void generateNewFcContentMethod( ClassSourceCodeVisitor cv ) {
		BlockSourceCodeVisitor mv =
			cv.visitMethod(
				Modifier.PUBLIC, null, "Object", "newFcContent", null,
				new String[]{InstantiationException.class.getName()} );
		generateNewFcContentMethod(mv);
		mv.visitEnd();
	}
	protected void generateNewFcContentMethod( BlockSourceCodeVisitor mv ) {
		mv.visitIns("throw","new","UnsupportedOperationException()");
	}

	protected void generateNewFcInstanceMethod( ClassSourceCodeVisitor cv ) {
		BlockSourceCodeVisitor mv =
			cv.visitMethod(
				Modifier.PUBLIC, null, Component.class.getName(),
				"newFcInstance", null,
				new String[]{InstantiationException.class.getName()} );
		generateNewFcInstanceMethod(mv);
		mv.visitEnd();
	}
	protected void generateNewFcInstanceMethod( BlockSourceCodeVisitor mv ) {
		mv.visitIns("throw","new","UnsupportedOperationException()");
	}

	protected void generateNewFcInstanceContentMethod( ClassSourceCodeVisitor cv ) {
		BlockSourceCodeVisitor mv =
			cv.visitMethod(
				Modifier.PUBLIC, null, Component.class.getName(),
				"newFcInstance", new String[]{"Object content"},
				new String[]{InstantiationException.class.getName()} );
		generateNewFcInstanceContentMethod(mv);
		mv.visitEnd();
	}
	protected void generateNewFcInstanceContentMethod( BlockSourceCodeVisitor mv ) {
		mv.visitIns("throw","new","UnsupportedOperationException()");
	}

	protected void generateExtraCode( ClassSourceCodeVisitor cv ) {}


	// -----------------------------------------------------------------------
	// Code generators
	// -----------------------------------------------------------------------

	protected void generateContentChecks( BlockSourceCodeVisitor mv ) {

		String ctrlDesc = membraneDesc.getDescriptor();
		String contentClassName = JuliacHelper.getContentClassName(ctrlDesc,contentDesc);

		if( contentClassName != null ) {
			boolean checkForBC = false;
			InterfaceType[] its = ct.getFcInterfaceTypes();
			for (int i = 0; i < its.length; i++) {
				if( its[i].isFcClientItf() ) {
					checkForBC = true;
				}
				else {
					String signature = its[i].getFcItfSignature();
					BlockSourceCodeVisitor then =
						mv.visitIf(
							"content!=null","&&","!(content","instanceof",signature+")");
					then.visitVar("String","msg","\""+contentClassName,"should implement",signature+"\"");
					then.visitIns("throw","new",InstantiationException.class.getName()+"(msg)").visitEnd();
				}
			}
			if( checkForBC ) {
				generateContentCheckForBC(mv);
			}
		}
	}

	protected void generateContentCheckForBC( BlockSourceCodeVisitor mv ) {

		String ctrlDesc = membraneDesc.getDescriptor();
		String contentClassName = JuliacHelper.getContentClassName(ctrlDesc,contentDesc);

		BlockSourceCodeVisitor then =
			mv.visitIf(
				"content!=null","&&","!(content","instanceof",BindingController.class.getName()+")");
		then.visitVar("String","msg","\""+contentClassName,"should implement",BindingController.class.getName()+"\"");
		then.visitIns("throw","new",InstantiationException.class.getName()+"(msg)").visitEnd();
	}

	protected void generateInitializationContextForContent( BlockSourceCodeVisitor mv ) {

		String ctrlDesc = membraneDesc.getDescriptor();

		mv.visitIns("ic.interfaces.put(\"/content\",content)");
		mv.visitIns("ic.interfaces.put(\"/controllerDesc\",\""+ctrlDesc+"\")");

		/*
		 * In the case of template components, Julia factory controller expects
		 * ic.content to reference contentDesc (which is an array describing
		 * the component associated to the template).
		 */
		if( ctrlDesc.endsWith("Template") ) {
			mv.visitSet("ic.content",JuliacHelper.javaifyContentDesc(contentDesc).toString());
		}
		else {
			mv.visitSet("ic.content","content");
		}
	}

	/**
	 * <p>
	 * Generate the source code corresponding to the creation of interceptors
	 * associated to the specified interface type.
	 * </p>
	 *
	 * <p>
	 * Declared as public as this method is called by {@link
	 * org.objectweb.fractal.juliac.opt.oo.MembraneInitializerOOCtrlClassGenerator}.
	 * </p>
	 *
	 * @param mv  the method visitor for generating the code
	 * @param it  the interface type
	 * @param delegate  the variable name referencing the delegate for the
	 *                  created interceptor
	 * @return    <code>true</code> if an interceptor has been created
	 */
	public boolean generateInterceptorCreation(
		BlockSourceCodeVisitor mv, InterfaceType it, String delegate ) {

		/*
		 * Check whether the code for instantiating an interceptor must be
		 * generated or not.
		 */
		InterfaceTypeConfigurableItf scg =
			membraneDesc.getInterceptorClassGenerator();
		if( scg == null ) {
			return false;
		}
		String signature = it.getFcItfSignature();
		Object cl = loadClass(signature);
		String pkgRoot = jc.getPkgRoot();
		scg.setInterfaceType(it,cl,pkgRoot);
		scg.setMembraneDesc(membraneDesc);
		if( ! scg.match() ) {
			return false;
		}

		/*
		 * Generate the code which instantiates the corresponding interceptor.
		 */
		String name = scg.getTargetTypeName();
		mv.visitSet("intercept", "new",name+"()");
		mv.visitIns("ic.controllers.add(intercept)");
		if( ! it.isFcClientItf() ) {
			// For server interfaces, let the interceptor delegate to the content
			mv.visitIns("(("+Interceptor.class.getName()+")intercept).setFcItfDelegate("+delegate+")");
		}

		return true;
	}

	protected Object loadClass( String signature ) {
		Class<?> cl = jc.loadClass(signature);
		return cl;
	}

	@Override
	public Object getSourceType() {
		return source;
	}
}
