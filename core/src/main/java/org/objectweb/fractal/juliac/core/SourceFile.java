/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import javax.tools.SimpleJavaFileObject;

import org.objectweb.fractal.juliac.api.SourceFileItf;
import org.objectweb.fractal.juliac.commons.io.FileHelper;

/**
 * This class represents a source code file, either provided as input or
 * generated, which is manipulatd by Juliac.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.3
 */
public abstract class SourceFile extends SimpleJavaFileObject
implements SourceFileItf {

	/** The qualified name of the source file. */
	protected String qname;

	/**
	 * The extension of the source file name, e.g. java or fractal.
	 * @since 2.6
	 */
	protected String ext;

	protected SourceFile( URI uri, String qname, String ext ) {
		super(uri,Kind.SOURCE);
		this.qname = qname;
		this.ext = ext;
	}


	// -----------------------------------------------------------------------
	// Public methods
	// -----------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.SourceFileItf#getName()
	 */
	@Override
	public String getName() {
		String[] names = qname.split("[.]");
		String name = names[names.length-1]+"."+ext;
		return name;
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.SourceFileItf#getQname()
	 */
	@Override
	public String getQname() {
		return qname;
	}

	/**
	 * Add all Java files contained in the specified location to the specified
	 * list of {@link SourceFile}s. A location denotes either an absolute path
	 * or a path relative to <code>root</code>, for a directory or a jar file.
	 *
	 * @param root  the base directory
	 * @param src   the location
	 * @param sfs   the list of source files
	 * @since 2.2.1
	 */
	public static void addAllJavaFiles(
		File root, String src, List<SourceFileItf> sfs )
	throws IOException {

		File input = FileHelper.getFile(root,src);
		if( ! input.exists() ) {
			String msg = "No such file or directory <"+root+","+src+">";
			throw new IOException(msg);
		}

		if( input.isDirectory() ) {
			SourceFile.addAllJavaFilesInDir(input,input,sfs);
		}
		else {
			String name = input.getName();
			String ext = name.substring(name.length()-4);
			if( ext.equals(".jar") ) {
				SourceFile.addAllJavaFilesInJar(input,sfs);
			}
			else {
				String msg = src+ " must be a directory or a .jar file";
				throw new IOException(msg);
			}
		}
	}

	/**
	 * Add all Java source files contained in the specified <code>dir</code>
	 * directory to the specified list of {@link SourceFile}s. Recursively scan
	 * the directory.
	 *
	 * @param root  the root directory where the scan begins
	 * @param dir   the current directory to scan
	 * @param sfs   the list of source files
	 */
	private static void addAllJavaFilesInDir(
		File root, File dir, List<SourceFileItf> sfs )
	throws IOException {

		File[] fs = dir.listFiles();
		if( fs == null ) {
			final String msg =
				dir.getAbsolutePath()+" must refer to a directory";
			throw new IOException(msg);
		}

		/*
		 * Handle .java files contained in the current directory.
		 */
		int headlength = root.getAbsolutePath().length();
		for (File f : fs) {
			if( ! f.isDirectory() && f.getName().endsWith(".java") ) {
				String fname = f.getAbsolutePath();
				fname = fname.substring(headlength+1);  // +1 for /
				fname = fname.substring(0,fname.length()-5);  // remove ".java"
				fname = fname.replace(File.separatorChar,'.');
				URI uri = f.toURI();
				SourceFile sf = new SourceFileDir(uri,fname,"java");
				sfs.add(sf);
			}
		}

		/*
		 * Recursively scan subdirectories.
		 */
		for (File f : fs) {
			if( f.isDirectory() ) {
				addAllJavaFilesInDir(root,f,sfs);
			}
		}
	}

	/**
	 * Add all Java source files contained in the specified .jar file to the
	 * specified list of {@link SourceFile}s.
	 *
	 * @param jar  the jar file
	 * @param sfs  the list of source files
	 */
	private static void addAllJavaFilesInJar( File jar, List<SourceFileItf> sfs )
	throws IOException {

		try( FileInputStream fis = new FileInputStream(jar) ) {
			try( JarInputStream jis = new JarInputStream(fis) ) {

				JarEntry je = jis.getNextJarEntry();
				while( je != null ) {

					String name = je.getName();
					if( je.isDirectory() || ! name.endsWith(".java") ) {
						// Skip non .java files
						je = jis.getNextJarEntry();
						continue;
					}

					// Entry qname
					String fname = name.substring(0,name.length()-5);  // remove ".java"
					fname = fname.replace('/','.');

					/*
					 * Entry content.
					 *
					 * je.getSize() proved to be unsuccesful on some jar files created
					 * with the command line jar cf. Returned size was -1. Switch to
					 * method read(byte[],int,int) which returns -1 when the end of the
					 * entry is reached. Use exactly this method (and not one of the
					 * other read methods) as this is the only one which is overriden
					 * in JarInputStream.
					 */
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					byte[] buffer = new byte[1024];
					int len;
					while( (len=jis.read(buffer,0,buffer.length)) != -1 ) {
						baos.write(buffer,0,len);
					}
					String content = new String(baos.toByteArray());

					URI uri = jar.toURI();
					SourceFileJar sfj = new SourceFileJar(uri,fname,"java",content);
					sfs.add(sfj);

					je = jis.getNextJarEntry();
				}
			}
		}
	}


	// -----------------------------------------------------------------------
	// j.l.Object methods
	// -----------------------------------------------------------------------

	@Override
	public boolean equals( Object other ) {
		if( !(other instanceof SourceFile) ) {
			return false;
		}
		return qname.equals(((SourceFile)other).qname);
	}

	@Override
	public int hashCode() {
		return qname.hashCode();
	}

	@Override
	public String toString() {
		String s = uri.toString();
		return s;
	}
}
