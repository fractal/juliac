/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.desc;

/**
 * A struct-like class for holding data about a binding which has been parsed
 * by the Juliac Fractal ADL backend.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.2
 */
public class BindingDesc {

	private BindingType bindingType;
	private ComponentDesc<?> clt;
	private String cltItfName;
	private ComponentDesc<?> srv;
	private String srvItfName;

	public BindingDesc(
		BindingType bindingType,
		ComponentDesc<?> clt, String cltItfName,
		ComponentDesc<?> srv, String srvItfName ) {

		this.bindingType = bindingType;
		this.clt = clt;
		this.cltItfName = cltItfName;
		this.srv = srv;
		this.srvItfName = srvItfName;
	}

	/**
	 * @since 2.2
	 */
	public BindingType getBindingType() {
		return bindingType;
	}

	public ComponentDesc<?> getClt() {
		return clt;
	}

	public String getCltItfName() {
		return cltItfName;
	}

	public ComponentDesc<?> getSrv() {
		return srv;
	}

	public String getSrvItfName() {
		return srvItfName;
	}

	@Override
	public String toString() {
		return clt.toString()+'.'+cltItfName+"->"+srv+'.'+srvItfName;
	}
}
