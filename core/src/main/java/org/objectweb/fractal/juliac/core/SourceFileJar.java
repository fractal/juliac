/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core;

import java.io.File;
import java.io.IOException;
import java.net.URI;

/**
 * Implementation for {@link SourceFile}s stored in a jar file.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.3
 */
public class SourceFileJar extends SourceFile {

	private CharSequence content;

	public SourceFileJar( URI uri, String qname, String ext, String content ) {
		super(uri,qname,ext);
		this.content = content;
	}

	@Override
	public CharSequence getCharContent( boolean ignoreEncodingErrors ) throws IOException {
		return content;
	}

	@Override
	public String toString() {
		String jar = super.toString();
		String s = "jar:"+jar+"!"+qname.replace('.',File.separatorChar)+'.'+ext;
		return s;
	}
}
