/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.helper;


import org.objectweb.fractal.juliac.core.Constants;

/**
 * This class provides general purpose helper methods for Juliac.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class JuliacHelper {

	/**
	 * Return a string containing the Java code to create a content descriptor
	 * similar to the specified parameter.
	 */
	public static StringBuilder javaifyContentDesc( Object contentDesc ) {

		StringBuilder sb = new StringBuilder();

		if( contentDesc == null ) {
			sb.append("null");
		}
		else if( contentDesc instanceof Object[] ) {
			sb.append("new Object[]{");
			Object[] array = (Object[]) contentDesc;
			for (Object o : array) {
				sb.append(javaifyContentDesc(o));
				sb.append(',');
			}
			sb.append("}");
		}
		else {
			// Assume the only other possible case is String
			sb.append('"');
			sb.append(contentDesc);
			sb.append('"');
		}

		return sb;
	}

	/**
	 * Return the specified type name prefixed with the default package for
	 * Juliac generated code.
	 */
	public static String getJuliacGeneratedStrongTypeName( String fqn ) {
		final String s = Constants.JULIAC_RUNTIME_GENERATED + "." + fqn;
		return s;
	}

	/**
	 * Return the content class name associated to the specified controller and
	 * content descriptors. For template components, this method returns the
	 * content class name of components created by the template.
	 *
	 * @throws IllegalArgumentException
	 *      if contentDesc is not a legal content descriptor
	 */
	public static String getContentClassName(
		String ctrlDesc, Object contentDesc ) {

		/*
		 * Adaptation for templates components.
		 * Templates are created with something like:
		 * Component cTmpl = cf.newFcInstance(
		 *      cType, "flatPrimitiveTemplate",
		 *      new Object[] { "flatPrimitive", "ClientImpl" });
		 */
		if( ctrlDesc.endsWith("Template") ) {

			if( !(contentDesc instanceof Object[]) ) {
				final String msg =
					"contentDesc should be an array when "+ctrlDesc+" is used";
				throw new IllegalArgumentException(msg);
			}
			Object[] cont = (Object[]) contentDesc;

			if( !(cont[0] instanceof String) ) {
				final String msg = "Inner controllerDesc should be a String";
				throw new IllegalArgumentException(msg);
			}
			if( cont[1]!=null && !(cont[1] instanceof String) ) {
				final String msg = "Inner contentDesc should be a String";
				throw new IllegalArgumentException(msg);
			}

			return (String) cont[1];
		}
		else {
			if( contentDesc!=null && !(contentDesc instanceof String) ) {
				final String msg = "contentDesc should be a String";
				throw new IllegalArgumentException(msg);
			}
			return (String) contentDesc;
		}
	}

	/**
	 * Return the provider prefix (i.e. a slash starting and ending string such
	 * as <code>/koch/</code>) inserted in front of the specified controller
	 * descriptor.
	 *
	 * @param ctrlDesc  the controller descriptor
	 * @return  the prefix contained in the specified controller descriptor or
	 *             <code>null</code> if the specified controller descriptor is
	 *             <code>null</code> or if there is no such prefix
	 */
	public static String getControllerDescPrefix( String ctrlDesc ) {

		if( ctrlDesc == null || ctrlDesc.charAt(0) != '/' ) {
			// No prefix
			return null;
		}

		int lastslash = ctrlDesc.lastIndexOf('/');
		if( lastslash!=-1 && lastslash+1 < ctrlDesc.length() ) {
			String prefix = ctrlDesc.substring(0,lastslash+1);
			return prefix;
		}

		// No trailing slash character
		return null;
	}

	/**
	 * Strip the provider prefix (i.e. a slash starting and ending string such
	 * as <code>/koch/</code>) inserted in front of the specified controller
	 * descriptor. If there is no such prefix, return the controller descriptor
	 * unchanged.
	 */
	public static String stripControllerDescPrefix( String ctrlDesc ) {
		String prefix = getControllerDescPrefix(ctrlDesc);
		if( prefix == null ) {
			return ctrlDesc;
		}
		String cdesc = ctrlDesc.substring(prefix.length());
		return cdesc;
	}
}
