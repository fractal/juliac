/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Victor Noel
 */

package org.objectweb.fractal.juliac.core;

import java.io.CharArrayWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.function.Predicate;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.processing.ProcessingEnvironment;
import javax.tools.Diagnostic;
import javax.tools.Diagnostic.Kind;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;

import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.type.BasicComponentType;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.JuliacModuleItf;
import org.objectweb.fractal.juliac.api.JuliacRuntimeException;
import org.objectweb.fractal.juliac.api.RuntimeClassNotFoundException;
import org.objectweb.fractal.juliac.api.SourceFileItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.visit.FileSourceCodeVisitor;
import org.objectweb.fractal.juliac.commons.io.FileHelper;
import org.objectweb.fractal.juliac.commons.lang.ClassLoaderHelper;
import org.objectweb.fractal.juliac.core.desc.ADLParserSupportItf;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.visit.FileSourceCodeWriter;

/**
 * A Java source code generator for Fractal components.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Victor Noel <victor.noel@linagora.com>
 */
public class Juliac implements JuliacItf {

	@Override
	public void close() {
		unregisterAll();
	}

	// ----------------------------------------------------------------------
	// Constants
	// ----------------------------------------------------------------------

	/** Default directory name for compiled generated files. */
	static final String DEFAULT_CLASS_DIR_NAME = "target/classes";

	/** Default directory name for generated files. */
	static final String DEFAULT_GEN_DIR_NAME =
		"target/generated-sources/juliac";


	// ----------------------------------------------------------------------
	// Methods for generating component source code
	// ----------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#generate(java.lang.String, java.lang.String)
	 */
	@Override
	public void generate( String adl, String targetname ) throws IOException {

		ADLParserSupportItf supportItf = lookupAndFilter(ADLParserSupportItf.class,adl);
		String pkgRoot = getPkgRoot();
		String rootedName = pkgRoot + targetname;

		Logger logger = getLogger();
		logger.info("[ADLParser: "+supportItf.getClass().getName()+"]");

		supportItf.generate(adl,rootedName);
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#generate(java.lang.String)
	 */
	@Override
	public void generate( String ctrlDesc )
	throws IOException, InstantiationException {

		ComponentType ct = new BasicComponentType(new BasicInterfaceType[0]);

		FCSourceCodeGeneratorItf fcscg =
			lookupAndFilter(FCSourceCodeGeneratorItf.class,ctrlDesc);

		Logger logger = getLogger();
		logger.info("[Generator: "+fcscg.getClass().getName()+"]");

		fcscg.generateMembraneImpl(ct,ctrlDesc,null,null);
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#generate(java.lang.String, java.lang.Object)
	 */
	@Override
	public void generate( String ctrlDesc, Object source )
	throws IOException, InstantiationException {

		ComponentType ct = new BasicComponentType(new BasicInterfaceType[0]);

		FCSourceCodeGeneratorItf fcscg =
			lookupAndFilter(FCSourceCodeGeneratorItf.class,ctrlDesc);

		Logger logger = getLogger();
		logger.info("[Generator: "+fcscg.getClass().getName()+"]");

		fcscg.generateMembraneImpl(ct,ctrlDesc,null,source);
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#generate(org.objectweb.fractal.api.Type, java.lang.Object, java.lang.Object, java.lang.Object)
	 */
	@Override
	public SourceCodeGeneratorItf generate(
		Type type, Object controllerDesc, Object contentDesc, Object source )
	throws IOException {

		FCSourceCodeGeneratorItf fcscg =
			lookupAndFilter(FCSourceCodeGeneratorItf.class,controllerDesc);

		Logger logger = getLogger();
		logger.info("[Generator: "+fcscg.getClass().getName()+"]");

		SourceCodeGeneratorItf icg =
			fcscg.generate(type,controllerDesc,contentDesc,source);
		return icg;
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#generateForDesc(java.lang.String)
	 */
	@Override
	public void generateForDesc( String desc )
	throws IOException, InstantiationException {

		/*
		 * This method is named generateForDesc instead of generate to avoid
		 * mismatches with the generate(Object) method which was existing prior
		 * to version 2.2.4. Else pieces of code which were using
		 * generate(Object) while transmitting a String would end up executing
		 * instead generate(String).
		 */

		final String msg =
			"Expected component descriptor "+
			"[name/signature/bool/bool/bool;...,controllerDesc,contentDesc]. "+
			"Got: "+desc;

		if( desc.length() < "[x/x/true/true/true,x,x]".length() ) {
			throw new IllegalArgumentException(msg);
		}

		if( desc.charAt(0)!='[' && desc.charAt(desc.length()-1)!=']' ) {
			throw new IllegalArgumentException(msg);
		}
		desc = desc.substring(1,desc.length()-1);

		/*
		 * We need 3 arguments delimited by a comma or 2 if the trailing
		 * character is a comma.
		 */
		String[] elts = desc.split(",");
		if( !( elts.length==3 || (elts.length==2 && desc.endsWith(",")) ) ) {
			throw new IllegalArgumentException(msg);
		}
		String t = elts[0];
		String ctrlDesc = elts[1];
		String contentDesc = elts.length==2 ? null: elts[2];

		String[] types = t.split(";");
		InterfaceType[] its = new InterfaceType[types.length];
		for( int i=0 ; i<types.length ; i++ ) {
			String type = types[i];
			elts = type.split("/");
			if( elts.length != 5 ) {
				throw new IllegalArgumentException(msg);
			}
			String name = elts[0];
			String signature = elts[1];
			boolean isClient = Boolean.parseBoolean(elts[2]);
			boolean isOptional = Boolean.parseBoolean(elts[3]);
			boolean isCollection = Boolean.parseBoolean(elts[4]);
			its[i] =
				new BasicInterfaceType(
					name, signature, isClient, isOptional, isCollection );
		}
		ComponentType ct = new BasicComponentType(its);

		generate(ct,ctrlDesc,contentDesc,null);
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#generateSourceCode(org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf)
	 */
	@Override
	public void generateSourceCode( SourceCodeGeneratorItf scg )
	throws IOException {

		String targetClassName = scg.getTargetTypeName();
		if( hasBeenGenerated(targetClassName) ) {
			return;
		}

		URI uri = generateSourceCodeOverride(scg);

		Object sourceType = scg.getSourceType();
		addGenerated(
			new SourceFileDir(uri,targetClassName,"java"), sourceType );
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#generateSourceCodeOverride(org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf)
	 */
	@Override
	public URI generateSourceCodeOverride( SourceCodeGeneratorItf scg )
	throws IOException {

		File genDir = getGenDir();
		String targetClassName = scg.getTargetTypeName();

		String filename =
			targetClassName.replace('.',File.separatorChar)+".java";
		File f = new File(genDir,filename);
		File fdir = f.getParentFile();
		fdir.mkdirs();

		/*
		 * Do not generate directly in a FileWriter to avoid generating
		 * incomplete files that may cause errors when compiling.
		 *
		 * The issue appeared with the <gracefulExit> MOJO option. The purpose
		 * is to exit the MOJO without reporting an exception (the functionality
		 * was requested by Nicolas from Petals Link.) If the generation throws
		 * an exception, an incomplete file may be generated, the MOJO exits,
		 * the regular Maven compiler MOJO compiles the incomplete file, which
		 * may generate compilation errors.
		 *
		 * To prevent this from happening, we first generate the code in memory
		 * and then dump the content in the file once the generation has been
		 * completed.
		 */
		try( CharArrayWriter caw = new CharArrayWriter() ) {
			PrintWriter pw = new PrintWriter(caw);
			FileSourceCodeVisitor cw = new FileSourceCodeWriter(pw);
			scg.generate(cw);
			try( Writer writer = new FileWriter(f) ) {
				caw.writeTo(writer);
			}
		}

		URI uri = f.toURI();
		return uri;
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#hasBeenGenerated(java.lang.String)
	 */
	@Override
	public boolean hasBeenGenerated( String name ) {

		// Check whether the type has been generated by Juliac
		if( contains(name) ) {
			return true;
		}

		// Check whether the type exists in the classpath
		try {
			loadClass(name);
			return true;
		}
		catch( RuntimeClassNotFoundException cnfe ) {}

		return false;
	}


	// ----------------------------------------------------------------------
	// Methods for compiling code
	// ----------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#build()
	 */
	@Override
	public void build() throws IOException {

		/*
		 * Compile input and generated source files.
		 */
		Logger logger = getLogger();
		logger.info("Compiling...");
		if( inputFiles.size()!=0 || generatedFiles.size()!=0 ) {

			// Compile
			compile();

			// Report on compiled files
			for (SourceFileItf sf : inputFiles) {
				logger.fine("   "+sf.getQname());
			}
			for (SourceFileItf sf : generatedFiles) {
				logger.fine("   "+sf.getQname());
			}
			final int total = inputFiles.size() + generatedFiles.size();
			final String msg =
				total+" file(s) compiled to "+
				getClassDir().getAbsolutePath();
			logger.info(msg);
		}
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#addGenerated(org.objectweb.fractal.juliac.SourceFile, java.lang.Object)
	 */
	@Override
	public void addGenerated( SourceFileItf sf, Object sourceType ) {
		generatedFiles.add(sf);
		String qname = sf.getQname();
		sourceTypes.put(qname,sourceType);
		Logger logger = getLogger();
		logger.info(sf.getQname());
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#contains(java.lang.String)
	 */
	@Override
	public boolean contains( String qname ) {
		return sourceTypes.containsKey(qname);
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#getSourceType(java.lang.String)
	 */
	@Override
	public Object getSourceType( String qname ) {
		return sourceTypes.get(qname);
	}

	/**
	 * Compile the input source and generated files.
	 *
	 * @since 2.7
	 */
	private void compile() throws IOException {

		// Only compiles if there are some files to compile
		if( inputFiles.size() + generatedFiles.size() > 0 ) {
			List<SourceFileItf> sfs = new ArrayList<>();
			sfs.addAll(inputFiles);
			sfs.addAll(generatedFiles);
			File classDir = getClassDir();
			ClassLoader classLoader = getClassLoader();
			List<File> cpes = ClassLoaderHelper.getClassPathEntries(classLoader);
			String javaReleaseVersionNumber = getJavaReleaseVersionNumber();
			Logger logger = getLogger();
			compile(sfs,classDir,cpes,javaReleaseVersionNumber,logger);
		}

		// Reset the lists of input and generated files
		inputFiles = new ArrayList<>();
		generatedFiles = new ArrayList<>();
	}

	/**
	 * Compile the specified files.
	 * 
	 * @since 2.8
	 */
	private void compile(
		List<SourceFileItf> sfs, File classDir, List<File> cpes,
		String javaReleaseVersionNumber, Logger logger )
	throws IOException {

		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

		/*
		 * Compiler options.
		 */
		List<String> options = new ArrayList<>();
		options.add("--release");
		options.add(javaReleaseVersionNumber);

		/*
		 * Set locations for output and classpath.
		 */
		StandardJavaFileManager fm = compiler.getStandardFileManager(null,null,null);
		fm.setLocation(StandardLocation.CLASS_OUTPUT,Collections.singleton(classDir));
		fm.setLocation(StandardLocation.CLASS_PATH,cpes);

		/*
		 * Compile the code.
		 */
		DiagnosticCollector<JavaFileObject> dc = new DiagnosticCollector<>();
		CompilationTask ct = compiler.getTask(null,fm,dc,options,null,sfs);
		boolean success = ct.call();

		/*
		 * Report errors, warnings and info messages if any.
		 */
		List<Diagnostic<? extends JavaFileObject>> diags = dc.getDiagnostics();
		for (Diagnostic<?> diag : diags) {
			Kind kind = diag.getKind();
			if( kind.equals(Kind.ERROR) ) {
				logger.severe(diag.toString());
			}
			else if( kind.equals(Kind.WARNING) ) {
				logger.warning(diag.toString());
			}
			else {
				logger.info(diag.toString());
			}
		}
		if (!success) {
			final String msg = "Compilation error";
			throw new IOException(msg);
		}
	}

	/** The source files which are given as input to Juliac. */
	private List<SourceFileItf> inputFiles = new ArrayList<>();

	/** The source files which are generated by Juliac. */
	private List<SourceFileItf> generatedFiles = new ArrayList<>();

	/**
	 * The source types that triggered the generation of files. The map
	 * is indexed by qualified names of generated files.
	 *
	 * @since 2.7
	 */
	private Map<String,Object> sourceTypes = new HashMap<>();


	// ----------------------------------------------------------------------
	// Class loading
	// ----------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#loadClass(java.lang.String)
	 */
	@Override
	public <T> Class<T> loadClass( String name ) {
		ClassLoader classLoader = getClassLoader();
		try {
			@SuppressWarnings("unchecked")
			Class<T> cl = (Class<T>) classLoader.loadClass(name);
			return cl;
		}
		catch( ClassNotFoundException cnfe ) {
			if( primitives.containsKey(name) ) {
				@SuppressWarnings("unchecked")
				Class<T> cl = (Class<T>) primitives.get(name);
				return cl;
			}
		}
		throw new RuntimeClassNotFoundException(name);
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#instantiate(java.lang.Class)
	 */
	@Override
	public <T> T instantiate( Class<T> cl ) {

		try {
			T o = cl.getConstructor().newInstance();
			return o;
		}
		catch ( NoSuchMethodException | InvocationTargetException |
					java.lang.InstantiationException |
					IllegalAccessException e ) {
			throw new JuliacRuntimeException(e);
		}
	}

	/**
	 * {@link Class}es for primitive types.
	 */
	private static final Map<String,Class<?>> primitives =
		new HashMap<String,Class<?>>() {
			private static final long serialVersionUID = 3918048772003217772L;
		{
			put("boolean",boolean.class);
			put("char",char.class);
			put("byte",byte.class);
			put("short",short.class);
			put("int",int.class);
			put("long",long.class);
			put("float",float.class);
			put("double",double.class);
			put("void",void.class);
		}};


	// ----------------------------------------------------------------------
	// Modules and services management
	// ----------------------------------------------------------------------

	private Map<Class<? extends JuliacModuleItf>,List<JuliacModuleItf>> services =
		new HashMap<>();

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#unregisterAll()
	 */
	@Override
	public void unregisterAll() {

		/*
		 * Method close() is supposed to unregister service implementations by
		 * calling unregister(Class<T>,T).
		 */
		Collection<List<JuliacModuleItf>> services =
			new ArrayList<>(this.services.values());

		for (List<JuliacModuleItf> e : services) {
			List<JuliacModuleItf> copy = new ArrayList<>(e);
			for (JuliacModuleItf service : copy) {
				try {
					service.close(this);
				}
				catch( Throwable t ) {
					// Whatever the problem, move on to the next service
					Logger logger = getLogger();
					final String msg =
						t.getClass()+" thrown with message "+
						t.getMessage()+" when calling close(this) on "+
						service;
					logger.warning(msg);
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#register(java.lang.Class, T)
	 */
	@Override
	public <T extends JuliacModuleItf> void register( Class<T> itf, T impl ) {
		List<JuliacModuleItf> services = null;
		if( this.services.containsKey(itf) ) {
			services = this.services.get(itf);
		}
		else {
			services = new ArrayList<>();
			this.services.put(itf,services);
		}
		services.add(impl);
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#lookup(java.lang.Class)
	 */
	@Override
	public <T extends JuliacModuleItf> Iterable<T> lookup( Class<T> itf ) {

		// Check whether the service has already been registered before
		if( services.containsKey(itf) ) {
			List<JuliacModuleItf> services = this.services.get(itf);
			if( services.size() > 0 ) {
				@SuppressWarnings("unchecked")
				Iterable<T> ret = (Iterable<T>) services;
				return ret;
			}
		}

		/*
		 * Check whether the service can be loaded as a Java service.
		 *
		 * Use the class loader of the service type to load the service
		 * implementation. Else juliac-adlet-core complains that the
		 * implementation cannot be casted to the type. This is certainly due to
		 * the fact that different class loaders are used by the annotation
		 * processor tool.
		 */
		ClassLoader cl = itf.getClassLoader();
		ServiceLoader<T> sl = ServiceLoader.load(itf,cl);
		for (JuliacModuleItf m : sl) {
			try {
				m.init(this);
			}
			catch (IOException e) {
				throw new JuliacRuntimeException(e);
			}
		}

		return sl;
	}

	@Override
	public <T extends JuliacModuleItf & Predicate<P>,P> T lookupAndFilter(
		Class<T> itf, P value ) {

		Iterable<T> services = lookup(itf);
		for (T service : services) {
			if( service.test(value) ) {
				return service;
			}
		}

		final String msg =
			"No such service implementation: "+itf.getName()+" "+value;
		throw new IllegalArgumentException(msg);
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#lookupFirst(java.lang.Class)
	 */
	@Override
	public <T extends JuliacModuleItf> T lookupFirst( Class<T> itf ) {

		Iterable<T> services = lookup(itf);
		Iterator<T> iterator = services.iterator();
		T service = iterator.next();
		return service;
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#unregister(java.lang.Class, T)
	 */
	@Override
	public <T extends JuliacModuleItf> void unregister( Class<T> itf, T impl ) {
		if( services.containsKey(itf) ) {
			List<JuliacModuleItf> services = this.services.get(itf);
			services.remove(impl);
		}
	}


	// ----------------------------------------------------------------------
	// Juliac properties
	// ----------------------------------------------------------------------

	/**
	 * The base directory for locations specified in the parameters.
	 */
	private File baseDir = new File(".");

	/**
	 * The list of locations containing source files. A location denotes either
	 * an absolute path or a path relative to {@link #baseDir}, for a directory
	 * or a jar file.
	 */
	private List<String> srcs = new ArrayList<>();

	/**
	 * The list of locations containing source files for librairies. A location
	 * denotes either an absolute path or a path relative to {@link #baseDir},
	 * for a directory or a jar file.
	 */
	private List<String> srclibs = new ArrayList<>();

	/**
	 * The root package for generated code.
	 */
	private String pkgRoot = "";

	/**
	 * The name of the directory where generated files are stored. The name
	 * denotes either an absolute path or a path relative to {@link #baseDir}.
	 * The resulting {@link File} is stored in {@link #genDirName}.
	 */
	private String genDirName = DEFAULT_GEN_DIR_NAME;

	/**
	 * The directory where generated files are stored.
	 */
	private File genDir;

	/**
	 * The name of the directory where generated files are compiled. The name
	 * denotes either an absolute path or a path relative to {@link #baseDir}.
	 * The resulting {@link File} is stored in {@link #classDir}.
	 */
	private String classDirName = DEFAULT_CLASS_DIR_NAME;

	/**
	 * The directory where generated files are compiled.
	 */
	private File classDir;

	private ClassLoader classLoader;

	/**
	 * The default logger.
	 * @since 2.2.4
	 */
	private Logger logger = null;

	/**
	 * The name of the default logger.
	 * @since 2.2.4
	 */
	private static final String LOGGER_NAME = "org.objectweb.fractal.juliac";

	/**
	 * Reference to the processing environment of the JDK annotation processing
	 * tool for the Adlet extension.
	 *
	 * @since 2.7
	 */
	private ProcessingEnvironment processingEnvironment = null;

	/**
	 * The comma-separated list of Julia configuration files.
	 * @since 2.7
	 */
	private String juliaCfgFiles = "";


	// ----------------------------------------------------------------------
	// Setter/getter methods
	// ----------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#setProcessingEnvironment(javax.annotation.processing.ProcessingEnvironment)
	 */
	@Override
	public void setProcessingEnvironment( ProcessingEnvironment pe ) {
		this.processingEnvironment = pe;
	}
	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#getProcessingEnvironment()
	 */
	@Override
	public ProcessingEnvironment getProcessingEnvironment() {
		return processingEnvironment;
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#setJuliaCfgFiles(java.lang.String)
	 */
	@Override
	public void setJuliaCfgFiles( String juliaConfig ) {
		this.juliaCfgFiles = juliaConfig;
	}
	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#getJuliaCfgFiles()
	 */
	@Override
	public String getJuliaCfgFiles() {
		return juliaCfgFiles;
	}


	// ----------------------------------------------------------------------
	// Setter/getter methods for Juliac file and directory related properties
	// ----------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#setPkgRoot(java.lang.String)
	 */
	@Override
	public void setPkgRoot( final String pkgRoot ) {
		this.pkgRoot = pkgRoot;
		if( pkgRoot!=null && pkgRoot.length()!=0 ) {
			this.pkgRoot += '.';
		}
	}
	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#getPkgRoot()
	 */
	@Override
	public String getPkgRoot() { return pkgRoot; }

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#setBaseDir(java.io.File)
	 */
	@Override
	public void setBaseDir( File baseDir ) { this.baseDir = baseDir; }
	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#getBaseDir()
	 */
	@Override
	public File getBaseDir() { return baseDir; }

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#getSrcs()
	 */
	@Override
	public String[] getSrcs() {
		/*
		 * Return a copy in order to prevent unwanted adding in the list.
		 * Elements must be added in the list by invoking either {@link
		 * #addSrc(String)} or {@link #addSrcs(String[])}.
		 */
		return srcs.toArray(new String[srcs.size()]);
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#addSrc(java.lang.String)
	 */
	@Override
	public void addSrc( String src ) throws IOException {
		File baseDir = getBaseDir();
		SourceFile.addAllJavaFiles(baseDir,src,inputFiles);
		srcs.add(src);
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#addSrcs(java.lang.String[])
	 */
	@Override
	public void addSrcs( String[] srcs ) throws IOException {
		for (String src : srcs) {
			addSrc(src);
		}
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#addResource(java.lang.String, java.lang.StringBuilder)
	 */
	@Override
	public void addResource( String filename, StringBuilder sb )
	throws IOException {

		// Copy the resource to the class directory
		File genDir = getClassDir();
		File file = new File(genDir,filename);
		file.getParentFile().mkdirs();
		try( Writer writer = new FileWriter(file) ) {
			writer.write(sb.toString());
		}

		logger.info(filename+" copied to "+file.getAbsolutePath());
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#getSrclibs()
	 */
	@Override
	public String[] getSrclibs() {
		/*
		 * Return a copy in order to prevent unwanted adding in the list.
		 * Elements must be added in the list by invoking either {@link
		 * #addSrclib(String)} or {@link #addSrclibs(String[])}.
		 */
		return srclibs.toArray(new String[srclibs.size()]);
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#addSrclib(java.lang.String)
	 */
	@Override
	public void addSrclib( String srclib ) {
		srclibs.add(srclib);
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#addSrclibs(java.lang.String[])
	 */
	@Override
	public void addSrclibs( String[] srclibs ) {
		for (String srclib : srclibs) {
			addSrclib(srclib);
		}
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#getGenDir()
	 */
	@Override
	public File getGenDir() throws IOException {
		if( genDir == null ) {
			File baseDir = getBaseDir();
			String genDirName = getGenDirName();
			genDir = initDir(baseDir,genDirName);
		}
		return genDir;
	}
	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#getGenDirName()
	 */
	@Override
	public String getGenDirName() { return genDirName; }
	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#setGenDirName(java.lang.String)
	 */
	@Override
	public void setGenDirName( String genDirName ) throws IOException {
		File baseDir = getBaseDir();
		genDir = initDir(baseDir,genDirName);
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#getClassDir()
	 */
	@Override
	public File getClassDir() throws IOException {
		if( classDir == null ) {
			File baseDir = getBaseDir();
			String classDirName = getClassDirName();
			classDir = initDir(baseDir,classDirName);
		}
		return classDir;
	}
	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#getClassDirName()
	 */
	@Override
	public String getClassDirName() { return classDirName; }
	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#setClassDirName(java.lang.String)
	 */
	@Override
	public void setClassDirName( String classDirName ) throws IOException {
		File baseDir = getBaseDir();
		classDir = initDir(baseDir,classDirName);
	}


	// ----------------------------------------------------------------------
	// Setter/getter methods for the class loader
	// ----------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#getClassLoader()
	 */
	@Override
	public ClassLoader getClassLoader() {
		if( classLoader == null ) {
			classLoader = getClass().getClassLoader();
		}
		return classLoader;
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#setClassLoader(java.lang.ClassLoader)
	 */
	@Override
	public void setClassLoader( ClassLoader classLoader ) {
		this.classLoader = classLoader;
	}


	// ----------------------------------------------------------------------
	// Logging facilities
	// ----------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.JuliacItf#getLogger()
	 */
	@Override
	public Logger getLogger() {

		if( logger == null ) {
			logger = Logger.getLogger(LOGGER_NAME);
			Logger parent = logger.getParent();
			Handler[] handlers = parent.getHandlers();
			for (Handler handler : handlers) {
				Formatter newFormatter = new LoggerFormatter();
				handler.setFormatter(newFormatter);
			}
		}

		return logger;
	}

	/**
	 * Define a log level.
	 *
	 * @param logger  the logger
	 * @param level   the log level
	 * @since 2.2.4
	 */
	public static void setLevel( Logger logger, Level level ) {
		logger.setLevel(level);
		Logger parent = logger.getParent();
		Handler[] handlers = parent.getHandlers();
		for (Handler handler : handlers) {
			handler.setLevel(level);
		}
	}

	@Override
	public String getJavaReleaseVersionNumber() {
		/*
		 * To keep things simple, we rely on the version number returned by the
		 * Java environment. A more configurable version would consist in adding
		 * a corresponding option in the front-ends: command line, Maven plugin,
		 * and options passed to the annotation processors in Adlet.
		 */
		return System.getProperty("java.specification.version");
	}



	// ----------------------------------------------------------------------
	// Implementation specific
	// ----------------------------------------------------------------------

	/**
	 * Check that the specified file name corresponds to an existing directory.
	 * If not, create the corresponding hierarchy of directories.
	 *
	 * @param baseDir  the base directory
	 * @param dirName  the file name
	 * @return  the bottom-most created directory
	 *
	 * @throws IOException  if the file name exists but is not a directory
	 */
	private static File initDir( File baseDir, String dirName )
	throws IOException {

		File dir = FileHelper.getFile(baseDir,dirName);
		if( dir.exists() && !dir.isDirectory() ) {
			String msg = dir.getAbsolutePath()+" is not a directory";
			throw new IOException(msg);
		}
		if( ! dir.exists() ) {
			dir.mkdirs();
		}
		return dir;
	}
}
