/***
 * Juliac
 * Copyright (C) 2010-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.core.visit;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.nullToEmpty;

import com.google.common.base.Joiner;

/**
 * Helper methods for code generation.
 *
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 * @since 2.3
 */
public class CodeHelper {

	public static final String asString(Object o) {
		checkNotNull(o);
		if (o instanceof Class<?>) {
			return (((Class<?>) o).getName());
		}
		return nullToEmpty(o.toString());
	}

	public static final String[] convert(Object... o) {
		String[] res = new String[o.length];
		for (int i=0;i<o.length;i++) {
			res[i] = asString(o[i]);
		}
		return res;
	}

	public static final String asString(String sep, Object... o) {
		if (o == null || o.length == 0) {
			return null;
		}
		return Joiner.on(sep).skipNulls().join(convert(o));
	}

	public static final String asString(final Object... code) {
		return asString(" ", code);
	}

	public static final String _throw(Object ex, Object... params) {
		return "throw "+_new(ex,params);
	}

	public static final String _new(Object cls, Object... params) {
		String res =  "new "+asString(cls)+"(";
		if (params!=null && params.length>0)
			res += asString(",",params);
		return res+")";
	}
}
