/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Victor Noel
 */

package org.objectweb.fractal.juliac.core.helper;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.type.BasicComponentType;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.api.JuliacRuntimeException;
import org.objectweb.fractal.juliac.commons.lang.ClassHelper;

/**
 * This class provides helper methods for the {@link InterfaceType} interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Victor Noel <victor.noel@linagora.com>
 */
public class InterfaceTypeHelper {

	/**
	 * Return a string containing the Fractal ADL definition of the specified
	 * {@link InterfaceType}.
	 *
	 * @since 2.6
	 */
	public static StringBuilder fractalADLify( InterfaceType it ) {
		StringBuilder sb = new StringBuilder();
		sb.append("  <interface ");
		sb.append("name=\"");
		sb.append(it.getFcItfName());
		sb.append("\" ");
		sb.append("role=\"server\" ");
		sb.append("signature=\"");
		sb.append(it.getFcItfSignature());
		sb.append("\" ");
		sb.append("/>\n");
		return sb;
	}

	/**
	 * Return a string containing the Java code to create an array of
	 * {@link InterfaceType}s similar to the specified parameter.
	 */
	public static StringBuilder javaify( InterfaceType[] its ) {

		StringBuilder sb = new StringBuilder();

		sb.append("new "+BasicComponentType.class.getName()+"( ");
		sb.append("new "+InterfaceType.class.getName()+"[]{");

		for (int i = 0; i < its.length; i++) {
			sb.append(javaify(its[i]));
			sb.append(",");
		}

		sb.append("} )");

		return sb;
	}

	/**
	 * Return a string containing the Java code to create an
	 * {@link InterfaceType} similar to the specified parameter.
	 */
	public static StringBuilder javaify( InterfaceType it ) {

		StringBuilder sb = new StringBuilder();

		String itfname = it.getFcItfName();
		sb.append("new "+BasicInterfaceType.class.getName()+"(");
		sb.append("\""+itfname+"\",");
		sb.append("\""+it.getFcItfSignature()+"\",");
		sb.append((it.isFcClientItf()?"true":"false")+",");
		sb.append((it.isFcOptionalItf()?"true":"false")+",");
		sb.append((it.isFcCollectionItf()?"true":"false"));
		sb.append(")");

		return sb;
	}

	/**
	 * Return a copy of the given interface type where the role of the interface
	 * has been inverted: if the role of the specified interface type is client
	 * (resp. server), the role of the returned interface type is server (resp.
	 * client).
	 */
	public static InterfaceType newSymetricInterfaceType( InterfaceType it ) {
		String name = it.getFcItfName();
		return newSymetricInterfaceType(it,name);
	}

	/**
	 * Return a copy of the given interface type where the role of the interface
	 * has been inverted: if the role of the specified interface type is client
	 * (resp. server), the role of the returned interface type is server (resp.
	 * client).
	 *
	 * @param it    the interface type
	 * @param name  the name of the new interface type
	 * @since 2.5
	 */
	public static InterfaceType newSymetricInterfaceType(
		InterfaceType it, String name ) {
		return
			new BasicInterfaceType(
				name,
				it.getFcItfSignature(),
				! it.isFcClientItf(),   // client <-> server
				it.isFcOptionalItf(),
				it.isFcCollectionItf()
			);
	}

	/**
	 * Given the interface types of a control membrane, return the corresponding
	 * type for the application (level 0) interfaces. This consists in removing
	 * the two slash characters which are in front of the interface names.
	 *
	 * @since 2.5
	 */
	public static InterfaceType[] downToLevel0InterfaceType( InterfaceType[] srcits ) {
		InterfaceType[] its = new InterfaceType[ srcits.length ];
		for (int i = 0; i < srcits.length; i++) {
			its[i] = InterfaceTypeHelper.downToLevel0InterfaceType(srcits[i]);
		}
		return its;
	}

	/**
	 * Given the interface type of a control membrane, return the corresponding
	 * type for the application (level 0) interface. This consists in removing
	 * the two slash characters which are in front of the interface name.
	 */
	public static InterfaceType downToLevel0InterfaceType( InterfaceType srcit ) {
		InterfaceType it =
			new BasicInterfaceType(
				srcit.getFcItfName().substring(2),     // remove //
				srcit.getFcItfSignature(),
				srcit.isFcClientItf(),
				srcit.isFcOptionalItf(),
				srcit.isFcCollectionItf()
			);
		return it;
	}

	/**
	 * Return <code>true</code> if one the specified {@link InterfaceType}s
	 * defines a <code>content-controller</code> or a
	 * <code>basic-content-controller</code> interface. This denotes a
	 * composite component. <code>basic-content-controller</code> is defined by
	 * Dream for the <code>dreamActivitiesComposite</code> controller
	 * descriptor.
	 *
	 * @since 2.1.6
	 */
	public static boolean isComposite( InterfaceType[] its ) {
		for (InterfaceType it : its) {
			String itname = it.getFcItfName();
			if( itname.equals("content-controller") ||
				itname.equals("basic-content-controller") ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Return a new component type constructed from the specified array of
	 * interface types. Wraps the declared {@link InstantiationExcepton} into a
	 * {@link JuliacRuntimeException}.
	 *
	 * @param its  the interface types
	 * @return     the corresponding component type
	 * @throws JuliacRuntimeException  if the component type cannot be created
	 * @since 2.5
	 */
	public static ComponentType newBasicComponentType( InterfaceType[] its ) {
		try {
			ComponentType ct = new BasicComponentType(its);
			return ct;
		}
		catch( InstantiationException ie ) {
			throw new JuliacRuntimeException(ie);
		}
	}

	/**
	 * Return the server interface type associated with the specified name and
	 * class.
	 *
	 * @param name  the name
	 * @param cl    the class
	 * @return      the corresponding server interface type
	 * @since 2.6
	 */
	public static InterfaceType toInterfaceType( String name, Class<?> cl ) {
		String signature = cl.getName();
		InterfaceType it =
			new BasicInterfaceType( name, signature, false, false, false );
		return it;
	}

	/**
	 * Return the signature of the specified interface type including the
	 * generic parameter names of the form <code>&lt;P1,P2,...&gt;</code> if
	 * any.
	 *
	 * @param it       the interface type
	 * @param proxycl  the class corresponding to the interface type
	 * @return    the corresponding signature
	 */
	public static String getInterfaceTypeSignature(
		InterfaceType it, Class<?> proxycl ) {

		String signature = it.getFcItfSignature();

		String[] tpnames = ClassHelper.getTypeParameterNames(proxycl);
		if( tpnames.length == 0 ) {
			return signature;
		}

		StringBuilder sb = new StringBuilder(signature);
		String s = ClassHelper.getTypeParameterNamesSignature(tpnames);
		sb.append(s);
		return sb.toString();
	}
}
