/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.desc;

import java.util.List;

import org.objectweb.fractal.julia.loader.Tree;

/**
 * A struct-like class for holding data about a controller.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class ControllerDesc {

	private String implName;
	private List<String> layerImplNames;
	private Tree tree;

	/**
	 * Create a new controller descriptor.
	 *
	 * @param implName  the name of the class implementing this controller
	 * @param layerImplNames
	 *      the class names of the layers used to generate the implementation of
	 *      this controller
	 *  @param tree
	 *      the configuration tree defining this controller (used to initialize
	 *      this controller if needed)
	 */
	public ControllerDesc( String implName, List<String> layerImplNames, Tree tree ) {
		this.implName = implName;
		this.layerImplNames = layerImplNames;
		this.tree = tree;
	}

	/**
	 * Return the name of the class implementing this controller.
	 */
	public String getImplName() {
		return implName;
	}

	/**
	 * Set the name of the class implementing this controller.
	 */
	public void setImplName( String implName ) {
		this.implName = implName;
	}

	/**
	 * Return class names of the layers used to generate the implementation of
	 * this controller.
	 */
	public List<String> getLayerImplNames() {
		return layerImplNames;
	}

	/**
	 * When no implementation class is specified for this controller and when
	 * there is only one layer specified, return the name of this unique layer.
	 * Else return the name of the class implementing this controller.
	 *
	 * @since 2.5
	 */
	public String getImplNameOrUniqueLayerImplName() {
		if( implName.length()==0 && layerImplNames.size()==1 ) {
			String ret = layerImplNames.get(0);
			return ret;
		}
		return implName;
	}

	/**
	 * Return the configuration tree defining this controller.
	 */
	public Tree getTree() {
		return tree;
	}

	@Override
	public String toString() {
		return implName;
	}
}
