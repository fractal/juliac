/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Victor Noel
 */

package org.objectweb.fractal.juliac.core;

/**
 * Values recognized as command line options when invoking
 * {@link JuliacCmdLine}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Victor Noel <victor.noel@linagora.com>
 * @since 1.2
 */
public enum CmdLineOptions {

	/**
	 * List of directories and jar files containing source files. The separator
	 * element is the system-dependent path-separator character.
	 */
	SRCS,

	/**
	 * List of directories and jar files containing mixin source files. The
	 * separator element is the system-dependent path-separator character.
	 */
	MIXINS,

	/** Root package for generated code. */
	PKGROOT,

	/** Directory where generated source code is to be dumped. */
	GENSRC,

	/** Directory where compiled generated source code is to be dumped. */
	GENCLASS,

	/**
	 * The base directory for locations specified in the parameters.
	 * @since 2.6
	 */
	BASEDIR,

	/**
	 * The comma-separated list of Julia config files.
	 * @since 2.7
	 */
	JULIACFGFILES;
}
