/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URI;

/**
 * Implementation for {@link SourceFile}s stored in a directory.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.3
 */
public class SourceFileDir extends SourceFile {

	public SourceFileDir( URI uri, String qname, String ext ) {
		super(uri,qname,ext);
	}

	@Override
	public CharSequence getCharContent( boolean ignoreEncodingErrors )
	throws IOException {
		File f = new File(uri);
		try( Reader reader = new FileReader(f) ) {
			StringBuilder sb = new StringBuilder();
			char[] str = new char[1024];
			int len;
			while( (len=reader.read(str)) != -1 ) {
				sb.append(str,0,len);
			}
			return sb;
		}
	}
}
