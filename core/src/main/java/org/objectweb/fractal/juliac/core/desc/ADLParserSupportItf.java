/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.desc;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

import org.objectweb.fractal.juliac.api.JuliacModuleItf;

/**
 * This interface describes the services provided by an ADL parser to Juliac.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.0
 */
public interface ADLParserSupportItf
extends JuliacModuleItf, Predicate<String> {

	/**
	 * Parse the specified ADL descriptor and return a model of it.
	 *
	 * @param adl      the fully-qualified name of the ADL descriptor
	 * @param context  contextual information
	 * @return         the model of the specified ADL
	 */
	public ComponentDesc<?> parse( String adl, Map<Object,Object> context )
	throws IOException;

	/**
	 * Generate the source code of a factory class for the specified ADL
	 * descriptor.
	 *
	 * @param adl         the fully-qualified name of the ADL descriptor
	 * @param targetname  the fully-qualified name of the factory class
	 */
	default void generate( String adl, String targetname )
	throws IOException {
		Map<Object,Object> context = new HashMap<>();
		ComponentDesc<?> cdesc = parse(adl,context);
		generate(cdesc,targetname);
	}

	/**
	 * Generate the source code of a factory class for the specified model of
	 * the ADL.
	 *
	 * @param cdesc       the model of the ADL descriptor
	 * @param targetname  the fully-qualified name of the factory class
	 * @since 2.5
	 */
	public void generate( ComponentDesc<?> cdesc, String targetname )
	throws IOException;
}
