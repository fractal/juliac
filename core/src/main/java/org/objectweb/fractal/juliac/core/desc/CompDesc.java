/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.desc;

import java.util.List;

import org.objectweb.fractal.api.type.ComponentType;

/**
 * A struct-like class for holding data about a component which has been parsed
 * by the Juliac Fractal ADL backend.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.6
 */
public class CompDesc {

	private String id;
	private ComponentType ct;
	private List<String> compIDs;

	public CompDesc( String id, ComponentType ct, List<String> compIDs ) {
		this.id = id;
		this.ct = ct;
		this.compIDs = compIDs;
	}

	/**
	 * Return the identifier associated to this component.
	 *
	 * Identifiers are created by the {@link
	 * org.objectweb.fractal.juliac.adl.stati.JuliacImplementationBuilder}
	 * Fractal ADL backend component. Each identifier corresponds to a variable
	 * name which holds the reference to the component instance.
	 */
	public String getID() {
		return id;
	}

	public ComponentType getCT() {
		return ct;
	}

	/**
	 * Return the identifiers associated with components referenced by this
	 * component. The list also includes the identifiers of the current
	 * component. This list may be <code>null</code> if these identifiers can
	 * not be computed when the current instance is created. This is the case
	 * for example in the {@link
	 * org.objectweb.fractal.juliac.adl.stati.JuliacImplementationBuilder} class
	 * of the <code>plugin/fractaladl</code> module.
	 */
	public List<String> getCompIDs() {
		return compIDs;
	}

	public void setCompIDs( List<String> compIDs ) {
		this.compIDs = compIDs;
	}

	/**
	 * Return the component identifier.
	 */
	@Override
	public String toString() {
		/*
		 * Instances of this class are used by Fractal ADL in replacement of
		 * component identifiers of type String. toString() returns the
		 * identifier to let Fractal ADL output the same result as if simple
		 * identifiers were used.
		 */
		return id;
	}
}
