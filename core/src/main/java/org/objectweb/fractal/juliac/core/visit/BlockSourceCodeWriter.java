/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.core.visit;

import static com.google.common.base.Preconditions.checkState;
import static org.objectweb.fractal.juliac.core.visit.CodeHelper.asString;

import java.io.PrintWriter;

import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.CatchSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ThenSourceCodeVisitor;

import com.google.common.collect.Lists;

/**
 * A {@link BlockSourceCodeVisitor} which generates the source code of a block
 * of code.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 */
public class BlockSourceCodeWriter extends AbstractCodeWriter implements
		BlockSourceCodeVisitor {

	public BlockSourceCodeWriter(PrintWriter pw, int il, boolean indented) {
		super(pw, il, indented);
	}

	/**
	 * Visit the beginning of the block.
	 */
	@Override
	public BlockSourceCodeVisitor visitBegin() {
		checkState(state == BEGIN_STATE,
				"Block beginning cannot be visited at this stage of the visit");

		writeln("{");
		indent++;

		state = BODY_STATE;

		return this;
	}

	private final void checkBodyState(final String text) {
		checkState(state == BODY_STATE, "Block " + text
				+ " cannot be visited at this stage of the visit");
	}

	/**
	 * Visit a piece of the source code of the block.
	 *
	 * @param code the visited piece of code
	 */
	@Override
	public void visit(Object code) {
		checkBodyState("body");

		writer().write(asString(code));
	}

	@Override
	public final BlockSourceCodeVisitor visitln(Object... code) {
		checkBodyState("body");

		writer().write(asString(code)+"\n");

		return this;
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeVisitor#visitComment(java.lang.String)
	 */
	@Override
	public BlockSourceCodeVisitor visitComment(String comment) {
		checkBodyState("body");

		visitln("// "+comment);

		return this;
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeVisitor#visitIns(java.lang.String[])
	 */
	@Override
	public BlockSourceCodeVisitor visitIns(Object... code) {
		checkBodyState("body");

		write(code);
		writeln(";");

		return this;
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeVisitor#visitVar(java.lang.String, java.lang.String, java.lang.String[])
	 */
	@Override
	public BlockSourceCodeVisitor visitVar(Object type, String name,
			Object... code) {
		if (code == null || code.length == 0)
			return visitIns(type, name);
		return visitIns(type, name, "=", asString(code));
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeVisitor#visitSet(java.lang.String, java.lang.String[])
	 */
	@Override
	public BlockSourceCodeVisitor visitSet(String name, Object... code) {
		return visitIns(Lists.asList(name, "=", code).toArray());
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeVisitor#visitIf(java.lang.String[])
	 */
	@Override
	public ThenSourceCodeVisitor visitIf(Object... condition) {
		checkBodyState("if");

		writeln("if", "(", asString(condition), ")");

		return (ThenSourceCodeVisitor) new ThenSourceCodeWriter(writer(), indent)
				.visitBegin();
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeVisitor#visitWhile(java.lang.String[])
	 */
	@Override
	public BlockSourceCodeVisitor visitWhile(Object... condition) {
		checkBodyState("while");

		writeln("while", "(", asString(condition), ")");

		return new BlockSourceCodeWriter(writer(), indent, false).visitBegin();
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeVisitor#visitFor(java.lang.String[])
	 */
	@Override
	public BlockSourceCodeVisitor visitFor(Object... condition) {
		checkBodyState("for");

		writeln("for", "(", asString(condition), ")");

		return new BlockSourceCodeWriter(writer(), indent, false).visitBegin();
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeVisitor#visitTry()
	 */
	@Override
	public CatchSourceCodeVisitor visitTry() {
		checkBodyState("try");

		writeln("try");

		return (CatchSourceCodeVisitor) new CatchSourceCodeWriter(writer(),
				indent).visitBegin();
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeVisitor#visitSync(java.lang.String)
	 */
	@Override
	public BlockSourceCodeVisitor visitSync(String obj) {
		checkBodyState("synchronized");

		writeln("synchronized("+obj+")");

		return new BlockSourceCodeWriter(writer(), indent, false).visitBegin();
	}


	/**
	 * Visit the end of the block.
	 */
	@Override
	public void visitEnd() {
		checkState(state == BODY_STATE,
				"Block end cannot be visited at this stage of the visit");

		indent--;
		writeln("}");
		writeln();

		state = FINAL_STATE;
	}

	// ----------------------------------------------------------------------
	// Visitor state constants
	// ----------------------------------------------------------------------

	protected int state = BEGIN_STATE;
	protected static final int BEGIN_STATE = 0;
	protected static final int BODY_STATE = 1;
	protected static final int FINAL_STATE = 2;
}
