/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.desc;

/**
 * A struct-like class for holding data about the setting of an attribute which
 * has been parsed by the Juliac Fractal ADL backend.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.2
 */
public class AttributeDesc {

	private ComponentDesc<?> component;
	private String attributeController;
	private String name;
	private String value;

	/** @since 2.5 */
	private String type;

	/**
	 * The name of the setter name or <code>null</code> if the attribute is
	 * readonly.
	 *
	 * @since 2.7
	 */
	private String setterName;

	public AttributeDesc(
		ComponentDesc<?> component, String attributeController,
		String name, String value, String type, String setterName ) {

		this.component = component;
		this.attributeController = attributeController;
		this.name = name;
		this.value = value;
		this.type = type;
		this.setterName = setterName;
	}

	public ComponentDesc<?> getComponent() {
		return component;
	}

	public String getAttributeController() {
		return attributeController;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	/** @since 2.5 */
	public String getType() {
		return type;
	}

	/** @since 2.7 */
	public String getSetterName() {
		return setterName;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(name);
		if( type != null ) {
			sb.append('<');
			sb.append(type);
			sb.append('>');
		}
		sb.append('=');
		sb.append(value);
		return sb.toString();
	}
}
