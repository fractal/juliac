/***
 * Juliac
 * Copyright (C) 2016-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.desc;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;

/**
 * Abstract implementation of the {@link ADLParserSupportItf} interface.
 *
 * @param T   the type of the ADL descriptor
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public abstract class AbstractADLSupportImpl<T> implements ADLParserSupportItf {

	// -------------------------------------------------------------------
	// Implementation of the JuliacModuletItf
	// -------------------------------------------------------------------

	protected JuliacItf jc;

	@Override
	public void init( JuliacItf jc ) {
		this.jc = jc;
		jc.register(ADLParserSupportItf.class,this);
	}

	@Override
	public void close( JuliacItf jc ) {
		jc.unregister(ADLParserSupportItf.class,this);
	}


	// -------------------------------------------------------------------
	// Implementation of the ADLParserSupportItf
	// -------------------------------------------------------------------

	@Override
	public ComponentDesc<Object> parse( String adl, Map<Object,Object> context )
	throws IOException {
		T cl = load(adl,context);
		ComponentDesc<Object> cdesc = toComponentDesc(cl);
		return cdesc;
	}

	@Override
	public void generate( String adl, String targetname )
	throws IOException {
		ComponentDesc<Object> cdesc = parse(adl,null);
		generate(cdesc,targetname);
		generate(cdesc);
	}

	@Override
	public void generate( ComponentDesc<?> cdesc, String targetname )
	throws IOException {

		SourceCodeGeneratorItf cg =
			getComponentDescFactoryClassGenerator(cdesc,targetname);
		jc.generateSourceCode(cg);
	}


	// ----------------------------------------------------------------------
	// Methods to be specified or overridden by subclasses
	// ----------------------------------------------------------------------

	/**
	 * Load the specified ADL descriptor.
	 */
	protected abstract T load( String adl, Map<Object,Object> context )
	throws IOException;

	/**
	 * Transform the specified ADL descriptor to {@link ComponentDesc}.
	 */
	protected abstract ComponentDesc<Object> toComponentDesc( T adl )
	throws IOException;

	/**
	 * Return the factory class generator to be used.
	 */
	protected SourceCodeGeneratorItf getComponentDescFactoryClassGenerator(
		ComponentDesc<?> cdesc, String targetname  ) {

		return new ComponentDescFactoryClassGenerator<>(jc,cdesc,targetname);
	}


	// ----------------------------------------------------------------------
	// Implementation specific
	// ----------------------------------------------------------------------

	/**
	 * Recursively generate code for the specified component and its
	 * subcomponents.
	 */
	private void generate( ComponentDesc<Object> cdesc ) throws IOException {

		Object controllerDesc = cdesc.getCtrlDesc();

		FCSourceCodeGeneratorItf fcscg =
			jc.lookupAndFilter(FCSourceCodeGeneratorItf.class,controllerDesc);
		fcscg.generate(cdesc);

		/*
		 * Recurse for subcomponents.
		 */
		List<ComponentDesc<Object>> subs = cdesc.getSubComponents();
		for (ComponentDesc<Object> sub : subs) {
			generate(sub);
		}
   }
}
