/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.visit;

import java.io.IOException;
import java.io.Writer;

import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;

/**
 * This class provides a {@link Writer} stream where characters are outputed in
 * a {@link BlockSourceCodeVisitor}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.1
 */
public class IOWriterMethodSourceCodeWriter extends Writer {

	protected BlockSourceCodeVisitor mv;

	public IOWriterMethodSourceCodeWriter( BlockSourceCodeVisitor mv ) {
		this.mv = mv;
	}

	@Override
	public void close() throws IOException {
		// Indeed empty
	}

	@Override
	public void flush() throws IOException {
		// Indeed empty
	}

	@Override
	public void write( char[] cbuf, int off, int len ) throws IOException {
		String code = new String(cbuf,off,len);
		mv.visit(code);
	}
}
