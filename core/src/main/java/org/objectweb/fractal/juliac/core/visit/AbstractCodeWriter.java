/***
 * Juliac
 * Copyright (C) 2010-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Romain Rouvoy
 * Contributor: Philippe Merle
 */

package org.objectweb.fractal.juliac.core.visit;

import static org.objectweb.fractal.juliac.core.visit.CodeHelper.asString;

import java.io.PrintWriter;

/**
 * An abstract class for generating the source code of classes.
 *
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 * @since 2.3
 */
public abstract class AbstractCodeWriter {

	private final String DEFAULT_INDENT = "  "; // Default indent: 2 space characters
	private final PrintWriter pw;
	protected boolean indented = false;
	protected int indent; // Current indent level

	protected AbstractCodeWriter(PrintWriter pw, int level, boolean indented) {
		this.pw = pw;
		this.indent = level;
		this.indented = indented;
	}

	protected AbstractCodeWriter(PrintWriter pw, int level) {
		this(pw,level,false);
	}

	protected AbstractCodeWriter(PrintWriter pw) {
		this(pw,0);
	}

	protected final PrintWriter writer() {
		return this.pw;
	}


	/**
	 * Write the pieces of code used as parameter into the target file.
	 *
	 * @param code source code to be dumped
	 */
	protected final void write(final Object... code) {
		if (code == null || code.length == 0) {
			return ;
		}
		if (!this.indented) {
			for (int i = 0; i < this.indent; i++) {
				writer().print(DEFAULT_INDENT);
			}
			this.indented = true;
		}
		writer().print(asString(code));
// Ph. Merle: Don't flush after each write in order to improve performance.
//        writer().flush();
	}

	/**
	 * Write the pieces of code used as parameter into the target file.
	 *
	 * @param code source code to be dumped
	 */
	protected final void writeln(final Object... code) {
		write(code);
		writer().println();
		this.indented = false;
	}
}
