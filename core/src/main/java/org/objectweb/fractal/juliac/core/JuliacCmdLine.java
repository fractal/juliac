/***
 * Juliac
 * Copyright (C) 2017-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Victor Noel
 */

package org.objectweb.fractal.juliac.core;

import static java.io.File.pathSeparator;
import static java.util.logging.Level.FINE;
import static org.objectweb.fractal.juliac.core.CmdLineFlags.DEBUG;
import static org.objectweb.fractal.juliac.core.CmdLineOptions.BASEDIR;
import static org.objectweb.fractal.juliac.core.CmdLineOptions.GENCLASS;
import static org.objectweb.fractal.juliac.core.CmdLineOptions.GENSRC;
import static org.objectweb.fractal.juliac.core.CmdLineOptions.JULIACFGFILES;
import static org.objectweb.fractal.juliac.core.CmdLineOptions.MIXINS;
import static org.objectweb.fractal.juliac.core.CmdLineOptions.PKGROOT;
import static org.objectweb.fractal.juliac.core.CmdLineOptions.SRCS;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;
import java.util.logging.Logger;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.JuliacModuleItf;
import org.objectweb.fractal.juliac.commons.util.CmdLineArgs;

/**
 * The Juliac command line entry point.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.7
 */
public class JuliacCmdLine {

	public static void main( String[] args )
	throws IOException, InstantiationException {

		if( args.length == 0 ) {
			usage();
			return;
		}

		/*
		 * Register the command line options and flags.
		 */
		CmdLineArgs<CmdLineFlags,CmdLineOptions> cla = new CmdLineArgs<>();
		CmdLineOptions[] options = CmdLineOptions.values();
		for (CmdLineOptions option : options) {
			cla.registerOption(option);
		}
		cla.registerFlags(CmdLineFlags.values());

		JuliacItf jc = new Juliac();
		Logger logger = jc.getLogger();

		/*
		 * Analyze the command line arguments.
		 */
		cla.parse(args);

		/*
		 * Set the base directory for locations specified in the parameters.
		 */
		String baseDir = cla.getOptionValue(BASEDIR);
		if( baseDir != null ) {
			File f = new File(baseDir);
			jc.setBaseDir(f);
		}

		/*
		 * Retrieve other options and flags.
		 * Assume default values when missing.
		 */
		String s = cla.getOptionValue(SRCS);
		String[] srcs = s==null ? new String[0] : s.split(pathSeparator);
		jc.addSrcs(srcs);

		s = cla.getOptionValue(MIXINS);
		String[] srclibs = s==null ? new String[0] : s.split(pathSeparator);
		jc.addSrclibs(srclibs);

		String pkgRoot = cla.getOptionValue(PKGROOT);
		if( pkgRoot != null ) {
			jc.setPkgRoot(pkgRoot);
		}

		String genDirName = cla.getOptionValue(GENSRC);
		if( genDirName != null ) {
			jc.setGenDirName(genDirName);
		}

		String classDirName = cla.getOptionValue(GENCLASS);
		if( classDirName != null ) {
			jc.setClassDirName(classDirName);
		}

		boolean debug = cla.isFlagSet(DEBUG);
		if(debug) {
			Juliac.setLevel(logger,FINE);
		}

		String juliaCfgFiles = cla.getOptionValue(JULIACFGFILES);
		if( juliaCfgFiles != null ) {
			jc.setJuliaCfgFiles(juliaCfgFiles);
		}

		/*
		 * Retrieve the ADL files or membrane descriptors which must be
		 * generated.
		 */
		List<String> files = cla.getFiles();

		/*
		 * Log information about the classpath.
		 */
		logger.fine("Juliac classpath:");
		ClassLoader cl = Juliac.class.getClassLoader();
		if( cl instanceof URLClassLoader ) {
			URL[] urls = ((URLClassLoader) cl).getURLs();
			for (URL url : urls) {
				logger.fine("  "+url);
			}
		}
		else {
			final String msg =
				"Class loader for the Juliac class is not a URLClassLoader";
			logger.fine(msg);
		}
		logger.fine("-----------------");

		/*
		 * Load user specified JuliacModuleItf Java services.
		 * Perform after compiling since some may need to be compiled.
		 */
		jc.lookup(JuliacModuleItf.class);

		/*
		 * Generate the source code.
		 */
		for (String file : files) {
			logger.info(file+"...");

			/*
			 * file may be either a membrane descriptor, an ADL descriptor, or a
			 * list of component descriptors. Handle membrane descriptors first
			 * in case the ADL and component descriptors use it.
			 */
			try {
				// file is a membrane descriptor
				jc.generate(file);
			}
			catch( IllegalArgumentException iae ) {
				try {
					// file is an ADL descriptor
					jc.generate(file,file);
				}
				catch( IllegalArgumentException iae2 ) {
					try {
						// file is a list of component descriptors
						String[] descs = file.split(":");
						for (String desc : descs) {
							jc.generateForDesc(desc);
						}
					}
					catch( IllegalArgumentException iae3 ) {
						logger.log(FINE,"Exception for membrane descriptor",iae);
						logger.log(FINE,"Exception for ADL descriptor",iae2);
						logger.log(FINE,"Exception for component descriptor",iae3);
						final String msg =
							file+" should be either a membrane descriptor, "+
							"an ADL descriptor, or a list of component "+
							"descriptors";
						throw new IllegalArgumentException(msg);
					}
				}
			}
		}

		/*
		 * Close Juliac.
		 */
		jc.close();
		logger.info("Done.");
	}

	/**
	 * Display command line usage.
	 */
	private static void usage() {

		JuliacItf jc = new Juliac();
		Logger logger = jc.getLogger();

		logger.info("java "+JuliacCmdLine.class.getName()+" [options] types");
		logger.info("Generate the Java source code associated with the specified ADL, membrane or component descriptors");
		logger.info("");

		logger.info("Options:");
		logger.info("");
		logger.info("  --debug                 : report debug information");
		logger.info("  --gensrc <dir>          : directory for generated source code (default: "+Juliac.DEFAULT_GEN_DIR_NAME+")");
		logger.info("  --genclass <dir>        : directory for compiled code (default: "+Juliac.DEFAULT_CLASS_DIR_NAME+")");
		logger.info("  --juliaCfgFiles <files> : comma-separated list of Julia configuration files");
		logger.info("  --mixins <dirs>         : mixin layers");
		logger.info("  --pkgRoot <name>        : root package for generated code");
		logger.info("  --srcs <dirs>           : source files (default: src/main/java)");
		logger.info("");

		logger.info("Details:");
		logger.info("  <dirs> is a colon-separated list of directories or jar files");
	}
}
