/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.desc;

import java.util.HashMap;
import java.util.Map;

/**
 * Binding types (normal, export, import) as defined by {@link BindingBuilder}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2
 */
public enum BindingType {

	/** Normal binding as defined by BindingBuilder.NORMAL_BINDING */
	NORMAL(0),

	/** Export binding as defined by BindingBuilder.EXPORT_BINDING. */
	EXPORT(1),

	/** Import binding as defined by BindingBuilder.IMPORT_BINDING. */
	IMPORT(2);

	/** @since 2.8 */
	private int value;

	/** @since 2.8 */
	private BindingType( int value ) {
		this.value = value;
	}

	/**
	 * Return the binding type for the specified integer value in accordance
	 * with the constant values defined in {@link BindingBuilder} for binding
	 * types.
	 *
	 * @param value  the integer value
	 * @return       the corresponding binding type
	 * @throws IllegalArgumentException
	 *      if there is no such binding type for the specified value
	 */
	public static BindingType getType( int value ) {

		// Initialize map
		if( map == null ) {
			map = new HashMap<>();
			BindingType[] bts = BindingType.values();
			for (BindingType bt : bts) {
				map.put(bt.value,bt);
			}
		}

		if( ! map.containsKey(value) ) {
			final String msg = "No binding type for value: "+value;
			throw new IllegalArgumentException(msg);
		}

		BindingType bt = map.get(value);
		return bt;
	}

	/**
	 * The association between constants defined in BindingBuilder and
	 * BindingType enums.
	 */
	private static Map<Integer,BindingType> map;
}
