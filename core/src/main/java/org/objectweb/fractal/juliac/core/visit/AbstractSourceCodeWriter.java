/***
 * Juliac
 * Copyright (C) 2010-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.core.visit;

import java.io.PrintWriter;

import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.CatchSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ThenSourceCodeVisitor;

import com.google.common.base.Preconditions;

/**
 * A {@link BlockSourceCodeVisitor} which generates the source code of an abstract block
 * of code.
 *
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 * @since 2.3
 */
public class AbstractSourceCodeWriter extends AbstractCodeWriter implements BlockSourceCodeVisitor {

	public AbstractSourceCodeWriter(PrintWriter writer) {
		super(writer,1);
	}

	private final BlockSourceCodeVisitor illegalState() {
		throw new IllegalStateException("Block body cannot be visited at this stage of the visit");
	}

	/**
	 * Visit the beginning of the block.
	 */
	@Override
	public BlockSourceCodeVisitor visitBegin() {
		Preconditions.checkState(state == BEGIN_STATE, "Block beginning cannot be visited at this stage of the visit");

		writeln(";");
		state = FINAL_STATE;
		return this;
	}

	/**
	 * Visit a piece of the source code of the block.
	 *
	 * @param code  the visited piece of code
	 */
	@Override
	public void visit( Object code ) {
		illegalState();
	}

	/**
	 * Visit a line of the source code of the block.
	 *
	 * @param code  the visited piece of code
	 */
	@Override
	public BlockSourceCodeVisitor visitln(Object... code ) {
		return illegalState();
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeVisitor#visitComment(java.lang.String)
	 */
	@Override
	public BlockSourceCodeVisitor visitComment(String comment) {
		return illegalState();
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeVisitor#visitVar(java.lang.String, java.lang.String, java.lang.String[])
	 */
	@Override
	public BlockSourceCodeVisitor visitVar(Object type, String name,
			Object... code) {
		return illegalState();
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeVisitor#visitIns(java.lang.String[])
	 */
	@Override
	public BlockSourceCodeVisitor visitIns(Object... code) {
		return illegalState();
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeVisitor#visitSet(java.lang.String, java.lang.String[])
	 */
	@Override
	public BlockSourceCodeVisitor visitSet(String name, Object... code) {
		return illegalState();
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeVisitor#visitIf(java.lang.String[])
	 */
	@Override
	public ThenSourceCodeVisitor visitIf(Object... condition) {
		return (ThenSourceCodeVisitor) illegalState();
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeVisitor#visitWhile(java.lang.String[])
	 */
	@Override
	public BlockSourceCodeVisitor visitWhile(Object... condition) {
		return illegalState();
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeVisitor#visitFor(java.lang.String[])
	 */
	@Override
	public BlockSourceCodeVisitor visitFor(Object... condition) {
		return illegalState();
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeVisitor#visitTry()
	 */
	@Override
	public CatchSourceCodeVisitor visitTry() {
		return (CatchSourceCodeVisitor) illegalState();
	}

	@Override
	public BlockSourceCodeVisitor visitSync(String obj) {
		return illegalState();
	}

	/**
	 * Visit the end of the block.
	 */
	@Override
	public void visitEnd() {
	}


	// ----------------------------------------------------------------------
	// Visitor state constants
	// ----------------------------------------------------------------------

	private int state = BEGIN_STATE;
	private static final int BEGIN_STATE = 0;
	private static final int FINAL_STATE = 2;
}
