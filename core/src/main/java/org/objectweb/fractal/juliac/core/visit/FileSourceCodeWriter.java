/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.core.visit;

import static com.google.common.base.Preconditions.checkState;

import java.io.PrintWriter;

import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.FileSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.InterfaceSourceCodeVisitor;

/**
 * A {@link FileSourceCodeVisitor} that generates the content of a source code
 * file.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 * @since 1.1
 */
public class FileSourceCodeWriter extends AbstractCodeWriter
implements FileSourceCodeVisitor {

	public FileSourceCodeWriter( PrintWriter pw ) {
		super(pw);
	}

	@Override
	public void visitComment( String comment ) {
		writeln(comment);
	}

	@Override
	public void visitPackageName( String name ) {

		checkState(
			state == STATES.PACKAGE_NAME_STATE,
			"Package name cannot be visited at this stage of the visit" );

		writeln();
		writeln("package",name+";");
		writeln();

		state = STATES.IMPORTS_STATE;
	}

	@Override
	public void visitImport( String name ) {

		if( state.ordinal() < STATES.IMPORTS_STATE.ordinal() ) {
			state = STATES.IMPORTS_STATE;
		}

		checkState(
			state == STATES.IMPORTS_STATE,
			"Imports cannot be visited at this stage of the visit" );

		writeln("import",name+";");
	}

	@Override
	public ClassSourceCodeVisitor visitPublicClass() {

		if( state.ordinal() < STATES.BODY_STATE.ordinal() ) {
			state = STATES.BODY_STATE;
		}

		checkState(
			state == STATES.BODY_STATE,
			"Class declaration cannot be visited at this stage of the visit" );

		state = STATES.END_STATE;
		return new ClassSourceCodeWriter(writer());
	}

	@Override
	public InterfaceSourceCodeVisitor visitPublicInterface() {

	  if( state.ordinal() < STATES.BODY_STATE.ordinal() ) {
		  state = STATES.BODY_STATE;
	  }

	  checkState(
		  state == STATES.BODY_STATE,
		  "Interface declaration cannot be visited at this stage of the visit" );

	  state = STATES.END_STATE;
	  return new InterfaceSourceCodeWriter(writer());
	}


	// ----------------------------------------------------------------------
	// Visitor state constants
	// ----------------------------------------------------------------------

	private STATES state = STATES.PACKAGE_NAME_STATE;

	private static enum STATES {
		PACKAGE_NAME_STATE,
		IMPORTS_STATE,
		BODY_STATE,
		END_STATE
	};
}
