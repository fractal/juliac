/***
 * Juliac
 * Copyright (C) 2010-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.core.visit;

import static com.google.common.base.Preconditions.checkState;

import java.io.PrintWriter;

import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ThenSourceCodeVisitor;

/**
 * A {@link ThenSourceCodeWriter} which generates the source code of
 * <code>then</code> block of code.
 *
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 * @since 2.3
 */
public class ThenSourceCodeWriter extends BlockSourceCodeWriter
implements ThenSourceCodeVisitor {

	public ThenSourceCodeWriter(PrintWriter pw, int il) {
		super(pw, il, false);
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.IfSourceCodeVisitor#visitElse()
	 */
	@Override
	public BlockSourceCodeVisitor visitElse() {
		visitEnd();
		writeln("else");
		return new BlockSourceCodeWriter(writer(), indent, false).visitBegin();
	}

	/* (non-Javadoc)
	 * @see org.objectweb.fractal.juliac.visit.BlockSourceCodeWriter#visitEnd()
	 */
	@Override
	public void visitEnd() {
		checkState(state == BODY_STATE,
				"Block end cannot be visited at this stage of the visit");

		indent--;
		writeln("}");

		state = FINAL_STATE;
	}
}
