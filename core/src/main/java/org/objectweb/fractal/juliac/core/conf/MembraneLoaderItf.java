/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.conf;

import java.util.List;

import javax.lang.model.element.TypeElement;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.JuliacModuleItf;
import org.objectweb.fractal.juliac.api.desc.NoSuchControllerDescriptorException;
import org.objectweb.fractal.juliac.core.desc.ControllerDesc;

/**
 * Interface for retrieving membrane configuration data.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.3
 */
public interface MembraneLoaderItf extends JuliacModuleItf {

	/**
	 * <p>
	 * Register the specified membrane definition with the specified controller
	 * descriptor.
	 * </p>
	 *
	 * <p>The membrane definition is:</p>
	 * <ul>
	 * <li>a @{@link Membrane}-annotated {@link Class} in Tinfi and Adlet,</li>
	 * <li>a @{@link Membrane}-annotated {@link TypeElement} in Adlet,</li>
	 * <li>unused in the rest of Juliac that relies on Julia <code>.cfg</code>
	 *     files for loading membrane definitions and where this method throws
	 *     {@link UnsupportedOperationException}.</li>
	 * </ul>
	 *
	 * @param ctrlDesc  the controller descriptor
	 * @param cl        the membrane definition
	 * @return
	 *         the membrane definition previously associated with the controller
	 *         descriptor or <code>null</null> if no such controller descriptor was
	 *         registered before
	 * @throws IllegalArgumentException
	 *         if the specified membrane definition is not valid
	 * @since 2.5
	 */
	public Object put( String ctrlDesc, Object cl );

	/**
	 * Return the membrane definition associated to the specified controller
	 * descriptor. Return <code>null</code> if no such controller membrane
	 * definition has been registered.
	 *
	 * @since 2.8
	 */
	public Object get( String ctrlDesc );

	/**
	 * Return <code>true</code> if the specified membrane descriptor exists.
	 */
	public boolean containsKey( String ctrlDesc );

	/**
	 * Return the type of the membrane associated with the specified controller
	 * descriptor. The component type is needed for parametric membranes. In
	 * this case, the attribute controller implementation depends on the
	 * attribute control interfaces which is specified in the component type.
	 *
	 * @param ct        the component type
	 * @param ctrlDesc  the control membrane descriptor
	 * @throws NoSuchControllerDescriptorException
	 *         if the specified controller descriptor does not exist
	 */
	public InterfaceType[] getMembraneType( ComponentType ct, String ctrlDesc );

	/**
	 * Return the list of controller descriptors associated with the specified
	 * control membrane descriptor and component type.
	 *
	 * @param ct        the component type
	 * @param ctrlDesc  the control membrane descriptor
	 * @throws NoSuchControllerDescriptorException
	 *         if the specified controller descriptor does not exist
	 */
	public List<ControllerDesc> getCtrlImplLayers(
		ComponentType ct, String ctrlDesc );

	/**
	 * Return the class name of the interceptor class generator associated with
	 * the specified controller descriptor and component type. Return
	 * <code>null</code> if there is no such interceptor class generator.
	 *
	 * @param ctrlDesc  the control membrane descriptor
	 * @throws NoSuchControllerDescriptorException
	 *         if the specified controller descriptor does not exist
	 */
	public String getInterceptorClassGeneratorName( String ctrlDesc );

	/**
	 * Return the array of interceptor code generator class names associated
	 * with the specified controller descriptor and component type. Return
	 * <code>null</code> if there is no such interceptor code generator.
	 *
	 * @param ctrlDesc  the control membrane descriptor
	 * @throws NoSuchControllerDescriptorException
	 *         if the specified controller descriptor does not exist
	 */
	public String[] getInterceptorSourceCodeGeneratorNames( String ctrlDesc );
}
