/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.desc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.type.BasicComponentType;

/**
 * This class stores the model of a component. Component may be defined by ADL,
 * e.g. Fractal ADL or the SCA ADL, or by annotated Java classes in the case of
 * the Adlet extension.
 *
 * @param <CT>  the type of the code element for this component definition
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class ComponentDesc<CT> {

	private String id;
	private String name;
	private String definition;
	private ComponentType ct;
	private String ctrlDesc;
	private String contentClassName;
	private CT source;

	private Map<String,AttributeDesc> attributes = new HashMap<>();
	private List<ComponentDesc<CT>> superComponents = new ArrayList<>();
	private List<ComponentDesc<CT>> subComponents = new ArrayList<>();

	/*
	 * Bindings are redundantly stored in a map and in a list to provide an
	 * access based either on client interface names or on the order in which
	 * the bindings were registered with this component.
	 */

	private Map<String,BindingDesc> bindings = new HashMap<>();
	private List<BindingDesc> lbindings = new ArrayList<>();

	/**
	 * @param id
	 *      the component identifier (the name of the variable which stores the
	 *      component reference)
	 * @param name        the component name
	 * @param definition  the fully-qualified name of the file defining the component
	 * @param ct          the component type
	 * @param ctrlDesc    the controller descriptor
	 * @param contentClassName
	 *      the component implementation (may be <code>null</code> for composite
	 *      components)
	 * @param source      the code element for this component definition
	 */
	public ComponentDesc(
		String id, String name, String definition,
		ComponentType ct, String ctrlDesc, String contentClassName, CT source ) {

		this.id = id;
		this.name = name;
		this.definition = definition;
		this.ct = ct;
		this.ctrlDesc = ctrlDesc;
		this.contentClassName = contentClassName;
		this.source = source;
	}

	/**
	 * Return the identifier associated to this component.
	 *
	 * Identifiers are created by the {@link
	 * org.objectweb.fractal.juliac.adl.model.JuliacImplementationBuilder}
	 * Fractal ADL backend component. Each identifier corresponds to a variable
	 * name which holds the reference to the component instance.
	 */
	public String getID() { return id; }

	public String getName() { return name; }
	public void setName( String name ) { this.name = name; }

	public String getDefinition() { return definition; }

	public ComponentType getCT() { return ct; }
	public void setComponentType( ComponentType ct ) { this.ct = ct; }

	public String getCtrlDesc() { return ctrlDesc; }
	public void setCtrlDesc( String ctrlDesc ) { this.ctrlDesc = ctrlDesc; }

	public String getContentClassName() { return contentClassName; }
	public void setContentClassName( String s ) { this.contentClassName = s; }

	public CT getSource() { return source; }

	/**
	 * Return the component identifier.
	 */
	@Override
	public String toString() {
		/*
		 * Instances of this class are used by Fractal ADL in replacement of
		 * component identifiers of type String. toString() returns the
		 * identifier to let Fractal ADL output the same result as if simple
		 * identifiers were used.
		 */
		return id;
	}

	/**
	 * Add the specified array of interface types to the current component type.
	 *
	 * @throws InstantiationException
	 *      if the specified interface types introduce inconsistencies in the
	 *      current component type
	 */
	public void addInterfaceTypes( InterfaceType[] its )
	throws InstantiationException {
		InterfaceType[] currentits = ct.getFcInterfaceTypes();
		InterfaceType[] newits =
			new InterfaceType[ currentits.length + its.length ];
		System.arraycopy(currentits,0,newits,0,currentits.length);
		System.arraycopy(its,0,newits,currentits.length,its.length);
		ct = new BasicComponentType(newits);
	}


	// ------------------------------------------------------------------
	// Attributes
	// ------------------------------------------------------------------

	public void putAttribute( String name, AttributeDesc adesc ) {
		attributes.put(name,adesc);
	}

	public AttributeDesc getAttribute( String name ) {
		return attributes.get(name);
	}

	public Set<String> getAttributeNames() {
		return attributes.keySet();
	}


	// ------------------------------------------------------------------
	// Bindings
	// ------------------------------------------------------------------

	/**
	 * Add a new binding originating from this component.
	 *
	 * @param cltItfName  the client interface name
	 * @param bdesc       the binding descriptor
	 * @throws IllegalArgumentException
	 *      if <code>cltItfName</code> and the value returned by
	 *      <code>bdesc.getCltItfName()</code> differ
	 */
	public void putBinding( String cltItfName, BindingDesc bdesc ) {

		/*
		 * Defensive programming. Precondition checking.
		 */
		String bdescCltItfName = bdesc.getCltItfName();
		if( ! cltItfName.equals(bdescCltItfName) ) {
			String msg =
				"cltItfName ("+cltItfName+
				") and the value returned by bdesc.getCltItfName ("+
				bdescCltItfName+") differ";
			throw new IllegalArgumentException(msg);
		}

		bindings.put(cltItfName,bdesc);
		lbindings.add(bdesc);
	}

	public BindingDesc getBinding( String cltIftName ) {
		BindingDesc bdesc = bindings.get(cltIftName);
		return bdesc;
	}

	public Set<String> getBindingCltItfNames() {
		return bindings.keySet();
	}

	/**
	 * Return the bindings sorted in the order in which they were registered
	 * with this component.
	 */
	public List<BindingDesc> getBindingDescs() {
		return lbindings;
	}

	/**
	 * Return the {@link ComponentDesc} bound to the specified interface name.
	 * Return <code>null</code> if there is no component bound to the specified
	 * interface name or if no such interface is associated to the current
	 * component.
	 */
	public ComponentDesc<?> getBoundComponent( String name ) {
		BindingDesc bdesc = bindings.get(name);
		if( bdesc == null ) {
			return null;
		}
		ComponentDesc<?> cdesc = bdesc.getSrv();
		return cdesc;
	}

	/**
	 * Return the primitive or parametricPrimitive {@link ComponentDesc} bound
	 * to the specified interface name. Return <code>null</code> if there is no
	 * such component or if no such interface is associated to the current
	 * component.
	 */
	public ComponentDesc<?> getBoundPrimitiveComponent( String name ) {

		BindingDesc bdesc = bindings.get(name);
		if( bdesc == null ) {
			return null;
		}

		ComponentDesc<?> srv = bdesc.getSrv();

		if( srv.getSubComponents().size() == 0 ) {
			return srv;
		}
		else {
			String srvItfName = bdesc.getSrvItfName();
			ComponentDesc<?> bound = srv.getBoundPrimitiveComponent(srvItfName);
			return bound;
		}
	}

	/**
	 * Return the {@link ComponentDesc} associated with the specified controller
	 * descriptor and bound to the specified interface name. Return
	 * <code>null</code> if there is no component bound to the specified
	 * interface name or if no such interface is associated to the current
	 * component.
	 */
	public ComponentDesc<?> getBoundComponent( String name, String ctrlDesc ) {

		BindingDesc bdesc = bindings.get(name);
		if( bdesc == null ) {
			return null;
		}

		String srvItfName = bdesc.getSrvItfName();
		ComponentDesc<?> srv = bdesc.getSrv();
		String srvCtrlDesc = srv.getCtrlDesc();

		if( srvCtrlDesc.equals(ctrlDesc) ) {
			return srv;
		}
		else {
			ComponentDesc<?> bound = srv.getBoundComponent(srvItfName,ctrlDesc);
			return bound;
		}
	}


	// ------------------------------------------------------------------
	// Super and sub components
	// ------------------------------------------------------------------

	public void addSuperComponent( ComponentDesc<CT> parent ) {
		superComponents.add(parent);
	}

	public void addSubComponent( ComponentDesc<CT> sub ) {
		subComponents.add(sub);
	}

	public List<ComponentDesc<CT>> getSuperComponents() {
		return superComponents;
	}

	public List<ComponentDesc<CT>> getSubComponents() {
		return subComponents;
	}
}
