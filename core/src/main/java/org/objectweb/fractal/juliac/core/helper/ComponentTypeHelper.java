/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.helper;

import java.util.Set;
import java.util.TreeSet;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.JuliacRuntimeException;

/**
 * This class provides helper methods for the {@link ComponentType} interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 */
public class ComponentTypeHelper {

	/**
	 * Return a stringified hash code for the specified {@link ComponentType}.
	 */
	public static String hexhash( ComponentType ct ) {

		/*
		 * Sort interface types in the alphabetical order of their name. The
		 * idea is that, since component types [I,J] and [J,I] are the same,
		 * the same hash should be returned, in order to avoid duplicate
		 * unecessary code generation.
		 *
		 * This fix has been introduced in Juliac 2.4 on April 1, 2011,
		 * following a discussion with Philippe that requests this feature in
		 * FraSCAti when modules are merged to produce a component, but that the
		 * merge order leads to different component types, although only one
		 * component type exists (but with interfaces sorted in different
		 * orders.)
		 */

		InterfaceType[] its = ct.getFcInterfaceTypes();
		Set<String> names = new TreeSet<>();
		for (InterfaceType it : its) {
			String name = it.getFcItfName();
			names.add(name);
		}

		StringBuilder sct = new StringBuilder("[");
		boolean first = true;
		for (String name : names) {
			InterfaceType it = null;
			try {
				it = ct.getFcInterfaceType(name);
			}
			catch (NoSuchInterfaceException nsie) {
				// Shouldn't occur since name if one the interface names from ct
				throw new JuliacRuntimeException(nsie);
			}
			if(first) { first = false; }
			else { sct.append(','); }
			sct.append(it.toString());
		}
		sct.append(']');

		/*
		 * toString() mandatory to compute the hash on the String, not the
		 * StringBuilder. 2 Strings with the same content have the same hash
		 * code, whereas 2 StringBuilders with the same content are considered as
		 * different objects, and do not share the same hash code.
		 */
		int hashct = sct.toString().hashCode();
		String hexhashct = Integer.toHexString(hashct);
		return hexhashct;
	}

	/**
	 * Return a string containing the Java code to create a {@link
	 * ComponentType} similar to the specified parameter.
	 */
	public static StringBuilder javaify( ComponentType ct ) {
		InterfaceType[] its = ct.getFcInterfaceTypes();
		return InterfaceTypeHelper.javaify(its);
	}
}
