/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.opt;

import org.objectweb.fractal.juliac.api.generator.ClassGeneratorItf;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.FileSourceCodeVisitor;

/**
 * Default implementation for class generators.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.2.2
 */
public abstract class ClassGenerator extends TypeGenerator
implements ClassGeneratorItf {

	@Override
	public void generate( FileSourceCodeVisitor fv ) {

		super.generate(fv);

		ClassSourceCodeVisitor cv = fv.visitPublicClass();
		generate(cv);
	}

	@Override
	public void generate( ClassSourceCodeVisitor cv ) {

		int modifiers = getTypeModifiers();
		String name = getSimpleTypeName();
		String[] genericTypeParameters = getGenericTypeParameters();
		String superClassName = getSuperClassName();
		String[] implementedInterfaceNames = getImplementedInterfaceNames();

		cv.visit(
			modifiers, name, genericTypeParameters, superClassName,
			implementedInterfaceNames );

		generateFields(cv);
		generateConstructors(cv);
		generateMethods(cv);

		/*
		 * Generate static blocks after fields as the Java compiler complains
		 * ("Cannot reference a field before it is defined") if the static block
		 * uses a static field which has not yet been defined.
		 */
		generateStaticParts(cv);

		cv.visitEnd();
	}

	@Override public String getSuperClassName() { return null; }
	@Override public String[] getImplementedInterfaceNames() { return null; }

	@Override public void generateStaticParts( ClassSourceCodeVisitor cv ) {}
	@Override public void generateFields( ClassSourceCodeVisitor cv ) {}
	@Override public void generateConstructors( ClassSourceCodeVisitor cv ) {}
	@Override public void generateMethods( ClassSourceCodeVisitor cv ) {}
}
