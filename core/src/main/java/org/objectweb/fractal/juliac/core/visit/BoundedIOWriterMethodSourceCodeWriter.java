/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.juliac.core.visit;

import java.io.IOException;
import java.lang.reflect.Modifier;

import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;

/**
 * This class is used by Juliac for generating large component factories. The
 * idea is to overcome the 64KB limit which exists for Java methods. This limit
 * is reached when trying to generate the factory method for an assembly of
 * around 1,000 components. Such a factory consists of around 4,000 lines of
 * Java code. To overcome the limit, this class splits the factory in methods
 * with 1,000 lines of Java code.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@univ-lille.fr>
 * @since 2.1.4
 */
public class BoundedIOWriterMethodSourceCodeWriter
extends IOWriterMethodSourceCodeWriter {

	public BoundedIOWriterMethodSourceCodeWriter( ClassSourceCodeVisitor cv ) {
		super(null);
		this.cv = cv;
	}

	@Override
	public void write( char[] cbuf, int off, int len ) throws IOException {

		/*
		 * New group of COUNT lines.
		 */
		if( line == 0 ) {

			if( newFcInstance > 0 ) {
				// Close the previous newFcInstance999 method
				endOfMethod();
			}

			// Declare the new newFcInstance999 method
			mv = cv.visitMethod(
				Modifier.PRIVATE, null, "void", "newFcInstance"+newFcInstance,
				null, new String[]{Exception.class.getName()} );
			newFcInstance++;
			line = 1;   // Force line to be non zero (the 1st call to write may not contain \n
		}

		/*
		 * Dump the current code.
		 */
		super.write(cbuf,off,len);

		for (int i = 0; i < len; i++) {
			if( cbuf[off+i] == '\n' ) {
				line++;
			}
		}
		if( line >= MAX ) {
			/*
			 * We may go beyond the limit of MAX lines if several lines are
			 * written at once. But that shouldn't be such an important issue.
			 */
			line = 0;
		}
	}

	@Override
	public void close() throws IOException {
		if( mv != null ) {
			endOfMethod();
		}
		super.close();
	}

	public int getNewFcInstanceMethodCount() {
		return newFcInstance;
	}


	// ----------------------------------------------------------------------
	// Instance data
	// ----------------------------------------------------------------------

	/** The visitor for generating the code of the current class. */
	private ClassSourceCodeVisitor cv;

	/** The index of the current newFcInstance999 method. */
	private int newFcInstance = 0;

	/** The number of lines in the current newFcInstance999 method. */
	private int line = 0;

	/** Maximun number of lines per method. */
	private static final int MAX = 1000;


	// ----------------------------------------------------------------------
	// Implementation specific
	// ----------------------------------------------------------------------

	/**
	 * Generate the trailing code for a newFcInstance999 method.
	 */
	private void endOfMethod() {

		/*
		 * Closes the previous newFcInstance999 method.
		 */
		mv.visitEnd();
		mv = null;
	}
}
