/***
 * Juliac
 * Copyright (C) 2007-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core;

/**
 * This class stores constant values used by Juliac.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1
 */
public class Constants {

	public static final String JULIAC_RUNTIME_EXCEPTION = "org.objectweb.fractal.juliac.runtime.RuntimeException";
	public static final String JULIAC_RUNTIME_FACTORY = "org.objectweb.fractal.juliac.runtime.Factory";
	public static final String JULIAC_RUNTIME_MEMBRANE_INITIALIZER = "org.objectweb.fractal.juliac.runtime.MembraneInitializer";

	/**
	 * The package prefix used by Juliac for code which special needs. This
	 * includes:
	 *
	 * <ul>
	 * <li>initalizer classes for component without a content class (such as
	 *     composites),</li>
	 * <li>Fractal interface and interceptor classes for types with a reserved
	 *     package name (starting with <code>java.</code> or
	 *     <code>javax.</code>),</li>
	 * <li>controller implementation classes.</li>
	 * </ul>
	 *
	 * The value of this constant must be the same as the one of {@link
	 * org.objectweb.fractal.juliac.runtime.Juliac#JULIAC_GENERATED} defined in
	 * the juliac/runtime/oo module.
	 */
	public static final String JULIAC_RUNTIME_GENERATED = "juliac";
}
