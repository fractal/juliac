/***
 * Juliac
 * Copyright (C) 2015-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.opt;

import org.objectweb.fractal.juliac.api.visit.FileSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.InterfaceSourceCodeVisitor;

/**
 * Default implementation for interface generators.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.6
 */
public abstract class InterfaceGenerator extends TypeGenerator {

	@Override
	public void generate( FileSourceCodeVisitor fv ) {
		super.generate(fv);
		InterfaceSourceCodeVisitor iv = fv.visitPublicInterface();
		generate(iv);
	}

	protected void generate( InterfaceSourceCodeVisitor iv ) {
		int modifiers = getTypeModifiers();
		String name = getSimpleTypeName();
		String[] genericTypeParameters = getGenericTypeParameters();
		String[] extendedInterfaceNames = getExtendedInterfaceNames();
		iv.visit(modifiers,name,genericTypeParameters,extendedInterfaceNames);

		generateFields(iv);
		generateMethods(iv);

		iv.visitEnd();
	}

	protected void generateFields( InterfaceSourceCodeVisitor iv ) {}
	protected void generateMethods( InterfaceSourceCodeVisitor iv ) {}

	protected String[] getExtendedInterfaceNames() { return null; }
}
