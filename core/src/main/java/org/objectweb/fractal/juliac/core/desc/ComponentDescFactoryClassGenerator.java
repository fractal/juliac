/***
 * Juliac
 * Copyright (C) 2009-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.desc;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.juliac.api.JuliacItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.commons.lang.ClassHelper;
import org.objectweb.fractal.juliac.core.opt.AbstractComponentFactoryClassGenerator;
import org.objectweb.fractal.juliac.core.opt.FCSourceCodeGeneratorItf;
import org.objectweb.fractal.util.Fractal;

/**
 * This class generates the source code of a component {@link
 * org.objectweb.fractal.api.factory.Factory}. This source code generator visits
 * a model of an ADL file.
 *
 * @param CT  the type of the component descriptor
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 2.1.6
 */
public class ComponentDescFactoryClassGenerator<CT>
extends AbstractComponentFactoryClassGenerator {

	private JuliacItf jc;
	private ComponentDesc<CT> cdesc;

	public ComponentDescFactoryClassGenerator(
		JuliacItf jc, ComponentDesc<CT> cdesc, String targetname ) {

		super(targetname);
		this.jc = jc;
		this.cdesc = cdesc;
	}

	/**
	 * Generate the <code>newFcInstance999</code> methods.
	 *
	 * This method is invoked from
	 * InitializerUltraCompCtrlClassGenerator#generateNFICMInstantiateMembrane
	 * and is thus declared as public.
	 *
	 * @param pw     the print writer where the factory is to be outputted
	 * @param declareLocalVars
	 *      <code>true</code> if the variables which hold references to
	 *      components need to be declared
	 */
	@Override
	public CompDesc generateNewFcInstanceMethods(
		PrintWriter pw, boolean declareLocalVars )
	throws IOException {

		/*
		 * Generate the code of the factory.
		 */
		generate(pw,declareLocalVars);

		/*
		 * Traverse the architecture to retrieve component identifiers.
		 */
		List<String> compIDs = new ArrayList<>();
		addCompID(cdesc,compIDs);

		String id = cdesc.getID();
		ComponentType ct = cdesc.getCT();
		CompDesc cd = new CompDesc(id,ct,compIDs);
		return cd;
	}

	/**
	 * Add the identifier of the specified component and of all its
	 * subcomponents to the specified list.
	 *
	 * @param cdesc    the component to traverse
	 * @param compIDs  the list of identifiers
	 */
	private void addCompID( ComponentDesc<CT> cdesc, List<String> compIDs ) {

		String id = cdesc.getID();
		compIDs.add(id);

		List<ComponentDesc<CT>> subcompdescs = cdesc.getSubComponents();
		for (ComponentDesc<CT> subcompdesc : subcompdescs) {
			addCompID(subcompdesc,compIDs);
		}
	}


	// ------------------------------------------------------------------
	// Component factory code generation
	// ------------------------------------------------------------------

	/**
	 * Generate a factory for the current component.
	 *
	 * @param pw     the print writer where the factory is to be outputted
	 * @param declareLocalVars
	 *      <code>true</code> if the variables which hold references to
	 *      components need to be declared
	 */
	protected void generate( PrintWriter pw, boolean declareLocalVars )
	throws IOException {
		generateInstantiations(cdesc,pw,declareLocalVars);
		generateHierarchy(cdesc,pw);
		generateBindings(cdesc,pw);
	}

	/*
	 * In the following methods, cdesc is added systematicaly as a parameter,
	 * although cdesc is an instance variable. We need this extra parameter as
	 * these methods may be called recursively with different cdescs.
	 */

	protected void generateInstantiations(
		ComponentDesc<CT> cdesc, PrintWriter pw, boolean declareLocalVars )
	throws IOException {

		String ctrlDesc = cdesc.getCtrlDesc();
		String id = cdesc.getID();

		/*
		 * Generate the membrane code for the component.
		 */
		FCSourceCodeGeneratorItf fcscg =
			jc.lookupAndFilter(FCSourceCodeGeneratorItf.class,ctrlDesc);
		SourceCodeGeneratorItf icg = fcscg.generate(cdesc);

		/*
		 * Instantiate the component.
		 */
		String initializerClassName = icg.getTargetTypeName();

		if( declareLocalVars ) {
			pw.print  (Component.class.getName());
			pw.print  (" ");
		}
		pw.print  (id);
		pw.print  (" = new ");
		pw.print  (initializerClassName);
		pw.println("().newFcInstance();");

		generateComponentName(cdesc,pw);
		generateAttributes(cdesc,pw);

		/*
		 * Iterate on sub-components.
		 */
		List<ComponentDesc<CT>> subComponents = cdesc.getSubComponents();
		for (ComponentDesc<CT> sub : subComponents) {
			generateInstantiations(sub,pw,declareLocalVars);
		}
	}

	protected void generateComponentName(
		ComponentDesc<CT> cdesc, PrintWriter pw ) {

		String id = cdesc.getID();
		String name = cdesc.getName();

		pw.print  (Fractal.class.getName());
		pw.print  (".getNameController(");
		pw.print  (id);
		pw.print  (").setFcName(\"");
		pw.print  (name);
		pw.println("\");");
	}

	protected void generateAttributes(
		ComponentDesc<CT> cdesc, PrintWriter pw ) {

		Set<String> names = cdesc.getAttributeNames();
		String id = cdesc.getID();

		for( String name : names ) {

			AttributeDesc adesc = cdesc.getAttribute(name);
			String settername = adesc.getSetterName();

			if( settername == null ) {
				// Skip readonly attribute
				continue;
			}

			String attributeController = adesc.getAttributeController();
			String type = adesc.getType();
			String value = adesc.getValue();

			pw.print  ("((");
			pw.print  (attributeController);
			pw.print  (")");
			pw.print  (Fractal.class.getName());
			pw.print  (".getAttributeController(");
			pw.print  (id);
			pw.print  (")).");
			pw.print  (settername);

			/*
			 * Dependending on the type of the attribute, determine the
			 * delimiter for its value: " for strings, ' for characters, nothing
			 * in other cases.
			 */
			final String delimiter =
				getValueDelimiter(type,attributeController,settername);

			pw.print  ("(");
			pw.print  (delimiter);
			pw.print  (value);
			pw.print  (delimiter);
			pw.println(");");
		}
	}

	protected void generateHierarchy( ComponentDesc<CT> cdesc, PrintWriter pw ) {

		List<ComponentDesc<CT>> subComponents = cdesc.getSubComponents();
		String id = cdesc.getID();

		for (ComponentDesc<?> sub : subComponents) {
			String subid = sub.getID();
			pw.print  (Fractal.class.getName());
			pw.print  (".getContentController(");
			pw.print  (id);
			pw.print  (").addFcSubComponent(");
			pw.print  (subid);
			pw.println(");");
		}

		/*
		 * Iterate on sub-components.
		 */
		for (ComponentDesc<CT> sub : subComponents) {
			generateHierarchy(sub,pw);
		}
	}

	protected void generateBindings( ComponentDesc<CT> cdesc, PrintWriter pw ) {

		List<BindingDesc> bdescs = cdesc.getBindingDescs();
		String id = cdesc.getID();
		List<ComponentDesc<CT>> subComponents = cdesc.getSubComponents();

		for (BindingDesc bdesc : bdescs) {

			BindingType btype = bdesc.getBindingType();
			String cltItfName = bdesc.getCltItfName();
			String srvItfName = bdesc.getSrvItfName();
			ComponentDesc<?> srv = bdesc.getSrv();
			String srvid = srv.getID();

			pw.print  (Fractal.class.getName());
			pw.print  (".getBindingController(");
			pw.print  (id);
			pw.print  (").bindFc(\"");
			pw.print  (cltItfName);
			pw.print  ("\",");
			if( btype.equals(BindingType.IMPORT) ) {
				pw.print  (Fractal.class.getName());
				pw.print  (".getContentController(");
				pw.print  (srvid);
				pw.print  (").getFcInternalInterface(\"");
			}
			else {
				pw.print  (srvid);
				pw.print  (".getFcInterface(\"");
			}
			pw.print  (srvItfName);
			pw.println("\"));");
		}

		/*
		 * Iterate on sub-components.
		 */
		for (ComponentDesc<CT> sub : subComponents) {
			generateBindings(sub,pw);
		}
	}

	/**
	 * Return the delimiter for the value set, either by the specified type, or
	 * by the specified setter method name in the specified interface.
	 *
	 * @param type        the type of the value
	 * @param itfName     the interface name
	 * @param setterName  the setter method name
	 * @return            the delimiter
	 * @since 2.7
	 */
	protected String getValueDelimiter(
		String type, String itfName, String setterName ) {

		/*
		 * We do not use type here, and only rely on itfName and setterName for
		 * retrieving the delimiter. The reason is that type is null for ADL
		 * models built by JuliacAttributeBuilder. type is used in
		 * org.objectweb.fractal.juliac.adlet.desc.ComponentDescFactoryClassGenerator
		 * where the current method is overriden.
		 */

		final Class<?> atype = getAttrType(itfName,setterName);
		final String delimiter = ClassHelper.getValueDelimiter(atype);
		return delimiter;
	}


	// ------------------------------------------------------------------
	// Implementation specific
	// ------------------------------------------------------------------

	/**
	 * Return the type of the attribute set by the specified setter method name
	 * in the specified interface.
	 *
	 * @param itfName     the interface name
	 * @param setterName  the setter method name
	 * @return            the type of the attribute
	 * @throws IllegalArgumentException
	 *         if the interface does not contain the corresponding setter
	 * @since 2.2.4
	 */
	private Class<?> getAttrType( String itfName, String setterName ) {

		Class<?> cl = jc.loadClass(itfName);
		Method[] ums = cl.getMethods();
		Method umeth = null;
		for (Method um : ums) {
			String umname = um.getName();
			if( umname.equals(setterName) ) {
				umeth = um;
				break;
			}
		}
		if( umeth == null ) {
			final String msg = "No setter method "+setterName+" in "+itfName;
			throw new IllegalArgumentException(msg);
		}

		Class<?>[] ptypes = umeth.getParameterTypes();
		if( ptypes.length == 0 ) {
			final String msg =
				"Method "+setterName+" in "+itfName+
				" is expected to declare at least one parameter";
			throw new IllegalArgumentException(msg);
		}

		return ptypes[0];
	}
}
