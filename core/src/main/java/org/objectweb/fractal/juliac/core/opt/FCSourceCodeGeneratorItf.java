/***
 * Juliac
 * Copyright (C) 2008-2021 Inria, Univ. Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.juliac.core.opt;

import java.io.IOException;
import java.util.function.Predicate;

import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.JuliacModuleItf;
import org.objectweb.fractal.juliac.api.desc.MembraneDesc;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.core.desc.ComponentDesc;

/**
 * Interface for Fractal component code generators.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille.fr>
 * @since 1.2
 */
public interface FCSourceCodeGeneratorItf
extends JuliacModuleItf, Predicate<Object> {

	/**
	 * Set the prefix for controller descriptors associated with this generator.
	 * This prefix is of the form <code>/prefix/</code> and may be specified
	 * when several different optimization level source code generators are used
	 * jointly to distinguish which optimization level should be used for a
	 * particuler component, e.g. <code>/koch/primitive</code> to specify that
	 * this is a primitive component that must be handled by the optimization
	 * level source code generator associated with the <code>/koch/</code>
	 * prefix.
	 *
	 * @param prefix  the controller descriptor prefix
	 * @since 2.5
	 */
	public void setCtrlDescPrefix( String prefix );

	/**
	 * Generate the source code associated to a Fractal component.
	 * The membrane implementation and the initializer implementation are
	 * generated.
	 *
	 * @param cdesc  the component descriptor
	 * @return       the initializer class generator for this component
	 * @since 2.7
	 */
	default SourceCodeGeneratorItf generate( ComponentDesc<?> cdesc )
	throws IOException {
		Type type = cdesc.getCT();
		Object controllerDesc = cdesc.getCtrlDesc();
		Object contentDesc = cdesc.getContentClassName();
		Object source = cdesc.getSource();
		SourceCodeGeneratorItf scg =
			generate(type,controllerDesc,contentDesc,source);
		return scg;
	}

	/**
	 * Generate the source code associated to a Fractal component.
	 * The membrane implementation and the initializer implementation are
	 * generated.
	 *
	 * @param type            the component type
	 * @param controllerDesc  the component controller descriptor
	 * @param contentDesc     the component content descriptor
	 * @param source          the code element that triggered the generation
	 * @return  the initializer class generator for this component
	 */
	public SourceCodeGeneratorItf generate(
		Type type, Object controllerDesc, Object contentDesc, Object source )
	throws IOException;

	/**
	 * Generate the source code of the membrane associated to a Fractal
	 * component.
	 *
	 * @param ct                the component type
	 * @param ctrlDesc          the component controller descriptor
	 * @param contentClassName  the name of the component content class
	 * @param source            the code element that triggered the generation
	 * @return                  a membrane descriptor
	 */
	public MembraneDesc<?> generateMembraneImpl(
		ComponentType ct, String ctrlDesc, String contentClassName,
		Object source )
	throws IOException;

	/**
	 * Return the code generator for component interfaces.
	 */
	public SourceCodeGeneratorItf getInterfaceClassGenerator(InterfaceType it);
}
